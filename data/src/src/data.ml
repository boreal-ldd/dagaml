type ('k, 'v) ht = ('k, 'v) Hashtbl.t

type ('k, 'vk, 'vv) data = {
  data : ('vk, 'vv) ht;
  sons : ('k, ('k, 'vk, 'vv) data) ht;
}

type ('kk, 'kv, 'vk, 'vv) t = {
  mutable keys : 'kk list;
  mutable vals : 'vk list;
  mutable keys_vals : ('kk, 'kv list) Hashtbl.t;
  mutable tree : ('kk * 'kv, 'vk, 'vv) data;
  mutable default_size : int;
}

let empty_data default_size =
  { data = Hashtbl.create default_size;
    sons = Hashtbl.create default_size }

let create ?(default_size = 32) () =
  {
    keys = [];
    vals = [];
    keys_vals = Hashtbl.create default_size;
    tree = empty_data default_size;
    default_size;
  }

(*  adds the element [e] at the end of the list [l]
      if it does not already exists in [l]
    returns [true] iff the element is added
 *)
let add_last l x : ( _ list * bool) =
  let rec funrec carry l x : _ list option =
    match l with
    | [] -> Some (List.rev_append carry [x])
    | h::t ->
      if h = x
      then None
      else funrec (h::carry) t x
  in
  match funrec [] l x with
  | Some l -> (l, true )
  | None   -> (l, false)

let add_key t kk =
  t.keys <- fst (add_last t.keys kk)

let add_val t vk =
  t.vals <- fst (add_last t.vals vk)

let add_key_val t kk kv =
  add_key t kk;
  match Hashtbl.find_opt t.keys_vals kk with
  | Some kvl ->
    Hashtbl.replace t.keys_vals kk (fst(add_last kvl kv))
  | None ->
    Hashtbl.add t.keys_vals kk [kv]

let get_key_val t kk : _ list  =
  match Hashtbl.find_opt t.keys_vals kk with
  | Some kvl -> kvl
  | None     -> []

let check_legit t kk_kv : unit =
  Hashtbl.iter
    (fun kk kv -> assert(List.mem kk t.keys))
    kk_kv;
  ()

let get_subtree t kk_kv : (_, _, _) data =
  check_legit t kk_kv;
  let tree = ref t.tree in
  List.iter
    (fun kk ->
      match Hashtbl.find_opt kk_kv kk with
      | Some kv ->
      (
        match Hashtbl.find_opt (!tree).sons (kk, kv) with
        | Some tree' -> tree := tree'
        | None -> (
          let tree' = empty_data t.default_size in
          Hashtbl.add (!tree).sons (kk, kv) tree';
          tree := tree'
        )
      )
      | None -> ()
    ) t.keys;
  !tree

let set_value t kk_kv vk vv =
  let tree = get_subtree t kk_kv in
  assert(List.mem vk t.vals);
  Hashtbl.replace tree.data vk vv

let get_value t kk_kv vk =
  let tree = get_subtree t kk_kv in
  assert(List.mem vk t.vals);
  Hashtbl.find tree.data vk

let get_value_opt t kk_kv vk =
  let tree = get_subtree t kk_kv in
  Hashtbl.find_opt tree.data vk

module OfS =
struct
  let single_split_on_char s c =
    match String.index_opt s c with
    | Some i ->
    (
      let pref = String.sub s 0 i in
      let post = String.sub s
        (succ i)
        (String.length s - (succ i))
      in
      Some (i, pref, post)
    )
    | None -> None

  let parse_line t line =
    let liste =
      line
      |> String.split_on_char ' '
      |> List.filter (fun v -> v <> "")
    in
    let kk_kv = Hashtbl.create t.default_size in
    List.iter (fun word ->
      match single_split_on_char word ':' with
      | Some(i, kk, kv) -> (
        add_key_val t kk kv;
        Hashtbl.add kk_kv kk kv
      )
      | None ->
      match single_split_on_char word '=' with
      | Some(i, vk, vv) -> (
        add_val t vk;
        set_value t kk_kv vk vv
      )
      | None -> (
        print_string ("the word \""^word^"\" could not be parsed"); print_newline();
        assert false
      )
    ) liste

  let load_channel t cha =
    Tools.iter_input_lines cha (parse_line t)

  let load_file t target =
    let cha = open_in target in
    load_channel t cha;
    close_in cha;
    ()
end

module ToS =
struct
  let dump_channel_data t cha data kl =
    if Hashtbl.length data > 0
    then (
      List.iter
        (fun (kk, kv) ->
          output_string cha kk;
          output_string cha ":";
          output_string cha kv;
          output_string cha " ")
        kl;
      List.iter
        (fun vk -> match Hashtbl.find_opt data vk with
          | Some vv -> (
            output_string cha vk;
            output_string cha "=";
            output_string cha vv;
            output_string cha " "
          )
          | None    -> ()) t.vals;
      output_string cha "\n";
      flush cha
    )

  let rec dump_channel_rec t cha tree kl =
    dump_channel_data t cha tree.data kl;
    Hashtbl.to_seq tree.sons
      |> MySeq.sorted
        ~cmp:(fun kd kd' -> Stdlib.compare (fst kd) (fst kd'))
      |> Seq.iter
        (fun (k, tree') -> dump_channel_rec t cha tree' (kl@[k]))

  let dump_channel t cha =
    dump_channel_rec t cha t.tree []

  let dump_file t target =
    let cha = open_out target in
    dump_channel t cha;
    close_out cha;
    ()

  let print t =
    dump_channel t Stdlib.stdout

end
