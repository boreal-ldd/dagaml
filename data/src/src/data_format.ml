let _ =
  let file_in = Sys.argv.(1) in
  let file_out = Sys.argv.(2) in
  let data = Data.create () in
  Data.OfS.load_file data file_in;
  Data.ToS.dump_file data file_out;
  exit 0
