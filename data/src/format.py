import sys

def do(target) :
  FILE = open(target, 'r')
  TEXT0 = FILE.read()
  TEXT = TEXT0
  FILE.close()
  TEXT = TEXT.replace('\t', '  ')
  while TEXT.find(' \n') >= 0 :
    TEXT = TEXT.replace(' \n', '\n')
  while TEXT.find('\n\n\n') >= 0 :
    TEXT = TEXT.replace('\n\n\n', '\n\n')
  if TEXT != TEXT0 :
    FILE = open(target, 'w')
    FILE.write(TEXT)
    FILE.close()

do(sys.argv[1])
