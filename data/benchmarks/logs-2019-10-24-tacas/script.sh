data_script="python3 ../../script/"
if true
then
	echo "" > data/benches.data
fi

## Section 1 agregation of data within each benchmark set

# iscas99
if true
then
	echo "do bench:iscas99"
	#python3 $extra/from_stats_csv_to_data_v2.py csv/iscas99-stats.csv data/iscas99-stats.data
	#python3 $extra/from_model_csv_to_data.py csv/iscas99-o-nucx.csv o-nucx data/iscas99-o-nucx.data
	cat data/iscas99-stats.data data/iscas99-o-nucx.data > data/iscas99-full.data
	python3 $extra/data_format.py data/iscas99-full.data data/iscas99-full.data
	#python3 $extra/script_total_size.py data/iscas99-full.data data/iscas99-total-size.data
	#cat data/iscas99-total-size.data >> data/iscas99-full.data
	python3 $extra/data_format.py data/iscas99-full.data data/iscas99-full.data
	#python3 $extra/script_log_average.py data/iscas99-full.data size_total_byte data/iscas99-log-total-size.data
	#python3 $extra/script_log_average.py data/iscas99-full.data num_nodes       data/iscas99-log-num-nodes.data
	cat data/iscas99-log-total-size.data data/iscas99-log-num-nodes.data | sed "s/^/bench:iscas99 /" >> data/benches.data
fi
#lgsynth91
if true
then
	echo "do bench:lgsynth91"
	python3 $extra/from_stats_csv_to_data_v2.py csv/lgsynth91-stats.csv data/lgsynth91-stats.data
	python3 $extra/from_model_csv_to_data.py csv/lgsynth91-o-nucx.csv o-nucx data/lgsynth91-o-nucx.data
	cat data/lgsynth91-stats.data data/lgsynth91-o-nucx.data > data/lgsynth91-full.data
	python3 $extra/data_format.py data/lgsynth91-full.data data/lgsynth91-full.data
	python3 $extra/script_total_size.py data/lgsynth91-full.data data/lgsynth91-total-size.data
	cat data/lgsynth91-total-size.data >> data/lgsynth91-full.data
	python3 $extra/data_format.py data/lgsynth91-full.data data/lgsynth91-full.data
	python3 $extra/script_log_average.py data/lgsynth91-full.data size_total_byte data/lgsynth91-log-total-size.data
	python3 $extra/script_log_average.py data/lgsynth91-full.data num_nodes       data/lgsynth91-log-num-nodes.data
	cat data/lgsynth91-log-total-size.data data/lgsynth91-log-num-nodes.data | sed "s/^/bench:lgsynth91 /" >> data/benches.data
fi
#satlib
if true
then 
	for bench in 'uf20' 'uf50' 'uf75' 'uf100' 'uf125'
	do
		echo "do bench:$bench step:model-enumeration"
		echo "" > data/$bench.data
		for model in 'o-u' 'o-nu' 'o-c10' 'o-uc0' 'o-nucx' 'u-nu' 'u-nuc' 'u-nux' 'l-nux'
		do
			#echo "do bench:$bench model:$model"
			python3 $extra/from_model_csv_to_data.py csv/$bench-$model.csv $model data/$bench-$model.data
			cat data/$bench-$model.data 
			cat data/$bench-$model.data >> data/$bench.data
		done
		echo "do bench:$bench step:finalize"
		python3 $extra/data_format.py data/$bench.data data/$bench.data
		python3 $extra/script_log_average.py data/$bench.data size_total_byte data/$bench-log-size.data
		python3 $extra/script_log_average.py data/$bench.data num_nodes       data/$bench-log-node.data
		cat data/$bench-log-size.data data/$bench-log-node.data | sed "s/^/bench:cnf-$bench /" >> data/benches.data
	done
fi

#(negb) satlib
if true
then 
	for bench_name in 'uf20' 'uf50' 'uf75' 'uf125'
	do
		bench="negb-$bench_name"
		bench_tag="negb-cnf-$bench_name"
		echo "do bench:$bench step:model-enumeration"
		echo "" > data/$bench.data
		for model in 'o-u' 'o-nu' 'o-c10' 'o-uc0' 'o-nucx' # 'u-nu' 'u-nuc' 'u-nux' 'l-nux'
		do
			#echo "do bench:$bench model:$model"
			python3 $extra/from_model_csv_to_data.py csv/$bench-$model.csv $model data/$bench-$model.data
			cat data/$bench-$model.data >> data/$bench.data
		done
		echo "do bench:$bench step:finalize"
		python3 $extra/data_format.py data/$bench.data data/$bench.data
		python3 $extra/script_log_average.py data/$bench.data size_total_byte data/$bench-log-size.data
		python3 $extra/script_log_average.py data/$bench.data num_nodes       data/$bench-log-node.data
		cat data/$bench-log-size.data data/$bench-log-node.data | sed "s/^/bench:$bench_tag /" >> data/benches.data
	done
fi

# Section 2 : agregation of data between benchmark sets
if true
then
	echo "do step:finalize"
	python3 $extra/data_format.py data/benches.data data/benches.data
	python3 $extra/script_log_cmp.py data/benches.data size_total_byte_log_mean o-u data/benches-log-size.data
	python3 $extra/script_log_cmp.py data/benches.data num_nodes_log_mean       o-u data/benches-log-node.data
  cat data/benches-log-size.data data/benches-log-node.data > data/benches-log.data
	python3 $extra/data_to_csv.py data/benches-log.data csv-out/benches-log.csv bench: model: num_nodes_log_mean_rel100= size_total_byte_log_mean_rel100=	
	models1="model:{o-u,o-nu,o-c10,o-uc0,o-nucx}"
	models2="model:{o-u,o-nu,o-c10,o-uc0,o-nucx,u-nu,u-nuc,u-nux,l-nux}"
	python3 $extra/data_to_csv.py data/lgsynth91-full.data csv-out/lgsynth91.csv "file_name:{cm150a_orig,comp_orig,mux_orig,my_adder_orig,rot_orig}" $models2 num_nodes= size_nodes_byte= size_total_byte=
	python3 $extra/data_to_csv.py data/iscas99-full.data csv-out/iscas99.csv "file_name:{b04,b07,b09,b11}" $models2 num_nodes= size_nodes_byte= size_total_byte=
	#WIP
	python3 $extra/data_to_csv_wip.py data/iscas99-full.data csv-out/iscas99_wip.csv -args:subtable_iscas99.txt
	python3 $extra/data_to_csv.py data/uf125.data csv-out/uf125.csv $models2 "file_name:{uf125-035,uf125-071,uf125-078,uf125-088,uf125-096}" num_nodes= size_nodes_byte= size_total_byte=
	python3 $extra/data_to_csv.py data/uf75.data csv-out/uf75.csv $models2 "file_name:{uf75-014,uf75-021,uf75-050,uf75-094,uf75-098}" num_nodes= size_nodes_byte= size_total_byte=
	#WIP
	python3 $extra/data_to_csv_wip.py data/uf75.data csv-out/uf75_wip.csv "$models2:\\H" "file_name:{uf75-014,uf75-021,uf75-050,uf75-094,uf75-098}" num_nodes= size_nodes_byte= size_total_byte=
fi

