import lib
import data

def single_key_root(string):
  if ':' in string :
    l = string.split(':')
    assert(len(l)==2)
    assert(l[0] != '' and l[1] != '')
    return (l[0], l[1])
  else:
    assert(False)

def parse_key_root(liste):
  return list(map(single_key_root, liste))

def single_calc_value(string):
  i0 = string.index(':')
  key = string[:i0]
  expr = string[(i0+1):]
  assert(key != '' and expr != '')
  return (key, expr)

def parse_calc_value(liste):
  return list(map(single_calc_value, liste))

def single_meta_key(dK, string):
  l = string.split(':')
  assert(len(l)==3)
  tk = l[0]
  dk = l[1]
  dv = l[2]
  if tk in dK :
    dK[tk][dk] = dv
  else:
    dK[tk] = {dk:dv}

def parse_meta_key(liste):
  dK = dict()
  for s in liste : single_meta_key(dK, s)
  return dK

__data_process_keys__ = {'meta-key', 'key-root', 'calc-value'}

def data_process(idata, KARGS):
	#parsing options into parameters [data_process_kwargs]
  data_process_kwargs = {
    'meta-key' : parse_meta_key(KARGS.get('meta-key', [])),
    'key-root' : parse_key_root(KARGS.get('key-root', [])),
    'calc-value' : parse_calc_value(KARGS.get('calc-value', [])),
  }
	#processing input data [idata] using parameters [data_process_kwargs]
  return data.process(idata, **data_process_kwargs)

if __name__ == "__main__" :
  import sys
  args = sys.argv[1:]
	#preprocessing arguments (unfolding configuration file)
  args = lib.preproc_args(*args)

  LARGS, SARGS, KARGS, NO_PARSE = lib.preparse_args(*args)

  KARGS_KEYS = {'fin', 'fout'}.add(__data_process_keys__)

  assert(set(KARGS.keys()).issubset(KARGS_KEYS))

	#Catching Input File Name
  if 'fin' in KARGS :
    FileIn = KARGS['fin'][0]
  else:
    FileIn = LARGS[0]
    LARGS  = LARGS[1:]

	#Catching Output File Name
  if 'fout' in KARGS :
    FileOut = KARGS['fout'][0]
  else:
    FileOut = LARGS[0]
    LARGS   = LARGS[1:]

	#Parsing Input File [FileIn] into Input Data [idata]
  idata = data.data(file=FileIn)

	#Processing Input Data [idata] into Processed Data [odata]
  odata = data_process(idata, KARGS)

	#Dumping Processed Data [odata] to Output File [FileOut]
  odata.dump_file(FileOut)
