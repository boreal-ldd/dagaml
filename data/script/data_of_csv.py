import sys
import lib
import data

args = sys.argv[1:]

args = lib.preproc_args(*args)

LARGS, SARGS, KARGS, NO_PARSE = lib.preparse_args(*args)

KARGS_KEYS = {'fin', 'fout', 'meta-key', 'header', 'key-root', 'key-leaf', 'csep', 'lsep', 'calc-value'}

assert(set(KARGS.keys()).issubset(KARGS_KEYS))

if 'fin' in KARGS :
    FileIn = KARGS['fin'][0]
else:
    FileIn = LARGS[0]
    LARGS  = LARGS[1:]

if 'fout' in KARGS :
    FileOut = KARGS['fout'][0]
else:
    FileOut = LARGS[0]
    LARGS   = LARGS[1:]

SETS = KARGS.get('meta-key', [])

if 'header' in KARGS :
    HEAD = []
    for value in KARGS['header'] :
        keys = value.split(',')
        HEAD += keys
else:
    HEAD = None

BODY = None

ODATA = data.data()

def parse_calc_value(string):
    key, sep, expr = lib.split_first(string, ':')
    assert(key != '' and expr != '')
    ODATA.new_val_key(key)
    return (key, expr)

LINE_CALC = \
    list(map(parse_calc_value, KARGS.get('calc-value', [])))

def parse_key_root(string):
    res = lib.split_first(string, ':', '=')
    if res == None: assert(False)
    else:
        key, sep, value = res
        assert(key != '' and value != '')
        if sep == ':' :
            ODATA.new_key_key(key)
            return (key, value)
        elif sep == '=' : assert(False)
        else : assert(False)

def parse_key_leaf(string):
    res = lib.split_first(string, ':', '=')
    if res == None: assert(False)
    else:
        key, sep, value = res
        if sep == '=' :
            ODATA.new_val_key(key)
            return (key, value)
        elif sep == ':' : assert(False)
        else : assert(False)

DDR = dict(map(parse_key_root, KARGS.get('key-root', [])))
DDL = dict(map(parse_key_leaf, KARGS.get('key-leaf', [])))

CSEP = KARGS['csep'][-1] if 'csep' in KARGS else ' '
LSEP = KARGS['lsep'][-1] if 'lsep' in KARGS else '\n'

FILE = open(FileIn)
TEXT = lib.elim_epsilon([lib.elim_epsilon(line.split(CSEP)) for line in FILE.read().split(LSEP)])
FILE.close()

dK = dict()

def parse_keys(string):
    D = dict()
    key, sep, value = lib.split_first(string, ':', '=')
    assert(key != '')
        # [key] can't be empty
    if value != '' :
        print('[error] [data_of_csv/parse_keys] [value] should be empty')
        print('string:%s\nkey:%s\nsep:%s\nvalue:%s'%(string, key, sep, value))
        assert(False)
        # [value] should be empty (for now) as no semantic is attached
    if sep == ':':
        ODATA.new_key_key(key)
        D['type'] = 'KK'
    elif sep == '=':
        ODATA.new_val_key(key)
        D['type'] = 'VK'
    else : assert(False)
    dK[key] = D
    return (key, D)

if   HEAD == None and BODY == None :
    HEAD = [parse_keys(key) for key in TEXT[0]]
    #print(HEAD)
    BODY = TEXT[1:]
    #print(BODY[0])
elif HEAD != None and BODY == None :
    HEAD = [parse_keys(key) for key in HEAD]
    BODY = TEXT
elif HEAD != None and BODY != None :
    pass
elif HEAD == None and BODY != None :
    assert(False)

def parse_args(string):
    """ --set:table-key:dict-key:dict-value """
    l = string.split(':')
    try:
        tk = l[0]
        dk = l[1]
        dv = l[2]
        dK[tk][dk] = dv
    except:
        print('--set:%s:%s:%s'%(l[0], l[1], l[2]))
        assert(False)

for arg in SETS : parse_args(arg)

for line in BODY :
    DK = DDR.copy()
    DV = dict()
    for (k, km), v in zip(HEAD, line):
        # apply:format-value
        if   'format-value' in km :
            format_value = km['format-value']
            if format_value == 'extract-file-name' :
                v = lib.parse_file_name(v)
            else :
                assert(False)
        else :
            pass
        # apply:type
        if   km['type'] == 'KK' :
            DK[k] = v
        elif km['type'] == 'VK' :
            ODATA.set_value(DK, k, v)
            DV[k] = v
        else:
            assert(False)
    for key, value in DDL.items():
        ODATA.set_value(DK, key, value)
        DV[key] = value
    for k, e in LINE_CALC :
        try :
            ee = e.format(**DV)
            v = str(eval(ee)).replace(' ','')
            #print('[eval] : %s -> %s -> %s'%(e, ee, v))
            DV[k] = str(v)
            ODATA.set_value(DK, k, v)
        except KeyError :
            print('[warning] KeyError upon --calc-value evaluation')
            print('[warning] [line] '+' '.join(line))
            print('[warning] [clef] '+k)
            print('[warning] [eval] '+e)
            print()

ODATA.dump_file(FileOut)
