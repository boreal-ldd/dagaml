def analyse(L):
  D = dict()
  for x in L:
    D[x] = D.get(x, 0) + 1
  return D

def multi_replace(T, S, d):
  for s in S:
    T = T.replace(s, d)
  return T

def float_of_time(x) :
  l = x.split('m')
  m = int(l[0])
  l = float(l[1].split('s')[0].replace(',', '.'))
  return 60*m + l

def init_set(x, *args):
  if len(args) == 1:
    return {args[0]:x}
  else:
    return {args[0]:init_set(x, *args[1:])}

def dict_set(D, x, *args):
  if len(args) == 1:
    D[args[0]] = x
  else:
    if args[0] in D:
      dict_set(D[args[0]], x, *args[1:])
    else:
      D[args[0]] = init_set(x, *args[1:])

def big_union(S, *SS) :
  A = S.copy()
  for s in SS :
    A = A.union(s)
  return A

def normalize(t):
  return t.replace(' ', '_').replace(':', '-').replace('=', '-')

def split_file_name(full):
  l = full.split('.')
  name = l[0].split('/')
  key = normalize(name[-1])
  kdir = normalize('/'.join(name[:len(name)-1]))
  tag = normalize('-'.join(l[1:]))
  return (kdir, key, tag)

def speedy(x):
  if x < 0 :
    print('++ speed-up : '+str(1/(x+1)))
  elif x > 0 :
    print('-- speed-down : '+str(x+1))
  else:
    print('== equality == ')

def cmp_avlog(avlogA, avlogB, tagA = "A", tagB = "B", base=10):
  if avlogA < avlogB :
    print(str(tagA)+' faster than '+str(tagB))
    print('avlog delta  :'+str(avlogB-avlogA))
    rate = base**(avlogA-avlogB)-1
    print('speed-up rate:'+str(rate)+'%')
    print('speed-up factor:'+str(1/(rate+1))+'x')
  elif avlogA > avlogB :
    print(str(tagB)+' faster than '+str(tagA))
    print('avlog delta  :'+str(avlogA-avlogB))
    rate = base**(avlogB-avlogA)-1
    print('speed-up rate:'+str(rate)+'%')
    print('speed-up factor:'+str(1/(rate+1))+'x')
  else:
    print('== equality ==')

def tr_min(M) :
  if len(M) == 0 : return []
  else:
    nC = min(map(len, M))
    return [[line[i] for line in M] for i in range(nC)]

def tr_max(M, default=None):
  if len(M) == 0 : return []
  else:
    nC = max(map(len, M))
    return [[line[i] if i < len(line) else default for line in M] for i in range(nC)]

def elim_epsilon(L) :
  return [x for x in L if x != '' and x != []]

def get_stats(L) :
    mean = sum(L)/len(L)
    ecart = (sum((x - mean)**2 for x in L)/len(L))**0.5
    maxi = max(L)
    mini = min(L)
    return(mean, ecart, mini, maxi)

def str_matrix(M, default='', fill = ' ', col_sep = ' ', lin_sep = '\n'):
    assert(len(fill) == 1)
    SM = [list(map(str, line)) for line in M]
    tSM1 = tr_max(SM, default)
    Cw = [max(map(len, col)) for col in tSM1]
    tSM2 = [[x+fill*(w-len(x)) for x in col] for w, col in zip(Cw, tSM1)]
    return lin_sep.join(col_sep.join(map(str, line)) for line in tr_min(tSM2))

def stre_matrix(M, default='', fill = ' ', col_sep = ' ', line_sep = '\n', allign='left') :
    assert(len(fill) == 1)
    for line in M :
      try :
        for s, d in line :
          assert(type(s) == str)
          assert(type(d) == dict)
      except Exception as err:
        print('[lib/stre_matrix] line:%s'%str(line))
        raise err
    DO = {'default':default, 'fill':fill, 'col_sep':col_sep, 'line_sep':line_sep}
    SM = [[(str(v), d) for v, d in line] for line in M]
    tSM1 = tr_max(SM, (default, dict()))
    Cw = [max(len(v) for v, _ in col) for col in tSM1]
    def fill_up_cell(cell, size, param):
      try :
        my_allign = param.get('allign', allign)
        my_fill   = param.get('fill'  , fill)
        #print('param:%s my_allign:%s my_fill:%s'%(str(param), str(my_allign), str(my_fill)))
      except AttributeError as err :
        print('[lib/stre_matrix/fill_up_cell] cell:%s'%str(cell))
        print('[lib/stre_matrix/fill_up_cell] size:%s'%str(size))
        print('[lib/stre_matrix/fill_up_cell] param:%s'%str(param))
        raise err
      if   my_allign == 'left':
        return cell + my_fill * (size - len(cell))
      elif my_allign == 'right':
        return my_fill * (size - len(cell)) + cell
      elif my_allign == 'center':
        n = size - len(cell)
        n2 = n // 2
        n1 = n - n2
        return fill*n1 + cell + fill*n2
      else:
        assert(False)
    try:
      tSM2 = [[fill_up_cell(x, w, d) for x, d in col] for w, col in zip(Cw, tSM1)]
    except AttributeError:
      print('[lib/stre_matrix] M:')
      print(M)
      print('[lib/stre_matrix] tSM1:')
      print(tSM1)
      assert(False)
    return line_sep.join(col_sep.join(map(str, line)) for line in tr_min(tSM2))

def tex_matrix(M, default='', fill = ' ', col_sep = ' & ', lin_sep = ' \\\\\n', **args):
    return str_matrix(M, default, fill, col_sep, lin_sep, **args)

def texe_matrix(M, default='', fill = ' ', col_sep = ' & ', lin_sep = ' \\\\\n', **args):
    return stre_matrix(M, default, fill, col_sep, lin_sep, **args)

def csv_matrix(M):
    return "\n".join(','.join(map(str, line)) for line in M)

def csve_matrix(M, default='', fill = ' ', col_sep = ' , ', lin_sep = ' \n', **args):
    return stre_matrix(M, default, fill, col_sep, lin_sep, **args)

def hcat_matrix(M1, M2) :
    return [l1 + l2 for l1, l2 in zip(M1, M2)]

def hcat_list_matrix(L1, M2) :
    return [[x1] + l2 for x1, l2 in zip(L1, M2)]

def fact(n) :
  if n <= 1 :
    return 1
  else :
    k = 1
    for i in range(2, n+1):
      k = k * i
    return k

def list_slice(L, K) :
    assert(len(L) % K == 0)
    return [[L[k*K+i] for i in range(K)] for k in range(len(L)//K)]

def print_matrix(mat, mode = 'csv'):
    C1bis = [['\\']+HEAD] + lib.hcat_list_matrix(HEAD, mat)
    if mode == 'csv':
        print(csv_matrix(C1bis))
    elif mode == 'str':
        print(str_matrix(C1bis))
    elif mode == 'tex':
        print(tex_matrix(C1bis))
    else :
        print('[print_matrix] error while picking matrix display mode')

def write_matrix(cha, mat, mode = 'csv', expr=False, **args):
  if   mode == 'csv':
    if expr :
      return cha.write(csve_matrix(mat, **args))
    else:
      return cha.write(csv_matrix(mat, **args))
  elif mode == 'str':
    if expr :
      return cha.write(stre_matrix(mat, **args))
    else:
      return cha.write(str_matrix(mat, **args))
  elif mode == 'tex':
    if expr :
      return cha.write(texe_matrix(mat, **args))
    else:
      return cha.write(tex_matrix(math, **args))
  else :
    print('[write_matrix] error while picking matrix display mode (mode:%s)'%str(mode))
  

def parse_file_name(name):
    return name.split('/')[-1].split('.')[0]

def multi_split_list(slist, *splits):
    if len(splits) == 0:
        return slist
    else:
        assert(len(splits) >= 1)
        split0 = splits[0]
        splits0 = splits[1:]
        return multi_split_list(sum((item.split(split0) for item in slist), []), *splits0)

def multi_split(string, *splits):
    return multi_split_list([string], *splits)

def prefix(s1, s2):
    """ return [True] iff [s2] is a prefix of [s1]"""
    return (len(s2) <= len(s1)) and (s1[:len(s2)] == s2)

def preproc_args(*args):
  ARGS = []
  for arg in args :
    if prefix(arg, "--args:") :
      arg = arg[len("--args:"):]
      if ':' in arg :
        i0 = arg.index(':')
        target = arg[:i0]
        params = [word.split(':') for word in arg[(i0+1):].split(',')]
      else:
        target = arg
        params = []
      FILE = open(target)
      TEXT = FILE.read()
      FILE.close()
      for k, v in params :
        TEXT = TEXT.replace('{{'+k+'}}', v)
      lines = [line.split('#')[0].replace(' ', '') for line in TEXT.split('\n')]
      args = elim_epsilon(multi_split_list(lines, ' ', '\n', '\t'))
      ARGS += preproc_args(*args)
    else:
      ARGS.append(arg)
  return ARGS

# [preparse_argcs *args = (LARGS, SARGS, KARGS, NO_PARSE)]
#
# LARGS = []
# SARGS = ''
# NO_PARSE = False
# KARGS = (string, string list) dict
#
# for arg in args:
#   if NO_PARSE    : LARGS += [arg]
#   else
#    match arg with
#   | -word       -> SARGS += word
#   | --          -> NO_PARSE = True
#   | --key:value -> KARGS[key] += [value]
#   | --key       -> KARGS[key] += [None]
#   | _           -> LARGS += [arg]

def preparse_args(*args):
  SARGS = ['']
  KARGS = dict()
  NO_PARSE = [False]
  def parse_arg(arg) :
    if len(arg) == 0 :
      return None
    elif NO_PARSE[0] :
      return arg
    elif arg[0] == '-' :
      if len(arg) == 1 :
        assert(False)
      elif arg[1] == '-' :
        if len(arg) == 2 :
          NO_PARSE[0] = True
          return None
        if ':' in arg :
          i0 = arg.index(':')
          key = arg[2:i0]
          val = arg[(i0+1):]
        else:
          key = arg[2:]
          val = None
        if key in KARGS :
          KARGS[key].append(val)
        else:
          KARGS[key] = [val]
        return None
      else:
        SARGS[0] += arg[1:]
        return None
    else:
      return arg
  LARGS = [arg for arg in map(parse_arg, args) if arg != None]
  return (LARGS, SARGS[0], KARGS, NO_PARSE[0])

def parse_largs(LARGS, KARGS, *keys):
  def do(key):
    if key in KARGS:
      return KARGS[key][0]
    else:
      V = LARGS.pop(0)
      KARGS[key] = [V]
      return V
  return list(map(do, keys))

import math

def si_positive(x, pprint=False):
  lx = int(math.log(x, 1000))
  if lx == 0 :
    return str(x)
  else:
    nx = int(1000*x*(1000**(-lx)))
    snx = str(nx)
    big = snx[:-3]
    low = snx[-3:]
    snx = big+'.'+low
    DD = {-1:'m', -2:'\mu', -3:'n', 1:'k', 2:'M', 3:'G', 4:'T', 5:'P'}
    if pprint and lx in DD :
      return snx+' '+DD[lx]
    else:
      return snx+' E'+str(lx*3)

def si_float(x, pprint=False, sign_plus='', sign_minus='-'):
  try:
    if x == 0 :
      return '0'
    elif x > 0 :
      return sign_plus+si_positive(x, pprint)
    else :
      assert(x < 0)
      return sing_minus+si_positive(-x, pprint)
  except:
    print('[lib/si_float] [error]')
    assert(False)

def si_positiveK3(x, pprint=False):
  lx = int(math.log(x, 1000))
  if lx == 0 :
    if int(x) == x :
      return str(int(x))
    else:
      return str(x)
  else:
    nx = int(1000*x*(1000**(-lx)))
    snx = str(nx)
    big = snx[:-3]
    assert(len(big)<=3)
    low = snx[-3:-len(big)]
    assert(len(big)+len(low)<=3)
    if len(big) == 3 or int(low) == 0:
      snx = big+' '*(4-len(big))
    else:
      snx = big+'.'+low
    DD = {-1:'m', -2:'\mu', -3:'n', 1:'k', 2:'M', 3:'G', 4:'T', 5:'P'}
    if pprint and lx in DD :
      return snx+' '+DD[lx]
    else:
      return snx+' E'+str(lx*3)

def si_floatK3(x, pprint=False, sign_plus='', sign_minus='-'):
  try:
    if x == 0 :
      return '0 '
    else:
      sign = sign_plus if x > 0 else sign_minus
      return sign+si_positiveK3(abs(x), pprint)
  except:
    print('[lib/si_float] [error]')
    assert(False)

def split_first(string, *seps):
    iseps = [(sep, string.index(sep)) for sep in seps if sep in string]
    if iseps == [] :
        return None
    else :
        mindex = len(string) 
        msep   = ''
        for sep, index in iseps :
            if index < mindex :
                mindex = index
                msep = sep
        assert(mindex < len(string))
        head = string[:mindex]
        tail = string[(mindex+len(sep)):]
        return (head, sep, tail)
