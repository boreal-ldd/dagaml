import sys
import lib

file_in = sys.argv[1]
file_out = sys.argv[2]

def parse_line(line):
    hit = lib.split_first(line, ':')
    assert(hit != None)
    (fullname, _, tail) = hit
    l = fullname.split('/')
    directory = '/'.join(l[:-1])
    corename = l[-1]
    hit = lib.split_first(corename, '.')
    assert(hit != None)
    (benchmark, _, ext) = hit
    hit = lib.split_first(tail, ':')
    assert(hit != None)
    (ttype, _, value) = hit
    return "dir:%s bench:%s file_ext:%s max-heap-byte=%s\n"%(directory, benchmark, ext, str(int(value)))

FILE = open(file_in)
M = [ parse_line(line) for line in FILE.read().split('\n') if line != '']
FILE.close()

FILE = open(file_out, 'w')
for line in M: FILE.write(line)
FILE.close()
