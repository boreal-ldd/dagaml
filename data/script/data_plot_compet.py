import sys
import math
import lib
import data

file_in   = sys.argv[1]
file_out  = sys.argv[2]
model_key = sys.argv[3]
time_key  = sys.argv[4]

print('file_in:   %s'%file_in)
print('file_out:  %s'%file_out)
print('model_key: %s'%model_key)
print('time_key:  %s'%time_key)


DICT = dict()

def parse_line(nbl, line):
    words = line.split(' ')
    models = [ word for word in words if lib.prefix(word, model_key) ]
    if not len(models) == 1:
        print('nbl:%d'%nbl)
        print('line:"%s"'%line)
        print('words:%s'%str(words))
        print('models:%s'%str(models))
        assert(False)
    model = models[0][len(model_key):]
    times  = [ word for word in words if lib.prefix(word, time_key ) ]
    assert(len(times) == 1)
    time = float(times[0][len(time_key):])
    if model in DICT :
        DICT[model].append(time)
    else:
        DICT[model] = [time]

FILE = open(file_in)
for nbl, line in enumerate(FILE.readlines()):
    if line != '' : parse_line(nbl, line.replace('\n', ''))
FILE.close()

import matplotlib.pyplot as plt

#plt.subplot(211)
plt.grid(True)
MAX_TIME = None
MAX_LENGTH = 0
for model, times in DICT.items():
    times.sort()
    MAX_TIME = max(MAX_TIME, times[-1]) if MAX_TIME != None else times[-1]
    MAX_LENGTH = max(MAX_LENGTH, len(times))
    #print(model, times)
    X = list(range(len(times)))
    plt.plot(X, times, label=model)
print('MAX_TIME:%d'%MAX_TIME)
print('MAX_LENGTH:%d'%MAX_LENGTH)
plt.xlabel('nb. formulae')
plt.ylabel('time (s)')
plt.axis([0, MAX_LENGTH, 0, MAX_TIME])
#plt.axis([0, MAX_TIME, 0, MAX_LENGTH])
#plt.show()
plt.legend()
plt.savefig(file_out)

"""
#plt.subplot(212)
plt.clf()
plt.grid(True)
plt.plot(TB[ABC-1], label='abc/cudd/robdd')
plt.plot(TB[NU-1] , label='dagaml/grobdd/nu')
plt.plot(TB[NUC-1], label='dagaml/grobdd/nuc')
plt.ylabel('nb. formulae')
plt.xlabel('time (s)')
plt.axis([0, 60, 0, 1000])
#plt.show()
plt.legend()
plt.savefig(file_out+'.cumul.png')
#plt.savefig(file_out+'.svg')
"""

