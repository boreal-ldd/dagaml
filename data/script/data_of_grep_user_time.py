import sys
import lib

file_in = sys.argv[1]
file_out = sys.argv[2]

def parse_line(line):
    hit = lib.split_first(line, ':')
    assert(hit != None)
    (fullname, _, tail) = hit
    l = fullname.split('/')
    directory = '/'.join(l[:-1])
    corename = l[-1]
    hit = lib.split_first(corename, '.')
    assert(hit != None)
    (benchmark, _, ext) = hit
    hit = lib.split_first(tail, '\t')
    assert(hit != None)
    (ttype, _, value) = hit
    if ttype not in {'real', 'user', 'sys'}:
        print("[ttype:%s] not in {'real', 'user', 'sys'}"%(ttype))
        print("[line=%s]"%line)
        print("[file_name=%s]"%file_name)
        print("[core=%s]"%core)
        print("[ext=%s]"%ext)
        print("[ttype=%s]"%ttype)
        print("[value=%s]"%value)
        assert(False)
    fvalue = lib.float_of_time(value)
    return "dir:%s bench:%s file_ext:%s time-%s=%s\n"%(directory, benchmark, ext, ttype, str(fvalue))

FILE = open(file_in)
M = [ parse_line(line) for line in FILE.read().split('\n') if line != '']
FILE.close()

FILE = open(file_out, 'w')
for line in M: FILE.write(line)
FILE.close()
