import sys
import lib
import data
import data_process

args = sys.argv[1:]

args = lib.preproc_args(*args)

LARGS, SARGS, KARGS, NO_PARSE = lib.preparse_args(*args)

KLARGS = lib.parse_largs(LARGS, KARGS, 'fin', 'fout')

FileIn = KARGS['fin'][0]
FileOut = KARGS['fout'][0]

IDATA = data.data(file = FileIn)

def single_key(arg):
  #print('[parse_key] arg:%s'%str(arg))
  if ':' in arg :
    l = arg.split(':')
    kkey = l[0]
    if '\\H' in l :
        i = l.index('\\H')
        mode = 'H'
    else:
        mode = 'V'
    if len(l) == 1 or l[1]=="" or l[1] == '\\*' :
        kvals = list(sorted(IDATA.__keys_vals__[kkey]))
        return ('K'+mode, kkey, kvals)
    elif l[1][0]=='{' :
        assert('}' in l[1])
        kvals = l[1].split('{')[1].split('}')[0].split(',')
        return ('K'+mode, kkey, kvals)
    else:
        print('unrecognized key pattern : "%s"'%arg)
        assert(False)
  elif '=' in arg:
    l = arg.split('=')
    assert(len(l)==2 and l[1]=="")
    return ('V', l[0])
  else:
    assert(False)
    

def parse_keys(LARGS):
  RES = list(map(single_key, LARGS))
  #vertical keys
  KV = [val[1:] for val in RES if val[0] == 'KV']
  #horizontal keys
  KH = [val[1:] for val in RES if val[0] == 'KH']
  #vals
  V  = [val[1:] for val in RES if val[0] == 'V' ]
  return KV, KH, V

KEYS_V, KEYS_H, VALS = parse_keys(LARGS)

#Dict Meta Key
DMK = data_process.parse_meta_key(KARGS.get('meta-key', []))
#print('DMK:%s'%str(DMK))

def get_meta(*args):
  def aux(key):
    if key in DMK :
      DK = DMK[key]
      return dict((k, v) for k, v in DK.items() if k in args)
    else:
      return dict()
  return aux

get_format = get_meta('allign')

def format_value(DK, kval):
  val = str(IDATA.get_value(DK, kval))
  if kval in DMK :
    return data.apply_format(val, DMK[kval])
  else:
    return val

#with horizontal header
def recKH(DK, keys) :
    if keys == [] :
        try:
            TAIL = [(format_value(DK, kval), get_format(kval)) for kval, in VALS]
            return TAIL
        except KeyError :
            print('[warning] [data_to_csv/recKH] DK:%s'%str(DK))
            raise KeyError
    else:
        key, kvals = keys[0]
        TT = []
        for kval in kvals :
            DK2 = DK.copy()
            DK2[key] = kval
            TT += recKH(DK2, keys[1:])
        return TT

def dumpKH(DK):
  HEAD = [(DK[key], get_format(key)) for key, _ in KEYS_V]
  try :
    TAIL = recKH(DK, KEYS_H)
    return [HEAD+TAIL]
  except KeyError :
    print('[warning] [data_to_csv/dumpKH] output:%s'%(FileOut))
    return []

def recKV(DK, keys_h):
    if len(keys_h) == 0 :
        return dumpKH(DK)
    else:
        kkey, kvals = keys_h[0]
        TT = []
        for kval in kvals :
            DK2 = DK.copy()
            DK2[kkey] = kval
            RR = recKV(DK2, keys_h[1:])
            #print(RR)
            TT += RR
        return TT

def dumpKV():
  return recKV(dict(), KEYS_V)

def dump_keyV(kk):
  return (kk+':', get_format(kk))

def dump_keyH(kk):
  return (kk+':\\H', get_format(kk))

def dump_val(vk):
  try:
    return (vk+'=', get_format(vk))
  except:
    print('[dump_val] vk:%s'%str(vk))
    assert(False)

SKEYS_V = [dump_keyV(kk) for kk, _ in KEYS_V]
SKEYS_H = [dump_keyH(kk) for kk, _ in KEYS_H]
SVALS   = [dump_val (vk) for vk,   in VALS  ]
TT = [SKEYS_V + SKEYS_H + SVALS] + dumpKV()
#print(TT)

out_mode = KARGS.get('out-mode', ['str'])[0]
FILE = open(FileOut, 'w')
try:
  lib.write_matrix(FILE, TT, out_mode, expr=True)
  FILE.write('\n')
except AssertionError:
  print(TT)
  assert(False)
FILE.close()


