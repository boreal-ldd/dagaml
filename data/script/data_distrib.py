import sys
import lib
import data
import data_process

args = sys.argv[1:]

args = lib.preproc_args(*args)

LARGS, SARGS, KARGS, NO_PARSE = lib.preparse_args(*args)

KLARGS = lib.parse_largs(LARGS, KARGS, 'fin', 'fout')

FileIn = KARGS['fin'][0]
FileOut = KARGS['fout'][0]

IDATA = data.data(file = FileIn)

CROSS_KEYS = ['file_name', 'model']
VALUE_KEYS = [
    ('card_nodes', {'min':80, 'step':1}),
    ('size_total_bytes', {'min':0, 'step':1000})
]

CROSS = [(kkey, list(sorted(IDATA.__keys_vals__[kkey]))) for kkey in CROSS_KEYS]
VALUE = VALUE_KEYS

ODATA = data.data()

for kk in CROSS_KEYS : ODATA.new_key_key(kk)
for vk, _ in VALUE_KEYS : ODATA.new_val_key(vk)

def doVALUE(DK):
    for (vk, option) in VALUE :
        L = eval(IDATA.get_value(DK, vk))
        assert(type(L) == list)
        if L == []:
            ODATA.set_value(DK, vk, [])
        else:
            MIN = option['min'] if 'min' in option else min(L)
            if 'min' not in option : print('[data_distrib/doVALUE] (DK:%s) (vk:%s) min:%d'%(str(DK), str(vk), MIN))
            MAX = option['max'] if 'max' in option else max(L)
            if 'max' not in option : print('[data_distrib/doVALUE] (DK:%s) (vk:%s) max:%d'%(str(DK), str(vk), MAX))
            STEP = option.get('step', 1)
            NSTEP = (MAX-MIN)/STEP
            NSTEP = int(NSTEP) + 1
            H = [0 for _ in range(NSTEP)]
            out_of_bound = False
            for x in L :
                if x >= MIN and x <= MAX :
                    H[(x-MIN)//STEP]+=1
                else :
                    out_of_bound = True
            if out_of_bound :
                print('[warning] [data_distrib/doVALUE] out of bound value (DK:%s)'%(str(DK)))
            vv = '['+','.join(map(str, H))+']'
            ODATA.set_value(DK, vk, vv)

def recCROSS(DK, keys) :
    if keys == [] :
        try:
            doVALUE(DK)
        except KeyError :
            print('[warning] [data_distrib/recCROSS] DK:%s'%str(DK))
            raise KeyError
    else:
        key, kvals = keys[0] 
        tail = keys[1:]
        for kval in kvals :
            DK2 = DK.copy()
            DK2[key] = kval
            recCROSS(DK2, tail)

recCROSS(dict(), CROSS)
ODATA.dump_file(FileOut)
