import sys
import lib
import data
import data_process

args = sys.argv[1:]

args = lib.preproc_args(*args)

LARGS, SARGS, KARGS, NO_PARSE = lib.preparse_args(*args)

KLARGS = lib.parse_largs(LARGS, KARGS, 'fin', 'fout')

FileIn = KARGS['fin'][0]
FileOut = KARGS['fout'][0]

IDATA = data.data(file = FileIn)

CROSS_KEYS = ['file_name', 'model']
MERGE_KEYS = ['permut']
VALUE_KEYS = ['card_nodes', 'size_nodes_bytes', 'card_edges', 'size_edges_bytes', 'size_total_bytes']

CROSS = [(kkey, list(sorted(IDATA.__keys_vals__[kkey]))) for kkey in CROSS_KEYS]
MERGE = [(kkey, list(sorted(IDATA.__keys_vals__[kkey]))) for kkey in MERGE_KEYS]
VALUE = VALUE_KEYS

ODATA = data.data()

for kk in CROSS_KEYS : ODATA.new_key_key(kk)
for vk in VALUE_KEYS : ODATA.new_val_key(vk)

# recCROSS => enumerates cross-product keys
# recMERGE => enumerates to-be-merged keys
# mergeVALUE => merges to 'dict' of 


def mergeVALUE(K1, K2) :
    return dict((k, K1.get(k, [])+K2.get(k, [])) for k in VALUE)

def recMERGE(DK, keys) :
    if keys == [] :
        value = IDATA.get_values(DK, VALUE)
        return dict(zip(VALUE, map(lambda x : [x], value)))
    else:
        key, kvals = keys[0]
        tail = keys[1:]
        merged = dict()
        for kval in kvals :
            DK2 = DK.copy()
            DK2[key] = kval
            merged = mergeVALUE(merged, recMERGE(DK2, tail))
        return merged

def recCROSS(DK, keys) :
    if keys == [] :
        try:
            merged = recMERGE(DK, MERGE)
            str_merged = [(k, '['+','.join(map(str, v))+']') for (k, v) in merged.items()]
            ODATA.set_values(DK, str_merged)
        except KeyError :
            print('[warning] [data_merge_keys/recCROSS] DK:%s'%str(DK))
            raise KeyError
    else:
        key, kvals = keys[0] 
        tail = keys[1:]
        for kval in kvals :
            DK2 = DK.copy()
            DK2[key] = kval
            recCROSS(DK2, tail)

recCROSS(dict(), CROSS)
ODATA.dump_file(FileOut)
