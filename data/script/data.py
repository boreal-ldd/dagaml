import sys
import lib

class data :
  def __init__(self, **kwargs):
    self.__keys__ = []
    self.__vals__ = []
    self.__keys_vals__ = dict()
    self.__data__ = dict()
    self.__deduction__ = None #WIP
    if 'file' in kwargs :
      DD = dict()
      if 'key_root' in kwargs :
        DD['key_root'] = kwargs['key_root']
      self.load_file(kwargs['file'], **DD)
  
  def __str__(self):
    return str(self.__dict__)
  
  def new_key_key(self, key):
    assert(key not in self.__keys__)
    self.__keys__.append(key)
    self.__keys_vals__[key] = set()

  def new_val_key(self, key):
    if key in self.__vals__ :
        print('[error] [data/class:data/method:new_val_key] val-key alredy registered in self.__vals__')
        print('key:%s'%key)
        print('__vals__:%s'%str(self.__vals__))
        assert(False)
    self.__vals__.append(key)
  
  def __set_value__(self, D, DK, VK, V):
    for key in DK :
      assert(key in self.__keys__)
    if VK not in self.__vals__ :
        print('[error] [data/class:data/method:__set_value__] val-key not registered')
        print('VK:%s'%VK)
        print('__vals__:%s'%str(self.__vals__))
        assert(False)
    for key in self.__keys__ :
      if key in DK :
        kv = (key, DK[key])
        if kv in D:
          D = D[kv]
        else:
          D[kv] = dict()
          D = D[kv]
    if None in D:
      D[None][VK] = V
    else:
      D[None] = {VK : V}
  
  def set_value(self, DK, VK, V):
    self.__set_value__(self.__data__, DK, VK, V)

  def set_values(self, DK, VKVViter):
    for (vk, vv) in VKVViter :
        self.set_value(DK, vk, vv)
  
  def __get_value__(self, D, DK, VK):
    for key in DK :
      if key not in self.__keys__ :
        print('[data/__get_value__] key:%s'%str(key))
        print('[data/__get_value__] __keys__:%s'%str(self.__keys__))
        assert(False)
    if VK not in self.__vals__ :
      print('[data/__get_value__] vk:%s'%str(VK))
      print('[data/__get_value__] __vals__:%s'%str(self.__vals__))
      assert(False)
    for key in self.__keys__ :
        if key in DK :
            kkey = (key, DK[key])
            if kkey in D : D = D[kkey]
            else: raise KeyError(VK)
    if None in D and VK in D[None] :
      return D[None][VK]
    else:
      raise KeyError(VK)
  
  def get_value(self, DK, VK):
    return self.__get_value__(self.__data__, DK, VK)

  def get_values(self, DK, VKiter):
    return [self.get_value(DK, vk) for vk in VKiter]

  def get_item(self, vkey, **kkey):
    return self.get_value(kkey, vkey)
  
  def parse_line(self, line, keys0 = dict()):
    liste = lib.elim_epsilon(line.split(' '))
    keys = keys0.copy()
    last_is_key = False
    for word in liste :
      key, sep, value = lib.split_first(word, ':', '=')
      if sep == ':' :
        last_is_key = True
        if key not in self.__keys__ :
            assert(key not in self.__vals__)
            self.new_key_key(key)
        self.__keys_vals__[key].add(value)
        keys[key] = value
      if sep == '=' :
        last_is_key = False
        if key not in self.__vals__ :
            assert(key not in self.__keys__)
            self.new_val_key(key)
        self.set_value(keys, key, value)
        # [WIP] deduction-rule based computation
        # [NOTE] currently deduction is limited to single hypothesis deductions
        if self.__deduction__ != None :
            if key in self.__deduction__ :
                D = self.__deduction__[key]
                if value in D :
                    for (key, sep, value) in D[value] :
                        assert(sep == '=') #current limitation
                        self.set_value(keys, key, value)
    if last_is_key :
      print("[data:parse_line] [WARNING] data file ending with key descriptor")
  
  def load_file(self, target, **kwargs):
    kr = kwargs.get('key_root', [])
    for k in kr :
      if k not in self.__keys__ :
        self.__keys__.append(k)
        self.__keys_vals__[k] = dict()
    keys0 = dict(kr)
    FILE = open(target)
    for lines in FILE.readlines():
      for line in lines.split('\n') :
        if line!='' and line[0]!='#': self.parse_line(line, keys0)
    FILE.close()
  
  def __map_reduce__(self, fmap, fred, union, D, Ks, DV):
    DV = union(DV, D.get(None, dict()))
    R = fmap(Ks, DV)
    ITER = sorted((k, v) for (k, v) in D.items() if k != None)
    LIST = [(key, sub,  self.__map_reduce__(fmap, fred, union, sub, Ks+[key], DV)) for key, sub in ITER]
    return fred(R, LIST)

  def __map_reduce_fmap__(Ks, DV) : return None
  def __map_reduce_fred__(R, L) : return None
  def __map_reduce_union__(DV, D) : return D

  def map_reduce(self, fmap, fred = __map_reduce_fred__, union = __map_reduce_union__, Ks = [], DV = dict()):
    return self.__map_reduce__(fmap, fred, union, self.__data__, Ks, DV)
  
  def __write__(self, CHA, D, Ks = []):
    N = 0
    if None in D :
      DV = D[None]
      keys = ' '.join(kk+':'+str(kv) for kk, kv in Ks)
      vals = ' '.join(vk+'='+str(DV[vk]) for vk in self.__vals__ if vk in DV)
      N+=CHA.write(keys+' '+vals+'\n')
    for k, v in sorted((k, v) for (k, v) in D.items() if k != None):
      N+=self.__write__(CHA, v, Ks+[k])
    return N
  
  def dump_file(self, target):
    FILE = open(target, 'w')
    self.__write__(FILE, self.__data__, [])
    FILE.close()
  
  def print(self):
    self.__write__(sys.stdout, self.__data__, [])

__process_keys__ = {'meta-key', 'key-root', 'calc-value'}

def apply_format(val, meta):
  if 'format-value' in meta :
    format_value = meta['format-value']
    if format_value == 'extract-file-name' :
      return lib.parse_file_name(val)
    elif format_value == 'si-float' :
      return lib.si_float(float(val)).replace(' ', '')
    elif format_value == 'si-float-pp' :
      return lib.si_float(float(val), pprint=True).replace(' ', '')
    elif format_value == 'si-float-k3' :
      return lib.si_floatK3(float(val), pprint=True).replace(' ', '')
    else:
      assert(False)
  else:
    return val

def process(idata, odata = None, **kwargs):
  assert(set(kwargs.keys()).issubset(__process_keys__))
  odata = odata if odata != None else data()
  key_root = kwargs.get('key-root', [])
  meta_key = dict(kwargs.get('meta-key', []))
  key_root_keys = [k for k, _ in key_root] 
  assert(len(set(idata.__keys__).intersection(key_root_keys))==0)
  #assert(len(idata__.intersection(idata.__keys__)) == 0)
  odata.__keys__= key_root_keys+ idata.__keys__
  odata.__keys_vals__ = dict((k, dict()) for k in key_root)
  odata.__keys_vals__.update(idata.__keys_vals__)
  odata.__vals__ = idata.__vals__
  calc_value = kwargs.get('calc-value', [])
  for k, e in calc_value :
    odata.__vals__.append(k)
  #CDV : Cumulated DV
  #DV  : Dict Value
  def union(CDV_DV, DV) :
    CDV, _ = CDV_DV
    CDV = CDV.copy()
    CDV.update(DV)
    return (CDV, DV)
  def fred(R, L) : 
    return None
  def format_value(k_v):
    k, v = k_v
    if k in meta_key :
      v = apply_format(val, meta_key[k])
    return (k, v)
  def process_key(k_v):
    k, v = format_value(k_v)
    return (k, v)
  def fmap(keys, CDV_DV):
    CDV, DV = CDV_DV
    DK = dict(map(process_key, keys))
    #add all specific values in odata
    for vk_vv in DV.items():
      vk, vv = format_value(vk_vv)
      odata.set_value(DK, vk, vv)
    #compute (if possible) new values
    for k, e in calc_value :
      try :
        ee = e.format(**CDV)
        v = str(eval(e))
        CDV[k] = v
        odata.set_value(DK, k, v)
      except KeyError :
        pass
  idata.map_reduce(fmap, fred, union, Ks = key_root, DV = (dict(), dict()))
  return odata
