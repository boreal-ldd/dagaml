import sys
import lib
import data

FileIn = sys.argv[1]
FileSed = sys.argv[2]
FileOut = sys.argv[3]


FILE = open(FileSed)
TEXT = [line for line in FILE.read().split('\n') if line != '']
FILE.close()

DATA = data.data()

CONV = dict()

VKs = []

for line in TEXT:
    L = line.split(' ')
    index = L.index('=>')
    HYPS = L[:index]
    HYPS = [lib.split_first(word, ':', '=') for word in HYPS]
    # hypothesis
    CONC = L[(index+1):]
    CONC = [lib.split_first(word, ':', '=') for word in CONC]
    for k, s, v in CONC :
        if k not in VKs : VKs.append(k)
    # conclusions
    assert(len(HYPS) == 1)
    # current limitation single hypothesis of the form K=V
    H = HYPS[0]
    key, sep, value = HYPS[0]
    assert(sep == '=')
    if key in CONV :
        if value in CONV[key] :
            CONV[key][value].extend(CONC)
        else:
            CONV[key][value] = CONC
    else:
        CONV[key] = {value:CONC}

for vk in VKs :
    DATA.new_val_key(vk)

DATA.__deduction__ = CONV

DATA.load_file(FileIn)

DATA.dump_file(FileOut)
