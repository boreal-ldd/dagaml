import sys
import lib
import data
import data_process

args = sys.argv[1:]

args = lib.preproc_args(*args)

LARGS, SARGS, KARGS, NO_PARSE = lib.preparse_args(*args)

KLARGS = lib.parse_largs(LARGS, KARGS, 'fin', 'fout')

FileIn = KARGS['fin'][0]
FileOut = KARGS['fout'][0]

IDATA = data.data(file = FileIn)

CROSS_KEYS = ['file_name', 'model']
VALUE_KEYS = ['card_nodes', 'size_nodes_bytes', 'card_edges', 'size_edges_bytes', 'size_total_bytes']
FIELDS = ('mean', 'ecart', 'min', 'max')

CROSS = [(kkey, list(sorted(IDATA.__keys_vals__[kkey]))) for kkey in CROSS_KEYS]
VALUE = VALUE_KEYS

ODATA = data.data()

for kk in CROSS_KEYS : ODATA.new_key_key(kk)
for vk in VALUE_KEYS :
    for tag in FIELDS :
        ODATA.new_val_key(vk+'_'+tag)

def doVALUE(DK):
    for vk in VALUE :
        L = eval(IDATA.get_value(DK, vk))
        assert(type(L) == list)
        values = lib.get_stats(L)
        ODATA.set_values(DK, [(vk+'_'+tag, val) for (val, tag) in zip(values, FIELDS)])

def recCROSS(DK, keys) :
    if keys == [] :
        try:
            doVALUE(DK)
        except KeyError :
            print('[warning] [data_merge_keys/recCROSS] DK:%s'%str(DK))
            raise KeyError
    else:
        key, kvals = keys[0] 
        tail = keys[1:]
        for kval in kvals :
            DK2 = DK.copy()
            DK2[key] = kval
            recCROSS(DK2, tail)

recCROSS(dict(), CROSS)
ODATA.dump_file(FileOut)
