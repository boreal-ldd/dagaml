root="$(readlink -f "$(dirname $(readlink -f $0))/..")"
extra="$root/extra/"
script="$root/script/"

echo "[data] root: " $root
cd $root
for src in $@
do
	echo "src: " $src
	dst=$(python $extra/extract_name.py $src)
	echo "dst: " $dst
	install -D /dev/null $dst.data
	cp $src $dst.data
	python $script/data_format.py $dst.data $dst.format.data
done
