# LGPL-3.0 Linking Exception
#
# Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
#
# DAGaml : Abstract DAG manipulation in OCaml

all:
	rm -rf bin || true &> /dev/null
	mkdir bin || true &> /dev/null
	(cd src && make)

SRCS=-Is src,src/src,src/core,src/ldd_model,src/io,src/conv,src/oops,src/circuits,src/waterfall
NPROC=$(shell nproc || echo 1)

OB=ocamlbuild -r -j $(NPROC) $(SRCS) -use-ocamlfind

lib :
	$(OB) \
		DAGaml.cma \
		DAGaml.d.cma \
		DAGaml.cmi \
		DAGaml.cmx \
		DAGaml.cmxa \
		DAGaml.cmxs

install : lib
	ocamlfind remove  DAGaml
	ocamlfind install DAGaml META _build/DAGaml.*

uninstall : all
	ocamlfind remove  DAGaml

clean:
	rm -rf _build
	cd src && make clean

tests:
	cd src && make tests

tests_waterfall:
	cd src && make tests_waterfall

tests_rbtf:
	cd src && make tests_rbtf

tests_mathieu:
	cd src && make tests_mathieu

tests_crbtf:
	cd src && make tests_crbtf

tests_nary:
	cd src && make nary

ounits:
	cd src && make ounits
