# DAGaml
Abstract manipulations of Directed Acyclic Graph (DAG) using OCaml.

[*Wikipedia*](https://en.wikipedia.org/wiki/Directed_acyclic_graph)

[*OCaml*](https://ocaml.org/)


# Why ?
Directed Acyclic Graphs (DAGs) are present in many fields, for example logic synthesis: logic circuits (such as And-Inverter Graph (AIG)), ROBDD (and LambdaDD), even the Disjoint Support Decomposition (DSD) of a multi output function is a DAG.
For more informations about DAGs, the [Wikipedia page](https://en.wikipedia.org/wiki/Directed_acyclic_graph) is a good starting point.


DAGaml provides a uniform way to handle DAG, in particular: export to Graphviz, serialization and de-serialization.
It is free to use and contributions are welcomed. 
If you do so, please send me a message so I can keep track of who is using it and for which purpose.
Also, feel free to send me links to articles/reports that could help design this library.

If you want to help me design this tool but you don't know where to start, just drop a line. 

# Installation
To properly setup your machine, please refer to INSTALL.md file.

Then, use the script 'make' to compile DAGaml and put symbolic links in a "bin/" folder

If you want to install DAGaml as library, 'make install'.

# How to

Once compiled, you may find the program conv\_ext.native in the "bin/" folder.

You may use it to convert between different file format for example

```shell
>>> bin/conv_ext.native file .v file .o-nu.B.pure
```

Will convert the file 'file.v' (written in Verilog) into a LDD-O-NU representation (Bryant's model extended with complemented edges), you can convert it back to Verilog using :

```shell
>>> bin/conv_ext.native file .o-nu.B.pure file.o-nu .v
```

Even though you could have done both in a raw:


```shell
>>> bin/conv_ext.native file .v file .o-nu.B.pure file.o-nu .v
```

Currently parser for the following languages have been implemented:
- DIMACS (CNF)
- Verilog (very limited, fails if any variable's name contains '()[]', do not support register yet, only a single module)
- PLA (very limited)
- CMD (Command Language for Cloud-DD)

# Notes

The subdirectory data is a simple copy of the DATA project, in order to make the setup simpler

The files located in DAGaml-benchmarks are mostly from official benchmarks, they are here free to use under their initial license.

If you wish to explore the code, use the ARIADNE.md thread ;-)
