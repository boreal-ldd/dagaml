#TODO : add this commend "#!/bin/python3" at the beginning of python's scripts
dagaml="$(readlink -f "$(dirname $(readlink -f $0))/..")"
set -e
extra=$dagaml/extra
script="$dagaml/data/script"
bin="$dagaml/data/bin"

# Section 3 : agregation of data between benchmark sets
if true
then
	echo "do step:finalize"
	#$bin/data_format.native data/benches.data data/benches.data
	#python3 $extra/script_log_cmp.py data/benches.data size_total_byte_log_mean o-u data/benches-log-size.data
	#python3 $extra/script_log_cmp.py data/benches.data num_nodes_log_mean       o-u data/benches-log-node.data
  #cat data/benches-log-size.data data/benches-log-node.data > data/benches-log.data
	#python3 $script/data_to_csv.py data/benches-log.data csv-out/benches-log.csv bench: model: num_nodes_log_mean_rel100= size_total_byte_log_mean_rel100=	
	#models1="model:{o-u,o-nu,o-c10,o-uc0,o-nucx,u-nu,u-nux,u-nuc,u-nucx}:\\H"
	#models1="model:{o-nucx,u-nucx}:\\H"
	models1="model:{dd,o-nucx,u-nucx}:\\H"
	#models2="model:{o-u,o-nu,o-c10,o-uc0,o-nucx,u-nu,u-nuc,u-nux,l-nux}:\\H"
	#models2="model:{o-u,o-nu,u-nu,o-c10,o-uc0,u-nuc,u-nux}:\\H"
	for bench in "uf20-91" "uf75-325" "uf100-430"  "uf125-538" "uf150-645" "uf125-538" "uf175-753" "uf200-860" "lgsynth91" "iscas99" "uint_mult" "nqueens"
	do
		if [ -e data/$bench.data ]
		then
			for outmode in "csv" "str" "tex"
			do
				python3 $script/data_to_csv.py data/$bench.data csv-out/$bench.$outmode "file_name:" $models1 --args:subtable_$outmode.txt:out-mode:$outmode
			done
		else
			echo "missing log file : $bench.data"
		fi
	done
fi
