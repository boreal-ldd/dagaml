(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * SnowStorm : Snowflake boosted with DAGaml
 *)

open GuaCaml
open Extra

module type Sig =
sig
  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  (* [compute
   *      ~model:computing_model=default_computing_model
   *      ~verbose:intverbose=0
   *      ~wap_solver=default_wap_solver
   *       man param funlist = solved] where:
   *  - [model : computing_model], sets the model of LDD to use in the background
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val andl_compute :
    int ->        (* verbose *)
    wap_solver -> (* wap-solver *)
    bool ->       (* support_consistency *)
    MLBDD.man ->
    int list ->
    MLBDD.t list ->
      MLBDD.t

  (* [compute ?verbose:intverbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val argmax_uint_compute :
    int -> (* verbose *)
    wap_solver -> (* wap_solver *)
    (MLBDD.man -> MLBDD.t list -> 'res option) -> (* halt_bpp *)
    bool -> (* support_consistency *)
     MLBDD.man ->
     int list ->
     Snowflake.MlbddUInt.puint list ->
      (MLBDD.t, 'res option) result

  (* [compute ?verbose:intverbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val argmax_uint_select_compute :
    int ->
    wap_solver ->
    (MLBDD.man -> MLBDD.t list -> 'res option) ->
    bool ->
     MLBDD.man ->
     int list ->
     Snowflake.MlbddUInt.puint list ->
      ((MLBDD.var * MLBDD.t)list, 'res option) result
end

module LddMlbdd = Ldd_mlbdd.LDD.OOPS.Model

module type MSig =
sig
  module Q : AQEOPS.MSig
  module WCoSTreD : AQEOPS_CoSTreD_ArgMax_vUInt.Sig
    with type Q.t = Q.t
    and  type Q.f = Q.f
    and  type Q.F.f' = Q.F.f'
  open WCoSTreD
  open Avuint

  val of_mlbdd : Q.t -> LddMlbdd.t -> LddMlbdd.f -> Q.f
  val to_mlbdd : Q.t -> LddMlbdd.t -> Q.f -> LddMlbdd.f
end

module ThisTools =
struct
  let specific_list_max l =
    match l with
    | [] -> -1
    | _  -> fst(MyList.list_max l)

  let specific_support_max f =
    f |> MLBDD.support
      |> MLBDD.list_of_support
      |> specific_list_max

  let specific_support_puint_max x =
    x |> Snowflake.MlbddUInt.P.sorted_support
      |> specific_list_max

  let andl_arity (fl:MLBDD.t list) (p:int list) : int =
    succ (max (specific_list_max p) (fl ||> specific_support_max |> specific_list_max))

  let argmax_uint_arity (fl:Snowflake.MlbddUInt.puint list) (p:int list) : int =
    succ (max (specific_list_max p) (fl ||> specific_support_puint_max |> specific_list_max))
end

module Make(Model:MSig) : Sig =
struct
  let prefix = "Make"

  open Model
  open WCoSTreD
  open Avuint

  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  let lambda_of_mlbdd (t:Q.t) (man:MLBDD.man) (ari:int) (sem:MLBDD.t) : Q.f =
    Model.of_mlbdd t man LddMlbdd.{ari; sem}

  let mlbdd_of_lambda (t:Q.t) (man:MLBDD.man) (f:Q.f) : MLBDD.t =
    (Model.to_mlbdd t man f).LddMlbdd.sem

  let andl_compute
      (verbose:int)
      (wap_solver:wap_solver)
      (support_consistency:bool)
      (man:MLBDD.man)
      (parameters:int list)
      (fl:MLBDD.t list) : MLBDD.t =
    match fl with
    | [] -> MLBDD.dtrue man
    | _ -> (
      let arity = ThisTools.andl_arity fl parameters in
      let t : Q.t = Q.newman() in
      let fl' : Q.f list = fl ||> (lambda_of_mlbdd t man arity) in
      AndL.compute ~verbose ~wap_solver ~support_consistency t arity parameters fl'
      |> (mlbdd_of_lambda t man)
    )

  let lambda_of_mlbdd_uint (t:Q.t) (man:MLBDD.man) (ari:int) (u:Snowflake.MlbddUInt.uint) : Vuint.uint =
    Vuint.{
      arity = ari;
      array = Array.map (lambda_of_mlbdd t man ari) u
    }

  let lambda_of_mlbdd_puint (t:Q.t) (man:MLBDD.man) (ari:int) ((x, u):Snowflake.MlbddUInt.puint) : Avuint.puint =
    (lambda_of_mlbdd t man ari x, lambda_of_mlbdd_uint t man ari u)

  let mlbdd_of_lambda_uint (t:Q.t) (man:MLBDD.man) (u:Vuint.uint) : Snowflake.MlbddUInt.uint =
    Array.map (mlbdd_of_lambda t man) u.Vuint.array

  let mlbdd_of_lambda_puint (t:Q.t) (man:MLBDD.man) ((x, u):Avuint.puint) : Snowflake.MlbddUInt.puint =
    (mlbdd_of_lambda t man x, mlbdd_of_lambda_uint t man u)

  let argmax_uint_compute
    (verbose:int)
    (wap_solver:wap_solver)
    (halt_bpp:(MLBDD.man -> MLBDD.t list -> 'res option))
    (support_consistency:bool)
    (man:MLBDD.man)
    (parameters:int list)
    (fl:Snowflake.MlbddUInt.puint list) : (MLBDD.t, 'res option) result =
    match fl with
    | [] -> Ok(MLBDD.dtrue man)
    | _ -> (
      let arity = ThisTools.argmax_uint_arity fl parameters in
      let t : Q.t = Q.newman() in
      let fl' : Avuint.puint list = fl ||> (lambda_of_mlbdd_puint t man arity) in
      let halt_bpp (t:Q.t) (fl:Q.f list) : _ option =
        halt_bpp man (fl ||> mlbdd_of_lambda t man)
      in
      Argmax_UInt.compute ~verbose ~wap_solver ~halt_bpp ~support_consistency t arity parameters fl'
      |> (
        function
        | Ok f -> Ok(mlbdd_of_lambda t man f)
        | Error err -> Error err
      )
    )

  let argmax_uint_select_compute
    (verbose:int)
    (wap_solver:wap_solver)
    (halt_bpp:(MLBDD.man -> MLBDD.t list -> 'res option))
    (support_consistency:bool)
    (man:MLBDD.man)
    (parameters:int list)
    (fl:Snowflake.MlbddUInt.puint list) :
      ((MLBDD.var * MLBDD.t)list, 'res option) result =
    match fl with
    | [] -> Ok []
    | _ -> (
      let arity = ThisTools.argmax_uint_arity fl parameters in
      let t : Q.t = Q.newman() in
      let fl' : Avuint.puint list = fl ||> (lambda_of_mlbdd_puint t man arity) in
      let halt_bpp (t:Q.t) (fl:Q.f list) : _ option =
        halt_bpp man (fl ||> mlbdd_of_lambda t man)
      in
      Argmax_UInt_Select.compute ~verbose ~wap_solver ~halt_bpp ~support_consistency t arity parameters fl'
      |> (
        function
        | Ok assoc -> Ok(assoc ||> (fun (v, f) -> (v, mlbdd_of_lambda t man f)))
        | Error err -> Error err
      )
    )
end

module Model_LDD_B_O_U =
struct
  module Q = Ldd_B_o_u_advanced.OOPS.Model
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Q)

  module LambdaOfMlbdd = Ldd_mlbdd.ComposeOops(Q)
  module MlbddOfLambda = Ldd_B_o_u.ComposeOops(LddMlbdd)

  let of_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:LddMlbdd.f) : Q.f =
    LambdaOfMlbdd.translate man t [f] |> List.hd

  let to_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:Q.f) : LddMlbdd.f =
    let t0 = Ldd_B_o_u_advanced.OOPS.get_man_ldd t in
    MlbddOfLambda.translate t0 man [f] |> List.hd
end

module LDD_B_O_U : Sig = Make(Model_LDD_B_O_U)

module Model_LDD_B_O_NU =
struct
  module Q = Ldd_B_o_nu_advanced.OOPS.Model
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Q)

  module LambdaOfMlbdd = Ldd_mlbdd.ComposeOops(Q)
  module MlbddOfLambda = Ldd_B_o_nu.ComposeOops(LddMlbdd)

  let of_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:LddMlbdd.f) : Q.f =
    LambdaOfMlbdd.translate man t [f] |> List.hd

  let to_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:Q.f) : LddMlbdd.f =
    let t0 = Ldd_B_o_nu_advanced.OOPS.get_man_ldd t in
    MlbddOfLambda.translate t0 man [f] |> List.hd
end

module LDD_B_O_NU : Sig = Make(Model_LDD_B_O_NU)

module Model_LDD_B_O_NUCX =
struct
  module Q = Ldd_B_o_nucx_advanced.OOPS.Model
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Q)

  (* module LambdaOfMlbdd = Ldd_mlbdd.ComposeOops(Q) *)
  module LambdaOfMlbdd = Ldd_mlbdd.TranslateAqeops(Q)
  (* module MlbddOfLambda = Ldd_B_o_nucx.ComposeOops(LddMlbdd) *)
  module MlbddOfLambda = Ldd_mlbdd.ImportAqeops(Q)

  let of_mlbdd_man : (Q.t * LddMlbdd.t * LambdaOfMlbdd.t) option ref = ref None

  let of_mlbdd_get (t0:Q.t) (t1:LddMlbdd.t) : LambdaOfMlbdd.t =
    match !of_mlbdd_man with
    | Some(t0', t1', man)
      when (if t0' == t0 && t1 == t1
        then true
        else (
          print_endline "[DAGaml.SnowStormModel.Model_LDD_B_U_NU] replaced 'of_mlbdd' manager";
          false
        )
      ) -> man
    | _ -> (
      let man = LambdaOfMlbdd.newman t1 t0 in
      of_mlbdd_man := Some(t0, t1, man);
      man
    )

  let of_mlbdd (t0:Q.t) (t1:LddMlbdd.t) (f:LddMlbdd.f) : Q.f =
    let man = of_mlbdd_get t0 t1 in
    LambdaOfMlbdd.compute man f

  let to_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:Q.f) : LddMlbdd.f =
    MlbddOfLambda.translate man t [f] |> List.hd
end

module LDD_B_O_NUCX : Sig = Make(Model_LDD_B_O_NUCX)

module Model_LDD_B_U_NU =
struct
  module Q = Ldd_B_u_nu_advanced.OOPS.Model
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Q)

  module LambdaOfMlbdd = Ldd_mlbdd.ComposeOops(Q)
  module MlbddOfLambda = Ldd_B_u_nu.ComposeOops(LddMlbdd)

  let of_mlbdd (t0:Q.t) (t1:LddMlbdd.t) (f:LddMlbdd.f) : Q.f =
    LambdaOfMlbdd.translate t1 t0 [f] |> List.hd

  let to_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:Q.f) : LddMlbdd.f =
    let t0 = Ldd_B_u_nu_advanced.OOPS.get_man_ldd t in
    MlbddOfLambda.translate t0 man [f] |> List.hd
end

module LDD_B_U_NU : Sig = Make(Model_LDD_B_U_NU)

module Model_LDD_B_U_NUCX =
struct
  module Q = Ldd_B_u_nucx_advanced.OOPS.Model
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Q)

  module LambdaOfMlbdd = Ldd_mlbdd.ComposeOops(Q)
  module MlbddOfLambda = Ldd_B_u_nucx.ComposeOops(LddMlbdd)

  let of_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:LddMlbdd.f) : Q.f =
    LambdaOfMlbdd.translate man t [f] |> List.hd

  let to_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:Q.f) : LddMlbdd.f =
    let t0 = Ldd_B_u_nucx_advanced.OOPS.get_man_ldd t in
    MlbddOfLambda.translate t0 man [f] |> List.hd
end

module LDD_B_U_NUCX : Sig = Make(Model_LDD_B_U_NUCX)

module Model_LDD_MLBDD =
struct
  module Q = Ldd_mlbdd.LDD.OOPS.Model
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Q)

  module LambdaOfMlbdd = Ldd_mlbdd.ComposeOops(Q)
  module MlbddOfLambda = Ldd_mlbdd.ComposeOops(LddMlbdd)

  let of_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:LddMlbdd.f) : Q.f =
    LambdaOfMlbdd.translate man t [f] |> List.hd

  let to_mlbdd (t:Q.t) (man:LddMlbdd.t) (f:Q.f) : LddMlbdd.f =
    MlbddOfLambda.translate t man [f] |> List.hd
end

module LDD_MLBDD : Sig = Make(Model_LDD_MLBDD)
