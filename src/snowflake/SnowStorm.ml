(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * SnowStorm : Snowflake boosted with DAGaml
 *)

type computing_model =
  | LDD_B_O_U
  | LDD_B_O_NU
  | LDD_B_O_NUCX
  | LDD_B_U_NU
  | LDD_B_U_NUCX
  | LDD_MLBDD

let default_computing_model : computing_model = LDD_B_U_NUCX

(*
let get_computing_module : computing_model -> (module X : SnowStormModel.Sig) =
  function
  | LDD_B_O_U    -> SnowStormModel.LDD_B_O_U
  | LDD_B_O_NU   -> SnowStormModel.LDD_B_O_NU
  | LDD_B_O_NUCX -> SnowStormModel.LDD_B_O_NUCX
  | LDD_B_U_NU   -> SnowStormModel.LDD_B_U_NU
  | LDD_B_U_NUCX -> SnowStormModel.LDD_B_U_NUCX
  | LDD_MLBDD    -> SnowStormModel.LDD_MLBDD
 *)

module AndL =
struct
  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Snowflake.Wap_exchange.input], is the WAP modelisation of this system
   *)
  let wap_of_system : MLBDD.man -> int list -> MLBDD.t list -> Snowflake.Wap_exchange.input =
    Snowflake.MlbddExample.AndL.wap_of_system

  let default_wap_solver : wap_solver =
    Snowflake.Wap_lightspeed.lightspeed_greedy

  (* [compute
   *      ~model:computing_model=default_computing_model
   *      ~verbose:intverbose=0
   *      ~wap_solver=default_wap_solver
   *       man param funlist = solved] where:
   *  - [model : computing_model], sets the model of LDD to use in the background
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  let compute ?(model=default_computing_model) ?(verbose=0) ?(wap_solver=default_wap_solver) ?(support_consistency=true) (man:MLBDD.man)
    (parameters:int list) (fl:MLBDD.t list) : MLBDD.t =
    match model with
    | LDD_B_O_U    -> SnowStormModel.LDD_B_O_U   .andl_compute verbose wap_solver support_consistency man parameters fl
    | LDD_B_O_NU   -> SnowStormModel.LDD_B_O_NU  .andl_compute verbose wap_solver support_consistency man parameters fl
    | LDD_B_O_NUCX -> SnowStormModel.LDD_B_O_NUCX.andl_compute verbose wap_solver support_consistency man parameters fl
    | LDD_B_U_NU   -> SnowStormModel.LDD_B_U_NU  .andl_compute verbose wap_solver support_consistency man parameters fl
    | LDD_B_U_NUCX -> SnowStormModel.LDD_B_U_NUCX.andl_compute verbose wap_solver support_consistency man parameters fl
    | LDD_MLBDD    -> SnowStormModel.LDD_MLBDD   .andl_compute verbose wap_solver support_consistency man parameters fl
end

module Argmax_UInt =
struct
  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Snowflake.Wap_exchange.input], is the WAP modelisation of this system
   *)
  let wap_of_system : MLBDD.man -> int list -> Snowflake.MlbddUInt.puint list -> Snowflake.Wap_exchange.input =
    Snowflake.MlbddExample.Argmax_UInt.wap_of_system

  let default_wap_solver : wap_solver =
    Snowflake.Wap_lightspeed.lightspeed_greedy

  (* [compute ?verbose:intverbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  let compute ?(model=default_computing_model) ?(verbose=0) ?(wap_solver=default_wap_solver)
    ?(halt_bpp=(fun _ _ -> None)) ?(support_consistency=true) (man:MLBDD.man)
    (parameters:int list) (fl:Snowflake.MlbddUInt.puint list) : (MLBDD.t, 'res option) result =
    match model with
    | LDD_B_O_U    -> SnowStormModel.LDD_B_O_U   .argmax_uint_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_O_NU   -> SnowStormModel.LDD_B_O_NU  .argmax_uint_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_O_NUCX -> SnowStormModel.LDD_B_O_NUCX.argmax_uint_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_U_NU   -> SnowStormModel.LDD_B_U_NU  .argmax_uint_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_U_NUCX -> SnowStormModel.LDD_B_U_NUCX.argmax_uint_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_MLBDD    -> SnowStormModel.LDD_MLBDD   .argmax_uint_compute verbose wap_solver halt_bpp support_consistency man parameters fl
end

module Argmax_UInt_Select =
struct
  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Snowflake.Wap_exchange.input], is the WAP modelisation of this system
   *)
  let wap_of_system : MLBDD.man -> int list -> Snowflake.MlbddUInt.puint list -> Snowflake.Wap_exchange.input =
    Snowflake.MlbddExample.Argmax_UInt.wap_of_system

  let default_wap_solver : wap_solver =
    Snowflake.Wap_lightspeed.lightspeed_greedy

  (* [compute ~verbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  let compute ?(model=default_computing_model) ?(verbose=0) ?(wap_solver=default_wap_solver)
    ?(halt_bpp=(fun _ _ -> None)) ?(support_consistency=true) (man:MLBDD.man)
    (parameters:int list) (fl:Snowflake.MlbddUInt.puint list) : ((MLBDD.var * MLBDD.t)list, 'res option) result =
    match model with
    | LDD_B_O_U    -> SnowStormModel.LDD_B_O_U   .argmax_uint_select_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_O_NU   -> SnowStormModel.LDD_B_O_NU  .argmax_uint_select_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_O_NUCX -> SnowStormModel.LDD_B_O_NUCX.argmax_uint_select_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_U_NU   -> SnowStormModel.LDD_B_U_NU  .argmax_uint_select_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_B_U_NUCX -> SnowStormModel.LDD_B_U_NUCX.argmax_uint_select_compute verbose wap_solver halt_bpp support_consistency man parameters fl
    | LDD_MLBDD    -> SnowStormModel.LDD_MLBDD   .argmax_uint_select_compute verbose wap_solver halt_bpp support_consistency man parameters fl
end
