(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * MlbddUInt : Unsigned Integers (a.k.a., natural numbers) encoded as a list of BDD using little-endian encoding
 * remark : this file was mostly imported from DAGaml's oops/vUInt
 *)

open GuaCaml
open MlbddUtils

val fulladd2 : f -> f -> f * f
val fulladd3 : f -> f -> f -> f * f

type uint = f array

val uint_get : t -> ?default:f -> int -> uint -> f
val uint_of_bool : f -> uint
val uint_of_int : t -> int -> uint

val uint_one : t -> uint

val add : t -> ?carry:f -> uint -> uint -> uint

val ( +/ ) : t -> uint -> uint -> uint

val array_add : t -> uint array -> uint
val  list_add : t -> uint list  -> uint

val ( +?/ ) : t -> uint -> int -> uint
val ( ?+/ ) : t -> int -> uint -> uint

(* decreasing shift *)
val shift_right : t -> int -> uint -> uint
val ( >>/ )     : t -> uint -> int -> uint

(* increasing shift *)
val shift_left  : t -> int -> uint -> uint
val ( <</ )     : t -> uint -> int -> uint

val bitwise_binop :
  (f -> f -> f) -> t -> ?defaultX:f -> ?defaultY:f ->
    uint -> uint -> uint

val ( |&/ ) : t -> uint -> uint -> uint
val ( |^/ ) : t -> uint -> uint -> uint
val ( ||/ ) : t -> uint -> uint -> uint
val ( |=/ ) : t -> uint -> uint -> uint

val bitwise_choice : t -> f -> uint -> uint -> uint

val zero_choice : t -> f -> uint -> uint

val shift_right' : t -> uint -> uint -> uint
val ( >>// )     : t -> uint -> uint -> uint

val shift_left'  : t -> uint -> uint -> uint
val ( <<// )     : t -> uint -> uint -> uint

val card : t -> f array -> uint

(* [exp_card t bXa =  (>>//) t (uint_one t ) (card t bXa)] *)
(* [exp_card t a = r] where:
 * - [a : bool^(supp) -> bool] (where [supp := support(a)])
 * - [r : bool^(supp) -> uint]
 *)
val exp_card : t -> f array -> uint

val scalar_binop_left  : (f -> f -> f) -> f -> uint -> uint
val scalar_binop_right : (f -> f -> f) -> uint -> f -> uint

val ( |.&/ ) : f -> uint -> uint
val ( |.^/ ) : f -> uint -> uint
val ( |.|/ ) : f -> uint -> uint
val ( |.=/ ) : f -> uint -> uint

val ( |&./ ) : uint -> f -> uint
val ( |^./ ) : uint -> f -> uint
val ( ||./ ) : uint -> f -> uint
val ( |=./ ) : uint -> f -> uint

val lX : bool -> t -> uint -> uint -> f
val gX : bool -> t -> uint -> uint -> f

val lt      : t -> uint -> uint -> f
val ( </ )  : t -> uint -> uint -> f

val le      : t -> uint -> uint -> f
val ( <=/ ) : t -> uint -> uint -> f

val gt      : t -> uint -> uint -> f
val ( >/ )  : t -> uint -> uint -> f

val ge      : t -> uint -> uint -> f
val ( >=/ ) : t -> uint -> uint -> f

val argmax : f -> MLBDD.support -> uint -> f * uint

val equal_uint : uint -> uint -> bool

type puint = f * uint
  (* partial function [B^n -> N] *)

module P :
sig
  val support : puint -> MLBDD.support
  val sorted_support : puint -> supp
  val zero : t -> puint
  val of_guard : f -> puint
  val of_uint : t -> uint -> puint
  val ( +/ ) : t -> puint -> puint -> puint
  val addl : t -> puint list -> puint
  val coproj_proj : supp -> puint -> f * puint
  val is_trivial : puint -> bool option
  val equal : puint -> puint -> bool
end
