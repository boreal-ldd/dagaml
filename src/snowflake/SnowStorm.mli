(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * SnowStorm : Snowflake boosted with DAGaml
 *)

type computing_model =
  | LDD_B_O_U
  | LDD_B_O_NU
  | LDD_B_O_NUCX
  | LDD_B_U_NU
  | LDD_B_U_NUCX
  | LDD_MLBDD

val default_computing_model : computing_model (* = LDD_B_U_NUCX *)

module AndL :
sig
  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Snowflake.Wap_exchange.input], is the WAP modelisation of this system
   *)
  val wap_of_system : MLBDD.man -> int list -> MLBDD.t list -> Snowflake.Wap_exchange.input

  (* let default_wap_solver : wap_solver = Wap_greedy.lightweight_greedy *)
  val default_wap_solver : wap_solver

  (* [compute
   *      ~model:computing_model=default_computing_model
   *      ~verbose:intverbose=0
   *      ~wap_solver=default_wap_solver
   *       man param funlist = solved] where:
   *  - [model : computing_model], sets the model of LDD to use in the background
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val compute :
    ?model:computing_model ->
    ?verbose:int ->
    ?wap_solver:wap_solver ->
    ?support_consistency:bool ->
    MLBDD.man ->
    int list ->
    MLBDD.t list ->
      MLBDD.t
end

module Argmax_UInt :
sig
  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Snowflake.Wap_exchange.input], is the WAP modelisation of this system
   *)
  val wap_of_system : MLBDD.man -> int list -> Snowflake.MlbddUInt.puint list -> Snowflake.Wap_exchange.input

  (* let default_wap_solver : wap_solver = Wap_greedy.lightweight_greedy *)
  val default_wap_solver : wap_solver

  (* [compute ?verbose:intverbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val compute :
    ?model:computing_model ->
    ?verbose:int ->
    ?wap_solver:wap_solver ->
    ?halt_bpp:(MLBDD.man -> MLBDD.t list -> 'res option) ->
    ?support_consistency:bool ->
     MLBDD.man ->
     int list ->
     Snowflake.MlbddUInt.puint list ->
      (MLBDD.t, 'res option) result
end

module Argmax_UInt_Select :
sig
  type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

  (* [wap_of_system man param funlist = wap_input] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [wap_input : Snowflake.Wap_exchange.input], is the WAP modelisation of this system
   *)
  val wap_of_system : MLBDD.man -> int list -> Snowflake.MlbddUInt.puint list -> Snowflake.Wap_exchange.input

  (* let default_wap_solver : wap_solver = Wap_greedy.lightweight_greedy *)
  val default_wap_solver : wap_solver

  (* [compute ?verbose:intverbose=0 ~wap_solver=default_wap_solver man param funlist = solved] where:
   *  - [man : MLBDD.man], is the MLBDD manager of the function in [funlist]
   *  - [param : int list], is the set of mode variables sorted by increasing order
   *  - [funlist : MLBDD.t list], is the set of Boolean Function which are to be conjuncted
   *  - [solved : MLBDD.t], is the conjunction of the Boolean Functions in [funlist]
   *
   * === NOTE ===
   * verbose = 0 -> no verbosity (except on errors) [DEFAULT]
   * verbose = 1 -> display time profiling
   * verbose = 2 -> display WAP decomposition
   *)
  val compute :
    ?model:computing_model ->
    ?verbose:int ->
    ?wap_solver:wap_solver ->
    ?halt_bpp:(MLBDD.man -> MLBDD.t list -> 'res option) ->
    ?support_consistency:bool ->
     MLBDD.man ->
     int list ->
     Snowflake.MlbddUInt.puint list ->
      ((MLBDD.var * MLBDD.t)list, 'res option) result
end
