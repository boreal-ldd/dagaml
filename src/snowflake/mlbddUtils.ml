(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * MlbddUtils : MLBDD toolbox
 *)

open GuaCaml
open Extra
open STools

(* Interface to DAGaml's OOPS *)

module OOPS =
struct
  type t = MLBDD.man
  type f = {
    arity : int;
    bdd   : MLBDD.t;
  }

  let arity _ f = f.arity

  (*  val ( ->> ) : t -> bool list -> f -> f *)
  let cneg (t:t) (b:bool) (f:f) : f =
    if b then {f with bdd = MLBDD.dnot f.bdd} else f
  let ( &! ) t f1 f2 =
    assert(f1.arity = f2.arity);
    {f1 with bdd = MLBDD.dand f1.bdd f2.bdd}
  let ( |! ) t f1 f2 =
    assert(f1.arity = f2.arity);
    {f1 with bdd = MLBDD.dor  f1.bdd f2.bdd}
  let ( ^! ) t f1 f2 =
    assert(f1.arity = f2.arity);
    {f1 with bdd = MLBDD.xor  f1.bdd f2.bdd}
  let ( =! ) t f1 f2 =
    assert(f1.arity = f2.arity);
    {f1 with bdd = MLBDD.eq   f1.bdd f2.bdd}
  let ( *! ) t f0 f1 =
    assert(f0.arity = f1.arity);
    let f0' = MLBDD.permutef succ f0.bdd in
    let f1' = MLBDD.permutef succ f1.bdd in
    {arity = succ f0.arity; bdd = MLBDD.ite f0' 0 f1'}

  let cst t b arity =
    assert(0 <= arity);
    {arity; bdd = if b then MLBDD.dtrue t else MLBDD.dfalse t}
  let var t b k arity =
    assert(0 <= k && k < arity);
    cneg t b {arity; bdd = MLBDD.ithvar t k}

  (*
   * [to_bool f] returns
   * - [Some true ] if [f] is the constant true
   * - [Some false] if [f] is the constant false
   * - [None] if [f] is not a constant
   *)
  let to_bool _ f : bool option =
    match MLBDD.inspect f.bdd with
    | MLBDD.False -> Some false
    | MLBDD.True  -> Some true
    | _ -> None
end

type t = MLBDD.man
type f = MLBDD.t

type supp = MLBDD.var list (* MLBDD.var = int *)

let cst t b = if b then MLBDD.dtrue t else MLBDD.dfalse t

let ( ~! ) = MLBDD.dnot
let cneg b f = if b then ~! f else f
let ( &! ) = MLBDD.dand
let ( |! ) = MLBDD.dor
let ( ^! ) = MLBDD.xor
let ( =! ) = MLBDD.eq

let var t b k = cneg b (MLBDD.ithvar t k)

let is_trivial (f:MLBDD.t) : bool option =
  match MLBDD.inspect f with
  | MLBDD.False -> Some false
  | MLBDD.True  -> Some true
  | _ -> None

(* [sorted_support f = il] where :
 * - [il] is the indexes of the variables in the support of [f]
 * - [il] is sorted in increasing order (compatible with GuaCaml.SetList)
 *)
let sorted_support (f:MLBDD.t) : int list =
  f |> MLBDD.support |> MLBDD.list_of_support |> SetList.sort

(*
 * [lexand man fl = f]
 * where [f] is the BDD obtained by conjuncting the functions in the list [fl]
 * [lexand] firsts sorts the term according to the lexicographic order on their support,
 *   then conjunct them backward
 *)
let lexand (man:MLBDD.man) (l:MLBDD.t list) : MLBDD.t =
  let annot = MyList.map (fun f -> (sorted_support f, f)) l in
  let sorted = List.sort (fun x y -> Stdlib.compare(fst x)(fst y)) annot in
  List.fold_left ( &! ) (MLBDD.dtrue man) (List.rev_map snd sorted)

(* @isamdae *)
(* val is_not_false : MLBDD.t -> bool *)

let is_not_false e = not (MLBDD.is_false e)

(* @isamdae *)
(* val implies : MLBDD.t -> MLBDD.t -> bool *)

let implies a b = MLBDD.is_true (MLBDD.imply a b)

(* BDD operation on lists *)

(* val ite_list : MLBDD.t list -> MLBDD.var -> MLBDD.t list -> MLBDD.t list *)

let ite_list f v t =
  assert(List.length f = List.length t);
  MyList.map2 (fun l r -> MLBDD.ite l v r) f t

(* BDD operations on arrays *)

(* @isamdae *)
(* val ite_array : MLBDD.t array -> MLBDD.var -> MLBDD.t array -> MLBDD.t array *)

let ite_array f v t = Array.map2 (fun l r -> MLBDD.ite l v r) f t

(* Evaluation of a BDD from the sorted association list (assoc:(MLBDD.var -> bool)) where:
 *   - dom(assoc) supersets support(phi)
 *)
(* val eval : MLBDD.t -> (MLBDD.var * bool) list -> bool *)

let eval (phi:MLBDD.t) (subst:(MLBDD.var * bool)list) : bool =
  let memo, apply = MemoTable.(make default_size) in
  let rec eval_rec phi subst =
    match subst with
    | [] -> (
      match is_trivial phi with
      | Some cst -> cst
      | None     -> failwith "[MlbddUtils.eval] total evaluation required"
    )
    | (v0, c0)::subst' ->
    match MLBDD.inspect phi with
      | MLBDD.False -> false
      | MLBDD.True -> true
      | MLBDD.If (phi0, var0, phi1) ->
             if v0 < var0
        then eval_mem phi subst' (* unstack one evalution and retry *)
                                 (* [TODO] make an auxiliary function which unstack untill it works *)
        else (
          if v0 <> var0 then failwith ("[MbddUtils.eval] the variable '"^(ToS.int var0)^"' is not set by: "^(ToS.(list(int * bool)) subst));
          if c0
          then eval_mem phi1 subst'
          else eval_mem phi0 subst'
        )
      | MLBDD.Not phi0 -> not (eval_mem phi0 subst)
  and eval_mem phi subst = apply
      (fun _ -> eval_rec phi subst)
      (MLBDD.id phi, subst) in
  eval_rec phi subst

(* Partial Evaluation of a BDD using the sorted association list (assoc:(MLBDD.var -> bool)) where:
 *)
(* partial eval from ordered lists of both BDD variables and their valuation,
 * using the memoization procedure by Joan Thibault *)
(* val peval : MLBDD.t -> (MLBDD.var list -> bool list -> MLBDD.t *)

let peval (phi:MLBDD.t) (subst:(MLBDD.var * bool)list) : MLBDD.t =
  let man = MLBDD.manager phi in
  let memo, apply = MemoTable.(make default_size) in
  let rec peval_rec phi subst =
    match subst with
    | [] -> phi
    | (v0, c0)::subst' ->
    match MLBDD.inspect phi with
      | MLBDD.False -> MLBDD.dfalse man
      | MLBDD.True -> MLBDD.dtrue man
      | MLBDD.If (phi0, var0, phi1) ->
             if v0 < var0
        then peval_mem phi subst' (* unstack one evalution and retry *)
                                  (* [TODO] make an auxiliary function which unstacks untill it works *)
        else if v0 = var0
        then (
          if c0
          then peval_mem phi1 subst'
          else peval_mem phi0 subst'
        )
        else (
          let phi0' = peval_mem phi0 subst
          and phi1' = peval_mem phi1 subst in
          MLBDD.ite phi0' var0 phi1'
        )
      | MLBDD.Not phi0 -> ~! (peval_mem phi0 subst)
  and peval_mem phi subst = apply
      (fun _ -> peval_rec phi subst)
      (MLBDD.id phi, subst) in
  peval_rec phi subst

(* similar to [MLBDD.ite f0 fc f1] excepts that fc is a function and not a variable
 *)

let fite (f0_:MLBDD.t) (fc_:MLBDD.t) (f1_:MLBDD.t) : MLBDD.t =
  (~! fc_ &! f0_) |! (fc_ &! f1_)

(* variable substitution by functions (using an association list from variables [MLBDD.var] to functions [MLBDD.t])
 * [subst phi [(x1, psi1); (x2, psi2); ...] = phi[x1:=psi1, x2:=psi2, ...]
 * the semantic of this operation is easily defined iff supp(phi_i) \inter (x1, ..., xk) is empty
 * otherwise one must understand that all substitutions are performed on the input formula
 * e.g. [subst  {x1 /\ x2} [(x1, {x2}); (x2, {\lnot x1}) = {x2 /\ \lnot x1}] ( = {(\lnot x1) /\ x2} )
 * In practice, this result is obtained by completely exploring the formula before to rebuilt it in one go.
 *)
(* val subst : MLBDD.t -> (MLBDD.var * MLBDD.t) list -> MLBDD.t *)

let subst (phi:MLBDD.t) (subst:(MLBDD.var * MLBDD.t)list) : MLBDD.t =
  let man = MLBDD.manager phi in
  let memo, apply = MemoTable.(make default_size) in
  let rec subst_rec phi subst =
    match subst with
    | [] -> phi
    | (v0, psi0)::subst' ->
    match MLBDD.inspect phi with
      | MLBDD.False -> MLBDD.dfalse man
      | MLBDD.True -> MLBDD.dtrue man
      | MLBDD.If (phi0, var0, phi1) ->
             if v0 < var0
        then subst_mem phi subst' (* unstack one substitution and retry *)
        else if var0 = v0
        then (
          match is_trivial psi0 with
          | Some false -> subst_mem phi0 subst'
          | Some true  -> subst_mem phi1 subst'
          | None -> (
            let phi0' = subst_mem phi0 subst'
            and phi1' = subst_mem phi1 subst' in
            fite phi0' psi0 phi1'
          )
        )
        else (
          let phi0' = subst_mem phi0 subst
          and phi1' = subst_mem phi1 subst in
          MLBDD.ite phi0' var0 phi1'
        )
      | MLBDD.Not phi0 -> MLBDD.dnot (subst_mem phi0 subst)
  and subst_mem phi subst = apply
      (fun _ -> subst_rec phi subst)
      (MLBDD.id phi, subst) in
  subst_rec phi subst

(* val sat_select : MLBDD.t -> int list -> MLBDD.t * MLBDD.t
 * [sat_select f suppB = g * f'] where :
 * - [f  : bool^(suppA) -> bool^(suppB) -> bool] (where suppA = support(f) - suppB)
 * - [f' : bool^(suppA) -> bool^(suppB) -> bool]
 * - [g  : bool^(suppA)                 -> bool]
 * such that :
 * - [suppB is SetList.sorted]
 * - [f' => f]
 * - [\forall (a:suppA). (\exists! (b:suppB) f'(a, b)) \/ f(a, .) = false_{suppB}]
 * - [\exists(suppB) f' = \exists(suppB) f]
 *   + i.e. [\forall (a:suppA). (\exists (b:suppB) f'(a, b)) <=> (\exists (b:suppB) f(a, b))]
 * - [\forall (a:suppA). (\forall (b1 b2:suppB) f'(a, b1) = true -> f'(a, b2) = true -> b1 = b2)]
 * - [g = \exists(suppB) f]
 * === In Other Words ===
 * We define [suppA := support(f) - suppB] the set of parameter variables.
 * For any valuation of parameters variables (a:suppA), we denote by
 * [f(a, .) := peval f (List.combine suppA a)] the subfunction of f[suppA:=a].
 * Either :
 * (1) [is_false f(a, .) = true ] (i.e., [f(a. .)] is unsatisfiable, i.e., [f(a, .)] has no solution)
 *   Then, [g(a) = False] and [f'(a, .) = False], or,
 * (2) [is_false f(a, .) = false] (i.e., [f(a, .)] is satisfiable, i.e. [f(a, .)] has at least one solution)
 *   Then, [g(a) = True ] and [f'(a, .)] has exacly one solution (b:suppB) such that [f(a, b) = True]
 *   (i.e. [b] is a solution of [f(a, .)])
 *)

(* val sat_select : MLBDD.t -> MLBDD.support -> MLBDD.t * MLBDD.t *)

let sat_select phi supp =
  if not (SetList.sorted supp)
  then invalid_arg "[Snowflake.MlbddUtils.sat_select] [SetList.sorted supp = false]";
  let man = MLBDD.manager phi in
  (* main procedure: inductive approach *)
  let memo, apply = MemoTable.(make default_size) in
  let rec select_rec phi supp =
    match supp with
    | [] -> (phi, phi) (* all remaining variables are parameter variables *)
    | v0::supp' -> (
      match MLBDD.inspectb phi with
      (* case 0 : no solution in this branch *)
      | MLBDD.BFalse -> (MLBDD.dfalse man, MLBDD.dfalse man)
      (* case 1 : choose the solution in which the remaining edges do not appear;
       * arbitrary choice to keep only one matching *)
      | MLBDD.BTrue  -> (
        let sol = List.fold_left
          (fun sol v -> sol &! (var man true v))
          (MLBDD.dtrue man)
          supp
        in
        (MLBDD.dtrue man, sol)
      )
      | MLBDD.BIf (phi0, var0, phi1) -> (
             if v0 < var0
        then (
          (* [v0] is a useless and non-parameter variable *)
          let (g', phi') = select_mem phi supp' in
          (g', phi' &! (var man true v0))
        )
        else if v0 = var0
        then (
          (* [v0] is a non-useless and non-parameter variable *)
          let (g'0, phi'0) = select_mem phi0 supp' in
          let (g'1, phi'1) = select_mem phi1 supp' in
          (* phi' := var0 ? phi'1 /\ ~g'0 * : var0 *)
          let phi' = MLBDD.ite phi'0 var0 ((~! g'0) &! phi'1) in
          (g'0 |! g'1, phi')
        )
        else (
          (* [var0] is a non-useless parameter variable *)
          let (g'0, phi'0) = select_mem phi0 supp in
          let (g'1, phi'1) = select_mem phi1 supp in
          let phi' = MLBDD.ite phi'0 var0 phi'1 in
          let g' = MLBDD.ite g'0 var0 g'1 in
          (g', phi')
        )
      )
    )
  and select_mem phi supp =
    apply (fun _ -> select_rec phi supp) (MLBDD.id phi, supp)
  in
  select_rec phi supp

(* val exist_proj : MLBDD.t -> int list -> MLBDD.t * (MLBDD.var * MLBDD.t) list
 * [exists_proj f suppB = g * (assoc[(v, p_v)])] where :
 * - [f  : bool^(suppA) -> bool^(suppB) -> bool] (where suppA = support(f) - suppB)
 * - [g  : bool^(suppA)                 -> bool]
 * such that :
 * - [suppB is SetList.sorted]
 * - [g = \exists(suppB) f]
 * - [assoc is Assoc.sorted]
 * - [dom(assoc) = suppB]
 * - [\forall v\in suppB, p_v : bool^(suppA) -> bool]
 *   + i.e., [[assoc] : suppB -> bool^(suppA) -> bool)]
 * - [\forall v\in suppB, p_v := \exists(suppB - {v}) phi)[v:=1]]
 * === In Other Words ===
 * [exists_proj man phi suppB = (guard, assoc)] takes as input a BDD
 * [phi : bool^suppA -> bool^suppB -> bool] (where suppA := support(f) - suppB)
 * and a sorted list of variables [suppB].
 * It returns [g = \exists(suppB) phi : bool^(suppA) -> bool] the set of modes
 * for which there exists a solution and [assoc:suppB -> (bool^(suppA) -> bool)]
 * a sorted association list which associate to each variable [v] in [suppB] a
 * BDD [p_v := (\exists(suppB - {v}) phi)[v:=1] : bool^(suppA) -> bool].
 *)

let exists_proj (phi:MLBDD.t) (supp:int list) : MLBDD.t * (MLBDD.var * MLBDD.t) list =
  if not (SetList.sorted supp)
  then invalid_arg "[Snowflake.MlbddUtils.exists_proj] [SetList.sorted supp = false]";
  let man = MLBDD.manager phi in
  let ff = MLBDD.dfalse man in
  let tt = MLBDD.dtrue  man in
  let memo, apply = MemoTable.(make default_size) in
  let rec eproj_rec phi supp : MLBDD.t * (MLBDD.t list) =
    match supp with
    | [] -> (phi, []) (* all remaining variables are parameter variables *)
    | v0::supp' -> (
      match MLBDD.inspectb phi with
      | MLBDD.BFalse -> (ff, List.rev_map (fun _ -> ff) supp)
      | MLBDD.BTrue  -> (tt, List.rev_map (fun _ -> ff) supp)
      | MLBDD.BIf(phi0, var0, phi1) -> (
             if v0 < var0
        then (
          (* [v0] is a useless and non-parameter variable *)
          let (g', a') = eproj_mem phi supp' in
          (g', (g'::a'))
        )
        else if v0 = var0
        then (
          (* [v0] is a non-useless and non-parameter variable *)
          let (g'0, a'0) = eproj_mem phi0 supp' in
          let (g'1, a'1) = eproj_mem phi1 supp' in
          let g' = g'0 |! g'1 in
          let a' = g'1 :: (MyList.map2 ( |! ) a'0 a'1) in
          (g', a')
        )
        else (
          (* [var0] is a non-useless parameter variable *)
          let (g'0, a'0) = eproj_mem phi0 supp in
          let (g'1, a'1) = eproj_mem phi1 supp in
          let g' = MLBDD.ite g'0 var0 g'1 in
          let a' = ite_list  a'0 var0 a'1 in
          (g', a')
        )
      )
    )
  and eproj_mem phi supp =
    apply (fun _ -> eproj_rec phi supp) (MLBDD.id phi, supp)
  in
  let (g, a) = eproj_rec phi supp in
  (g, List.combine supp a)

let argmax (guard:MLBDD.t) (supp:MLBDD.support) (phi:MLBDD.t) : MLBDD.t * MLBDD.t =
  let max' = MLBDD.exists supp (( &! ) guard phi) in
  let guard' = ( &! ) guard (( =! ) phi max') in
  (max', guard')

(* val lexmax_rev : MLBDD.t -> MLBDD.support -> MLBDD.t array -> MLBDD.t * (MLBDD.t array) *)

let lexmax_rev guard supp lex : MLBDD.t * (MLBDD.t array) =
  let man = MLBDD.manager guard in
  let len = Array.length lex in
  let res = Array.make len (MLBDD.dfalse man) in
  let rec argmax_rec res g lex i =
    if i < 0 then (g, res) else (
      let (r_i, g') = argmax g supp lex.(i) in
      res.(i) <- r_i;
      argmax_rec res g' lex (pred i)
    )
  in
  argmax_rec res guard lex (pred len)

(* val lexmax : MLBDD.t -> MLBDD.support -> MLBDD.t array -> MLBDD.t * (MLBDD.t array) *)
let lexmax guard supp lex =
  let guard', max' = lexmax_rev guard supp (MyArray.rev lex) in
  (guard', MyArray.rev max')

type psubst = f * (MLBDD.var * f) list

module PSubst =
struct
  let sorted_domain ((inv, fl):psubst) : supp =
    let sfi = sorted_support inv in
    let sfl = fl ||> snd ||> sorted_support |> SetList.union_list in
    SetList.(union sfi sfl)
  let domain (xs:psubst) : MLBDD.support =
    MLBDD.support_of_list(sorted_domain xs)
  let sorted_codomain ((inv, fl):psubst) : supp =
    SetList.sort (fl ||> fst)
  let codomain (xs:psubst) : MLBDD.support =
    MLBDD.support_of_list(sorted_codomain xs)
  let sorted_support (xs:psubst) : supp =
    let dom = sorted_domain xs in
    let codom = sorted_codomain xs in
    SetList.union dom codom
  let support (xs:psubst) : MLBDD.support =
    MLBDD.support_of_list(sorted_support xs)

  let subst (f:f) ((inv, fl):psubst) : f =
    subst (MLBDD.dand f inv) fl

  let of_guard (g:f) (elim:supp) : psubst =
    assert(SetList.sorted elim);
    let inv1, g'   = sat_select  g  elim in
    let inv2, proj = exists_proj g' elim in
    assert(MLBDD.equal inv1 inv2);
    (inv1, proj)

  let is_trivial ((inv, _):psubst) : bool option =
    is_trivial inv

  let equal ((i1, fl1):psubst) ((i2, fl2):psubst) : bool =
    MLBDD.equal i1 i2 &&
    List.length fl1 = List.length fl2 &&
    List.for_all2 (fun (v1, f1) (v2, f2) -> v1 = v2 && MLBDD.equal f1 f2) fl1 fl2
end
