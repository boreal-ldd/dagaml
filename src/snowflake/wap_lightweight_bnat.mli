(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_lightweight :
 *   Simpl heuristic for WAP, which iteratively select a vertex with lightest neighborhood then
 *   apply H-reduction (true-twin merging).
*)

open GuaCaml
open BTools
open GraphHFT.Type

val opmin : (BNat.nat * 'a)option -> (BNat.nat*'a) -> (BNat.nat * 'a) option
val find_best : hg -> (BNat.nat * int) option
val lightweight_find_lopt : hg -> BNat.nat * int list list
val lightweight_greedy : ?verbose:bool -> Wap_exchange.input -> Wap_exchange.output
