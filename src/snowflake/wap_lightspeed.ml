(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_lightspeed :
 *   Simple Heuristic for WAP, similar to Wap_lightweight but lazily Simpl heuristic for WAP, which iteratively select a vertex with lightest neighbors then
 *   apply H-reduction (true-twin merging).
*)

open GuaCaml
open Extra
open STools
open BTools
open GraphHFT.Type
open GraphGenLA.Type

let prefix = "Snowflake.Wap_lightspeed"

let verbose = false

type v = {
          index  : int;
  mutable weight : int; (* weight of this vertex *)
  mutable neighb : int; (* weight of its neighbors *)
  mutable names  : int list; (* sorted list of initial names *)
  mutable edges  : int list; (* sorted list of indices of this vertex neighbors *)
                             (* dangling edges are lazily removed *)
          alive  : bool; (* alive = false iff vertex is fixed-terminal *)
}

type g = {
  (* an array of vertex *)
          vertices : v option array;
  (* one bag for each value of neighbors weight
   * these bags only contain alive vertices
   *)
          buckets  : int list array;
  mutable lightest : int;
}

module ToS =
struct
  include ToS
  let v (v:v) : string =
    let l = [
      "index", int v.index;
      "weight", int v.weight;
      "neighb", int v.neighb;
      "names", list int v.names;
      "edges", list int v.edges;
      "alive", bool v.alive
    ] in
    "{"^(SUtils.catmap "; " (fun (k, v) -> k^" = "^v) l)^"}"

  let g (g:g) : string =
    let l = [
      "vertices", array(option v) g.vertices;
      "buckets", array(list int) g.buckets;
      "lightest", int g.lightest
    ] in
    "{"^(SUtils.catmap "; " (fun (k, v) -> k^" = "^v) l)^"}"
end

let get g j = g.vertices.(j)
let set g j v = g.vertices.(j) <- v

let assert_lightest (g:g) : unit =
  for i = 0 to g.lightest -1
  do
    assert(g.buckets.(i) = []);
  done;
  ()

let compute_neighb (g:g) (v:v) : int =
  assert(SetList.sorted v.edges);
  assert(List.mem v.index v.edges = false);
  List.fold_left (fun sw j ->
    match get g j with
    | Some vj -> sw+vj.weight
    | None    -> sw) v.weight v.edges

let vertex_check_neighb (g:g) (v:v) : bool =
  v.neighb = compute_neighb g v

let graph_check_neighb (g:g) : bool =
  Array.for_all (function
    | Some vi -> vertex_check_neighb g vi
    | None    -> true) g.vertices

(* returns true if the vertex still exists *)
let normalize_vertex (g:g) (i:int) : bool =
  let opmap_fun j =
    if get g j = None then None else Some j
  in
  match (get g i) with
  | Some vi -> (
    vi.edges <- MyList.opmap opmap_fun vi.edges;
    true
  )
  | None    -> false

let merge (g:g) (i:int) (j:int) : unit =
  assert(i < j);
  match get g i, get g j with
  | Some vi, Some vj -> (
    assert(vi.alive  = vj.alive);
    assert(vi.neighb = vj.neighb);
    assert(SetList.union vi.edges [vi.index] = SetList.union vj.edges [vj.index]); (* [DEBUG] *)
    set g j None;
    vi.weight <- vi.weight + vj.weight;
    vi.names  <- SetList.union vi.names vj.names;
  )
  | _, _ -> assert false

let normalize_bucket (g:g) (nw:int) (bk:int list) : int list =
  let opmap1 i =
    if normalize_vertex g i (* we do not consider suppressed nodes *)
    then (
      let vi = Tools.unop (get g i) in
      (* assert(vertex_check_neighb g vi); *)
      if vi.neighb = nw (* we do not consider nodes with innacurate weight *)
      then Some(SetList.union vi.edges [vi.index], i)
      else (
        if verbose then print_endline ("[normalize_bucket] {v:"^(ToS.int i)^"} has been discard from current bucket as was not correctly weighted");
        None
      )
    )
    else (
      if verbose then print_endline ("[normalize_bucket] {v:"^(ToS.int i)^"} has been discard from current bucket as it no longer exists");
      None
    )
  in
  let foldmap1 ((ei, i), c) (ej, j) =
    if ei = ej
    then (
      assert(i < j);
      merge g i j;
      ((ei, i), c)
    )
    else ((ej, j), (j::c))
  in
  let annot = bk
    |> SetList.sort
    |> MyList.opmap opmap1
    |> List.sort Stdlib.compare
  in
  if verbose then print_endline ("[normalize_bucket] annot:"^(ToS.(list((list int) * int)) annot));
  match annot with
  | [] -> []
  | (ei, i)::annot' -> (
    List.fold_left foldmap1 ((ei, i), [i]) annot'
    |> snd |> List.rev
    |> SetList.sort
  )

(* retuns the weight of vj after elimination of v *)
let vertex_add_clique (g:g) (vi:v) (vj:v) : int =
  (* assert(List.mem vj.index vj.edges = false); *)
  let edges, wl =
    SetList.minus vi.edges [vj.index]
    |> SetList.union vj.edges
(*    |> (fun s -> assert(List.mem vj.index s = false); s) *)
    |> MyList.opmap
      (fun k ->
        match get g k with
        (* we remove v from their neighbors *)
        | Some vk when k <> vi.index ->
          Some(k, vk.weight)
        | _ -> None
      )
    |> List.split
  in
  let neighb = MyList.sum (vj.weight::wl) in
  vj.neighb <- neighb;
  vj.edges  <- edges;
  (* assert(vj.neighb = compute_neighb g vj); *)
  if vj.alive then array_push g.buckets vj.neighb vj.index;
  vj.neighb

(* vertex_suppress updates the value of g.lightest *)
let vertex_suppress (g:g) (vi:v) (k:int) : unit =
  (* let prefix = "["^prefix^".vertex_suppress]" in *)
  (* print_endline (prefix^" vi:"^(ToS.v vi)); *)
  assert(vi.alive);
  (* just checking that the vertex is in the right bucket *)
  assert(vi.neighb = k);
  (* [DEBUG]
  let neighb = compute_neighb g vi in
  if vi.neighb <> neighb
  then (
    print_endline (prefix^"    neighb:"^(ToS.int neighb));
    print_endline (prefix^" vi.neighb:"^(ToS.int vi.neighb));
    print_endline (prefix^" vi:"^(ToS.v vi));
    failwith (prefix^" detected internal inconsistency on neighbors weight")
  );
   *)
  set g vi.index None;
  let mapfun j =
    match get g j with
    | Some vj -> (
      g.lightest <-
        min g.lightest (vertex_add_clique g vi vj);
      Some j
    )
    | None    -> None
  in
  (* assert(List.mem vi.index vi.edges = false); *)
  (* assert(SetList.sorted vi.edges); *)
  vi.edges <- MyList.opmap mapfun vi.edges |> SetList.sort;
  ()

let rec lightspeed_rec (carry:v list) (g:g) : v list =
  (* assert(graph_check_neighb g); *)
  if verbose then (
    print_newline();
    print_endline ("carry:"^(ToS.(list v) carry));
    print_endline ("g:"^(ToS.g g));
    assert_lightest g;);
  let bka = g.buckets in
  let k = g.lightest in
  if k < Array.length g.buckets
  then (
    let l = normalize_bucket g k bka.(k) in
    (* assert(graph_check_neighb g); *)
    if verbose then print_endline ("normalized(buckets.(k)):"^(ToS.(list int) l));
    match l with
    | [] -> (
      bka.(k) <- [];
      g.lightest <- succ k;
      lightspeed_rec carry g
    )
    | i::bkk -> (
      bka.(k) <- bkk;
      let vi = Tools.unop (get g i) in
      (* assert(vertex_check_neighb g vi); *)
      vertex_suppress g vi k;
      lightspeed_rec (vi::carry) g
    )
  )
  else (List.rev carry)

let of_graphHFT (hg:GraphHFT.Type.hg) : g =
  let sumw = MyArray.sum GraphHFT.vertex_weight hg in
  let buckets = Array.make (sumw+1) [] in
  let lightest = ref (sumw+1) in
  let of_vertex (hv:GraphHFT.Type.hv) : v option =
    let index = hv.GraphGenLA.Type.index in
    let neighb = GraphHFT.neighbors_weight hg index in
    let alive  = GraphHFT.vertex_alive hv in
    if alive then (
      lightest := min !lightest neighb;
      array_push buckets neighb index;
    );
    let edges  = hv.GraphGenLA.Type.edges ||> fst in
    (* assert(List.mem index edges = false); *)
    Some {
      index;
      weight = GraphHFT.vertex_weight hv;
      neighb;
      names  = GraphHFT.vertex_names hv;
      edges;
      alive;
    }
  in
  let vertices = Array.map of_vertex hg in
  {vertices; buckets; lightest = !lightest}

let lightspeed_lopt ?(verbose=false) (hg:GraphHFT.Type.hg) : BNat.nat * (int list list) =
  let g = of_graphHFT hg in
  let vl = lightspeed_rec [] g in
  let cost : BNat.nat =
    vl
    ||> (fun v -> v.neighb)
    |> (fun il ->
      if verbose then
        print_endline ("[lightspeed_lopt] cost(not normalized):"^(ToS.(list int) il));
      il)
    |> Bicimal.normalize
    |> BNat.of_bicimal
  in
  let seq = vl ||> (fun v -> v.names) in
  (cost, seq)

let lightspeed_greedy ?(verbose=false) (input:Wap_exchange.input) : Wap_exchange.output =
  let prefix = "["^prefix^".lightspeed_greedy]" in
  let stop = OProfile.(time_start default) (prefix^" to_GraphHFT") in
  let hg : GraphHFT.Type.hg = Wap_exchange_utils.to_GraphHFT input in
  stop();
  let stop = OProfile.(time_start default) (prefix^" find_lopt") in
  let bnat_cost, seq = lightspeed_lopt ~verbose hg in
  stop();
  if verbose then print_endline (prefix^" blk:"^(STools.ToS.(list int) (seq ||> List.length)));
  if verbose then print_endline (prefix^" seq:"^(STools.ToS.(list(list int)) seq));
  let stop = OProfile.(time_start default) (prefix^" operator_SSO") in
  let output = Wap_exchange_utils.operator_SSO ~verbose input seq in
  stop();
  (* Check Cost Signature *)
  let bici_cost = output.Wap_exchange.cost in
  let bici_bnat_cost = BNat.to_bicimal bnat_cost in
  (if not (bici_bnat_cost = bici_cost)
  then (
    print_endline (prefix^" operator_SSO.cost:"^(STools.ToS.(list int) bici_cost));
    print_endline (prefix^"    lighspeed.cost:"^(STools.ToS.(list int) bici_bnat_cost));
    print_endline (prefix^"    lighspeed.cost:"^(BNat.to_string bnat_cost));
    print_endline (prefix^" input:"^(Wap_exchange_utils.ToS.input input));
    failwith (prefix^" cost inconsistency")
  ));
  output

