(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

open GuaCaml
open IterExtra

let n = (int_of_string Sys.argv.(1))

let file = Sys.argv.(2)
let err_file = Sys.argv.(3)

module TT = Ldd_B_u_nucx_types
module M = Ldd_B_u_nucx
module T = M.LDD
module A = Ldd_B_u_nucx_advanced

let man = T.G1.newman ()

let ( *! ) f0 f1 = T.G1.push man ((), f0, f1)

let peval_man = T.PEvalR.newman man
let peval p f = T.PEvalR.map_edge peval_man p f

let and_man = T.And.newman man peval_man
let ( &! ) f1 f2 = T.And.map and_man ((), f1, f2)

(* (* TEST 00 *)
let _ =
  print_endline "\nTEST 00";
  let d = TT.{neg = true; noutput = 193; ninput = Some (5); decomp = UCX ([B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B()], [(B (true, [A(); A(); (B true); A(); A(); A()]))])} in
  let d0 = TT.{neg = false; noutput = 4; ninput = Some (3); decomp = UCX ([A(); A(); A(); A()], [(B (true, [(B true); A(); A(); A()]))])} in
  let d00 = TT.{neg = false; noutput = 2; ninput = None; decomp = UCX ([A(); A()], [(B (true, [(B true); (B true)]))])} in
  let d01 = TT.{neg = true; noutput = 2; ninput = None; decomp = Id 0} in
  let d1 = TT.{neg = false; noutput = 4; ninput = Some (3); decomp = UCX ([B(); A(); A(); A()], [])} in
  let d10 = TT.{neg = false; noutput = 2; ninput = None; decomp = UCX ([A(); A()], [(B (true, [(B true); (B true)]))])} in
  let d11 = TT.{neg = true; noutput = 2; ninput = None; decomp = Id 0} in

  let f00 = (d00, Tree.GLeaf ()) in
  let f01 = (d01, Tree.GLeaf ()) in
  let f0 = M.ThisG.compose d0 (f00 *! f01) in
  let f10 = (d10, Tree.GLeaf ()) in
  let f11 = (d11, Tree.GLeaf ()) in
  let f1 = M.ThisG.compose d1 (f10 *! f11) in
  let f = M.ThisG.compose d (f0 *! f1) in

  let param = Support.of_sorted_support 193 [130] in
  let d = TT.{neg = false; noutput = 193; ninput = Some (4); decomp = UCX ([B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); A(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B(); B()], [(B (false, [A(); A(); (B true); A(); A()]))])} in
  let d0 = TT.{neg = false; noutput = 3; ninput = None; decomp = UCX ([A(); A(); A()], [(B (false, [(B true); A(); A()])); (B (true, [(B true); B false]))])} in
  let d1 = TT.{neg = false; noutput = 3; ninput = None; decomp = UCX ([B(); A(); A()], [(B (true, [(B true); B false]))])} in
  let g0 = (d0, Tree.GLeaf()) in
  let g1 = (d1, Tree.GLeaf()) in
  let g_ref = M.ThisG.compose d (g0 *! g1) in

  let aman = A.OOPS.makeman() in

  let g = A.OOPS.Model.existsA aman param f in
  assert(A.OOPS.Model.eq aman g g_ref);
  ()
*)

(*
let or_man = T.OR.newman man
let ( |! ) f1 f2 = T.OR.map or_man ((), f1, f2)
 *)
let xor_man = T.Xor.newman man peval_man
let ( ^! ) f1 f2 = T.Xor.map xor_man ((), f1, f2)

let make_bool (b:bool) : T.G0.edge' = M.cst b 0

let gen_bnb =
  let gen_bool = Iter.gen_bool $$ make_bool in
  let rec aux (lx: T.G0.edge' Iter.iter) = function
    | 0 ->  lx
    | n ->  ( aux (lx $* lx $$ (fun (x, y) -> x *! y)) (n-1) )
  in (fun n -> assert(n>=0); aux gen_bool n)

let count s n f (i, x) =
  if i mod n = 0 then (print_string s; print_int i; print_string "\r"; flush stdout);
  f x

let g n = gen_bnb n
let g2 n = (g n) $* (g n)
let g22 n = (g2 n) $* (g2 n)

let _ =
  print_string "TEST 1.0 : cons is reversible"; print_newline();
  M.LDD.to_file man (g (n+2) |> Iter.enumerate 0 $$ (count "T:1.0 : " 10000 (fun x -> x)) |> Iter.to_list) file;
  print_newline();

  print_string "TEST 2.0 : and primitives are consistent (NONE)"; print_newline();

  Iter.iter (fun (x, y) -> assert((make_bool x) &! (make_bool y) = (make_bool (x&&y)));) (Iter.gen_bool $* Iter.gen_bool);
  print_string "TEST 2.1 : and is consistant"; print_newline();
  g22 n |> Iter.enumerate 0 $$ (count "T:2.0 : " 10000 (fun ((x0, x1), (y0, y1)) ->
    let x01 = x0 *! x1
    and y01 = y0 *! y1
    and xy0 = x0 &! y0
    and xy1 = x1 &! y1 in
    let calcX = x01 &! y01
    and calcY = xy0 *! xy1 in
    assert(calcX = calcY);
    )) |> Iter.iter ignore;
  print_newline();

(*
  print_string "TEST 2.0 : or primitives are consistent (NONE)"; print_newline();

  Iter.iter (fun (x, y) -> assert((make_bool x) &! (make_bool y) = (make_bool (x&&y)));) (Iter.gen_bool $* Iter.gen_bool);
  print_string "TEST 2.1 : or is consistant"; print_newline();
  g22 n |> Iter.enumerate 0 $$ (count "T:2.0 : " 10000 (fun ((x0, x1), (y0, y1)) ->
    let x01 = x0 *! x1
    and y01 = y0 *! y1
    and xy0 = x0 |! y0
    and xy1 = x1 |! y1 in
    let calcX = x01 |! y01
    and calcY = xy0 *! xy1 in
    assert(calcX = calcY);
    )) |> Iter.iter ignore;
  print_newline();

  *)

  print_string "TEST 3.0 : xor is consistant"; print_newline();
  g22 n |> Iter.enumerate 0 $$ (count "T:3.0 : " 10000 (fun ((x0, x1), (y0, y1)) ->
    let x01 = x0 *! x1
    and y01 = y0 *! y1
    and xy0 = x0 ^! y0
    and xy1 = x1 ^! y1 in
    let calcX = x01 ^! y01
    and calcY = xy0 *! xy1 in
    if (calcX <> calcY)
    then
    (
      let edges = [x0; x1; y0; y1; x01; y01; xy0; xy1; calcX; calcY] in
      (*let strman = Udag.String.newman () in
      let stredges = M.LDD.to_dot man strman edges in
      Udag.String.to_dot_file strman stredges err_file;*)
      ignore(M.LDD.to_file man edges err_file);
      assert false;
    );
    )) |> Iter.iter ignore;
  print_newline();

  print_string "TEST 4.0 : partial evaluation (PEvalR) is consistant"; print_newline();

  let gen_assign n = (Iter.of_list [None; Some false; Some true]) $^ n in

  let peman : T.PEvalR.tt = T.PEvalR.newman man in
  let peval (p:bool option list) f = T.PEvalR.solve peman p f in

  let gp n = (g2 n) $* (gen_assign n) in

  gp (n+1) |> Iter.enumerate 0 $$ (count "T:4.0 : " 10000 (fun ((x0, x1), set) ->
    let x01 = x0 *! x1 in
    let pe_set_x0 = peval set x0
    and pe_set_x1 = peval set x1
    and pe_0set_x01 = peval ((Some false)::set) x01
    and pe_1set_x01 = peval ((Some true )::set) x01
    and pe_Nset_x01 = peval ( None       ::set) x01 in
    let pe_set_x0_pe_set_x1 = pe_set_x0 *! pe_set_x1 in
    assert(pe_set_x0 = pe_0set_x01);
    assert(pe_set_x1 = pe_1set_x01);
    assert(pe_Nset_x01 = pe_set_x0_pe_set_x1);
    )) |> Iter.iter ignore;
  print_newline();

  print_string "TEST 5.0 : universal quantifier elimination (ForallR) is consistant"; print_newline();

  let gen_quant n : bool list iter = Iter.gen_bool $^ n in
  let gq n = (g2 n) $* (gen_quant n) in

  let uqeman = A.ForallR.newman man and_man in
  let forall (p:bool list) f = A.ForallR.solve uqeman p f in

  let adv_man = A.OOPS.Model.newman () in
  let to_string (f:A.OOPS.Model.f) : string =
    A.OOPS.Model.to_string adv_man f
  in

  gq (n+1) |> Iter.enumerate 0 $$ (count "T:5.0 : " 10000 (fun ((x0, x1), set) ->
    let x01 = x0 *! x1 in
    let uqe_set_x0 = forall set x0
    and uqe_set_x1 = forall set x1
    and uqe_0set_x01 = forall (false::set) x01
    and uqe_1set_x01 = forall (true ::set) x01 in
    let config_and = uqe_set_x0 &! uqe_set_x1
    and config_sha = uqe_set_x0 *! uqe_set_x1 in
    if uqe_0set_x01 <> config_sha
    then (
      print_endline "[noreg_ldd_B_u_nucx:UQE] failure with 'uqe_0set_x01 <> config_sha'";
      print_endline ("[noreg_ldd_B_u_nucx:UQE] (false::set):(false::"^(STools.ToS.(list bool) set)^")");
      print_endline ("[noreg_ldd_B_u_nucx:UQE] x0:"^(to_string x0));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] x1:"^(to_string x1));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] x01:"^(to_string x01));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] uqe_set_x0:"^(to_string uqe_set_x0));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] uqe_set_x1:"^(to_string uqe_set_x1));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] uqe_0set_x01:"^(to_string uqe_0set_x01));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] config_sha:"^(to_string config_sha));
      failwith "[DAGaml.noreg_ldd_B_u_nucx:UQE] failure with 'uqe_0set_x01 <> config_sha'";
    );
    if uqe_1set_x01 <> config_and
    then (
      print_endline "[noreg_ldd_B_u_nucx:UQE] failure with 'uqe_1set_x01 <> config_and'";
      print_endline ("[noreg_ldd_B_u_nucx:UQE] (true ::set):(true ::"^(STools.ToS.(list bool) set)^")");
      print_endline ("[noreg_ldd_B_u_nucx:UQE] x0:"^(to_string x0));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] x1:"^(to_string x1));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] x01:"^(to_string x01));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] uqe_1set_x01:"^(to_string uqe_1set_x01));
      print_endline ("[noreg_ldd_B_u_nucx:UQE] config_and:"^(to_string config_and));
      failwith "[DAGaml.noreg_ldd_B_u_nucx:UQE] failure with 'uqe_1set_x01 <> config_and'";
    );
    )) |> Iter.iter ignore;
  print_newline();

  exit 0
