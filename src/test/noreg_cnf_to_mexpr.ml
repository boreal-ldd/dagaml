
module OOPS = NuB.GroBdd.OOPS

let (&!) = OOPS.(&!)
let (|!) = OOPS.(|!)

let _ =
  let file_in  = Sys.argv.(1) in
  let file_out = Sys.argv.(2) in
  let file_pure0 = Sys.argv.(3) in
  let file_pure1 = Sys.argv.(4) in
  let file_v1 = Sys.argv.(5) in
  let file_v2 = Sys.argv.(6) in
  let cnf = StrLoadCnf.load_file file_in in
  StrDumpCnf.dump_file file_out cnf;
  let pureB = NuB.GroBdd.newman() in
  let vars = OOPS.array_make_n_var pureB cnf.CnfTypes.input in
  let conv_clauses lll =
    let conv_clause ll =
      let conv_lit (b, x) = OOPS.cneg pureB b vars.(x) in
      OOPS.list_or pureB (Tools.map conv_lit ll)
    in
    OOPS.list_and pureB (Tools.map conv_clause lll)
  in
  let fB0 = conv_clauses cnf.CnfTypes.clauses in
  let pureB0 = pureB.cons in
  NuB.GroBdd.to_file pureB0 [fB0] file_pure0;
  let fB1 = OOPS.LoadCnf.step2 cnf pureB in
  NuB.GroBdd.to_file pureB0 [fB1] file_pure1;
  let cnax = Cnax.CNAX.G1.newman() in
  let fC1 = Cnax.OOPS.LoadCnf.step2 cnf cnax in
  let scnf = IoUtils.make
    "CNF" cnax cnf.CnfTypes.input [|fC1.Cnax.OOPS.Model.edge|]
  in
  let sexpr = IoUtils.(expr_of_cnax scnf string_fnn) in
  StrDumpVerilog.dump_file file_v1 sexpr;
  let snuBpure = IoUtils.nuBpure_of_expr sexpr in
  let sexpr = IoUtils.(expr_of_nuBpure snuBpure string_fnn) in
  StrDumpVerilog.dump_file file_v2 sexpr;
  ()
