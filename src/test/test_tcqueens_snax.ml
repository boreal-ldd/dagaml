(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

(* All Right Reserved

   Copyright (c) 2015-2020 Joan Thibault
*)

(*
  [./test_nqueen_snax.native n]
  generates a verilog file containing an encoding of
  the [n]-queen problem.
  the output file is "workdir/[n]-queens.v"
 *)
open GuaCaml
let man = Snax.Module.G1.newman();;

module OOPS = SnaxOops.OOPS
open OOPS.Model
open OOPS.VUInt

let n = int_of_string Sys.argv.(1);;

let file_out =
  if Array.length  Sys.argv > 2
  then Sys.argv.(2)
  else ("workdir/"^(string_of_int n)^"-tcqueens.v");;

let start = Sys.time() ;;

print_string "------ begin computation ------";;
print_newline();;

(* [c] denote the main constraint *)

let x, c = range man n;;
let c = OOPS.array_and man (OOPS.array_copy_fun_t man n c);;
let xx = Array.init n ((+?/) man x);;
let xxy = Array.map (copy_t man n) xx;;
let xy = copy_t man n x;;
let arity = xy.(0).arity;;

let matD = Array.init n (fun x -> Array.init x (fun y ->
  assert(y < x);
  let h = x - y in
  let a = (<>/) man xxy.(h).(x) xxy.(0).(y)
  and b = (<>/) man xxy.(h).(y) xxy.(0).(x) in
  (&!) man a b
));;

let diags = OOPS.array_and man (MyArray.flatten matD);;

let matL = Array.init n (fun x -> Array.init x (fun y ->
  assert(y < x);
  (<>/) man xy.(x) xy.(y)
));;

let lines = OOPS.array_and man (MyArray.flatten matL);;

let n_queens = (&!) man c ((&!) man lines diags);;

print_string "------ end of computation ------";;
print_newline();;

print_string "time:\t";;
print_float (Sys.time() -. start);;
print_string " s.";;
print_newline();;

let sys : string Snax.system = IoTypes.{
  name = "tcqueens";
  man;
  inps = Array.init arity (fun x -> "x"^(string_of_int x));
  regs = [||];
  wire = [||];
  outs = [|"y0"|];
  sset = [|("y0", n_queens.edge)|];
};;

let sexpr = IoUtils.expr_of_snax sys IoUtils.string_fnn in
StrDumpVerilog.dump_file file_out sexpr;;

exit 0;;
