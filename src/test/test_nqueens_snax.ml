(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

(*
  [./test_nqueen_snax.native n]
  generates a verilog file containing an encoding of
  the [n]-queen problem.
  the output file is "workdir/[n]-queens.v"
 *)

let man = Snax.Module.G1.newman();;

module OOPS = SnaxOops.OOPS
open OOPS.Model

let n = (int_of_string Sys.argv.(1));;

let file_out =
  if Array.length  Sys.argv > 2
  then Sys.argv.(2)
  else ("workdir/"^(string_of_int n)^"-queens.v");;

let start = Sys.time() ;;

print_string "------ begin computation ------";;
print_newline();;

let ( *! ) : f -> f -> f = ( *! ) man;;
let ( &! ) : f -> f -> f = ( &! ) man;;
let ( |! ) : f -> f -> f = ( |! ) man;;
let ( ^! ) : f -> f -> f = ( ^! ) man;;
let ( =! ) : f -> f -> f = ( =! ) man;;
let ( ->> ) : bool list -> f -> f = ( ->> ) man;;

let var_n : f array = OOPS.array_make_n_var man n;;

let ue_var_n : f = OOPS.array_exactly_one man var_n;;

let rows : f array = OOPS.array_copy_fun_t man n ue_var_n;;
let cols : f array = OOPS.array_copy_fun man n ue_var_n;;

let et_cols : f = OOPS.array_and man cols;;
let et_rows : f = OOPS.array_and man rows;;
let n_tower : f = et_cols &! et_rows;;

let liste = ref [];;

for k = 1 to n-1
do
    (*    i0 + j0 = k
        i1 + (n-j1) = k
        (n-i2) + j2 = k
        (n-i3) + (n-j3) = k *)
    let vars = OOPS.array_make_n_var man (k+1) in
    let uniq = OOPS.array_atmost_one man  vars in
    let t0 = Array.make (n*n) false
    and t1 = Array.make (n*n) false
    and t2 = Array.make (n*n) false
    and t3 = Array.make (n*n) false in
    for i = 0 to k
    do
        let j = k - i in
        t0.(     i *n +      j ) <- true;
        t1.((n-1-i)*n +      j ) <- true;
        t2.(     i *n + (n-1-j)) <- true;
        t3.((n-1-i)*n + (n-1-j)) <- true;
    done;
    let uniq0 = (Array.to_list t0) ->> uniq
    and uniq1 = (Array.to_list t1) ->> uniq
    and uniq2 = (Array.to_list t2) ->> uniq
    and uniq3 = (Array.to_list t3) ->> uniq in
    liste := uniq0::uniq1::uniq2::uniq3::(!liste);
done;

print_string "starting computation (#and = "; print_int (List.length(!liste)); print_string" )";
print_newline();;

let diag_uniq = OOPS.list_and man (!liste);;

let n_queens = n_tower &! diag_uniq;;
(*print_string "n_queens : "; cnt n_queens;;
OOPS.store_ALLsat man n_queens "../dots/nQueens.sat";;*)

print_string "------ end of computation ------";;
print_newline();;

print_string "time:\t";;
print_float (Sys.time() -. start);;
print_string " s.";;
print_newline();;

let sys = IoUtils.make "nqueens" man (n*n) [|n_queens.edge|];;
let sexpr = IoUtils.expr_of_snax sys IoUtils.string_fnn in
StrDumpVerilog.dump_file file_out sexpr;;

exit 0;;
