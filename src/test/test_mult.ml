(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

(*
  Generation of n-bit multiplier as a verilog file
 *)
let nbit = int_of_string(Sys.argv.(1))
let file_out = Sys.argv.(2)
let man = SnaxOops.newman()

module OOPS = SnaxOops.OOPS
open OOPS.VUInt

let arity = 2 * nbit

let vars = OOPS.array_make_n_var man arity
let x  = {arity; array = Array.init nbit (fun i -> vars.(2*i+0))}
let y  = {arity; array = Array.init nbit (fun i -> vars.(2*i+1))}

let z = mult man x y

(*
let edges = (Array.to_list vars)@(Array.to_list eqL.array)@(Array.to_list eqR.array)@[edge] in
*)
let edges = Array.to_list z.array
let name = "uint_mult_"^(string_of_int nbit)
let _ =
  ConvUtils.snaxoops_to_verilog man edges name file_out;
  exit 0
