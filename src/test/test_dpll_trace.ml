open GuaCaml
open Extra
open STools
open CnfTypes
open DpllTrace

module ComposeOops(MO:OOPS.MSig) =
struct
  module GO = OOPS.Make(MO)

  module Model =
  struct
    type extra = {
      oman : MO.t;
      fa   : MO.f array;
      arity: int;
    }
    type xedge = int * MO.f
    type xnode = xedge
    type param = int list

    let get_var t b i =
      MO.cneg t.oman b t.fa.(i)

    let rec map_formule (t:extra) : TraceTypes.t -> MO.f =
      function
      | TraceTypes.Cst b -> (MO.cst t.oman b t.arity)
      | TraceTypes.And (ul, tl) -> (
        GO.list_and t.oman ~arity:(Some t.arity)
          ((ul ||> (fun (i, b) -> get_var t b i)) @
           ((tl ||> map_formule t)))
      )
      | TraceTypes.Sha (v, f0, f1) -> (
        let c = get_var t false v in
        GO.ite t.oman c (map_formule t f0) (map_formule t f1)
      )
  end

  type t   = Model.extra
  type f0  = TraceTypes.t
  type tt0 = unit

  let newman (oman:MO.t) (fa:MO.f array) (arity:int): t =
    Model.{oman; fa; arity}

  let compute (man:t) (f:TraceTypes.file) : MO.f =
    Model.map_formule man f.TraceTypes.formule

  let translate ?(verbose=false) (oman:MO.t) ?man ?(order=None) (f:TraceTypes.file) : MO.f =
    let len = f.TraceTypes.input in
    let man =
      match man with
      | Some man -> (
        assert(Array.length man.Model.fa = len);
        assert(order = None); (* not implemented yet *)
        man
      )
      | None -> (
        let vars = GO.array_make_n_var oman len in
        let vars =
          match order with
          | None -> vars
          | Some o -> (
            if verbose then print_endline ("order:"^(ToS.(array int) o));
            Array.init (Array.length vars) (fun i -> vars.(o.(i)))
          )
        in
        newman oman vars len
      )
    in
    compute man f

  let export_to_system ?(verbose=false) (f:TraceTypes.file) ?(name="CNF") ?(order=None) (fileB:string) : string GO.system =
    let man = MO.newman() in
    let inps =
      Array.map (fun i -> "x"^(string_of_int i))
      (match order with
       | None -> Array.init f.TraceTypes.input (fun i -> i)
       | Some order -> (
         assert(Array.length order = f.TraceTypes.input);
         order
       ))
    in
    if verbose then print_endline ("inps:"^(ToS.(array string) inps));
    let out = translate ~verbose man ~order f in
    IoTypes.{
      name;
      man;
      inps;
      regs = [||];
      wire = [||];
      outs = [|"y0"|];
      sset = [|"y0", out|];
    }

  let export_to_file ?(system=true) ?(verbose=false) (fileA:TraceTypes.file) ?(name="CNF") ?(order=None) (fileB:string) : unit =
    let sys = export_to_system ~verbose fileA ~name ~order fileB in
    if system
    then GO.system_tof sys fileB
    else GO.to_file sys fileB

end

module TO_O_NU   = ComposeOops(Ldd_B_o_nu.LDD.OOPS.Model)
module TO_O_UC0  = ComposeOops(Ldd_B_o_uc0.LDD.OOPS.Model)
module TO_O_NUCX = ComposeOops(Ldd_B_o_nucx.LDD.OOPS.Model)
module TO_U_NU   = ComposeOops(Ldd_B_u_nu.LDD.OOPS.Model)
module TO_U_NUX  = ComposeOops(Ldd_B_u_nux.LDD.OOPS.Model)
module TO_U_NUC  = ComposeOops(Ldd_B_u_nuc.LDD.OOPS.Model)
module TO_U_NUCX = ComposeOops(Ldd_B_u_nucx.LDD.OOPS.Model)
module TO_SNAX   = ComposeOops(SnaxOops.OOPS.Model)

type params = {
  mutable param_input_file  : string option;
  mutable param_output_mode : string option;
  mutable param_output_file : string option;
  mutable param_order : int array option;
  param_verbose : bool ref;
}

let params = {
  param_input_file  = None;
  param_output_mode = None;
  param_output_file = None;
  param_order       = None;
  param_verbose     = ref false;
}

let set_input_file s =
  assert(params.param_input_file = None);
  params.param_input_file <- Some s

let set_output_mode s =
  assert(params.param_output_mode = None);
  params.param_output_mode <- Some s

let set_output_file s =
  assert(params.param_output_file = None);
  params.param_output_file <- Some s

let set_order (s:string) : unit =
  let order = s
    |> String.split_on_char ' '
    ||> OfS.int
    |> Array.of_list
  in
  params.param_order <- Some order

let spec =
  [("-v"     , Arg.Set params.param_verbose, "Verbose"       );
   ("--if"   , Arg.String set_input_file   , "Input File"    );
   ("--mode" , Arg.String set_output_mode  , "Output Mode"   );
   ("--of"   , Arg.String set_output_file  , "Output File"   );
   ("--order", Arg.String set_order        , "Variable Order");
  ]

let usage = Sys.argv.(0) ^ " [options] input.cnf $mode:{o-nucx, u-nucx} output.$mode.B.pure"

let print_usage () = Arg.usage spec usage

let print_status msg =
  if !(params.param_verbose) then print_endline msg

let parse_anon s =
  match params.param_input_file with
  | None -> set_input_file s
  | Some _ ->
  match params.param_output_mode with
  | None -> set_output_mode s
  | Some _ ->
  match params.param_output_file with
  | None -> set_output_file s
  | Some _ ->
  match params.param_order with
  | None -> set_order s
  | Some _ -> assert false

let export_to_file ?(verbose=false) mode fileA order fileB =
  match mode with
  | "o-nu"   ->   TO_O_NU.export_to_file ~verbose fileA ~order fileB
  | "o-uc0"  ->  TO_O_UC0.export_to_file ~verbose fileA ~order fileB
  | "o-nucx" -> TO_O_NUCX.export_to_file ~verbose fileA ~order fileB
  | "u-nu"   ->   TO_U_NU.export_to_file ~verbose fileA ~order fileB
  | "u-nux"  ->  TO_U_NUX.export_to_file ~verbose fileA ~order fileB
  | "u-nuc"  ->  TO_U_NUC.export_to_file ~verbose fileA ~order fileB
  | "u-nucx" -> TO_U_NUCX.export_to_file ~verbose fileA ~order fileB
  | "snax"   ->   TO_SNAX.export_to_file ~verbose fileA ~order fileB
  | "v"      -> (
    let sys_snax =
      TO_SNAX.export_to_system ~verbose fileA ~order fileB
      |> SnaxOops.system_of_oops_system
    in
    let sys_expr = IoUtils.expr_of_snax sys_snax IoUtils.string_fnn in
    StrDumpVerilog.dump_file fileB sys_expr
  )
  | _ -> failwith ("[export_to_file] unknown mode : \""^(String.escaped mode)^"\"")

let _ =
  Arg.parse spec parse_anon usage;
  let input_file = Tools.unop params.param_input_file in
  let output_file = Tools.unop params.param_output_file in
  let output_mode = Tools.unop params.param_output_mode in
  let file = StrLoadCnf.load_file input_file in
  let file = dpll_naive file in
  let order = params.param_order in
  if !(params.param_verbose)
  then print_endline ("order:"^ToS.(option(array int)) order);
  (match order with
    | Some order ->
      assert(Array.length order = file.TraceTypes.input);
    | None -> ());
  export_to_file ~verbose:(!(params.param_verbose)) output_mode file order output_file;
  ()
