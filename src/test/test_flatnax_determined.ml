(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

open GuaCaml
open Extra
module CSTypes = Constraint_system_types
module CSUtils = Constraint_system_utils
(* [MOVEME?] MyHashtbl ? *)
let assoc_of_hashtbl_with_filter
    ?(sort=false)
     (p:'k -> 'v -> bool)
     (h:('k, 'v)Hashtbl.t) :
        ('k * 'v) list =
  let stack = ref [] in
  Hashtbl.iter (fun k v -> if p k v then (stack := (k, v) :: !stack)) h;
  if sort
  then List.sort Stdlib.compare !stack
  else !stack

let _ =
  let file_in = Sys.argv.(1) in
  let mode = Sys.argv.(2) in
  ignore mode;
  let file_out = Sys.argv.(3) in
  let var = int_of_string Sys.argv.(4) in
  print_endline "[test_flatnax._] step:0";
  let cir_sys = StrLoadCir.load_file file_in in (* clause = int Expr.expr *)
  print_endline "[test_flatnax._] step:1";
  let nax_sys = IoUtils.nax_of_expr (CSUtils.to_iotypes_system cir_sys) in
  print_endline "[test_flatnax._] step:2";
  let edges_array = Array.map snd nax_sys.IoTypes.sset in
  print_endline "[test_flatnax._] step:3";
  let edges_list  = Array.to_list edges_array in
  print_endline "[test_flatnax._] step:4";
  ignore edges_list; (* Nax.ToGraphviz.to_graphviz_file nax_sys.IoTypes.man edges_list file_out; *)
  print_endline "[test_flatnax._] step:5";
  let flatnax_man = FlatNAX.Module.G1.newman() in
  print_endline "[test_flatnax._] step:6";
  let nax_man = nax_sys.IoTypes.man in
  let flatnax_edges = FlatNAX.OfNAX.export nax_man flatnax_man edges_array in
  print_endline "[test_flatnax._] step:7";
  let flatnax_edges = FlatNAX.Flatten.export flatnax_man flatnax_man flatnax_edges in
  let next0 = Tree.GLeaf (Some var) in
  let flatnax_edges0 : _ array = FlatNAX.Utils.subst_ident flatnax_man flatnax_man 1 flatnax_edges next0 (false, Tree.GLeaf None) in
  let flatnax_edges1 : _ array = FlatNAX.Utils.subst_ident flatnax_man flatnax_man 1 flatnax_edges next0 (true , Tree.GLeaf None) in
  let flatnax_edges' : _ list = (Array.to_list flatnax_edges0) @> (Array.to_list flatnax_edges1) in

  let nax_edges' : _ array = FlatNAX.ToNAX.export flatnax_man nax_man (Array.of_list flatnax_edges') in
  let expr_edges' = Nax.ToExpr.export nax_man nax_edges' in
  (* FlatNAX.ToGraphviz.to_graphviz_file flatnax_man flatnax_edges' file_out; *)
  let cir'_sys = CSTypes.{cir_sys with
    name = cir_sys.name^"'";
    man  = ();
    formule = Array.to_list expr_edges';
  } in
  StrDumpCir.dump_file file_out cir'_sys;
  ()
