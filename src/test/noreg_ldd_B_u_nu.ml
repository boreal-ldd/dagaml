(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

open GuaCaml
open IterExtra

let n = (int_of_string Sys.argv.(1))

let file = Sys.argv.(2)
let err_file = Sys.argv.(3)

module M = Ldd_B_u_nu
module T = M.LDD

let man = T.G1.newman ()

let ( *! ) f0 f1 = T.G1.push man ((), f0, f1)

let and_man = T.And.newman man
let ( &! ) f1 f2 = T.And.map and_man ((), f1, f2)
(*
let or_man = T.OR.newman man
let ( |! ) f1 f2 = T.OR.map or_man ((), f1, f2)
 *)
let xor_man = T.Xor.newman man
let ( ^! ) f1 f2 = T.Xor.map xor_man ((), f1, f2)

let make_bool (b:bool) : T.G0.edge' = M.cst b 0

let gen_bnb =
  let gen_bool = Iter.gen_bool $$ make_bool in
  let rec aux (lx: T.G0.edge' Iter.iter) = function
    | 0 ->  lx
    | n ->  ( aux (lx $* lx $$ (fun (x, y) -> x *! y)) (n-1) )
  in (fun n -> assert(n>=0); aux gen_bool n)

let count s n f (i, x) =
  if i mod n = 0 then (print_string s; print_int i; print_string "\r"; flush stdout);
  f x

let gn = gen_bnb n
let gn2 = gn $* gn
let gn22 = gn2 $* gn2

let _ =
  print_string "TEST 1.0 : cons is reversible"; print_newline();
  M.LDD.to_file man (gn |> Iter.enumerate 0 $$ (count "T:1.0 : " 10000 (fun x -> x)) |> Iter.to_list) file;
  print_newline();

  print_string "TEST 2.0 : and primitives are consistent (NONE)"; print_newline();

  Iter.iter (fun (x, y) -> assert((make_bool x) &! (make_bool y) = (make_bool (x&&y)));) (Iter.gen_bool $* Iter.gen_bool);
  print_string "TEST 2.1 : and is consistant"; print_newline();
  gn22 |> Iter.enumerate 0 $$ (count "T:2.0 : " 10000 (fun ((x0, x1), (y0, y1)) ->
    let x01 = x0 *! x1
    and y01 = y0 *! y1
    and xy0 = x0 &! y0
    and xy1 = x1 &! y1 in
    let calcX = x01 &! y01
    and calcY = xy0 *! xy1 in
    assert(calcX = calcY);
    )) |> Iter.iter ignore;
  print_newline();

(*
  print_string "TEST 2.0 : or primitives are consistent (NONE)"; print_newline();

  Iter.iter (fun (x, y) -> assert((make_bool x) &! (make_bool y) = (make_bool (x&&y)));) (Iter.gen_bool $* Iter.gen_bool);
  print_string "TEST 2.1 : or is consistant"; print_newline();
  gn22 |> Iter.enumerate 0 $$ (count "T:2.0 : " 10000 (fun ((x0, x1), (y0, y1)) ->
    let x01 = x0 *! x1
    and y01 = y0 *! y1
    and xy0 = x0 |! y0
    and xy1 = x1 |! y1 in
    let calcX = x01 |! y01
    and calcY = xy0 *! xy1 in
    assert(calcX = calcY);
    )) |> Iter.iter ignore;
  print_newline();

  *)

  print_string "TEST 3.0 : xor is consistant"; print_newline();
  gn22 |> Iter.enumerate 0 $$ (count "T:2.0 : " 10000 (fun ((x0, x1), (y0, y1)) ->
    let x01 = x0 *! x1
    and y01 = y0 *! y1
    and xy0 = x0 ^! y0
    and xy1 = x1 ^! y1 in
    let calcX = x01 ^! y01
    and calcY = xy0 *! xy1 in
    if (calcX <> calcY)
    then
    (
      let edges = [x0; x1; y0; y1; x01; y01; xy0; xy1; calcX; calcY] in
      (*let strman = Udag.String.newman () in
      let stredges = M.LDD.to_dot man strman edges in
      Udag.String.to_dot_file strman stredges err_file;*)
      ignore(M.LDD.to_file man edges err_file);
      assert false;
    );
    )) |> Iter.iter ignore;
  print_newline();

  exit 0
(*
print_string "TEST 4.0 : partial evaluation is consistant"; print_newline()

let gen_assign n = (Iter.of_list [None; Some false; Some true]) $^ n

let peman, peval = M.PartEval.newman man

gn2 $* (gen_assign n) |> Iter.enumerate 0 $$ (count "T:2.0 : " 10000 (fun ((x0, x1), set) ->
  let x01 = x0 *! x1 in
  let pe_set_x0 = peval (Some set) x0
  and pe_set_x1 = peval (Some set) x1
  and pe_0set_x01 = peval (Some((Some false)::set)) x01
  and pe_1set_x01 = peval (Some((Some true )::set)) x01
  and pe_Nset_x01 = peval (Some( None       ::set)) x01 in
  let pe_set_x0_pe_set_x1 = pe_set_x0 *! pe_set_x1 in
  assert(pe_set_x0 = pe_0set_x01);
  assert(pe_set_x1 = pe_1set_x01);
  assert(pe_Nset_x01 = pe_set_x0_pe_set_x1);
  )) |> Iter.iter ignore
print_newline()
*)
