(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

open GuaCaml
open Extra
open STools

module ThisLdd = Ldd_B_o_u
module ThisLDD = ThisLdd.LDD
module ThisAdvanced = Ldd_B_o_u_advanced
module ThisAdvancedOops = ThisAdvanced.OOPS

let wap_solver =
  if true
  then Snowflake.Wap_lightspeed.lightspeed_greedy
  else Snowflake.Wap_lightweight.lightweight_greedy

let load_cnf target =
  if false
  then StrLoadCnf.load_file target
  else StrLoadCnf.load_qbf_file ~discardQ:true target

let main_wap_solver file_in file_out =
  file_in
  |> load_cnf
  |> CnfUtils.Wap.input_of_cnf
  |> (fun input ->
      print_endline (Snowflake.Wap_exchange_utils.ToS.input input);
      input)
  |> wap_solver
  |> (fun output ->
      print_endline ("cost:"^(ToS.(list int) output.Snowflake.Wap_exchange.cost));
      output)
  |> Snowflake.Wap_exchange_utils.ToS.output
  |> (fun string ->
        let cha_out = open_out file_out in
        output_string cha_out string;
        output_string cha_out "\n";
        close_out cha_out
     );
  ()

let main_dump_primgraph file_in file_out =
  let cnf = load_cnf file_in in
  let graph = CnfUtils.Wap.input_of_cnf cnf in
  let vars = graph.variables
  and params = graph.parameters in
  let nodes = SetList.union vars params in
  GraphKD.{nodes; cliques = graph.kdecomp}
(*  |> Tools.tee GraphKD.print_graph *)
  (* KD -> LA -> HFT *)
  |> GraphKD.to_GraphLA
  |> GraphLA.stree_of_graph
  |> (fun stree -> OfSTree.file [stree] file_out);
  ()

module QbfCCoSTreD_O_U = Ldd_B_o_u_advanced.QbfCCoSTreD

let _ =
  let mode  = Sys.argv.(1) in
  let file_in  = Sys.argv.(2) in
  let file_out  = Sys.argv.(3) in
  (match mode with
    | "--debug" ->
      (QbfCCoSTreD_O_U.main_debug file_in file_out)
    | "--debug-frp" ->
      (QbfCCoSTreD_O_U.main_debug ~file_frp:(Some Sys.argv.(4)) file_in file_out)
    | "--debug-bpp" ->
      (QbfCCoSTreD_O_U.main_debug ~file_bpp:(Some Sys.argv.(4)) file_in file_out)
    | "--debug-frp-bpp" ->
      (QbfCCoSTreD_O_U.main_debug ~file_frp:(Some Sys.argv.(4)) ~file_bpp:(Some Sys.argv.(5)) file_in file_out)
    | "--dummy" ->
      (QbfCCoSTreD_O_U.main_perf_dummy file_in file_out)
    | "--bpp-synth"   ->
      (QbfCCoSTreD_O_U.main_bpp_synthesis file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-synth-pure"   ->
      (QbfCCoSTreD_O_U.main_bpp_synthesis file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-synth-verilog"   ->
      (QbfCCoSTreD_O_U.main_bpp_synthesis file_in CnfUtils.CnfCoSTreD.MO_Verilog file_out)
    | "--bpp"   ->
      (QbfCCoSTreD_O_U.main_perf_bpp file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-pure" ->
      (QbfCCoSTreD_O_U.main_perf_bpp file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-verilog" ->
      (QbfCCoSTreD_O_U.main_perf_bpp file_in CnfUtils.CnfCoSTreD.MO_Verilog file_out)
    | "--bpp-bm"   ->
      (QbfCCoSTreD_O_U.main_perf_bpp ~bucketize_mode:(Tools.prio_of_string Sys.argv.(4)) file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-pure-bm" ->
      (QbfCCoSTreD_O_U.main_perf_bpp ~bucketize_mode:(Tools.prio_of_string Sys.argv.(4)) file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-verilog-bm" ->
      (QbfCCoSTreD_O_U.main_perf_bpp ~bucketize_mode:(Tools.prio_of_string Sys.argv.(4)) file_in CnfUtils.CnfCoSTreD.MO_Verilog file_out)
    | "--bpp-a"   ->
      (QbfCCoSTreD_O_U.main_perf_bpp_aqeops file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-a-pure" ->
      (QbfCCoSTreD_O_U.main_perf_bpp_aqeops file_in CnfUtils.CnfCoSTreD.MO_Pure file_out)
    | "--bpp-a-verilog" ->
      (QbfCCoSTreD_O_U.main_perf_bpp_aqeops file_in CnfUtils.CnfCoSTreD.MO_Verilog file_out)
    | "--frp"   ->
      (QbfCCoSTreD_O_U.main_perf_frp file_in file_out)
    | "--primgraph"   ->
      (main_dump_primgraph file_in file_out)
    | "--wap-solver"  ->
      (main_wap_solver file_in file_out)
    | _ -> failwith "[test_costred_bddG] unknown mode"
  );
  OProfile.print OProfile.default;
  ()
