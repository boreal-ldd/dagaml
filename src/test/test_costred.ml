(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

open GuaCaml
open Extra
open STools

(*
module ThisLdd = Ldd_B_o_nu
module ThisLDD = ThisLdd.LDD
module ThisAdvanced = Ldd_B_o_nu_advanced
module ThisAdvancedOops = ThisAdvanced.OOPS
 *)

type pure_type =
  | LDD_B_O_U
  | LDD_B_O_NU
  | LDD_B_O_NUCX
  | LDD_B_U_NU
  | LDD_B_U_NUCX

let compute_of_pure_type =
  function
  | LDD_B_O_U    -> Ldd_B_o_u_advanced.CoSTreD_Module.compute
  | LDD_B_O_NU   -> Ldd_B_o_nu_advanced.CoSTreD_Module.compute
  | LDD_B_O_NUCX -> Ldd_B_o_nucx_advanced.CoSTreD_Module.compute
  | LDD_B_U_NU   -> Ldd_B_u_nu_advanced.CoSTreD_Module.compute
  | LDD_B_U_NUCX -> Ldd_B_u_nucx_advanced.CoSTreD_Module.compute

let parse_pure_type =
  function
  | "LDD_B_O_U"    -> LDD_B_O_U
  | "LDD_B_O_NU"   -> LDD_B_O_NU
  | "LDD_B_O_NUCX" -> LDD_B_O_NUCX
  | "LDD_B_U_NU"   -> LDD_B_U_NU
  | "LDD_B_U_NUCX" -> LDD_B_U_NUCX
  | pure_type -> failwith ("[] unknown pure type :"^(STools.ToS.string pure_type))

let string_of_pure_type =
  function
  | LDD_B_O_U    -> "LDD_B_O_U"
  | LDD_B_O_NU   -> "LDD_B_O_NU"
  | LDD_B_O_NUCX -> "LDD_B_O_NUCX"
  | LDD_B_U_NU   -> "LDD_B_U_NU"
  | LDD_B_U_NUCX -> "LDD_B_U_NUCX"

module CSCoSTreD = Constraint_system_costred

type request =
  | Compute            of pure_type * CSCoSTreD.Types.request
  | PrimalGraph        of CSCoSTreD.PrimalGraph.Types.request
  | RefinedPrimalGraph of CSCoSTreD.RefinedPrimalGraph.Types.request

module ToS =
struct
  let request =
    function
    | Compute (pure_type, request) ->
      "Compute ("^(string_of_pure_type pure_type)^", "^(CSCoSTreD.Types.ToS.request request)^")"
    | PrimalGraph request ->
      "PrimalGraph "^(CSCoSTreD.PrimalGraph.Types.ToS.request request)
    | RefinedPrimalGraph request ->
      "PrimalGraph "^(CSCoSTreD.RefinedPrimalGraph.Types.ToS.request request)

  let request_pretty =
    function
    | Compute (pure_type, request) ->
      "Compute ("^(string_of_pure_type pure_type)^", "^(CSCoSTreD.Types.ToS.request_pretty request)^")"
    | PrimalGraph request ->
      "PrimalGraph "^(CSCoSTreD.PrimalGraph.Types.ToS.request_pretty request)
    | RefinedPrimalGraph request ->
      "PrimalGraph "^(CSCoSTreD.RefinedPrimalGraph.Types.ToS.request_pretty request)
end

let rec parse_args_compute
    (r:CSCoSTreD.Types.request)
    (args:string list) : CSCoSTreD.Types.request =
  match args with
  | [] -> r
  | hd::tl -> (
    let open CSCoSTreD.Types in
    let r = match hd with
      | "--input-cnf" -> set_input_type r IT_cnf
      | "--input-cir" -> set_input_type r IT_cir
      | "--perf-aqeops" -> set_perf r AQEOPS
      | "--perf-dummy"  -> set_perf r Dummy
      | "--perf-synthesis" -> set_perf r Synthesis
      | "--costred-frp" -> set_costred r FRP
      | "--costred-bpp" -> set_costred r BPP
      | "--costred-verbose-true" -> set_costred_verbose r true
      | "--costred-verbose-false" -> set_costred_verbose r false
      | "--output-pure" -> set_output_type r OT_Pure
      | "--output-verilog" -> set_output_type r OT_Verilog
      | _ -> (
        failwith
          ("[test_costred.parse_args_compute] unrecognized option "^
           (STools.ToS.string(String.escaped hd)))
      )
    in
    parse_args_compute r tl
  )

let rec parse_args_primal_graph
    (r:CSCoSTreD.PrimalGraph.Types.request)
    (args:string list) : CSCoSTreD.PrimalGraph.Types.request =
  match args with
  | [] -> r
  | hd :: tl -> (
    let open CSCoSTreD.PrimalGraph.Types in
    let r = match hd with
      | "--input-cnf" -> set_input_type r IT_cnf
      | "--input-cir" -> set_input_type r IT_cir
      | "--wap-true"  -> set_compute_wap r true
      | "--wap-false"  -> set_compute_wap r false
      | "--output-stree" -> set_output_type r OT_stree
      | "--output-ocaml" -> set_output_type r OT_ocaml
      | "--output-graphviz" -> set_output_type r OT_graphviz
      | _ -> (
        failwith
          ("[test_costred.parse_args_primal_graph] unrecognized option "^
           (STools.ToS.string(String.escaped hd)))
      )
    in
    parse_args_primal_graph r tl
  )

let rec parse_args_refined_primal_graph
    (r:CSCoSTreD.RefinedPrimalGraph.Types.request)
    (args:string list) : CSCoSTreD.RefinedPrimalGraph.Types.request =
  match args with
  | [] -> r
  | hd :: tl -> (
    let open CSCoSTreD.RefinedPrimalGraph.Types in
    match hd with
    | "--wap-true"        -> (
      let r = set_compute_wap r true in
      parse_args_refined_primal_graph r tl
    )
    | "--wap-false"       -> (
      let r = set_compute_wap r false in
      parse_args_refined_primal_graph r tl
    )
    | "--output-FT"       -> (
      let r = set_output_model r OM_FT in
      parse_args_refined_primal_graph r tl
    )
    | "--output-HFT"      -> (
      let r = set_output_model r OM_HFT in
      parse_args_refined_primal_graph r tl
    )
    | "--output-CFT"      -> (
      let r = set_output_model r OM_CFT in
      parse_args_refined_primal_graph r tl
    )
    | "--output-stree"    -> (
      let r = set_output_format r OF_stree in
      parse_args_refined_primal_graph r tl
    )
    | "--output-ocaml"    -> (
      let r = set_output_format r OF_ocaml in
      parse_args_refined_primal_graph r tl
    )
    | "--output-graphviz" -> (
      let r = set_output_format r OF_graphviz in
      parse_args_refined_primal_graph r tl
    )
    | "--set-vertices-of-interest" -> (
      match tl with
      | [] -> assert false
      | hd :: tl -> (
        let nv = int_of_string hd in
        let hd, tl = MyList.hdtl_nth nv tl in
        let r = set_vertices_of_interest r (Some (hd ||> int_of_string)) in
        parse_args_refined_primal_graph r tl
      )
    )
    | _ -> (
      failwith
        ("[test_costred.parse_args_refined_primal_graph] unrecognized option "^
         (STools.ToS.string(String.escaped hd)))
    )
  )

let parse_args (args:string list) : request =
  match args with
  | mode :: args -> (
    match mode with
    | "CoSTreD" -> (
      match args with
      | pure_type :: input_file :: output_file :: args -> (
        let pure_type = parse_pure_type pure_type in
        let r = CSCoSTreD.Types.builder input_file output_file () in
        Compute(pure_type, parse_args_compute r args)
      )
      | _ -> (
        failwith ("[test_costred.parse_args] not enough argument 'test CoSTreD <pure_type> <input_file> <output_file> [ <args> ... ]'");
      )
    )
    | "WAP"  -> (
      match args with
      | input_file :: output_file :: args -> (
        let r = CSCoSTreD.PrimalGraph.Types.builder input_file output_file in
        PrimalGraph(parse_args_primal_graph r args)
      )
      | _ -> (
        failwith ("[test_costred.parse_args] not enough argument 'test WAP <input_file> <output_file> [ <args> ... ]'");
      )
    )
    | "REFINED"  -> (
      match args with
      | input_file :: output_file :: args -> (
        let r = CSCoSTreD.RefinedPrimalGraph.Types.builder input_file output_file in
        RefinedPrimalGraph(parse_args_refined_primal_graph r args)
      )
      | _ -> (
        failwith ("[test_costred.parse_args] not enough argument 'test REFINED <input_file> <output_file> [ <args> ... ]'");
      )
    )
    | _ -> (
      failwith
        ("[test_costred.parse_args] unknown mode "^
         (STools.ToS.string(String.escaped mode)))
    )
  )
  | _ -> (
    failwith ("[test_costred.parse_args] no mode specified 'test <mode> [ <args> ... ]'");
  )

let _ =
  if Array.length Sys.argv < 4
  then (failwith "test <mode> <file_in> <file_out> [ <args> ... ]");
  let r = parse_args (List.tl(Array.to_list Sys.argv)) in
  print_endline(ToS.request_pretty r);
  let _ =
    match r with
    | Compute     (pure_type, request) -> compute_of_pure_type pure_type request
    | PrimalGraph request -> CSCoSTreD.PrimalGraph.compute request
    | RefinedPrimalGraph request -> CSCoSTreD.RefinedPrimalGraph.compute request
  in
  OProfile.print OProfile.default;
  ()
