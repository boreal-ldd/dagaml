(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : tests : specialized executables for testing purpose
 *)

let _ =
  let file_in = Sys.argv.(1) in
  let mode = Sys.argv.(2) in
  let file_out = Sys.argv.(3) in
  let args = [] in
  match mode with
  | "--v0" -> (ConvCore.Ext_to_Ext.cnf_to_verilog_v0 file_in file_out args)
  | "--vA:0" -> (ConvCore.Ext_to_Ext.cnf_to_verilog_vA file_in file_out ~simpl:0 args)
  | "--vA:1" -> (ConvCore.Ext_to_Ext.cnf_to_verilog_vA file_in file_out ~simpl:1 args)
  | "--vA:2" -> (ConvCore.Ext_to_Ext.cnf_to_verilog_vA file_in file_out ~simpl:2 args)
  | "--vA:3" -> (ConvCore.Ext_to_Ext.cnf_to_verilog_vA file_in file_out ~simpl:3 args)
  | _ -> failwith ("[test_cnf_to_verilog] unsupported mode:\""^mode^"\"")
