open GuaCaml
open CnfTypes
open Dpll

let _ =
  let input_file = Sys.argv.(1) in
  let file = StrLoadCnf.load_file input_file in
  match dpll_naive file with
  | Some sol -> (
    print_endline "s SATISFIABLE";
    print_string "v ";
    StrDumpCnf.output_clause print_string sol;
    print_newline();
  )
  | None ->
    print_endline "s UNSAT"
