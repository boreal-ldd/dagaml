(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Cwap_exchange_utils : toolbox for interfacing wap-solver with cwap-exchange interface.
*)

open GuaCaml
open Extra
open Cwap_exchange

let prefix = "DAGaml.Cwap_exchange_utils"

module ToS =
struct
  open STools
  open ToS

  let input (input:input) : string =
    "{variables="^(list int input.variables)^
    "; parameters="^(list int input.parameters)^
    "; quants="^(list((list int)*bool) input.quants)^
    "; kdecomp="^(list(list int) input.kdecomp)^"}"

  let variable_tag (vt:variable_tag) : string =
    "{name="^(int vt.name)^
    "; level="^(int vt.level)^
    "; kind="^(option bool vt.kind)^
    "; index="^(list int vt.index)^"}"

  let component (oc:component) : string =
    "{index="^(int oc.index)^
    "; is_param="^(bool oc.is_param)^
    "; internal="^(list int oc.internal)^
    "; cfrp_exists="^(list int oc.cfrp_exists)^
    "; cfrp_forall="^(list int oc.cfrp_forall)^
    "; cfrp_params="^(list int oc.cfrp_params)^
    "; cbpp_forall="^(list int oc.cbpp_forall)^
    "; cbpp_params="^(list int oc.cbpp_params)^
    "; tree_pred="^(option int oc.tree_pred)^
    "; univ_pred="^(list int oc.univ_pred)^"}"

  let variables (va:variables) : string =
    array (option variable_tag) va

  let sequence (s:sequence) : string =
    array component s

  let output (output:output) : string =
    "{input="^(input output.input)^
    "; variables="^(variables output.variables)^
    "; sequence="^(sequence output.sequence)^
    "; cost="^(list int output.cost)^
    "; optimal="^(bool output.optimal)^"}"
end

module ToSTree =
struct
  open STools
  open ToSTree

  let input (input:input) : Tree.stree =
    quad (list int)
         (list int)
         (list((list int)*bool))
         (list(list int))
         (input.variables, input.parameters, input.quants, input.kdecomp  )

  let variable_tag (vt:variable_tag) : Tree.stree =
    quad int int (option bool) (list int)
      (vt.name, vt.level, vt.kind, vt.index)

  let component (oc:component) : Tree.stree =
    Tree.Node [
      int oc.index;
      bool oc.is_param;
      list int oc.internal;
      list int oc.cfrp_exists;
      list int oc.cfrp_forall;
      list int oc.cfrp_params;
      list int oc.cbpp_forall;
      list int oc.cbpp_params;
      option int oc.tree_pred;
      list int oc.univ_pred;
    ]

  let variables (va:variables) : Tree.stree =
    array (option variable_tag) va
  let sequence (ca:sequence) : Tree.stree =
    array component ca

  let output (output:output) : Tree.stree =
    Tree.Node [
      input output.input;
      variables output.variables;
      sequence output.sequence;
      list int output.cost;
      bool output.optimal;
    ]
end

module OfStree =
struct
  open STools
  open OfSTree

  let input (t:Tree.stree) : input =
    let variables, parameters, quants, kdecomp =
      quad (list int) (list int) (list((list int)*bool)) (list(list int)) t
    in
    {variables; parameters; quants; kdecomp}

  let variable_tag (t:Tree.stree) : variable_tag =
    let name, level, kind, index =
      quad int int (option bool) (list int) t
    in
    {name; level; kind; index}

  let component (t:Tree.stree) : component  =
    match t with
    | Tree.Node [
      index;
      is_param;
      internal;
      cfrp_exists;
      cfrp_forall;
      cfrp_params;
      cbpp_forall;
      cbpp_params;
      tree_pred;
      univ_pred;
    ] -> {
      index = int index;
      is_param = bool is_param;
      internal    = list int internal;
      cfrp_exists = list int cfrp_exists;
      cfrp_forall = list int cfrp_forall;
      cfrp_params = list int cfrp_params;
      cbpp_forall = list int cbpp_forall;
      cbpp_params = list int cbpp_params;
      tree_pred   = option int tree_pred;
      univ_pred   = list int univ_pred;
    }
    | _ -> assert false

  let variables (st:Tree.stree) : variables =
    array (option variable_tag) st
  let sequence (st:Tree.stree) : sequence =
    array component st

  let output (t:Tree.stree) : output =
    match t with
    | Tree.Node [
      t_input;
      t_variables;
      t_sequence;
      cost;
      optimal;
    ] -> {
      input = input t_input;
      variables = variables t_variables;
      sequence = sequence t_sequence;
      cost = list int cost;
      optimal = bool optimal;
    }
    | _ -> assert false
end

(* [MOVEME] *)
(* [check_quants v q]
    returns true iff components [q] form a partition of variables [v]
 *)
let rec check_quants (v:int list) (q:quants) : bool =
  match q with
  | [] -> v = []
  | (il, _)::q' -> (
    SetList.subset_of il v &&
    (check_quants (SetList.minus v il) q')
  )

let check_input (graph:input) : bool =
  (SetList.sorted_nat graph.variables) &&
  (SetList.sorted_nat graph.parameters) &&
  (SetList.nointer graph.variables graph.parameters) &&
  (check_quants graph.variables graph.quants) &&
  (List.for_all SetList.sorted_nat graph.kdecomp) &&
  (
    let nodes = SetList.union graph.variables graph.parameters in
    List.for_all (fun k -> SetList.subset_of k nodes) graph.kdecomp
  )

(* computes the `variables` field of `output`.
   leaves the `index` field default-initialized at [] *)
let variables_of_input (graph:input) : variables =
  let len =
    match MyList.opmax (SetList.union graph.variables graph.parameters) with
    | None -> 0
    | Some(m, _) -> succ m
  in
  let variables = Array.make len None in
  let _ =
    let level = List.length graph.quants in
    List.iter (fun name ->
      assert(variables.(name) = None);
      variables.(name) <- Some {name; level; kind = None; index = []}
    ) graph.parameters
  in
  List.iteri (fun level (il, k) ->
    List.iter (fun name ->
      assert(variables.(name) = None);
      let kind = Some k in
      variables.(name) <- Some {name; level; kind; index = []}
    ) il
  ) graph.quants;
  variables

(* [MOVEME] GuaCaml.MyList *)
let rec is_partition ?(forbidden=([]:'a list)) (ll:'a list list) : bool =
  match ll with
  | [] -> true
  | l::ll -> (
    (SetList.nointer l forbidden) &&
    (is_partition ~forbidden:(SetList.union l forbidden) ll)
  )

let check_component (i:int) (len:int) (oc:component) : bool =
  assert(0 <= i && i < len);
  (oc.index = i) &&
  (match oc.tree_pred with
  | None -> (
    oc.is_param = true &&
    oc.cfrp_exists = [] &&
    oc.cfrp_params = [] &&
    oc.cbpp_forall = [] &&
    oc.cbpp_params = []
  )
  | Some pred -> (
    oc.is_param = false &&
    (oc.index < pred && pred < len) &&
    (is_partition [oc.internal; oc.cfrp_exists; oc.cfrp_forall; oc.cfrp_params])&&
    (is_partition [oc.internal; oc.cfrp_exists; oc.cbpp_forall; oc.cbpp_params])&&
    (SetList.subset_of oc.univ_pred oc.cfrp_forall) &&
    (SetList.subset_of oc.cfrp_forall oc.cbpp_forall) &&
    (SetList.subset_of oc.cfrp_params oc.cbpp_params)
  ))

let cfrp_supp (c:component) : int list =
  SetList.union_list [c.internal; c.cfrp_exists; c.cfrp_forall; c.cfrp_params]

let cfrp_proj (c:component) : int list =
  SetList.union_list [c.cfrp_exists; c.cfrp_forall; c.cfrp_params]

let cbpp_supp (c:component) : int list =
  SetList.union_list [c.internal; c.cfrp_exists; c.cbpp_forall; c.cbpp_params]

let check_sequence_pred (seq:sequence) : bool =
  let len = Array.length seq in
  MyArray.for_alli
    (fun i c ->
      (check_component i len c) &&
      (match c.tree_pred with
       | None -> true
       | Some pred -> (
         let cp = seq.(pred) in
         (SetList.subset_of c.cfrp_exists (cfrp_supp cp)) &&
         (c.cfrp_forall = SetList.union c.univ_pred cp.cfrp_forall) &&
         (c.cbpp_forall = SetList.union c.univ_pred cp.cbpp_forall) &&
         (c.cbpp_params = cp.cbpp_params)
      ))
    ) seq

let check_output (output:output) : bool =
  (check_input output.input) &&
  (check_sequence_pred output.sequence)

let assert_input (graph:input) : unit =
       if not (SetList.sorted_nat graph.variables)
    then failwith "[Cwap_exchange_utils.assert_input] graph.variables is not sorted"
  else if not (SetList.sorted_nat graph.parameters)
    then failwith "[Cwap_exchange_utils.assert_input] graph.parameters is not sorted"
  else if not (SetList.nointer graph.variables graph.parameters)
    then failwith "[Cwap_exchange_utils.assert_input] graph.variables and graph.parameters have common variables"
  else if not (List.for_all SetList.sorted_nat graph.kdecomp)
    then failwith "[Cwap_exchange_utils.assert_input] graph.kdecomp elements are not sorted"
  else (
    let nodes = SetList.union graph.variables graph.parameters in
    if not (List.for_all (fun k -> SetList.subset_of k nodes) graph.kdecomp)
    then failwith "[Cwap_exchange_utils.assert_input] graph.kdecomp elements are not included in available nodes"
    else ()
  )

let assert_sequence_pred (seq:sequence) : unit =
  let len = Array.length seq in
  Array.iteri
    (fun i c ->
      assert (check_component i len c);
      match c.tree_pred with
       | None -> ()
       | Some pred -> (
         let cp = seq.(pred) in
         assert(SetList.subset_of c.cfrp_exists (cfrp_supp cp));
         if not (SetList.subset_of c.cfrp_forall (SetList.union c.univ_pred cp.cfrp_forall))
         then (
           let open STools in
           print_endline ("cfrp_forall:"^(ToS.(list int) c.cfrp_forall));
           print_endline ("univ_pred:"^(ToS.(list int) c.univ_pred));
           print_endline ("pred.cfrp_forall"^(ToS.(list int) cp.cfrp_forall));
           failwith "[DAGaml.Cfrp.assert_sequence_pred] not(cfrp_forall is no subset of (univ_pred union pred.cfrp_forall)"
         );
         assert(c.cbpp_forall = SetList.union c.univ_pred cp.cbpp_forall);
         assert(c.cbpp_params = cp.cbpp_params);
      )
    ) seq

let assert_output (output:output) : unit =
  assert_input output.input;
  assert_sequence_pred output.sequence;
  ()

let list_sort (l:int list) : int list = SetList.sort l

(* [MOVEME] *)
(* merges consecutive quantification blocks (and removes empty ones) *)
let normalize_quants (quants:quants) : quants =
  let quants = MyList.opmap
    (fun (ql, qb) -> if ql = [] then None else (Some(list_sort ql, qb)))
    quants
  in
  let rec nq_rec0 carry ql qb : quants -> quants = function
    | [] -> List.rev ((ql, qb)::carry)
    | (ql', qb')::quants -> (
      assert(ql' <> []);
      if qb' = qb
        then nq_rec0 carry (SetList.union ql' ql) qb quants
        else nq_rec0 ((ql, qb)::carry) ql' qb' quants
    )
  in
  match quants with
  | [] -> []
  | (ql, qb)::quants -> (
    assert(ql <> []);
    nq_rec0 [] ql qb quants
  )

(* sort lists *)
let normalize_input (input:input) : input =
  {
    variables = list_sort input.variables;
    parameters = list_sort input.parameters;
    quants = normalize_quants input.quants;
    kdecomp = List.map list_sort input.kdecomp;
  }

let var_get (a:'a option array) (i:int) : 'a =
  match a.(i) with
  | None -> failwith "[Cwap_exchange_utils.var_get] illegal variable access"
  | Some v -> v

let var_set (a:'a option array) (i:int) (x:'a) : unit =
  a.(i) <- Some x

let min_exist_level (set:variable_tag list) : int option =
  match MyList.opmin (MyList.opmap (fun v -> match v.kind with Some true -> Some v.level |  _ -> None) set) with
  | None -> None
  | Some(_, lvl) -> Some lvl

let forall_name_higher_than_level ?(upper=None) (set:variable_tag list) : int list =
  match upper with
  | None -> (
    (* no remaining existential variables *)
    MyList.opmap (fun v -> if v.kind = None then Some v.name else None) set
  )
  | Some upper -> (
    MyList.opmap (fun v -> if v.kind = Some false && v.level < upper then None else Some v.name) set
  )

let forall_name_lower_than_level ?(upper=None) (set:variable_tag list) : int list =
  match upper with
  | None -> (
    (* no remaining existential variables *)
    MyList.opmap (fun v -> if v.kind = Some false then Some v.name else None) set
  )
  | Some upper -> (
    MyList.opmap (fun v -> if v.kind = Some false && v.level < upper then Some v.name else None) set
  )

let univ_proj (va:variables) (set:int list) : int list =
  if set = [] then [] else (
    let kv = set ||> (fun i -> var_get va i) in
    let upper = min_exist_level kv in
    forall_name_lower_than_level ~upper kv
  )

(* universal preprocessing, ensures that all components have an existential leading variable *)
let universal_preprocessing (kd:kdecomp) (va:variables) : kdecomp =
  kd |> MyList.opmap
    (fun k ->
      if k = [] then None else (
        let kv = k ||> (fun i -> var_get va i) in
        let upper = min_exist_level kv in
        let k' = forall_name_higher_than_level ~upper kv in
        if k' = [] then None else Some k'
      )
    )

(* [MOVEME] *)
(* [abstract_S_E kdecomp internal = (support, interface, kdecomp')] *)
let abstract_S_E
    (kdecomp:kdecomp)
    (internal:int list) : int list * int list * kdecomp =
  (* ng = neighborhood
     nng = no neighborhood *)
  let ng, nng = MyList.partition (SetList.nointer internal) kdecomp in
  (* flat neighborhood *)
  let support = SetList.union_list (internal :: ng) in
  (* strict neighborhood *)
  let interface = SetList.minus support internal in
  let kdecomp' = if interface = [] then nng else (interface :: nng) in
  (support, interface, kdecomp')

let var_get_kind va i : bool option = (var_get va i).kind

(* [variable_partition_kind va set = (cfrp_exists, cfrp_forall, cfrp_params)] *)
let variable_partition_kind (va:variables) (set:int list) : int list * int list * int list =
  let cfrp_exists = List.filter (fun i -> var_get_kind va i = Some true ) set in
  let cfrp_forall = List.filter (fun i -> var_get_kind va i = Some false) set in
  let cfrp_params = List.filter (fun i -> var_get_kind va i = None      ) set in
  (cfrp_exists, cfrp_forall, cfrp_params)

(* performs abstract existential elimination *)
let operator_S_E
    (input:input)
    (va:variables)
    (internal:int list) : component * input =
  let internal = list_sort internal in
  match input.quants with
  | [] -> assert false
  | (ql, qb)::qt -> (
    assert(ql <> []);
    assert(qb = true);
    assert(SetList.subset_of internal ql);
    let variables = SetList.minus input.variables internal in
    let parameters = input.parameters in
    let ql' = SetList.minus ql internal in
    let quants = if ql' = [] then qt else (ql', qb)::qt in
    let support, interface, kdecomp = abstract_S_E input.kdecomp internal in
    let input = {variables; parameters; quants; kdecomp} in
    let cfrp_exists, cfrp_forall, cfrp_params = variable_partition_kind va interface in
    let component = {
      index = -1;
      is_param = false;
      internal;
      cfrp_exists;
      cfrp_forall;
      cfrp_params;
      cbpp_forall = [];
      cbpp_params = [];
      tree_pred = None;
      univ_pred = [];
    } in
    (component, input)
  )

(* performs abstract universal elimination *)
let operator_S_U (input:input) (vl:int list) : input =
  let internal = list_sort vl in
  match input.quants with
  | [] -> assert false
  | (ql, qb)::qt -> (
    assert(ql <> []);
    assert(qb = false);
    assert(SetList.subset_of internal ql);
    let variables = SetList.minus input.variables internal in
    let parameters = input.parameters in
    let ql' = SetList.minus ql internal in
    let quants = if ql' = [] then qt else (ql', qb)::qt in
    let support, interface, kdecomp = abstract_S_E input.kdecomp internal in
    {variables; parameters; quants; kdecomp}
  )

let var_set_single_index (va:variables) (i:int) (index:int) : unit =
  let v = var_get va i in
  var_set va i {v with index = [index]}

let var_get_single_index (va:variables) (i:int) : int =
  let v = var_get va i in
  match v.index with
  | [index] -> index
  | _ -> failwith "[Cwap_exchange_utils.var_get_single_index] unexpected number of indexes"

(* [compute_variable_index_E] (output:output) : unit]
   properly initialized the field `index` of each existential variable tag
   requires : `\forall i, sequence.(i).internal` is properly initialized
 *)
let compute_variable_index_E (output:output) : unit =
  let va = output.variables in
  Array.iteri
    (fun index c ->
      List.iter (fun i -> var_set_single_index va i index) c.internal
    ) output.sequence;
  ()

(* [compute_variable_index_U] (output:output) : unit]
   properly initialized the field `index` of each universal variable tag
   requires : `\forall i, sequence.(i).univ_pred` is properly initialized
 *)
let compute_variable_index_U (output:output) : unit =
  let v = output.variables in
  Array.iteri (fun index c ->
    if c.is_param = false
    then (
      List.iter
        (fun i ->
          let vt = var_get v i in
          assert(vt.kind = Some false);
          let index = SetList.union vt.index [i] in
          v.(i) <- Some {vt with index}
        )
        c.univ_pred
    )) output.sequence;
  ()

let min_index (va:variables) (set:int list) : int option =
  let rec min_index_rec va opmin_index : int list -> int option =
    function
    | [] -> opmin_index
    | i::set -> (
      let vi = var_get va i in
      if vi.kind = Some false (* i.e., vi.kind = Universal *)
      then min_index_rec va opmin_index set (* ignore current *)
      else min_index_rec va (Tools.opmin (vi.index |> List.hd) opmin_index) set
    )
  in
  min_index_rec va None set

let update_seq (output:output) ?(no_param=false) (f:int -> component -> component) : unit =
  let seq = output.sequence in
  if no_param
  then (Array.iteri (fun i c -> if not c.is_param then seq.(i) <- (f i c)) seq)
  else (Array.iteri (fun i c ->                        seq.(i) <- (f i c)) seq)

(* [compute_tree_pred (output:output) : unit]
   properly initializes the `tree_pred` field
   requires : `\forall v, v.kind = Some true -> v.index` is properly initialized
 *)
let compute_tree_pred (output:output) : unit =
  let va = output.variables in
  let len = Array.length output.sequence in
  update_seq output ~no_param:true
    (fun i c ->
      let tree_pred = Tools.unop_default (len-1) (min_index va c.cfrp_exists) in
      {c with tree_pred = Some tree_pred}
    )

let assert_tree_pred (output:output) : unit =
  let seq = output.sequence in
  let len = Array.length seq in
  Array.iteri (fun i ci ->
    if ci.is_param
    then assert(ci.tree_pred = None)
    else (
      assert(ci.tree_pred <> None);
      let j = Tools.unop ci.tree_pred in
      assert(0 <= j && j < len);
    )
  ) seq;
  ()

let var_get_level va i : int = (var_get va i).level

let min_level (va:variables) (set:int list) : int option =
  match MyList.opmin (set ||>  var_get_level va) with
  | None -> None
  | Some(_, i) -> Some i

let name_lower_than_level (va:variables) ?(upper=None) (set:int list) : int list =
  match upper with
  | None -> set
  | Some upper -> (List.filter (fun i -> var_get_level va i < upper) set)

(* [compute_univ_pred (output:output) : unit]
   properly initializes the `univ_pred` field.
   requires : `\forall v, v.kind = Some true -> v.index` is properly initialized
 *)
let compute_univ_pred (output:output) : unit =
  let va = output.variables in
  update_seq output ~no_param:true
    (fun i c ->
      (* 1. compute lvl1 = min level for neighbor existential variables *)
      let upper : int option = min_level va c.cfrp_exists in
      (* 2. compute universal variables with lower rank than [upper] *)
      let univ_pred : int list = name_lower_than_level va ~upper c.cfrp_forall in
      {c with univ_pred}
    )

(* properly initializes fields `cbpp_forall` and `cbpp_params` *)
let compute_cbpp (output:output) : unit =
  let seq = output.sequence in
  let len = Array.length seq in
  for i = len - 1 downto 0
  do
    assert(seq.(i).index = i);
    let c = seq.(i) in
    if c.is_param
    then (
      assert(c.tree_pred = None);
      assert(c.univ_pred = []);
      ()
    )
    else (
      assert(c.tree_pred <> None);
      let pred_index = Tools.unop c.tree_pred in
      let pred = seq.(pred_index) in
      let cbpp_forall = SetList.union pred.cbpp_forall c.univ_pred in
      let cbpp_params = pred.cbpp_params in
      if not (SetList.subset_of c.cfrp_forall cbpp_forall)
      then (failwith "[DAGaml.Cfrp.compute_cbpp] cfrp_forall is no subset of cbpp_forall");
      if not (SetList.subset_of c.cfrp_params cbpp_params)
      then (failwith "[DAGaml.Cfrp.compute_cbpp] cfrp_params is no subset of cbpp_params");
      seq.(i) <- {c with cbpp_forall; cbpp_params}
    )
  done

(* [compute_back_dependencies (output:output) : unit]
   properly initialize the following fields :
     - cbpp_forall
     - cbpp_params
     - tree_pred
     - univ_pred
   Time Complexity O(nk) where
     - n = 'number of vertices' and
     - k = 'suppressing sequence size'
 *)
let compute_back_dependancies (output:output) : unit =
  (* compute `index` field in `variable_tag` for existential variables *)
  compute_variable_index_E output;
  (* compute `tree_pred` field in `component` *)
  compute_tree_pred output;
  assert_tree_pred output;
  (* compute `univ_pred` field in `compoennt` *)
  compute_univ_pred output;
  (* compute `index` field in `variable_tag` for universal variables *)
  compute_variable_index_U output;
  (* remaining fields `cbpp_forall` and `cbpp_params` *)
  compute_cbpp output;
  (* checks conformity *)
  assert_output output;
  ()

(* [operator_SSO input vll] takes :
    - an [input] problem description and
    - a supressing sequence [vll]
    and returns
    - an annotated suppressing sequence [output]
 *)
let operator_SSO ?(verbose=false) ?(optimal=false) (input:input) (vll:quants) : output =
  let prefix = "["^prefix^".operator_SS0]" in
  let variables = variables_of_input input in
  let k, outl, cost, inp = List.fold_left
    (fun (i, out, cost, inp) (ql, qb) ->
      if qb
      then ((* existential elimination *)
        let out', inp' = operator_S_E inp variables ql in
        let out' = {out' with index = i} in
        let cost' = List.(length out'.internal +
                          length out'.cfrp_exists +
                          length out'.cfrp_forall +
                          length out'.cfrp_params) in
        (succ i, out'::out, cost'::cost, inp')
      )
      else ((* universal elimination *)
        let inp' = operator_S_U inp ql in
        (i, out, cost, inp')
      )
    )
    (0, [], [], input) vll
  in
  if List.length outl <> k
  then (invalid_arg (prefix^" internal inconsistency, please contact support"));
  if inp.variables <> []
  then (invalid_arg (prefix^" some variable do not appear in the given sequence"));
  if inp.parameters <> input.parameters
  then (invalid_arg (prefix^" parameter inconsistency"));
  (* create an extra component for parameter variables *)
  let param_comp = {
    index = k;
    is_param = true;
    internal = input.parameters;
    cfrp_exists = [];
    cfrp_forall = [];
    cfrp_params = [];
    cbpp_forall = [];
    cbpp_params = [];
    tree_pred = None;
    univ_pred = [];
  } in
  if verbose then print_endline ("[operator_SSO] cost(not normalized):"^(STools.ToS.(list int)(List.rev cost)));
  let output = {
    input;
    variables;
    sequence = MyArray.of_rev_list (param_comp::outl);
    cost = Bicimal.normalize (List.rev cost);
    optimal;
  } in
  (* print_endline ("[Cwap_exchange_utils.operator_SSO] output:"^(ToS.output output)); *)
  compute_back_dependancies output;
  (* print_endline ("[Cwap_exchange_utils.operator_SSO] output:"^(ToS.output output)); *)
  (* assert_output output; *)
  output (* |> Tools.check check_output *)

open Snowflake

let abstract_SS_E (kd:kdecomp) (ss:int list list) : int list * kdecomp =
  List.fold_left (fun (cs, kd) s ->
    let supp, _, kd' = abstract_S_E kd s in
    let cs' = [List.length supp] in
(*  print_endline ("[DAGaml.Cwap_exchange_utils.abstract_SS_E] cs:"^STools.ToS.(list int cs));
    print_endline ("[DAGaml.Cwap_exchange_utils.abstract_SS_E] kd:"^STools.ToS.(list(list int) kd));
    print_endline ("[DAGaml.Cwap_exchange_utils.abstract_SS_E] cs':"^STools.ToS.(list int cs'));
    print_endline ("[DAGaml.Cwap_exchange_utils.abstract_SS_E] kd':"^STools.ToS.(list(list int) kd')); *)
    (Bicimal.add cs cs', kd')) ([], kd) ss

let first_wap_input
    (layer0:int list)
    (variables0:int list)
    (parameters0:int list)
    (quants:quants)
    (kdecomp:kdecomp) : Wap_exchange.input =
  let variables = layer0 in
  let other_variables = SetList.minus variables0 layer0 in
  let parameters = SetList.union other_variables parameters0 in
  Wap_exchange.{variables; parameters; kdecomp}

let meta_proto_solver
    (wap_solver:Wap_exchange.proto_solver)
    (input:input) : quants * int list =
  let rec mps_rec (carry:quants) (cost:int list) (input:input) : quants * int list =
    match input.quants with
    | [] -> (List.rev carry, cost)
    | (ql, qb)::quants -> (
      if qb
      then ( (* existential layer *)
        let wap_input =
          first_wap_input ql input.variables input.parameters quants input.kdecomp
        in
        let ss0, cost0 = wap_solver wap_input in
        let cost1, kdecomp = abstract_SS_E input.kdecomp ss0 in
        if cost0 <> cost1
        then (
          print_endline ("ss0:"^STools.ToS.(list(list int)ss0));
          print_endline ("cost0:"^STools.ToS.(list int cost0));
          print_endline ("cost1:"^STools.ToS.(list int cost1));
          failwith "[DAGaml.meta_proto_solver] cost0 <> cost1";
        );
        let input = {
          variables = SetList.minus input.variables ql;
          parameters = input.parameters;
          quants;
          kdecomp; (* result of the application of [ss0] to [input.kdecomp] *)
        } in
        let carry' = List.rev_append (ss0 ||> (fun s -> (s, true))) carry in
        mps_rec carry' (Bicimal.add cost cost0) input
      )
      else ( (* universal layer *)
        mps_rec ((ql, false)::carry) cost (operator_S_U input ql)
      )
    )
  in
  mps_rec [] [] input
