open GuaCaml
open CnfTypes

let dpll_naive_pick (maxi:int) (f:formule) : int =
  Dpll.dpll_naive_pick maxi f

let dpll_maxvarimpact_pick (maxi:int) (f:formule) : int =
  Dpll.dpll_maxvarimpact_pick maxi f

type pick =
  | MinVar
  | MaxVarImpact

let pick = ref MaxVarImpact

let dpll_pick maxi f =
  match !pick with
  | MinVar -> dpll_naive_pick maxi f
  | MaxVarImpact -> dpll_maxvarimpact_pick maxi f

let rec dpll_naive_rec ?(verbose=false) (depth:int) (maxi:int) (f:formule) : TraceTypes.t =
(*  print_endline ((String.make depth ' ')^"[0]f:"^(CnfUtils.ToS.formule f)); *)
  match CnfUtils.simplify f with
  | Error None -> TraceTypes.cst false
  | Error(Some units) -> TraceTypes.units units
  | Ok(units, f) -> (
    if verbose then print_endline ((String.make depth ' ')^"units:"^(CnfUtils.ToS.clause units));
    (* print_endline ((String.make depth ' ')^"[1]f:"^(CnfUtils.ToS.formule f)); *)
    let v : int = dpll_pick maxi f in
    if verbose then print_endline ((String.make depth ' ')^"v:"^(string_of_int v));
    let f0 = dpll_naive_rec ~verbose (succ depth) maxi ([(v, true )]::f) in (* v = 1 *)
    let f1 = dpll_naive_rec ~verbose (succ depth) maxi ([(v, false)]::f) in (* not v = 1 *)
    TraceTypes.andl ~u:units [TraceTypes.sha v f0 f1]
  )

(* [dpll_naive file = res] where
 * match res with
 * | Some value -> a satisfying assignment
 * | None       -> the input formula is unsatisfiable
 *)
let dpll_naive ?(verbose=false) (file:CnfTypes.system) : TraceTypes.file =
  assert(file.quants = []);
  let input = file.CSTypes.input in
  let quants = file.CSTypes.quants in
  let formule = dpll_naive_rec ~verbose 0 file.CSTypes.input file.CSTypes.formule in
  TraceTypes.{input; quants; formule}

(* [MOVEME]
let test : formule = [[(12, false)]; [(12, false); (17, false)]; [(16, true); (19, false)]; [(17, true); (19, false)]; [(12, true); (13, true)]; [(13, false); (15, true); (19, false)]; [(13, true); (14, true); (19, false)]; [(13, false); (19, false)]; [(17, true); (18, false)]; [(13, false); (17, true)]; [(12, false); (16, true)]]

let _ =
  print_endline ("test:"^(CnfUtils.ToS.formule test));
  assert(CnfUtils.simplify test = Error(Some [(12, false); (13, true); (17, true); (19, false)]));
*)
