(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_exchange : describes the interface formats between CoSTreD and WAP
*)

type quants = (int list * bool) list
type kdecomp = int list list


type input = {
  variables : int list; (* set of suppressibles vertices *)
  parameters: int list; (* set of unsuppressible vertices *)
  quants    : quants; (* quantification levels, the first element is the inner most level *)
  (* false = universal, true = existential *)
  kdecomp   : kdecomp; (* describes the graphs as a sum of clique (each [int list] element) *)
}
(* typically if your system is described as a set of equation
   the primal graph of your system can be described by adding
   a clique for each equation containing the variables appearing in it.
   For example the following pseudo-code computes a valid primal graph :
   [List.map (fun equation -> 'list of variables appearing in' equation) equations]
 *)

type proto_solver = input -> quants * int list

type variable_tag = {
  name  : int;
  level : int; (* at which quantification level, this variable is removed *)
  kind  : bool option; (* Some false = universal, Some true = existential, None = parameter *)
  index : int list; (* in which component(s) this variable is eliminated:
    - existential : length = exactly one
    - universal   : length = any non-negative integer
    - parameter   : length = exactly one *)
}

type component = {
  index       : int;        (* index of the current component *)
  is_param    : bool;       (* [is_param = true ] this is a parameter type component
                               [is_param = false] this is a variable  type component *)
  internal    : int list;   (* list of variable to be eliminated during CFRP *)
  cfrp_exists : int list;   (* existential variables in the strict neighborhood during CFRP *)
  cfrp_forall : int list;   (* universal   variables in the strict neighborhood during CFRP *)
  cfrp_params : int list;   (* parameter   variables in the strict neighborhood during CFRP *)
(*cbpp_exist = cfrp_exists; (* existential variables in the strict neighborhood during CBPP, _a priori_ same as `cfrp_exists` *) *)
  cbpp_forall : int list;   (* universal   variables in the strict neighborhood during CBPP, _a priori_ supersets `cfrp_forall` *)
  cbpp_params : int list;   (* parameter   variables in the struct neighborhood during CBPP, _a priori_ supersets `cfrp_params` *)
  tree_pred   : int option; (* parent tree-node/component if any *)
  univ_pred   : int list;   (* list of variable to be universally quantified, before sending the transformed conjunct *)
}

(* [TODO] UPDATE COMMENTS *)
(* remark: relational set properties between fields

  support = internal \sqcup interface
  internal \subset input.variables
  interface = var_interface \sqcup param_interface (* \sqcup = disjoint union *)
  var_interface = interface \union input.variables
  param_interface = interface \union input.parameters
  tree_pred = Some j -> j > index
 *)

type variables = variable_tag option array
  (* [variables] : describes the affiliation of each variable, to a given elimination level, and elimination index *)
type sequence = component array
  (* [sequence] : describes the suppression sequence with relevant annotations *)

type output = {
  input : input;
  (* [input] : pointer to the input object *)
  variables : variables;
  (* [variables] : relevant annotation for each variable, (name, kind, level, index) *)
  sequence : sequence;
  (* [sequence] : describes the suppression sequence with relevant annotations *)
  cost : int list;
  (* [cost] represents the WAP-cost of the [sequence]
     [cost] is represented using [bicimal] representation type *)
  optimal : bool;
  (* [optimal] is [true] if the sequence is proved optimal, [false] otherwise *)
  (* in particular [optimal = false] does not imply that the sequence is suboptimal,
     only that one could not prove it (e.g. time/memory restriction) *)
}

type solver = input -> output
