(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_lightweight :
 *   Simpl heuristic for WAP, which iteratively select a vertex with lightest neighborhood then
 *   apply H-reduction (true-twin merging).
*)

open GuaCaml
open Extra
open STools
open BTools
open GGLA_HFT.Type
open GraphGenLA.Type

(* [Snow.Wap_lightweight]
val opmin : (BNat.nat * 'a)option -> (BNat.nat*'a) -> (BNat.nat * 'a) option
val find_best : hg -> (BNat.nat * int) option
val lightweight_find_lopt : hg -> BNat.nat * int list list
val lightweight_greedy : ?verbose:bool -> Wap_exchange.input -> Wap_exchange.output
 *)
let prefix = "DAGaml.Cwap_lightweight"

open Snowflake

let lightweight_wap_proto_solver ?(remove_useless=false) : Wap_exchange.proto_solver = fun input ->
  assert(Wap_exchange_utils.check_input input);
  let kd = Wap_exchange_utils.to_GraphKD input in
  let hg = GGLA_HFT.of_GraphKD ~remove_useless kd in
  let bnat_cost, seq = Wap_lightweight.lightweight_find_lopt hg in
  (seq, BNat.to_bicimal bnat_cost)

let lightweight_proto_solver : Cwap_exchange.proto_solver = fun input ->
  Cwap_exchange_utils.meta_proto_solver lightweight_wap_proto_solver input

let lightweight_solver ?(verbose=false) : Cwap_exchange.solver = fun input ->
  let quants, cost = lightweight_proto_solver input in
  (if verbose then print_endline ("[DAGaml.Cwap_lightweight.lightweight_solver] cost:"^(STools.ToS.(list int) cost)));
  let output = Cwap_exchange_utils.operator_SSO ~verbose input quants in
  assert(output.cost = cost);
  output

(* [Snow.Wap_lightspeed]
val opmin : (BNat.nat * 'a)option -> (BNat.nat*'a) -> (BNat.nat * 'a) option
val find_best : hg -> (BNat.nat * int) option
val lightspeed_find_lopt : hg -> BNat.nat * int list list
val lightspeed_greedy : ?verbose:bool -> Wap_exchange.input -> Wap_exchange.output
 *)

let lightspeed_wap_proto_solver ?(verbose=false) ?(remove_useless=false) : Wap_exchange.proto_solver = fun input ->
  assert(Wap_exchange_utils.check_input input);
  let kd = Wap_exchange_utils.to_GraphKD input in
  let hg = GGLA_HFT.of_GraphKD ~remove_useless kd in
  let bnat_cost, seq = Wap_lightspeed.lightspeed_lopt ~verbose hg in
  (seq, BNat.to_bicimal bnat_cost)

let lightspeed_proto_solver ?(verbose=false) : Cwap_exchange.proto_solver = fun input ->
  Cwap_exchange_utils.meta_proto_solver (lightspeed_wap_proto_solver ~verbose) input

let lightspeed_solver ?(verbose=false) : Cwap_exchange.solver = fun input ->
  let quants, cost = lightspeed_proto_solver input in
  (if verbose then print_endline ("[DAGaml.Cwap_lightweight.lightspeed_solver] cost:"^(STools.ToS.(list int) cost)));
  let output = Cwap_exchange_utils.operator_SSO ~verbose input quants in
  assert(output.cost = cost);
  output
