open GuaCaml

type t =
  | Cst   of bool
  | And   of (int * bool) list * (t list) (* all conjuncts have disjoint supports *)
  | Sha   of int * t * t (* (v, t0, t1) *)

type file = {
  input  : int;
  quants : Constraint_system_types.quants;
    (* [int list] is sorted by increasing order
       set of variables *)
    (* inner quantifiers are the last ones in quants
       quants_0 are the top-level quantifier *)
  formule : t;
}

module ToS =
struct
  open STools
  include ToS

  let term = pair int bool
  let rec formule = function
    | Cst b -> "Cst "^(bool b)
    | And (u, tl) -> "And "^(pair (list term) (list formule) (u, tl))
    | Sha (v, f0, f1) -> "Sha "^(trio int formule formule (v, f0, f1))

  let file file =
    "{ input="^(int file.input)^
    "; quants="^(CnfTypes.ToS.quants file.quants)^
    "; formule="^(formule file.formule)^"}"
end

let cst b = Cst b
let units u =
  if u = [] then Cst true else And(u, [])
let andl ?(u=[]) tl =
  match u, tl with
  | [], []  -> Cst true
  | [], [f] -> f
  | u , [f] -> (
    match f with
    | Cst false -> Cst false
    | Cst true  -> units u
    | And(u', fl') -> (
      match CnfUtils.clause_addl u u' with
      | []  -> Cst false
      | u'' -> And(u'', fl')
    )
    | _ -> And(u, tl)
  )
  | u , tl  -> And(u, tl)
let sha v f0 f1 =
  match f0, f1 with
  | Cst b0, Cst b1 -> if b0 = b1 then Cst b0 else And([v, b0],[])
  | Cst false, f1 -> andl ~u:[v, false] [f1]
  | f0, Cst false -> andl ~u:[v, true ] [f0]
  | f0, f1 -> Sha(v, f0, f1)

