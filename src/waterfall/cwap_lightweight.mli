(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CoSTreD and MLBDD.
 *
 * Wap_lightweight :
 *   Simpl heuristic for WAP, which iteratively select a vertex with lightest neighborhood then
 *   apply H-reduction (true-twin merging).
*)

open GuaCaml
open BTools
open GGLA_HFT.Type

open Snowflake

val lightweight_wap_proto_solver : ?remove_useless:bool -> Wap_exchange.proto_solver
val lightspeed_wap_proto_solver : ?verbose:bool -> ?remove_useless:bool -> Wap_exchange.proto_solver

val lightweight_proto_solver : Cwap_exchange.proto_solver
val lightspeed_proto_solver : ?verbose:bool -> Cwap_exchange.proto_solver

val lightweight_solver : ?verbose:bool -> Cwap_exchange.solver
val lightspeed_solver : ?verbose:bool -> Cwap_exchange.solver
