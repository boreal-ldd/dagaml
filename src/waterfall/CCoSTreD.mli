(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CCoSTreD and MLBDD.
 *
 * CCoSTreD :
 *   Functor Implementation of the CCoSTreD (Cascading CoSTreD) algorithms :
 *     CFRP : Cascading Forward Reduction Process
 *     CBPP : Cascading Backward Propagation Process
*)

open GuaCaml
open Extra
open STools

type supp = int list

(* Section 1. Forward Reduction Process *)

module type MSigCFRP =
sig
  type t (* representation language manager *)
  type f0 (* input representation language *)
  type f1 (* intermediary representation language *)
  type f2 (* output representation language *)

  (* [support t f] returns a sorted (super)set of the variables on which the function [f] depends *)
  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp

  val elim_exists : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> f1 * f2
  (* [FIXME] fix comments *)
  (* [elim_exists t ga f0l f1l supp elim proj = (projected, coprojected)] where :
      - \forall i, fl_i \subset supp
      - elim \subset supp
      - proj = supp - elim
      - [vl] \subset NS
      - [proj] is the projection of the conjunction fl on vl complement
      - [proj] is the result of existentially eliminating [elim] variables
      - [coproj] is the coprojection relatively to this projection operator
      - [fl] <> []
      - [n] : global arity
   *)
  (* [solve] guarentees that :
      - [support coproj] \subset NS
      - [support proj] \subset NS \setminus [vl]
      - [solve t [] vl = true * true]
   *)
  (* the boolean/exists case
     [solver t fl vl = (proj, coproj)] with
      coproj = /\_i vl_i
      proj = \exists vl. coproj
   *)

  val elim_forall0 : t -> int -> f0 -> supp -> supp -> supp -> f0 (* [LATER] change for (f0, f1) AB.ab *)
  val elim_forall1 : t -> int -> f1 -> supp -> supp -> supp -> f1
  (* [elim_forall t i f supp elim proj = projected]
     where :
     - [projected] is the projected of [f] (which support is included in [supp]) with respect to [elim]
     - [projected]' support is included in [proj]
   *)

  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  (* [trivial t f = Some true ] :
        the constraint [f] is not relevant
        (e.g. (fun _ -> true) in the QBF case)
     [trivial t f = Some false] :
        the constraint [f] is contradictory
        (i.e. any system containing it has no solution)
        (e.g. (fun _ -> false) in the QBF case)
     [trivial t f = None      ] : the contraints [f] is not trivial
        (e.g. any non-constant function in the QBF case)
   *)

end

module type SigCFRP =
sig
  module M : MSigCFRP

  type cfrp_state = {
    cfrp_f0a : M.f0 list array;
    cfrp_f1a : M.f1 list array;
    cfrp_f2a : M.f2 option array;
  }

  (*  [apply t n fl wap_out = opa] where
        - [n] is the global arity of the problem
        - [opa = None] was proved to have no solution
        - [opa = Some a] has
   *)
  val apply : M.t -> ?check:bool -> int -> Cwap_exchange.output -> M.f0 list -> M.f1 list-> cfrp_state option
  (* Forward Reduction Process *)
end

module MakeCFRP(M0:MSigCFRP) : SigCFRP
  with type M.t  = M0.t
  and  type M.f0  = M0.f0
  and  type M.f1  = M0.f1
  and  type M.f2  = M0.f2

(* Section 2. Backward Propagation Process *)
module type MSigCBPP =
sig
  type t (* representation language manager *)
  type f2 (* input representation language of functions *)
  type f3 (* output representation language of functions *)

  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp
  (*  [support t f = il] where
        [il] is the sorted (increasing order) (super)set of variables on which
        the function [f] depends
   *)
  (* val backsolve : t -> int -> (f * supp) -> f' *)
  (*  [backsolver t n (f, sf) = f']
        maps parameter variables to the output language
   *)

  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> supp -> f3
  (*  [backproj t n f3 f2 supp3 introU elimE supp2 = f3']
        add [introU] useless variables to [f3] then project it according to [elimE]
        and combine with [f2]
        - supp(f3) \subset supp3
        - supp(f2) \subset supp2
   *)

end

module type SigCBPP =
sig
  module M : MSigCBPP

  type cbpp_state = {
    cbpp_f2a : M.f2 option array;
    cbpp_f3a : M.f3 option array;
  }

  val apply : M.t -> ?check:bool -> int -> Cwap_exchange.output -> cbpp_state -> M.f3 array
  (* Backward Propagation Process *)
end

module MakeCBPP(M0:MSigCBPP) : SigCBPP
  with type M.t  = M0.t
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3

(* Section 3. Full Reduction (CCoSTreD) *)
module type MSig =
sig
  type t (* representation language (RL) manager *)
  type f0 (* input RL *)
  type f1 (* pre-CFRP  intermediary RL *)
  type f2 (* post-CFRP intermediary RL *)
  type f3 (* post-CBPP RL *)

  (* support *)
  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp

  (* trivial *)
  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  val trivial3 : t -> f3 -> bool option

  (* Section 1. CFRP *)
  (* [elim_exists t n f0l f1l supp elim proj = (projected, coprojected)] *)
  val elim_exists : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> f1 * f2
  (* [elim_forall t i f supp elim proj = projected] *)
  val elim_forall0 : t -> int -> f0 -> supp -> supp -> supp -> f0 (* [LATER] change for (f0, f1) AB.ab *)
  val elim_forall1 : t -> int -> f1 -> supp -> supp -> supp -> f1

  (* Section 2.0 CFRP to CBPP *)
  val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3

  (* Section 2. CBPP *)
  (*  [backproj t n f3 f2 supp3 introU elimE supp2 = f3'] *)
  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> supp -> f3
end

module type Sig =
sig
  module M : MSig

  module CFRP : SigCFRP
    with type M.t  = M.t
    and  type M.f0  = M.f0
    and  type M.f1  = M.f1
    and  type M.f2  = M.f2

  module CBPP : SigCBPP
    with type M.t  = M.t
    and  type M.f2  = M.f2
    and  type M.f3  = M.f3

  (* Cascading Forward Reduction Process *)
  val apply_cfrp : M.t -> ?check:bool -> int -> Cwap_exchange.output -> M.f0 list -> M.f1 list-> CFRP.cfrp_state option

  val cbpp_of_cfrp : M.t -> ?check:bool -> int -> Cwap_exchange.output -> CFRP.cfrp_state -> CBPP.cbpp_state

  (* Cascading Backward Propagation Process *)
  val apply_cbpp : M.t -> ?check:bool -> int -> Cwap_exchange.output -> CBPP.cbpp_state -> M.f3 array

  type state = {
    f0a : M.f0 list array;
    f1a : M.f1 list array;
    f2a : M.f2 option array;
    f3a : M.f3 array;
  }

  (* Full Reduction Process *)
  val apply : M.t -> ?check:bool -> int -> Cwap_exchange.output -> M.f0 list -> M.f1 list -> state option
end

module Make(M0:MSig) : Sig
  with type M.t  = M0.t
  and  type M.f0 = M0.f0
  and  type M.f1 = M0.f1
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3
