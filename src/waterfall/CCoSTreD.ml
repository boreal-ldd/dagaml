(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * Snowflake : A Generic Symbolic Dynamic Programming framework interfacing WAP, CCoSTreD and MLBDD.
 *
 * CCoSTreD :
 *   Functor Implementation of the CCoSTreD (Cascading CoSTreD) algorithms :
 *     CFRP : Cascading Forward Reduction Process
 *     CBPP : Cascading Backward Propagation Process
*)

open GuaCaml
open Extra
open STools

type supp = int list

(* Section 1. Forward Reduction Process *)

module type MSigCFRP =
sig
  type t (* representation language manager *)
  type f0 (* input representation language *)
  type f1 (* intermediary representation language *)
  type f2 (* output representation language *)

  (* [support t f] returns a sorted (super)set of the variables on which the function [f] depends *)
  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp

  val elim_exists : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> f1 * f2
  (* [FIXME] fix comments *)
  (* [elim_exists t n f0l f1l supp elim proj = (projected, coprojected)] where :
      - \forall i, fl_i \subset supp
      - elim \subset supp
      - proj = supp - elim
      - [vl] \subset NS
      - [proj] is the projection of the conjunction fl on vl complement
      - [proj] is the result of existentially eliminating [elim] variables
      - [coproj] is the coprojection relatively to this projection operator
      - [fl] <> []
      - [n] : global arity
   *)
  (* [solve] guarentees that :
      - [support coproj] \subset NS
      - [support proj] \subset NS \setminus [vl]
      - [solve t [] vl = true * true]
   *)
  (* the boolean/exists case
     [solver t fl vl = (proj, coproj)] with
      coproj = /\_i vl_i
      proj = \exists vl. coproj
   *)

  val elim_forall0 : t -> int -> f0 -> supp -> supp -> supp -> f0 (* [LATER] change for (f0, f1) AB.ab *)
  val elim_forall1 : t -> int -> f1 -> supp -> supp -> supp -> f1
  (* [elim_forall t i f supp elim proj = projected]
     where :
     - [projected] is the projected of [f] (which support is included in [supp]) with respect to [elim]
     - [projected]' support is included in [proj]
   *)

  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  (* [trivial t f = Some true ] :
        the constraint [f] is not relevant
        (e.g. (fun _ -> true) in the QBF case)
     [trivial t f = Some false] :
        the constraint [f] is contradictory
        (i.e. any system containing it has no solution)
        (e.g. (fun _ -> false) in the QBF case)
     [trivial t f = None      ] : the contraints [f] is not trivial
        (e.g. any non-constant function in the QBF case)
   *)

end

module type SigCFRP =
sig
  module M : MSigCFRP

  type cfrp_state = {
    cfrp_f0a : M.f0 list array;
    cfrp_f1a : M.f1 list array;
    cfrp_f2a : M.f2 option array;
  }

  (*  [apply t n fl wap_out = opa] where
        - [n] is the global arity of the problem
        - [opa = None] was proved to have no solution
        - [opa = Some a] has
   *)
  val apply : M.t -> ?check:bool -> int -> Cwap_exchange.output -> M.f0 list -> M.f1 list-> cfrp_state option
  (* Forward Reduction Process *)
end

module MakeCFRP(M0:MSigCFRP) : SigCFRP
  with type M.t  = M0.t
  and  type M.f0  = M0.f0
  and  type M.f1  = M0.f1
  and  type M.f2  = M0.f2
=
struct
  module M = M0

  type f0 = M.f0 * supp
  type f1 = M.f1 * supp
  type f2 = M.f2 * supp

  type cfrp_state = {
    cfrp_f0a : M.f0 list array;
    cfrp_f1a : M.f1 list array;
    cfrp_f2a : M.f2 option array;
  }

  (* [solve_step t ~check:true ga wap_output cfrp_state i = continue] where :
       - t : function manager(s)
       - [check = false] disables some support consistency verification
       - ga : global arity
       - wap_output : Cwap_exchange.output (precomputed block dependancies)
       - cfrp_state : global state manager
       - i : index to deal with during current step
       - [continue = false] indicates that the current has been proved unsatifiable
    *)
  let solve_step t ?(check=true) ga wap state i : bool =
    let f0l = state.cfrp_f0a.(i) in
    let f1l = state.cfrp_f1a.(i) in
    let oc = wap.Cwap_exchange.sequence.(i) in
    let proj = Cwap_exchange_utils.cfrp_proj oc in
    let elim = oc.Cwap_exchange.internal in
    let supp = Cwap_exchange_utils.cfrp_supp oc in
    print_endline ToS.("[DAGaml.CCoSTreD.MakeCFRP.solve_step] {step:"^(int i)^"; size:"^(int (List.length supp))^"; univ:"^(int (List.length oc.Cwap_exchange.univ_pred))^"}");
    match oc.Cwap_exchange.tree_pred with
    | None -> true
    | Some pred -> (
      if check
      then (
        print_endline "[DAGaml.CCoSTreD.solver_step] verification f0l";
        List.iter (fun f0 -> assert(SetList.subset_of (M.support0 t f0) supp)) f0l;
        print_endline "[DAGaml.CCoSTreD.solver_step] verification f1m";
        List.iter (fun f1 -> assert(SetList.subset_of (M.support1 t f1) supp)) f1l;
      );
      let f1', f2' = M.elim_exists t ga f0l f1l supp elim proj in
      assert(state.cfrp_f2a.(i) = None);
      if check
      then (
        print_endline "[DAGaml.CCoSTreD.solver_step] verification f1', f2'";
        assert(SetList.subset_of (M.support1 t f1') proj);
        assert(SetList.subset_of (M.support2 t f2') supp);
      );
      (* state.cfrp_f0a.(i) <- []; (* removes irrelevant links *) *)
      (* state.cfrp_f1a.(i) <- []; (* removes irrelevant links *) *)
      state.cfrp_f2a.(i) <- Some f2';
      match M.trivial2 t f2' with
      | Some false -> (
        print_endline "[DAGaml.CCoSTreD.solver_step] UNSAT : f2'";
        false
      )
      | _ -> (
        match M.trivial1 t f1' with
        | Some false -> (
          print_endline "[DAGaml.CCoSTreD.solver_step] UNSAT : f1'";
          false
        )
        | Some true  -> true
        | None -> (
          let elim = oc.Cwap_exchange.univ_pred in
          if elim = []
          then (array_push state.cfrp_f1a pred f1'; true)
          else (
            print_endline "[DAGaml.CCoSTreD.solver_step] universal elimination";
            let supp = proj in
            let proj = SetList.minus supp elim in
            let f1'' = M.elim_forall1 t ga f1' supp elim proj in
            match M.trivial1 t f1'' with
            | Some false -> (
              print_endline "[DAGaml.CCoSTreD.solver_step] UNSAT f1''";
              false
            )
            | Some true  -> true
            | None -> (array_push state.cfrp_f1a pred f1''; true)
          )
        )
      )
    )

  let init_state t ?(check=true) ga wap (f0l:M.f0 list) (f1l:M.f1 list) : cfrp_state option =
    let len = Array.length wap.Cwap_exchange.sequence in
    let va = wap.Cwap_exchange.variables in
    let seq = wap.Cwap_exchange.sequence in
    let cfrp_f0a = Array.make len [] in
    let cfrp_f1a = Array.make len [] in
    let cfrp_f2a = Array.make len None in
    if
      (* first, we normalize and classify f0 functions *)
      (List.for_all
        (fun f0 ->
          let s = M.support0 t f0 in
          let u = Cwap_exchange_utils.univ_proj va s in
          let f0' = M.elim_forall0 t ga f0 s u (SetList.minus s u) in
          match M.trivial0 t f0' with
          | Some true  -> true
          | Some false -> false
          | None -> (
            let s = M.support0 t f0' in
            let i = Cwap_exchange_utils.min_index va s |> Tools.unop in
            if check
            then (
              let supp_i = Cwap_exchange_utils.cfrp_supp seq.(i) in
              if not(SetList.subset_of s supp_i)
              then (
                print_endline  "[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f0] [error] (supp subset_of seq.(i).supp):false";
                print_endline ("[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f0] [error] i:"^(ToS.int i));
                print_endline ("[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f0] [error] supp:"^(ToS.(list int) s));
                print_endline ("[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f0] [error] seq.(i).supp:"^(ToS.(list int) supp_i));
                failwith "[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f0] supp not subset of seq.(i).supp";
              )
            );
            array_push cfrp_f0a i f0';
            true
          )
        ) f0l) &&
      (* then, we normalize and classify f1 functions *)
      (List.for_all
        (fun f1 ->
          let s = M.support1 t f1 in
          let u = Cwap_exchange_utils.univ_proj va s in
          let f1' = M.elim_forall1 t ga f1 s u (SetList.minus s u) in
          match M.trivial1 t f1' with
          | Some true  -> true
          | Some false -> false
          | None -> (
            let s = M.support1 t f1' in
            let i = Cwap_exchange_utils.min_index va s |> Tools.unop in
            if check
            then (
              let supp_i = Cwap_exchange_utils.cfrp_supp seq.(i) in
              if not(SetList.subset_of s supp_i)
              then (
                print_endline  "[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f1] [error] (supp subset_of seq.(i).supp):false";
                print_endline ("[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f1] [error] i:"^(ToS.int i));
                print_endline ("[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f1] [error] supp:"^(ToS.(list int) s));
                print_endline ("[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f1] [error] seq.(i).supp:"^(ToS.(list int) supp_i));
                failwith "[DAGaml.CCoSTreD.MakeCFRP.init_state:normalize_and_classify_f1] supp not subset of seq.(i).supp";
              )
            );
            array_push cfrp_f1a i f1';
            true
          )
        ) f1l)
    then Some {cfrp_f0a; cfrp_f1a; cfrp_f2a}
    else None

  (* Forward Reduction Process *)
  (* ga : global arity *)
  let apply t ?(check=true) (ga:int) (wap:Cwap_exchange.output) (f0l:M.f0 list) (fl1:M.f1 list) : cfrp_state option =
    match init_state t ~check ga wap f0l fl1 with
    | None -> None
    | Some state -> (
      let n = Array.length wap.Cwap_exchange.sequence in
      let rec apply_rec i =
        if i >= n then Some state else (
          if solve_step t ~check ga wap state i
          then apply_rec (succ i)
          else None
        )
      in apply_rec 0
    )
end

(* Section 2. Backward Propagation Process *)
module type MSigCBPP =
sig
  type t (* representation language manager *)
  type f2 (* input representation language of functions *)
  type f3 (* output representation language of functions *)

  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp
  (*  [support t f = il] where
        [il] is the sorted (increasing order) (super)set of variables on which
        the function [f] depends
   *)
  (* val backsolve : t -> int -> (f * supp) -> f' *)
  (*  [backsolver t n (f, sf) = f']
        maps parameter variables to the output language
   *)

  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> supp -> f3
  (*  [backproj t n f3 f2 supp3 introU elimE supp2 = f3']
        add [introU] useless variables to [f3] then project it according to [elimE]
        and combine with [f2]
        - supp(f3) \subset supp3
        - supp(f2) \subset supp2
   *)

end

module type SigCBPP =
sig
  module M : MSigCBPP

  type cbpp_state = {
    cbpp_f2a : M.f2 option array;
    cbpp_f3a : M.f3 option array;
  }

  val apply : M.t -> ?check:bool -> int -> Cwap_exchange.output -> cbpp_state -> M.f3 array
  (* Backward Propagation Process *)
end

module MakeCBPP(M0:MSigCBPP) : SigCBPP
  with type M.t  = M0.t
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3
=
struct
  module M = M0

  type cbpp_state = {
    cbpp_f2a : M.f2 option array;
    cbpp_f3a : M.f3 option array;
  }

  let solve_step t ?(check=true) ga wap state i : unit =
    let oc2 = wap.Cwap_exchange.sequence.(i) in
    match oc2.Cwap_exchange.tree_pred with
    | None -> (
      assert(state.cbpp_f3a.(i) |> Tools.isSome);
      ()
    )
    | Some pred -> (
      let f2 = state.cbpp_f2a.(i) |> Tools.unop in
      let f3 = state.cbpp_f3a.(pred) |> Tools.unop in
      let introU = oc2.Cwap_exchange.univ_pred in
      let oc3 = wap.Cwap_exchange.sequence.(pred) in
      let supp3 = Cwap_exchange.(SetList.union_list [oc3.internal; oc3.cfrp_exists; oc3.cbpp_forall; oc3.cbpp_params]) in
      let supp2 = Cwap_exchange.(SetList.union_list [oc2.internal; oc2.cfrp_exists; oc2.cfrp_forall; oc2.cfrp_params]) in
      let elimE = SetList.minus (SetList.minus supp3 supp2) introU in
      let f3' = M.backproj t ga f3 f2 supp3 introU elimE supp2 in
      let supp3' = Cwap_exchange.(SetList.union_list [oc2.internal; oc2.cfrp_exists; oc2.cbpp_forall; oc2.cbpp_params]) in
      if check then (
        assert(SetList.subset_of (M.support3 t f3') supp3');
      );
      state.cbpp_f3a.(i) <- Some f3';
    )

  (* ga : global arity *)
  let apply t ?(check=true) (ga:int) (wap:Cwap_exchange.output) (state:cbpp_state) : M.f3 array =
    let n = Array.length wap.Cwap_exchange.sequence in
    let rec apply_rec i =
      if i >= 0 then (
        solve_step t ~check ga wap state i;
        apply_rec (pred i)
      )
    in
    apply_rec (n-1);
    MyArray.map_unop state.cbpp_f3a
end

(* Section 3. Full Reduction (CCoSTreD) *)
module type MSig =
sig
  type t (* representation language (RL) manager *)
  type f0 (* input RL *)
  type f1 (* pre-CFRP  intermediary RL *)
  type f2 (* post-CFRP intermediary RL *)
  type f3 (* post-CBPP RL *)

  (* support *)
  val support0 : t -> f0 -> supp
  val support1 : t -> f1 -> supp
  val support2 : t -> f2 -> supp
  val support3 : t -> f3 -> supp

  (* trivial *)
  val trivial0 : t -> f0 -> bool option
  val trivial1 : t -> f1 -> bool option
  val trivial2 : t -> f2 -> bool option
  val trivial3 : t -> f3 -> bool option

  (* Section 1. CFRP *)
  (* [elim_exists t n f0l f1l supp elim proj = (projected, coprojected)] *)
  val elim_exists : t -> int -> f0 list -> f1 list -> supp -> supp -> supp -> f1 * f2
  (* [elim_forall t i f supp elim proj = projected] *)
  val elim_forall0 : t -> int -> f0 -> supp -> supp -> supp -> f0 (* [LATER] change for (f0, f1) AB.ab *)
  val elim_forall1 : t -> int -> f1 -> supp -> supp -> supp -> f1

  (* Section 2.0 CFRP to CBPP *)
  val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3

  (* Section 2. CBPP *)
  (*  [backproj t n f3 f2 supp3 introU elimE supp2 = f3'] *)
  val backproj : t -> int -> f3 -> f2 -> supp -> supp -> supp -> supp -> f3
end

module type Sig =
sig
  module M : MSig

  module CFRP : SigCFRP
    with type M.t  = M.t
    and  type M.f0  = M.f0
    and  type M.f1  = M.f1
    and  type M.f2  = M.f2

  module CBPP : SigCBPP
    with type M.t  = M.t
    and  type M.f2  = M.f2
    and  type M.f3  = M.f3

  (* Cascading Forward Reduction Process *)
  val apply_cfrp : M.t -> ?check:bool -> int -> Cwap_exchange.output -> M.f0 list -> M.f1 list-> CFRP.cfrp_state option

  val cbpp_of_cfrp : M.t -> ?check:bool -> int -> Cwap_exchange.output -> CFRP.cfrp_state -> CBPP.cbpp_state

  (* Cascading Backward Propagation Process *)
  val apply_cbpp : M.t -> ?check:bool -> int -> Cwap_exchange.output -> CBPP.cbpp_state -> M.f3 array

  type state = {
    f0a : M.f0 list array;
    f1a : M.f1 list array;
    f2a : M.f2 option array;
    f3a : M.f3 array;
  }

  (* Full Reduction Process *)
  val apply : M.t -> ?check:bool -> int -> Cwap_exchange.output -> M.f0 list -> M.f1 list -> state option
end

module Make(M0:MSig) : Sig
  with type M.t  = M0.t
  and  type M.f0 = M0.f0
  and  type M.f1 = M0.f1
  and  type M.f2 = M0.f2
  and  type M.f3 = M0.f3
=
struct
  module M = M0
  module CFRP = MakeCFRP(M)
  module CBPP = MakeCBPP(M)

  let apply_cfrp = CFRP.apply
  let apply_cbpp = CBPP.apply

  type cfrp_state = CFRP.cfrp_state
  type cbpp_state = CBPP.cbpp_state

  type state = {
    f0a : M.f0 list array;
    f1a : M.f1 list array;
    f2a : M.f2 option array;
    f3a : M.f3 array;
  }

  (* val cbpp_of_cfrp : M.t -> ?check:bool -> int -> Cwap_exchange.output -> CFRP.cfrp_state -> CBPP.cbpp_state *)
  let cbpp_of_cfrp t ?(check=true) ga wap (cfrp_state:cfrp_state) : cbpp_state =
    let seq = wap.Cwap_exchange.sequence in
    let len = Array.length seq in
    let cbpp_f3a = Array.make len None in
    Array.iteri
      (fun i ci ->
        match ci.Cwap_exchange.tree_pred with
        | Some _ -> ()
        | None -> (
          let f0l = cfrp_state.CFRP.cfrp_f0a.(i) in
          let f1l = cfrp_state.CFRP.cfrp_f1a.(i) in
          let supp = ci.Cwap_exchange.internal in
          let f3 = M.solve_parameter t ga f0l f1l supp in
          cbpp_f3a.(i) <- Some f3
        )
      )
      seq;
    CBPP.{cbpp_f2a = cfrp_state.cfrp_f2a; cbpp_f3a}

  (* [apply t ~check ga f0l f1l out]
     sequencially call the CFRP and CBPP processes
     by default ~check:true, if ~check:false turns off support consistency verification
   *)
  let apply t ?(check=true) ga wap f0l f1l =
    (* print_endline ("[DAGaml.CCoSTreD.Make.apply] ga:"^STools.ToS.(int ga)); *)
    match apply_cfrp t ~check ga wap f0l f1l with
    | None -> None
    | Some cfrp_state -> (
      let cbpp_state = cbpp_of_cfrp t ~check ga wap cfrp_state in
      let f3a = apply_cbpp t ~check ga wap cbpp_state in
      Some {
        f0a = cfrp_state.cfrp_f0a;
        f1a = cfrp_state.cfrp_f1a;
        f2a = cfrp_state.cfrp_f2a;
        f3a;
      }
    )
end
