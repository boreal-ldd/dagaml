open GuaCaml
open CnfTypes.CSTypes
open CnfTypes

let dpll_naive_pick (maxi:int) (f:formule) : int =
  List.fold_left (fun (v:int) (c:clause) ->
    List.fold_left (fun j (i, _) -> min j i) v c)
    maxi
    f

(* [MOVEME] *)
let findmax (x:'a array) : int * 'a =
  let maxi = ref x.(0) in
  let idx = ref 0 in
  Array.iteri (fun j v ->
    if v > !maxi
    then (
      maxi := v;
      idx := j;
    )) x;
  (!idx, !maxi)

let dpll_maxvarimpact_pick (maxi:int) (f:formule) : int =
  let varimpact = Array.make maxi 0. in
  List.iter (fun c ->
    let k = 2. ** (float_of_int(-(List.length c))) in
    List.iter
      (fun (i, _) -> varimpact.(i) <- varimpact.(i) +. k)
      c)
    f;
  assert(maxi>0);
  fst(findmax varimpact)

type pick =
  | MinVar
  | MaxVarImpact

let pick = ref MaxVarImpact

let dpll_pick maxi f =
  match !pick with
  | MinVar -> dpll_naive_pick maxi f
  | MaxVarImpact -> dpll_maxvarimpact_pick maxi f

let rec dpll_naive_rec (depth:int) (maxi:int) (f:formule):clause option =
(*  print_endline ((String.make depth ' ')^"[0]f:"^(CnfUtils.ToS.formule f)); *)
  match CnfUtils.simplify f with
  | Error None -> None
  | Error(Some units) -> Some units
  | Ok(units, f) -> (
    print_endline ((String.make depth ' ')^"units:"^(CnfUtils.ToS.clause units));
    (* print_endline ((String.make depth ' ')^"[1]f:"^(CnfUtils.ToS.formule f)); *)
    let v : int = dpll_pick maxi f in
    print_endline ((String.make depth ' ')^"v:"^(string_of_int v));
    match dpll_naive_rec (succ depth) maxi ([(v, true )]::f) with
    | Some value -> (
      match CnfUtils.clause_addl units value with
      | [] -> assert false
      | sol -> Some sol
    )
    | None ->
    match dpll_naive_rec (succ depth) maxi ([(v, false)]::f) with
    | Some value -> (
      match CnfUtils.clause_addl units value with
      | [] -> assert false
      | sol -> Some sol
    )
    | None -> None
  )

(* [dpll_naive file = res] where
 * match res with
 * | Some value -> a satisfying assignment
 * | None       -> the input formula is unsatisfiable
 *)
let dpll_naive (file:CnfTypes.system) : clause option =
  assert(file.quants = []);
  dpll_naive_rec 0 file.input file.formule

(* [MOVEME]
let test : formule = [[(12, false)]; [(12, false); (17, false)]; [(16, true); (19, false)]; [(17, true); (19, false)]; [(12, true); (13, true)]; [(13, false); (15, true); (19, false)]; [(13, true); (14, true); (19, false)]; [(13, false); (19, false)]; [(17, true); (18, false)]; [(13, false); (17, true)]; [(12, false); (16, true)]]

let _ =
  print_endline ("test:"^(CnfUtils.ToS.formule test));
  assert(CnfUtils.simplify test = Error(Some [(12, false); (13, true); (17, true); (19, false)]));
*)
