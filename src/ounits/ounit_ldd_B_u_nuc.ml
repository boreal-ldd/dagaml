(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ounits : OUnit Tests
 *
 * module  : ounit_ldd_u_nuc : LambdaDD-U-NUC Unit Tests
 *)

open GuaCaml
open AB

open Ldd_B_u_nuc_types
open Ldd_B_u_nuc_io
open Ldd_B_u_nuc_utils
open Ldd_B_u_nuc_gops

module ThisS = Ldd_B_u_nuc_io.ToS

open OUnit

let tt = true
let ff = false

let pee = pedge_of_edge

let test_00 () =
  let blk = {neg = false; arity = 1; block = Id 0} in
  let s = ToB.edge blk [] in
  let blk', s' = OfB.edge s in
  error_assert "s' = []" (s' = []);
  error_assert "blk' = blk" (blk' = blk)

let _ = push_test "test_00" test_00


(* [FIXME]
let _ = push_test "test_01" (fun () ->
  let x0 = ({neg=ff; arity=2; block=SUC(tt, {hasS=ff; hasU=ff; maxC=Some 0}, [C(ff, 0); C(ff, 0)])}, Tree.GLeaf ()) in
  let x1 = ({neg=ff; arity=2; block=C0}, Tree.GLeaf ()) in
  let y0 = ({neg=ff; arity=2; block=SUC(tt, {hasS=ff; hasU=ff; maxC=Some 0}, [C(ff, 0); C(tt, 0)])}, Tree.GLeaf ()) in
  let y1 = ({neg=ff; arity=2; block=C0}, Tree.GLeaf ()) in

  let xy0 : _ edge' =({neg=tt; arity=2; block=Id 0}, Tree.GLeaf()) in
  let xy1 : _ edge' =({neg=ff; arity=2; block=C0}, Tree.GLeaf()) in

  (* print_endline ("xy0: "^ThisS.edge' STools.ToS.ignore xy0); *)

  assert(solve_and ((), pee x0, pee y0) = merge3_of_edge xy0);
  assert(solve_and ((), pee x1, pee y1) = merge3_of_edge xy1);

  let x01=({neg=ff; arity=3; block=SUC(ff, {hasS=ff; hasU=ff; maxC=Some 1}, [C(tt, 0); C(ff, 1); C(ff, 1)])}, Tree.GLeaf()) in
  let y01=({neg=ff; arity=3; block=SUC(ff, {hasS=ff; hasU=ff; maxC=Some 1}, [C(tt, 0); C(ff, 1); C(tt, 1)])}, Tree.GLeaf()) in

  assert(solve_cons ((), x0, x1) = merge_of_edge x01);
  assert(solve_cons ((), y0, y1) = merge_of_edge y01);

  (* print_endline ("x01: "^ThisS.edge' STools.ToS.ignore x01); *)

  let _xy_01 : _ edge' = ({neg=tt; arity=3; block=SUC(tt, {hasS=ff; hasU=tt; maxC=Some 0}, [C(tt, 0); C(tt, 0); U])}, Tree.GLeaf()) in
  let _01_xy : _ edge' = ({neg=tt; arity=3; block=SUC(tt, {hasS=ff; hasU=tt; maxC=Some 0}, [C(tt, 0); C(tt, 0); U])}, Tree.GLeaf()) in

  assert(solve_cons ((), xy0, xy1) = merge_of_edge _xy_01);
  assert(solve_and  ((), pee x01, pee y01) = merge3_of_edge (pee _01_xy));
(*
  print_endline ("_xy_01: "^ThisS.emerge STools.ToS.ignore _xy_01);
  print_endline ("_01_xy: "^ThisS.pemerge3 STools.ToS.ignore _01_xy);
 *)
  ()
)
 *)

let _ = run_tests()
