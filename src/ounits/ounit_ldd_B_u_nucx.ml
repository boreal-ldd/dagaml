(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ounits : OUnit Tests
 *
 * module  : ounit_ldd_u_nucx : LambdaDD-U-NUCX Unit Tests
 *)

open GuaCaml
open OUnit
open AB

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_io
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_cons
open Ldd_B_u_nucx_peval
open Ldd_B_u_nucx_andb
open Ldd_B_u_nucx_xorb

let test_01 () =
  let ee = ({neg = false; noutput = 0; ninput = Some (0); decomp = UCX ([], [])}, Tree.GLeaf ()) in
  error_assert
    "check_edge ee = false"
    (Ldd_B_u_nucx_check.check_edge ee = false)

let _ = push_test "test_01" test_01

let _ = run_tests()

let _ =
  print_endline "\ntest D00";
  let blk = {neg=false; noutput=3; ninput=None; decomp=UCX([A(); A(); A()],[B(false,[B false; A(); A()]); A[B(); B()]])} in
  let s = ToB.sized_block blk [] in
  let n = blk.noutput in
  let blk', s' = OfB.sized_block n s in
  assert(blk' = blk);
  assert(s' = []);

  print_endline "\ntest D01";
  let blk = {neg = false; noutput = 6; ninput = Some 6; decomp = UCX ([A(); A(); A(); A(); A(); A()], [])} in
  let s = ToB.sized_block blk [] in
  let n = blk.noutput in
  let blk', s' = OfB.sized_block n s in
  assert(blk' = blk);
  assert(s' = []);

  print_endline "\ntest D02";
  let blk = {neg = false; noutput = 6; ninput = Some 4; decomp = UCX ([A(); A(); A(); A(); A(); A()], [B (false, [A(); A(); (B true); A(); A(); B true])])} in
  let s = ToB.sized_block blk [] in
  let n = blk.noutput in
  let blk', s' = OfB.sized_block n s in
  assert(blk' = blk);
  assert(s' = []);

  ()

let _ =
  print_endline "\ntest C00";
  let max = 10 in
  let min = 1 in
  let n = 2 in
  let s = BTools.ToB.int' min max n [] in
  let n', s' = BTools.OfB.int' min max s in
  assert(n' = n);
  assert(s' = []);

  print_endline "\ntest C01";
  let rowU = [B(); B(); A(); A(); B(); B(); A(); A(); B(); A()] in
  let s = ToB.sized_rowU rowU [] in
  let n = List.length rowU in
  let (rowU', nA), s' = OfB.sized_rowU n s in
  assert(nA = MyList.count isA rowU);
  assert(rowU' = rowU);
  assert(s' = []);

  ()

let _ =
  print_endline "\ntest A00";
  let rowU = [A(); A()] in
  let s = ToB.rowU rowU [] in
  let rowU', s' = OfB.rowU s in
  assert(rowU' = rowU);
  assert(s' = []);

  print_endline "\ntest A01";
  let rowX = [B(); B()] in
  let s = ToB.rowX rowX [] in
  let rowX', s' = OfB.rowX s in
  assert(rowX' = rowX);
  assert(s' = []);

  print_endline "\ntest A02";
  let matUCX = ([A(); A()], [(A [B(); B()])]) in
  let s = ToB.matUCX matUCX [] in
  let matUCX', s' = OfB.matUCX s in
  assert(matUCX' = matUCX);
  assert(s' = []);

  print_endline "\ntest A03";
  let matUCX = ([A(); A()], [(B (true, [(B false); (B false)]))]) in
  let s = ToB.matUCX matUCX [] in
  let matUCX', s' = OfB.matUCX s in
  assert(matUCX' = matUCX);
  assert(s' = []);

  print_endline "\ntest A04";
  let edge = {neg = true; noutput = 2; ninput = None; decomp = UCX ([A(); A()], [(A [B(); B()])])} in
  print_endline ("[test A04] edge :"^(ToS.edge edge ));
  let s = ToB.edge edge [] in
  let edge', s' = OfB.edge s in
  print_endline ("[test A04] edge':"^(ToS.edge edge'));
  assert(edge = edge');
  assert(s' = []);

  print_endline "\ntest A05";
  let edge = {neg = false; noutput = 2; ninput = None; decomp = UCX ([A(); A()], [(B (true, [(B false); (B false)]))])} in
  let s = ToB.edge edge [] in
  let edge', s' = OfB.edge s in
  assert(edge = edge');
  assert(s' = []);

  ()

let _ =
  print_endline "\ntest 00";
  let e_00 =({neg = true; noutput = 2; ninput = None; decomp = C0}, Tree.GLeaf()) in
  let e_01 =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); B()], [B(false, [B false])])}, Tree.GLeaf()) in
  assert(check_edge e_00 = true);
  assert(check_edge e_01 = false);
  let m3_00 =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); B()], [B(false, [B false])])}, Utils.M3Edge(Tree.GLeaf())) in
  assert(check_merge3 m3_00 = false);

  print_endline "\ntest 01";
  let rCX = B((true, [B false; A()])) in
  let bD = true in
  let i = 0 in
  let bdecomp =(false, UCX([A(); A()], [B((true, [B false; B false]))])) in
  assert(compose_rowCX_with_Id rCX bD i = bdecomp);

  print_endline "\ntest 02";
  let x0 : _ edge' =({neg = true; noutput = 1; ninput = None; decomp = C0}, Tree.GLeaf()) in
  let x1 : _ edge' =({neg = true; noutput = 1; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  let x01 : _ edge' =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  assert(solve_cons x0 x1 = merge_of_edge x01);

  print_endline "\ntest 03";
  let y0 : _ edge' =({neg = true; noutput = 1; ninput = None; decomp = C0}, Tree.GLeaf()) in
  let y1 : _ edge' =({neg = false; noutput = 1; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  let y01 : _ edge' =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B true ])])}, Tree.GLeaf()) in
  assert(solve_cons y0 y1 = merge_of_edge y01);

  print_endline "\ntest 04";
  let rowCX = B((true, [B false; A()])) in
  let block = {neg = false; noutput = 1; ninput = None; decomp = C0} in
  let result = {neg = true; noutput = 2; ninput = None; decomp = Id 0} in
  assert(compose_rowCX_block rowCX block = result);

  print_endline "\ntest 05";
  let fx =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  let fy =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B true ])])}, Tree.GLeaf()) in
  let fxy =({neg = true; noutput = 2; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

  print_endline "\ntest 06";
  let p =[Some  false; None] in
  let f =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B((true, [B false; B false ]))])}, Tree.GLeaf()) in
  let pf =({neg = true; noutput = 1; ninput = None; decomp = C0}, Tree.GLeaf()) in
  assert(peval_pedge p f = pf);

  print_endline "\ntest 07";
  let fx =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B((true, [B false; B false ]))])}, Tree.GLeaf()) in
  let fy =({neg = true; noutput = 2; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  let fxy =({neg = true; noutput = 2; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

  print_endline "\ntest 08";
  let fx =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B((true, [B false; B false ]))])}, Tree.GLeaf()) in
  let fy =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B((true, [B(true ); B false ]))])}, Tree.GLeaf()) in
  let fxy =({neg = true; noutput = 2; ninput = None; decomp = Id 1}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

  print_endline "\ntest 09";
  let fx =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  let fy =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B true; B true ])])}, Tree.GLeaf()) in
  let fxy =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B true; B true ])])}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

  print_endline "\ntest 10";
  let rowCX = B((false, [B true; B true ])) in
  let block = {neg = true; noutput = 0; ninput = None; decomp = C0} in
  let block' = {neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B true; B true])])} in
  assert(compose_rowCX_block rowCX block = block');

  print_endline "\ntest 11";
  let p =[Some false; None] in
  let pe =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [A([B(); B()])])}, Tree.GLeaf()) in
  let ppe =({neg = true; noutput = 1; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  assert(peval_pedge p pe = ppe);

  print_endline "\ntest 12";
  let fx =({neg = true; noutput = 2; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  let fy =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [A[B(); B()]])}, Tree.GLeaf()) in
  let fxy =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B true; B true ])])}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

 (*(*[TODO] implement advanced local solver *)
  print_endline "\ntest 12";
  let fx =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [A[B(); B()]])}, Tree.GLeaf()) in
  let fy =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  let fxy =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B true; B true])])}, Tree.GLeaf()) in
  assert(solve_xor fx fy = merge3_of_edge fxy);
   *)

  print_endline "\ntest 13";
  let f0 =({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  let f1 =({neg = true; noutput = 2; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  let f01 =({neg = true; noutput = 3; ninput = None; decomp = UCX([A(); A(); A()], [B(false, [A(); B false; A()]); B(true, [B true; B true])])}, Tree.GLeaf()) in
  assert(solve_cons f0 f1 = merge_of_edge f01);

  print_endline "\ntest 14";
  let fx =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); A()], [B((true, [B false; B false; B false ]))])}, Tree.GLeaf()) in
  let fy =({neg = false; noutput = 3; ninput = None; decomp = Id 2}, Tree.GLeaf()) in
  let fxy =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); A()], [B((false, [A(); A(); B false ])); B((true, [B false; B false ]))])}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

  print_endline "\ntest 15";
  let fx =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B((true, [B false; B false ]))])}, Tree.GLeaf()) in
  let fy =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B((true, [B false; B true  ]))])}, Tree.GLeaf()) in
  let fxy =({neg = true; noutput = 3; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

  print_endline "\ntest 16";
  let p =[Some  false; None; None] in
  let pe =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B((true, [B false; B false ]))])}, Tree.GLeaf()) in
  let ppe =({neg = true; noutput = 2; ninput = None; decomp = C0}, Tree.GLeaf()) in
  assert(peval_pedge p pe = ppe);

  print_endline "\ntest 17";
  let fx =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B((true, [B false; B false ]))])}, Tree.GLeaf()) in
  let fy =({neg = true; noutput = 3; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  let fxy =({neg = true; noutput = 3; ninput = None; decomp = Id 0}, Tree.GLeaf()) in
  assert(solve_and fx fy = merge3_of_edge fxy);

  print_endline "\ntest 18";
  let fx =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  let fy =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B(true, [B false; B true])])}, Tree.GLeaf()) in
  ignore(solve_xor fx fy);

  print_endline "\ntest 19";
  let f0 =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  let f1 =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [B(true, [B false; B true])])}, Tree.GLeaf()) in
  let f01 =({neg= true; noutput = 4; ninput = None; decomp = UCX([A(); A(); A(); B()], [B(false, [A(); B false; A()]); A[B(); B()]])}, Tree.GLeaf()) in
  assert(solve_cons f0 f1 = merge_of_edge f01);

  print_endline "\ntest 20";
  let block = {neg = true; noutput = 3; ninput = None; decomp = UCX([A(); A()], [A[B(); B()]])} in
  assert(check_block block = false);

  print_endline "\ntest 21";
  let bmatUCX =(false, ([A(); A(); B()], [])) in
  let edge =({neg = true; noutput = 2; ninput = None; decomp = UCX([A(); A()], [A[B(); B()]])}, Tree.GLeaf()) in
  let result =({neg = true; noutput = 3; ninput = None; decomp = UCX([A(); A(); B()], [A[B(); B()]])}, Tree.GLeaf()) in
  assert(compose_bmatUCX_edge bmatUCX edge = result);

  print_endline "\ntest 22";
  let x0 = ({neg = true; noutput = 3; ninput = None; decomp = UCX ([A(); A(); A()], [(B (false, [(B false); A(); A()])); (A [B(); B()])])}, (Tree.GLeaf ())) in
  let x1 = ({neg = true; noutput = 3; ninput = None; decomp = Id 0}, (Tree.GLeaf())) in
  let p = [None; None; None] in
  let x01 = ({neg = true; noutput = 4; ninput = None; decomp = UCX ([A(); A(); A(); A()], [(B (false, [A(); B false; A(); A()])); (B(true, [B true; A(); A()])); A[B(); B()]])}, Tree.GLeaf()) in
  assert(node_pull x01 = MEdge((), x0, x1));
  assert(solve_cons x0 x1 = merge_of_edge x01);

  assert(peval_pedge p (pedge_of_edge x0) = pedge_of_edge x0);
  assert(peval_pedge p (pedge_of_edge x1) = pedge_of_edge x1);
  assert(peval_pedge ((Some false)::p) (pedge_of_edge x01) = peval_pedge p (pedge_of_edge x0));
  assert(peval_pedge ((Some true )::p) (pedge_of_edge x01) = peval_pedge p (pedge_of_edge x1));
  assert(peval_pedge ( None       ::p) (pedge_of_edge x01) = pedge_of_edge x01);

(*
  print_endline "\ntest 23";
  let x0 =({neg = true; noutput = 3; ninput = None; decomp = C0}, Tree.GLeaf()) in
  let x1 =({neg = false; noutput = 3; ninput = None; decomp = UCX([A(); A(); A()], [B(true, [B false; B false; B false])])}, Tree.GLeaf()) in
  let y0 =({neg = true; noutput = 3; ninput = None; decomp = UCX([A(); A(); A()], [B(false, [B false; A(); A()]); A[B(); B()]])}, Tree.GLeaf()) in
  let y1 =({neg = true; noutput = 3; ninput = None; decomp = UCX([A(); A(); A()], [B(false, [B false; A(); A()]); B(true, [B false; B false])])}, Tree.GLeaf()) in

  let xy0 = ({neg = true; noutput = 3; ninput = None; decomp = UCX([A(); A(); A()], [B(false, [B false; A(); A()]); A[B(); B()]])}, Tree.GLeaf()) in
  let xy1 = ({neg = true; noutput = 3; ninput = None; decomp = Id 0}, Tree.GLeaf()) in

  let x01 = ({neg = false; noutput = 4; ninput = None; decomp = UCX([A(); A(); A(); A()], [B(true, [B false; B false; B false; B false])])}, Tree.GLeaf()) in

  let y01_ee = {neg = true; noutput = 4; ninput = Some 3; decomp = UCX([A(); A(); A(); A()], [B(false, [A(); B false; A(); A()])])} in
  let y01_0 = ({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [A[B(); B()]])}, Tree.GLeaf()) in
  let y01_1 = ({neg = false; noutput = 2; ninput = None; decomp = UCX([A(); A()], [B(true, [B false; B false])])}, Tree.GLeaf()) in
  let y01 = (y01_ee, Utils.MNode((), y01_0, y01_1)) in
  let y01_pe = (y01_ee, Tree.GLink(None, ((), y01_0, y01_1))) in

  assert(solve_and x0 y0 = merge3_of_edge xy0);  print_newline();
  assert(solve_and x1 y1 = merge3_of_edge xy1);  print_newline();
  assert(solve_cons x0 x1 = merge_of_edge x01);  print_newline();
  assert(solve_cons y0 y1 = y01);                print_newline();

  let x01y01_pe = (
    {neg = false; noutput = 4; ninput = (Some 3); decomp = UCX ([A(); A(); A(); A()], [(B (true, [A(); (B false); A(); A()]))])},
    Utils.M3Node (((),
      ({neg = true; noutput = 3; ninput = (Some 3); decomp = UCX ([A(); A(); A()], [])}, Tree.GLink (None, ((), y01_0, y01_1))),
      ({neg = false; noutput = 3; ninput = None; decomp = UCX ([A(); A(); A()], [(B (true, [(B false); (B false); (B false)]))])}, Tree.GLeaf ()))
    )
  ) in

  let xy0xy1 = ({neg = false; noutput = 4; ninput = None; decomp = UCX ([A(); A(); B(); B()], [(B (true, [(B false); (B false)]))])}, Tree.GLeaf ()) in

  assert(solve_and x01 y01_pe = x01y01_pe); print_newline();
  assert(solve_cons xy0 xy1 = merge_of_edge xy0xy1); print_newline();
 *)

  print_endline "\ntest 24";
  let x0 = ({neg = true; noutput = 3; ninput = None; decomp = C0}, Tree.GLeaf ()) in
  let x1 = ({neg = false; noutput = 3; ninput = Some 3; decomp = UCX ([A (); A (); A ()], [])}, Tree.GLink ()) in
  let p = [None; None; None] in
  let x01 = ({neg = false; noutput = 4; ninput = Some 3; decomp = UCX ([A (); A (); A (); A ()], [B (true, [B false; A (); A (); A ()])])}, Tree.GLink ()) in
  assert(solve_cons x0 x1 = merge_of_edge x01);
  assert(peval_pedge ((Some false)::p) (pedge_of_edge x01) = peval_pedge p (pedge_of_edge x0));
  assert(peval_pedge ((Some true )::p) (pedge_of_edge x01) = peval_pedge p (pedge_of_edge x1));
  assert(peval_pedge ( None       ::p) (pedge_of_edge x01) = pedge_of_edge x01);


  exit 0

