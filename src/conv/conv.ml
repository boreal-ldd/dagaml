(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : conv    : DAGaml's standard conversion modules
 *
 * module  : Conv    : conversion module which maps file extension and ConvCore's conversion methods
 *)

open GuaCaml
open STools
open BTools
open ConvTypes
open ConvCore

let conv tagA fileA tagB fileB args =
match tagA, tagB with
| ExtO _, _  -> failwith "[conv/conv] tagA is output_tag"
| _, ExtI _  -> failwith "[conv/conv] tagB is input_tag"
| ExtI (), _ -> failwith "[conv/conv] input_tag is empty"
| Int(Pure, O_U, VB), Ext BddH ->
(
  let pure_bdd, edges = Ldd_B_o_u.LDD.of_file fileA in
  let cha = open_out fileB in
  let edges = Ldd_B_o_u.LDD.to_hr pure_bdd edges cha in
  close_out cha;
  print_string "edges : ";
  print_string ToS.(list int edges);
  print_newline();
)
| Ext Verilog, Ext Cmd -> (
  ConvCore.Ext_to_Ext.verilog_to_cmd fileA fileB args
)
| Ext Cmd, Ext Verilog -> (
  ConvCore.Ext_to_Ext.cmd_to_verilog fileA fileB args
)
| Ext Verilog, Ext Verilog -> (
  ConvCore.Ext_to_Ext.verilog_to_verilog fileA fileB args
)
| Ext Verilog, Ext Cnf -> (
  ConvCore.Ext_to_Ext.verilog_to_cnf fileA fileB args
)
| Ext Verilog, Int(Pure, modeleB, VB) -> (
  ConvCore.Ext_to_BPure.of_verilog fileA modeleB fileB args
)
| Ext Pla, Int(Pure, modeleB, VB) -> (
  ConvCore.Ext_to_BPure.of_pla fileA modeleB fileB args
)
| Ext Nnf, Ext Verilog -> (
  ConvCore.Ext_to_Ext.nnf_to_verilog fileA fileB args
)
| Ext Cnf, Ext Verilog ->
(
  ConvCore.Ext_to_Ext.cnf_to_verilog_vA fileA fileB args
)
| Ext Cnf, Ext Cnf ->
(
  print_string "[conv/conv] Cnf to Cnf"; print_newline();
  let qcnf = StrLoadCnf.load_file fileA in
  StrDumpCnf.dump_file fileB qcnf
)
| Ext QCnf, Ext QCnf ->
(
  print_string "[conv/conv] QCnf to QCnf"; print_newline();
  let qcnf = StrLoadCnf.load_qbf_file fileA in
  StrDumpCnf.dump_qbf_file fileB qcnf
)
| Ext Cnf, Int(Pure, modeleB, VB) -> (
  ConvCore.Ext_to_BPure.cnf_to_ fileA modeleB fileB args
)
| Int(Pure, modeleA, VB), Ext Verilog -> (
  ConvCore.BPure_to_Ext.to_verilog modeleA fileA fileB args
)
| Int(Pure, modele, VB), ExtO Dot -> (
  ConvCore.BPure_to_Ext.to_dot modele fileA fileB args
)
| Int(Pure, modele, VB), ExtO Stats -> (
  ConvCore.BPure_to_Ext.to_stats modele fileA fileB args
)
| Int(Pure, modele, VB), ExtO CntSat -> (
  ConvCore.BPure_to_Ext.to_cntsat modele fileA fileB args
)
| tagX, tagY -> (
  invalid_arg ConvTypesDump.("[conv/conv] WIP : no conversion available between [ "^(file_ext tagX)^" ] and [ "^(file_ext tagY)^" ].")
)
