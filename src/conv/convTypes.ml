(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : conv    : DAGaml's standard conversion modules
 *
 * module  : ConvTypes : enumeration of file formats
 *)

type extern_tag =
| Verilog
| Cnf
| Cir
| Cmd
| QCnf
| QCir
| Nnf  (* http://www.cril.univ-artois.fr/kc/index.html *)
| Pla
| BddH (* Human Readable Bdd *)
type input_tag = unit
type output_tag =
| Dot
| Check
| Stats
| CntSat
| AllSat
type modele_tag =
| O_U
| O_NU
| O_C10
| O_UC0
| O_NUCX
| U_NU
| U_NUX
| U_NUC
| U_NUCX
| L_NNUX
type tacx_pure =
| Tacx
| Pure
type version_tag =
| VB
type file_tag =
| Ext  of extern_tag
| ExtI of input_tag
| ExtO of output_tag
| Int  of (tacx_pure * modele_tag * version_tag)
