(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : conv    : DAGaml's standard conversion modules
 *
 * module  : ConvUtils : conversion toolbox
 *)

open GuaCaml

let snaxoops_to_verilog (snax:Snax.Module.G1.manager) (edges:SnaxOops.OOPS.Model.f list) (name:string) (file:string) : unit =
  let ninputs = List.fold_left
    (fun acc f -> max acc f.SnaxOops.OOPS.Model.arity) 0 edges
  in
  let funmap f = f.SnaxOops.OOPS.Model.edge in
  let sys = IoUtils.make
    name snax ninputs (MyArray.(map_of_list funmap edges))
  in
  let sexpr = IoUtils.expr_of_snax sys IoUtils.string_fnn in
  StrDumpVerilog.dump_file file sexpr;
  ()
