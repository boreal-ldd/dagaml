(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : conv    : DAGaml's standard conversion modules
 *
 * module  : ConvTypesLoad : conversion from file extension to format types
 *)

open GuaCaml
open STools
open ConvTypes

let extern_tag = function
| "v"       -> Some Verilog
| "cnf"     -> Some Cnf
| "cir"     -> Some Cir
| "cmd"     -> Some Cmd
| "dimacs"  -> Some Cnf
| "qcnf"    -> Some QCnf
| "qdimacs" -> Some QCnf
| "qcir"    -> Some QCir
| "nnf"     -> Some Nnf
| "pla"     -> Some Pla
| "bdd"     -> Some BddH
| _         -> None
let input_tag _  = None
let output_tag = function
| "dot"    -> Some Dot
| "check"  -> Some Check
| "stats"  -> Some Stats
| "cntsat" -> Some CntSat
| "allsat" -> Some AllSat
| _ -> None
let modele_tag = function
| "bdd"    -> Some O_U
| "o-u"    -> Some O_U
| "bryant" -> Some O_NU
| "o-nu"   -> Some O_NU
| "zdd"    -> Some O_C10
| "o-c10"  -> Some O_C10
| "o-uc0"  -> Some O_UC0
| "o-nucx" -> Some O_NUCX
| "nu"     -> Some U_NU
| "u-nu"   -> Some U_NU
| "nux"    -> Some U_NUX
| "u-nux"  -> Some U_NUX
| "nuc"    -> Some U_NUC
| "u-nuc"  -> Some U_NUC
| "nucx"   -> Some U_NUCX
| "u-nucx" -> Some U_NUCX
| "nni"    -> Some L_NNUX
| "l-nux"  -> Some L_NNUX
| "l-nnux" -> Some L_NNUX
| _ -> None
let tacx_pure = function
| "tacx"   -> Some Tacx
| "pure"   -> Some Pure
| _ -> None
let version_tag = function
| "B" -> Some VB
| _ -> None
let file_tag = function
| [x] ->
(
  match extern_tag x with
  | Some y -> Ext y
  | None -> match input_tag x with
  | Some y -> ExtI y
  | None -> match output_tag x with
  | Some y -> ExtO y
  | None ->
    (invalid_arg ("[conv/convTypesLoad] file_tag: x := \""^x^"\""))
)
| [y; z; x] ->
( match tacx_pure x, modele_tag y, version_tag z with
  | Some x, Some y, Some z -> Int(x, y, z)
  | _ ->
    (invalid_arg ("[conv/convTypesLoad] file_tag: (y, z, x) := (\""^y^"\", \""^z^"\", \""^x^"\")"))
)
| l ->
    (invalid_arg ("[conv/convTypesLoad] file_tag: l := "^ToS.(list string l)))

let file_ext string = match SUtils.split '.' string with
| ""::tail -> file_tag tail
| _ -> failwith ("conv/convTypesLoad:file_ext - error parsing : \""^(string)^"\"")
