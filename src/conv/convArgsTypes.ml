(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : conv    : DAGaml's standard conversion modules
 *
 * module  : ConvArgsTypes : Conversion of Argument Types
 *)

open GuaCaml
open STools

type args =
| Stats of (Tree.stree -> unit)

let get_stats liste = MyList.opmap (function
  | Stats func -> Some func
  (* | _ -> None *) (* unused match case *)
  ) liste
let map_stats stats args = match get_stats args with
  | [] -> ()
  | liste -> (
    let stats = stats() in
    List.iter (fun func -> func stats) liste
  )
let print_stats = Stats(fun stree -> OfSTree.pprint [stree]; print_newline())
