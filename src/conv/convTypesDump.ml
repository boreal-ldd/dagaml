(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : conv    : DAGaml's standard conversion modules
 *
 * module  : ConvTypesDump : conversion from format types to file extensions
 *)

open GuaCaml
open ConvTypes
let extern_tag = function
| Verilog -> "v"
| Cnf     -> "cnf"
| Cir     -> "cir"
| Cmd     -> "cmd"
| QCnf    -> "qcnf"
| QCir    -> "qcir"
| Nnf     -> "nnf"
| Pla     -> "pla"
| BddH    -> "bdd"
let input_tag () = failwith "input_tag is empty"
let output_tag = function
| Dot     -> "dot"
| Check   -> "check"
| Stats   -> "stats"
| CntSat  -> "cntsat"
| AllSat  -> "allsat"
let modele_tag = function
| O_U    -> "o-u"
| O_NU   -> "o-nu"
| O_C10  -> "o-c10"
| O_UC0  -> "o-uc0"
| O_NUCX -> "o-nucx"
| U_NU   -> "u-nu"
| U_NUX  -> "u-nux"
| U_NUC  -> "u-nuc"
| U_NUCX -> "u-nucx"
| L_NNUX -> "l-nnux"
let tacx_pure = function
| Tacx    -> "tacx"
| Pure    -> "pure"
let version_tag = function
| VB      -> "B"
let file_tag = function
| Ext  x  -> [extern_tag x]
| ExtI x  -> [input_tag  x]
| ExtO x  -> [output_tag x]
| Int  (x, y, z) ->
  [modele_tag y; version_tag z; tacx_pure x]
let file_ext x = "."^(String.concat"."(file_tag x))
