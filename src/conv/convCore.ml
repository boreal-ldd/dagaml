(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : conv    : DAGaml's standard conversion modules
 *
 * module  : ConvCore : individual methods for conversion between various formats
 *)

open GuaCaml
open Extra
open STools
open BTools
open ConvTypes

module Ext_to_Ext =
struct
  let verilog_to_verilog fileA fileB args =
    let mexpr = StrLoadVerilog.load_file fileA in
    StrDumpVerilog.dump_file fileB mexpr

  let verilog_to_cnf fileA fileB args =
    let sexpr = StrLoadVerilog.load_file fileA in
    let ssnax = IoUtils.snax_of_expr sexpr in
    let ninput = Array.length sexpr.inps in
    let cnf =
      Snax.Module.export_cnf ssnax.IoTypes.man ninput
        (Tools.map snd (Array.to_list ssnax.IoTypes.sset))
    in
    StrDumpCnf.dump_file fileB cnf

  let cnf_to_verilog_vA fileA fileB ?(simpl=3) args =
    let cnf   = StrLoadCnf.load_file fileA in
    let snax  = Snax.Module.G1.newman () in
    let fA    = SnaxOops.OOPS.LoadCnfA.export_cnf snax ~simpl cnf in
    let sys = IoUtils.make
      cnf.Constraint_system_types.name snax cnf.Constraint_system_types.input [|fA.SnaxOops.OOPS.Model.edge|]
    in
    let sexpr = IoUtils.expr_of_snax sys IoUtils.string_fnn in
    StrDumpVerilog.dump_file fileB sexpr

  let cnf_to_verilog_v0 fileA fileB args =
    let cnf   = StrLoadCnf.load_file fileA in
    let snax  = Snax.Module.G1.newman () in
    let fA    = SnaxOops.OOPS.LoadCnf0.export_cnf snax cnf in
    let sys = IoUtils.make
      cnf.Constraint_system_types.name snax cnf.Constraint_system_types.input [|fA.SnaxOops.OOPS.Model.edge|]
    in
    let sexpr = IoUtils.expr_of_snax sys IoUtils.string_fnn in
    StrDumpVerilog.dump_file fileB sexpr

  let nnf_to_verilog fileA fileB args =
    fileA
    |> StrLoadNnf.load_file
    |> StrDumpVerilog.dump_file ~mode:"nnf" fileB

  let verilog_to_cmd fileA fileB args =
    fileA
    |> StrLoadVerilog.load_file
    |> CmdUtils.OfExpr.convert
    |> StrDumpCmd.dump_file fileB

  let cmd_to_verilog fileA fileB args =
    fileA
    |> StrLoadCmd.load_expr_file
    |> IoUtils.normalize_expr_with_snax
    |> StrDumpVerilog.dump_file fileB
end

(* Conversion from External to Internal/B/Pure *)

module Ext_to_BPure =
struct
  let cnf_to_ ?(out_list=false) ?(smart=true) fileA modeleB fileB args =
    match modeleB with
    | O_U    ->    Ldd_B_o_u.from_cnf ~out_list ~smart fileA fileB
    | O_NU   ->   Ldd_B_o_nu.from_cnf ~out_list ~smart fileA fileB
    | O_C10  ->  Ldd_B_o_c10.from_cnf ~out_list ~smart fileA fileB
    | O_UC0  ->  Ldd_B_o_uc0.from_cnf ~out_list ~smart fileA fileB
    | O_NUCX -> Ldd_B_o_nucx.from_cnf ~out_list ~smart fileA fileB
    | U_NU   ->   Ldd_B_u_nu.from_cnf ~out_list ~smart fileA fileB
    | U_NUX  ->  Ldd_B_u_nux.from_cnf ~out_list ~smart fileA fileB
    | U_NUC  ->  Ldd_B_u_nuc.from_cnf ~out_list ~smart fileA fileB
    | U_NUCX -> Ldd_B_u_nucx.from_cnf ~out_list ~smart fileA fileB
    | L_NNUX -> assert false

  let of_verilog ?(out_list=false) fileA modeleB fileB args =
    match modeleB with
    | O_U    ->    Ldd_B_o_u.of_verilog ~out_list fileA fileB
    | O_NU   ->   Ldd_B_o_nu.of_verilog ~out_list fileA fileB
    | O_C10  ->  Ldd_B_o_c10.of_verilog ~out_list fileA fileB
    | O_UC0  ->  Ldd_B_o_uc0.of_verilog ~out_list fileA fileB
    | O_NUCX -> Ldd_B_o_nucx.of_verilog ~out_list fileA fileB
    | U_NU   ->   Ldd_B_u_nu.of_verilog ~out_list fileA fileB
    | U_NUX  ->  Ldd_B_u_nux.of_verilog ~out_list fileA fileB
    | U_NUC  ->  Ldd_B_u_nuc.of_verilog ~out_list fileA fileB
    | U_NUCX -> Ldd_B_u_nucx.of_verilog ~out_list fileA fileB
    | L_NNUX -> assert false

  let of_pla fileA ?(out_list=false) modeleB fileB args =
    match modeleB with
    | O_U    ->    Ldd_B_o_u.of_pla ~out_list fileA fileB
    | O_NU   ->   Ldd_B_o_nu.of_pla ~out_list fileA fileB
    | O_C10  ->  Ldd_B_o_c10.of_pla ~out_list fileA fileB
    | O_UC0  ->  Ldd_B_o_uc0.of_pla ~out_list fileA fileB
    | O_NUCX -> Ldd_B_o_nucx.of_pla ~out_list fileA fileB
    | U_NU   ->   Ldd_B_u_nu.of_pla ~out_list fileA fileB
    | U_NUX  ->  Ldd_B_u_nux.of_pla ~out_list fileA fileB
    | U_NUC  ->  Ldd_B_u_nuc.of_pla ~out_list fileA fileB
    | U_NUCX -> Ldd_B_u_nucx.of_pla ~out_list fileA fileB
    | L_NNUX -> assert false
end

(* Conversion from Internal/B/Pure to External *)

module BPure_to_Ext =
struct
  let to_verilog modeleA fileA fileB args =
    match modeleA with
    | O_U    ->    Ldd_B_o_u.to_verilog fileA fileB
    | O_NU   ->   Ldd_B_o_nu.to_verilog fileA fileB
    | O_C10  ->  Ldd_B_o_c10.to_verilog fileA fileB
    | O_UC0  ->  Ldd_B_o_uc0.to_verilog fileA fileB
    | O_NUCX -> Ldd_B_o_nucx.to_verilog fileA fileB
    | U_NU   ->   Ldd_B_u_nu.to_verilog fileA fileB
    | U_NUX  ->  Ldd_B_u_nux.to_verilog fileA fileB
    | U_NUC  ->  Ldd_B_u_nuc.to_verilog fileA fileB
    | U_NUCX -> Ldd_B_u_nucx.to_verilog fileA fileB
    | L_NNUX -> assert false

  let to_dot modele fileA fileB args =
    match modele with
    | O_U    ->    Ldd_B_o_u.sys_to_dot fileA fileB
    | O_NU   ->   Ldd_B_o_nu.sys_to_dot fileA fileB
    | O_C10  ->  Ldd_B_o_c10.sys_to_dot fileA fileB
    | O_UC0  ->  Ldd_B_o_uc0.sys_to_dot fileA fileB
    | O_NUCX -> Ldd_B_o_nucx.sys_to_dot fileA fileB
    | U_NU   ->   Ldd_B_u_nu.sys_to_dot fileA fileB
    | U_NUX  ->  Ldd_B_u_nux.sys_to_dot fileA fileB
    | U_NUC  ->  Ldd_B_u_nuc.sys_to_dot fileA fileB
    | U_NUCX -> Ldd_B_u_nucx.sys_to_dot fileA fileB
    | L_NNUX -> assert false

  let to_stats modele fileA fileB args =
    let stats = match modele with
    | O_U    ->    Ldd_B_o_u.sys_to_stats fileA
    | O_NU   ->   Ldd_B_o_nu.sys_to_stats fileA
    | O_C10  ->  Ldd_B_o_c10.sys_to_stats fileA
    | O_UC0  ->  Ldd_B_o_uc0.sys_to_stats fileA
    | O_NUCX -> Ldd_B_o_nucx.sys_to_stats fileA
    | U_NU   ->   Ldd_B_u_nu.sys_to_stats fileA
    | U_NUX  ->  Ldd_B_u_nux.sys_to_stats fileA
    | U_NUC  ->  Ldd_B_u_nuc.sys_to_stats fileA
    | U_NUCX -> Ldd_B_u_nucx.sys_to_stats fileA
    | L_NNUX -> assert false
    in
    OfSTree.file stats fileB

  let to_cntsat modele fileA fileB args =
    let cntsat = match modele with
    | O_U    -> failwith "[ConvCore] noconv .bdd.B.pure -> .cntsat"
    | O_NU   ->   Ldd_B_o_nu_advanced.sys_to_cntsat fileA
    | O_C10  -> failwith "[ConvCore] noconv .zdd.B.pure -> .cntsat"
    | O_UC0  -> failwith "[ConvCore] noconv .o-uc0.B.pure -> .cntsat"
    | O_NUCX -> failwith "[ConvCore] noconv .o-nucx.B.pure -> .cntsat"
    | U_NU|U_NUX|U_NUC|U_NUCX|L_NNUX -> assert false
    in
    OfSTree.file cntsat fileB;
    ()
end
