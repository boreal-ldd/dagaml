(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrLoadCnf : file parsing of (Q)DIMACS file
 *)

open GuaCaml
open Extra
open STools
open CnfTypes
open ParseUtils

module Stream = Iter.Stream

let my_lexer (stream:char Stream.t) : BasicLexer.stream =
  BasicLexer.lexer
    ~is_rmline:BasicLexer.dimacs_is_rmline
     stream

let pstring x = print_string x; print_string " "; flush stdout
(* let pstring x = () *)
let pchar x = print_char x; print_char ' '; flush stdout
(* let pchar x = () *)

let parse_clause stream =
  let rec aux carry =
    let x = BasicLexer.parse_int stream in
    if x = 0
    then (List.rev carry)
    else aux ((if x < 0 then (-x-1, true) else (x-1, false))::carry)
  in aux []

let rec parse_formule ?(carry=[]) (nclause:int) (stream:BasicLexer.stream)  =
  if nclause = 0
  then (List.rev carry)
  else (
    parse_formule
      ~carry:((parse_clause stream)::carry)
      (pred nclause)
       stream
  )

let parse_quant stream =
  match Stream.peek stream with
  | Some(BasicLexer.Str "a") -> (
    Stream.junk stream;
    Some(false, parse_clause stream ||> fst)
  )
  | Some(BasicLexer.Str "e") -> (
    Stream.junk stream;
    Some(true , parse_clause stream ||> fst)
  )
  |                      _   ->
    None

let rec parse_quants ?(carry=[]) stream =
  match parse_quant stream with
  | None -> List.rev carry
  | Some q -> (
    parse_quants ~carry:(q::carry) stream
  )

let parse_qbf_module ?(sort=true) ?(simpl=true) stream =
  assert(BasicLexer.parse_str stream = "p");
  assert(BasicLexer.parse_str stream = "cnf");
  let input = BasicLexer.parse_int stream in
  let nbformule = BasicLexer.parse_int stream in
  let quants = parse_quants stream in
  let formule = parse_formule nbformule stream in
  let formule = if sort
    then (formule ||> SetList.sort)
    else  formule
  in
  let formule = if simpl
    then (
      match CnfUtils.formule_trivial formule with
      | Some f -> f
      | None -> (
        print_endline "[warning] [DAGaml.StrLoadCnf.parse_qbf_module] the parsed formule is trivially inconsistent";
        [[]]
      )
    )
    else formule
  in
  CSTypes.{input; name = "cnf"; quants; man = (); formule}

let parse_module ?(sort=true) ?(simpl=true) stream =
  let cnf = parse_qbf_module ~sort ~simpl stream in
  assert(cnf.quants = []);
  (* Checks that the formula is a CNF and not a QCNF *)
  cnf

let load_qbf ?(sort=true) stream =
  stream |> my_lexer |> parse_qbf_module ~sort

let load ?(sort=true) ?(simpl=true) stream =
  stream |> my_lexer |> parse_module ~sort ~simpl

let load_qbf_file ?(sort=true) ?(simpl=true) ?(discardQ=false) target =
  print_endline ("[StrLoadCnf.load_qbf_file] target:"^target);
  let file = open_in target in
  let cnf = load_qbf ~sort (Stream.of_channel_as_char file) in
  if sort
    then if not(CnfUtils.file_sorted cnf)
      then (
        print_endline ("cnf:"^(CnfUtils.ToS.system cnf));
        StrDumpCnf.dump_qbf print_string cnf;
        print_newline();
        failwith "[StrLoadCnf.load_qbf_file] inconsistent state (parsed is not sorted), please contact development team";
      );
  close_in file;
  print_endline ("[StrLoadCnf.load_qbf_file] parsed!");
  if discardQ
  then {cnf with quants = []}
  else  cnf

let load_file ?(sort=true) ?(simpl=true) target : CnfTypes.system =
  print_endline ("[StrLoadCnf.load_file] target:"^target);
  let file = open_in target in
  let cnf = load ~sort (Stream.of_channel_as_char file) in
  if sort then assert(CnfUtils.file_sorted cnf);
  close_in file;
  print_endline ("[StrLoadCnf.load_file] parsed!");
  cnf

