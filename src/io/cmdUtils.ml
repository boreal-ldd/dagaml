(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CmdUtils : Cloud-DD's command language [WIP]
 *)

(* All Right Reserved

   Copyright (c) 2020-2020 Joan Thibault
*)

open GuaCaml
open Extra
open CmdTypes

module PrettyToS =
struct

  let var v = v
  let f (b, s) =
    if b then ("!"^s) else s

  let cmd_vl cmd vl =
    String.concat " " (cmd::(vl ||> var))
  let cmd_fl cmd fl =
    String.concat " " (cmd::(fl ||> f  ))
  let cmd_set_fl cmd v fl =
    String.concat " " (cmd::(var v)::(fl ||> f  ))

  let filter_cmd = function
    | Var []
    | Delete []
    | Info []
    | Count [] -> None
    | cmd -> Some cmd

  let filter_prg prg = MyList.opmap filter_cmd prg

  let cmd = function
    | Comment s   -> "#"^(String.escaped s)
    | Var vl      -> cmd_vl "var" vl
    | Delete vl   -> cmd_vl "delete" vl
    | Info fl     -> cmd_fl "info" fl
    | Collect     -> "collect"
    | Status      -> "status"
    | Count fl    -> cmd_fl "count" fl
    | Satisfy fl  -> cmd_fl "satisfy" fl
    | And (v, fl) -> cmd_set_fl "and" v fl
    | Or  (v, fl) -> cmd_set_fl "or"  v fl
    | Xor (v, fl) -> cmd_set_fl "xor" v fl
    | Ite (v, fc, f0, f1) -> cmd_set_fl "ite" v [fc; f0; f1]
    | Equal (f0, f1) -> cmd_fl "equal" [f0; f1]
    | Not (v, fx) -> cmd_set_fl "not" v [fx]
    | Time -> "time"
    | Flush -> "flush"
    | Quit -> "quit"

  let prg p = String.concat "\n" (p |> filter_prg ||> cmd)
end

module ToExpr =
struct
  let rec find_inputs ?(carry=[]) = function
    | [] -> List.rev carry
    | cmd::prg -> (
      match cmd with
      | Var vl ->
        find_inputs ~carry:(List.rev_append vl carry) prg
      | _ -> find_inputs ~carry prg
    )

  type sset = (string * (string Expr.expr)) list

  let rec convert_prg ?(carry=([]:sset)) (ns:NameSpace.var_type NameSpace.t) (prg:prg) : sset =
    let expr_of_f ((b, v):f) =
      let v' = NameSpace.get ns v in
      if b then Expr.(no (PVar v')) else Expr.(PVar v')
    in
    let expr_of_fl (fl:fl) = Tools.map expr_of_f fl in
    let outs_push_f (sset:sset) ((b, v):f) : sset =
      let v' = NameSpace.push ns ~tag:NameSpace.Output v in
      let e = if b then Expr.no(PVar v) else (PVar v) in
      (v', e) :: sset
    in
    let outs_push_fl (sset:sset) (fl:fl) =
      List.fold_left outs_push_f sset fl
    in
    let finalize () = List.rev carry in
    match prg with
    | [] -> finalize ()
    | cmd::prg -> (
      match cmd with
      | Comment _ -> convert_prg ~carry ns prg
      | Var _     -> convert_prg ~carry ns prg
      | Delete vl -> (
        List.iter (NameSpace.delete ns) vl;
        convert_prg ~carry ns prg
      )
      | Info fl ->
        (convert_prg ~carry:(outs_push_fl carry fl) ns prg)
      | Collect -> convert_prg ~carry ns prg
      | Status  -> convert_prg ~carry ns prg
      | Count fl ->
        (convert_prg ~carry:(outs_push_fl carry fl) ns prg)
      | Satisfy fl ->
        (convert_prg ~carry:(outs_push_fl carry fl) ns prg)
      | And (v, fl) -> (
        let el = expr_of_fl fl in
        let v' = NameSpace.push ns v in
        convert_prg ~carry:((v', Expr.andl el)::carry) ns prg
      )
      | Or (v, fl) -> (
        let el = expr_of_fl fl in
        let v' = NameSpace.push ns v in
        convert_prg ~carry:((v', Expr.orl  el)::carry) ns prg
      )
      | Xor (v, fl) -> (
        let el = expr_of_fl fl in
        let v' = NameSpace.push ns v in
        convert_prg ~carry:((v', Expr.xorl el)::carry) ns prg
      )
      | Ite(v, fc, f1, f0) -> (
        let ec = expr_of_f fc in
        let e1 = expr_of_f f1 in
        let e0 = expr_of_f f0 in
        let v' = NameSpace.push ns v in
        convert_prg ~carry:((v', Expr.ite ec e1 e0)::carry) ns prg
      )
      | Equal (f0, f1) ->
        (convert_prg ~carry:(outs_push_fl carry [f0; f1]) ns prg)
      | Not (v, f) -> (
        let e = expr_of_f f in
        let v' = NameSpace.push ns v in
        convert_prg ~carry:((v', Expr.no e)::carry) ns prg
      )
      | Time  -> convert_prg ~carry ns prg
      | Flush -> convert_prg ~carry ns prg
      | Quit  -> finalize()
    )

  (* variable overide is forbiden for input variables *)
  (* the name of intermediary variables is forgotten *)
  let convert ?(name="cmd") (prg:prg) : string IoTypes.expr =
    let inps = find_inputs prg in
    let len_prg = List.length prg in
    let name_space = NameSpace.newman ~sep:"-" NameSpace.Wire ~h_size:(List.length inps + len_prg) () in
    (* variable overide is forbiden for input variables *)
    List.iter (fun inp -> ignore(NameSpace.push' name_space ~tag:NameSpace.Input inp)) inps;
    let sset = convert_prg name_space prg in
    let inps', wire', outs' = NameSpace.split_var_type ~rev:false name_space.NameSpace.wires in
    if not (inps = inps')
    then (
      print_endline ("inps :"^(STools.ToS.(list string) inps ));
      print_endline ("inps':"^(STools.ToS.(list string) inps'));
    );
    let inps = Array.of_list inps in
    let regs = [||] in
    let wire = Array.of_list wire' in
    let outs = Array.of_list outs' in
    let sset = Array.of_list sset in
    IoTypes.{name; man = (); inps; regs; wire; outs; sset}
end

module OfExpr =
struct
  let convert (sys:string IoTypes.expr) : prg =
    print_endline "[CmdUtils.OfExpr.convert] [0]";
    let nax = IoUtils.nax_of_expr sys in
    print_endline "[CmdUtils.OfExpr.convert] [1]";
    let prg = Nax.ToCmd.export nax in
    print_endline "[CmdUtils.OfExpr.convert] [2]";
    prg
end
