(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrLoadCmd : file parsing for Cmd language
 *)

open GuaCaml
open Extra
open STools
open CmdTypes
open CmdUtils
open ParseUtils

module Stream = Iter.Stream

let my_lexer (stream:char Stream.t) : CmtLexer.stream =
  CmtLexer.lexer
    ~is_cmtline:CmtLexer.python_is_rmline
     stream

let pstring x = print_string x; print_string " "; flush stdout
(* let pstring x = () *)
let pchar x = print_char x; print_char ' '; flush stdout
(* let pchar x = () *)

let var_of_string (str:string) : var =
  assert(String.length str > 0);
  assert(str.[0] <> '!');
  str

let parse_var s : var =
  var_of_string(CmtLexer.parse_str s)

let f_of_string (str:string) : f =
  assert(String.length str > 0);
  if str.[0] = '!'
  then (
    let str = String.(sub str 1 (length str -1)) in
    assert(String.length str > 0);
    (true , str)
  )
  else
    (false, str)

let parse_f s : f =
  f_of_string(CmtLexer.parse_str s)

let is_cmd : string -> bool = function
  | "var"
  | "delete"
  | "info"
  | "collect"
  | "status"
  | "count"
  | "satisfy"
  | "and"
  | "or"
  | "xor"
  | "ite"
  | "equal"
  | "not"
  | "time"
  | "flush"
  | "quit" -> true
  | _     -> false

let rec parse_fl ?(carry=[]) s : fl =
  match Stream.peek s with
  | Some(CmtLexer.Str str) when not(is_cmd str) -> (
    Stream.junk s;
    parse_fl ~carry:((f_of_string str)::carry) s
  )
  | _ -> List.rev carry

let rec parse_vl ?(carry=[]) s : var list =
  match Stream.peek s with
  | Some(CmtLexer.Str str) when not(is_cmd str) -> (
    Stream.junk s;
    parse_vl ~carry:((var_of_string str)::carry) s
  )
  | _ -> List.rev carry

let parse_cmd (tk:CmtLexer.token) s : cmd =
  match tk with
  | CmtLexer.Cmt str -> Comment str
  | CmtLexer.Str str -> (
    if not(is_cmd str)
    then (failwith ("[StrLoadCmd.parse_cmd] (not cmd) str:"^(String.escaped str)));
    match str with
    | "var"    -> Var(parse_vl s)
    | "delete" -> Delete (parse_vl s)
    | "info"   -> Info(parse_fl s)
    | "collect" -> Collect
    | "status"  -> Status
    | "count"   -> Count(parse_fl s)
    | "satisfy" -> Satisfy(parse_fl s)
    | "and"     -> (
      let v = parse_var s in
      let fl = parse_fl s in
      And(v, fl)
    )
    | "or"      -> (
      let v = parse_var s in
      let fl = parse_fl s in
      Or(v, fl)
    )
    | "xor"     -> (
      let v = parse_var s in
      let fl = parse_fl s in
      Xor(v, fl)
    )
    | "ite"     -> (
      let v = parse_var s in
      let fc = parse_f s in
      let f1 = parse_f s in
      let f0 = parse_f s in
      Ite(v, fc, f1, f0)
    )
    | "equal"   -> (
      let f0 = parse_f s in
      let f1 = parse_f s in
      Equal(f0, f1)
    )
    | "not"     -> (
      let v = parse_var s in
      let f = parse_f s in
      Not(v, f)
    )
    | "time" -> Time
    | "flush" -> Flush
    | "quit" -> Quit
    | _ -> assert false
  )
  | CmtLexer.Int _ -> assert false

let rec parse_prg ?(carry=[]) s : prg =
  match Stream.peek s with
  | None -> List.rev carry
  | Some tk -> (
    Stream.junk s;
    let cmd = parse_cmd tk s in
    parse_prg ~carry:(cmd::carry) s
  )

let load stream =
  stream |> my_lexer |> parse_prg

let load_file target =
  let file = open_in target in
  let prg = load (Stream.of_channel_as_char file) in
  close_in file;
  prg

let load_expr stream =
  load stream |> ToExpr.convert

let load_expr_file target =
  load_file target |> ToExpr.convert
