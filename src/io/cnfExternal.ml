(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CnfExternal : Interfacing with External CNF-based Solver and Compilers
 *)

open GuaCaml
open Extra
open STools
open CnfTypes

let prefix = "DAGaml.CnfExternal"

module D4 =
struct
  let prefix = prefix ^ ".D4"
  let temp_file ?(prefix="") suffix : string =
    Filename.temp_file ("cnf_external_d4_"^prefix^"_") suffix

  let d4 = "/bin/d4"

  let is_false ?(verbose=false) (file_out:string) : bool =
    let prefix = "["^prefix^".is_false]" in
    let cha = open_in file_out in
    let text = Tools.input_lines cha in
    close_in cha;
    let ifind_query s =
      if String.length s > 0 && s.[0] = 's'
      then (
        if verbose then print_endline (prefix^" s:"^s);
        let line = String.split_on_char ' ' s in
        assert(List.length line > 1);
        Some(List.nth line 1 = "0")
      )
      else None
    in
    match MyList.ifind ifind_query text with
    | None -> failwith (prefix^" inconsistency at the interface with D4")
    | Some (_, v) -> v

  let make_false (file:file) : string IoTypes.expr =
    let get_var i = "x"^(string_of_int i) in
    IoTypes.{
      name = "CnfExternalD4MakeFalse";
      man  = ();
      inps = Array.init file.input get_var;
      regs = [||];
      wire = [||];
      outs = [|"y0"|];
      sset = [|("y0", Expr.PCst false)|];
    }

  let compute ?(verbose=false) (file:file) : string IoTypes.expr =
    let prefix = "["^prefix^".compute]" in
    let file_cnf = temp_file ".cnf" in
    let file_nnf = temp_file ".nnf" in
    let file_out = temp_file ".out" in
    StrDumpCnf.dump_file file_cnf file;
    let command =
      d4^" "^file_cnf^" -out=\""^file_nnf^"\" > "^file_out
    in
    if verbose then print_endline (prefix^"  cnf(inputs):"^(ToS.int  file.input ));
    if verbose then print_endline (prefix^" cnf(formule):"^(ToS.int(List.length file.formule)));
    let status = Sys.command command in
    if verbose then print_endline (prefix^" status:"^(ToS.int status));
    let expr =
      if is_false ~verbose file_out
      then make_false file
      else StrLoadNnf.load_file file_nnf
    in
    Sys.remove file_cnf;
    Sys.remove file_nnf;
    Sys.remove file_out;
    expr
end
