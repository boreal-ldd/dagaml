(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrDumpCmd : file generation for Cmd language
 *)

open GuaCaml
open CmdTypes
open CmdUtils

(* [TODO] line-based writting to avoid peak size effect *)
let dump (output : string -> unit) (prg:prg) : unit =
  output (PrettyToS.prg prg)

let dump_file (file:string) (prg:prg) : unit =
  let fout = open_out file in
  dump (output_string fout) prg;
  close_out fout
