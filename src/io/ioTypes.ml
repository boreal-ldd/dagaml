(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : IoTypes : standard exchange representation
 *)

type ('s, 't, 'f) system = {
  name :  string;         (* name of the system     *)
(* if [name] is an empty string, the system's name if exported is implementation dependent *)
  man  : 't;              (* the manager of ['f]s   *)
  inps : 's array;        (* input array            *)
  regs : 's array;        (* register variables     *)
  wire : 's array;        (* intermediary variables *)
  outs : 's array;        (* output variables       *)
  sset : ('s * 'f) array; (* assignments : ['f]s    *)
}
(** the [sset] array must be asignable in order
 **)

type 's expr = ('s, unit, 's Expr.expr) system
