(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CirTypes : Circuit (constraint system) types
 *)

open GuaCaml

module CSTypes = Constraint_system_types

type clause = int Expr.expr

type formule = clause CSTypes.formule

type system = (unit, clause) CSTypes.system

module ToS =
struct
  include CSTypes.ToS

  let clause (clause:clause) : string =
    Expr.ToS.expr int clause

  let formule (f:formule) : string =
    formule clause f

  let system (f:system) : string =
    system unit clause f
end
