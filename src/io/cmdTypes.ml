(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CmdTypes : Cloud-DD's command language [WIP]
 *)

type var = string

(** function vector
  bool = false => identity
  bool = ture  => negation
 **)
type f = bool * string
type fl = f list

type cmd =
  | Comment of string
  | Var of var list (* variable list *)
  | Delete of var list (* intermediate value list*)
  | Info of fl
  | Collect
  | Status
  | Count of fl
  | Satisfy of fl
  | And of var * fl
  (* 'Cst v true ' is implemented as 'And v []' *)
  | Or of var * fl
  (* 'nor' is implemented with 'and' *)
  (* 'Cst v false' is implemented as 'Or  v []' *)
  | Xor of var * fl
  (* Ite (var, test, if1, if0) *)
  | Ite of var * f * f * f
  (* 'cst dest var' is implemented
    match var with
    | false -> Or var []
    | true  -> And var []
   *)
  | Equal of f * f
  | Not of var * f
  | Time
  | Flush
  | Quit

type prg = cmd list
