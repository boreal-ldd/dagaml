(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrLoadVerilog : file parsing for Verilog
 *)

open GuaCaml
open STools
open Expr
open IoUtils

module Stream = Iter.Stream

type token =
  | Kwd of string
  | Sym of char
  | Ident of string

let my_lexer stream : token Stream.t =
  let is_space = function ' ' | '\t' | '\n' -> true | _ -> false in
  let rec spaces () = match Stream.peek stream with
  | None -> ()
  | Some head -> if is_space head then (Stream.junk stream; spaces ()) else ()
  in
  let is_symbol = function '=' | '~' | '&' | '|' | '^' | '(' | ')' | ';' | ',' -> true | _ -> false in
  let is_kwd = function
    | "assign"
    | "module"
    | "endmodule"
    | "input"
    | "output"
    | "wire"
    | "1'b0"
    | "1'b1"   -> true
    | _ -> false
  in
  let word () =
    let rec aux s = match Stream.peek stream with
      | None -> SUtils.implode (List.rev s)
      | Some c ->
      ( if is_space c || is_symbol c
        then (SUtils.implode (List.rev s))
        else (Stream.junk stream; aux (c::s))
      )
    in
    let w = aux [] in
    if is_kwd w
      then Kwd w
      else Ident w
  in
  let aux x : token option=
    spaces ();
    match Stream.peek stream with
    | None -> None
    | Some head ->
    Some (
      assert(not(is_space head));
      if is_symbol head
      then (Stream.junk stream; Sym head)
      else (word ())
    )
  in
  Stream.from aux 0

(*let pstring x = print_string x; print_string " "; flush stdout*)
let pstring _ = ()
(*let pchar x = print_char x; print_char ' '; flush stdout*)
let pchar _ = ()

let rec parse_leaf stream = match Stream.next stream with
  | Ident var -> pstring var; PVar var
  | Sym '(' ->
  (
    pstring "(";
    let expr = parse_expr stream in
    PUop (PNop, expr)
  )
  | Sym '~' ->
  (
    pstring "~";
    let expr = parse_leaf stream in
    PUop (PNot, expr)
  )
  | Kwd "1'b0" ->
  (
    pstring "1'b0";
    PCst false
  )
  | Kwd "1'b1" ->
  (
    pstring "1'b1";
    PCst true
  )
  | _ -> assert false

and     parse_expr stream =
  let first = parse_leaf stream in
  match Stream.next stream with
  | Sym ';' -> pstring "\n"; first
  | Sym ')' -> pstring ")"; first
  | Sym '&' ->
  (
    pstring "&";
    let second = parse_expr stream in
    PBop (PAnd, first, second)
  )
  | Sym '|' ->
  (
    pstring "|";
    let second = parse_expr stream in
    PBop (POr, first, second)
  )
  | Sym '^' ->
  (
    pstring "^";
    let second = parse_expr stream in
    PBop (PXor, first, second)
  )
  | x -> assert false

let parse_ident stream = match Stream.next stream with
  | Ident name -> name
  | _ -> assert false

let rec parse_coma_list stream =
  let rec aux carry = match Stream.peek stream with
  | None -> assert false
  | Some head -> match head with
    | Sym ',' ->
    (
      Stream.junk stream;
      let ident = parse_ident stream in
      aux (ident::carry)
    )
    | _ -> List.rev carry
  in
  match Stream.next stream with
  | Ident ident -> aux [ident]
  | _ -> assert false

let parse_braket_list stream = match Stream.next stream with
  | Sym '(' ->
  (
    let liste = parse_coma_list stream in
    match Stream.next stream with
    | Sym ')' -> liste
    | _ -> assert false
(*parse_coma_list stream*)
  )
  | _ -> assert false

let parse_semicolon stream = match Stream.next stream with
  | Sym ';' -> ()
  | _ -> assert false

let parse_semicolon_list stream =
  let liste = parse_coma_list stream in
  parse_semicolon stream;
  liste

let parse_kwd stream = match Stream.next stream with
  | Kwd kwd -> kwd
  | _ -> assert false

let parse_sym stream = match Stream.next stream with
  | Sym sym -> sym
  | _ -> assert false

let parse_assign stream =
  let ident = parse_ident stream in
  assert('=' = parse_sym stream);
  let expr = parse_expr stream in
  (ident, expr)

let parse_module_aux mymodule stream =
  let reform l =
    Array.of_list(MyList.flatten(List.rev l))
  in
  let rec aux input output wire assign =
  match Stream.next stream with
  | Kwd "endmodule" -> IoTypes.{
    name = mymodule.name;
    man  = ();
    inps = reform input;
    regs = [||];
    wire = reform wire;
    outs = reform output;
    sset = assign |> List.rev |> Array.of_list;
  }
  | Kwd "input" ->
  (
    let liste = parse_semicolon_list stream in
    aux (liste::input) output wire assign
  )
  | Kwd "output" ->
  (
    let liste = parse_semicolon_list stream in
    aux input (liste::output) wire assign
  )
  | Kwd "wire" ->
  (
    let liste = parse_semicolon_list stream in
    aux input output (liste::wire) assign
  )
  | Kwd "assign" ->
  (
    let item = parse_assign stream in
    aux input output wire (item::assign)
  )
  | _ -> assert false
  in aux
    [mymodule.inps |> Array.to_list ]
    [mymodule.outs |> Array.to_list ]
    [mymodule.wire |> Array.to_list ]
    (mymodule.sset |> Array.to_list |> List.rev)

let parse_module stream =
  let rec aux () = match Stream.next stream with
  | Kwd "module" ->
  (
    let name = parse_ident stream in
    let param = parse_braket_list stream in
    parse_semicolon stream;
    let module_expr = IoTypes.{
      name;
      man = ();
      inps = [||];
      regs = [||];
      wire = [||];
      outs = [||];
      sset = [||];
    } in
    let mymodule = parse_module_aux module_expr stream in
    assert((Array.length mymodule.inps)
      + (Array.length mymodule.outs) = List.length param);
    print_newline();
    mymodule;
  )
  | _ -> aux ()
  in aux ()

let load stream = stream |> my_lexer |> parse_module

let load_file file : string IoTypes.expr =
  let fin = open_in file in
  let mexpr = load (Stream.of_channel_as_char fin) in
  close_in fin;
  mexpr
