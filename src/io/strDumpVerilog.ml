(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2018-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrDumpVerilog : file generation for Verilog
 *)

open GuaCaml
open IoUtils

let strdump_expr ?(mode="aig") expr =
  match mode with
  | "aig" -> (
    RExpr.strdump
    (fun x -> x)
    (function true -> "1'b1" | false -> "1'b0")
    RExpr.(function RAnd -> " & " | RXor -> " ^ ")
    "~"
    (RExpr.rexpr_of_expr expr)
  )
  | "nnf" -> (
    (* print_endline (Expr.ToS.expr (fun x -> x) expr); *)
    let nnf = NnfExpr.rexpr_of_expr expr in
    (* print_endline (NnfExpr.ToS.expr (fun x -> x) nnf); *)
    NnfExpr.strdump
    (fun x -> x)
    (function true -> "1'b1" | false -> "1'b0")
    (function true -> " | "  | false -> " & ")
    (function true -> "~"    | false -> "")
    nnf
  )
  | _ -> assert false

let dump ?(mode="aig") (output : string -> unit) (sys:string IoTypes.expr) =
  output ("module "^sys.name^" (");
  let params = Array.append sys.inps sys.outs in
  let output_concat a =
    output(String.concat ", " (Array.to_list a))
  in
  output_concat params;
  output ")";
  if Array.length sys.inps > 0
  then (
    output ";\n\tinput ";
    output_concat sys.inps;
  );
  output ";\n\toutput ";
  output_concat sys.outs;
  if Array.length sys.wire > 0
  then (
    output ";\n\twire ";
    output_concat sys.wire;
  );
  output ";\n";
  let dump_assign (name, expr) =
    output ("\tassign "^name^" = "^(strdump_expr ~mode expr)^";\n")
  in
  Array.iter dump_assign sys.sset;
  output "endmodule\n"

let dump_file ?(mode="aig") file mexpr =
  let fout = open_out file in
  dump ~mode (output_string fout) mexpr;
  close_out fout
