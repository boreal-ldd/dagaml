(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : IoUtils : Toolbox for IoTypes
 *)

open GuaCaml
open Extra
open STools
open BTools
open IoTypes

module ToF :
sig
  open Io.ToF
  val system : 's t -> ('t * ('f array)) t -> ('s, 't, 'f) IoTypes.system t
end
=
struct
  open Io.ToF

  let system
      (tof_s:'s t)
      (tof_man:('t * ('f array)) t)
      (cha:out_channel)
      (s:('s, 't, 'f) IoTypes.system) : unit =
    string cha s.name;
    array tof_s cha s.inps;
    array tof_s cha s.regs;
    array tof_s cha s.wire;
    array tof_s cha s.outs;
    let sset_s, sset_f = MyArray.split s.sset in
    array tof_s cha sset_s;
    tof_man cha (s.man, sset_f);
    ()
end

module OfF :
sig
  open Io.OfF
  val system : 's t -> ('t * ('f array)) t -> ('s, 't, 'f) IoTypes.system t
end
=
struct
  open Io.OfF

  let system
      (off_s:'s t)
      (off_man:('t * ('f array)) t)
      (cha:in_channel) : ('s, 't, 'f) IoTypes.system =
    let name = string cha in
    let inps = array off_s cha in
    let regs = array off_s cha in
    let wire = array off_s cha in
    let outs = array off_s cha in
    let sset_s = array off_s cha in
    let man, sset_f = off_man cha in
    let sset = MyArray.combine sset_s sset_f in
    {name; man; inps; regs; wire; outs; sset}
end

module ToBStream :
sig
  val system : 's bw -> ('t * ('f array)) bw -> ('s, 't, 'f) IoTypes.system bw
end
=
struct
  open ToBStream

  let system
      (bw_s:'s bw)
      (bw_man:('t * ('f array)) bw)
      (cha:Channel.t)
      (s:('s, 't, 'f) IoTypes.system) : unit =
    string cha s.name;
    array bw_s cha s.inps;
    array bw_s cha s.regs;
    array bw_s cha s.wire;
    array bw_s cha s.outs;
    let sset_s, sset_f = MyArray.split s.sset in
    array bw_s cha sset_s;
    bw_man cha (s.man, sset_f);
    ()
end

module OfBStream :
sig
  val system : 's br -> ('t * ('f array)) br -> ('s, 't, 'f) IoTypes.system br
end
=
struct
  open OfBStream

  let system
      (br_s:'s br)
      (br_man:('t * ('f array)) br)
      (cha:Channel.t) : ('s, 't, 'f) IoTypes.system =
    let name = string cha in
    let inps = array br_s cha in
    let regs = array br_s cha in
    let wire = array br_s cha in
    let outs = array br_s cha in
    let sset_s = array br_s cha in
    let man, sset_f = br_man cha in
    let sset = MyArray.combine sset_s sset_f in
    {name; man; inps; regs; wire; outs; sset}
end

(* [TODO] add sanity check *)
let import_expr
    (cst : bool -> int -> 'f)
    (var : int  -> 'f array)
    ((&!): 'f -> 'f -> 'f)
    ((^!): 'f -> 'f -> 'f)
    (neg : 'f -> 'f)
    (sys : 's expr) (man:'t) : ('s, 't, 'f) system =
  let inputs = Array.append sys.inps sys.regs in
  let nvars = Array.length inputs in
  let vars = var nvars in
  let size = nvars + Array.(
    (length sys.wire) +
    (length sys.outs)
  ) in
  let memo : ('s, 'f) Hashtbl.t = Hashtbl.create size in
  (* initialize the table with [sys.inps] and [sys.regs] *)
  Array.iter2 (Hashtbl.add memo) inputs vars;
  let rec eval_expr = function
  | Expr.PCst b -> cst b nvars
  | Expr.PVar v -> Hashtbl.find memo v
  | Expr.PUop(uop, e) ->
  (
    let f = eval_expr e in
    match uop with
    | Expr.PNop ->     f
    | Expr.PNot -> neg f
  )
  | Expr.PBop (bop, e0, e1) ->
  (
    let f0 = eval_expr e0
    and f1 = eval_expr e1 in
    match bop with
    | Expr.PAnd  ->          f0  &!      f1
    | Expr.POr   -> neg((neg f0) &! (neg f1))
    | Expr.PImp  -> neg(     f0  &! (neg f1))
    | Expr.PIff  -> neg(     f0  ^!      f1)
    | Expr.PXor  ->          f0  ^!      f1
  )
  in
  for i = 0 to Array.length sys.sset - 1
  do
    let s, e = sys.sset.(i) in
    try
      Hashtbl.add memo s (eval_expr e)
    with err ->
      let stre = Expr.to_string (fun _ -> " _ ") e in
      print_string ("[IoUtils] error @sset["^(string_of_int i)^"] := "^stre^";"); print_newline();
      raise err
  done;
  let funmap s = (s, Hashtbl.find memo s) in
  let outs = Array.map funmap sys.outs in
  let regs = Array.map funmap sys.regs in
  {
    name = sys.name;
    man;
    inps = sys.inps;
    regs = sys.regs;
    wire = [||];
    outs = sys.outs;
    sset = Array.append outs regs
  }

(* normalize 's expr =
  (int expr,
   int -> 's,
   int -> number of inputs
    => input wires have no inputs
   int -> number of output
    => output wires may have outputs
   int -> [true] if is input
   int -> [true] if is output)
 *)
let normalize_system (sys:'s expr) : (int expr) * ('s array) * (int array) * (int array) * (bool array) * (bool array) =
  let size = Array.(
    length sys.inps +
    length sys.regs +
    length sys.wire +
    length sys.outs)
  in
  let array = Array.make size None in
  let ninput = Array.make size 0 in
  let noutput = Array.make size 0 in
  let is_input = Array.make size false in
  let is_output = Array.make size false in
  let assoc = Hashtbl.create size in
  let index = ref 0 in
  (* initialize the table with [sys.inps] and [sys.regs] *)
  Array.iter (fun name ->
    let index = !++ index in
    array.(index) <- Some name;
    is_input.(index) <- true;
    Hashtbl.add assoc name index
  ) (Array.append sys.inps sys.regs);
  (* keep filling the table with [sys.sset] *)
  let sset = Array.map (fun (s, expr) ->
    let index = !++ index in
    array.(index) <- Some s;
    ninput.(index) <- Expr.count_var expr;
    let expr' =
      Expr.map_var (fun v ->
        let i = Hashtbl.find assoc v in
        noutput.(i) <- succ noutput.(i);
        i) expr
    in
    Hashtbl.add assoc s index;
    (index, expr')
    ) sys.sset in
  Array.iter (fun s ->
    let i = Hashtbl.find assoc s in
    is_output.(i) <- true;
    ) sys.outs;
  let funmap s = Hashtbl.find assoc s in
  let sys = {
    name = sys.name;
    man = ();
    inps = Array.map funmap sys.inps;
    regs = Array.map funmap sys.regs;
    wire = Array.map funmap sys.wire;
    outs = Array.map funmap sys.outs;
    sset;
  } in
  (sys, MyArray.map_unop array, ninput, noutput, is_input, is_output)

let snax_of_expr sys : _ Snax.system =
  let man = Snax.Module.G1.newman() in
  let push tag x y = Snax.Module.G1.push man (tag, x, y) in
  import_expr
    (fun cst size -> (cst, Tree.GLeaf None))
    (fun     size -> Array.init size (fun x -> (false, Tree.GLeaf(Some x))))
    (fun edge0 edge1 -> push Snax.And edge0 edge1)
    (fun edge0 edge1 -> push Snax.Xor edge0 edge1)
    (fun (neg, node) -> (not neg, node))
    sys man

let nax_of_expr sys : _ Nax.system =
  let man = Nax.NAX.G1.newman() in
  let push tag x y = Nax.NAX.G1.push man (tag, x, y) in
  import_expr
    (fun cst size -> (cst, Tree.GLeaf None))
    (fun     size -> Array.init size (fun x -> (false, Tree.GLeaf(Some x))))
    (fun edge0 edge1 -> push Nax.And edge0 edge1)
    (fun edge0 edge1 -> push Nax.Xor edge0 edge1)
    (fun (neg, node) -> (not neg, node))
    sys man

(* FIXME :
    assume sys0.wire = [||]
    assume sys0.regs = [||]
    assume sys1.wire = [||]
    assume sys1.regs = [||]
 *)
let compose
  (sys0:('s0, 't0, 'f0) system)
  (sys1:(int, 't1, int Expr.expr) system)
  (fnn : 's0 array -> int -> 's0 array)
(*  fnn : Find New Names
    [fnn used num = names]
    ensures (Array.length names = num)
    ensures (\forall x\in used, y\in names, x <> y)
        aka (used \inter names = \emptyset)
 *)
    : ('s0, 't1, 's0 Expr.expr) system =
  assert(Array.length sys0.wire = 0);
  assert(Array.length sys0.regs = 0);
  assert(Array.length sys0.sset = Array.length sys0.outs);
  let names = Array.append sys0.inps sys0.outs in
  let nwire = Array.length sys1.wire in
  let wire = fnn names nwire in
  let remap = Array.make (Array.length names + nwire) None in
  Array.iteri (fun i x -> remap.(i) <- Some x) sys0.inps;
  Array.iter2 (fun x y -> remap.(x) <- Some y) sys1.wire wire;
  Array.iter2 (fun x y -> remap.(x) <- Some y) sys1.outs sys0.outs;
  let remap = Array.map Tools.unop remap in
  let funmap (s, e) =
    (remap.(s), Expr.map_var (Array.get remap) e)
  in
  let sset = Array.map funmap sys1.sset in
  {
    name = sys0.name;
    man  = ();
    inps = sys0.inps;
    regs = sys0.regs;
    wire;
    outs = sys0.outs;
    sset;
  }

(* FIXME :
    assume sys.wire = [||]
    assume sys.regs = [||]
 *)
let expr_of_nax (sys0:string Nax.system)
  (fnn : 's array -> int -> 's array) : 's expr =
(*  fnn : find new names
    [fnn used num = names]
    ensures (Array.length names = num)
    ensures (\forall x\in used, y\in names, x <> y)
        aka (used \inter names = \emptyset)
 *)
  assert(Array.length sys0.wire = 0);
  assert(Array.length sys0.regs = 0);
  let nvars = Array.length sys0.inps in
  assert(Array.length sys0.sset = Array.length sys0.outs);
  let outputs = Array.map snd sys0.sset in
  let sys1 = Nax.ToSysExpr.export sys0.man nvars outputs in
  compose sys0 sys1 fnn

let nax_of_snax (sys:'s Snax.system) : 's Nax.system =
  let nax = Nax.NAX.G1.newman() in
  let tonax = Snax.Module.TO_NAX.newman sys.man nax in
  let apply = Snax.Module.TO_NAX.rec_edge tonax in
  {
    name = sys.name;
    man  = nax;
    inps = sys.inps;
    regs = sys.regs;
    wire = sys.wire;
    outs = sys.outs;
    sset = Array.map (fun (s, e) -> (s, apply e)) sys.sset;
  }

let expr_of_snax sys fnn =
  expr_of_nax (nax_of_snax sys) fnn

(*  fnn : find new names
    [fnn used num = names]
    ensures (Array.length names = num)
    ensures (\forall x\in used, y\in names, x <> y)
        aka (used \inter names = \emptyset)
 *)
let string_fnn_vanilla (sa:string array) (n:int) : string array =
  let maxlen = Array.fold_left (fun len s -> max len (String.length s)) 0 sa in
  let pref = SUtils.ntimes "w" (succ maxlen) in
  Array.init n (fun i -> pref^(string_of_int i))

let string_fnn (sa:string array) (n:int) : string array =
  let h_size = Array.length sa + n in
  let ns = NameSpace.newman ~h_size () () in
  Array.iter (fun s -> ignore(NameSpace.push' ns s)) sa;
  Array.init n (fun i -> NameSpace.push ns "w")

let make name man ninps sset =
  let gen x n = x^(string_of_int n) in
  let inps = Array.init ninps (gen "x") in
  let outs = Array.init (Array.length sset) (gen "y") in
  let sset = Array.map2 (fun x y -> (x, y)) outs sset in
  {name; man; inps; regs = [||]; wire = [||]; outs; sset}

let normalize_expr_with_snax (sys:string IoTypes.expr) : string IoTypes.expr =
  expr_of_snax(snax_of_expr sys)string_fnn

let system_from_expr (name:string) (ni:int) (outl:int Expr.expr list) : int expr =
  let no = List.length outl in
  let inps = Array.init ni (fun i -> i) in
  let outs = Array.init no (fun i -> ni+i) in
  let sset =
    outl
    |> Array.of_list
    |> Array.mapi (fun i e -> (ni+i, e))
  in
  {name; man = (); inps; regs = [||]; wire = [||]; outs; sset}
