(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CirUtils : Circuit (constraint system) utilities
 *)

open GuaCaml
open Extra
open CirTypes

module CSUtils = Constraint_system_utils

module Wap =
struct
  open Snowflake

  let support = ExprUtils.support

  let kdecomp_of_formule (f:formule) : int list list =
    CSUtils.Wap.kdecomp_of_formule support f

  let input_of_cir (sys:system) : Wap_exchange.input =
    CSUtils.Wap.input_of_system (fun _ -> support) sys

  let input_of_qcir ?(extra_kind=(Some true)) (sys:system) : Cwap_exchange.input =
    CSUtils.Wap.input_of_qsystem ~extra_kind (fun _ -> support) sys

  let refined_primal_graph ?(verbose=false) (sys:system) : GGLA_FT.Type.hg =
    CSUtils.Wap.refined_primal_graph_of_system ~verbose
      (fun () expr -> ExprUtils.primal_graph sys.input expr)
      sys
end

module Utils =
struct
  (* returns for each constraint the number of variable of [interest] appear in it *)
  let linear_support_interest ?(verbose=false) ?(sorted=false) (interest:int list) (sys:system) : int list =
    let interest = if sorted then (assert(SetList.sorted_nat interest); interest) else SetList.sort interest in
    sys.formule ||> (fun c -> List.length (SetList.inter (ExprUtils.support c) interest))

(* [TODO]
  (* for each sub-term returns its occurence-at-depth list (trailling zeros are removed)
   * depth = 0 =>
   * depth = 0 => constraints themselves
   *)
  let exact_subterm_occurence_at_depth ?(verbose=false) (sys:system) :
 *)
end
