(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrLoadExpr : stream parsing of 'expr' expression
 *)

open GuaCaml
open STools
open IoUtils
open Expr

module Stream = Iter.Stream

type 'a token =
  | Kwd of string
  | Sym of char
  | Ident of 'a

type 'a stream = 'a token Stream.t

let pstring (x:string) = print_string x; print_string " "; flush stdout
let pstring (x:string) = ()
let pchar (x:char) = print_char x; print_char ' '; flush stdout
let pchar (x:char) = ()

let rec parse_leaf
    (string_of_var:'a -> string)
    (stream:'a stream) : 'a expr =
  match Stream.next stream with
  | Kwd kwd -> (
    match kwd with
    | "true" -> PCst true
    | "false" -> PCst false
    | _ -> failwith "[DAGaml.StrLoadExpr.parse_leaf] parsing error 'keyword'"
  )
  | Ident var -> pstring(string_of_var var); PVar var
  | Sym '(' ->
  (
    pstring "(";
    let expr = parse_expr string_of_var stream in
    PUop (PNop, expr)
  )
  | Sym '-' | Sym '~' | Sym '!' ->
  (
    pstring "~";
    let expr = parse_leaf string_of_var stream in
    PUop (PNot, expr)
  )
  | _ -> failwith "[DAGaml.StrLoadExpr.parse_leaf] parsing error 'default'"

and     parse_expr
    (string_of_var:'a -> string)
    (stream:'a stream) : 'a expr =
  let first = parse_leaf string_of_var stream in
  match Stream.next stream with
  | Sym '\n' | Sym ';' -> pstring "\n"; first
  | Sym ')' -> pstring ")"; first
  | Sym '&' | Sym '*' ->
  (
    pstring "&";
    let second = parse_expr string_of_var stream in
    PBop (PAnd, first, second)
  )
  | Sym '|' | Sym '+'->
  (
    pstring "|";
    let second = parse_expr string_of_var stream in
    PBop (POr, first, second)
  )
  | Sym '^' ->
  (
    pstring "^";
    let second = parse_expr string_of_var stream in
    PBop (PXor, first, second)
  )
  | x -> assert false

let parse_kwd (stream:'a token Stream.t) : string =
  match Stream.next stream with
  | Kwd kwd -> kwd
  | Sym _ -> failwith "[DAGaml.StrLoadExpr.parse_kwd] parsing error : Symbol"
  | Ident _ -> failwith "[DAGaml.StrLoadExpr.parse_kwd] parsing error : Ident"

let parse_sym (stream:'a token Stream.t) : char =
  match Stream.next stream with
  | Sym sym -> sym
  | Kwd _ -> failwith "[DAGaml.StrLoadExpr.parse_sym] parsing error : Keyword"
  | Ident _ -> failwith "[DAGaml.StrLoadExpr.parse_sym] parsing error : Ident"

let parse_ident (stream:'a token Stream.t) : 'a =
  match Stream.next stream with
  | Ident ident -> ident
  | Sym _ -> failwith "[DAGaml.StrLoadExpr.parse_ident] parsing error : Symbol"
  | Kwd _ -> failwith "[DAGaml.StrLoadExpr.parse_ident] parsing error : Keyword"

open ParseUtils

let from_SSBL
    (ident_of_word:string -> 'a option)
    (keyword_of_word:string -> string option)
    (stream:SSBL.stream) : 'a token Stream.t =
  let rec aux x : 'a token option =
    match Stream.peek stream with
    | None -> None
    | Some token ->
      Stream.junk stream;
      match token.SSBL.ttoken with
      | SSBL.Unary _ -> aux x
      | SSBL.Symbol sym -> Some(Sym sym)
      | SSBL.Word word ->
        match ident_of_word word with
        | Some ident -> (
          (* print_endline ("ident:"^word); *)
          Some(Ident ident)
        )
        | None ->
        match keyword_of_word word with
        | Some kwd -> (
          (* print_endline ("keyword:"^word); *)
          Some(Kwd kwd)
        )
        | None -> failwith ("[StrLoadExpr.from_SSBL] unparsed word : "^(STools.ToS.string word))
  in
  Stream.from aux 0

