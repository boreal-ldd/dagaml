(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CnfTypes : Conjunctive Normal Form
 *)

open GuaCaml

module CSTypes = Constraint_system_types

type term = int * bool
type clause = term list

type formule = clause CSTypes.formule

type system = (unit, clause) CSTypes.system

module ToS =
struct
  include CSTypes.ToS

  let term = pair int bool
  let clause (clause:clause) : string =
    (list term clause)

  let formule (f:formule) : string =
    formule clause f

  let system (f:system) : string =
    system unit clause f
end
