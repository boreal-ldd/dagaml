(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CnfUtils : Toolbox for CNF
 *)

open GuaCaml
open Extra
open CnfTypes
open CSTypes

let prefix = "DAGaml.CnfUtils"

type tree =
  | Const of bool
    (* Boolean Constant *)
  | Units of term list * tree
    (* Unitary Clauses *)
  | OrUnits of term list * tree
    (* Or-Unitary *)
  | Sha3  of int * tree * tree * tree
    (* Shannon TriJunction :
        (var, without, positive(0), negative(1))
     *)
  | DSAnd of tree list
    (* Disjoint Support And *)

module ToS =
struct
  include CnfTypes.ToS

  let rec tree = function
    | Const b -> "Const "^(bool b)
    | Units (tl, t) -> "Units "^(pair (list term) tree (tl, t))
    | OrUnits (tl, t) -> "OrUnits "^(pair (list term) tree (tl, t))
    | Sha3 (i, tX, t0, t1) -> "Sha3 "^(quad int tree tree tree (i, tX, t0, t1))
    | DSAnd tl -> "DSand "^(list tree tl)
end

module ToSTree =
struct
  include STools.ToSTree
  let rec tree = function
    | Const b -> (
      Tree.Leaf (if b then "True" else "False")
    )
    | Units (tl, t) -> (
      Tree.Node [
        Tree.Leaf "Units";
        Tree.Node (MyList.map (leaf ToS.term) tl);
        tree t
      ]
    )
    | OrUnits (tl, t) -> (
      Tree.Node [
        Tree.Leaf "OrUnits";
        Tree.Node (MyList.map (leaf ToS.term) tl);
        tree t
      ]
    )
    | Sha3 (i, tX, t0, t1) -> (
      Tree.Node [
        Tree.Leaf "Sha3";
        int i;
        tree tX;
        tree t0;
        tree t1;
       ]
    )
    | DSAnd tl -> (
      Tree.Node ((Tree.Leaf "DSand")::(MyList.map tree tl))
    )
end

module ToPrettyS =
struct
  open STools
  open ToS

  let term (t:term) : string =
    int(StrDumpCnf.int_of_term t)

  let clause ?(fin=" 0") (c:clause) : string =
    (SUtils.catmap " " term c)^fin

  let formule ?(fin=" 0") ?(sep="\n") (f:CnfTypes.formule) : string =
    SUtils.catmap sep (clause ~fin) f
end

let support (cl:clause) : int list =
  let supp = cl ||> fst in
  assert(SetList.sorted_nat supp);
  supp

(* checks that terms inside [clause] are sorted increasingly *)
let rec clause_sorted ?(last=(-1)) (clause:clause) : bool =
  match clause with
  | [] -> true
  | (i, b)::clause -> (
(*    (last < i) && clause_sorted ~last:i clause *)
         if last < i
    then clause_sorted ~last:i clause
    else if last = i
    then (print_endline ("["^prefix^".clause_sorted] [warning] contradiction last=i:"^(STools.ToS.int last)); false)
    else (print_endline ("["^prefix^".clause_sorted] [warning] non-sorted last:"^(STools.ToS.int last)^">i:"^(STools.ToS.int i)); false)
  )

let formule_sorted (f:CnfTypes.formule) : bool =
  List.for_all clause_sorted f

let file_sorted (cnf:CnfTypes.system) : bool =
  formule_sorted cnf.formule

(* returns true if the clause is a tautology, i.e. it contains
   a litteral both positively and negatively *)

let rec clause_tauto_rec xi xb (clause:clause) : bool =
  match clause with
  | [] -> false
  | (yi, yb)::clause ->
    if yi = xi
    then if yb = xb
      then failwith "[DAGaml.CnfUtils.clause_tauto_rec] state inconsistency:clause is not sorted"
      else true
    else clause_tauto_rec yi yb clause

let clause_tauto : clause -> bool =
  function
  | [] -> false
  | (xi, xb)::clause -> clause_tauto_rec xi xb clause

let clause_trivial : clause -> bool option =
  function
  | [] -> Some false
  | (xi, xb)::clause ->
    if clause_tauto_rec xi xb clause
    then Some true
    else None

(* assumes that formule in [f] are non-strictly sorted (i.e.,
   they may contain inconsistent litterals *)
(* [formule_trivial f = Some f']
     where [[f']] = [[f]], but all formule are strictly sorted
     i.e. all formule containing inconsistent litteral have
     been removed
   [formule_trivial f = None] iff [f] contains an empty clause
     thus [[f]] = false
 *)
let formule_trivial (f:CnfTypes.formule) : CnfTypes.formule option =
  let rec formule_trivial_rec (carry:CnfTypes.formule) =
    function
    | [] -> Some(List.rev carry)
    | c::f -> (
      match clause_trivial c with
      | None       -> formule_trivial_rec (c::carry) f
      | Some true  -> formule_trivial_rec carry f
      | Some false -> None
    )
  in formule_trivial_rec [] f

(* #alt [clause_sorted]
let clause_sorted (cl:clause) : bool =
  MyList.for_all_succ_fst
    ~fst:((<=)0)
     (fun x y -> x < y)
     clause
 *)

(* [terml_set ((i, b):term) clause = opclause]
  match opclause with
  | Ok clause   -> clause[i:=b]
  | Error true  -> clause is satisfied
  | Error false -> contradiction detected
 *)
let rec terml_set
    ?(carry=([]:clause))
     (((ix, bx) as term):term)
     (clause:clause) : (clause, bool) result =
  match clause with
  | [] -> (if carry = []
    then Error false
    else Ok (List.rev carry)
  )
  | ((iy, by) as term')::clause' -> (
    if ix = iy
    then if bx = by
      then Error true
      else (terml_set ~carry term clause')
    else (
      terml_set ~carry:(term'::carry) term clause'
    )
  )

(*  [clause_add term clause = clause'] *)
(*  [[clause']] = [[term]] /\ [[clause]] *)
(*  remark :
      [clause'] = [ [] ] iff clause' is contradictory
 *)
let rec clause_add
    ?(carry=([]:clause))
   (((ix, bx) as term):term)
     (clause:clause) : clause =
  match clause with
  | [] -> List.rev(term::carry)
  | ((iy, by) as term')::clause' -> (
    if ix = iy
    then if bx = by
      (* term is redundant with clause *)
      then (List.rev_append carry clause)
      (* term is contradicts clause *)
      else []
    else if ix < iy
      then (List.rev_append carry (term::clause))
      else (clause_add ~carry:(term'::carry) term clause')
  )

(*  allows several term addition simulteanously
    assumes :
      - terml is sorted
      - clause is sorted
 *)
(*  [clause_add terml clause = clause'] *)
(*  [[clause']] = [[terml]] /\ [[clause]] *)
(*  remark :
      [clause'] = [ [] ] iff clause' is contradictory
 *)
let rec clause_addl
    ?(carry=([]:clause))
     (terml:term list)
     (clause:clause) : clause =
  match terml, clause with
  | [], _ -> (List.rev_append carry clause)
  | _, [] -> (List.rev_append carry terml)
  | ((ix, bx) as x)::terml', ((iy, by) as y)::clause' -> (
    if ix = iy
    then if bx = by
      then (clause_addl ~carry:(y::carry) terml' clause')
      else []
    else if ix < iy
      then (clause_addl ~carry:(x::carry) terml' clause )
      else (clause_addl ~carry:(y::carry) terml  clause')
  )

(* Same as [terml_set] but assumes that :
    - [clause] is sorted
 *)
let rec clause_set
    ?(carry=([]:clause))
   (((ix, bx) as term):term)
     (clause:clause) : (clause, bool) result =
  match clause with
  | [] -> (
    if carry = []
    then Error false
    else Ok(List.rev carry)
  )
  | ((iy, by) as term')::clause' -> (
    if ix = iy
    then if bx = by
      then Error true
      else Ok(List.rev_append carry clause')
    else if ix > iy
      then Ok(List.rev_append carry clause )
    (* the clause is unchanged *)
      else (
        clause_set ~carry:(term'::carry) term clause'
      )
  )

(* allows to perform multiple term assignment simultaneously *)
let rec clause_setl
    ?(carry=([]:clause))
     (setl:term list)
     (clause:clause) : (clause, bool) result =
  match setl, clause with
  | _ , [] -> (
    if carry = []
    then Error false
    else Ok (List.rev_append carry clause)
  )
  | [], _  -> Ok(List.rev_append carry clause)
  | (ix, bx)::setl', ((iy, by) as y)::clause' -> (
    if ix = iy
    then if bx = by
      then Error true
      else clause_setl ~carry setl' clause'
    else if ix > iy
      then clause_setl ~carry:(y::carry) setl clause'
      else clause_setl ~carry setl' clause
  )

(* [MOVEME] *)
let tos_result sok serr = function
  | Ok ok -> "Ok ("^(sok ok)^")"
  | Error err -> "Error ("^(serr err)^")"

(* [DEBUG]
let clause_setl setl clause =
  print_endline ("[clause_setl] setl:"^(ToS.clause setl));
  print_endline ("[clause_setl] clause:"^(ToS.clause clause));
  let res = clause_setl setl clause in
  print_endline ("[clause_setl] res:"^(tos_result ToS.clause STools.ToS.bool res));
  res
 *)

let rec formule_set
    ?(carry=([]:CnfTypes.formule))
     (term:term)
     (formule:CnfTypes.formule) : (CnfTypes.formule, bool) result =
  match formule with
  | [] -> (
    if carry = []
    then Error true
    else Ok(List.rev carry)
  )
  | clause::formule -> (
    match clause_set term clause with
    | Error false -> Error false
    | Error true  ->
      (formule_set ~carry term formule)
    | Ok clause ->
      (formule_set ~carry:(clause::carry) term formule)
  )

(* put back the 'unit formule' into the formula *)
let rec formule_add_units
    (units:term list)
    (formule:CnfTypes.formule) : CnfTypes.formule =
  match units with
  | [] -> formule
  | term::units -> (
    formule_add_units units ([term]::formule)
  )

(*  Ok (units, formule)
      => non-trivial formula
    Error(Some units)
      => formula is satisfiable and reduces to unit formule
    Error None
      => formula is unsatisfiable
 *)
type units_formule_result =
  ((term list) * CnfTypes.formule, term list option) result

let formule_unit_setl
   ?(units=([]:term list))
    (terml:term list)
    (formule:CnfTypes.formule) : units_formule_result =
  (* apply 'multi-variables affectation' and 'unit clause detection' *)
  let rec aux1
      (carry:CnfTypes.formule)
      (units:term list) (* sorted *)
      (terml:term list) (* sorted *)
      (formule:CnfTypes.formule) : units_formule_result =
    match formule with
    | [] -> (
      assert(clause_sorted units);
      if carry = []
      then Error(Some units)
      else Ok(units, (List.rev carry))
    )
    | clause::formule -> (
      match clause_setl terml clause with
      | Error false -> Error None
      | Error true  -> (
        (* Clause Satisfied *)
        aux1 carry units terml formule
      )
      | Ok [term] -> (
        (* Unit Clause Detected *)
        match clause_add term units with
        | [] -> Error None
        | units' -> (
          (* [TODO] time saving by also adding to terml *)
          (* let terml' = clause_add term terml in *)
          aux1 carry units' terml formule
        )
      )
      | Ok clause -> (
        aux1 (clause::carry) units terml formule
      )
    )
  in
  (* apply 'multi-variables affectation' and 'unit clause propagation' iteratively *)
  let rec aux2
      (units:term list) (* sorted *) (* accumulated set of unit clauses *)
      (terml:term list) (* sorted *) (* currently propagating unit clauses *)
      (formule:CnfTypes.formule) : units_formule_result =
    match aux1 [] [] terml formule with
    | Error None -> Error None
    | Error(Some units') -> (
      match clause_addl units' units with
      | [] -> Error None
      | units'' -> Error(Some units'')
    )
    | Ok(units', formule') -> (
      assert(formule' <> []);
           if units' = []
      then Ok(units, formule')
      else (
        match clause_addl units' units with
        | [] -> Error None
        | units'' ->
          (aux2 units'' units' formule')
      )
    )
  in
  if terml = []
  then Ok(units, formule)
  else (aux2 units terml formule)

let connected_components xmax formule =
  let uf = UnionFind.init xmax in
  List.iter (function
    | [] -> assert false
    | (i, _)::tail ->
      List.iter
        (fun (j, _) -> ignore(UnionFind.union uf i j))
         tail
    ) formule;
  let remap, _ = UnionFind.ends uf in
  let split = Array.make (Array.length remap) [] in
  let push x y = split.(x) <- y::split.(x) in
  List.iter (function
    | [] -> assert false
    | ((i, _)::_) as clause -> push remap.(i) clause
    ) formule;
  let parts = List.filter (function [] -> false | _ -> true) (Array.to_list split) in
  parts

let rec clause_trisjunction
     (ix:int)
    ?(carry=([]:clause))
     (clause:clause) : (bool * clause) option =
  match clause with
  | [] -> None
  | ((iy, by) as term)::clause' -> (
    if ix = iy
    then Some (by, List.rev_append carry clause')
    else if ix < iy
    then None
    else clause_trisjunction ix ~carry:(term::carry) clause'
  )

let rec formule_trisjunction
      (ix:int)
     ?(assert_noempty=true)
     ?(carry0=([]:CnfTypes.formule))
     ?(carry1=([]:CnfTypes.formule))
     ?(carryX=([]:CnfTypes.formule))
      (formule:CnfTypes.formule) : CnfTypes.formule * CnfTypes.formule * CnfTypes.formule =
  match formule with
  | [] -> (List.rev carryX, List.rev carry0, List.rev carry1)
  | clause::formule -> (
    match clause_trisjunction ix clause with
    | None -> (
      formule_trisjunction ix
        ~assert_noempty
        ~carry0
        ~carry1
        ~carryX:(clause::carryX) formule
    )
    | Some(_, []) when assert_noempty -> assert false
      (* otherwise clause was a unit clause and should have been dealt with *)
    | Some (false, clause') -> (
      formule_trisjunction ix
        ~assert_noempty
        ~carry0:(clause'::carry0)
        ~carry1
        ~carryX formule
    )
    | Some (true , clause') -> (
      formule_trisjunction ix
        ~assert_noempty
        ~carry0
        ~carry1:(clause'::carry1)
        ~carryX formule
    )
  )

let clause_unique (clause:clause) : (clause, bool) result =
  let rec unique carry ix bx clause =
    match clause with
    | [] -> (
      if carry = []
      then Error false
      else Ok(List.rev carry)
    )
    | ((iy, by) as term)::clause' ->
      if ix = iy
      then if bx = by
        then unique carry ix bx clause'
        else Error false
      else (
        assert(ix < iy);
        unique (term::carry) iy by clause'
      )
  in
  match clause with
  | [] -> Error true
  | ((ix, bx) as term)::clause ->
    (unique [term] ix bx clause)

let clause_sort (clause:clause) : (clause, bool) result =
  clause_unique (List.sort Stdlib.compare clause)

let rec formule_sort
  ?(carry=([]:CnfTypes.formule))
   (formule:CnfTypes.formule) : (CnfTypes.formule, bool) result =
  match formule with
  | [] -> (
    if carry = []
    then Error true
    else Ok(List.rev carry)
  )
  | clause::formule' -> (
    match clause_sort clause with
    | Error false -> Error false
    | Error true  ->
      formule_sort ~carry                  formule'
    | Ok clause' -> (
      formule_sort ~carry:(clause'::carry) formule'
    )
  )

let rec find_units
    ?(units=([]:term list))
    ?(carry=([]:CnfTypes.formule))
     (formule:CnfTypes.formule) : units_formule_result =
  match formule with
  | [] -> (
    match carry, clause_sort units with
    | _ , Error false -> Error None
    | [], Error true  -> Error(Some [])
    | _ , Error true  -> Ok([], List.rev carry)
    | [], Ok units    -> Error(Some units)
    | _ , Ok units    -> Ok(units, List.rev carry)
  )
  | [term]::formule ->
    find_units ~units:(term::units) ~carry formule
  | clause::formule ->
    find_units ~units ~carry:(clause::carry) formule

let simplify (formule:CnfTypes.formule) : units_formule_result =
  match find_units formule with
  | Error err -> Error err
  | Ok(units, formule') -> (
(*  print_endline ("[DAGaml.CnfUtils.simplify] units:"^ToS.(list term units)); *)
    formule_unit_setl ~units units formule'
  )

let cons_orunits orunits tree =
  if orunits = []
  then tree
  else (
    match tree with
    | Const true -> Const true
    | OrUnits (tl', tree') -> (
      match clause_addl tl' orunits with
      | [] -> Const true (* reversed semantic with OR *)
      | tl'' -> OrUnits(tl'', tree')
    )
    | Units ([term], Const true) -> (
      let tree' =
        match clause_add term orunits with
        | [] -> Const true (* reversed semantic with OK *)
        | tl'' -> OrUnits(tl'', Const false)
      in
      tree'
    )
    | _ -> OrUnits(orunits, tree)
  )

let cons_units (units:term list) (tree:tree) : tree =
  if units = []
  then tree
  else (
    match tree with
    | Const false -> Const false
    | Units (tl', tree') -> (
      match clause_addl tl' units with
      | [] -> Const false
      | tl'' -> Units(tl'', tree')
    )
    | OrUnits ([term], Const false) -> (
      match clause_add term units with
      | [] -> Const false
      | tl'' -> Units(tl'', Const true)
    )
    | _ -> Units(units, tree)
  )

let treefy_shannon_nosimpl (order:int list) formule =
  let rec map_formule
      (order:int list)
      (formule:CnfTypes.formule) : tree =
         if formule = []
      then (Const true)
    else if List.exists ((=)[]) formule
      then (Const false)
      else rec_formule order formule
  and     rec_formule
      (order:int list)
      (formule:CnfTypes.formule) : tree =
    if formule = []
    then Const true
    else (
      match order with
      | [] -> assert false
      | x0::order' -> (
        let fX, f0, f1 = formule_trisjunction ~assert_noempty:false x0 formule in
        (* [TESTME] if f0 = [] && f1 = [] then fX' else ( ... ) *)
        let fX = map_formule order' fX
        and f0 = map_formule order' f0
        and f1 = map_formule order' f1 in
        Sha3(x0, fX, f0, f1)
      )
    )
  in
  match formule_sort formule with
  | Error cst -> Const cst
  | Ok formule -> (map_formule order formule)

let treefy_shannon (order:int list) (arity:int) formule =
  assert(formule_sorted formule);
  let rec map_formule
      (path:char list)
      (order:int list)
      (arity:int)
      (formule:CnfTypes.formule) : tree =
    if false && Tools.mode = Tools.Debug
    then (
      let tag = STools.SUtils.implode(List.rev path) in
      let target = "subfile-"^tag^".cnf" in
      print_endline ("dumping:"^target);
      let cnf : CnfTypes.system = {name = "cnf"; input = arity; quants = []; man = (); formule} in
      StrDumpCnf.dump_file target cnf;
      ()
    );
    match simplify formule with
    | Error None ->
      (Const false)
    | Error(Some units) ->
      (cons_units units (Const true))
    | Ok(units, formule') -> (
      (cons_units units
        (rec_formule path order arity formule'))
    )
  and     rec_formule
      (path:char list)
      (order:int list)
      (arity:int)
      (formule:CnfTypes.formule) : tree =
    assert(formule <> []);
    match order with
    | [] -> assert false
    | x0::order' -> (
      let fX, f0, f1 = formule_trisjunction x0 formule in
      (* [TESTME] if f0 = [] && f1 = [] then fX' else ( ... ) *)
      let fX = map_formule ('X'::path) order' arity fX
      and f0 = map_formule ('0'::path) order' arity f0
      and f1 = map_formule ('1'::path) order' arity f1 in
      Sha3(x0, fX, f0, f1)
    )
  in
  match formule_sort formule with
  | Error cst -> (Const cst)
  | Ok formule -> (
    map_formule [] order arity formule
  )

let formule_in_order (f:CnfTypes.formule) (order:int list) : bool =
  let support = SetList.sort order in
  List.for_all (fun c -> SetList.subset_of (c ||> fst) support) f

(* treefy_shannon + connected components *)
let treefy_shannon_cc (order:int list) (xmax:int) formule =
  assert(formule_sorted formule);
  if not (formule_in_order formule order)
  then (
    print_endline  "[DAGaml.CnfUtils:rec_formule] [error] formule_in_order:false";
    print_endline ("[DAGaml.CnfUtils:rec_formule] [error] formule:"^(ToS.formule formule));
    print_endline ("[DAGaml.CnfUtils:rec_formule] [error] order:"^(STools.ToS.(list int) order));
    failwith "[DAGaml.CnfUtils:rec_formule] formule_in_order:false"
  );
  let rec map_formule
      (order:int list)
      (formule:CnfTypes.formule) : tree =
    match simplify formule with
    | Error None -> Const false
    | Error(Some units) ->
      (cons_units units (Const true))
    | Ok(units, formule') -> (
      (cons_units units (split_formule order formule'))
    )
  and   split_formule order formule =
    let ccl = connected_components xmax formule in
    match ccl with
    | [] -> Const true
    | [f] -> rec_formule order f
    | fl -> DSAnd(Tools.map (rec_formule order) fl)
  and     rec_formule
      (order:int list)
      (formule:CnfTypes.formule) : tree =
    assert(formule <> []);
    match order with
    | [] -> assert false
    | x0::order' -> (
      let fX, f0, f1 = formule_trisjunction x0 formule in
      let fX = map_formule order' fX
      (* [TESTME] if f0 = [] && f1 = [] then fX' else ( ... ) *)
      and f0 = map_formule order' f0
      and f1 = map_formule order' f1 in
      Sha3(x0, fX, f0, f1)
    )
  in
  match formule_sort formule with
  | Error cst -> Const cst
  | Ok formule -> (map_formule order formule)

let rec tree_size = function
  | Const _ -> 1
  | Units (tl, t) ->
    1 + List.length tl + tree_size t
  | OrUnits (tl, t) ->
    1 + List.length tl + tree_size t
  | Sha3  (_, tX, t0, t1) ->
    1 + tree_size tX + tree_size t0 + tree_size t1
  | DSAnd tl ->
    (* 1 + sum_i(tree_size tl_i) *)
    List.fold_left (fun s t -> s + tree_size t) 1 tl
    (* Disjoint Support And *)

let tree_simplify_const t =
  let rec tree_simplify = function
    | Const b -> Const b
    | Units (tl, t) -> (
      let t' = tree_simplify t in
      cons_units tl t'
    )
    | OrUnits (tl, t) -> (
      let t' = tree_simplify t in
      cons_orunits tl t'
    )
    | Sha3 (i, tX, t0, t1) -> (
      let tX' = tree_simplify tX
      and t0' = tree_simplify t0
      and t1' = tree_simplify t1 in
      match tX', t0', t1' with
      | Const false, _, _
      | _, Const false, Const false -> Const false
      | _, Const true , Const true  -> tX'
      | Const true, _, Const true   -> cons_orunits [(i, false)] t0'
      | Const true, Const true, _   -> cons_orunits [(i, true )] t1'
      | Const true, _, Const false  ->   cons_units [(i, true )] t0'
      | Const true, Const false, _  ->   cons_units [(i, false)] t1'
      | _, _, _ -> Sha3 (i, tX', t0', t1')
    )
    | DSAnd tl -> DSAnd (MyList.map tree_simplify tl)
  in tree_simplify t

let pprint tree : unit =
  print_newline();
  STools.OfSTree.pprint [ToSTree.tree tree];
  let size = tree_size tree in
  print_string ToS.("tree-size:"^(int size)); print_newline();
  ()

(*  [bucketize_formule sa f = fa * f']
    sa : int list array
      (where each sa.(i) is a component support)
    f : formule
    (*
      type term = int * bool
      type clause = term list
      type formule = clause list
    *)
    fa : formule array
    f' : formule
    bucket-sorts the formule [c_i] of [f]
    such that :
    | mode = ASAP -> [c_i] is put into the first bucket  [b_j] which contains support.
      [c_i] \in [b_j] =>
        - [support(c_i) \subset sa.(j)]
        - \forall k < j [not(support(c_i) \subset sa.(k))]
    | mode = ALAP -> [c_i] is put into the last  bucket  [b_j] which contains support.
    | mode = ALL  -> [c_i] is put into the all   buckets [b_j] which contain support.
    if [c_i] does not fit in any bucket it is added to [f']
 *)
let bucketize_clause_asap
    (sa:int list array)
    (clause:CnfTypes.clause)
    (fa:CnfTypes.formule array)
    (f':CnfTypes.formule ref) : unit =
  let supp : int list = clause ||> fst in
  assert(SetList.sorted_nat supp);
  let len = Array.length sa in
  assert(Array.length sa = Array.length fa);
  let rec loop i : unit =
    if i < len
    then if SetList.subset_of supp sa.(i)
      then (fa.(i) <- clause::fa.(i))
      else (loop (succ i))
    else (f' := clause::(!f'))
  in
  loop 0

let bucketize_clause_alap
    (sa:int list array)
    (clause:CnfTypes.clause)
    (fa:CnfTypes.formule array)
    (f':CnfTypes.formule ref) : unit =
  let supp : int list = clause ||> fst in
  assert(SetList.sorted_nat supp);
  let len = Array.length sa in
  assert(Array.length sa = Array.length fa);
  let rec loop i : unit =
    if i >= 0
    then if SetList.subset_of supp sa.(i)
      then (fa.(i) <- clause::fa.(i))
      else (loop (pred i))
    else (f' := clause::(!f'))
  in
  loop (len-1)

let bucketize_clause_all
    (sa:int list array)
    (clause:CnfTypes.clause)
    (fa:CnfTypes.formule array)
    (f':CnfTypes.formule ref) : unit =
  let supp : int list = clause ||> fst in
  assert(SetList.sorted_nat supp);
  let len = Array.length sa in
  assert(Array.length sa = Array.length fa);
  let rec loop (matched:bool) i : unit =
    if i < len
    then if SetList.subset_of supp sa.(i)
      then (fa.(i) <- clause::fa.(i); loop true (succ i))
      else (loop matched (succ i))
    else if matched
      then ()
      else (f' := clause::(!f'))
  in
  loop false 0

let bucketize_clause
   ?(mode=Tools.ASAP)
    (sa:int list array)
    (clause:CnfTypes.clause)
    (fa:CnfTypes.formule array)
    (f':CnfTypes.formule ref) : unit =
  match mode with
  | Tools.ASAP ->  bucketize_clause_asap sa clause fa f'
  | Tools.ALAP ->  bucketize_clause_alap sa clause fa f'
  | Tools.ALL  ->  bucketize_clause_all  sa clause fa f'

let bucketize_formule
  ?(mode=Tools.ASAP)
   (sa:int list array)
   (formule:CnfTypes.formule) : CnfTypes.formule array * CnfTypes.formule =
  let len = Array.length sa in
  let fa = Array.make len [] in
  let f' = ref [] in
  List.iter (fun clause -> bucketize_clause ~mode sa clause fa f') formule;
  Array.iteri (fun i l -> fa.(i) <- List.rev l) fa;
  (fa, List.rev !f')

module Wap =
struct
  open Snowflake

  module CSUtils = Constraint_system_utils

  let support (cl:CnfTypes.clause) : int list =
    cl ||> fst

  let kdecomp_of_formule (f:CnfTypes.formule) : int list list =
    CSUtils.Wap.kdecomp_of_formule support f

  let input_of_cnf (cnf:CnfTypes.system) : Wap_exchange.input =
    assert(file_sorted cnf);
    CSUtils.Wap.input_of_system (fun _ -> support) cnf

  let input_of_qbf ?(extra_kind=(Some true)) (cnf:CnfTypes.system) : Cwap_exchange.input =
    assert(file_sorted cnf);
    CSUtils.Wap.input_of_qsystem ~extra_kind (fun _ -> support) cnf
end

module CnfCoSTreD =
struct
  type mode_out =
    | MO_Pure
    | MO_Verilog
end

module type CnfCoSTreD_Sig =
sig
  val main_bpp_synthesis : string -> CnfCoSTreD.mode_out -> string -> unit
  val main_debug :
     string ->
    ?bucketize_mode:Tools.prio_t ->
    ?file_frp:string option ->
    ?file_bpp:string option ->
     string -> unit
  val main_perf_bpp :
    ?bucketize_mode:Tools.prio_t ->
     string -> CnfCoSTreD.mode_out -> string -> unit
  val main_perf_bpp_aqeops : string -> CnfCoSTreD.mode_out -> string -> unit
  val main_perf_frp : string -> string -> unit
  val main_perf_dummy : string -> string -> unit
end

let file_of_formule ?input ?(quants=[]) (formule:CnfTypes.formule) : CnfTypes.system =
  let input =
    match input with
    | Some v -> v
    | None -> (
      formule
      ||> (fun c -> c ||> fst |> MyList.list_max |> fst)
      |> MyList.list_max |> fst
      |> succ
    )
  in
  {input; name = "cnf"; quants; man = (); formule}

let formule_support (file:CnfTypes.system) : bool array =
  let support = Array.make file.CSTypes.input false in
  List.iter
    (List.iter (fun (i, _) -> support.(i) <- true))
     file.formule;
  support

let file_rename (file:CnfTypes.system) (input:int) (r:int array) : CnfTypes.system =
  let clause_rename r (c:clause) : clause =
    c
    ||>
     (fun (i, b) ->
      let ri = r.(i) in
      assert(ri >= 0 && ri < input);
      (ri, b)
     )
  in
  let formule = file.formule ||> clause_rename r in
  let  quant_rename r ((b, s):quant) : quant =
    (b, s ||>
     (fun i ->
      let ri = r.(i) in
      assert(ri >= 0 && ri < input);
      ri
     ))
  in
  let quants = file.quants ||> quant_rename r in
  {input; name = "cnf"; quants; man = (); formule}

(* [relative_file_of_formule ~input ~quants:[] formule = (rename, reverse, file')]
 * where [file'] is equivalent to [file:={input; quants; formule}] after order preserving renaming
 *)
let relative_file_of_formule input ?(name="cnf") ?(quants=[]) (formule:CnfTypes.formule) : int option array * int array * CnfTypes.system =
  let file = {input; name; quants; man = (); formule} in
  let supp = formule_support file in
  let rename, reverse = MyArray.true_rename supp in
  let input' = MyArray.count_Some rename in
  let rename' = Array.map (function Some x -> x | None -> -1) rename in
  let file' = file_rename file input' rename' in
  (rename, reverse, file')

