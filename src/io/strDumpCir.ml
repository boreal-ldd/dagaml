(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : CirTypes : Circuit (constraint system) types
 *)

open GuaCaml

open CirTypes
module CSUtils = Constraint_system_utils

module FromExpr =
struct
  let dump_clause_stream (output:string -> unit) (clause:clause) : unit =
    output (Expr.to_string string_of_int clause);
    output "\n";
    ()

  let dump_system_stream (output:string -> unit) (sys:system) : unit =
    let nv = sys.CSTypes.input in
    let nc = List.length sys.CSTypes.formule in
    output ("p cir "^(string_of_int nv)^" "^(string_of_int nc)^"\n");
    List.iter (dump_clause_stream output) sys.CSTypes.formule;
    ()

  let dump_file (target:string) (sys:system) : unit =
    let channel = open_out target in
    dump_system_stream (output_string channel) sys;
    close_out channel;
    ()
end

module FromRExpr =
struct
  let dump_clause_stream (output:string -> unit) clause : unit =
    output (RExpr.ToPrettyS.rexpr string_of_int clause);
    output "\n";
    ()

  let dump_system_stream (output:string -> unit) sys : unit =
    let nv = sys.CSTypes.input in
    let nc = List.length sys.CSTypes.formule in
    output ("p cir "^(string_of_int nv)^" "^(string_of_int nc)^"\n");
    List.iter (dump_clause_stream output) sys.CSTypes.formule;
    ()

  let dump_file (target:string) sys : unit =
    let channel = open_out target in
    dump_system_stream (output_string channel) sys;
    close_out channel;
    ()
end

let dump_file (target:string) (sys:system) : unit =
  sys
  |> CSUtils.map_system (fun () -> ()) RExpr.rexpr_of_expr
  |> FromRExpr.dump_file target

