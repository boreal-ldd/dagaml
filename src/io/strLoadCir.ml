(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrLoadCir : file parsing of Boolean Constraint file
 *)

open GuaCaml
open Extra
open STools
open CnfTypes
open ParseUtils
open StrLoadExpr

module CSTypes = Constraint_system_types

let lexer_kind = function
  | ' ' | '\t' -> -1
  | '(' | ')' | '&' | '|' | '-' | '~' | '!' | '\n' -> 0
  | _ -> 1

let my_lexer (stream:char Stream.t) : SSBL.stream =
  SSBL.lexer lexer_kind stream

type clause = int Expr.expr

type formule = clause CSTypes.formule

type system = (unit, clause) CSTypes.system

module ToS =
struct
  let clause = Expr.ToS.expr string_of_int

  let formule = CSTypes.ToS.formule clause

  let system = CSTypes.ToS.system ToS.unit clause
end

let parse_expr : int token Stream.t -> clause = StrLoadExpr.parse_expr string_of_int

let rec parse_formule (carry:formule) (stream:int stream) : formule =
  match Stream.peek stream with
  | None -> List.rev carry
  | Some token -> (
    match token with
    | Sym '\n' -> (Stream.junk stream; parse_formule carry stream)
    | _ -> (
      let clause = parse_expr stream in
      parse_formule (clause::carry) stream
    )
  )

let parse_module (stream:int stream) : system =
  assert(StrLoadExpr.parse_kwd stream = "p");
  assert(StrLoadExpr.parse_kwd stream = "cir");
  let input = StrLoadExpr.parse_ident stream in
  let nbformule = StrLoadExpr.parse_ident stream in
  assert(StrLoadExpr.parse_sym stream = '\n');
  let formule = parse_formule [] stream in
  let length_formule = List.length formule in
  if nbformule <> length_formule
  then (print_endline ("[warning] nbformule:"^(string_of_int nbformule)^" length(formule):"^(string_of_int length_formule)));
  CSTypes.{name = "cir"; input; quants = []; man = (); formule}

let load (stream:char Stream.t) : system =
     stream
(*|> Stream.tee_char *)
  |> my_lexer
  |> StrLoadExpr.from_SSBL
      (fun word ->
        try Some(int_of_string word)
        with _ -> None)
      (function
        | "p" -> Some "p"
        | "cir" -> Some "cir"
        | "true" -> Some "true"
        | "false" -> Some "false"
        | _ -> None)
  |> parse_module

let load_file target =
  print_endline ("[StrLoadCir.load_file] target:"^target);
  let file = open_in target in
  let cnf = load (Stream.of_channel_as_char file) in
  close_in file;
  print_endline ("[StrLoadCir.load_file] parsed!");
  cnf

(*
let _ =
  let file_name = Sys.argv.(1) in
  let sys = load_file file_name in
  print_endline(ToS.system sys);
  ()
 *)
