(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrLoadPla : file parsing of PLA file
 *)

open GuaCaml
open STools
open IoUtils
open Expr
open StrLoadExpr

let my_lexer stream : string token Stream.t =
  let is_space = function ' ' | '\t' | '\n' -> true | _ -> false in
  let rec rmline () =
    try match Stream.next stream with
      | '\n' -> ()
      |  _   -> rmline ()
    with _ -> ()
  in
  let rec spaces () = match Stream.peek stream with
  | None -> ()
  | Some head -> if is_space head then (Stream.junk stream; spaces ()) else ()
  in
  let is_symbol = function '=' | '~' | '&' | '|' | '^' | '(' | ')' | ';' | ',' | '*' | '+' | '!' -> true | _ -> false in
  let word () =
    let rec aux s = match Stream.peek stream with
      | None -> SUtils.implode (List.rev s)
      | Some c ->
      ( if is_space c || is_symbol c
        then (SUtils.implode (List.rev s))
        else (Stream.junk stream; aux (c::s))
      )
    in
    let w = aux [] in match w with
    | "INORDER"
    | "OUTORDER" -> Kwd w
    | _ -> Ident w
  in
  let rec aux (x:int) : string token option=
    spaces ();
    match Stream.peek stream with
    | None -> None
    | Some head ->
    (
      if head = '#'
      then (rmline (); aux x)
      else
      (
        assert(not(is_space head));
        Some ( if is_symbol head
        then (Stream.junk stream; Sym head)
        else (word ()))
      )
    )
  in
  Stream.from aux 0

let tee = Iter.Stream.tee_char

let parse_leaf : string token Stream.t -> _ = StrLoadExpr.parse_leaf (fun s -> s)
let parse_expr : string token Stream.t -> _ = StrLoadExpr.parse_expr (fun s -> s)

let parse_ident stream = match Stream.next stream with
  | Ident name -> name
  | _ -> assert false

let rec parse_coma_list stream =
  let rec aux carry = match Stream.peek stream with
  | None -> assert false
  | Some head -> match head with
    | Sym ',' ->
    (
      Stream.junk stream;
      let ident = parse_ident stream in
      aux (ident::carry)
    )
    | _ -> List.rev carry
  in
  match Stream.next stream with
  | Ident ident -> aux [ident]
  | _ -> assert false

let parse_braket_list stream = match Stream.next stream with
  | Sym '(' ->
  (
    let liste = parse_coma_list stream in
    match Stream.next stream with
    | Sym ')' -> liste
    | _ -> assert false
  )
  | _ -> assert false

let parse_list stream =
  let rec aux carry = match Stream.next stream with
  | Sym ';' -> List.rev carry
  | Ident ident -> aux (ident::carry)
  | _ -> assert false
  in aux []

let parse_semicolon stream = match Stream.next stream with
  | Sym ';' -> ()
  | _ -> assert false

let parse_semicolon_list stream =
  let liste = parse_coma_list stream in
  parse_semicolon stream;
  liste

let parse_kwd stream = match Stream.next stream with
  | Kwd kwd -> kwd
  | _ -> assert false

let parse_sym stream = match Stream.next stream with
  | Sym sym -> sym
  | _ -> assert false

let parse_assign stream =
  let ident = parse_ident stream in
  assert('=' = parse_sym stream);
  let expr = parse_expr stream in
  (ident, expr)

let parse_module_aux mymodule stream =
  let append a l = Array.(append a (of_list l)) in
  let rec aux carry =
    try
      match Stream.next stream with
      | Kwd "INORDER" ->
      (
        assert('=' = parse_sym stream);
        let liste = parse_list stream in
        aux IoTypes.{carry with
          inps = append carry.inps liste}
      )
      | Kwd "OUTORDER" ->
      (
        assert('=' = parse_sym stream);
        let liste = parse_list stream in
        aux IoTypes.{carry with
          outs = append carry.outs liste}
      )
      | Ident ident ->
      (
        assert('=' = parse_sym stream);
        let expr = parse_expr stream in
        let assign = (ident, expr) in
        aux IoTypes.{carry with
          sset = append carry.sset [assign]}
      )
      | _ -> assert false
    with _ -> carry
  in aux mymodule

let module_expr = IoTypes.{
  name = "PLA";
  man  = ();
  inps = [||];
  regs = [||];
  wire = [||];
  outs = [||];
  sset = [||];
}

let parse_module stream =
  let sys = parse_module_aux module_expr stream in
  let notwire = Array.append sys.outs sys.regs in
  let wire = sys.sset
    |> Array.map fst
    |> Array.to_list
    |> List.filter (fun x -> not(Array.mem x notwire))
    |> Array.of_list
  in
  IoTypes.{sys with wire}

let load stream = stream |> my_lexer |> parse_module

let load_file file =
  let fin = open_in file in
  let mexpr = load (Stream.of_channel_as_char fin) in
  close_in fin;
  mexpr
