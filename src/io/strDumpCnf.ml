(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : StrDumpCnf : file generation of (Q)DIMACS file
 *)

open CnfTypes

let int_of_term (var, bool) : int =
  (if bool then -(var+1) else (var+1))

let output_term output (t:term) : unit =
  output (string_of_int (int_of_term t))

let rec output_clause output : clause -> unit =
  function
  | [] -> output " 0"
  | t::c -> (
    output " ";
    output_term output t;
    output_clause output c
  )

let rec output_formule output : formule -> unit =
  function
  | [] -> ()
  | c::f -> (
    output_clause output c;
    output "\n";
    output_formule output f
  )

let dump_qbf (output : string -> unit) (file:system) : unit =
  (* printing header *)
  output ("p cnf "^(string_of_int file.input)^" "^(string_of_int (List.length file.formule))^"\n");
  (* printing quantifiers *)
  List.iter (fun (q, vars) ->
    output (if q then "e" else "a");
    List.iter (fun var ->
      output (" "^(string_of_int(succ var)))
    ) vars;
    output " 0\n";
  ) file.quants;
  (* printing formule *)
  output_formule output file.formule;
  ()

let dump output file =
  assert(file.CSTypes.quants = []);
  dump_qbf output file

let dump_qbf_file (file:string) (cnf:system) : unit =
  let fout = open_out file in
  dump_qbf (output_string fout) cnf;
  close_out fout

let dump_file (file:string) (qcnf:system) =
  assert(qcnf.CSTypes.quants = []);
  dump_qbf_file file qcnf

