(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : Support : Deals with the representation of the set of
 *                     support variable
 *)

open GuaCaml
open Extra

type t = BRLE.t

let of_BRLE : t -> BRLE.t = fun x -> x
let to_BRLE : BRLE.t -> t = fun x -> x

let of_sorted_support : int -> int list -> t = BRLE.of_int_list
let to_sorted_support : t -> int list = BRLE.to_int_list

let of_bool_list : bool list -> t = BRLE.reduce
let to_bool_list : t -> bool list = BRLE.expand

let union : t -> t -> t = BRLE.bw_or
let union_list (a:int) (tl:t list) : t =
  List.fold_left BRLE.bw_or (BRLE.single (a, false)) tl
let inter : t -> t -> t = BRLE.bw_and
let inter_list (a:int) (tl:t list) : t =
  List.fold_left BRLE.bw_and (BRLE.single (a, true)) tl

