(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : OBool : maps Booleans as an OOPS interface
 *)

module OOPS =
struct
  module Model =
  struct
    type edge = {arity : int; value : bool}
    let ( ->> ) blist edge = assert false
    let arity edge = edge.arity
    (* where true represent significant variables and false non significant ones *)
    let   neg edge =
      {arity = edge.arity; value = not edge.value}
    let ( *! ) edge0 edge1 = assert false
    let ( &! ) edge0 edge1 =
      assert(edge0.arity = edge1.arity);
      {arity = edge0.arity; value = edge0.value && edge1.value}
    let ( ^! ) edge0 edge1 =
      assert(edge0.arity = edge1.arity);
      {arity = edge0.arity; value = edge0.value <> edge1.value}
    let ( |! ) edge0 edge1 =
      assert(edge0.arity = edge1.arity);
      {arity = edge0.arity; value = edge0.value || edge1.value}
    let ( =! ) edge0 edge1 =
      assert(edge0.arity = edge1.arity);
      {arity = edge0.arity; value = edge0.value =  edge1.value}

    let cst value arity = {arity; value}
    let to_bool edge = Some edge.value
  end

  include OOPS.MODULE(Model)

  module SUInt = SUInt.MODULE(Model) (* fixed Size INTeger *)
  module VUInt = VUInt.MODULE(Model) (* Variable size INTeger *)

end
