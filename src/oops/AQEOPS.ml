(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : AQEOPS : Avanced Quantifier Elimination of ordered OperatorS
 *
 * === NOTE ===
 * exposes a common interface for ordered and uniform functional
 * computations
 * extends [QEOPS] with :
 * - [X] n-ary binary operators [DONE]
 * - [ ] AQE (Alternating Quantifier Elimination)
 * - [ ] AQE + n-ary binary operators
 * - [X] ordered primitives manipulation
 * - [ ] uniform primitives manipulation
 *
 * === REMARK ===
 *   [AQEOPS] does not extends the semantic of [QEOPS].
 *   However it allows for developper to implement smarter combined solutions to a specific (but generic) task.
 *   e.g.,
 *   - local structural analysis
 *   - reducing conversion overhead (e.g. computation models based on De Morgan's law)
 *   We provide a functor to derive an [AQEOPS] module from a [QEOPS] one.
 *)

open GuaCaml
open Extra

type prim = U | X | C of bool * bool

module type MSig =
sig
  include QEOPS.MSig

  val ( &!! ) : t -> int -> f list -> f
  val ( |!! ) : t -> int -> f list -> f
  val ( ^!! ) : t -> int -> f list -> f
  val ( =!! ) : t -> int -> f list -> f

  (* if [builtin_o_prim p = false] using either constructors may either have
   * consistent behaviour or raise an exception upon usaged *)
  val builtin_o_prim : prim -> bool (* return [true] iff [prim] is implemented within the computation model *)
  (* counts the number of ordered [prim] primitives available *)
  val detect_o_prim : t -> prim -> f -> int
  (* pushes [n:int] ordered [prim] primitives *)
  val intro_o_prim  : t -> prim -> int -> f -> f
  (* removes [n:int] ordered [prim] primitives *)
  val elim_o_prim   : t -> prim -> int -> f -> f
end

module OfQEOPS(Q:QEOPS.MSig) : MSig
  with type t = Q.t
  and  type f = Q.f =
struct
  include Q

  let ( &!! ) (t:Q.t) (ga:int) (fl:Q.f list) : Q.f =
    match fl with
    | [] -> (Q.cst t true ga)
    | f0::fl -> (List.fold_left (Q.(&!) t) f0 fl)

  let ( |!! ) (t:Q.t) (ga:int) (fl:Q.f list) : Q.f =
    match fl with
    | [] -> (Q.cst t false ga)
    | f0::fl -> (List.fold_left (Q.(|!) t) f0 fl)

  let ( ^!! ) (t:Q.t) (ga:int) (fl:Q.f list) : Q.f =
    match fl with
    | [] -> (Q.cst t false ga)
    | f0::fl -> (List.fold_left (Q.(^!) t) f0 fl)

  let ( =!! ) (t:Q.t) (ga:int) (fl:Q.f list) : Q.f =
    match fl with
    | [] -> (Q.cst t true ga)
    | f0::fl -> (List.fold_left (Q.(=!) t) f0 fl)

  let builtin_o_prim _ = false (* return [true] iff [prim] is implemented within the computation model *)
  let detect_o_prim _ _ _ = assert false  (* counts the number of ordered [prim] primitives available *)
  let intro_o_prim _ _ _ _ = assert false (* push [n:int] ordered [prim] primitives *)
  let elim_o_prim _ _ _ _ = assert false  (* removes [n:int] ordered [prim] primitives *)
end

let prefix = "DAGaml.AQEOPS"

module type Sig =
sig
  module Q : MSig
  open Q

  module Oops : OOPS.Sig
    with type O.t = Q.t
    and  type O.f = Q.f
    and  type O.F.f' = Q.F.f'
  open Oops

  val gen_exists_A : t -> Support.t -> f -> f
  val gen_forall_A : t -> Support.t -> f -> f

(*
  val existsA : t -> Support.t -> f -> f
  val forallA : t -> Support.t -> f -> f
 *)

  (* [sat_select_A t f supp = (x, s)] where :
   *  - [supp : Support.t] is the set of non-parameter variables
   *)
  val sat_select_A : t -> f -> Support.t -> f * f
  val pushU : t -> f -> f
  val pushU_list : t -> f list -> f list
  val sha_list : t -> f list -> f list -> f list

  type psubst = {
    arity   : int;
    partial : f;
    assign  : (int * f) list;
  }

  (* [exists_proj_A t f supp = xs] where :
   *  - [supp : Support.t] is the set of non-parameter variables
   *)
  val exists_proj_A : t -> f -> Support.t -> psubst

  (* [argmaxA t g supp f = (x', f')] where :
   *  - [supp : Support.t] is the set of non-parameter variables
   *)
  val argmaxA : t -> f -> Support.t -> f -> f * f

  val lexmaxA_rev : t -> f -> Support.t -> f array -> f * f array
  val lexmaxA : t -> f -> Support.t -> f array -> f * f array

  module PSubst :
  sig
    val domain : t -> psubst -> Support.t
    val codomain : t -> psubst -> Support.t
    val support : t -> psubst -> Support.t

    val of_guard_A : t -> f -> Support.t -> psubst
    val is_trivial : t -> psubst -> bool option
    val equal : t -> psubst -> psubst -> bool
  end
end

module Make(Q:MSig) : Sig
  with type Q.t = Q.t
  and  type Q.f = Q.f
  and  type Q.F.f' = Q.F.f'
=
struct
  module Q = Q
  open Q

  module Oops = OOPS.Make(Q)
  open Oops

  let pushU (t:Q.t) (f:Q.f) : Q.f = Q.( *! ) t f f
  let pushU_list (t:Q.t) (fl:Q.f list) : Q.f list = fl ||> (pushU t)
  let sha_list (t:Q.t) (fl0:Q.f list) (fl1:Q.f list) =
    MyList.map2 (Q.( *! ) t) fl0 fl1

  let gen_exists_A (t:Q.t) (param:Support.t) (f:Q.f) : Q.f =
    let param = Support.to_bool_list param in
    assert(Q.arity t f = List.length param);
    let memo, apply = MemoTable.(make default_size) in
    let rec exists_rec (f:Q.f) (p:bool list) =
      let a = Q.arity t f in
      assert(a = List.length p); (* [DEBUG] *)
      match p with
      | [] -> f
      | p0::p' -> (
        match Q.to_bool t f with
        (* case 0 : constant case *)
        | Some b -> Q.cst t b a
        (* case 1 : shannon case *)
        | None -> (
          let f0 = Q.cofactor t false f in
          let f1 = Q.cofactor t true  f in
          let f'0 = exists_mem f0 p' in
          let f'1 = exists_mem f1 p' in
          let f' = if p0
            then (pushU t (Q.( |! ) t f'0 f'1))
            else (Q.( *! ) t f'0 f'1)
          in
          f'
        )
      )
    and exists_mem (f:Q.f) (p:bool list) : Q.f =
      apply (fun _ -> exists_rec f p) (Q.id t f, p)
    in
    exists_rec f param

(*
  let existsA (t:Q.t) (param:Support.t) f : Q.f =
    print_endline "AQEOPS.existsA";
    let r1 = gen_exists_A t param f
    and r2 = Q.existsA t param f in
    if not (Q.eq t r1 r2)
    then(
      print_endline "[DAGaml.AQEOPS.existsA(generic-check)] result inconsistency between optimized and generic version";
      print_endline ("f = "^(Q.to_string t f));
      print_endline ("param = "^(STools.ToS.(list int) (Support.to_sorted_support param)));
      print_endline ("r1 = gen_exists_A t param f = "^(Q.to_string t r1));
      print_endline ("r2 = Q.exists t param f     = "^(Q.to_string t r1));
      failwith "[DAGaml.AQEOPS.existsA(generic-check)] result inconsistency between optimized and generic version";
    );
    r1
  *)

  let gen_forall_A (t:Q.t) (param:Support.t) (f:Q.f) : Q.f =
    let param = Support.to_bool_list param in
    assert(Q.arity t f = List.length param);
    let memo, apply = MemoTable.(make default_size) in
    let rec forall_rec (f:Q.f) (p:bool list) =
      let a = Q.arity t f in
      assert(a = List.length p); (* [DEBUG] *)
      match p with
      | [] -> f
      | p0::p' -> (
        match Q.to_bool t f with
        (* case 0 : constant case *)
        | Some b -> Q.cst t b a
        (* case 1 : shannon case *)
        | None -> (
          let f0 = Q.cofactor t false f in
          let f1 = Q.cofactor t true  f in
          let f'0 = forall_mem f0 p' in
          let f'1 = forall_mem f1 p' in
          let f' = if p0
            then (pushU t (Q.( &! ) t f'0 f'1))
            else (Q.( *! ) t f'0 f'1)
          in
          f'
        )
      )
    and forall_mem (f:Q.f) (p:bool list) : Q.f =
      apply (fun _ -> forall_rec f p) (Q.id t f, p)
    in
    forall_rec f param

(*
  let forallA (t:Q.t) (param:Support.t) (f:Q.f) : Q.f =
    print_endline "AQEOPS.forallA";
    let r1 = gen_forall_A t param f
    and r2 = Q.forallA t param f in
    assert(Q.eq t r1 r2);
    r1
 *)

  (* val sat_select : t -> bool list -> f -> f * f
   * [sat_select t suppB f = g * f'] where :
   * - [f  : bool^(suppA) -> bool^(suppB) -> bool] (where suppA = support(f) - suppB)
   * - [f' : bool^(suppA) -> bool^(suppB) -> bool]
   * - [g  : bool^(suppA)                 -> bool]
   * such that :
   * - [suppB is Support.sorted]
   * - [f' => f]
   * - [\forall (a:suppA). (\exists! (b:suppB) f'(a, b)) \/ f(a, .) = false_{suppB}]
   * - [\exists(suppB) f' = \exists(suppB) f]
   *   + i.e. [\forall (a:suppA). (\exists (b:suppB) f'(a, b)) <=> (\exists (b:suppB) f(a, b))]
   * - [\forall (a:suppA). (\forall (b1 b2:suppB) f'(a, b1) = true -> f'(a, b2) = true -> b1 = b2)]
   * - [g = \exists(suppB) f]
   * === In Other Words ===
   * We define [suppA := support(f) - suppB] the set of parameter variables.
   * For any valuation of parameters variables (a:suppA), we denote by
   * [f(a, .) := peval f (List.combine suppA a)] the subfunction of f[suppA:=a].
   * Either :
   * (1) [is_false f(a, .) = true ] (i.e., [f(a. .)] is unsatisfiable, i.e., [f(a, .)] has no solution)
   *   Then, [g(a) = False] and [f'(a, .) = False], or,
   * (2) [is_false f(a, .) = false] (i.e., [f(a, .)] is satisfiable, i.e. [f(a, .)] has at least one solution)
   *   Then, [g(a) = True ] and [f'(a, .)] has exacly one solution (b:suppB) such that [f(a, b) = True]
   *   (i.e. [b] is a solution of [f(a, .)])
   *)

  let pushC (if0:bool) (th0:bool) (t:Q.t) (f:Q.f) : Q.f =
    let a = Q.arity t f in
    let c = Q.cst t th0 a in
    if if0 then Q.( *! ) t f c else Q.( *! ) t c f

  let memo_select_v0, apply_select_v0 = Hashcache.Module.(make default_size) (* in *)

  (* val sat_select : MLBDD.t -> Support.t -> MLBDD.t * MLBDD.t *)
  (* param.(i) iff x_i is a non-parameter variable *)
  (* the arity of the returned functions equals the arity of the input function *)
  let sat_select_A_v0 (t:Q.t) (f:Q.f) (param:Support.t) : Q.f * Q.f =
    (* let fst_r_ref = existsA t param f in (* [DEBUG] *) *)
    let param = Support.to_bool_list param in
    assert(Q.arity t f = List.length param);
    (* main procedure: inductive approach *)
    let apply = apply_select_v0 in
    let rec select_rec (f:Q.f) (p:bool list) =
      let a = Q.arity t f in
      assert(a = List.length p); (* [DEBUG] *)
      match p with
      | [] -> (f, f)
      | p0::p' ->
        match Q.to_bool t f with
        (* case 0 : no solution in this branch *)
        | Some false -> (Q.cst t false a, Q.cst t false a)
        (* case 1 : choose the solution in which the remaining edges do not
         * appear; arbitrary choice to keep only one matching *)
        | Some true  -> (
          let sol =
            p |> MyList.indexes_true
              ||> (Q.var t true a)
              |> (Q.(&!!) t a)
          in
          (Q.cst t true a, sol)
        )
        | None -> (
          let f0 = Q.cofactor t false f in
          let f1 = Q.cofactor t true  f in
          assert(Q.arity t f0 = a-1);
          assert(Q.arity t f1 = a-1);
          let oU = Q.eq t f0 f1 in
          if p0
          then ( (* non parameter variable *)
            if oU
            then (
              let (g'0, f'0) = select_rec f0 p' in
              let f' = pushC true false t f'0 in
              let g' = pushU t g'0 in
              (g', f')
            )
            else (
              let (g'0, f'0) = select_mem f0 p' in
              let (g'1, f'1) = select_mem f1 p' in
              (* phi' := var0 ? ( phi'1 /\ ~g'0 * ) : phi'0 *)
              assert(Q.arity t g'0 = Q.arity t f'1);
              let f' = Q.( *! ) t f'0 ( Q.( &!) t (neg t g'0) f'1) in
              let g' = pushU t (Q.(|!) t g'0 g'1) in
              assert(Q.arity t f' = a);
              assert(Q.arity t g' = a);
              (g', f')
            )
          )
          else ( (* parameter variable *)
            if oU
            then (
              let (g'0, f'0) = select_rec f0 p' in
              let g' = pushU t g'0 in
              let f' = pushU t f'0 in
              (g', f')
            )
            else (
              let (g'0, f'0) = select_mem f0 p' in
              let (g'1, f'1) = select_mem f1 p' in
              let g' = Q.( *! ) t g'0 g'1 in
              let f' = Q.( *! ) t f'0 f'1 in
              assert(Q.arity t f' = a);
              assert(Q.arity t g' = a);
              (g', f')
            )
          )
        )
    and select_mem (f:Q.f) (p:bool list) : Q.f * Q.f =
      apply (fun _ -> select_rec f p) (Q.id t f, p)
    in
    let r = select_rec f param in
    (* assert(Q.eq t (fst r) fst_r_ref); *)
    r

  let match_QfoP_BRLE (prim:prim) (t:Q.t) (f:Q.f) (p:BRLE.t) : (int * Q.f * bool * BRLE.t) option =
    if Q.builtin_o_prim prim
    then (
      let nP = Q.detect_o_prim t prim f in
      if nP = 0 then None else (
        match BRLE.uncons p with
        | None -> (* p = [] *) (failwith "[DAGaml.AQEOPS.match_QfoP_BRLE] inconsistent behaviour")
        | Some ((np, p0), _) -> (
          let n = min nP np in
          assert(n > 0);
          let fP = Q.elim_o_prim t prim n f in
          let _, pP = BRLE.pull ~n p in
          Some(n, fP, p0, pP)
        )
      )
    )
    else None

  let memo_select_v1, apply_select_v1 = Hashcache.Module.(make default_size) (* in *)

  (* val sat_select : MLBDD.t -> Support.t -> MLBDD.t * MLBDD.t *)
  (* param.(i) iff x_i is a non-parameter variable *)
  (* the arity of the returned functions equals the arity of the input function *)
  let sat_select_A_v1 (t:Q.t) (f:Q.f) (param:Support.t) : Q.f * Q.f =
    let pushU ?(n=1) (t:Q.t) (f:Q.f) : Q.f = Q.intro_o_prim t U n f in
    let pushC10 ?(n=1) (t:Q.t) (f:Q.f) : Q.f = Q.intro_o_prim t (C(true , false)) n f in
    let pushC00 ?(n=1) (t:Q.t) (f:Q.f) : Q.f = Q.intro_o_prim t (C(false, false)) n f in
    let rec sol_of_rev_param_rec (carry:Q.f) (p:BRLE.t) : Q.f =
      match BRLE.uncons p with
      | None -> carry
      | Some((n, p0), p') ->
        let carry = if p0
          then pushC10 ~n t carry
          else pushU   ~n t carry
        in
        sol_of_rev_param_rec carry p'
    in
    let sol_of_param (a:int) (p:BRLE.t) : Q.f =
      assert(a = BRLE.length p);
      sol_of_rev_param_rec (Q.cst t true 0) (BRLE.rev p)
(*      let f_ref =
        p |> BRLE.to_int_list
          ||> (Q.var t true a)
          |> (Q.(&!!) t a)
      in
      if not (Q.eq t f f_ref)
      then (
        print_endline "[DAGaml.AQEOPS.sat_select_A_v1:sol_of_param] inconsistency";
        print_endline ("[DAGaml.AQEOPS.sat_select_A_v1:sol_of_param] p:"^(BRLE.ToS.t p));
        print_endline ("[DAGaml.AQEOPS.sat_select_A_v1:sol_of_param] f:"^(Q.to_string t f));
        print_endline ("[DAGaml.AQEOPS.sat_select_A_v1:sol_of_param] f_ref:"^(Q.to_string t f_ref));
        failwith "[DAGaml.AQEOPS.sol_of_param] failure";
      );
      f *)
    in
    (* let fst_r_ref = existsA t param f in (* [DEBUG] *) *)
    let param = Support.to_BRLE param in
    assert(Q.arity t f = BRLE.length param);
    (* main procedure: inductive approach *)
    let apply = apply_select_v1 in
    let rec select_rec (f:Q.f) (p:BRLE.t) =
      let a = Q.arity t f in
      assert(a = BRLE.length p); (* [DEBUG] *)
      match Q.to_bool t f with
      | Some false -> (Q.cst t false a, Q.cst t false a)
      | Some true  -> (Q.cst t true  a, sol_of_param a p)
      | None -> (
        match match_QfoP_BRLE U t f p with
        | Some(n, fU, p0, pU) -> (
          let (g'U, f'U) = select_rec fU pU in
          let g' = pushU ~n t g'U in
          let f' =
            if p0
            then pushC10 ~n t f'U
            else pushU   ~n t f'U
          in
          assert(Q.arity t f' = a);
          assert(Q.arity t g' = a);
          (g', f')
        )
        | None ->
        match match_QfoP_BRLE (C(true, false)) t f p with
        | Some(n, fC, p0, pC) -> (
          (* print_endline ("n10:"^(string_of_int n)); *)
          let (g'C, f'C) = select_rec fC pC in
          let f' = pushC10 ~n t f'C in
          let g' = if p0
            then pushU   ~n t g'C
            else pushC10 ~n t g'C
          in
          assert(Q.arity t f' = a);
          assert(Q.arity t g' = a);
          (g', f')
        )
        | None ->
        match match_QfoP_BRLE (C(false, false)) t f p with
        | Some(n, fC, p0, pC) -> (
          (* print_endline ("n00:"^(string_of_int n)); *)
          let (g'C, f'C) = select_rec fC pC in
          let f' = pushC00 ~n t f'C in
          let g' = if p0
            then pushU   ~n t g'C
            else pushC00 ~n t g'C
          in
          assert(Q.arity t f' = a);
          assert(Q.arity t g' = a);
          (g', f')
        )
        | None -> (
          (* no useless nor 0-canalizing variables *)
          let p0, p' = BRLE.pull p in
          let f0 = Q.cofactor t false f in
          let f1 = Q.cofactor t true  f in
          let (g'0, f'0) = select_mem f0 p' in
          let (g'1, f'1) = select_mem f1 p' in
          let (g', f') = if p0
            then ( (* non parameter variable *)
              (* phi' := var0 ? ( phi'1 /\ ~g'0 * ) : phi'0 *)
              assert(Q.arity t g'0 = Q.arity t f'1);
              let f' = Q.( *! ) t f'0 ( Q.( &!) t (neg t g'0) f'1) in
              let g' = pushU t (Q.(|!) t g'0 g'1) in
              (g', f')
            )
            else ( (* parameter variable *)
              let g' = Q.( *! ) t g'0 g'1 in
              let f' = Q.( *! ) t f'0 f'1 in
              (g', f')
            )
          in
          assert(Q.arity t f' = a);
          assert(Q.arity t g' = a);
          (g', f')
        )
      )
    and select_mem (f:Q.f) (p:BRLE.t) : Q.f * Q.f =
      apply (fun _ -> select_rec f p) (Q.id t f, p)
    in
    let r = select_rec f param in
    (* assert(Q.eq t (fst r) fst_r_ref); *)
    r

  let sat_select_A_first : (unit -> unit) -> unit =
    let tag = ref true in
    (fun f -> if !tag then (tag := false; f()))

  (* param.(i) iff x_i is a non-parameter variable *)
  (* the arity of the returned functions equals the arity of the input function *)
  let sat_select_A (t:Q.t) (f:Q.f) (param:Support.t) : Q.f * Q.f =
    if Q.builtin_o_prim U && Q.builtin_o_prim (C(true, false))
    then (sat_select_A_first (fun () -> print_endline "[DAGaml.AQEOPS.sat_select_A] v:1"); sat_select_A_v1 t f param)
    else (sat_select_A_first (fun () -> print_endline "[DAGaml.AQEOPS.sat_select_A] v:0"); sat_select_A_v0 t f param)

  (* val exist_proj : MLBDD.t -> int list -> MLBDD.t * (MLBDD.var * MLBDD.t) list
   * [exists_proj f suppB = g * (assoc[(v, p_v)])] where :
   * - [f  : bool^(suppA) -> bool^(suppB) -> bool] (where suppA = support(f) - suppB)
   * - [g  : bool^(suppA)                 -> bool]
   * such that :
   * - [suppB is Support.sorted]
   * - [g = \exists(suppB) f]
   * - [assoc is Assoc.sorted]
   * - [dom(assoc) = suppB]
   * - [\forall v\in suppB, p_v : bool^(suppA) -> bool]
   *   + i.e., [[assoc] : suppB -> bool^(suppA) -> bool)]
   * - [\forall v\in suppB, p_v := \exists(suppB - {v}) phi)[v:=1]]
   * === In Other Words ===
   * [exists_proj man phi suppB = (guard, assoc)] takes as input a BDD
   * [phi : bool^suppA -> bool^suppB -> bool] (where suppA := support(f) - suppB)
   * and a sorted list of variables [suppB].
   * It returns [g = \exists(suppB) phi : bool^(suppA) -> bool] the set of modes
   * for which there exists a solution and [assoc:suppB -> (bool^(suppA) -> bool)]
   * a sorted association list which associate to each variable [v] in [suppB] a
   * BDD [p_v := (\exists(suppB - {v}) phi)[v:=1] : bool^(suppA) -> bool].
   *)

  type psubst = {
    arity   : int;
    partial : f;
    assign  : (int * f) list;
  }

  (* [FIXME] *)
  let memo_eproj_v0, apply_eproj_v0 = Hashcache.Module.(make default_size) (* in *)

  (* param.(i) iff x_i is a non-parameter variable *)
  (* the arity of the returned functions equals the arity of the input function *)
  let exists_proj_A_v0 (t:Q.t) (f:Q.f) (param:Support.t) : psubst =
    let param_sorted_support = Support.to_sorted_support param in
    let param = Support.to_bool_list param in
    (* [FIXME] *)
    let apply = apply_eproj_v0 in
    let rec eproj_rec (f:Q.f) (p:bool list) : Q.f * (Q.f list) =
      let a = Q.arity t f in
      match p with
      | [] -> (f, [])
      | p0::p' -> (
        match Q.to_bool t f with
        | Some b ->  (
          let a' = MyList.count_true p in
          (Q.cst t b a, MyList.make a' (Q.cst t false a))
        )
        | None -> (
          let f0 = Q.cofactor t false f in
          let f1 = Q.cofactor t true  f in
          let oU = Q.eq t f0 f1 in
          if p0
          then ( (* non parameter variable *)
            if oU
            then ( (* useless non parameter variable *)
              (* no branching => no memoization *)
              let (g'0, a'0) = eproj_rec f0 p' in
              let g' = g'0 in
              let a' = g'0 :: a'0 in
              (pushU t g', pushU_list t a')
            )
            else (
              let (g'0, a'0) = eproj_mem f0 p' in
              let (g'1, a'1) = eproj_mem f1 p' in
              let g' = Q.( |! ) t g'0 g'1 in
              let a' = g'1 :: (MyList.map2 (Q.(|!) t) a'0 a'1) in
              (pushU t g', pushU_list t a')
            )
          )
          else ( (* parameter variable *)
            if oU
            then ( (* useless parameter variable *)
              let (g'0, a'0) = eproj_rec f0 p' in
              let g' = pushU t g'0 in
              let a' = pushU_list t a'0 in
              (g', a')
            )
            else (
              let (g'0, a'0) = eproj_mem f0 p' in
              let (g'1, a'1) = eproj_mem f1 p' in
              let g' = Q.( *! ) t g'0 g'1 in
              let a' = sha_list t a'0 a'1 in
              (g', a')
            )
          )
        )
      )
    and eproj_mem f p =
      apply (fun _ -> eproj_rec f p) (Q.id t f, p)
    in
    let (g, a) = eproj_rec f param in
    let assign = List.combine param_sorted_support a in
    {arity = Q.arity t f; partial = g; assign}

  (* [FIXME] *)
  let memo_eproj_v1, apply_eproj_v1 = Hashcache.Module.(make default_size) (* in *)

  (* param.(i) iff x_i is a non-parameter variable *)
  (* the arity of the returned functions equals the arity of the input function *)
  (* [builtin_o_prim U = true]
   *)
  let exists_proj_A_v1 (t:Q.t) (f:Q.f) (param:Support.t) : psubst =
    let pushU ?(n=1) (t:Q.t) (f:Q.f) : Q.f = Q.intro_o_prim t U n f in
    let pushU_list ?(n=1) (t:Q.t) (fl:Q.f list) : Q.f list = fl ||> (pushU ~n t) in
    let param_sorted_support = Support.to_sorted_support param in
    let param = Support.to_BRLE param in
    (* [FIXME] *)
    let apply = apply_eproj_v1 in
    let rec eproj_rec (f:Q.f) (p:BRLE.t) : Q.f * (Q.f list) =
      let a = Q.arity t f in
      match Q.to_bool t f with
      | Some b ->  (
        let a' = BRLE.count_true p in
        (Q.cst t b a, MyList.make a' (Q.cst t false a))
      )
      | None -> (
        let nU = Q.detect_o_prim t U f in
        if nU = 0 (* no useless variables *)
        then (
          let p0, p' = BRLE.pull p in
          let f0 = Q.cofactor t false f in
          let f1 = Q.cofactor t true  f in
          assert(Q.arity t f0 = BRLE.length p');
          assert(Q.arity t f1 = BRLE.length p');
          let (g'0, a'0) = eproj_mem f0 p' in
          let (g'1, a'1) = eproj_mem f1 p' in
          if p0
          then ( (* non parameter variable *)
            let g' = Q.( |! ) t g'0 g'1 in
            let a' = g'1 :: (MyList.map2 (Q.(|!) t) a'0 a'1) in
            (pushU t g', pushU_list t a')
          )
          else ( (* parameter variable *)
            let g' = Q.( *! ) t g'0 g'1 in
            let a' = sha_list t a'0 a'1 in
            (g', a')
          )
        )
        else ( (* the [nU] first variables are useless *)
          assert(nU > 0);
          match BRLE.uncons p with
          | None -> (* p = [] *) (failwith "[DAGaml.AQEOPS.eproj_rec] inconsistent behaviour")
          | Some ((np, p0), _) -> (
            let n = min nU np in
            assert(n > 0);
            let fU = Q.elim_o_prim t U n f in
            let _, pU = BRLE.pull ~n p in
            (* no branching => no memoization *)
            let (g'U, a'U) = eproj_rec fU pU in
            let g' = pushU ~n t g'U in
            let carry = pushU_list ~n t a'U in
            let a' = if p0 then (MyList.ntimes ~carry g' n) else carry in
            (g', a')
          )
        )
      )
    and eproj_mem f p =
      apply (fun _ -> eproj_rec f p) (Q.id t f, p)
    in
    assert(Q.arity t f = BRLE.length param);
    let (g, a) = eproj_rec f param in
    let assign = List.combine param_sorted_support a in
    {arity = Q.arity t f; partial = g; assign}

  let exists_proj_A_first : (unit -> unit) -> unit =
    let tag = ref true in
    (fun f -> if !tag then (tag := false; f()))

  (* param.(i) iff x_i is a non-parameter variable *)
  (* the arity of the returned functions equals the arity of the input function *)
  let exists_proj_A (t:Q.t) (f:Q.f) (param:Support.t) : psubst =
    if Q.builtin_o_prim U
    then (exists_proj_A_first (fun () -> print_endline "[DAGaml.AQEOPS.exists_proj_A] v:1"); exists_proj_A_v1 t f param)
    else (exists_proj_A_first (fun () -> print_endline "[DAGaml.AQEOPS.exists_proj_A] v:0"); exists_proj_A_v0 t f param)

  let argmaxA (t:Q.t) (g:Q.f) (p:Support.t) (f:Q.f) : Q.f * Q.f =
    let f' = existsA t p (Q.( &! ) t g f) in
    let g' = ( &! ) t g (Q.( =! ) t f f') in
    (f', g')

  (* val lexmax_rev : MLBDD.t -> Support.t -> MLBDD.t array -> MLBDD.t * (MLBDD.t array) *)

  let lexmaxA_rev (t:Q.t) (g:Q.f) (p:Support.t) (lex:Q.f array) : Q.f * (Q.f array) =
    let ari = Q.arity t g in
    let len = Array.length lex in
    let res = Array.make len (Q.cst t false ari) in
    let rec argmax_rec res g lex i =
      if i < 0 then (g, res) else (
        let (r_i, g') = argmaxA t g p lex.(i) in
        res.(i) <- r_i;
        argmax_rec res g' lex (pred i)
      )
    in
    argmax_rec res g lex (pred len)

  (* val lexmax : MLBDD.t -> Support.t -> MLBDD.t array -> MLBDD.t * (MLBDD.t array) *)
  let lexmaxA (t:Q.t) (g:Q.f) (p:Support.t) (lex:Q.f array) : Q.f * (Q.f array) =
    let guard', max' = lexmaxA_rev t g p (MyArray.rev lex) in
    (guard', MyArray.rev max')

  module PSubst =
  struct
    let domain (t:t) (xs:psubst) : Support.t =
      let sfi = Q.support t xs.partial in
      let sfl = xs.assign ||> snd ||> Q.support t |> Support.union_list xs.arity in
      Support.union sfi sfl
    let codomain (t:t) (xs:psubst) : Support.t =
      Support.of_sorted_support xs.arity (xs.assign ||> fst |> SetList.sort)
    let support (t:t) (xs:psubst) : Support.t =
      Support.union (domain t xs) (codomain t xs)
(*
    let subst (t:t) (f:f) (xs:psubst) : f =
      subst (Q.(&!) t f xs.partial) xs.partial
 *)

    let of_guard_A (t:t) (g:f) (elim:Support.t) : psubst =
      let inv1, g'   = sat_select_A  t g  elim in
      let xs = exists_proj_A t g' elim in
      assert(Q.eq t inv1 xs.partial);
      xs

    let is_trivial (t:t) (xs:psubst) : bool option =
      Q.to_bool t xs.partial

    let equal (t:t) (x1:psubst) (x2:psubst) : bool =
      x1.arity = x1.arity &&
      Q.eq t x1.partial x2.partial &&
      List.length x1.assign = List.length x1.assign &&
      List.for_all2 (fun (v1, f1) (v2, f2) -> v1 = v2 && Q.eq t f1 f2) x1.assign x2.assign
  end
end
