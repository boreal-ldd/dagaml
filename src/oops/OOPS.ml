(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : OOPS : Ordered OPeratorS
 *)

open GuaCaml
open BTools

module type MSig =
sig
  type t (* [f]'s manager *)
  type f

  (* Section 1. Functional / Propositional Aspects *)

  (*
    semantic of mask elements :
      function
      | true  -> Significant
      | false -> Useless
  *)
  val ( ->> ) : t -> bool list -> f -> f
  (* where [true] represents significant variables and [false] non significant ones *)
  (* ?? add method : phase shifter      ??
     ?? add method : partial evaluation ??
   *)
  val arity  : t -> f -> int
  val   cneg : t -> bool -> f -> f
  val ( &! ) : t -> f -> f -> f
  val ( |! ) : t -> f -> f -> f
  val ( ^! ) : t -> f -> f -> f
  val ( =! ) : t -> f -> f -> f
  val ( *! ) : t -> f -> f -> f

  val cst : t -> bool -> int -> f
  val var : t -> bool -> int -> int -> f
  (* [var t b0 n k = f]
      where [f] is the function (b:\B^n) -> b0 \xor b_k
   *)

  (* return a consistent projection of fs
     to [Some bool] or [None] otherwise *)
  val to_bool : t -> f -> bool option

  val cofactor : t -> bool -> f -> f
  (* returns an identifier which can be used safely for memoization purpose *)
  val id : t -> f -> barray
  (* returns [true] iff both inputs are syntactically identical *)
  val eq : t -> f -> f -> bool

  (* Section 2. Manager Management *)

  (* creates a new manager *)
  val newman : unit -> t

  (* [copy_into t1 fl1 t2 = fl2]
     copies a list of function from a given manager into another one
     assumes : [t1 == t2] => time complexity O(1)
   *)
  val copy_into : t -> f list -> t -> f list

  (* [to_barray ~nocopy:false ~destruct:false t fl = ba]
     serializes a list of functions from a given manager to a [barray]
    ?(nocopy=false) : [nocopy=true] indicates that all nodes are reachable, and,
      let t' = newman() in
      let fl' = copy_into t fl t' in
      assert(t = t' and fl = fl');
      (* thus no copy is required *)
    ?(destruct=false) : [destruct=true] indicates that the manager will no longer
      be used, hence the manager can be destroyed during the process
   *)
  val to_barray : ?nocopy:bool -> ?destruct:bool -> t -> f list -> barray

  (* [of_barray ~t:None ba = (t, fl)]
     unserializes a list of functions into a given manager from a [barray]
     ?(t=None) : [t=Some t'] where t' is the manager where the functions
       should be added to.
       [br_fl ~t:(Some t') cha = (t', fl)]
                 [t=None] creates a new manager
   *)
  val of_barray : ?t:(t option) -> barray -> t * f list

  (* serializes a list of functionss from a given manager to an arbitrary channel [ToBStream.Channel.t]
    ?(nocopy=false) : [nocopy=true] indicates that all nodes are reachable, and,
      let t' = newman() in
      let fl' = copy_into t fl t' in
      assert(t = t' and fl = fl');
      (* thus no copy is required *)
    ?(destruct=false) : [destruct=true] indicates that the manager will no longer
      be used, hence the manager can be destroyed during the process
   *)
  val bw_fl : ?nocopy:bool -> ?destruct:bool -> t -> f list bw
  (* unserializes a list of functions into a given manager from an arbitrary channel [OfBStream.Channel.t]
     ?(t=None) : [t=Some t'] where t' is the manager where the functions
       should be added to.
       [br_fl ~t:(Some t') cha = (t', fl)]
                 [t=None] creates a new manager
  *)
  val br_fl : ?t:(t option) -> (t * f list) br
  (* returns statistics about [t] *)
  val t_stats : t -> Tree.stree
  (* returns statistics about [t] and [f list] *)
  val f_stats : t -> f list -> Tree.stree
  (* clear caches : does not change the external semantic of the manager *)
  val clear_caches : t -> unit
  (* [keep_clean t fl = fl'] may remove any dead nodes that is not in [fl]
     returns their new identification as [fl']
     This function should have a low time/memory overhead.
     ~normalize:false => if ~normalize:true then [tof] can be used with ~normalize:false
   *)
  val keep_clean : ?normalize:bool -> t -> f list -> f list

  (* can be used for debug purpose, all methods are assumed to return functions such that
      [check t f = true]
   *)
  val check : t -> f -> bool

  (* Section 3. File-based Management *)

  module F :
  sig
    (*
    type t' (* file-based storage manager *)
    val newman : unit -> t'
     *)

    type f' (* file-based storage, should have a low (in RAM) memory overhead *)

    val arity : f' -> int
    val size  : f' -> int (* in bytes *)

    (* ~nocopy=false *)
    (* ~normalize=true, if ~normalize=false saves time, but does not guarentee canonicity (if the models allows) *)
    val of_f : ?nocopy:bool -> ?normalize:bool -> t -> f  -> f'
    val to_f : t -> f' -> f

    val free : f' -> unit (* free any underlying file-based storage *)
    (* val free_all : t' -> unit (* free all underlying files *) *)

    (* ~normalize:true => make the dumped canonical (if ~normalize:false canonicity is not ensured) *)
    val tof : ?normalize:bool -> f' list Io.ToF.t
      (* [warning] [f' list] are not free-ed by calling to [dump] *)
    val off : f' list Io.OfF.t
  end

  (* ~nocopy=false *)
  (* ~normalize:true => make the dumped canonical (if ~normalize:false canonicity is not ensured) *)
  (* ~destruct:false => if ~destruct:true then the current can be irreversibly altered and should no longer be used *)
  (* using a destroyed manager has unspecified behaviour (safe implemention should raise an error upon attempt) *)
  val tof : ?nocopy:bool -> ?normalize:bool -> ?destruct:bool -> t -> f list Io.ToF.t (* compatible with [F.tof] *)
  val off : (t * (f list)) Io.OfF.t (* compatible with [F.off] *)
end

module type Sig =
sig
  module O : MSig
  open O

  val neg : t -> f -> f
  val var : t -> bool -> int -> f

  val cst0 : t -> f
  val cst1 : t -> f

  val ite : t -> f -> f -> f -> f
  val biite : t -> (f * f) -> f -> f -> f

  val push_pass : t -> f -> f

  val array_make_n_var : t -> int -> f array
  val array_make_n_bivar : t -> int -> (f * f) array

  val list_make_n_var : t -> int -> f list

  val array_and : t -> ?arity:(int option) -> f array -> f
  val  list_and : t -> ?arity:(int option) -> f list -> f
  val  init_and : t -> ?arity:(int option) -> int -> (int -> f) -> f

  val array_or : t -> ?arity:(int option) -> f array -> f
  val  list_or : t -> ?arity:(int option) -> f list -> f
  val  init_or : t -> ?arity:(int option) -> int -> (int -> f) -> f

  val array_neg : t -> f array -> f array
  val list_neg : t -> f list -> f list

  val array_at_least_one : t -> ?arity:(int option) -> f array -> f
  val  list_at_least_one : t -> ?arity:(int option) -> f list -> f

  val array_exactly_one : t -> f array -> f
  val array_atmost_one : t -> f array -> f

  val array_copy_fun : t -> int -> f -> f array
  val array_copy_fun_t : t -> int -> f -> f array

  val array_copy_fun_array : t -> int -> f array -> f Matrix.matrix
  val array_copy_fun_array_t : t -> int -> f array -> f Matrix.matrix

  type 's system = ('s, O.t, O.f) IoTypes.system

  module ToF :
  sig
    open Io.ToF
    val system : ?nocopy:bool -> 's t -> 's system t
    val ssystem : ?nocopy:bool -> string system t
  end

  module OfF :
  sig
    open Io.OfF
    val system : 's t -> 's system t
    val ssystem : string system t
  end

  val system_tof : ?nocopy:bool -> string system -> string -> unit
  val system_off : string -> string system

  module ToBStream :
  sig
    val system : ?nocopy:bool -> 's bw -> 's system bw
    val ssystem : ?nocopy:bool -> string system bw
  end

  module OfBStream :
  sig
    val system : 's br -> 's system br
    val ssystem : string system br
  end

  val to_file : ?nocopy:bool -> string system -> string -> unit
  val of_file : string -> string system
end

module Make(O:MSig) : Sig
  with type O.t    = O.t
  and  type O.f    = O.f
  and  type O.F.f' = O.F.f'
=
struct
  module O = O
  include O

  let neg t f = cneg t true f

  (* match b with false -> id | true -> neg *)
  let var t b n =
    assert(n>=1);
    ( *! ) t (cst t b (n-1)) (cst t (not b) (n-1))
  let cst0 t = cst t false 0
  let cst1 t = cst t true  0

  let ite t cond edge0 edge1 =
    (|!) t ((&!) t cond edge1) ((&!) t (neg t cond) edge0)

  let biite t (cond, ncond) edge0 edge1 =
    (|!) t ((&!) t cond edge1) ((&!) t ncond edge0)

  let push_pass t f =
    let n = arity t f in
    (->>) t (false::(MyList.ntimes true n)) f

  let array_make_n_var t n =
    Array.init n (fun k -> O.var t false n k)

  let array_make_n_bivar t n =
    Array.init n (fun k -> (O.var t false n k, O.var t true n k))

(* [LEGACY]
  let array_make_n_var t n =
    let var0 = var t false 1 in
    Array.init n (fun x -> (->>) t (Tools.subset true false x n)var0)
 *)

(* (* [TODO] profiling, improvement observed for QEOPS_CoSTreD2 + Ldd_G_o_u *)
  let array_make_n_var t n =
    Array.init n (fun k ->
      let k' = n-k-1 in
      let idk = Q.( *! ) t (Q.cst t false k') (Q.cst t true k') in
      let mask = MyList.ntimes
        ~carry:(MyList.make (n-k) true)
         false k
      in
      Q.(->>) t mask idk
    )
 *)

  let  list_make_n_var t n =
    Array.to_list (array_make_n_var t n)

  let array_and t ?(arity=None) fa =
    if Array.length fa = 0
    then (
      match arity with
      | None -> failwith "[OOPS.array_and] empty array /\ no arity"
      | Some n -> cst t true n
    )
    else (
      Tools.tree_of_array ((&!) t) fa
    )
  let  list_and t ?(arity=None) fl =
    match fl with
    | [] -> (
      match arity with
      | None -> failwith "[OOPS.listy_and] empty list /\ no arity"
      | Some n -> cst t true n
    )
    | _ -> (fl |> Array.of_list |> (array_and t))

  let init_and t ?(arity=None) size f = array_and t ~arity (Array.init size f)

  let array_or t ?(arity=None) fa =
    if Array.length fa = 0
    then (
      match arity with
      | None -> failwith "[OOPS.array_or] empty array /\ no arity"
      | Some n -> cst t false n
    )
    else (
      Tools.tree_of_array ((|!) t) fa
    )
  let  list_or t ?(arity=None) fl =
    fl |> Array.of_list |> (array_or ~arity t)

  let  init_or t ?(arity=None) size f = array_or ~arity t (Array.init size f)

  let array_neg t fa = Array.map (neg t) fa
  let  list_neg t fl =  Tools.map (neg t) fl

  let array_at_least_one = array_or
  let  list_at_least_one =  list_or

  let array_exactly_one t fa =
    let n = Array.length fa in
    assert(n>=1);
    let size = arity t fa.(0) in
    let neg_array = array_neg t fa in
    (* [tempX.(i)] iff forall k < i, not array.(k)
       [tempY.(i)] iff forall k > i, not array.(k) *)
    let tempX = Array.make n (cst t true size)
    and tempY = Array.make n (cst t true size) in
    for i = 0 to (n-2)
    do
      tempX.(i+1) <- (&!) t tempX.(i) neg_array.(i);
    done;
    for i = (n-1) downto 1
    do
      tempY.(i-1) <- (&!) t tempY.(i) neg_array.(i);
    done;
    init_or t n (fun i -> (&!) t ((&!) t tempX.(i) tempY.(i)) fa.(i))

  let array_atmost_one t fa =
    assert(Array.length fa >= 1);
    let one = array_exactly_one t fa
    and noone = array_and t (array_neg t fa) in
    (|!) t one noone

  let array_copy_fun t nb (f:f) =
    let size = arity t f in
  (* f -> [f..., .f.., ..f., ...f] *)
    let tn = MyList.ntimes true  size
    and fn = MyList.ntimes false size in
    let funmap idx = (->>) t (Tools.subset_t tn fn idx nb) f in
    Array.init nb funmap

  let array_copy_fun_t t nb (f:f) =
    let size = arity t f in
  (* f -> [f(0)...f(1)..., .f(0)...f(1).., etc.] *)
    let funmap idx =
      (->>) t (MyList.ncopy (Tools.subset true false idx nb) size) f
    in
    Array.init nb funmap

  let array_copy_fun_array t ncopy (fa:f array) =
    let nfunc = Array.length fa in
    fa
      |> Array.map (array_copy_fun t ncopy)
      |> Matrix.matrix_of_array2 nfunc ncopy
      |> Matrix.transpose

  let array_copy_fun_array_t t ncopy (fa:f array) =
    let nfunc = Array.length fa in
    fa
      |> Array.map (array_copy_fun_t t ncopy)
      |> Matrix.matrix_of_array2 nfunc ncopy
      |> Matrix.transpose

  type 's system = ('s, O.t, O.f) IoTypes.system

  module ToF :
  sig
    open Io.ToF
    val system : ?nocopy:bool -> 's t -> 's system t
    val ssystem : ?nocopy:bool -> string system t
  end
  =
  struct
    open Io.ToF
    open IoUtils.ToF

    let system ?(nocopy=false) tof_s cha (s:'s system) : unit =
      let bw_man cha (t, fl) =
        O.tof ~nocopy t cha (Array.to_list fl)
      in
      system tof_s bw_man cha s

    let ssystem ?(nocopy=false) cha s : unit = system ~nocopy string cha s
  end

  module OfF :
  sig
    open Io.OfF
    val system : 's t -> 's system t
    val ssystem : string system t
  end
  =
  struct
    open Io.OfF
    open IoUtils.OfF

    let system (off_s:'s t) cha : 's system =
      let br_man cha =
        let t, fl = O.off cha in
        (t, Array.of_list fl)
      in
      system off_s br_man cha

    let ssystem cha : string system = system string cha
  end

  let system_tof ?(nocopy=false) (sys:string system) (file:string) : unit =
    try
      let cha = open_out_bin file in
      ToF.ssystem ~nocopy cha sys;
      close_out cha;
    with exn -> raise (OUnit.cascade_exn "OfExprOfOops.system_tof" exn)

  let system_off (file:string) : string system =
    try
      let cha = open_in_bin file in
      let sys = OfF.ssystem cha in
      close_in cha;
      sys
    with exn -> raise (OUnit.cascade_exn "OfExprOfOops.system_off" exn)

  module ToBStream :
  sig
    val system : ?nocopy:bool -> 's bw -> 's system bw
    val ssystem : ?nocopy:bool -> string system bw
  end
  =
  struct
    open BTools.ToBStream
    open IoUtils.ToBStream

    let system ?(nocopy=false) bw_s cha (s:'s system) : unit =
      let bw_man cha (t, fl) =
        O.bw_fl ~nocopy t cha (Array.to_list fl)
      in
      system bw_s bw_man cha s

    let ssystem ?(nocopy=false) cha s : unit = system ~nocopy string cha s
  end

  module OfBStream :
  sig
    val system : 's br -> 's system br
    val ssystem : string system br
  end
  =
  struct
    open BTools.OfBStream
    open IoUtils.OfBStream

    let system (br_s:'s br) cha : 's system =
      let br_man cha =
        let t, fl = O.br_fl cha in
        (t, Array.of_list fl)
      in
      system br_s br_man cha

    let ssystem cha : string system = system string cha
  end

  let to_file ?(nocopy=false) (sys:string system) (file:string) : unit =
    try
      let cha = BTools.ToBStream.Channel.open_file file in
      ToBStream.ssystem ~nocopy cha sys;
      BTools.ToBStream.Channel.close_file cha
    with exn -> raise (OUnit.cascade_exn "IoUtils.to_file" exn)

  let of_file (file:string) : string system =
    try
      let cha = BTools.OfBStream.Channel.open_file file in
      let sys = OfBStream.ssystem cha in
      BTools.OfBStream.Channel.close_file cha;
      sys
    with exn -> raise (OUnit.cascade_exn "IoUtils.of_file" exn)
end
