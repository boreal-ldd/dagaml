(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : AVUInt : Advanced VUInt Operators
 *)

open GuaCaml
open Extra

let prefix = "DAGaml.AVUInt"

module type Sig =
sig
  module Q : AQEOPS.MSig
  open Q

  module Vuint : VUInt.Sig
    with type O.t = Q.t
    and  type O.f = Q.f
    and  type O.F.f' = Q.F.f'
  open Vuint

  module Aqeops : AQEOPS.Sig
    with type Q.t = Q.t
    and  type Q.f = Q.f
    and  type Q.F.f' = Q.F.f'

  val argmaxA_uint : t -> f -> Support.t -> uint -> f * uint

  (* puint = partial UInt *)
  type puint = f * uint

  module P :
  sig
    val argmaxA : t -> Support.t -> puint -> puint
    val support : t -> puint -> Support.t
    val zero : t -> int -> puint
    val of_guard : t -> f -> puint
    val of_uint : t -> uint -> puint
    val ( +/ ) : t -> puint -> puint -> puint
    val addl : t -> int -> puint list -> puint
    val coproj_proj : t -> Support.t -> puint -> f * puint
    val is_trivial : t -> puint -> bool option
  end
end

module Make(Q:AQEOPS.MSig) : Sig
  with type Q.t    = Q.t
  and  type Q.f    = Q.f
  and  type Q.F.f' = Q.F.f'
=
struct
  module Q = Q
  open Q

  module Vuint = VUInt.Make(Q)
  open Vuint

  module Aqeops = AQEOPS.Make(Q)
  open Aqeops

  let argmaxA_uint (t:t) (g:f) (elim:Support.t) (uint:uint) : f * uint =
    let x, array = lexmaxA_rev t g elim uint.array in
    (x, {uint with array})

  (* puint = partial UInt *)
  type puint = f * uint

  module P =
  struct
    let prefix = prefix ^ ".P"
    let argmaxA (t:t) (elim:Support.t) ((x, u):puint) : puint =
      argmaxA_uint t x elim u
    let support (t:t) ((x, n):puint) : Support.t =
      (x::(Array.to_list n.array)) ||> Q.support t |> Support.union_list n.arity

    let zero (t:t) (arity:int) : puint =
      (Q.cst t true arity, uint_zero t arity)
    let of_guard (t:t) (g:f) : puint =
      (g, uint_zero t (Q.arity t g))
    let of_uint (t:t) (fn:uint) : puint =
      let n = fn.arity in
      (Q.cst t true n, fn)

    let ( +/ ) (t:t) ((x1, n1):puint) ((x2, n2):puint) : puint =
      (( &! ) t x1 x2, ( +/ ) t n1 n2)

(*
    let addl (t:t) (arity:int) (xnl:puint list) : puint =
      match xnl with
      | [] -> zero t arity
      | h::xnl' -> (
        assert((snd h).arity = arity);
        List.fold_left (( +/ ) t) h xnl'
      )
 *)

    let addl (t:t) (arity:int) (xnl:puint list) : puint =
      let n' = list_add_3to2 t ~arity:(Some arity) (xnl ||> snd) in
      let g' = Q.( &!! ) t arity (xnl ||> fst) in
      (g', n')

    let coproj_proj (t:t) (param:Support.t) (xu:puint) : f * puint =
      let (x1', u2) = argmaxA t param xu in
      let x2 = Q.existsA t param (fst xu) in
      (x1', (x2, u2))

    let is_trivial (t:t) ((x, n):puint) : bool option =
      match Q.to_bool t x with
      | None -> None
      | Some false -> Some false
      | Some true -> (
        if Array.length n.array = 0
        then Some true
        else None
      )
  end
end
