(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : AQEOPS_CoSTreD_ArgMax_vUInt : interfacing DAGaml with CoSTreD via AQEOPS
 *                                      ArgMax/vUInt case
 *)

open GuaCaml
open Extra
open STools

let prefix = "DAGaml.AQEOPS_CoSTreD_ArgMax_vUInt"

module type Sig =
sig
  module Q : AQEOPS.MSig
  open Q

  module Avuint : AvUInt.Sig
    with type Q.t = Q.t
    and  type Q.f = Q.f
    and  type Q.F.f' = Q.F.f'
  open Avuint
  open Vuint
  open Aqeops
  open Oops

  module AndL :
  sig
    val wap_of_system : t -> int list -> f list -> Snowflake.Wap_exchange.input

    type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

    val default_wap_solver : wap_solver (* = Snowflake.Wap_lightspeed.lightspeed_greedy *)

    val compute : ?verbose:int -> ?wap_solver:wap_solver -> ?support_consistency:bool ->
      t -> int -> int list -> f list -> f
  end

  module Argmax_UInt :
  sig
    val wap_of_system : t -> int list -> puint list -> Snowflake.Wap_exchange.input

    type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

    val default_wap_solver : wap_solver (* = Snowflake.Wap_lightspeed.lightspeed_greedy *)

    (* [Error None]    => FRP detect constraint inconsistency (i.e., the formula is unsatisfiable)
     * [Error(Some r)] => otherwise, [halt_bpp _ _] returns [Some r]
     * [Ok f]          => otherwise, the FRP is complete, [halt_bpp _ _] checked uses-level consistency, BPP is complete
     *)
    val compute : ?verbose:int -> ?wap_solver:wap_solver -> ?halt_bpp:(t -> f list -> 'res option) -> ?support_consistency:bool ->
      t -> int -> int list -> puint list -> (f, 'res option) result
  end

  module Argmax_UInt_Select :
  sig
    val wap_of_system : t -> int list -> puint list -> Snowflake.Wap_exchange.input

    type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

    val default_wap_solver : wap_solver (* = Snowflake.Wap_lightspeed.lightspeed_greedy *)

    (* [Error None]    => FRP detect constraint inconsistency (i.e., the formula is unsatisfiable)
     * [Error(Some r)] => otherwise, [halt_bpp _ _] returns [Some r]
     * [Ok assoc]      => otherwise, the FRP is complete, [halt_bpp _ _] checked uses-level consistency, BPP is complete
     *)
    val compute : ?verbose:int -> ?wap_solver:wap_solver -> ?halt_bpp:(t -> f list -> 'res option) -> ?support_consistency:bool ->
      t -> int -> int list -> puint list -> ((int * Q.f)list, 'res option) result
  end
end

module Make(Q:AQEOPS.MSig) : Sig
  with type Q.t = Q.t
  and  type Q.f = Q.f
  and  type Q.F.f' = Q.F.f'
=
struct
  module Q = Q

  module Avuint = AvUInt.Make(Q)
  open Avuint
  open Vuint
  open Aqeops
  open Oops

  module AndL =
  struct
    let prefix = prefix ^ ".AndL"

    (* Section 3. Full Reduction (CoSTreD) *)
    module Model =
    struct
      type t = Q.t
      type f0 = Q.f (* input RL *)
      type f1 = Q.f (* pre-FRP  intermediary RL *)
      type f2 = Q.f (* post-FRP intermediary RL *)
      type f3 = Q.f (* post-BPP RL *)

      (* support *)
      let support0 t f = Q.support t f |> Support.to_sorted_support
      let support1 = support0
      let support2 = support0
      let support3 = support0

      (* trivial *)
      let trivial0 t f = Q.to_bool t f
      let trivial1 = trivial0
      let trivial2 = trivial0
      let trivial3 = trivial0

      type supp = int list

      (* Section 1. FRP *)
      (*  solve_variable (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 *)
      let solve_variable (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) ( _  :supp) (elim:supp) ( _  :supp) : (f1 list) * f2 =
        let f0 = Q.(&!!) man ga f0l in
        let cp = Q.(&!!) man ga (f0::f1l) in
        let p = Q.existsA man (Support.of_sorted_support ga elim) cp in
        ([p], cp)

      (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

      (* Section 2.0 FRP to BPP *)
      (*  solve_parameter (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 *)
      let solve_parameter (man:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
        let f0 = Q.(&!!) man ga f0l in
        let f1 = Q.(&!!) man ga (f0::f1l) in
        f1

      (* Section 2. BPP *)
      (*  backproj (man:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 *)
      let backproj (man:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 =
        let elim = SetList.minus supp3 inter in (* SetList.minus supp3 supp2 *)
        let f3' = Q.existsA man (Support.of_sorted_support ga elim) f3 in
        Q.(&!) man f3' f2
    end

    module Module = Snowflake.CoSTreD.Make(Model)

    let wap_of_system (man:Q.t) (parameters:int list) (fl:Q.f list) : Snowflake.Wap_exchange.input =
      let kdecomp : int list list = fl ||> (fun f -> f |> Q.support man |> Support.to_sorted_support) in
      let variables = SetList.union_list kdecomp in
      (* we remove parameter variables from suppressible variables *)
      let variables = SetList.minus variables parameters in
      let wap = Snowflake.Wap_exchange.{variables; parameters; kdecomp} in
      (* checks that wap's input is well formed *)
      Snowflake.Wap_exchange_utils.assert_input wap;
      wap

    type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

    let default_wap_solver : wap_solver =
      Snowflake.Wap_lightspeed.lightspeed_greedy

    let compute
       ?(verbose=0)
       ?(wap_solver=default_wap_solver)
       ?(support_consistency=true)
        (man:Q.t)
        (arity:int)
        (parameters:int list)
        (fl:Q.f list) : Q.f =
      let prefix = "["^prefix^".compute]" in
      let parameters = SetList.sort parameters in
      let stop = OProfile.(time_start default (prefix^" wap_building")) in
      let wap_in = wap_of_system man parameters fl in
      stop();
      if verbose >= 2 then print_endline (prefix^" wap_in:"^(Snowflake.Wap_exchange_utils.ToS.input wap_in));
      let stop = OProfile.(time_start default (prefix^" wap_solving")) in
      let out_wap = wap_solver wap_in in
      stop();
      if verbose >= 2 then print_endline (prefix^" out_wap:"^(Snowflake.Wap_exchange_utils.ToS.output out_wap));
      let stop = OProfile.(time_start default (prefix^" costred_call")) in
      let opr = Module.apply man ~check:support_consistency arity fl [] out_wap in
      stop();
      match opr with
      | None -> Q.cst man false arity
      | Some(_, _, out_bpp) -> (
        let stop = OProfile.(time_start default (prefix^" big_and")) in
        let f = Q.(&!!) man arity (Array.to_list out_bpp) in
        stop();
        if verbose >= 1 then OProfile.(print_table default);
        f
      )
  end

  module Argmax_UInt =
  struct
    let prefix = prefix ^ ".Argmax_Uint"

    (* Section 3. Full Reduction (CoSTreD) *)
    module Model =
    struct
      type t = Q.t
      (* input RL *)
      type f0 = Avuint.puint
      (* pre-FRP  intermediary RL *)
      type f1 = Avuint.puint
      (* post-FRP intermediary RL *)
      type f2 = Q.f
      (* post-BPP RL *)
      type f3 = Q.f

      (* support *)
      let support0 t xn = P.support t xn |> Support.to_sorted_support
      let support1 t xn = P.support t xn |> Support.to_sorted_support
      let support2 t f = Q.support t f   |> Support.to_sorted_support
      let support3 t f = Q.support t f   |> Support.to_sorted_support

      (* trivial *)
      let trivial0 t f = P.is_trivial t f
      let trivial1 t f = P.is_trivial t f
      let trivial2 t f = Q.to_bool t f
      let trivial3 t f = Q.to_bool t f

      type supp = int list

      (* Section 1. FRP *)
      (*  solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 *)
      let solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) ( _  :supp) (elim:supp) ( _  :supp) : (f1 list) * f2 =
        let tot = P.addl t ga (f0l@f1l) in
        let cp, p = P.coproj_proj t (Support.of_sorted_support ga elim) tot in
        ([p], cp)

      (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

      (* Section 2.0 FRP to BPP *)
      (*  solve_parameter (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 *)
      let solve_parameter (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
        let tot = P.addl t ga (f0l@f1l) in
        fst tot

      (* Section 2. BPP *)
      (*  backproj (t:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 *)
      let backproj (t:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 =
        let elim = SetList.minus supp3 inter in (* SetList.minus supp3 supp2 *)
        let f3' = Q.existsA t (Support.of_sorted_support ga elim) f3 in
        (Q.(&!) t f3' f2)
    end

    module Module = Snowflake.CoSTreD.Make(Model)

    let wap_of_system (t:Q.t) (parameters:int list) (fl:Avuint.puint list) : Snowflake.Wap_exchange.input =
      let kdecomp : int list list = fl ||> (fun f -> f |> P.support t |> Support.to_sorted_support) in
      let variables = SetList.union_list kdecomp in
      (* we remove parameter variables from suppressible variables *)
      let variables = SetList.minus variables parameters in
      let wap = Snowflake.Wap_exchange.{variables; parameters; kdecomp} in
      (* checks that wap's input is well formed *)
      Snowflake.Wap_exchange_utils.assert_input wap;
      wap

    type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

    let default_wap_solver : wap_solver =
      Snowflake.Wap_lightspeed.lightspeed_greedy

    (* returns [None] if the formula is false
       returns [Some _ ] otherwise *)
    let compute
       ?(verbose=0)
       ?(wap_solver=default_wap_solver)
       ?(halt_bpp=((fun _ _ -> None):(Model.t -> Model.f3 list -> 'res option)))
       ?(support_consistency=true)
       (* if stop_frp (if specified) returns [Some res], halts the computation after FRP and returns (Error res)
          otherwise continue with BPP *)
        (t:Q.t)
        (arity:int)
        (parameters:int list)
        (fl:Avuint.puint list) : (Q.f, 'res option) result =
      let prefix = "["^prefix^".compute]" in
      let parameters = SetList.sort parameters in
      let stop = OProfile.(time_start default (prefix^" wap_building")) in
      let wap_in = wap_of_system t parameters fl in
      stop();
      if verbose >= 2 then print_endline (prefix^" wap_in:"^(Snowflake.Wap_exchange_utils.ToS.input wap_in));
      let stop = OProfile.(time_start default (prefix^" wap_solving")) in
      let out_wap = wap_solver wap_in in
      stop();
      if verbose >= 2 then print_endline (prefix^" out_wap:"^(Snowflake.Wap_exchange_utils.ToS.output out_wap));
      let stop = OProfile.(time_start default (prefix^" costred_call")) in
      let opr = Module.apply_haltf3 t ~check:support_consistency arity fl [] out_wap halt_bpp in
      stop();
      match opr with
      | Error value -> Error value
      | Ok(_, _, out_bpp) -> (
        let stop = OProfile.(time_start default (prefix^" big_and")) in
        let f = Q.(&!!) t arity (Array.to_list out_bpp) in
        stop();
        if verbose >= 1 then OProfile.(print_table default);
        Ok f
      )
  end

  module Argmax_UInt_Select =
  struct
    let prefix = prefix ^ ".Argmax_UInt_Select"

    (* Section 3. Full Reduction (CoSTreD) *)
    module Model =
    struct
      let prefix = prefix ^ ".Model"
      type t = {
        bdd_man : Q.t;
        param_v : int list
      }
      (* input RL *)
      type f0 = Avuint.puint
      (* pre-FRP  intermediary RL *)
      type f1 = Avuint.puint
      (* post-FRP intermediary RL *)
      type f2 = Q.f
      (* post-BPP RL *)
      type f3 = psubst * Q.f

      (* support *)
      let support0 t xn = P.support t.bdd_man xn |> Support.to_sorted_support
      let support1 t xn = P.support t.bdd_man xn |> Support.to_sorted_support
      let support2 t f = Q.support t.bdd_man f   |> Support.to_sorted_support
      let support3 t ((xs, fs):f3) =
        Support.union (PSubst.support t.bdd_man xs) (Q.support t.bdd_man fs)
          |> Support.to_sorted_support

      (* trivial *)
      let trivial0 t f = P.is_trivial t.bdd_man f
      let trivial1 t f = P.is_trivial t.bdd_man f
      let trivial2 t f = Q.to_bool t.bdd_man f
      let trivial3 t ((xs, fs):f3) = Q.to_bool t.bdd_man fs

      type supp = int list

      (* Section 1. FRP *)
      (*  solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 *)
      let solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 =
        (* let prefix = "["^prefix^".solve_variable]" in *)
        (* let stop = OProfile.(time_start default (prefix^" P.addl")) in *)
        let tot = P.addl t.bdd_man ga (f0l@f1l) in
        (* stop(); *)
        (* let stop = OProfile.(time_start default (prefix^" P.coproj_proj")) in *)
        let cp, p = P.coproj_proj t.bdd_man (Support.of_sorted_support ga elim) tot in
        (* stop(); *)
        (* assert(SetList.subset_of (sorted_support cp) supp); *)
        (* assert(SetList.subset_of (P.sorted_support p) proj); *)
        ([p], cp)

      (* [solve t n f0l f1l supp elim proj = (proj, coproj)] *)

      (* Section 2.0 FRP to BPP *)
      (*  solve_parameter (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 *)
      let solve_parameter (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
        let tot = P.addl t.bdd_man ga (f0l@f1l) in
        (* assert(SetList.subset_of (P.sorted_support tot) t.param_v); *)
        (AQEOPS.{arity = ga; partial = fst tot; assign = []}, fst tot)

      (* Section 2. BPP *)
      (*  backproj (t:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (inter:supp) (supp2:supp) : f3 *)
      let backproj (t:t) (ga:int) ((xs', fs'):f3) (cp:f2) (xs'_supp:supp) (inter:supp) (cp_supp:supp) : f3 =
        (* let prefix = "["^prefix^".backproj]" in *)
        (*
        assert(SetList.subset_of (sorted_support cp) cp_supp);
        assert(SetList.subset_of (PSubst.sorted_support xs') (SetList.union xs'_supp t.param_v));
        assert(SetList.subset_of (PSubst.sorted_domain xs') t.param_v);
        assert(SetList.subset_of (sorted_support fs') (SetList.union xs'_supp t.param_v));
         *)
        (* advanced consistency verification *)
        (*
        let var3 = SetList.minus xs'_supp t.param_v in
        let var2 = SetList.minus cp_supp t.param_v in
        assert(PSubst.equal xs' (exists_proj fs' var3));
         *)

        (* cp  : is the local solution space *)
        (* fs' : is the selected solution space of the ancestor *)
        (* local_sol : is the intersection between cp and fs' restricted to supp2 *)
        let local_sol =
          let elim =
            SetList.minus xs'_supp (SetList.union cp_supp t.param_v)
            |> Support.of_sorted_support ga
          in
          (* we compute the local solution space by intersection *)
           Q.(&!) t.bdd_man cp (Q.existsA t.bdd_man elim fs')
        in
        (* intern_sol : is the projection of f3'2 to (supp2 - supp3) + param *)
        let intern_sol =
          let elim =
            SetList.minus (SetList.inter xs'_supp cp_supp) t.param_v
            |> Support.of_sorted_support ga
          in
          Q.existsA t.bdd_man elim local_sol
        in
        (* [DEBUG] we check that the transformation is equivalent to injecting local projections *)
        (* assert(Q.eq t intern_sol (PSubst.subst cp xs')); (* [DEBUG] *) *)
        (* we compute the relevant set of variables for selection and projection *)
        let sorted_elim =
          SetList.minus cp_supp (SetList.union xs'_supp t.param_v)
            |> Support.of_sorted_support ga
        in
        (* intern_selected : sat_select intern_sol *)
        let intern_selected =
          (* print_endline ("intern_sol:"^(Q.to_string t.bdd_man intern_sol)); *)
          let inv, guard = sat_select_A t.bdd_man intern_sol sorted_elim in
          assert(Q.eq t.bdd_man inv xs'.partial);
          guard
        in
        (* fs : local selected solution space *)
        let fs = Q.(&!) t.bdd_man local_sol intern_selected in
        (* xs_selected : computing internal projections *)
        let xs_selected : psubst =
          let xs = exists_proj_A t.bdd_man intern_selected sorted_elim in
          assert(Q.eq t.bdd_man xs.partial xs'.partial);
          xs
        in
        (* [DEBUG] we check that projections are properly computed *)
        (* assert(PSubst.equal xs_selected (PSubst.of_guard intern_sol sorted_elim)); *)
        (* xs : combining internal and ancestor projection to obtain local projections *)
        let snd_xs_1 = Assoc.update_with ~noconflict:true xs_selected.assign xs'.assign in
        let snd_xs = Assoc.restr snd_xs_1 cp_supp in
        let xs : psubst = {xs' with assign = snd_xs} in
        (* assert(PSubst.equal xs (exists_proj fs var2)); (* [DEBUG] *) *)
        (xs, fs)
    end

    module Module = Snowflake.CoSTreD.Make(Model)

    let wap_of_system (man:Q.t) (parameters:int list) (fl:Avuint.puint list) : Snowflake.Wap_exchange.input =
      let kdecomp : int list list = fl ||> (fun f -> f |> P.support man |> Support.to_sorted_support) in
      let variables = SetList.union_list kdecomp in
      (* we remove parameter variables from suppressible variables *)
      let variables = SetList.minus variables parameters in
      let wap = Snowflake.Wap_exchange.{variables; parameters; kdecomp} in
      (* checks that wap's input is well formed *)
      Snowflake.Wap_exchange_utils.assert_input wap;
      wap

    type wap_solver = Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output

    let default_wap_solver : wap_solver =
      Snowflake.Wap_lightspeed.lightspeed_greedy

    let compute
       ?(verbose=0)
       ?(wap_solver=default_wap_solver)
       ?(halt_bpp=((fun _ _ -> None):(Q.t -> Q.f list -> 'res option)))
       ?(support_consistency=true)
       (* if stop_frp (if specified) returns [Some res], halts the computation after FRP and returns (Error res)
          otherwise continue with BPP *)
        (man:Q.t)
        (arity:int)
        (parameters:int list)
        (fl:Avuint.puint list) : ((int * Q.f)list, 'res option) result =
      let prefix = "["^prefix^".compute]" in
      let parameters = SetList.sort parameters in
      let stop = OProfile.(time_start default (prefix^" wap_building")) in
      let wap_in = wap_of_system man parameters fl in
      stop();
      if verbose >= 2 then print_endline (prefix^" wap_in:"^(Snowflake.Wap_exchange_utils.ToS.input wap_in));
      let stop = OProfile.(time_start default (prefix^" wap_solving")) in
      let out_wap = wap_solver wap_in in
      stop();
      if verbose >= 2 then print_endline (prefix^" out_wap:"^(Snowflake.Wap_exchange_utils.ToS.output out_wap));
      let stop = OProfile.(time_start default (prefix^" costred_call")) in
      let t = Model.{bdd_man = man; param_v = parameters} in
      let halt (t:Model.t) (ifl:Model.f3 list) =
        halt_bpp t.Model.bdd_man (ifl ||> (fun (xs, _) -> xs.partial))
      in
      let opr = Module.apply_haltf3 t ~check:support_consistency arity fl [] out_wap halt in
      stop();
      match opr with
      | Error value -> Error value
      | Ok(_, _, out_bpp) -> (
        let stop = OProfile.(time_start default (prefix^" gathering")) in
        let fl = Array.fold_left (fun carry (xs, _) -> Assoc.update_with carry xs.assign) [] out_bpp in
        stop();
        if verbose >= 1 then OProfile.(print_table default);
        Ok fl
      )
  end
end
