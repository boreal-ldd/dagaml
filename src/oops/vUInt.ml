(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : VUInt : Variable size Unsigned INTegers
 *)

open GuaCaml

module type Sig =
sig
  module O : OOPS.MSig
  open O

  module Oops : OOPS.Sig
    with type O.t    = O.t
    and  type O.f    = O.f
    and  type O.F.f' = O.F.f'

  val fulladd2 : t -> f -> f -> f * f
  val fulladd3 : t -> f -> f -> f -> f * f

  (* arity of each [edge], usually arity <> Array.length array *)
  type uint = {arity : int; array : f array}

  val length : uint -> int
  val uint_get : t -> ?default:f -> int -> uint -> f

  val map : (f -> f) -> uint -> uint
  val uint_of_bool : t -> f -> uint
  val uint_of_int : t -> int -> int -> uint

  val uint_one : t -> int -> uint
  val uint_zero : t -> int -> uint

  val add : t -> ?carry:f -> uint -> uint -> uint
  val ( +/ ) : t -> uint -> uint -> uint

  val array_add : t -> uint array -> uint
  val  list_add : t ->  uint list -> uint

  val ( +?/ ) : t -> uint -> int -> uint
  val ( ?+/ ) : t -> int -> uint -> uint

  (* decreasing shift *)
  val shift_right : t -> int -> uint -> uint
  val ( >>/ ) : t -> uint -> int -> uint

  (* increasing shift *)
  val shift_left : t -> int -> uint -> uint
  val ( <</ ) : t -> uint -> int -> uint

  val add_3to2 : t -> uint -> uint -> uint -> uint * uint
  val  list_add_3to2 : t -> ?arity:(int option) -> uint list  -> uint
  val array_add_3to2 : t -> ?arity:(int option) -> uint array -> uint

  val bitwise_binop : (t->f->f->f) -> t -> ?defaultX:f -> ?defaultY:f -> uint -> uint -> uint

  val ( |&/ ) : t -> ?defaultX:f -> ?defaultY:f -> uint -> uint -> uint
  val ( |^/ ) : t -> ?defaultX:f -> ?defaultY:f -> uint -> uint -> uint
  val ( ||/ ) : t -> ?defaultX:f -> ?defaultY:f -> uint -> uint -> uint
  val ( |=/ ) : t -> ?defaultX:f -> ?defaultY:f -> uint -> uint -> uint
  val ( |*/ ) : t -> ?defaultX:f -> ?defaultY:f -> uint -> uint -> uint

  val (  =/ ) : t -> uint -> uint -> f
  val ( <>/ ) : t -> uint -> uint -> f

  val bitwise_choice : t -> f -> uint -> uint -> uint
  val zero_choice : t -> f -> uint -> uint

  val shift_right' : t -> uint -> uint -> uint
  val ( >>// ) : t -> uint -> uint -> uint

  val shift_left' : t -> uint -> uint -> uint
  val ( <<// ) : t -> uint -> uint -> uint

  val card : t -> f array -> uint
  val exp_card : t -> int -> f array -> uint
  val exp_card' : t -> int -> f array -> uint

  val scalar_binop_left  : (t->f->f->f) -> t -> f -> uint -> uint
  val scalar_binop_right : (t->f->f->f) -> t -> uint -> f -> uint

  val ( |.&/ ) : t -> f -> uint -> uint
  val ( |.^/ ) : t -> f -> uint -> uint
  val ( |.|/ ) : t -> f -> uint -> uint
  val ( |.=/ ) : t -> f -> uint -> uint
  val ( |.*/ ) : t -> f -> uint -> uint

  val ( |&./ ) : t -> uint -> f -> uint
  val ( |^./ ) : t -> uint -> f -> uint
  val ( ||./ ) : t -> uint -> f -> uint
  val ( |=./ ) : t -> uint -> f -> uint
  val ( |*./ ) : t -> uint -> f -> uint

  val lX : bool -> t -> uint -> uint -> f
  val lt : t -> uint -> uint -> f
  val ( </ ) : t -> uint -> uint -> f
  val le : t -> uint -> uint -> f
  val ( <=/ ) : t -> uint -> uint -> f

  val gX : bool -> t -> uint -> uint -> f
  val gt : t -> uint -> uint -> f
  val ge : t -> uint -> uint -> f

  val copy :  t -> int -> uint -> uint array
  val copy_t : t -> int -> uint -> uint array

  val input : t -> int -> uint

  val range : t -> int -> uint * f

  val truncate : t -> int -> uint -> uint

  val mult_length : int -> int -> int

  (* naive implementation of multiplication
    depth = 6 * n
    #gate = 6 * n^2
   *)
  val mult : t -> uint -> uint -> uint

  (* low depth implementation of multiplication
    depth = 3 * n + 3 * log n
    #gate = 6 * n^2
   *)
  val mult_3to2 : t -> uint -> uint -> uint
end

module Make(O:OOPS.MSig) : Sig
  with type O.t    = O.t
  and  type O.f    = O.f
  and  type O.F.f' = O.F.f'
=
struct
  module O = O
  open O
  module Oops = OOPS.Make(O)

  let fulladd2 t a b =
    let carry = (|!) t a b in
    let somme = (^!) t a b in
    (somme, carry)

  (*
    #gate = 6
    depth = 3
   *)
  let fulladd3 t a b c =
    let carry = (&!) t ((|!) t a ((&!) t b c)) ((|!) t b c) in
    let somme = (^!) t ((^!) t a b) c in
    (somme, carry)

  (*
    encoded with least significant bits first
    [|uint|] = \sum_i uint.array.(i) * 2^i
   *)
  type uint =
  {
(* arity of each [edge], usually arity <> Array.length array *)
    arity : int;
    array : f array
  }

  let length (u:uint) = Array.length u.array

  let uint_get t ?default i intX =
    if i < (Array.length intX.array)
    then intX.array.(i)
    else (match default with
      | Some default -> default
      | None -> (cst t false intX.arity)
    )

  let map f intX = {arity = intX.arity; array = Array.map f intX.array}

  let uint_of_bool (t:t) (f:f) : uint = {
    arity = arity t f;
    array = [|f|]
  }

  let uint_of_int t arity x =
    assert(x>=0);
    let bin = Tools.bin_of_int x in
    let array = Array.map (fun b -> cst t b arity) bin in
    {arity; array}

  let uint_one t arity = {arity; array = [|cst t true arity|]}
  let uint_zero t arity = {arity; array = [| |]}

  let add t ?carry (intX:uint) (intY:uint) : uint =
    let lenX = Array.length intX.array
    and lenY = Array.length intY.array in
    assert(intY.arity = intX.arity);
    let carry = ref (match carry with
      | None -> (cst t false intX.arity)
      | Some f -> assert(arity t f = intX.arity); f)
    in
    let arity = intX.arity in
    let len = max lenX lenY in
    {
      arity;
      array = Array.init (len+1) (fun i ->
        if i = len
        then (!carry)
        else
        (
          let c = !carry in
          let x = uint_get t i intX
          and y = uint_get t i intY in
          let xy, c = fulladd3 t x y c in
          carry := c;
          xy
        )
      )
    }

  let ( +/ ) t x y = add t x y

  let array_add t fa = Tools.tree_of_array (add t) fa
  let  list_add t fl = Tools.tree_of_list  (add t) fl

  let ( +?/ ) t uintX intY = (+/) t uintX (uint_of_int t uintX.arity intY)
  let ( ?+/ ) t intX uintY = (+/) t (uint_of_int t uintY.arity intX) uintY

  (* decreasing shift *)
  let shift_right t shift intX =
    let len = Array.length intX.array in
    if shift < len
    then
    (
      let array = Array.init (len-shift) (fun i -> intX.array.(i+shift)) in
      {arity = intX.arity; array}
    )
    else {arity = intX.arity; array = [||]}

  let ( >>/ ) t intX shift = shift_right t shift intX

  (* increasing shift *)
  let shift_left t shift intX =
    let len = Array.length intX.array
    and zero = cst t false intX.arity in
    let array = Array.init (len+shift) (fun i ->
      let i' = i - shift in
      if i' >= 0 then intX.array.(i') else zero) in
    {arity = intX.arity; array}

  let ( <</ ) t intX shift = shift_left t shift intX

  (* carry-free adder, constant depth
    add_3to2 x0 x1 x2 = (y0, y1) such that :
      - x0 + x1 + x2 = y0 + y1
    #gate = 6 * (max(length x0) (length x1) (length x2))
    depth = 3
   *)
  let add_3to2 t (x0:uint) (x1:uint) (x2:uint) : uint * uint =
    let arity = x0.arity in
    assert(x1.arity = arity);
    assert(x2.arity = arity);
    let l0 = length x0
    and l1 = length x1
    and l2 = length x2 in
    let len = max l0 (max l1 l2) in
    let a01 = Array.init len (fun i ->
      let x0i = uint_get t i x0
      and x1i = uint_get t i x1
      and x2i = uint_get t i x2 in
      (fulladd3 t x0i x1i x2i) (* (y0_i, y1_{i+1}) *)
    ) in
    let a0, a1 = MyArray.split a01 in
    let y0 = {arity; array = a0}
    and y1 = {arity; array = a1} in
    (y0, shift_left t 1 y1)

  let list_add_3to2 (t:O.t) ?(arity=None) (l:uint list) : uint =
    let rec aux0 t carry = function
      | x0::x1::x2::tail -> (
        let y0, y1 = add_3to2 t x0 x1 x2 in
        aux0 t (y1::y0::carry) tail
      )
      | tail ->
        (List.rev_append tail carry)
    in
    match MyList.get_onehash (fun f -> f.arity) l with
    | None -> (
      match arity with
      | None -> assert false
      | Some arity -> uint_zero t arity
    )
    | Some arity -> (
      let rec aux1 t = function
        | [] -> assert false
        | [x0] -> x0
        | [x0; x1] -> add t x0 x1
        | liste -> aux1 t (aux0 t [] liste)
      in aux1 t l
    )

  let array_add_3to2 t ?(arity=None) (a:uint array) : uint =
    list_add_3to2 t ~arity (Array.to_list a)

  let bitwise_binop (binop:t->f->f->f) (t:t) ?defaultX ?defaultY intX intY =
    let ari = intX.arity
    and lenX = Array.length intX.array
    and lenY = Array.length intY.array in
    let defaultX = match defaultX with Some edge -> edge | None -> (cst t false intX.arity)
    and defaultY = match defaultY with Some edge -> edge | None -> (cst t false intY.arity) in
    assert(ari = intY.arity);
    {
      arity = ari;
      array = Array.init (max lenX lenY) (fun i ->
        let x = uint_get t ~default:defaultX i intX
        and y = uint_get t ~default:defaultY i intY in
        binop t x y)
    }

  let ( |&/ ) = bitwise_binop ( &! )
  let ( |^/ ) = bitwise_binop ( ^! )
  let ( ||/ ) = bitwise_binop ( |! )
  let ( |=/ ) = bitwise_binop ( =! )
  let ( |*/ ) = bitwise_binop ( *! )

  let (  =/ ) t intX intY = Oops.array_and t ((|=/) t intX intY).array
  let ( <>/ ) t intX intY = Oops.neg t ((=/) t intX intY)

  let bitwise_choice t bX int0 int1 =
    bitwise_binop ((fun t -> Oops.ite t bX)) t int0 int1

  let zero_choice t bX intY =
    bitwise_choice t bX {arity = intY.arity; array = [||]} intY

  let shift_right' t shiftX intY =
    let result = ref intY in
    for i = 0 to (Array.length shiftX.array - 1)
    do
      result := bitwise_choice t shiftX.array.(i) !result (shift_right t (1 lsl i) !result);
    done;
    !result

  let ( >>// ) t intY shiftX = shift_right' t shiftX intY

  let shift_left' t shiftX intY =
    let result = ref intY in
    for i = 0 to (Array.length shiftX.array - 1)
    do
      result := bitwise_choice t shiftX.array.(i) !result (shift_left t (1 lsl i) !result);
    done;
    !result

  let ( <<// ) t intY shiftX = shift_left' t shiftX intY

  let card t bXarray = array_add t (Array.map (uint_of_bool t) bXarray)

  let exp_card t arity bXarray =
    let result = ref (uint_one t arity) in
    Array.iter (fun bX ->
      assert(O.arity t bX = arity);
      result := bitwise_choice t bX !result (shift_left t 1 !result);
    ) bXarray;
    !result

  let exp_card' t arity bXarray =
    (>>//) t (uint_one t arity) (card t bXarray)

  let scalar_binop_left binop t bitX intY =
    assert(arity t bitX = intY.arity);
    map (binop t bitX) intY

  let scalar_binop_right binop t intX bitY =
    assert(arity t bitY = intX.arity);
    map (fun bit -> binop t bit bitY) intX

  let ( |.&/ ) = scalar_binop_left ( &! )
  let ( |.^/ ) = scalar_binop_left ( ^! )
  let ( |.|/ ) = scalar_binop_left ( |! )
  let ( |.=/ ) = scalar_binop_left ( =! )
  let ( |.*/ ) = scalar_binop_left ( *! )

  let ( |&./ ) = scalar_binop_right ( &! )
  let ( |^./ ) = scalar_binop_right ( ^! )
  let ( ||./ ) = scalar_binop_right ( |! )
  let ( |=./ ) = scalar_binop_right ( =! )
  let ( |*./ ) = scalar_binop_right ( *! )

  let lX (default:bool) t (intX:uint) (intY:uint) =
    let ( <! ) x y = (&!) t (Oops.neg t x) y in
    let arity = intX.arity in
    assert(arity = intY.arity);
    let length = max (Array.length intX.array) (Array.length intY.array) in
    let carry = ref (cst t default arity) in
    for i = 0 to length - 1
    do
      let x = uint_get t i intX
      and y = uint_get t i intY in
      carry := Oops.ite t ((=!) t x y) (x <! y) !carry;
    done;
    !carry

  let lt = lX false
  let ( </ ) = lt

  let le = lX true
  let ( <=/ ) = le

  let gX default t intX intY = lX default t intY intX

  let gt = gX false
  let ( >/ ) = gt

  let ge = gX true
  let ( >=/ ) = ge

  let copy t ncopy myuint =
    let mat = Matrix.array2_of_matrix (Oops.array_copy_fun_array t ncopy myuint.array) in
    Array.map (fun array -> { arity = ncopy * myuint.arity; array}) mat

  let copy_t t ncopy myuint =
    let mat = Matrix.array2_of_matrix (Oops.array_copy_fun_array_t t ncopy myuint.array) in
    Array.map (fun array -> {arity = ncopy * myuint.arity; array}) mat

  let input t arity = {arity; array = Oops.array_make_n_var t arity}

  let range t n =
    let bin = Tools.bin_of_int (n-1) in
    let arity = Array.length bin in
    let array = Array.map (fun b -> cst t b arity) bin in
    let myuint = input t arity in
    myuint, ((<=/) t myuint {arity; array})

  (* assert(length(truncate t n x) <= n) *)
  let truncate t (n:int) (x:uint) : uint =
    if length x <= n then x else {
      arity = x.arity;
      array = Array.init n (fun i -> x.array.(i));
    }

  let mult_length lx ly =
    match lx, ly with
    | 0, _ | _, 0 -> 0
    | 1, z | z, 1 -> z
    | _, _ -> lx + ly

  (* naive implementation of multiplication
    depth = 6 * n
    #gate = 6 * n^2
   *)
  let mult t (x:uint) (y:uint) : uint =
    assert(x.arity = y.arity);
    let rec aux z i x y =
      if i < 0 then z
      else (
        let ix = uint_get t i x in
        let yi = ( |.&/ ) t ix y in
        let z' = add t z (shift_left t i yi) in
        aux z' (pred i) x y
      )
    in
    let z = aux (uint_zero t x.arity) (pred(length x)) x y in
    truncate t (mult_length (length x) (length y)) z

  (* low depth implementation of multiplication
    depth = 3 * n + 3 * log n
    #gate = 6 * n^2
   *)
  let mult_3to2 t (x:uint) (y:uint) : uint =
    assert(x.arity = y.arity);
    let y' = Array.init (length x) (fun i ->
      let ix = uint_get t i x in
      let yi = ( |.&/ ) t ix y in
      shift_left t i yi
    ) in
    let z = array_add_3to2 t ~arity:(Some x.arity) y' in
    truncate t (mult_length (length x) (length y)) z
end
