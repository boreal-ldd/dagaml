(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : Constraint System Types : Conjunctive Form
 *)

open GuaCaml
open Extra
open STools
open BTools

module CSTypes = Constraint_system_types
open CSTypes

module CSUtils = Constraint_system_utils
open CSUtils

module Types =
struct
  type input_type =
    | IT_cnf
    | IT_cir

  type input_system =
    | IC_cnf of CnfTypes.system
    | IC_cir of CirTypes.system

  type perf_type =
    | AQEOPS
    | Dummy
    | Synthesis

  type costred_type =
    | FRP
    | BPP

  type output_type =
    | OT_Pure
    | OT_Verilog

  type request = {
    req_input_target  : string;
    req_input_type    : input_type;
    req_perf          : perf_type;
    req_bucketize     : Tools.prio_t;
    req_costred          : costred_type;
    req_costred_verbose  : bool;
    req_output_target : string;
    req_output_type   : output_type;
  }

  let builder
      (req_input_target:string)
      ?(req_input_type = IT_cnf)
      ?(req_perf = AQEOPS)
      ?(req_bucketize=Tools.ASAP)
      ?(req_costred = BPP)
      ?(req_costred_verbose = false)
      (req_output_target:string)
      ?(req_output_type = OT_Verilog) () : request =
    {
      req_input_target;
      req_input_type;
      req_perf;
      req_bucketize;
      req_costred;
      req_costred_verbose;
      req_output_target;
      req_output_type;
    }

  let set_input_target  (r:request) (req_input_target:string) : request =
    {r with req_input_target}

  let set_input_type    (r:request) (req_input_type:input_type) : request =
    {r with req_input_type}

  let set_perf          (r:request) (req_perf:perf_type) : request =
    {r with req_perf}

  let set_bucketize     (r:request) (req_bucketize:Tools.prio_t) : request =
    {r with req_bucketize}

  let set_costred          (r:request) (req_costred:costred_type) : request =
    {r with req_costred}

  let set_costred_verbose  (r:request) (req_costred_verbose:bool) : request =
    {r with req_costred_verbose}

  let set_output_target (r:request) (req_output_target:string) : request =
    {r with req_output_target}

  let set_output_type   (r:request) (req_output_type:output_type) : request =
    {r with req_output_type}

  module ToS =
  struct
    open STools
    open ToS

    let input_type = function
      | IT_cnf -> "IT_cnf"
      | IT_cir -> "IT_cir"

    let perf_type = function
      | AQEOPS    -> "AQEOPS"
      | Dummy     -> "Dummy"
      | Synthesis -> "Synthesis"

    let costred_type = function
      | FRP    -> "FRP"
      | BPP    -> "BPP"

    let output_type = function
      | OT_Pure -> "OT_Pure"
      | OT_Verilog -> "OT_Verilog"

    let request request =
      "{ req_input_target =" ^(string request.req_input_target)^
      "; req_input ="        ^(input_type request.req_input_type)^
      "; req_perf ="         ^(perf_type request.req_perf)^
      "; req_bucketize ="    ^(Tools.string_of_prio request.req_bucketize)^
      "; req_costred ="         ^(costred_type request.req_costred)^
      "; req_costred_verbose =" ^(bool request.req_costred_verbose)^
      "; req_output_target ="^(string request.req_output_target)^
      "; req_output ="       ^(output_type request.req_output_type)^" }"

    let request_pretty request =
      "{\n\treq_input_target =" ^(string request.req_input_target)^
      ";\n\treq_input ="        ^(input_type request.req_input_type)^
      ";\n\treq_perf ="         ^(perf_type request.req_perf)^
      ";\n\treq_bucketize ="    ^(Tools.string_of_prio request.req_bucketize)^
      ";\n\treq_costred ="         ^(costred_type request.req_costred)^
      ";\n\treq_costred_verbose =" ^(bool request.req_costred_verbose)^
      ";\n\treq_output_target ="^(string request.req_output_target)^
      ";\n\treq_output ="       ^(output_type request.req_output_type)^"\n}"
  end
end

module SNAX = SnaxOops.OOPS.Model

(* [TODO?] add to AQEOPS ? *)
module type MSig =
sig
  module Q : AQEOPS.MSig

  val translate_pure_to_snax : Q.t -> SNAX.t -> Q.f list -> SNAX.f list

  val default_module_name : string
end

module type Sig =
sig
  val compute : Types.request -> unit
end

module Core =
struct
  open Types

  let load_cnf =
    if false
    then StrLoadCnf.load_file
    else StrLoadCnf.load_qbf_file ~discardQ:true

  let load_cir =
    StrLoadCir.load_file

  let input_target_to_input_system (input_target:string) (input_type:input_type) : input_system =
    match input_type with
    | IT_cnf -> IC_cnf(load_cnf input_target)
    | IT_cir -> IC_cir(load_cir input_target)

  let wap_solver = Snowflake.Wap_lightspeed.lightspeed_greedy

  (* [FIXME] *)
  (* the output format is not very appropriate and hard to read for sequence generation *)
  let wap_solver_ft (h:GGLA_FT.Type.hg) : BNat.nat * (int list list) =
    Snowflake.Wap_lightspeed.lightspeed_lopt (GGLA_HFT.of_FT h)
end

module Make(M:MSig) : Sig =
struct
  open Types
  open M

  type system = (Q.t, Q.f) CSTypes.system

  let eval_clause (t:Q.t) (n:int) (cl:CnfTypes.clause) : Q.f =
    Q.(|!!) t n (cl ||> (fun (k, b) -> Q.var t b n k))

  let eval_expr_param (t:Q.t) (n:int) : (int, Q.f) ExprUtils.reduce_expr = ExprUtils.{
    rcst = (fun b -> Q.cst t b n);
    rvar = (fun k -> Q.var t false n k);
    ruop = (fun uop e -> Q.cneg t (uop = Expr.PNot) e);
    rbop = (fun bop e1 e2 ->
      match bop with
      | Expr.PAnd -> Q.( &! ) t e1 e2
      | Expr.POr  -> Q.( |! ) t e1 e2
      | Expr.PIff -> Q.( =! ) t e1 e2
      | Expr.PImp -> Q.( |! ) t (Q.cneg t true e1) e2
      | Expr.PXor -> Q.( ^! ) t e1 e2
    )
  }

  let eval_expr (t:Q.t) (n:int) =
    let param = eval_expr_param t n in
    (fun (e:int Expr.expr) : Q.f -> ExprUtils.reduce_expr param e)

  let input_system_to_input_bdd_system (input_system:input_system) (verbose:bool) : system =
    match input_system with
    | IC_cnf sys -> (
      let t = Q.newman() in
      let n = sys.CSTypes.input in
      CSUtils.map_system ~verbose (fun () -> t) (eval_clause t n) sys
    )
    | IC_cir sys -> (
      let t = Q.newman() in
      let n = sys.CSTypes.input in
      CSUtils.map_system ~verbose (fun () -> t) (eval_expr   t n) sys
    )

  module CoSTreD = AQEOPS_CoSTreD.Make(Q)

  let input_bdd_system_to_output_bdd_system
      (sys:system)
      (verbose:bool)
      (costred_type:costred_type) : system =
    let frp_only : bool = (costred_type = FRP) in
    let costred = CoSTreD.compute ~verbose ~frp_only Core.wap_solver sys in
    let costred =
      match costred_type with
      | BPP -> costred.CoSTreD.out_bpp
      | FRP -> costred.CoSTreD.out_mid
    in
    {
      name    = sys.name;
      input   = sys.input;
      quants  = sys.quants;
      man     = sys.man;
      formule = Array.to_list costred;
    }

  let conj_to_snax (pure:Q.t) (ninputs:int) (pure_fa:Q.f array) : Snax.ssystem =
    let pure_fl = Array.to_list pure_fa in
    let snax = SNAX.newman() in
    let snax_fl = M.translate_pure_to_snax pure snax pure_fl in
    let snax_conj =
      SnaxOops.OOPS.list_and
         snax
        ~arity:(Some ninputs)
         snax_fl
    in
    IoUtils.make ("CoSTreD_Bpp_"^M.default_module_name) snax ninputs
      [| snax_conj.SnaxOops.OOPS.Model.edge |]

  module CORE = OOPS.Make(Q)

  let system_to_verilog (sys:string CORE.system) (output_file:string) : unit =
    let n = Array.length sys.IoTypes.inps in
    let ssnax = conj_to_snax sys.man n (Array.map snd sys.sset) in
    let sexpr = IoUtils.expr_of_snax ssnax IoUtils.string_fnn in
    StrDumpVerilog.dump_file output_file sexpr;
    ()

  let output_bdd_system_to_output_file
      (sys:system)
      (output_type:output_type)
      (output_file:string) : unit =
    let sys : string CORE.system =
      IoUtils.make
        ("CSCoSTreD_"^M.default_module_name)
        sys.man
        sys.input
        (Array.of_list sys.formule)
    in
    match output_type with
    | OT_Pure    -> CORE.system_tof ~nocopy:true sys output_file
    | OT_Verilog -> system_to_verilog sys output_file

  let compute (request:request) : unit =
    print_endline "[Constraint_system_costred.compute] input_target -> input_system";
    let input_system =
      Core.input_target_to_input_system
        request.req_input_target
        request.req_input_type
    in
    print_endline "[Constraint_system_costred.compute] input_system -> input_bdd_system";
    let input_bdd_system =
      input_system_to_input_bdd_system
        input_system
        request.req_costred_verbose
    in
    print_endline "[Constraint_system_costred.compute] input_bdd_system -> output_bdd_system";
    let output_bdd_system =
      input_bdd_system_to_output_bdd_system
        input_bdd_system
        request.req_costred_verbose
        request.req_costred
    in
    print_endline "[Constraint_system_costred.compute] output_bdd_system -> FILE ";
    output_bdd_system_to_output_file
      output_bdd_system
      request.req_output_type
      request.req_output_target;
    print_endline "[Constraint_system_costred.compute] DONE";
    ()
end

module PrimalGraph =
struct
  module Types =
  struct
    include Types

    type output_type =
      | OT_stree
      | OT_ocaml
      | OT_graphviz

    type request = {
      req_input_type    : Types.input_type;
      req_input_target  : string;
      req_compute_wap   : bool;
      req_output_type   : output_type;
      req_output_target : string;
    }

    let builder
        ?(req_input_type=Types.IT_cnf)
         (req_input_target:string)
        ?(req_compute_wap=false)
        ?(req_output_type=OT_graphviz)
         (req_output_target:string) : request =
      {
        req_input_type;
        req_input_target;
        req_compute_wap;
        req_output_type;
        req_output_target;
      }

    let set_input_type (r:request) (req_input_type:Types.input_type) : request =
      {r with req_input_type}

    let set_input_target (r:request) (req_input_target:string) : request =
      {r with req_input_target}

    let set_compute_wap (r:request) (req_compute_wap:bool) : request =
      {r with req_compute_wap}

    let set_output_type (r:request) (req_output_type:output_type) : request =
      {r with req_output_type}

    let set_output_target (r:request) (req_output_target:string) : request =
      {r with req_output_target}

    module ToS =
    struct
      open STools
      open ToS

      let output_type =
        function
        | OT_stree    -> "OT_stree"
        | OT_ocaml    -> "OT_ocaml"
        | OT_graphviz -> "OT_graphviz"

      let request (r:request) : string =
        "{ req_input_type ="^(Types.ToS.input_type r.req_input_type)^
        "; req_input_target ="^(string r.req_input_target)^
        "; req_compute_wap ="^(bool r.req_compute_wap)^
        "; req_output_type ="^(output_type r.req_output_type)^
        "; req_output_target ="^(string r.req_output_target)^" }"

      let request_pretty (r:request) : string =
        "{\n\treq_input_type ="^(Types.ToS.input_type r.req_input_type)^
        ";\n\treq_input_target ="^(string r.req_input_target)^
        ";\n\treq_compute_wap ="^(bool r.req_compute_wap)^
        ";\n\treq_output_type ="^(output_type r.req_output_type)^
        ";\n\treq_output_target ="^(string r.req_output_target)^"\n}"
    end
  end

  type wap_input = Snowflake.Wap_exchange.input

  let input_system_to_wap_input (sys:Types.input_system) : wap_input =
    match sys with
    | Types.IC_cnf sys -> (CnfUtils.Wap.input_of_cnf sys)
    | Types.IC_cir sys -> (CirUtils.Wap.input_of_cir sys)

  let compute (request:Types.request) : unit =
    let open Types in
    let input_system =
      Core.input_target_to_input_system
        request.req_input_target
        request.req_input_type
    in
    let wap_input =
      input_system_to_wap_input
        input_system
    in
    if request.req_compute_wap
    then (
      let wap_output = Core.wap_solver wap_input in
      match request.req_output_type with
      | Types.OT_ocaml -> (
        let string = Snowflake.Wap_exchange_utils.ToShiftS.output wap_output (Some 0) in
        STools.SUtils.output_string_to_file request.req_output_target string
      )
      | Types.OT_stree -> (
        let stree = Snowflake.Wap_exchange_utils.ToSTree.output wap_output in
        STools.OfSTree.file [stree] request.req_output_target
      )
      | _ -> failwith "[Constraint_system_costred.PrimalGraph.compute] non implemented combination"
    )
    else (
      match request.req_output_type with
      | Types.OT_ocaml -> (
        let string = Snowflake.Wap_exchange_utils.ToShiftS.input wap_input (Some 0) in
        STools.SUtils.output_string_to_file request.req_output_target string
      )
      | Types.OT_stree -> (
        let stree = Snowflake.Wap_exchange_utils.ToSTree.input wap_input in
        STools.OfSTree.file [stree] request.req_output_target
      )
      | Types.OT_graphviz -> (
        let profile_name = "[Constraint_system_costred.PrimalGraph.compute] wap:false output:graphviz" in
        OProfile.(uprofile default) profile_name
          (fun () ->
            Snowflake.Wap_exchange_utils.to_graphviz_file
              ~verbose:2
              ~check:false
              ~astree:false
              ~name:"PrimalGraph"
               request.req_output_target
               wap_input
          )
      )
    )
end

(* input = circuit *)
(* intermediary = GuaCaml.GGLA_HFT *)
(* output = OCaml | STree | Graphviz *)
module RefinedPrimalGraph =
struct
  module Types =
  struct
    include Types

    type output_model =
      | OM_FT
      | OM_HFT
      | OM_CFT

    type output_format =
      | OF_stree
      | OF_ocaml
      | OF_graphviz

    type request = {
      req_input_target  : string;
      req_vertices_of_interest : int list option;
      req_compute_wap   : bool;
      req_output_model  : output_model;
      req_output_format : output_format;
      req_output_target : string;
    }

    let builder
         (req_input_target:string)
        ?(req_vertices_of_interest=None)
        ?(req_compute_wap=false)
        ?(req_output_model=OM_FT)
        ?(req_output_format=OF_graphviz)
         (req_output_target:string) : request =
      {
        req_input_target;
        req_vertices_of_interest;
        req_compute_wap;
        req_output_model;
        req_output_format;
        req_output_target;
      }

    let set_input_target (r:request) (req_input_target:string) : request =
      {r with req_input_target}

    let set_vertices_of_interest (r:request) (req_vertices_of_interest:int list option) : request =
      {r with req_vertices_of_interest}

    let set_compute_wap (r:request) (req_compute_wap:bool) : request =
      {r with req_compute_wap}

    let set_output_model (r:request) (req_output_model:output_model) : request =
      {r with req_output_model}

    let set_output_format (r:request) (req_output_format:output_format) : request =
      {r with req_output_format}

    let set_output_target (r:request) (req_output_target:string) : request =
      {r with req_output_target}

    module ToS =
    struct
      open STools
      open ToS

      let output_model =
        function
        | OM_FT  -> "OM_FT"
        | OM_HFT -> "OM_HFT"
        | OM_CFT -> "OM_CFT"

      let output_format =
        function
        | OF_stree    -> "OF_stree"
        | OF_ocaml    -> "OF_ocaml"
        | OF_graphviz -> "OF_graphviz"

      let request (r:request) : string =
        SUtils.record [
          ("req_input_target", string r.req_input_target);
          ("req_vertices_of_interest", option(list int) r.req_vertices_of_interest);
          ("req_compute_wap", bool r.req_compute_wap);
          ("req_output_model", output_model r.req_output_model);
          ("req_output_format", output_format r.req_output_format);
          ("req_output_target", string r.req_output_target);
        ]

      let request_pretty (r:request) : string =
        "{\n\treq_input_target ="^(string r.req_input_target)^
        ";\n\treq_vertices_of_interest ="^(option(list int) r.req_vertices_of_interest)^
        ";\n\treq_compute_wap ="^(bool r.req_compute_wap)^
        ";\n\treq_output_model ="^(output_model r.req_output_model)^
        ";\n\treq_output_format ="^(output_format r.req_output_format)^
        ";\n\treq_output_target ="^(string r.req_output_target)^"\n}"
    end
  end

  type wap_input = Snowflake.Wap_exchange.input

  let output_primal_graph
      (output_model:Types.output_model)
      (output_format:Types.output_format)
      (input:GGLA_FT.Type.hg)
      (target:string) : unit =
    match output_model with
    | Types.OM_FT  -> (
      match output_format with
      | Types.OF_stree ->
          failwith "[Constraint_system_costred.RefinedPrimalGraph.compute] non implemented combination"
      | Types.OF_ocaml -> (
        let string = GGLA_FT.ToShiftS.hg ~inline:true input (Some 0) in
        STools.SUtils.output_string_to_file target string
      )
      | Types.OF_graphviz -> (
          failwith "[Constraint_system_costred.RefinedPrimalGraph.compute] non implemented combination"
(*
        GGLA_FT.to_graphviz_file
          ~astree:false
          ~name:"RefinedPrimalGraph"
            target
            pg_cft
 *)
      )
    )
    | Types.OM_HFT -> (
      let input = GGLA_HFT.of_FT ~remove_useless:true input in
      match output_format with
      | Types.OF_stree ->
          failwith "[Constraint_system_costred.RefinedPrimalGraph.compute] non implemented combination"
      | Types.OF_ocaml -> (
        let string = GGLA_HFT.ToShiftS.hg ~inline:true input (Some 0) in
        STools.SUtils.output_string_to_file target string
      )
      | Types.OF_graphviz -> (
        GGLA_HFT.to_graphviz_file
          ~astree:false
          ~name:"RefinedPrimalGraph"
            target
            input
      )
    )
    | Types.OM_CFT -> (
      let input = GGLA_CFT.of_FT ~remove_useless:true input in
      match output_format with
      | Types.OF_stree ->
          failwith "[Constraint_system_costred.RefinedPrimalGraph.compute] non implemented combination"
      | Types.OF_ocaml -> (
        let string = GGLA_CFT.ToShiftS.hg ~inline:true input (Some 0) in
        STools.SUtils.output_string_to_file target string
      )
      | Types.OF_graphviz -> (
        GGLA_CFT.to_graphviz_file
          ~astree:false
          ~name:"RefinedPrimalGraph"
            target
            input
      )
    )

  let compute (request:Types.request) : unit =
    let open Types in
    let cir = Core.load_cir request.req_input_target in
    (match request.req_vertices_of_interest with
      | None -> ()
      | Some vl -> (
        CirUtils.Utils.linear_support_interest vl cir
        |> List.iteri (fun i j -> print_endline STools.ToS.("cir.["^(int i)^"] lsi:"^(int j)))
      )
   );
    (* [FIXME] add parameter 'verbose' *)
    let pg_ft  = CirUtils.Wap.refined_primal_graph ~verbose:true cir in
    if request.req_compute_wap
    then (
      let (cost, elims) = Core.wap_solver_ft pg_ft in
      match request.req_output_format with
      | Types.OF_ocaml -> (
        let cost = BNat.to_bicimal cost in
        let string = STools.ToShiftS.(of_ToS STools.ToS.(list int) * (list(list int))) (cost, elims) (Some 0) in
        STools.SUtils.output_string_to_file request.req_output_target string
      )
      | _ -> failwith "[Constraint_system_costred.RefinedPrimalGraph.compute] non implemented combination"
    )
    else (
      output_primal_graph
        request.req_output_model
        request.req_output_format
        pg_ft
        request.req_output_target
    )
end
