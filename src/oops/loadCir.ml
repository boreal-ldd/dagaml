(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : LoadCir : Uses OOPS interface to interpret a Cir formula
 *)

open GuaCaml
open Extra
open CnfTypes
open StrLoadCir

(* straight forward conversion of a CNF as a propositional expression *)
module Make0(Model:OOPS.MSig) =
struct
  module G0 = OOPS.Make(Model)

  let compute_clause
      (t:Model.t)
      (nvar:int)
      (bivars:(Model.f * Model.f) array)
      (cl:clause) : Model.f =
    let eval_term (i, b) =
      let v, nv = bivars.(i) in
      if b then nv else v
    in
    cl
    ||> eval_term
    |> G0.list_or t ~arity:(Some nvar)

  let export_cnf (man:Model.t) (cnf:file) : Model.f =
    let bivars = G0.array_make_n_bivar man cnf.input in
    let fl =
      cnf.formule
      ||> (compute_clause man cnf.input bivars)
    in
    G0.list_and man ~arity:(Some cnf.input) fl

end

(* Similar to Make but:
  - Use CnfUtils.treefy to preprocess the formula into a tree
  - Use absolute variable naming
 *)
module MakeA(Model:OOPS.MSig) =
struct
  open Model
  module G0 = OOPS.Make(Model)
  open G0

  let rec export_tree
      (man:Model.t)
      (arity:int)
      (bivars:(Model.f * Model.f) array) : CnfUtils.tree -> Model.f =
    let eval_term (i, b) =
      let v, nv = bivars.(i) in
      if b then nv else v
    in
    function
    | CnfUtils.Const cst -> Model.cst man cst arity
    | CnfUtils.Units (units, tree) -> (
      List.fold_left
        (fun f ib -> Model.(&!) man f (eval_term ib))
        (export_tree man arity bivars tree) units
    )
    | CnfUtils.OrUnits (units, tree) -> (
      List.fold_left
        (fun f ib -> Model.(|!) man f (eval_term ib))
        (export_tree man arity bivars tree) units
    )
    | CnfUtils.Sha3 (var, tX, t0, t1) -> (
      Model.(&!) man
        ((export_tree man arity bivars tX):Model.f)
        (biite man bivars.(var)
          (export_tree man arity bivars t0)
          (export_tree man arity bivars t1))
    )
    | CnfUtils.DSAnd tl -> (
      G0.list_and man ~arity:(Some arity)
        (tl ||> export_tree man arity bivars)
    )

  let export_formule (man:Model.t) ?(simpl=3) (bivars:(Model.f * Model.f) array) (order:int list) (arity:int) formule : Model.f =
    let tree = match simpl with
      | _ when simpl < 0 -> failwith "simpl level is negative"
      | 0
      | 1 -> (CnfUtils.treefy_shannon_nosimpl order       formule)
      | 2 -> (CnfUtils.treefy_shannon         order arity formule)
      | 3 -> (CnfUtils.treefy_shannon_cc      order arity formule)
      | _ -> failwith "this simpl level has not been implemented"
    in
    (* CnfUtils.pprint tree; *)
    let tree =
      if simpl >= 1
      then (
        (* print_endline "[LoadCnf.export_formule] apply:tree_simplify_cost"; *)
        let tree = CnfUtils.tree_simplify_const tree in
        (* CnfUtils.pprint tree; *)
        tree
      )
      else tree
    in
    export_tree man arity bivars tree

  let export_cnf (man:Model.t) ?(simpl=3) (cnf:file) :  Model.f =
    let inputs = List.init cnf.input (fun i -> i) in
    let bivars = array_make_n_bivar man cnf.input in
    export_formule man ~simpl bivars inputs cnf.input cnf.formule

    (* [compute_treefy_bucket ~mode t sa vars cnf]
        perform bucketization of formule, then treefy/compile each bucket
     *)
  let compute_treefy_bucket
     ?(mode=Tools.ASAP)
      (t:Model.t)
      (sa:int list array) (* support array *)
      (bivars:(Model.f * Model.f) array)
      (cnf:file) : Model.f list * formule =
    let arity = cnf.input in
    assert(Array.length bivars = arity);
    let fa, f' = CnfUtils.bucketize_formule ~mode sa cnf.formule in
    let fa' = Array.map2 (fun supp formule ->
      export_formule t bivars supp arity formule
    ) sa fa in
    (Array.to_list fa', f')
end

(* Similar to Make but:
  - Uses CnfUtils.treefy to preprocess the formula into a tree
  - Uses absolute variable naming
  - Allows Quantifiers
 *)
module MakeQ(Q:QEOPS.MSig) =
struct
  include MakeA(Q)

  let export_qbf (t:Q.t) ?(simpl=3) (cnf:file) : Q.f =
    let rec loop (ga:int) (q:quants) (f:Q.f) : Q.f =
      match q with
      | [] -> f
      | (qt, il)::q' -> (
        assert(SetList.sorted_nat il);
        let supp = Support.of_sorted_support ga il in
        let f' = if qt
          then Q.existsA t supp f
          else Q.forallA t supp f
        in
        loop ga q' f'
      )
    in
    let f = export_cnf t ~simpl cnf in
    loop cnf.input (List.rev cnf.quants) f
end
