(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : OfExprOfOops : Conversion Functor from Expression
 *   parametered by an OOPS interface
 *)

open GuaCaml
open Extra
open STools
open BTools
open IoTypes
open IoUtils

module Make(OS:OOPS.MSig) =
struct
  module GO = OOPS.Make(OS)

  let import
      (sys : 's expr)
      (man:OS.t) : 's GO.system =
    import_expr
      (OS.cst man)
      (fun n -> GO.array_make_n_var man n)
      (OS.(&!) man)
      (OS.(^!) man)
      (OS.cneg man true) sys man

  open BTools

  type state =
    | Input of int
    | Expr of int Expr.expr
    | Term of OS.f
    | FTerm
    | Erased

  type block = {
    mutable state : state;
    (* count the number of references *)
    mutable noutput : int;
    (* [Some(file_name, size_bytes)] iff this function has already been archived, [None] otherwise *)
    mutable fterm : OS.F.f' option;
    is_input : bool;
    is_output : bool;
  }

  let force_archive = false
  let fterm_memory_limit = ref (if force_archive then 1_000_000 else 5_000_000_000)
  (* let archdrive_memory_limit = ref (if force_archive then 10_000_000 else 10_000_000_000) *)

  (* in french, colloquial word for surgery table *)
  type billard = {
            ninputs        : int;
    mutable man            : OS.t;
            blocks         : block array;
            time0          : float;
      (* cumulated size (in bytes) of f-termed functions *)
    mutable top_ftermed    : int;
    mutable ftermed        : int;
  }

  type 's billard_system = ('s, billard, int) IoTypes.system

  let billard_stats billard : string =
    let stat = Gc.quick_stat () in
    let heap = stat.Gc.heap_words * 8 in
    let top_heap = stat.Gc.top_heap_words * 8 in
     ("{ time="^(ToS.float(Sys.time() -. billard.time0))^
       " heap="^(ToS.pretty_int heap)^
       " {max="^(ToS.pretty_int top_heap)^"}"^
    " ftermed="^(ToS.pretty_int billard.ftermed)^
       " {max="^(ToS.pretty_int billard.top_ftermed)^"} }")

  let billard_create size ninputs noutput_a is_input_a is_output_a sset time0 : billard =
    let index = ref 0 in
    let blocks = Array.make size None in
    Tools.ntimes ninputs (fun() ->
      let index = !++ index in
      assert(is_input_a.(index));
      blocks.(index) <- Some {
        state = Input index;
        noutput = noutput_a.(index);
        fterm = None;
        is_input = true;
        is_output = is_output_a.(index);
      });
    Array.iter (fun (i, e) ->
      let index = !++ index in
      blocks.(index) <- Some {
        state = Expr e;
        noutput = noutput_a.(index);
        fterm = None;
        is_input = is_input_a.(index);
        is_output = is_output_a.(index);
      }) sset;
    {ninputs; man = OS.newman(); blocks = MyArray.map_unop blocks; ftermed = 0; top_ftermed = 0; time0}

  let billard_create size ninputs noutput_a is_input_a is_output_a sset time0 : billard =
    try billard_create size ninputs noutput_a is_input_a is_output_a sset time0
    with exn -> raise (OUnit.cascade_exn "IoUtils.billard_create" exn)

  let billard_clear billard : unit =
    Array.iter (fun block ->
      match block.fterm with
      | Some f' -> (OS.F.free f'; block.fterm <- None)
      | None -> ()) billard.blocks

  let billard_clear billard : unit =
    try billard_clear billard
    with exn -> raise (OUnit.cascade_exn "IoUtils.billard_clear" exn)

  let billard_get_var_intern billard i =
      let stop = OProfile.(time_start default "billard_get_var_intern") in
      (* print_endline ("[get_var] i: "^(ToS.pretty_int i)); *)
      let block = billard.blocks.(i) in
      (* print_endline ("[get_var] block.(i).noutput: "^(ToS.pretty_int block.noutput)); *)
      (* print_endline ("[get_var] block.(i).is_output: "^(ToS.bool block.is_output)); *)
      assert(block.noutput > 0 || block.is_output);
      let f =
        match block.state with
        | Input x -> (
          assert(0<=x && x < billard.ninputs);
          OS.var billard.man false billard.ninputs x
        )
        | Expr _ -> assert false
        | Term f -> f
        | FTerm -> (
          let f' = Tools.unop block.fterm in
          let f = OS.F.to_f billard.man f' in
          (* assert(OS.check billard.man f); (* [DEBUG] *) *)
          block.state <- Term f;
          f
        )
        | Erased -> (
          assert(block.noutput <= 0);
          assert false
        )
      in
      block.noutput <- pred block.noutput;
      if block.noutput <= 0 && not block.is_output
      then (
        block.state <- Erased;
        match block.fterm with
        | Some f' -> (
          let size_bytes = OS.F.size f' in
          OS.F.free f';
          block.fterm <- None;
          billard.ftermed <- billard.ftermed - size_bytes;
        )
        | None -> ()
      );
      stop();
      f

  (* (* [DEBUG] *)
  let billard_get_var_intern billard i =
    try billard_get_var_intern billard i
    with exn -> raise (OUnit.cascade_exn "IoUtils.billard_get_var_intern" exn)
   *)

  let billard_get_var_extern billard (dman:OS.t) i : OS.f =
      let stop = OProfile.(time_start default "billard_get_var_extern") in
      assert(billard.man != dman);
      (* print_endline ("[get_var] i: "^(ToS.pretty_int i)); *)
      let block = billard.blocks.(i) in
      (* print_endline ("[get_var] block.(i).noutput: "^(ToS.pretty_int block.noutput)); *)
      (* print_endline ("[get_var] block.(i).is_output: "^(ToS.bool block.is_output)); *)
      assert(block.noutput > 0 || block.is_output);
      let f = match block.state with
        | Input x -> (
          assert(0<=x && x < billard.ninputs);
          OS.var dman false billard.ninputs x
        )
        | Expr _ -> assert false
        | Term f -> (OS.copy_into billard.man [f] dman |> List.hd)
        | FTerm -> (OS.F.to_f dman (Tools.unop block.fterm))
        | Erased -> (
          assert(block.noutput <= 0);
          assert false
        )
      in
      stop();
      f

  let billard_get_var_extern billard dman i =
    try billard_get_var_extern billard dman i
    with exn -> raise (OUnit.cascade_exn "IoUtils.billard_get_var_extern" exn)

  let billard_eval_expr billard e : OS.f =
    let rec eval_expr = function
      | Expr.PCst b -> OS.cst billard.man b billard.ninputs
      | Expr.PVar v -> billard_get_var_intern billard v
      | Expr.PUop(uop, e) -> (
        let f = eval_expr e in
        match uop with
        | Expr.PNop ->     f
        | Expr.PNot -> OS.cneg billard.man true f
      )
      | Expr.PBop (bop, e0, e1) -> (
        let f0 = eval_expr e0
        and f1 = eval_expr e1 in
        match bop with
        | Expr.PAnd  -> OS.(&!) billard.man f0 f1
        | Expr.POr   -> OS.(|!) billard.man f0 f1
        | Expr.PImp  -> OS.(|!) billard.man (OS.cneg billard.man true f0) f1
        | Expr.PIff  -> OS.(=!) billard.man f0 f1
        | Expr.PXor  -> OS.(^!) billard.man f0 f1
      )
    in eval_expr e

  let billard_cleaning_process ?(normalize=false) billard (maxi:int) : unit =
    let stop = OProfile.(time_start default) "[billard_cleaning_process]" in
    print_endline ("[billard_cleaning_process] ante: "^(ToS.pretty_int maxi)^" "^(billard_stats billard));
    let stack = ref [] in
    (* fill [stack] with alive functions *)
    for i = billard.ninputs to maxi
    do
      let block = billard.blocks.(i) in
      match block.state with
      | Term f -> stack_push stack f
      | _ -> ()
    done;
    stack := OS.keep_clean ~normalize billard.man (List.rev !stack);
    for i = billard.ninputs to maxi
    do
      let block = billard.blocks.(i) in
      match block.state with
      | Term _ -> block.state <- Term(stack_pull stack)
      | _ -> ()
    done;
    assert(!stack = []);
    Gc.compact(); (* major-collection-cycle + heap-compaction *)
    print_endline ("[billard_cleaning_process] post: "^(ToS.pretty_int maxi)^" "^(billard_stats billard));
    stop();
    ()

  let billard_check billard : bool =
    Array.for_all (fun block ->
      match block.state with
      | Term f -> OS.check billard.man f
      | _ -> true
    ) billard.blocks

  let billard_fterm_process billard (maxi:int) : unit =
    let stop = OProfile.(time_start default) "[billard_fterm_process]" in
    OS.clear_caches billard.man;
    print_newline();
    print_endline ("[billard_fterm_process] proto: "^(ToS.pretty_int maxi)^" "^(billard_stats billard));
    (* first, we archdrive all archived elements *)
    let count = ref 0 in
    for i = billard.ninputs to maxi
    do
      let block = billard.blocks.(i) in
      match block.fterm with
      | Some _ -> (incr count; block.state <- FTerm)
      | None -> ()
    done;
    Gc.compact(); (* major-collection-cycle + heap-compaction *)
    (* [DEBUG]
    if !count > 0
    then (
      Gc.compact(); (* major-collection-cycle + heap-compaction *)
      billard_cleaning_process billard maxi
    ); *)
    print_endline ("[billard_fterm_process] ante: "^(ToS.pretty_int maxi)^" "^(billard_stats billard));
    (* then, we archdrive remaining elements *)
    for i = billard.ninputs to maxi
    do
      let block = billard.blocks.(i) in
      match block.state with
      | Term f -> (
        assert(OS.check billard.man f);
        let f' = OS.F.of_f ~normalize:false billard.man f in
        block.fterm <- Some f';
        let size_bytes = OS.F.size f' in
        billard.ftermed <- billard.ftermed + size_bytes;
        billard.top_ftermed <- max billard.top_ftermed billard.ftermed;
        block.state <- FTerm;
      )
      | _ -> ()
    done;
    billard.man <- OS.newman();
    Gc.compact(); (* major-collection-cycle + heap-compaction *)
    print_endline ("[billard_fterm_process] post: "^(ToS.pretty_int maxi)^" "^(billard_stats billard));
    stop();
    ()

  (* [TODO] add sanity check *)
  let billard_compute (sys:'s expr) : 's billard_system =
    let time0 = Sys.time() in
    let sys', names_a, ninput_a, noutput_a, is_input_a, is_output_a =
      normalize_system sys
    in
    let ninputs = Array.(length sys'.inps + length sys'.regs) in
    let size = Array.length names_a in
    assert(size = Array.length ninput_a);
    assert(size = Array.length noutput_a);
    assert(size = Array.length is_input_a);
    assert(size = Array.length is_output_a);
    let billard = billard_create size ninputs noutput_a is_input_a is_output_a sys'.sset time0 in
    let over = ToS.pretty_int(Array.length billard.blocks-1) in
    let too_much_memory billard (step:int) : unit =
      let stop = OProfile.(time_start default) "too_much_memory" in
      let stat = Gc.quick_stat () in
      let heap = stat.Gc.heap_words * 8 in
      (   if heap >= !fterm_memory_limit
        then billard_cleaning_process billard step);
      let stat = Gc.quick_stat () in
      let heap = stat.Gc.heap_words * 8 in
      (   if heap >= !fterm_memory_limit
        then billard_fterm_process billard step   );
      stop();
    in
    let get_expr = function
      | Expr e -> e
      | _ -> assert false
    in
    (* assert(billard_check billard); (* [DEBUG] *) *)
    for i = billard.ninputs to Array.length billard.blocks -1
    do
      print_endline ("[billard_compute] i: "^(ToS.pretty_int i)^" / "^over^" "^(billard_stats billard));
      let block = billard.blocks.(i) in
      let e = get_expr block.state in
      let f = billard_eval_expr billard e in
      block.state <- Term f;
      (* assert(billard_check billard); (* [DEBUG] *) *)
      (* (* [TEST] *) billard_cleaning_process billard i; *)
      too_much_memory billard i;
      (* assert(billard_check billard); (* [DEBUG] *) *)
    done;
    let sset = Array.map (fun i -> names_a.(i), i) (Array.append sys'.outs sys'.regs) in
    let name i = names_a.(i) in
    {
      name = sys.name;
      man = billard;
      inps = Array.map name sys'.inps;
      regs = Array.map name sys'.regs;
      wire = [||];
      outs = Array.map name sys'.outs;
      sset;
    }

  let compute_smart ?(man0:OS.t option) (sys: 's expr) : 's GO.system =
    let bsys = billard_compute sys in
    let billard = bsys.man in
    let man0 = match man0 with Some man0 -> man0 | None -> OS.newman() in
    print_endline ("[compute_smart] result agregation (proto) "^(billard_stats billard));
    if billard.ftermed > 0
    then (billard_fterm_process billard (Array.length billard.blocks -1))
    else (OS.clear_caches billard.man; Gc.compact()); (* major-collection-cycle + heap-compaction *)
    let funmap (name, index) = (name, billard_get_var_extern billard man0 index) in
    let sset =
      try Array.map funmap bsys.sset
      with exn -> raise (OUnit.cascade_exn "IoUtils.compute_smart:result_agregation" exn)
    in
    print_endline ("[compute_smart] result agregation (post) "^(billard_stats billard));
    billard_clear billard;
    assert(bsys.wire = [||]);
    { name = bsys.name; man = man0; inps = bsys.inps; regs = bsys.regs; wire = [||]; outs = bsys.outs; sset }

  let billard_tof_vanilla (tof_s:'s Io.ToF.t) (cha:out_channel) (sys:'s billard_system) : unit =
    print_endline ("[billard_tof_vanilla] proto "^(billard_stats sys.man));
    billard_cleaning_process ~normalize:true sys.man (Array.length sys.man.blocks -1);
    print_endline ("[billard_tof_vanilla] ante "^(billard_stats sys.man));
    let tof_man cha ((billard, ia):billard * (int array)) : unit =
      let fa = Array.map (billard_get_var_intern billard) ia in
      OS.tof ~nocopy:true ~normalize:false ~destruct:true billard.man cha (Array.to_list fa)
    in
    IoUtils.ToF.system tof_s tof_man cha sys;
    print_endline ("[billard_tof_vanilla] post "^(billard_stats sys.man));
    ()

  let billard_tof_combine (tof_s:'s Io.ToF.t) (cha:out_channel) (sys:'s billard_system) : unit =
    let tof_man cha ((billard, ia):billard * (int array)) : unit =
      (* to simplify storing process, put every output in some temporary file *)
      print_endline ("[billard_tof_combine] proto: "^(billard_stats billard));
      billard_fterm_process billard (Array.length billard.blocks -1);
      print_endline ("[billard_tof_combine] ante: "^(billard_stats billard));
      (* retrieves file names for each output *)
      let f'a : OS.F.f' array =
        Array.map (fun i ->Tools.unop(billard.blocks.(i).fterm)) ia
      in
      OS.F.tof cha (Array.to_list f'a);
    in
    IoUtils.ToF.system tof_s tof_man cha sys;
    print_endline ("[billard_tof_combine] combine:post "^(billard_stats sys.man));
    ()

  let billard_tof (tof_s:'s Io.ToF.t) (cha:out_channel) (sys:'s billard_system) : unit =
    if sys.man.ftermed > 0 (* [DEBUG] *)
    then (billard_tof_combine tof_s cha sys)
    else (billard_tof_vanilla tof_s cha sys)

  let compute_to_file (tof_s:'s Io.ToF.t) (sys:'s expr) (fileB:string) : unit =
    let bsys : 's billard_system = billard_compute sys in
    OS.clear_caches bsys.man.man; Gc.compact();
    let cha_fileB = open_out_bin fileB in
    billard_tof tof_s cha_fileB bsys;
    close_out cha_fileB;
    billard_clear bsys.man;
    ()

  let import_smart (sys:'s expr) (man0:OS.t) : 's GO.system = compute_smart ~man0 sys

  let pure_of_expr ?(out_list=true) ?(smart=true) (sys:string expr) (fileB:string) : unit =
    if smart
    then if true
      then (compute_to_file Io.ToF.string sys fileB)
      else (
        let sys = compute_smart sys in
        GO.to_file ~nocopy:true sys fileB
      )
    else (
      let sys = import sys (OS.newman()) in
      GO.to_file ~nocopy:true sys fileB
    )

  let pure_of_verilog ?(out_list=false) ?(smart=true) (fileA:string) (fileB:string) : unit =
    let expr = StrLoadVerilog.load_file fileA in
    pure_of_expr ~out_list ~smart expr fileB

  let pure_of_pla ?(out_list=false) ?(smart=true) (fileA:string) (fileB:string) : unit =
    let expr = StrLoadPla.load_file fileA in
    pure_of_expr ~out_list ~smart expr fileB

  let pure_of_cnf ?(out_list=false) ?(smart=true) (fileA:string) (fileB:string) : unit =
    let cnf   = StrLoadCnf.load_file fileA in
    let snax  = Snax.Module.G1.newman () in
    let fA    = SnaxOops.OOPS.LoadCnf0.export_cnf snax cnf in
    let sys = IoUtils.make
      "CNF" snax cnf.Constraint_system_types.input [|fA.SnaxOops.OOPS.Model.edge|]
    in
    let sexpr = IoUtils.expr_of_snax sys IoUtils.string_fnn in
    pure_of_expr ~out_list ~smart sexpr fileB

  let ldd_of_expr ?(out_list=false) ?(smart=true) (expr:string IoTypes.expr) : string GO.system =
    let file_ldd = Filename.temp_file "dagaml_OfExprOfOops_" ".ldd.pure" in
    pure_of_expr ~out_list ~smart expr file_ldd;
    let sys = GO.system_off file_ldd in
    Sys.remove file_ldd;
    sys

  (*
  let import_cnf_formula_d4 (t:OS.t) (ga:int) (f:CnfTypes.formule) : OS.f =
      let cnf = CnfUtils.file_of_formule ~input:ga f in
      let sys_expr = CnfExternal.D4.compute cnf in
      let sys_ldd = ldd_of_expr sys_expr in
      let f0 = OS.copy_into
        sys_ldd.IoTypes.man
        [snd sys_ldd.IoTypes.sset.(0)]
        t
        |> List.hd
      in
      f0
 *)

end
