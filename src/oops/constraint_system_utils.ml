(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : Constraint System Types : Conjunctive Form
 *)

open GuaCaml
open Extra

module CSTypes = Constraint_system_types
open CSTypes

let map_formule ?(verbose=false) (mapf:'f1 -> 'f2) (sys:'f1 formule) : 'f2 formule =
  let i = ref 0 in
  let len = List.length sys in
  sys ||> (fun f ->
    incr i;
    print_string ("[Constraint_system_utils.map_formule] step : "^(string_of_int !i)^" / "^(string_of_int len)^"\r");
    flush stdout;
    mapf f)
  |> (fun f -> print_newline(); f)

let map_system ?(verbose=false) (mapt:'t1 -> 't2) (mapf:'f1 -> 'f2) (sys:('t1, 'f1) system) : ('t2, 'f2) system =
  {
    name    = sys.name;
    input   = sys.input;
    quants  = sys.quants;
    man     = mapt sys.man;
    formule = map_formule ~verbose mapf sys.formule;
  }

let to_iotypes_system (sys:('t, 'f) system) : (int, 't, 'f) IoTypes.system =
  let name : string = sys.name in
  let man  : 't     = sys.man in
  let inps : int array = Array.init sys.input (fun i -> i) in
  let regs : int array = [||] in
  let wire : int array = [||] in
  let output : int = List.length sys.formule in
  let outs : int array = Array.init output (fun i -> sys.input + i) in
  let sset : (int * 'f) array = Array.map2 (fun x y -> (x, y)) outs (Array.of_list sys.formule) in
  IoTypes.{name; man; inps; regs; wire; outs; sset}

module Wap =
struct
  open Snowflake
  let kdecomp_of_formule (support:'f -> int list) (f:'f formule) : int list list =
    f ||> (fun f -> let s = support f in assert(SetList.sorted_nat s); s)

  let input_of_system (support:'t -> 'f -> int list) (sys:('t, 'f) system) : Wap_exchange.input =
    let kdecomp = kdecomp_of_formule (support sys.man) sys.formule in
    let variables = List.init sys.input (fun i -> i) in
    Wap_exchange.{variables; parameters=[]; kdecomp}

  let refined_primal_graph_of_system ?(verbose=false) (pg:'t -> 'f -> GGLA_FT.Type.hg) (sys:('t, 'f) system) : GGLA_FT.Type.hg =
    let n = List.length sys.formule in
    sys.formule
    |> MyList.mapi
      (fun i x ->
        if verbose then print_endline ("[CSUtils.Wap.refined_primal_graph_of_system] {index:"^(string_of_int i)^"/"^(string_of_int n)^"}");
        pg sys.man x
      )
    |> List.fold_left GGLA_FT.union (GGLA_FT.empty sys.input)

  let input_of_qsystem
      (support:'t -> 'f -> int list)
     ?(extra_kind=(Some true))
      (sys:('t, 'f) system) : Cwap_exchange.input =
    let kdecomp = kdecomp_of_formule (support sys.man) sys.formule in
    let variables = List.init sys.input (fun i -> i) in
    let quants = sys.quants ||> (fun (qb, ql) -> (ql, qb)) |> List.rev in
    let freeze = Array.make sys.input true in
    List.iter (fun (ql, _) -> List.iter (fun i -> freeze.(i) <- false) ql) quants;
    let extra = MyArray.list_of_indexes_true freeze in
    let input =
      match extra_kind with
      | None -> (
        let variables = SetList.minus variables extra in
        Cwap_exchange.{variables; parameters = extra; quants; kdecomp}
      )
      | Some qb -> (
        let quants = quants @ [(extra, true)] in
        Cwap_exchange.{variables; parameters = []; quants; kdecomp}
      )
    in
    Cwap_exchange_utils.normalize_input input
end
