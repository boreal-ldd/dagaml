(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : AQEOPS_CCoSTreD : interfacing DAGaml with CCoSTreD via AQEOPS
 *)

open GuaCaml
open Extra
open STools

module Make(Q:AQEOPS.MSig) =
struct
  module GOPS = OOPS.Make(Q)

  module Model : CCoSTreD.MSig
    with type t  = Q.t
    and  type f0 = Q.f
    and  type f1 = Q.f
    and  type f2 = Q.f
    and  type f3 = Q.f
  =
  struct
    type t  = Q.t
    type f0 = Q.f
    type f1 = Q.f
    type f2 = Q.f
    type f3 = Q.f

    type supp = int list (* not required by MSig *)

    let support t f =
      Q.support t f |> Support.to_sorted_support

    let support0 = support
    let support1 = support
    and support2 = support
    and support3 = support

    let trivial : t -> _ -> bool option = Q.to_bool
    let trivial0 = trivial
    let trivial1 = trivial
    and trivial2 = trivial
    and trivial3 = trivial

    (* Section 1. FRP *)

    let elim_exists (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : f1 * f2 =
      (* print_string "[QEOPS_RTBF2.solve_variable] begin"; print_newline(); *)
      let time_start (text:string) : _ =
        OProfile.(time_start default) ("[AQEOPS_CCoSTreD.Make.Model.elim_exists] "^text)
      in
      let stop = time_start "list_and" in
      let f = Q.(&!!) t ga (f0l@f1l) in
      stop();
      let stop = time_start "existA" in
      let elim_supp =
        try
          Support.of_sorted_support ga elim
        with _ -> (
          print_endline ("[AQEOPS_CCoSTreD.Make.Module.elim_exists] ga:"^STools.ToS.(int ga));
          print_endline ("[AQEOPS_CCoSTreD.Make.Module.elim_exists] elim:"^STools.ToS.(list int elim));
          failwith "[AQEOPS_CCoSTreD.Male.Module.elim_exists] MyList.of_sorted_indexes ga elim"
        )
      in
      let pf = Q.existsA t elim_supp f in
      stop();
      (pf, f)

    let elim_forall0 (t:t) (ga:int) (f0:f0) (supp:supp) (elim:supp) (proj:supp) : f0 =
      let elim_supp = Support.of_sorted_support ga elim in
      Q.forallA t elim_supp f0

    let elim_forall1 (t:t) (ga:int) (f1:f1) (supp:supp) (elim:supp) (proj:supp) : f1 =
      let elim_supp = Support.of_sorted_support ga elim in
      Q.forallA t elim_supp f1

    (* Section 2.0 FRP to BPP *)
    (* val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3 *)
    let solve_parameter t (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      let time_start (text:string) : _ =
        OProfile.(time_start default) ("[AQEOPS_CCoSTreD.Make.Model.solve_parameter] "^text)
      in
      let stop = time_start "list_and" in
      let f = Q.(&!!) t ga (f0l@f1l) in
      stop();
      f

    (* Section 2. BPP *)
    let backproj (t:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (introU:supp) (elimE:supp) (supp2:supp) : f3 =
      let elimE_supp = Support.of_sorted_support ga elimE in
      let f3' = Q.existsA t elimE_supp f3 in
      Q.(&!) t f3' f2
  end

  module Module = CCoSTreD.Make(Model)

  module CSTypes = Constraint_system_types
  module CSUtils = Constraint_system_utils

  type output = {
    out_wap : Cwap_exchange.output;
    out_sat : bool;
    out_bpp : Q.f array;
  }

  let internal_compute
      ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
       (out_wap:Cwap_exchange.output)
       (sys:(Q.t, Q.f) CSTypes.system) : output =
    let t  = sys.CSTypes.man in
    let n  = sys.CSTypes.input in
    let fl = sys.CSTypes.formule in
    if frp_only
    then (
      match Module.apply_cfrp t ~check:false n out_wap fl [] with
      | None -> (
        let ff = Q.cst t false n in
        {
          out_wap;
          out_sat = false;
          out_bpp = [| ff |];
        }
      )
      | Some(out_frp) ->
        {out_wap; out_sat = true; out_bpp = [||]}
    )
    else (
      match Module.apply t ~check:false n out_wap fl [] with
      | None -> (
        let ff = Q.cst t false n in
        {
          out_wap;
          out_sat = false;
          out_bpp = [| ff |];
        }
      )
      | Some state ->
        {out_wap; out_sat = true; out_bpp = state.Module.f3a}
    )

  module CNF =
  struct
    open CnfTypes

    let compute_clause
        (t:Q.t)
        (nvar:int)
        (vars:(Q.f * Q.f) array)
        (cl:clause) : Q.f =
      cl
      ||> (fun (i, b) -> (if b then snd else fst) vars.(i))
      |> Q.(|!!) t nvar

    module LoadCnfA = LoadCnf.MakeA(Q)

    (* #preprocesing : perform bucketization of formule, then treefy each buckets, the result is sent to CoSTreD *)
    let compute_treefy_bucket
       ?(mode=Tools.ASAP)
        (t:Q.t)
        (out_wap:Cwap_exchange.output)
        (vars:(Q.f * Q.f) array)
        (cnf:CnfTypes.system) : Q.f list =
      let stop = OProfile.(time_start default)
        "[AQEOPS_CCoSTreD.Make.CNF.compute_treefy_bucket]"
      in
      let sa : int list array = Array.map
        (fun oc ->
          SetList.union_list Cwap_exchange.([oc.internal; oc.cfrp_exists; oc.cfrp_forall; oc.cfrp_params]))
        out_wap.Cwap_exchange.sequence
      in
      let fl, fl' = LoadCnfA.compute_treefy_bucket ~mode t sa vars cnf in
    (* otherwise either :
      (1) compile them separately (safe)
      (2) compile them together (risky) *)
      assert(fl' = []);
      stop();
      fl

    (* assumes [CnfUtils.file_sorted cnf] *)
    let compute
          ?(bucketize_mode=Tools.ASAP)
          ?(treefy_bucket=false)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
          ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
         (wap:Cwap_exchange.input -> Cwap_exchange.output)
         (t:Q.t) (cnf:CnfTypes.system) : output =
      assert(CnfUtils.file_sorted cnf);
      let stop = OProfile.(time_start default) "[AQEOPS_CCoSTreD.MakeCNF.compute_bpp] wap-processing" in
      let wap_in  = CnfUtils.Wap.input_of_qbf cnf in
      let out_wap = wap wap_in in
      stop();
      let n = cnf.input in
      let vars = GOPS.array_make_n_bivar t n in
      let fl =
        if treefy_bucket
        then (compute_treefy_bucket ~mode:bucketize_mode t out_wap vars cnf)
        else (cnf.formule ||> (compute_clause t n vars))
      in
      let sys = CSTypes.{
        name = cnf.CSTypes.name;
        input = cnf.CSTypes.input;
        quants = cnf.CSTypes.quants;
        man = t;
        formule = fl;
      } in
      internal_compute ~frp_only out_wap sys

    let compute_dummy ?(lvl=0)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
(*        ?(dobpp=true) (* if [dobpp=false] then do not compute back-propagation field of the output *) *)
         (t:Q.t) (cnf:CnfTypes.system) : Q.f =
      let n = cnf.input in
      let vars = GOPS.array_make_n_bivar t n in
      let fl = cnf.formule ||> (compute_clause t n vars) in
      match lvl with
      | 0 -> Q.(&!!) t n fl
      | _ -> failwith "[AQEOPS_CCoSTreD.Make.CNF.compute_dummy] this level is not implemented"

  end
end

module MakeCNF(Q:AQEOPS.MSig) =
struct
  module GOPS = OOPS.Make(Q)

  module LoadCnfA = LoadCnf.MakeA(Q)

  module Model : CCoSTreD.MSig
    with type t  = Q.t
    and  type f0 = CnfTypes.clause (* sorted *)
    and  type f1 = Q.f
    and  type f2 = Q.f
    and  type f3 = Q.f
  =
  struct
    (* [type t] representation language manager *)
    type t = Q.t (* Q enabled *)
    type f0 = CnfTypes.clause (* sorted *)
    type f1 = Q.f
    type f2 = Q.f
    type f3 = Q.f

    type supp = int list (* not required by MSig *)

    let support t f =
      Q.support t f |> Support.to_sorted_support

    let support0 _ = CnfUtils.support
    let support1 = support
    and support2 = support
    and support3 = support

    let trivial : t -> _ -> bool option = Q.to_bool
    let trivial0 _ = function
      | [] -> Some false
      | _  -> None
    let trivial1 = trivial
    and trivial2 = trivial
    and trivial3 = trivial

    (* Section 1. FRP *)

    let elim_exists (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : f1 * f2 =
      let time_start (text:string) : _ =
        OProfile.(time_start default) ("[AQEOPS_CCoSTreD.MakeCNF.Model.elim_exists] "^text)
      in
      let stop = time_start "GOPS.array_make_n_var" in
      let vars = GOPS.array_make_n_bivar t ga in
      stop();
      let stop = time_start "LoadCnfA.export_formule" in
      (* [DEBUG] assert(CnfUtils.formule_in_order f0l supp); *)
      let f0 = LoadCnfA.export_formule t vars supp ga f0l in
      stop();
      let stop = time_start "list_and" in
      let f = Q.(&!!) t ga (f0::f1l) in
      stop();
      let stop = time_start "existA" in
      let elim_supp = Support.of_sorted_support ga elim in
      let pf = Q.existsA t elim_supp f in
      stop();
      (* print_string "[QEOPS_RTBF2.solve_variable] end"; print_newline(); *)
      (pf, f)

    let elim_forall0 (t:t) (ga:int) (f0:f0) (supp:supp) (elim:supp) (proj:supp) : f0 =
      Assoc.restr f0 proj

    let elim_forall1 (t:t) (ga:int) (f1:f1) (supp:supp) (elim:supp) (proj:supp) : f1 =
      let elim_supp = Support.of_sorted_support ga elim in
      Q.forallA t elim_supp f1

    (* Section 2.0 FRP to BPP *)
    (* val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3 *)
    let solve_parameter t (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      let vars = GOPS.array_make_n_bivar t ga in
      let stop = OProfile.(time_start default)
        "[AQEOPS_CCoSTreD.Model.solve_parameter] LoadCnfA.export_formule"
      in
      let f0 = LoadCnfA.export_formule t vars supp ga f0l in
      stop();
      let stop = OProfile.(time_start default)
        "[AQEOPS_CCoSTreD.Model.solve_parameter] list_and"
      in
      let f = Q.(&!!) t ga (f0::f1l) in
      stop();
      f

    (* Section 2. BPP *)
    let backproj (t:t) (ga:int) (f3:f3) (f2:f2) (supp3:supp) (introU:supp) (elimE:supp) (supp2:supp) : f3 =
      let elimE_supp = Support.of_sorted_support ga elimE in
      let f3' = Q.existsA t elimE_supp f3 in
      Q.(&!) t f3' f2
  end
  module Module = CCoSTreD.Make(Model)

  module CNF =
  struct
    open CnfTypes

    type output = {
      out_wap : Cwap_exchange.output;
      out_sat : bool;
      out_bpp : Q.f array;
    }

    (* assumes [CnfUtils.file_sorted cnf] *)
    let compute
          ?(treefy_bucket=false)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
          ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
         (wap:Cwap_exchange.input -> Cwap_exchange.output)
         (t:Q.t) (cnf:CnfTypes.system) : output =
      assert(CnfUtils.file_sorted cnf);
      let stop = OProfile.(time_start default)
        "[AQEOPS_CCoSTreD.Model.compute_bpp] wap-processing"
      in
      let wap_in  = CnfUtils.Wap.input_of_qbf cnf in
      let out_wap = wap wap_in in
      stop();
      let n = cnf.input in
      if frp_only
      then (
        match Module.apply_cfrp t ~check:false n out_wap cnf.formule [] with
        | None -> (
          let ff = Q.cst t false n in
          {
            out_wap;
            out_sat = false;
            out_bpp = [| ff |];
          }
        )
        | Some _ ->
          {out_wap; out_sat = true; out_bpp = [||]}
      )
      else (
        match Module.apply t ~check:false n out_wap cnf.formule [] with
        | None -> (
          let ff = Q.cst t false n in
          {
            out_wap;
            out_sat = false;
            out_bpp = [| ff |];
          }
        )
        | Some state -> (
          {out_wap; out_sat = true; out_bpp = state.Module.f3a}
        )
      )
  end
end
