(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : QEOPS : Quantifier Elimination and ordered OPeratorS
 *
 * === NOTE ===
 *
 * exposes a common interface for ordered and uniform functional
 * computations
 * extends [OOPS] with :
 * - support
 * - partial evalution
 * - quantifier elimination
 *)

module type MSig =
sig
  include OOPS.MSig
  (* [module type OOPS.MSig]
    type t (* [f]'s manager *)
    type f
    (*
      Ldd_B_u_nu_types.(function true -> S | false -> P)
    *)
    val ( ->> ) : t -> bool list -> f -> f
    (* ?? add method : phase shifter      ??
       ?? add method : partial evaluation ??
     *)
    val arity   : t -> f -> int
    (* where true represent significant variables and false non significant ones *)
    val   cneg : t -> bool -> f -> f
    val ( &! ) : t -> f -> f -> f
    val ( |! ) : t -> f -> f -> f
    val ( ^! ) : t -> f -> f -> f
    val ( =! ) : t -> f -> f -> f
    val ( *! ) : t -> f -> f -> f

    val cst : t -> bool -> int -> f

    (*  return a consistent projection of fs
        to [Some bool] or [None] otherwise
        remark:
          unless specified otherwise,
          [to_bool t f = None] does not mean
          than [f] is constant only that one
          could not prove it
     *)
    val to_bool : t -> f -> bool option
  *)

(* [TODO] ??
  val phase_shift : t -> bool list -> f -> f
  (*
    [bool = true ] -> complement variable
    [bool = false] -> do nothing
   *)
 *)

  (* displays [f] into a human-readable format *)
  val to_string : t -> f -> string

  (* [support t f = supp] *)
  val support : t -> f -> Support.t

  (* Partial Evaluation (Absolute/Relative) *)
  val pevalA : t -> (bool option) list -> f -> f
  (* Absolute naming
      [partial_evalA t bl f1 = f2] with :
        - [arity f2 = arity f1]
        - [length bl = arity f1]
      [bool = Some b] -> evaluate to [b]
      [bool = None  ] -> do nothing
   *)

  val pevalR : t -> (bool option) list -> f -> f
  (* Relative naming
      [partial_evalR t bl f1 = f2] with :
        - [arity f2 = count_true bl]
        - [length bl = arity f1]
      [bool = Some b] -> evaluate to [b]
      [bool = None  ] -> do nothing
   *)

  (* Existential Closure (Absolute/Relative) *)
  val existsA : t -> Support.t -> f -> f
  (* Absolute naming : existentially eliminates all variables in support *)
  val existsR : t -> Support.t -> f -> f
  (* Relative naming : existentially eliminates all variables in support *)

  (* Universal Closure (Absolute/Relative) *)
  val forallA : t -> Support.t -> f -> f
  (* Absolute naming : univerally eliminates all variables in support *)
  val forallR : t -> Support.t -> f -> f
  (* Relative naming : univerally eliminates all variables in support *)
end
