(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : io      : conversion from and to various file format
 *
 * module  : Constraint System Types : Conjunctive Form
 *)

open GuaCaml

type quant = bool * (int list)
(* quantifier:
  - bool = false -> universal  /forall
  - bool = true  -> existential/exists
 *)
type quants = quant list

type 'f formule = 'f list

type ('t, 'f) system = {
  name : string;
  input : int;
  quants : quants;
    (* [int list] is sorted by increasing order
       set of variables *)
    (* inner quantifiers are the last ones in quants
       quants_0 are the top-level quantifier *)
  man : 't;
  formule : 'f formule;
}

module ToS =
struct
  open STools
  include ToS

  let quant (q:quant) : string = (bool * (list int)) q
  let quants (q:quants) : string = list quant q

  let formule (f:'f -> string) (formule:'f formule) : string =
    list f formule

  let system (t:'t -> string) (f:'f -> string) (system:('t, 'f) system) : string =
    "{ name = "^(string system.name)^
    "; input = "^(int system.input)^
    "; quants = "^(quants system.quants)^
    "; man = "^(t system.man)^
    "; formule = "^(formule f system.formule)^" }"
end
