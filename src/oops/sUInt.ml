(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : SUInt : Sized Unsigned Integers
 *)

open GuaCaml

module Make(Model:OOPS.MSig) =
struct
  open Model
  module G0 = OOPS.Make(Model)

  let fulladd2 t a b =
    let carry = (|!) t a b in
    let somme = (^!) t a b in
    (somme, carry)

  let fulladd3 t a b c =
    let carry = (&!) t ((|!) t a ((&!) t b c)) ((|!) t b c) in
    let somme = (^!) t ((^!) t a b) c in
    (somme, carry)

  type sized_uint =
  {
(* arity of each [edge], usually arity <> Array.length array *)
    arity : int;
    array : f array
  }

  let map f intX = {arity = intX.arity; array = Array.map f intX.array}

  let add t ?carry intX intY =
    let ari = intX.arity
    and len = Array.length intX.array in
    assert(ari = intY.arity);
    assert(len = Array.length intY.array);
    let carry = ref (match carry with
      | None -> (cst t false ari)
      | Some edge -> assert(arity t edge = ari); edge)
    in
    let intXY = {
      arity = ari;
      array = Array.init len (fun i ->
        let x = intX.array.(i)
        and y = intY.array.(i)
        and c = !carry in
        let xy, c = fulladd3 t x y c in
        carry := c;
        xy)
    } in
    (intXY, !carry)

  let ( +/ ) t x y = add t x y |> fst

  (* zero padding shift left *)
  (* decreasing shift *)
  let shift_right (t:t) (shift:int) (intX:sized_uint) =
    let len = Array.length intX.array in
    assert(shift <= len);
    let zero = cst t false intX.arity in
    let array = Array.init len (fun i ->
      let i' = i + shift in
      if i' < len then intX.array.(i') else zero) in
    {arity = intX.arity; array}

  let ( >>/ ) t intX shift = shift_right t shift intX

  (* increasing shift *)
  let shift_left t shift intX =
    let len = Array.length intX.array in
    assert(shift <= len);
    let zero = cst t false intX.arity in
    let array = Array.init len (fun i ->
      let i' = i - shift in
      if i' >= 0 then intX.array.(i') else zero) in
    {arity = intX.arity; array}

  let ( <</ ) t intX shift = shift_left t shift intX

  let bitwise_binop binop t intX intY =
    let ari = intX.arity
    and len = Array.length intX.array in
    assert(ari = intY.arity);
    assert(len = Array.length intY.array);
    {arity = ari; array = Array.map2 (binop t) intX.array intY.array}

  let ( |&/ ) = bitwise_binop ( &! )
  let ( |^/ ) = bitwise_binop ( ^! )
  let ( ||/ ) = bitwise_binop ( |! )
  let ( |=/ ) = bitwise_binop ( =! )
  let ( |*/ ) = bitwise_binop ( *! )

  let (  =/ ) t intX intY = G0.array_and t ((|=/) t intX intY).array
  let ( <>/ ) t intX intY = G0.neg t ((=/) t intX intY)

  let scalar_binop_left binop t bitX intY =
    assert(arity t bitX = intY.arity);
    map (binop t bitX) intY

  let scalar_binop_right binop t intX bitY =
    assert(arity t bitY = intX.arity);
    map (fun bit -> binop t bit bitY) intX

  let ( |.&/ ) = scalar_binop_left ( &! )
  let ( |.^/ ) = scalar_binop_left ( ^! )
  let ( |.|/ ) = scalar_binop_left ( |! )
  let ( |.=/ ) = scalar_binop_left ( =! )
  let ( |.*/ ) = scalar_binop_left ( *! )

  let ( |&./ ) = scalar_binop_right ( &! )
  let ( |^./ ) = scalar_binop_right ( ^! )
  let ( ||./ ) = scalar_binop_right ( |! )
  let ( |=./ ) = scalar_binop_right ( =! )
  let ( |*./ ) = scalar_binop_right ( *! )

(*
let lX default man : t -> t -> Ldd_B_u_nu.TACX.edge =
  let ( =! ) = Oops.(=!) (Ldd_B_u_nu.(^!) man)
  and ( && ) = Ldd_B_u_nu.(&!) man in
  let ( || ) = Oops.(|!) ( && ) in
  let ite c x y = (c && x) || ((Ldd_B_u_nu.no c)&& y) in
  let ( <! ) x y = (Ldd_B_u_nu.no x) && y in fun x y ->
  let ari, len, x, y = access2 x y in
  let rec aux c i =
    if i < len
    then
    (
      let x = x i
      and y = y i in
      let c = ite ( x =! y ) c ( x <! y ) in
      aux c (i+1)
    )
    else c
  in aux (Oops.make_const default ari) 0

let lt = lX false
let le = lX true

let gX default man =
  let aux = lX (not default) man in
  fun x y -> Ldd_B_u_nu.no(aux x y)

let gt = gX false
let ge = gX true
*)

(*
let copy ncopy myuint =
  let mat = Oops.copy_funvect ncopy myuint.arity myuint.array in
  Array.map (fun array -> { arity = ncopy * myuint.arity; array = array }) mat

let copy_t ncopy myuint =
  let mat = Oops.copy_funvect_t ncopy myuint.arity myuint.array in
  Array.map (fun array -> { arity = ncopy * myuint.arity; array = array }) mat

let input man =
  let nvar = Oops.array_make_n_var man in fun n ->
  {
    arity = n;
    array = nvar n
  }

let range man =
  let (<=!) = le man
  and input = input man in fun n ->
  let arity = Array.length (Tools.bin_of_int (n-1)) in
  let myuint = input arity in
  myuint, (myuint <=! (of_int arity (n-1)))

let input_r man =
  let nvar = Oops.array_make_n_var man in fun n ->
  {
    arity = n;
    array = nvar n |> Array.to_list |> List.rev |> Array.of_list
  }

let range_r man =
  let (<=!) = le man
  and input = input_r man in fun n ->
  let arity = Array.length (Tools.bin_of_int (n-1)) in
  let myuint = input arity in
  myuint, (myuint <=! (of_int arity (n-1)))

let cat x fxs y fys =
  let xS = MyList.ntimes (Ldd_B_u_nu_types.S) x.arity
  and xP = MyList.ntimes (Ldd_B_u_nu_types.P) x.arity
  and yS = MyList.ntimes (Ldd_B_u_nu_types.S) y.arity
  and yP = MyList.ntimes (Ldd_B_u_nu_types.P) y.arity in
  let sp = xS @ yP
  and ps = xP @ yS in
  {
    arity = x.arity + y.arity;
    array = Array.map (Oops.(->>) sp) x.array
  },
  Array.map (Oops.(->>) sp) fxs,
  {
    arity = x.arity + y.arity;
    array = Array.map (Oops.(->>) ps) y.array
  },
  Array.map (Oops.(->>) ps) fys
*)

(*
  let multiply intX intY =
    let ari = intX.arity
    and len = Array.length intX.array in
    assert(ari = intY.arity);
    assert(len = Array.length intY.array);
*)

(*
let int_mult nvars x y =
  let x, y = if Array.length x <= Array.length y then x, y else y, x in
  let n = Array.length x in
  if Array.length x = 0
  then {nvars = nvars; shift = 0; array = [| |]}
  else
  (
    let funmap i xi = {nvars = nvars; shift = i; array = int_scalarmult xi y} in
    let square  = Array.mapi funmap x in
    let rec aux s i =
      print_string "x:"; print_int i; print_string "\r"; flush stdout;
      if i = n
      then s
      else aux (shiftint_fulladd s square.(i)) (i+1)
    in
    aux (square.(0)) 1
  )
*)

end
