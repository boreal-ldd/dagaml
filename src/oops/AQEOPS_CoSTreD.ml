(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : AQEOPS_CoSTreD : interfacing DAGaml with CoSTreD via AQEOPS
 *)

open GuaCaml
open Extra
open STools

module Make(Q:AQEOPS.MSig) =
struct
  module GOPS = OOPS.Make(Q)

  type man = {
                ldd : Q.t; (* Q enabled *)
    mutable    step : int;
    mutable   total : int;
    mutable verbose : bool;
  }

  module Model : Snowflake.CoSTreD.MSig
    with type t  = man
    and  type f0 = Q.f
    and  type f1 = Q.f
    and  type f2 = Q.f
    and  type f3 = Q.f
  =
  struct
    type t  = man
    type f0 = Q.f
    type f1 = Q.f
    type f2 = Q.f
    type f3 = Q.f

    type supp = int list (* not required by MSig *)

    let support man f =
      Q.support man.ldd f |> Support.to_sorted_support

    let support0 = support
    let support1 = support
    and support2 = support
    and support3 = support

    let trivial (t:t) : _ -> bool option = Q.to_bool t.ldd
    let trivial0 = trivial
    let trivial1 = trivial
    and trivial2 = trivial
    and trivial3 = trivial

    (* Section 1. FRP *)

    let solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 =
      if t.verbose then print_endline ("[AQEOPS_CoSTreD.Model.solve_variable] {step:"^(ToS.int t.step)^"{max:"^(ToS.int t.total)^"}}");
      t.step <- succ t.step;
      (* print_string "[AQEOPS_RTBF2.solve_variable] begin"; print_newline(); *)
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Make.Model.solve_variable] list_and"
      in
      let f = Q.(&!!) t.ldd ga (f0l@f1l) in
      stop();
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Make.Model.solve_variable] existA"
      in
      let elim_bl = Support.of_sorted_support ga elim in
      let pf = Q.existsA t.ldd elim_bl f in
      stop();
      ([pf], f)

    (* Section 2.0 FRP to BPP *)
    (* val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3 *)
    let solve_parameter t (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      if t.verbose then print_endline ("[AQEOPS_CoSTreD.Model.solve_parameter]");
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Make.Model.solve_parameter] list_and"
      in
      let f = Q.(&!!) t.ldd ga (f0l@f1l) in
      stop();
      f

    (* Section 2. BPP *)
    let backproj (t:t) (ga:int) (f3:f3) (f2:f2) supp3 inter supp2 : f3 =
      if t.verbose then print_endline ("[AQEOPS_CoSTreD.Model.backproj] {step:"^(ToS.int t.step)^"{max:"^(ToS.int t.total)^"}}");
      t.step <- pred t.step;
      let qg = SetList.minus supp3 inter in (* SetList.minus supp3 supp2 *)
      let qg_supp = Support.of_sorted_support ga qg in
      let f3' = Q.existsA t.ldd qg_supp f3 in
      Q.(&!) t.ldd f3' f2
  end
  module Module = Snowflake.CoSTreD.Make(Model)

  module CSTypes = Constraint_system_types
  module CSUtils = Constraint_system_utils

  type output = {
    out_wap : Snowflake.Wap_exchange.output;
    out_frp : Module.f201 array;
    out_sat : bool;
    out_mid : Q.f array;
    out_bpp : Q.f array;
  }

  let internal_compute
      ?(verbose=false)
      ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
       (out_wap:Snowflake.Wap_exchange.output)
       (sys:(Q.t, Q.f) CSTypes.system) : output =
    let t  = sys.CSTypes.man in
    let n  = sys.CSTypes.input in
    let fl = sys.CSTypes.formule in
    let total = Array.length out_wap.Snowflake.Wap_exchange.sequence in
    let man = Model.{ldd = t; step = 0; total; verbose} in
    if frp_only
    then (
      match Module.apply_frp man ~check:false n fl [] out_wap with
      | None -> (
        let ff = Q.cst t false n in
        {
          out_wap;
          out_frp = [| AB.A ff |];
          out_sat = false;
          out_mid = [| ff |];
          out_bpp = [||];
        }
      )
      | Some(out_frp) ->
        {out_wap; out_sat = true; out_frp; out_mid = [||]; out_bpp = [||]}
    )
    else (
      match Module.apply man ~check:false n fl [] out_wap with
      | None -> (
        let ff = Q.cst t false n in
        {
          out_wap;
          out_frp = [| AB.A ff |];
          out_sat = false;
          out_mid = [| ff |];
          out_bpp = [| ff |];
        }
      )
      | Some(out_frp, out_mid, out_bpp) ->
        let out_mid = Array.map AB.mergeAB out_mid in
        {out_wap; out_sat = true; out_frp; out_mid; out_bpp}
    )

  let compute
      ?(verbose=false)
      ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
       (wap:Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output)
       (sys:(Q.t, Q.f) CSTypes.system) : output =
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Make.compute] wap-processing"
      in
      let support t f = Q.support t f |> Support.to_sorted_support in
      let wap_in  = CSUtils.Wap.input_of_system support sys in
      let out_wap = wap wap_in in
      stop();
      internal_compute ~verbose ~frp_only out_wap sys

  let compute_dummy ?(lvl=0)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
(*        ?(dobpp=true) (* if [dobpp=false] then do not compute back-propagation field of the output *) *)
       (sys:(Q.t, Q.f) CSTypes.system) : Q.f =
    match lvl with
    | 0 -> Q.(&!!) sys.CSTypes.man sys.CSTypes.input sys.CSTypes.formule
    | _ -> failwith "[AQEOPS_CoSTreD.Make.CNF.compute_dummy] this level is not implemented"

  module CNF =
  struct
    open CnfTypes

    let compute_clause
        (t:Q.t)
        (nvar:int)
        (vars:(Q.f * Q.f) array)
        (cl:clause) : Q.f =
      cl
      ||> (fun (i, b) -> (if b then snd else fst) vars.(i))
      |> Q.(|!!) t nvar

    module LoadCnfA = LoadCnf.MakeA(Q)

    (* #preprocesing : perform bucketization of formule, then treefy each buckets, the result is sent to CoSTreD *)
    let compute_treefy_bucket
       ?(mode=Tools.ASAP)
        (t:Q.t)
        (out_wap:Snowflake.Wap_exchange.output)
        (vars:(Q.f * Q.f) array)
        (cnf:CnfTypes.system) : Q.f list =
      let stop = OProfile.(time_start default)
        "[AQEOPS_CoSTreD.Make.CNF.compute_treefy_bucket]"
      in
      let sa : int list array = Array.map
        (fun comp -> comp.Snowflake.Wap_exchange.support)
        out_wap.Snowflake.Wap_exchange.sequence
      in
      let fl, fl' = LoadCnfA.compute_treefy_bucket ~mode t sa vars cnf in
    (* otherwise either :
      (1) compile them separately (safe)
      (2) compile them together (risky) *)
      assert(fl' = []);
      stop();
      fl

    (* assumes [CnfUtils.file_sorted cnf] *)
    let compute
          ?(verbose=false)
          ?(bucketize_mode=Tools.ASAP)
          ?(treefy_bucket=false)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
          ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
         (wap:Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output)
         (t:Q.t) (cnf:CnfTypes.system) : output =
      assert(CnfUtils.file_sorted cnf);
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Make.CNF.compute] wap-processing"
      in
      let wap_in  = CnfUtils.Wap.input_of_cnf cnf in
      let out_wap = wap wap_in in
      stop();
      let n = cnf.input in
      let vars = GOPS.array_make_n_bivar t n in
      let fl =
        if treefy_bucket
        then (compute_treefy_bucket ~mode:bucketize_mode t out_wap vars cnf)
        else (cnf.formule ||> (compute_clause t n vars))
      in
      let sys = CSTypes.{
        name = cnf.CSTypes.name;
        input = cnf.CSTypes.input;
        quants = cnf.CSTypes.quants;
        man = t;
        formule = fl;
      } in
      internal_compute ~verbose ~frp_only out_wap sys

    let compute_dummy ?(lvl=0)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
(*        ?(dobpp=true) (* if [dobpp=false] then do not compute back-propagation field of the output *) *)
         (t:Q.t) (cnf:CnfTypes.system) : Q.f =
      let n = cnf.input in
      let vars = GOPS.array_make_n_bivar t n in
      let fl = cnf.formule ||> (compute_clause t n vars) in
      match lvl with
      | 0 -> Q.(&!!) t n fl
      | _ -> failwith "[AQEOPS_CoSTreD.Make.CNF.compute_dummy] this level is not implemented"

  end
end

module MakeCNF(Q:AQEOPS.MSig) =
struct
  module GOPS = OOPS.Make(Q)

  module LoadCnfA = LoadCnf.MakeA(Q)

  module Model : Snowflake.CoSTreD.MSig
    with type t  = Q.t
    and  type f0 = CnfTypes.clause (* sorted *)
    and  type f1 = Q.f
    and  type f2 = Q.f
    and  type f3 = Q.f
  =
  struct
    (* [type t] representation language manager *)
    type t = Q.t (* Q enabled *)
    type f0 = CnfTypes.clause (* sorted *)
    type f1 = Q.f
    type f2 = Q.f
    type f3 = Q.f

    type supp = int list (* not required by MSig *)

    let support t f =
      Q.support t f |> Support.to_sorted_support

    let support0 _ = CnfUtils.support
    let support1 = support
    and support2 = support
    and support3 = support

    let trivial : t -> _ -> bool option = Q.to_bool
    let trivial0 _ = function
      | [] -> Some false
      | _  -> None
    let trivial1 = trivial
    and trivial2 = trivial
    and trivial3 = trivial

    (* Section 1. FRP *)

    let solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 =
      (* print_string "[AQEOPS_RTBF2.solve_variable] begin"; print_newline(); *)
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_variable] GOPS.array_make_n_var"
      in
      let vars = GOPS.array_make_n_bivar t ga in
      stop();
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_variable] LoadCnfA.export_formule"
      in
      let f0 = LoadCnfA.export_formule t vars supp ga f0l in
      stop();
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_variable] list_and"
      in
      let f = Q.(&!!) t ga (f0::f1l) in
      stop();
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_variable] existA"
      in
      let elim_supp = Support.of_sorted_support ga elim in
      let pf = Q.existsA t elim_supp f in
      stop();
      (* print_string "[AQEOPS_RTBF2.solve_variable] end"; print_newline(); *)
      ([pf], f)

    (* Section 2.0 FRP to BPP *)
    (* val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3 *)
    let solve_parameter t (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      let vars = GOPS.array_make_n_bivar t ga in
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_parameter] LoadCnfA.export_formule"
      in
      let f0 = LoadCnfA.export_formule t vars supp ga f0l in
      stop();
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_parameter] list_and"
      in
      let f = Q.(&!!) t ga (f0::f1l) in
      stop();
      f

    (* Section 2. BPP *)
    let backproj (t:t) (ga:int) (f3:f3) (f2:f2) supp3 inter supp2 : f3 =
      let qg = SetList.minus supp3 inter in (* SetList.minus supp3 supp2 *)
      let qg_supp = Support.of_sorted_support ga qg in
      let f3' = Q.existsA t qg_supp f3 in
      Q.(&!) t f3' f2
  end
  module Module = Snowflake.CoSTreD.Make(Model)

  module CNF =
  struct
    open CnfTypes

    type output = {
      out_wap : Snowflake.Wap_exchange.output;
      out_frp : Module.f201 array;
      out_sat : bool;
      out_mid : Q.f array;
      out_bpp : Q.f array;
    }

    let compute
          ?(treefy_bucket=false)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
          ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
         (wap:Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output)
         (t:Q.t) (cnf:CnfTypes.system) : output =
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.compute] wap-processing"
      in
      let wap_in  = CnfUtils.Wap.input_of_cnf cnf in
      let out_wap = wap wap_in in
      stop();
      let n = cnf.input in
      if frp_only
      then (
        match Module.apply_frp t ~check:false n cnf.formule [] out_wap with
        | None -> (
          let ff = Q.cst t false n in
          {
            out_wap;
            out_sat = false;
            out_frp = [| AB.A ff |];
            out_mid = [||];
            out_bpp = [||];
          }
        )
        | Some(out_frp) ->
          {out_wap; out_sat = true; out_frp; out_mid = [||]; out_bpp = [||]}
      )
      else (
        match Module.apply t ~check:false n cnf.formule [] out_wap with
        | None -> (
          let ff = Q.cst t false n in
          {
            out_wap;
            out_frp = [| AB.A ff |];
            out_sat = false;
            out_mid = [| ff |];
            out_bpp = [| ff |];
          }
        )
        | Some(out_frp, out_mid, out_bpp) -> (
          let out_mid = Array.map AB.unB out_mid in
          {out_wap; out_sat = true; out_frp; out_mid; out_bpp}
        )
      )

  end
end

module MakeCNF_D4(Q:AQEOPS.MSig) =
struct
  module GOPS = OOPS.Make(Q)

  module LoadCnfA = LoadCnf.MakeA(Q)
  module OfExpr = OfExprOfOops.Make(Q)
  type man = {
                ldd : Q.t; (* Q enabled *)
    mutable    step : int;
    mutable   total : int;
    mutable verbose : bool;
  }

  module Model : Snowflake.CoSTreD.MSig
    with type t  = man
    and  type f0 = CnfTypes.clause (* sorted *)
    and  type f1 = Q.f
    and  type f2 = Q.f
    and  type f3 = Q.f
  =
  struct
    type t = man
    (* [type t] representation language manager *)
    type f0 = CnfTypes.clause (* sorted *)
    type f1 = Q.f
    type f2 = Q.f
    type f3 = Q.f

    type supp = int list (* not required by MSig *)

    let support t f =
      Q.support t.ldd f |> Support.to_sorted_support

    let support0 _ = CnfUtils.support
    let support1 = support
    and support2 = support
    and support3 = support

    let trivial t : _ -> bool option = Q.to_bool t.ldd
    let trivial0 _ = function
      | [] -> Some false
      | _  -> None
    let trivial1 = trivial
    and trivial2 = trivial
    and trivial3 = trivial

    (* Section 1. FRP *)

    let solve_variable (t:t) (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) (elim:supp) (proj:supp) : (f1 list) * f2 =
      if t.verbose then print_endline ("[AQEOPS_CoSTreD.Model.solve_variable] {step:"^(ToS.int t.step)^"{max:"^(ToS.int t.total)^"}}");
      t.step <- succ t.step;
      (* print_string "[AQEOPS_RTBF2.solve_variable] begin"; print_newline(); *)
      let f0 =
(*        if List.length supp > 50 (* [FIXME] turn it into a parameter *)
        (* if the sub-system is big enoug use D4 *)
        then (
          let stop = OProfile.time_start
            OProfile.default
            "[AQEOPS_CoSTreD.Model.solve_variable] CnfExternal.D4.compute"
          in
          let f0 = OfExpr.import_cnf_formula_d4 t.ldd ga f0l in
          stop();
          f0
        )
        (* otherwise use internal solver *)
        else ( *)
          let stop = OProfile.time_start
            OProfile.default
            "[AQEOPS_CoSTreD.Model.solve_variable] GOPS.array_make_n_var"
          in
          let vars = GOPS.array_make_n_bivar t.ldd ga in
          stop();
          let stop = OProfile.time_start
            OProfile.default
            "[AQEOPS_CoSTreD.Model.solve_variable] LoadCnfA.export_formule"
          in
          let f0 = LoadCnfA.export_formule t.ldd vars supp ga f0l in
          stop();
          f0
        (* ) *)
      in
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_variable] list_and"
      in
      let f = Q.(&!!) t.ldd ga (f0::f1l) in
      stop();
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_variable] existA"
      in
      let elim_supp = Support.of_sorted_support ga elim in
      let pf = Q.existsA t.ldd elim_supp f in
      stop();
      (* print_string "[AQEOPS_RTBF2.solve_variable] end"; print_newline(); *)
      ([pf], f)

    (* Section 2.0 FRP to BPP *)
    (* val solve_parameter : t -> int -> f0 list -> f1 list -> supp -> f3 *)
    let solve_parameter t (ga:int) (f0l:f0 list) (f1l:f1 list) (supp:supp) : f3 =
      if t.verbose then print_endline ("[AQEOPS_CoSTreD.Model.solve_parameter] {step:"^(ToS.int t.step)^"{max:"^(ToS.int t.total)^"}}");
      t.step <- succ t.step;
      let vars = GOPS.array_make_n_bivar t.ldd ga in
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_parameter] LoadCnfA.export_formule"
      in
      let f0 = LoadCnfA.export_formule t.ldd vars supp ga f0l in
      stop();
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.solve_parameter] list_and"
      in
      let f = Q.(&!!) t.ldd ga (f0::f1l) in
      stop();
      f

    (* Section 2. BPP *)
    let backproj (t:t) (ga:int) (f3:f3) (f2:f2) supp3 inter supp2 : f3 =
      if t.verbose then print_endline ("[AQEOPS_CoSTreD.Model.solve_backproj] {step:"^(ToS.int t.step)^"{max:"^(ToS.int t.total)^"}}");
      t.step <- succ t.step;
      let qg = SetList.minus supp3 inter in (* SetList.minus supp3 supp2 *)
      let qg_supp = Support.of_sorted_support ga qg in
      let f3' = Q.existsA t.ldd qg_supp f3 in
      Q.(&!) t.ldd f3' f2
  end
  module Module = Snowflake.CoSTreD.Make(Model)

  module CNF =
  struct
    open CnfTypes

    type output = {
      out_wap : Snowflake.Wap_exchange.output;
      out_frp : Module.f201 array;
      out_sat : bool;
      out_mid : Q.f array;
      out_bpp : Q.f array;
    }

    let compute
          ?(verbose=false)
          ?(treefy_bucket=false)
(*        ?(noqbf=true) (* if [noqbf=true] then check that [cnf] is not a QBF formula *) *)
          ?(frp_only=false) (* if [frp_only=true] then do not compute back-propagation field of the output *)
         (wap:Snowflake.Wap_exchange.input -> Snowflake.Wap_exchange.output)
         (t:Q.t) (cnf:CnfTypes.system) : output =
      let stop = OProfile.time_start
        OProfile.default
        "[AQEOPS_CoSTreD.Model.compute] wap-processing"
      in
      let wap_in  = CnfUtils.Wap.input_of_cnf cnf in
      let out_wap = wap wap_in in
      stop();
      let n = cnf.input in
      let total =
        Array.length out_wap.Snowflake.Wap_exchange.sequence
      in
      let man = Model.{ldd = t; step = 0; total; verbose} in
      if frp_only
      then (
        match Module.apply_frp man ~check:false n cnf.formule [] out_wap with
        | None -> (
          let ff = Q.cst t false n in
          {
            out_wap;
            out_sat = false;
            out_frp = [| AB.A ff |];
            out_mid = [||];
            out_bpp = [||];
          }
        )
        | Some(out_frp) ->
          {out_wap; out_sat = true; out_frp; out_mid = [||]; out_bpp = [||]}
      )
      else (
        match Module.apply man ~check:false n cnf.formule [] out_wap with
        | None -> (
          let ff = Q.cst t false n in
          {
            out_wap;
            out_frp = [| AB.A ff |];
            out_sat = false;
            out_mid = [| ff |];
            out_bpp = [| ff |];
          }
        )
        | Some(out_frp, out_mid, out_bpp) -> (
          let out_mid = Array.map AB.unB out_mid in
          {out_wap; out_sat = true; out_frp; out_mid; out_bpp}
        )
      )

  end
end
