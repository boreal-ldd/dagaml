(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : oops : Ordered OPeratorS
 *
 * module  : OopsGcOfOops : Functor for integrating a built-in GC mechanism
 *)

open GuaCaml
open BTools
open OOPS

module Make(OS:MSig) : MSig
  with type t = unit
  and  type f = OS.t * OS.f
=
struct
  type t = unit
  type f = OS.t * OS.f

  let ( ->> ) () mask ((t, f):f) : f = (t, OS.( ->> ) t mask f)
  let arity () (t, f) = OS.arity t f
  let cneg () b (t, f) = (t, OS.cneg t b f)
  let copy_bop bop (t1, f1) (t2, f2) : f =
    assert(OS.arity t1 f1 = OS.arity t2 f2);
    let t' = OS.newman() in
    let f1' = OS.copy_into t1 [f1] t' |> List.hd in
    let f2' = OS.copy_into t2 [f2] t' |> List.hd in
    let f' : OS.f = bop t' f1' f2' in
    OS.clear_caches t';
    (t', f')

  let ( &! ) () tf1 tf2 : f = copy_bop OS.( &! ) tf1 tf2
  let ( |! ) () tf1 tf2 : f = copy_bop OS.( |! ) tf1 tf2
  let ( ^! ) () tf1 tf2 : f = copy_bop OS.( ^! ) tf1 tf2
  let ( =! ) () tf1 tf2 : f = copy_bop OS.( =! ) tf1 tf2
  let ( *! ) () tf1 tf2 : f = copy_bop OS.( *! ) tf1 tf2

  let cst () b n : f =
    let t = OS.newman () in
    (t, OS.cst t b n)

  let var () b n k : f =
    let t = OS.newman () in
    (t, OS.var t b n k)

  let to_bool () (t, f) : bool option = OS.to_bool t f

  let cofactor () b (t, f) : f = (t, OS.cofactor t b f)

  let id () (t, f) : barray = OS.id t f

  let eq () (t0, f0) (t1, f1) : bool = t0 == t1 && (OS.eq t0 f0 f1)

  let newman () = ()

  let copy_into () (fl:f list) () : f list = fl

  let regroup ?(destruct=false) (fl:f list) : OS.t * OS.f list =
    let t' = OS.newman() in
    let fl' = Tools.map (fun (t, f) -> OS.copy_into t [f] t' |> List.hd) fl in
    (t', fl')

  let to_barray ?(nocopy=false) ?(destruct=false) () (fl:f list) : BTools.BArray.t =
    let t', fl' = regroup ~destruct fl in
    OS.to_barray ~nocopy:true ~destruct:true t' fl'

  let dispatch (t:OS.t) (fl:OS.f list) : f list =
    Tools.map (fun f ->
      let tf = OS.newman() in
      let f' = OS.copy_into t [f] tf |> List.hd in
      (tf, f')) fl

  let of_barray ?(t=(None:(t option))) (ba:BTools.BArray.t) : t * f list =
    let t, fl = OS.of_barray ba in
    ((), dispatch t fl)

  let bw_fl ?(nocopy=false) ?(destruct=false) () cha fl : unit =
    let t', fl' = regroup ~destruct fl in
    OS.bw_fl ~nocopy:true ~destruct:true t' cha fl'

  let br_fl ?(t=(None:(t option))) cha : t * f list =
    let t, fl = OS.br_fl cha in
    ((), dispatch t fl)

  let t_stats _ =
    Tree.Leaf "[OopsGcOfOops.t_stats] not_implemented_yet"
  let f_stats _ _ =
    Tree.Leaf "[OopsGcOfOops.f_stats] not_implemented_yet"
  let clear_caches () : unit = ()
  let keep_clean ?(normalize=false) () (fl:f list) : f list = fl

  (* let combine ?(level=0) inpa out = OS.combine ~level inpa out (* [OLD) *) *)

  let check () (t, f) = OS.check t f

  module F =
  struct
    type f' = OS.F.f'

    let prefix = "biggy_"
    let suffix = ".auto.f.pure"

    let arity = OS.F.arity
    let size  = OS.F.size

    let of_f ?(nocopy=false) ?(normalize=true) () ((t, f):f) : f' =
      OS.F.of_f ~nocopy ~normalize t f

    let to_f () (f':f') : f =
      let t = OS.newman() in
      let f = OS.F.to_f t f' in
      (t, f)

    let free (f':f') : unit = OS.F.free f'

    let tof ?(normalize=true) cha fl = OS.F.tof ~normalize cha fl
    let off cha = OS.F.off cha
  end
  let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) () cha fl =
    let t, fl = regroup ~destruct fl in
    OS.tof ~normalize:true ~destruct:false t cha fl
  let off cha =
    let t, fl = OS.off cha in
    ((), dispatch t fl)
end
