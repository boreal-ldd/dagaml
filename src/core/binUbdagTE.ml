(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : core    : manipulation of abstract DAG
 *
 * module  : BinUbdagTE : unique binarised DAG + composable edges + evalution propagation
 *)

open STools
open BTools
open Extra
open O3Extra

module type MODELE =
sig
  module M : BinUbdag.MODELE

  type peval

  val o3s_peval : peval IoB.t

  type 'i link = peval option * 'i
  type 'i next'' = 'i link M.next'
  type 'i edge'' = 'i link M.edge'
  type 'i node'' = 'i link M.node'
  type 'i tree'' = ('i next'', M.edge, M.node) GTree.edge

  val o3s_next'' : ('i IoB.t) -> 'i next'' IoB.t
  val o3s_edge'' : ('i IoB.t) -> 'i edge'' IoB.t
  val o3s_node'' : ('i IoB.t) -> 'i node'' IoB.t
  val o3s_tree'' : ('i IoB.t) -> 'i tree'' IoB.t

  val eval_edge : peval -> 'i edge'' ->  'i edge''
  val eval_node : peval -> 'i node'' -> ('i edge'', 'i node'') Utils.merge

  val push_node : 'i node'' -> 'i tree''

  val compose : M.edge -> 'i M.edge' -> 'i M.edge'

end

module type MODULE_SIG =
sig
  module M : MODELE
  module G : BinUbdag.MODULE_SIG with
        type M.leaf = M.M.leaf
    and type M.edge = M.M.edge
    and type M.node = M.M.node

  type link = M.peval option * G.ident
  type next'' = G.ident M.next''
  type edge'' = G.ident M.edge''
  type node'' = G.ident M.node''
  type tree'' = G.ident M.tree''

  type manager

  val makeman : int -> manager
  val newman : unit -> manager

  val dump_stats : manager -> Tree.stree

  val push : manager -> G.node' -> G.edge'
  val pull : manager -> G.ident -> G.node'

  val dump : manager -> G.edge' list -> Tree.stree
  val load : Tree.stree  -> manager * G.edge' list

  val eval : manager -> M.peval -> G.edge' -> G.edge'

  val export : manager -> G.manager
  val import : G.manager -> manager
end

module MODULE(Model:MODELE) =
struct
  module M = Model
  module G = BinUbdag.MODULE(Model.M)

  type link = M.peval option * G.ident
  type next'' = G.ident M.next''
  type edge'' = G.ident M.edge''
  type node'' = G.ident M.node''
  type tree'' = G.ident M.tree''

  let o3s_next'' = M.o3s_next'' G.o3s_ident
  let o3s_edge'' = M.o3s_edge'' G.o3s_ident
  let o3s_node'' = M.o3s_node'' G.o3s_ident
  let o3s_tree'' = M.o3s_tree'' G.o3s_ident

    type manager = {
    man : G.manager;
    mem : (M.peval * G.ident, BArray.t, G.edge', BArray.t) MemoBTable.t;
    eval : M.peval -> G.edge' -> G.edge';
    push : G.node' -> G.edge';
  }

  let export man = man.man

  let import' hsize man =
    let o3sA = IoB.closure (M.o3s_peval +* G.o3s_ident) in
    let mem, apply = MemoBTable.make o3sA G.o3b_edge' hsize in
    (*mem.MemoBTable.each <- (fun a aa b bb ->
      print_string "##";
      print_string (BArray.to_hexa_string aa);
      print_string " ";
      print_string (BArray.to_hexa_string bb);
      print_newline()
    );*)
    let rec push node : G.edge' = read_edge (M.push_node (Utils.pnode_of_node node))
    and     eval peval (edge : G.edge') : G.edge' =
      goup_edge' (M.eval_edge peval (Utils.pedge_of_edge edge))
    and     goup_edge ((edge, next) : edge'') : bool * G.edge' = match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (false, (edge, Tree.GLink ident))
        | Some peval -> (true, M.compose edge (eval_ident peval ident))
    and     goup_edge' edge : G.edge' = goup_edge edge |> snd
    and     eval_ident peval ident = apply (fun (peval, ident) ->
      let node = G.pull man ident |> Utils.pnode_of_node in
      match M.eval_node peval node with
      | Utils.MEdge edge -> goup_edge' edge
      | Utils.MNode (node, edge0, edge1) ->
        push (node, goup_edge' edge0, goup_edge' edge1)
    ) (peval, ident)
    and     goup_node edge (node, edge0, edge1) =
      let t0, edge0 = goup_edge edge0
      and t1, edge1 = goup_edge edge1 in
      let node = (node, edge0, edge1) in
      if t0||t1
      then (M.compose edge (push node))
      else (edge, Tree.GLink (G.push man node))
    and     read_edge (edge, next) = match next with
      | GTree.Leaf next -> goup_edge' (edge, next)
      | GTree.Node node -> goup_node   edge (read_node node |> Utils.pnode_of_node)
    and     read_node (node, edge0, edge1) =
      (node, read_edge edge0, read_edge edge1)
    in
    {man; mem; push; eval}

  let makeman hsize =
    let man = G.makeman hsize in
    import' hsize man

    let default_newman_hsize = 10000

    let newman () = makeman default_newman_hsize

  let import = import' default_newman_hsize

  let dump_stats man = Tree.Node [
    G.dump_stats man.man;
    MemoBTable.dump_stats man.mem;
  ]

  let dump_estats man edges = Tree.Node [
    G.dump_estats man.man edges;
    MemoBTable.dump_stats man.mem;
  ]

  let push man = man.push
  let pull man ident = G.pull man.man ident

  let dump man = G.dump man.man
  let load stree =
    let man, edges = G.load stree in
    (import man, edges)

  let eval man = man.eval
end

module MODULE_CACHED(Model:MODELE) =
struct
  module M = Model
  module G = BinUbdag.MODULE(Model.M)

  type link = M.peval option * G.ident
  type next'' = G.ident M.next''
  type edge'' = G.ident M.edge''
  type node'' = G.ident M.node''
  type tree'' = G.ident M.tree''

  let o3s_next'' = M.o3s_next'' G.o3s_ident
  let o3s_edge'' = M.o3s_edge'' G.o3s_ident
  let o3s_node'' = M.o3s_node'' G.o3s_ident
  let o3s_tree'' = M.o3s_tree'' G.o3s_ident

    type manager = {
    man : G.manager;
    mem : (M.peval * G.ident, BArray.t, G.edge', BArray.t) Hashcache.Conv.t;
    eval : M.peval -> G.edge' -> G.edge';
    push : G.node' -> G.edge';
  }

  let export man = man.man

  let import' hsize man =
    let o3sA = IoB.closure (M.o3s_peval +* G.o3s_ident) in
    let mem, apply = Hashcache.Conv.make o3sA G.o3b_edge' hsize in
    let rec push node : G.edge' = read_edge (M.push_node (Utils.pnode_of_node node))
    and     eval peval (edge : G.edge') : G.edge' =
      goup_edge' (M.eval_edge peval (Utils.pedge_of_edge edge))
    and     goup_edge ((edge, next) : edge'') : bool * G.edge' = match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (false, (edge, Tree.GLink ident))
        | Some peval -> (true, M.compose edge (eval_ident peval ident))
    and     goup_edge' edge : G.edge' = goup_edge edge |> snd
    and     eval_ident peval ident = apply (fun (peval, ident) ->
      let node = G.pull man ident |> Utils.pnode_of_node in
      match M.eval_node peval node with
      | Utils.MEdge edge -> goup_edge' edge
      | Utils.MNode (node, edge0, edge1) ->
        push (node, goup_edge' edge0, goup_edge' edge1)
    ) (peval, ident)
    and     goup_node edge (node, edge0, edge1) =
      let t0, edge0 = goup_edge edge0
      and t1, edge1 = goup_edge edge1 in
      let node = (node, edge0, edge1) in
      if t0||t1
      then (M.compose edge (push node))
      else (edge, Tree.GLink (G.push man node))
    and     read_edge (edge, next) = match next with
      | GTree.Leaf next -> goup_edge' (edge, next)
      | GTree.Node node -> goup_node   edge (read_node node |> Utils.pnode_of_node)
    and     read_node (node, edge0, edge1) =
      (node, read_edge edge0, read_edge edge1)
    in
    {man; mem; push; eval}

  let makeman hsize =
    let man = G.makeman hsize in
    import' hsize man

    let default_newman_hsize = 10000

    let newman () = makeman default_newman_hsize

  let import = import' default_newman_hsize

    let dump_stats man = Tree.Node [
    G.dump_stats man.man;
    Hashcache.Conv.dump_stats man.mem;
  ]

  let push man = man.push
  let pull man ident = G.pull man.man ident

  let dump man = G.dump man.man
  let load stree =
    let man, edges = G.load stree in
    (import man, edges)

  let eval man = man.eval

end

module type REWRITE_MODELE =
sig
  module M : MODULE_SIG

  type leaf = M.M.M.leaf
  type edge = M.M.M.edge
  type node = M.M.M.node

  type ident = M.G.ident
  type next' = M.G.next'
  type edge' = M.G.edge'
  type node' = M.G.node'

  type next'' = M.next''
  type edge'' = M.edge''
  type node'' = M.node''
  type tree'' = M.tree''

  val rewrite : node'' -> tree''
end

module REWRITE(Model:REWRITE_MODELE) =
struct

  type manager = {
    man : Model.M.manager;
    memR : (Model.ident, Model.ident, Model.edge', BArray.t) MemoBTable.t;
    memE : (Model.M.M.peval * Model.ident, BArray.t, Model.edge', BArray.t) MemoBTable.t;
    map : Model.M.M.peval option -> Model.edge' -> Model.edge';
  }

  let dump_stats man = Tree.Node [
    Tree.Node [ Tree.Leaf "memR:"; MemoBTable.dump_stats man.memR];
    Tree.Node [ Tree.Leaf "memE:"; MemoBTable.dump_stats man.memE];
  ]

  let makeman man hsize =
    let o3sB = Model.M.G.o3b_edge' in
    let memR, applyR = MemoBTable.make O3.id o3sB hsize in
    let o3sA = IoB.closure Model.M.(M.o3s_peval +* G.o3s_ident) in
    let memE, applyE = MemoBTable.make  o3sA o3sB hsize in
    let compose = Model.M.M.compose in
    let push : Model.node' -> Model.edge' = Model.M.push man
    and pull : Model.ident -> Model.node' = Model.M.pull man in
    (* goup = go up *)
    let rec goup_edge ((edge, next) : Model.edge'') : bool * Model.edge'= match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (false, (edge, Tree.GLink ident))
        | Some peval -> (true, compose edge (eval_ident peval ident))
    and     eval_ident peval ident : Model.edge' = applyE (fun (peval, ident) ->
      let node = pull ident in
      match Model.M.M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> goup_edge edge |> snd
      | Utils.MNode node -> goup_node node
    ) (peval, ident)
    and     goup_node (node, edge0, edge1) : Model.edge' =
      let t0, edge0 = goup_edge edge0
      and t1, edge1 = goup_edge edge1 in
      let node : Model.node' = (node, edge0, edge1) in
      if t0||t1
      then (rewr node)
      else (push node)
    and     rewr (node : Model.node') : Model.edge' = read_edge (Model.rewrite (Utils.pnode_of_node node))
    and     read_edge (edge, node) : Model.edge' = match node with
      | GTree.Leaf (next : Model.next'') -> goup_edge ((edge, next) : Model.edge'') |> snd
      | GTree.Node (node, edge0, edge1) ->
        goup_node (Utils.pnode_of_node (node, read_edge edge0, read_edge edge1))
    in
    let rec down_ident ident : Model.edge' = applyR (fun ident ->
      let (node, edge0, edge1) = pull ident in
      rewr (node, down_edge edge0, down_edge edge1)
    ) ident
    and     down_edge ((edge, next) : Model.edge') : Model.edge' = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink ident -> compose edge (down_ident ident)
    in
    let map opeval edge : Model.edge' =
      let edge = match opeval with
        | None -> edge
        | Some peval -> goup_edge (Model.M.M.eval_edge peval (Utils.pedge_of_edge edge)) |> snd
      in
      down_edge edge
    in
    {man; memR; memE; map}

  let default_newman_hsize = 10000

  let newman man = makeman man default_newman_hsize
end

module REWRITE_CACHED(Model:REWRITE_MODELE) =
struct

  type manager = {
    man : Model.M.manager;
    memR : (Model.ident, Model.ident, Model.edge', BArray.t) Hashcache.Conv.t;
    memE : (Model.M.M.peval * Model.ident, BArray.t, Model.edge', BArray.t) Hashcache.Conv.t;
    map : Model.M.M.peval option -> Model.edge' -> Model.edge';
  }

  let dump_stats man = Tree.Node [
    Tree.Node [ Tree.Leaf "memR:"; Hashcache.Conv.dump_stats man.memR];
    Tree.Node [ Tree.Leaf "memE:"; Hashcache.Conv.dump_stats man.memE];
  ]

  let makeman man hsize =
    let o3sB = Model.M.G.o3b_edge' in
    let memR, applyR = Hashcache.Conv.make O3.id o3sB hsize in
    let o3sA = IoB.closure Model.M.(M.o3s_peval +* G.o3s_ident) in
    let memE, applyE = Hashcache.Conv.make  o3sA o3sB hsize in
    let compose = Model.M.M.compose in
    let push : Model.node' -> Model.edge' = Model.M.push man
    and pull : Model.ident -> Model.node' = Model.M.pull man in
    (* goup = go up *)
    let rec goup_edge ((edge, next) : Model.edge'') : bool * Model.edge'= match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (false, (edge, Tree.GLink ident))
        | Some peval -> (true, compose edge (eval_ident peval ident))
    and     eval_ident peval ident : Model.edge' = applyE (fun (peval, ident) ->
      let node = pull ident in
      match Model.M.M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> goup_edge edge |> snd
      | Utils.MNode node -> goup_node node
    ) (peval, ident)
    and     goup_node (node, edge0, edge1) : Model.edge' =
      let t0, edge0 = goup_edge edge0
      and t1, edge1 = goup_edge edge1 in
      let node : Model.node' = (node, edge0, edge1) in
      if t0||t1
      then (rewr node)
      else (push node)
    and     rewr (node : Model.node') : Model.edge' = read_edge (Model.rewrite (Utils.pnode_of_node node))
    and     read_edge (edge, node) : Model.edge' = match node with
      | GTree.Leaf (next : Model.next'') -> goup_edge ((edge, next) : Model.edge'') |> snd
      | GTree.Node (node, edge0, edge1) ->
        goup_node (Utils.pnode_of_node (node, read_edge edge0, read_edge edge1))
    in
    let rec down_ident ident : Model.edge' = applyR (fun ident ->
      let (node, edge0, edge1) = pull ident in
      rewr (node, down_edge edge0, down_edge edge1)
    ) ident
    and     down_edge ((edge, next) : Model.edge') : Model.edge' = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink ident -> compose edge (down_ident ident)
    in
    let map opeval edge : Model.edge' =
      let edge = match opeval with
        | None -> edge
        | Some peval -> goup_edge (Model.M.M.eval_edge peval (Utils.pedge_of_edge edge)) |> snd
      in
      down_edge edge
    in
    {man; memR; memE; map}

  let default_newman_hsize = 10000

  let newman man = makeman man default_newman_hsize

end

module IMPORT(Model:MODULE_SIG) =
struct
  module G = Model

  module MODELE =
  struct
    module M = G.G

    type extra  = G.manager
    type xnode  = M.edge'
    type xnode' = BArray.t
    type xedge  = M.edge'

    let o3_xnode = M.o3b_edge'

    type next' = (unit -> xnode) M.M.next'
    type edge' = (unit -> xnode) M.M.edge'
    type node' = (unit -> xnode) M.M.node'

    let rec_edge (edge, next) : M.edge' = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink node -> G.M.compose edge (node())

    let map_node man (node, edge0, edge1) : M.edge' =
      G.push man (node, rec_edge edge0, rec_edge edge1)

    let map_edge man edge : M.edge' = rec_edge edge

  end

  include BinUbdag.EXPORT(MODELE)

end

module STDIO(M1:MODELE) =
struct
  module G1 = MODULE(M1)
  module G0 = G1.G

  module REMAN = BinUbdag.REMAN(G0)
  module IMPORT = IMPORT(G1)

  let to_stree man edges =
    let ubdag = G1.export man in
    let ubdag' = G0.newman () in
    let man = REMAN.newman ubdag ubdag' in
    let map = REMAN.map_edge man in
    let edges' = Tools.map map edges in
    G0.dump ubdag' edges'

  let of_stree stree =
    let ubdag', edges' = G0.load stree in
    let grobdd = G1.newman () in
    let man = IMPORT.newman ubdag' grobdd in
    let map = IMPORT.rec_edge man in
    let edges = Tools.map map edges' in
    (grobdd, edges)

  let to_file man edges target =
    OfSTree.file [to_stree man edges] target

  let of_file target =
    match ToSTree.file target with
      | [] -> assert false
      | objet::_ -> of_stree objet
end
