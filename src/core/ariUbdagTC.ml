(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : core    : manipulation of abstract DAG
 *
 * module  : AriUbdagTC : arity-aware unique binarised DAG + composable edges + shannon normalization
 *)
open GuaCaml
open STools
open Extra
open BTools
open O3Extra

module type MSig =
sig
  module M : AriUbdag.MSig with
    type node = unit

  val arity : 'i M.edge' -> int

  val push_node : 'i M.node' -> M.edge * ('i M.next', 'i M.node') Utils.merge
  val pull_node : 'i M.edge' -> ('i M.node', 'i * ('i M.node' -> 'i M.node')) Utils.merge

  val compose : M.edge -> 'i M.edge' -> 'i M.edge'
end

module type Sig =
sig
  module M : MSig
  module G : AriUbdag.Sig with
        type M.leaf = M.M.leaf
    and type M.edge = M.M.edge
    and type M.node = M.M.node

  type manager

  val makeman : int -> manager
  val newman : unit -> manager

  val dump_stats : manager -> Tree.stree

  val push : manager -> G.node' -> G.edge'
  val pull : manager -> G.edge' -> G.node'

  val export : manager -> G.manager

  val copy_into : manager -> manager -> G.edge' -> G.edge'
  val copy_list_into : manager -> manager -> G.edge' list -> G.edge' list
end

module Make(Model:MSig) =
struct
  module M = Model
  module G = AriUbdag.Make(Model.M)

  type manager = G.manager

  let makeman = G.makeman

  let newman  = G.newman

  let dump_stats = G.dump_stats
  let dump_estats = G.dump_estats

  let push man node' =
    let edge, merge = Model.push_node node' in
    (edge, match merge with
    | Utils.MEdge next' -> next'
    | Utils.MNode node' -> Tree.GLink (G.push man node'))
  let pull man edge' =
    match Model.pull_node edge' with
    | Utils.MEdge node' -> node'
    | Utils.MNode (ident, func) -> func (G.pull man ident)

  let dump = G.dump
  let load = G.load

  let export man = man

  let copy_into t1 t2 : G.edge' -> G.edge' =
    if t1 == t2 then (fun e -> e)
    else (
      let mem, apply = MemoTable.(make default_size) in
      let rec map_next edge = function
        | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
        | Tree.GLink link -> M.compose edge (map_rec link)
      and     map_edge (ee, nx) =
        map_next ee nx
      and     map_link link =
        let tag, e0, e1 = G.pull t1 link in
        let e0' = map_edge e0
        and e1' = map_edge e1 in
        push t2 (tag, e0', e1')
      and     map_rec  link =
        apply map_link link
      in map_edge
      (* [DEBUG]
      (fun e ->
        print_endline "[AriUbdagTC.copy_into] ante:traverse";
        assert(G.traverse t1 e);
        print_endline "[AriUbdagTC.copy_into] begin";
        let e' = map_edge e in
        print_endline "[AriUbdagTC.copy_into] post:traverse";
        assert(G.traverse t2 e');
        print_endline "[AriUbdagTC.copy_into] end";
        e'
      ) *)
    )

  let copy_list_into t1 t2 : G.edge' list -> G.edge' list =
    if t1 == t2 then (fun fl -> fl) else (
      let copy_edge = copy_into t1 t2 in
      Tools.map copy_edge
    )

end

module type BINOP_MSig =
sig
  module M : Sig

  val solver : M.G.node' -> M.M.M.edge * (M.G.next', M.G.node', M.G.node') Utils.merge3

end

module BINOP(Model:BINOP_MSig) =
struct

  module M = Model

  type manager = {
    man : Model.M.manager;
    mem : (Model.M.G.node', BArray.t, Model.M.G.edge', BArray.t) MemoBTable.t;
    map : Model.M.G.node' -> Model.M.G.edge';
  }

  type t = Model.M.manager
  type tt = manager

  let makeman man hsize =
    let mem, apply = MemoBTable.make Model.M.G.o3b_node' Model.M.G.o3b_edge' hsize in
    let push = Model.M.push man
    and pull = Model.M.pull man
    and compose = Model.M.M.compose in
    let rec calc_node ((), edge'X, edge'Y) =
      let edge, merge3 = Model.solver ((), edge'X, edge'Y) in
      match merge3 with
      | Utils.M3Edge next' -> (edge, next')
      | Utils.M3Cons node' -> compose edge (push node')
      | Utils.M3Node node' -> compose edge (apply calc_rec node')
    and     calc_rec ((), edgeX, edgeY) =
      let ((), edgeX0, edgeX1) = pull edgeX
      and ((), edgeY0, edgeY1) = pull edgeY in
      let edge'0 = calc_node ((), edgeX0, edgeY0)
      and edge'1 = calc_node ((), edgeX1, edgeY1) in
      push ((), edge'0, edge'1)
    in {man; mem; map = calc_node}

    let newman man = makeman man MemoBTable.default_size

    let dump_stats man = MemoBTable.dump_stats man.mem

  let map man = man.map

  let solve man f g = map man ((), f, g)

  let clear man : unit = MemoBTable.clear man.mem
end

module BINOP_CACHED(Model:BINOP_MSig) =
struct

  module M = Model

  type manager = {
    man : Model.M.manager;
    mem : (Model.M.G.node', BArray.t, Model.M.G.edge', BArray.t) Hashcache.Conv.t;
    map : Model.M.G.node' -> Model.M.G.edge';
  }

  type t = Model.M.manager
  type tt = manager

  let makeman man hsize =
    let mem, apply = Hashcache.Conv.make Model.M.G.o3b_node' Model.M.G.o3b_edge' hsize in
    let push = Model.M.push man
    and pull = Model.M.pull man
    and compose = Model.M.M.compose in
    let rec calc_node ((), edge'X, edge'Y) =
      let edge, merge3 = Model.solver ((), edge'X, edge'Y) in
      match merge3 with
      | Utils.M3Edge next' -> (edge, next')
      | Utils.M3Cons node' -> compose edge (push node')
      | Utils.M3Node node' -> compose edge (apply calc_rec node')
    and     calc_rec ((), edgeX, edgeY) =
      let ((), edgeX0, edgeX1) = pull edgeX
      and ((), edgeY0, edgeY1) = pull edgeY in
      let edge'0 = calc_node ((), edgeX0, edgeY0)
      and edge'1 = calc_node ((), edgeX1, edgeY1) in
      push ((), edge'0, edge'1)
    in {man; mem; map = calc_node}

  let newman man = makeman man Hashcache.default_size

  let dump_stats man = Hashcache.Conv.dump_stats man.mem

  let map man = man.map

  let solve man f g = map man ((), f, g)

  let clear man : unit = Hashcache.Conv.clear man.mem
end

module type PEvalR_MSig =
sig
  module M : Sig

  type peval

  val iob_peval : peval IoB.t

  type next = peval option * M.G.ident

  type next' = next M.M.M.next'
  type edge' = next M.M.M.edge'
  type node' = next M.M.M.node'

  val eval_edge : peval -> edge' ->  edge'
  val eval_node : peval -> node' -> (edge', node') Utils.merge

end

module type PEvalR_Sig =
sig
  module M : PEvalR_MSig

  type manager

  val rec_edge : manager -> M.edge' -> M.M.G.edge'
  val rec_node : manager -> M.peval -> M.M.G.ident -> M.M.G.edge'

  val map_edge : manager -> M.peval -> M.M.G.edge' -> M.M.G.edge'
  val map_node : manager -> M.peval -> M.M.G.node' -> M.M.G.edge'

  val dump_stats : manager -> Tree.stree

end

module PEvalR(Model:PEvalR_MSig) =
struct
  module M = Model

  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.ident, BArray.t, M.M.G.edge', BArray.t) MemoBTable.t;
    rec_edge : M.edge' -> M.M.G.edge';
    rec_node : M.peval -> M.M.G.ident -> M.M.G.edge';
    map_edge : M.peval -> M.M.G.edge' -> M.M.G.edge';
    map_node : M.peval -> M.M.G.node' -> M.M.G.edge';
  }

  type t = M.M.manager
  type tt = manager

  let dump_stats man = MemoBTable.dump_stats man.mem

  let makeman man hsize =
    let iobA = IoB.closure (M.iob_peval +* M.M.G.iob_ident)
    and iobB = M.M.G.o3b_edge' in
    let mem, apply = MemoBTable.make iobA iobB hsize in
    let compose = M.M.M.compose in
    let pull =
      let man = M.M.export man in
      M.M.G.pull man
    in
    let rec rec_edge  (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (edge, Tree.GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (pull ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> rec_edge edge
      | Utils.MNode (node, edge0, edge1) ->
        M.M.push man (node, rec_edge edge0, rec_edge edge1)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

    let newman man = makeman man MemoBTable.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

  let clear man : unit = MemoBTable.clear man.mem
end

module PEvalR_CACHED(Model:PEvalR_MSig) =
struct
  module M = Model

  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.ident, BArray.t, M.M.G.edge', BArray.t) Hashcache.Conv.t;
    rec_edge : M.edge' -> M.M.G.edge';
    rec_node : M.peval -> M.M.G.ident -> M.M.G.edge';
    map_edge : M.peval -> M.M.G.edge' -> M.M.G.edge';
    map_node : M.peval -> M.M.G.node' -> M.M.G.edge';
  }

  type t = M.M.manager
  type tt = manager

  let dump_stats man = Hashcache.Conv.dump_stats man.mem

  let makeman man hsize =
    let iobA = IoB.closure (M.iob_peval +* M.M.G.iob_ident)
    and iobB = M.M.G.o3b_edge' in
    let mem, apply = Hashcache.Conv.make iobA iobB hsize in
    let compose = M.M.M.compose in
    let pull =
      let man = M.M.export man in
      M.M.G.pull man
    in
    let rec rec_edge  (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (edge, Tree.GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (pull ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> rec_edge edge
      | Utils.MNode (node, edge0, edge1) ->
        M.M.push man (node, rec_edge edge0, rec_edge edge1)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

  let newman man = makeman man Hashcache.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

  let clear man : unit = Hashcache.Conv.clear man.mem
end

module type EBINOP_MSig =
sig
  module M : PEvalR_Sig

  val solver : M.M.node' -> M.M.M.M.M.edge * (M.M.next', M.M.node', M.M.node') Utils.merge3
end

module EBINOP(Model:EBINOP_MSig) =
struct
  module M = Model

  type manager = {
    man : M.M.M.M.manager;
    eva : M.M.manager;
    mem : (M.M.M.M.G.node', BArray.t, M.M.M.M.G.edge', BArray.t) MemoBTable.t;
    map : M.M.M.M.G.node' -> M.M.M.M.G.edge';
  }

  type t =  M.M.M.M.manager
  type te = M.M.manager
  type tt = manager

  let makeman man eva hsize =
    let mem, apply = M.M.M.M.G.(MemoBTable.make o3b_node' o3b_edge') hsize in
    let compose = M.M.M.M.M.compose
    and push = M.M.M.M.push man
    and pull = M.M.M.M.pull man
    and eval = M.M.rec_node eva in
    let rec eva_edge (edge, next) = match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink(None, node) -> (false, (edge, Tree.GLink node))
      | Tree.GLink(Some peval, node) -> (true, compose edge (eval peval node))
    and     eva_edge' edge = eva_edge edge |> snd
    and     eva_node' (node, edge0, edge1) =
      (node, eva_edge' edge0, eva_edge' edge1)
    and     eva_node (node, edge0, edge1) =
      let t0, edge0 = eva_edge edge0
      and t1, edge1 = eva_edge edge1 in
      (t0||t1, (node, edge0, edge1))
    and     rec_node node =
      let edge, merge = M.solver(Utils.pnode_of_node node) in
      match merge with
      | Utils.M3Edge next -> eva_edge' (edge, next)
      | Utils.M3Cons node -> compose edge (push(eva_node' node))
      | Utils.M3Node node ->
      let t, node = eva_node node in
      compose edge (if t then (rec_node node) else
      (apply (fun ((), x, y) -> (
        let ((), x0, x1) = pull x
        and ((), y0, y1) = pull y in
        let xy0 = rec_node ((), x0, y0)
        and xy1 = rec_node ((), x1, y1) in
        push ((), xy0, xy1)
      )) node))
    in
    {man; eva; mem; map = rec_node}

  let newman man extra = makeman man extra MemoBTable.default_size

  let map man = man.map

  let dump_stats man = MemoBTable.dump_stats man.mem

  let solve man f g = map man ((), f, g)

  let clear man : unit = MemoBTable.clear man.mem
end

module EBINOP_CACHED(Model:EBINOP_MSig) =
struct
  module M = Model

  type manager = {
    man : M.M.M.M.manager;
    eva : M.M.manager;
    mem : (M.M.M.M.G.node', BArray.t, M.M.M.M.G.edge', BArray.t) Hashcache.Conv.t;
    map : M.M.M.M.G.node' -> M.M.M.M.G.edge';
  }

  type t =  M.M.M.M.manager
  type te = M.M.manager
  type tt = manager

  let makeman man eva hsize =
    let mem, apply = M.M.M.M.G.(Hashcache.Conv.make o3b_node' o3b_edge') hsize in
    let compose = M.M.M.M.M.compose
    and push = M.M.M.M.push man
    and pull = M.M.M.M.pull man
    and eval = M.M.rec_node eva in
    let rec eva_edge (edge, next) = match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink(None, node) -> (false, (edge, Tree.GLink node))
      | Tree.GLink(Some peval, node) -> (true, compose edge (eval peval node))
    and     eva_edge' edge = eva_edge edge |> snd
    and     eva_node' (node, edge0, edge1) =
      (node, eva_edge' edge0, eva_edge' edge1)
    and     eva_node (node, edge0, edge1) =
      let t0, edge0 = eva_edge edge0
      and t1, edge1 = eva_edge edge1 in
      (t0||t1, (node, edge0, edge1))
    and     rec_node node =
      let edge, merge = M.solver(Utils.pnode_of_node node) in
      match merge with
      | Utils.M3Edge next -> eva_edge' (edge, next)
      | Utils.M3Cons node -> compose edge (push(eva_node' node))
      | Utils.M3Node node ->
      let t, node = eva_node node in
      compose edge (if t then (rec_node node) else
      (apply (fun ((), x, y) -> (
        let ((), x0, x1) = pull x
        and ((), y0, y1) = pull y in
        let xy0 = rec_node ((), x0, y0)
        and xy1 = rec_node ((), x1, y1) in
        push ((), xy0, xy1)
      )) node))
    in
    {man; eva; mem; map = rec_node}

  let newman man extra = makeman man extra Hashcache.default_size

  let map man = man.map

  let dump_stats man = Hashcache.Conv.dump_stats man.mem

  let solve man f g = map man ((), f, g)

  let clear man : unit = Hashcache.Conv.clear man.mem
end

module IMPORT(Model:Sig) =
struct
  module G = Model

  module MSig =
  struct
    module M = G.G

    type extra  = G.manager
    type xnode  = M.edge'
    type xnode' = BArray.t
    type xedge  = M.edge'

    let o3_xnode = M.o3b_edge'

    let rec_edge (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink node -> G.M.compose edge (node())

    let map_node man (node, edge0, edge1) =
      G.push man (node, rec_edge edge0, rec_edge edge1)

    let map_edge man edge = rec_edge edge

  end

  include AriUbdag.EXPORT(MSig)

end

module IMPORT_CACHED(Model:Sig) =
struct
  module G = Model

  module MSig =
  struct
    module M = G.G

    type extra  = G.manager
    type xnode  = M.edge'
    type xnode' = BArray.t
    type xedge  = M.edge'

    let o3_xnode = M.o3b_edge'

    let rec_edge (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink node -> G.M.compose edge (node())

    let map_node man (node, edge0, edge1) =
      G.push man (node, rec_edge edge0, rec_edge edge1)

    let map_edge man edge = rec_edge edge

  end

  include AriUbdag.EXPORT_CACHED(MSig)

end

module STDIO(M1:MSig) =
struct
  module G1 = Make(M1)
  module G0 = G1.G

  module REMAN = AriUbdag.REMAN(G0)
  module IMPORT = IMPORT(G1)

  let to_stree man edges =
    let ubdag = G1.export man in
    let ubdag' = G0.newman () in
    let man = REMAN.newman ubdag ubdag' in
    let map = REMAN.map_edge man in
    let edges' = Tools.map map edges in
    G0.dump ubdag' edges'

  let of_stree ?(man=None) stree =
    let ubdag', edges' = G0.load stree in
    let grobdd = match man with
      | Some man -> man
      | None     -> G1.newman ()
    in
    let man = IMPORT.newman ubdag' grobdd in
    let map = IMPORT.rec_edge man in
    let edges = Tools.map map edges' in
    (grobdd, edges)

  let to_file man edges target =
    OfSTree.file [to_stree man edges] target

  let of_file ?(man=None) target =
    match ToSTree.file target with
      | [] -> assert false
      | objet::_ -> of_stree ~man:man objet
end

module type EXPORT_MSig =
sig
  module M : Sig

  type xedge
  type xedge'
  type extra

  val o3_xedge : (xedge, xedge') O3.o3

  val map_edge: extra -> (M.G.edge' -> xedge) -> M.G.edge' -> xedge
  val push:     extra -> unit * xedge * xedge -> xedge

end

module EXPORT(M:EXPORT_MSig) =
struct

  type manager = {
    man : M.M.manager;
    extra : M.extra;
    mem : (M.M.G.edge', BArray.t, M.xedge, M.xedge') MemoBTable.t;
    map_edge : M.M.G.edge' -> M.xedge;
  }

  let dump_stats man = MemoBTable.dump_stats man.mem

  let makeman man extra hsize =
    let mem, apply = MemoBTable.make M.M.G.o3b_edge' M.o3_xedge hsize in
    let rec map_edge edge = apply (M.map_edge extra rec_edge) edge
    and     rec_edge edge =
      let ((), edge0, edge1) = M.M.pull man edge in
      M.push extra ((), map_edge edge0, map_edge edge1)
    in
    {man; extra; mem; map_edge}

  let newman man extra = makeman man extra MemoBTable.default_size

  let map_edge man = man.map_edge
end

module EXPORT_CACHED(M:EXPORT_MSig) =
struct

  type manager = {
    man : M.M.manager;
    extra : M.extra;
    mem : (M.M.G.edge', BArray.t, M.xedge, M.xedge') Hashcache.Conv.t;
    map_edge : M.M.G.edge' -> M.xedge;
  }

  let dump_stats man = Hashcache.Conv.dump_stats man.mem

  let makeman man extra hsize =
    let mem, apply = Hashcache.Conv.make M.M.G.o3b_edge' M.o3_xedge hsize in
    let rec map_edge edge = apply (M.map_edge extra rec_edge) edge
    and     rec_edge edge =
      let ((), edge0, edge1) = M.M.pull man edge in
      M.push extra ((), map_edge edge0, map_edge edge1)
    in
    {man; extra; mem; map_edge}

  let newman man extra = makeman man extra Hashcache.default_size

  let map_edge man = man.map_edge
end

module type EXPORT_NOC_MSig =
sig
  module M : Sig

  type xedge
  type extra

  val map_edge: extra -> (M.G.edge' -> xedge) -> M.G.edge' -> xedge
  val push:     extra -> unit * xedge * xedge -> xedge

end

module EXPORT_NOC(M:EXPORT_NOC_MSig) =
struct

  type manager = {
    man : M.M.manager;
    extra : M.extra;
    mem : (M.M.G.edge', M.xedge) MemoTable.t;
    map_edge : M.M.G.edge' -> M.xedge;
  }

  let dump_stats man = MemoTable.dump_stats man.mem

  let makeman man extra hsize =
    let mem, apply = MemoTable.make hsize in
    let rec map_edge edge = apply (M.map_edge extra rec_edge) edge
    and     rec_edge edge =
      let ((), edge0, edge1) = M.M.pull man edge in
      M.push extra ((), map_edge edge0, map_edge edge1)
    in
    {man; extra; mem; map_edge}

  let newman man extra = makeman man extra MemoTable.default_size

  let map_edge man = man.map_edge
end

(** Simple Bottom-Up Visitor (With Top-Down Parameter):
  * The result is computed in a bottom-up fashion
  * An additional manager can be used [type extra]
  *    ( if not required set [type extra = unit] )
  * An additional parameter can be propagated downward the DAG using [type param]
  *    ( if not required set [type param = unit] )
  **)
module type BottomUp_MSig =
sig
  module M : Sig

  type extra (* = unit, if no extra-manager is required *)
  type param (* = unit, if no parameter is required *)
             (* [param] must be comparable and hashable using [Stdlib] module *)
  type xedge

  val map_edge :
    extra -> (param -> M.G.edge' -> xedge) -> param -> M.G.edge' -> xedge
  val map_node :
    extra -> (param -> M.G.edge' -> xedge) -> param -> M.G.node' -> xedge
end

module BottomUp_NOC(M:BottomUp_MSig) =
struct
  type t = M.M.manager
  type f = M.M.G.edge'
  type extra = M.extra
  type param = M.param
  type xf = M.xedge

  type tt = {
    man   : t;
    extra : extra;
    mem   : (param * f, xf) MemoTable.t;
    rec_node : param * f -> xf;
    rec_edge : param -> f -> xf;
  }

  let dump_stats (t:tt) = MemoTable.dump_stats t.mem

  let makeman (man:t) ?(hsize=MemoTable.default_size) (extra:M.extra) : tt =
    let mem, apply = MemoTable.make hsize in
    let rec rec_node ((param, f):(param * f)) : xf =
      M.map_node extra mem_node param (M.M.pull man f)
    and     mem_node (param:param) (f:f) : xf =
      apply rec_node (param, f)
    in
    let rec_edge param f : xf =
      M.map_edge extra mem_node param f
    in
    {man; extra; mem; rec_node; rec_edge}

  let newman man extra = makeman man extra

  let get_extra man = man.extra
  let get_master man = man.man
  let solve man param edge = man.rec_edge param edge
  let rec_edge man param edge = man.rec_edge param edge
  let rec_node man param edge = man.rec_node (param, edge)
end

module type QUANT_MSig =
sig
  module M : Sig

  type peval

  val iob_peval : peval IoB.t

  type next = peval option * M.G.ident

  type next' = next M.M.M.next'
  type edge' = next M.M.M.edge'
  type node' = next M.M.M.node'

  val eval_edge : peval -> edge' ->  edge'
  val eval_node : peval -> node' -> (edge', node', node') Utils.merge3

end

module type QUANT_Sig =
sig
  module M : QUANT_MSig

  type manager

  val rec_edge : manager -> M.edge' -> M.M.G.edge'
  val rec_node : manager -> M.peval -> M.M.G.ident -> M.M.G.edge'

  val map_edge : manager -> M.peval -> M.M.G.edge' -> M.M.G.edge'
  val map_node : manager -> M.peval -> M.M.G.node' -> M.M.G.edge'

  val dump_stats : manager -> Tree.stree

end

module QUANT(Model:QUANT_MSig) =
struct
  module M = Model

  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.ident, BArray.t, M.M.G.edge', BArray.t) MemoBTable.t;
    rec_edge : M.edge' -> M.M.G.edge';
    rec_node : M.peval -> M.M.G.ident -> M.M.G.edge';
    map_edge : M.peval -> M.M.G.edge' -> M.M.G.edge';
    map_node : M.peval -> M.M.G.node' -> M.M.G.edge';
  }

  let dump_stats man = MemoBTable.dump_stats man.mem

  let makeman man (solve_node : M.M.G.node' -> M.M.G.edge') hsize =
    let iobA = IoB.closure (M.iob_peval +* M.M.G.iob_ident)
    and iobB = M.M.G.o3b_edge' in
    let mem, apply = MemoBTable.make iobA iobB hsize in
    MemoBTable.set_err mem (fun x opa opaa opb opbb ->
      print_newline();
      print_string "@@ PEvalR - man.mem : ";
      print_int x;
      print_newline();
      print_string "opa  :";
      print_string ToS.(option ignore opa);
      print_newline();
      print_string "opaa :";
      print_string (ToS.option BArray.to_hexa_string opaa);
      print_newline();
      print_string "opb  :";
      print_string ToS.(option ignore opb);
      print_newline();
      print_string "opbb :";
      print_string (ToS.option BArray.to_hexa_string opbb);
      print_newline();
      ()
    );
    let compose = M.M.M.compose in
    let pull =
      let man = M.M.export man in
      M.M.G.pull man
    in
    let rec rec_edge  (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (edge, Tree.GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (pull ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.M3Edge edge -> rec_edge edge
      | Utils.M3Cons (node, edge0, edge1) ->
        M.M.push man (node, rec_edge edge0, rec_edge edge1)
      | Utils.M3Node (node, edge0, edge1) ->
        solve_node   (node, rec_edge edge0, rec_edge edge1)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

    let newman man solver = makeman man solver MemoBTable.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

end

module QUANT_CACHED(Model:QUANT_MSig) =
struct
  module M = Model

  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.ident, BArray.t, M.M.G.edge', BArray.t) Hashcache.Conv.t;
    rec_edge : M.edge' -> M.M.G.edge';
    rec_node : M.peval -> M.M.G.ident -> M.M.G.edge';
    map_edge : M.peval -> M.M.G.edge' -> M.M.G.edge';
    map_node : M.peval -> M.M.G.node' -> M.M.G.edge';
  }

  let dump_stats man = Hashcache.Conv.dump_stats man.mem

  let makeman man (solve_node : M.M.G.node' -> M.M.G.edge') hsize =
    let iobA = IoB.closure (M.iob_peval +* M.M.G.iob_ident)
    and iobB = M.M.G.o3b_edge' in
    let mem, apply = Hashcache.Conv.make iobA iobB hsize in
    let compose = M.M.M.compose in
    let pull =
      let man = M.M.export man in
      M.M.G.pull man
    in
    let rec rec_edge  (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (edge, Tree.GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (pull ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.M3Edge edge -> rec_edge edge
      | Utils.M3Cons (node, edge0, edge1) ->
        M.M.push man (node, rec_edge edge0, rec_edge edge1)
      | Utils.M3Node (node, edge0, edge1) ->
        solve_node   (node, rec_edge edge0, rec_edge edge1)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

    let newman man solver = makeman man solver Hashcache.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

end
