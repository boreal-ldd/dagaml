(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : core    : manipulation of abstract DAG
 *
 * module  : BinUdagT : unique binarised DAG + composable edges
 *)

open GuaCaml
open STools
open BTools
open Extra
open O3Extra

open Tree
open GnTree.Type

module type MSig =
sig
  module M : BinUdag.MSig

  val push_node : 'i M.Type.node'' -> M.Type.edge * ('i M.Type.next'', 'i M.Type.node'') Utils.merge

  val compose : M.Type.edge -> 'i M.Type.edge'' -> 'i M.Type.edge''

end

module type Sig =
sig
  module M : MSig
  module G : BinUdag.Sig with
        type M.Type.leaf = M.M.Type.leaf
    and type M.Type.edge = M.M.Type.edge
    and type M.Type.node = M.M.Type.node

  type manager

  val makeman : int -> manager
  val newman : unit -> manager

  val dump_stats : manager -> Tree.stree

  val push : manager -> G.Type.node' -> G.Type.edge'
  val pull : manager -> G.Type.ident -> G.Type.node'

  val dump : manager -> G.Type.edge' list -> Tree.stree
  val load : Tree.stree  -> manager * G.Type.edge' list

  val copy_into : manager -> manager -> G.Type.edge' -> G.Type.edge'
  val copy_list_into : manager -> manager -> G.Type.edge' list -> G.Type.edge' list

  val export : manager -> G.manager
  val import : G.manager -> manager
end

module Make(Model:MSig) =
struct
  module M = Model
  module G = BinUdag.Make(Model.M)

  type manager = G.manager

  let makeman = G.makeman

  let newman  = G.newman

  let dump_stats = G.dump_stats
  let dump_estats = G.dump_estats

  let push man node' =
    let edge, merge = M.push_node node' in
    (edge, match merge with
    | Utils.MEdge next' -> next'
    | Utils.MNode node' -> GLink (G.push man node'))
  let pull man ident = G.pull man ident

  let dump = G.dump
  let load = G.load

  let export man = man
  let import man = man

  let copy_into t1 t2 : G.Type.edge' -> G.Type.edge' =
    if t1 == t2 then (fun e -> e)
    else (
      let mem, apply = MemoTable.(make default_size) in
      let rec map_next edge = function
        | GLeaf leaf -> (edge, GLeaf leaf)
        | GLink link -> M.compose edge (map_rec link)
      and     map_edge (ee, nx) =
        map_next ee nx
      and     map_node (nn, el) =
        (nn, el ||> map_edge)
      and     map_link link =
        link
        |> pull t1
        |> map_node
        |> push t2
      and     map_rec  link =
        apply map_link link
      in map_edge
      (* [DEBUG]
      (fun e ->
        print_endline "[BinUdagT.copy_into] begin";
        assert(G.traverse t1 e);
        let e' = map_edge e in
        assert(G.traverse t2 e');
        print_endline "[BinUdagT.copy_into] end";
        e'
      ) *)
    )

  let copy_list_into t1 t2 : G.Type.edge' list -> G.Type.edge' list =
    if t1 == t2 then (fun fl -> fl) else (
      let copy_edge = copy_into t1 t2 in
      Tools.map copy_edge
    )

end

module type PEVAL_MSig =
sig
  module M : Sig

  type peval

  val iob_peval : peval IoB.t

  type next = peval option * M.G.Type.ident

  type next' = next M.M.M.Type.next''
  type edge' = next M.M.M.Type.edge''
  type node' = next M.M.M.Type.node''

  val eval_edge : peval -> edge' ->  edge'
  val eval_node : peval -> node' -> (edge', node') Utils.merge
end

module type PEVAL_SIG =
sig
  module M : PEVAL_MSig

  type manager

  val rec_edge : manager -> M.edge' -> M.M.G.Type.edge'
  val rec_node : manager -> M.peval -> M.M.G.Type.ident -> M.M.G.Type.edge'
  val map_edge : manager -> M.peval -> M.M.G.Type.edge' -> M.M.G.Type.edge'
  val map_node : manager -> M.peval -> M.M.G.Type.node' -> M.M.G.Type.edge'

end

module PEVAL(M:PEVAL_MSig) =
struct
  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.Type.ident, BArray.t, M.M.G.Type.edge', BArray.t) MemoBTable.t;
    rec_edge : M.edge' -> M.M.G.Type.edge';
    rec_node : M.peval -> M.M.G.Type.ident -> M.M.G.Type.edge';
    map_edge : M.peval -> M.M.G.Type.edge' -> M.M.G.Type.edge';
    map_node : M.peval -> M.M.G.Type.node' -> M.M.G.Type.edge';
  }

  module ToB =
  struct
    open BTools.ToB

    let input = (fst M.iob_peval) * M.M.G.ToB.ident
  end

  module OfB =
  struct
    open BTools.OfB

    let input = (snd M.iob_peval) * M.M.G.OfB.ident
  end

  let makeman man hsize =
    let iobA = IoB.closure (ToB.input, OfB.input) in
    let mem, apply = MemoBTable.make iobA M.M.G.(ToBin.edge', OfBin.edge') hsize in
    let compose = M.M.M.compose in
    let rec rec_edge  (edge, next) = match next with
      | GLeaf leaf -> (edge, GLeaf leaf)
      | GLink (opeval, ident) -> match opeval with
        | None -> (edge, GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (M.M.pull man ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (GnTree.Utils.pnode_of_node node) with
      | Utils.MEdge edge -> rec_edge edge
      | Utils.MNode (node, edges) ->
        M.M.push man (node, edges ||> rec_edge)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (GnTree.Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

    let newman man = makeman man MemoBTable.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

end

module PEVAL_CACHED(M:PEVAL_MSig) =
struct
  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.Type.ident, BArray.t, M.M.G.Type.edge', BArray.t) Hashcache.Conv.t;
    rec_edge : M.edge' -> M.M.G.Type.edge';
    rec_node : M.peval -> M.M.G.Type.ident -> M.M.G.Type.edge';
    map_edge : M.peval -> M.M.G.Type.edge' -> M.M.G.Type.edge';
    map_node : M.peval -> M.M.G.Type.node' -> M.M.G.Type.edge';
  }

  module ToB =
  struct
    open BTools.ToB

    let input = (fst M.iob_peval) * M.M.G.ToB.ident
  end

  module OfB =
  struct
    open BTools.OfB

    let input = (snd M.iob_peval) * M.M.G.OfB.ident
  end

  let makeman man hsize =
    let iobA = IoB.closure (ToB.input, OfB.input) in
    let mem, apply = Hashcache.Conv.make iobA M.M.G.(ToBin.edge', OfBin.edge') hsize in
    let compose = M.M.M.compose in
    let rec rec_edge  (edge, next) = match next with
      | GLeaf leaf -> (edge, GLeaf leaf)
      | GLink (opeval, ident) -> match opeval with
        | None -> (edge, Tree.GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (M.M.pull man ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (GnTree.Utils.pnode_of_node node) with
      | Utils.MEdge edge -> rec_edge edge
      | Utils.MNode (node, edges) ->
        M.M.push man (node, edges ||> rec_edge)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

    let newman man = makeman man Hashcache.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

end

module type REWRITE_MSig =
sig
  module M : PEVAL_SIG

  type leaf = M.M.M.M.M.Type.leaf
  type edge = M.M.M.M.M.Type.edge
  type node = M.M.M.M.M.Type.node

  type ident = M.M.M.G.Type.ident
  type next' = M.M.M.G.Type.next'
  type edge' = M.M.M.G.Type.edge'
  type node' = M.M.M.G.Type.node'

  type next'' = M.M.next'
  type edge'' = M.M.edge'
  type node'' = M.M.node'
  type tree'' = (next'', edge, node) GnTree.Type.edge

  val rewrite : node'' -> tree''
end



module REWRITE_Make(MBT:MemoBTable.Sig)(Model:REWRITE_MSig) =
struct

  type manager = {
    man : Model.M.M.M.manager;
    memR : (Model.ident, Model.ident, Model.edge', BArray.t) MBT.t;
    memE : (Model.M.M.peval * Model.ident, BArray.t, Model.edge', BArray.t) MBT.t;
    map : Model.M.M.peval option -> Model.edge' -> Model.edge';
  }

  let makeman man hsize =
    let iobB = Model.M.M.M.G.O3B.edge' in
    let memR, applyR = MBT.make O3.id iobB hsize in
    let iobA = IoB.closure Model.M.M.(iob_peval +* M.G.IoB.ident) in
    let memE, applyE = MBT.make iobA  iobB hsize in
    let compose = Model.M.M.M.M.compose in
    let push : Model.node' -> Model.edge' = Model.M.M.M.push man
    and pull : Model.ident -> Model.node' = Model.M.M.M.pull man in
    (* goup = go up *)
    let rec goup_edge ((edge, next) : Model.edge'') : bool * Model.edge'= match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (false, (edge, Tree.GLink ident))
        | Some peval -> (true, compose edge (eval_ident peval ident))
    and     eval_ident peval ident : Model.edge' = applyE (fun (peval, ident) ->
      let node = pull ident in
      match Model.M.M.eval_node peval (GnTree.Utils.pnode_of_node node) with
      | Utils.MEdge edge -> goup_edge edge |> snd
      | Utils.MNode node -> goup_node node
    ) (peval, ident)
    and     goup_node (node, edges) : Model.edge' =
      let tl, edges =
        edges
        ||> goup_edge
        |> List.split
      in
      let node : Model.node' = (node, edges) in
      if List.exists (fun x -> x) tl
      then (rewr node)
      else (push node)
    and     rewr (node : Model.node') : Model.edge' =
      read_edge (Model.rewrite (GnTree.Utils.pnode_of_node node))
    and     read_edge (edge, node) : Model.edge' = match node with
      | Leaf (next : Model.next'') -> goup_edge ((edge, next) : Model.edge'') |> snd
      | Node (node, edges) ->
        goup_node (GnTree.Utils.pnode_of_node (node, edges ||> read_edge))
    in
    let rec down_ident ident : Model.edge' = applyR (fun ident ->
      let (node, edges) = pull ident in
      rewr (node, edges ||> down_edge)
    ) ident
    and     down_edge ((edge, next) : Model.edge') : Model.edge' = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink ident -> compose edge (down_ident ident)
    in
    let map opeval edge : Model.edge' =
      let edge = match opeval with
        | None -> edge
        | Some peval -> goup_edge (Model.M.M.eval_edge peval (Utils.pedge_of_edge edge)) |> snd
      in
      down_edge edge
    in
    {man; memR; memE; map}
end

module REWRITE = REWRITE_Make(MemoBTable.Module)
module REWRITE_CACHED = REWRITE_Make(Hashcache.Conv)

module IMPORT(Model:Sig) =
struct
  module G = Model

  module MSig =
  struct
    module M = G.G

    type extra  = G.manager
    type xnode  = M.Type.edge'
    type xnode' = BArray.t
    type xedge  = M.Type.edge'

    let o3_xnode = M.O3B.edge'

    let rec_edge (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink node -> G.M.compose edge (node())

    let map_node man (node, edges) =
      G.push man (node, edges ||> rec_edge)

    let map_edge man edge = rec_edge edge

  end

  include BinUdag.EXPORT(MSig)

end

module STDIO(M1:MSig) =
struct
  module G1 = Make(M1)
  module G0 = G1.G

  module REMAN = BinUdag.REMAN(G0)
  module IMPORT = IMPORT(G1)

  let to_stree man edges =
    let ubdag = G1.export man in
    let ubdag' = G0.newman () in
    let man = REMAN.newman ubdag ubdag' in
    let map = REMAN.map_edge man in
    let edges' = Tools.map map edges in
    G0.dump ubdag' edges'

  let of_stree stree =
    let ubdag', edges' = G0.load stree in
    let grobdd = G1.newman () in
    let man = IMPORT.newman ubdag' grobdd in
    let map = IMPORT.map_edge man in
    let edges = Tools.map map edges' in
    (grobdd, edges)

  let to_file man edges target =
    OfSTree.file [to_stree man edges] target

  let of_file target =
    match ToSTree.file target with
      | [] -> assert false
      | objet::_ -> of_stree objet
end
