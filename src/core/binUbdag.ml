(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : core    : manipulation of abstract DAG
 *
 * module  : BinUbdag : unique binarised DAG
 *)

open GuaCaml
open Extra
open O3Extra
open BTools
open STools

type ('leaf, 'link) next' = ('link, 'leaf) Tree.gnext
type ('edge, 'leaf, 'link) edge' = 'edge * ('leaf, 'link) next'
type ('node, 'edge, 'leaf, 'link) node' = 'node * ('edge, 'leaf, 'link) edge' * ('edge, 'leaf, 'link) edge'

module type MSig =
sig
  type leaf
  type edge
  type node

  val string_of_leaf : leaf -> string
  val string_of_edge : edge -> string
  val string_of_node : node -> string

  type 'i next' = ('i, leaf) Tree.gnext
  type 'i edge' = edge * 'i next'
  type 'i node' = node * 'i edge' * 'i edge'

  (* BList-based serialization *)
  val iob_leaf : leaf iob
  val iob_edge : edge iob
  val iob_node : node iob

  val iob_next' : 'i iob -> 'i next' iob
  val iob_edge' : 'i iob -> 'i edge' iob
  val iob_node' : 'i iob -> 'i node' iob

  (* assert(x |> dump |> load = x) && assert(x |> load |> dump |> stream) *)
  val __check_reverse__ : bool
end

module type Sig =
sig
  type ident

  module M : MSig

  type next' = ident M.next'
  type edge' = ident M.edge'
  type node' = ident M.node'

  type manager

  val makeman : int -> manager
  val newman : unit -> manager

  val dump_stats : manager -> Tree.stree

  val push : manager -> node' -> ident
  val pull : manager -> ident -> node'

  val string_of_ident : ident -> string
  val iob_ident : ident iob

  val string_of_next' : next' -> string
  val iob_next' : next' iob

  val string_of_edge' : edge' -> string
  val iob_edge' : edge' iob
  val o3b_edge' : edge' IoB.b

  val string_of_node' : node' -> string
  val iob_node' : node' iob
  val o3b_node' : node' IoB.b

  val dump : manager -> edge' list -> Tree.stree
  val load : Tree.stree  -> manager * edge' list

  val copy_into : manager -> manager -> edge' -> edge'
  val copy_list_into : manager -> manager -> edge' list -> edge' list
  val traverse : manager -> edge' -> bool

  type 'lk bw_next' = 'lk bw -> 'lk M.next' bw
  type 'lk bw_node' = 'lk bw -> 'lk M.node' bw
  type 'lk bw_edge' = 'lk bw -> 'lk M.edge' bw

  val bw_node_list : ident bw_node' -> manager -> ident list bw
  val bw_edge_list : ident bw_node' -> ident bw_edge' -> manager -> edge' list bw

  type 'lk br_next' = 'lk br -> 'lk M.next' br
  type 'lk br_node' = 'lk br -> 'lk M.node' br
  type 'lk br_edge' = 'lk br -> 'lk M.edge' br

  val br_node_list : ident br_node' -> manager -> ident list br
  val br_edge_list : ident br_node' -> ident br_edge' -> manager -> edge' list br

  type 'lk bre_node' =  'lk M.edge' br -> ('lk M.node' -> 'lk) -> 'lk M.edge' br
  type 'lk bre_edge' =  'lk M.edge' br ->                         'lk M.edge' br

  val bre_node_list : ident bre_node' -> manager -> edge' list br
  val bre_edge_list : ident bre_node' -> ident bre_edge' -> manager -> edge' list br

  val export : manager ->
    (* apply *) ((ident -> 'res) -> ident -> 'res) ->
    (* recfun *) ((unit -> 'res) M.node' -> 'res) ->
      ident -> 'res

  val combine_layerized_file : ?normalize:bool -> in_channel array -> out_channel -> unit

  module ToF :
  sig
    val man_edges :
      ?nocopy:bool ->
      ?normalize:bool ->
      ?destruct:bool ->
        manager -> edge' list Io.ToF.t
  end

  module OfF :
  sig
    val man_edges : (manager * (edge' list)) Io.OfF.t
  end
end

module Make(Model:MSig) =
struct
  module M = Model
  type ident = { height : int; index : int }

  type next' = ident M.next'
  type edge' = ident M.edge'
  type node' = ident M.node'

  type manager = {
    mutable man : H2Array.t array;
  }

  let default_layer = 2

  let makeman h_size =
    {man = Array.init default_layer (fun _ -> H2Array.create ~h_size ~a_size:h_size ())}

  let default_hsize = 16

  let newman () = makeman default_hsize

  let getsize man : int =
    Array.fold_left (fun sum h2t -> sum +
      H2Array.mapreduce h2t 0 (fun ba _ -> BArray.size ba) (+)) 0 man.man

  let number_of_nodes man : int =
    Array.fold_left (fun sum h2t -> sum + H2Array.length h2t) 0 man.man

  let dump_stats man = Tree.Node [
    Tree.Node [Tree.Leaf "card_nodes: "; ToSTree.int (number_of_nodes man) ];
    Tree.Node [Tree.Leaf "size_nodes_bytes:  "; ToSTree.int (getsize man)            ];
  ]

  let getsize_edge (ee, nx) =
    (List.length(fst M.iob_edge ee []) + 7) / 8
    (* upper rounding *)
  let getsize_edges edges =
    MyList.mapreduce edges 0 getsize_edge (+)

  let dump_estats man edges = Tree.(Node [
    Node [ Leaf "nodes: "; dump_stats man];
    Node [ Leaf "card_edges: "; ToSTree.int (List.length edges)];
    Node [ Leaf "size_edges_bytes: "; ToSTree.int (getsize_edges edges)]
  ])

  let string_of_ident x =
    ("{height="^(ToS.int x.height)^"; index="^(ToS.int x.index)^"}")

  let tob_ident x s =
    ToB.(pair int int) (x.height, x.index) s
  let ofb_ident s =
    let (height, index), s = OfB.(pair int int) s in
    ({height; index}, s)

  (* [REUSE]
  let tob_ident level x s =
    let tob_height h s =
      assert(0 <= h && h < level);
      ToB.int ((level-1)-h) s
    in
    ToB.(pair tob_height int) (x.height, x.index) s
  let ofb_ident level s =
    let ofb_height s =
      let k, s = OfB.int s in
      let h = (level-1)-k in
      assert(0 <= h && h < level);
      h, s
    in
    let (height, index), s = OfB.(pair int int) s in
    ({height; index}, s)
    *)

  let iob_ident = (tob_ident, ofb_ident)

  let string_of_next' nx =
    TreeUtils.ToS.gnext string_of_ident M.string_of_leaf nx
  let iob_next' = M.iob_next' iob_ident

  let string_of_edge' = ToS.pair M.string_of_edge string_of_next'
  let iob_edge' = M.iob_edge' iob_ident

  let string_of_node' = ToS.trio M.string_of_node string_of_edge' string_of_edge'
  let iob_node' = M.iob_node' iob_ident

  let o3b_edge' = IoB.closure iob_edge'
  let o3b_node' = IoB.closure iob_node'

  let height_next : next' -> int =
    function
    | Tree.GLeaf _ -> 0
    | Tree.GLink lk -> lk.height

  let height_edge ((_, nx):edge') : int = height_next nx
  let height_node ((_, e0, e1):node') : int =
    succ(max(height_edge e0)(height_edge e1))

  let resize_double (man:manager) : unit =
    let n  = Array.length man.man in
    let a' = Array.init (n lsl 1) (fun i ->
      if i < n  then Array.unsafe_get man.man i
                else H2Array.create ~h_size:default_hsize ~a_size:default_hsize ()) in
    man.man <- a';
    ()

  let push man (node:node') : ident =
    let height = height_node node in
    let bitv = fst o3b_node' node in
    if M.__check_reverse__
    then (
      let node' : node' = snd o3b_node' bitv in
      assert(node = node')
    );
    if height >= Array.length man.man then (resize_double man);
    assert(height < Array.length man.man);
    let index = H2Array.push man.man.(height) bitv in
    {height; index}

  let pull man ident =
    assert(ident.height < Array.length man.man);
    let bitv = H2Array.pull man.man.(ident.height) ident.index in
    let node = snd o3b_node' bitv in
    if M.__check_reverse__
    then (
      let bitv' = fst o3b_node' node in
      assert(bitv = bitv')
    );
    node

  let dump man edges =
    let man = ToSTree.array (H2Array.to_stree ) man.man
    and edges = Tools.map (fst o3b_edge' >> BArray.to_stree) edges in
    Tree.Node (man::edges)

  let load = function
    | Tree.Node (man::edges) -> (
      let man = OfSTree.array (H2Array.of_stree) man
      and edges = Tools.map (BArray.of_stree >> snd o3b_edge') edges in
      ({man}, edges)
    )
    | _ -> assert false

  let bw_edge' cha e = ToBStream.barray cha (fst o3b_edge' e)
  let tof_edge' cha e = BArray.ToF.barray cha (fst o3b_edge' e)

  let true_height (man:manager) : int =
    assert(H2Array.length man.man.(0) = 0);
    let rec loop i n maxi =
      if i < n
      then if H2Array.length man.man.(i) > 0
        then loop (succ i) n i
        else loop (succ i) n maxi
      else maxi
    in loop 1 (Array.length man.man) 0

  (* (* [CANONICAL] [BUGGY] *)
  let bw man cha (edges:edge' list) =
    let h = true_height man in
    ToBStream.int cha h;
    Array.iter (H2Array.bw ToBStream.barray cha)
               (Array.sub man.man 0 (succ h));
    ToBStream.(list bw_edge') cha edges;
    ()
   *)

  let bw ?(destruct=false) man cha (edges:edge' list) =
    ToBStream.int cha (Array.length man.man);
    if destruct
    then (
      print_endline "[BinUbdag.bw] destruct:true";
      Array.iteri (fun i h2t ->
        H2Array.bw ~destruct:true cha h2t;
        man.man.(i) <- man.man.(0);
        Gc.compact()) man.man;
    )
    else (
      Array.iter (H2Array.bw cha) man.man;
    );
    ToBStream.(list bw_edge') cha edges;
    ()

  let br_edge' cha = snd o3b_edge' (OfBStream.barray cha)
  let off_edge' cha = snd o3b_edge' (BArray.OfF.barray cha)

  let br cha =
    let height = OfBStream.int cha in
    let man = Array.init height (fun _ -> H2Array.br cha) in
    let edges = OfBStream.(list br_edge') cha in
    ({man}, edges)

  (* combines managers and concatenates outgoing edges
     => works on a per layer basis
     ?(normalize=true)
   *)
  let combine_layerized_file ?(normalize=true) (inpa:in_channel array) (out:out_channel) : unit =
    let time0 = Sys.time() in
    let n = Array.length inpa in
    let heights = Array.map (fun cha -> Io.OfF.int cha) inpa in
    let height' = Array.fold_left max 0 heights in
    Io.ToF.int out height';
    let remaps = Array.map (fun h -> Array.make h None) heights in
    let map_link r hmax lk =
      assert(lk.height < hmax);
      let index = match r.(lk.height) with
        | Some hr -> hr.(lk.index)
        | None    ->     lk.index
      in {lk with index}
    in
    let map_next r hmax = function
      | Tree.GLeaf lf -> Tree.GLeaf lf
      | Tree.GLink lk -> Tree.GLink (map_link r hmax lk)
    in
    let map_edge r hmax (ee, nx) = (ee, map_next r hmax nx) in
    let map_node r hmax (tag, e0, e1) =
      (tag, map_edge r hmax e0, map_edge r hmax e1) in
    let remap_node _ r hmax (ba:barray) : barray =
      ba |> snd o3b_node'
         |> map_node r hmax
         |> fst o3b_node'
    in
    let over = ToS.pretty_int(height' -1) in
    for h = 0 to height' -1 (* for each height [h] *)
    do
      print_endline ("[combine_layerized_file] h: "^(ToS.pretty_int h)^" / "^over^" "^(STools.short_stats time0));
      let h2t_h = H2Array.(create ~h_size:default_size ()) in
      for i = 0 to n -1 (* for each input [i] *)
      do if h < heights.(i) then (
        let r_i = remaps.(i) in
        let h2t_i_h = H2Array.OfF.h2array inpa.(i) in
        let r_i_h = Array.make (H2Array.get_index h2t_i_h) (-1) in
        remaps.(i).(h) <- Some r_i_h;
        H2Array.iter h2t_i_h
          (fun ba index -> r_i_h.(index) <- H2Array.push h2t_h (remap_node h r_i h ba));
      )
      done;
      if normalize
      then (
        let rc = H2Array.sort h2t_h () in
        for i = 0 to n -1 (* for each input [i] *)
        do if h < heights.(i)
          then (
            let rC = Tools.unop remaps.(i).(h) in
            MyArray.rename_compose_inplace rC rc
          )
        done
      );
      H2Array.ToF.h2array out h2t_h;
    done;
    print_endline ("[combine_layerized_file] finalize: "^(STools.short_stats time0));
    Array.mapi (fun i cha -> Io.OfF.list off_edge' cha ||> map_edge remaps.(i) height') inpa
    |> Array.to_list |> List.flatten
    |> Io.ToF.list tof_edge' out;
    print_endline ("[combine_layerized_file] done: "^(STools.short_stats time0));
    ()

  let to_barray ?(destruct=false) man edges =
    let cha = ToBStream.Channel.open_barray () in
    bw man cha edges;
    ToBStream.Channel.close_barray cha

  let of_barray ba =
    let cha = OfBStream.Channel.open_barray ba in
    let man, edges = br cha in
    OfBStream.Channel.close_barray cha;
    (man, edges)

  let export man
    (apply : (ident -> 'xnode) -> ident -> 'xnode)
    (recfun:(unit -> 'xnode) M.node' -> 'xnode) :
      ident -> 'xnode =
    let rec map_next = function
      | Tree.GLeaf leaf -> Tree.GLeaf leaf
      | Tree.GLink link -> Tree.GLink (fun () -> rec_node link)
    and     map_edge (edge, next) =
      (edge, map_next next)
    and     map_node (node, edge0, edge1) =
      (node, map_edge edge0, map_edge edge1)
    and     rec_node ident = apply (fun ident ->
      recfun (map_node (pull man ident))) ident
    in rec_node

  let traverse t : edge' -> bool =
    let mem, apply = MemoTable.(make default_size) in
    let rec map_next = function
      | Tree.GLeaf leaf -> true
      | Tree.GLink link -> map_rec link
    and     map_edge (_, nx) =
      map_next nx
    and     map_link link =
      let tag, e0, e1 = pull t link in
      (map_edge e0) && (map_edge e1)
    and     map_rec  link =
      apply map_link link
    in map_edge

  let copy_into t1 t2 : edge' -> edge' =
    if t1 == t2 then (fun e -> e)
    else (
      let mem, apply = MemoTable.(make default_size) in
      let rec map_next = function
        | Tree.GLeaf leaf -> Tree.GLeaf leaf
        | Tree.GLink link -> Tree.GLink (map_rec link)
      and     map_edge (ee, nx) =
        (ee, map_next nx)
      and     map_link link =
        let tag, e0, e1 = pull t1 link in
        let e0' = map_edge e0
        and e1' = map_edge e1 in
        push t2 (tag, e0', e1')
      and     map_rec  link =
        apply map_link link
      in map_edge
    )

  let copy_list_into t1 t2 : edge' list -> edge' list =
    if t1 == t2 then (fun fl -> fl) else (
      let copy_edge = copy_into t1 t2 in
      Tools.map copy_edge
    )

  (* top-down computation of unreachable nodes
      (set each node's state at (+1) if reachable, (-1) otherwise *)
  let reachable ?(clean=false) ?(hr=false) man (fl:edge' list) : unit =
    (* perform a naive layer-based elimination of dead nodes *)
    Array.iter (fun h -> H2Array.fill_state h (-1)) man.man;
    let go_next = function
      | Tree.GLeaf _ -> ()
      | Tree.GLink lk ->
        (H2Array.set_state man.man.(lk.height) lk.index 1)
    in
    let go_edge (_, nx) = go_next nx in
    let go_node (_, e0, e1) = go_edge e0; go_edge e1 in
    List.iter go_edge fl;
    for height = (Array.length man.man) -1 downto 0
    do
      let h = man.man.(height) in
      for index = 0 to H2Array.get_index h -1
      do
        match H2Array.get_state h index with
        | Some s when s >= 0 ->
          go_node (pull man {height; index})
        | _ -> ()
      done;
      if clean
      then H2Array.keep_clean_smart ~hr man.man.(height);
    done;
    ()

  (* top-down computation and removal of unreachable nodes *)
  let collect ?(hr=true) man (fl:edge' list) : unit =
    (* perform a naive layer-based elimination of dead nodes *)
    let alive = Array.map
      (fun h -> BArray.make (H2Array.get_index h) false)
      man.man
    in
    let go_next = function
      | Tree.GLeaf _ -> ()
      | Tree.GLink lk ->
        (BArray.set alive.(lk.height) lk.index true)
    in
    let go_edge (_, nx) = go_next nx in
    let go_node (_, e0, e1) = go_edge e0; go_edge e1 in
    List.iter go_edge fl;
    let b0 = BArray.make 0 false in
    for height = (Array.length man.man) -1 downto 0
    do
      let alive_h = alive.(height) in
      alive.(height) <- b0;
      H2Array.keep_clean_barray ~hr man.man.(height) alive_h;
      BArray.iteri
        (fun index alive -> if alive then go_node (pull man {height; index}))
        alive_h;
    done;
    Array.iter (fun l -> assert(BArray.length l = 0)) alive;
    (* List.iter (fun f -> assert(traverse man f = true)) fl; (* [DEBUG] *) *)
    ()

  (* top-down computation and removal of unreachable nodes *)
  let collect ?(hr=true) man (fl:edge' list) : unit =
    reachable ~clean:true ~hr man fl

  (* bottom-up renaming of the nodes
     ?(normalize=false)
     if normalize = true  : preserves relative order of the nodes
     if normalize = false : canonical if the model is order-invariant
      (e.g. ordered and uniform ones but not linears)
   *)
  let compact man ?(hr=true) ?(normalize=false) (fl:edge' list) : (edge' list) =
    let rename = Array.make (Array.length man.man) None in
    let map_link hmax lk =
      assert(lk.height < hmax);
      let index = match rename.(lk.height) with
        | Some hr -> (
          let index = hr.(lk.index) in
          assert(index >= 0);
          index
        )
        | None    ->      lk.index
      in {lk with index}
    in
    let map_next hmax = function
      | Tree.GLeaf lf -> Tree.GLeaf lf
      | Tree.GLink lk -> Tree.GLink (map_link hmax lk)
    in
    let map_edge hmax (ee, nx) = (ee, map_next hmax nx) in
    let map_node hmax (tag, e0, e1) =
      (tag, map_edge hmax e0, map_edge hmax e1) in
    let remap_node hmax (ba:barray) : barray =
      ba |> snd o3b_node'
         |> map_node hmax
         |> fst o3b_node'
    in
    for height = 0 to (Array.length man.man) -1
    do
      let r =
        H2Array.compact_fmap man.man.(height)
          ~hr:(hr && (not normalize))
          ~copy_fst:false
          ~copy_snd:false
          (fun _ _ ba -> Some(remap_node height ba))
      in
      if normalize
      then (
        MyArray.rename_compose_inplace r
          (H2Array.sort man.man.(height) ~hr ())
      );
      rename.(height) <- Some r;
    done;
    MyList.map (map_edge (Array.length man.man)) fl

  let keep_clean ?(hr=true) ?(compactify=true) ?(normalize=false) man (fl:edge' list) : (edge' list) =
    collect ~hr:(hr && compactify) man fl;
    let fl = if compactify || normalize
      then compact man ~hr ~normalize fl
      else                            fl
    in
    fl

  type 'lk bw_next' = 'lk bw -> 'lk M.next' bw
  type 'lk bw_node' = 'lk bw -> 'lk M.node' bw
  type 'lk bw_edge' = 'lk bw -> 'lk M.edge' bw

  module BW =
  struct
    open ToBStream
    open TreeUtils.ToBStream

    let default_size = 10_000

(* [REUSE]
    let bw_gen
      (leaf:M.leaf bw)
      (edge:M.edge bw)
      (node:M.node bw) =
      let next' link c x = gnext leaf link c x in
      let edge' link c x = (edge * (next' link)) c x in
      let node' link c x = (trio node (edge' link) (edge' link)) c x in
      (next', edge', node')
 *)

(* [DEBUG]
    let (bw_next'_default, bw_edge'_default, bw_node'_default) =
      bw_gen bw_leaf bw_edge bw_node
 *)

    let node_core
        (man:manager)
        (find: ident -> int option)
        (add:  ident -> int -> unit)
        (bw:   'lk bw_node')
        ?(bw_ident : int -> int t = fun _ -> int)
        (cha:Channel.t) : ident -> unit =
      let rec_node cha rec_node ident nnode =
        let nnode = ref nnode in
        let bw_link cha lk =
          nnode := rec_node lk !nnode
        in
        bw bw_link cha (pull man ident);
        !nnode
      in
      let nnode = ref 0 in
      (fun node ->
        nnode := dag_core find add (fun x -> x) rec_node
         bw_ident cha node !nnode
      )

    let node_list
      (man:manager)
      (bw:   'lk bw_node')
      ?(bw_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) (nodes:ident list) : unit =
      let memo = Hashtbl.create default_size in
      let bw_node = node_core man
        (Hashtbl.find_opt memo) (Hashtbl.add memo)
         bw ~bw_ident cha
      in
      int cha (List.length nodes);
      List.iter bw_node nodes

    let huffman_node_list
      (man:manager)
      (bw:   'lk bw_node')
      ?(bw_ident = int)
      (cha:Channel.t) (nodes:ident list) : unit =
      let _, clk_ident, end_ident = Tools.cnt default_size in
      let cha_null = Channel.open_null() in
      node_list man bw ~bw_ident:(fun _ _ -> clk_ident)
        cha_null nodes;
      Channel.close_null cha_null;
      let post (l:int list) : int t =
        IntHeap.BW.intheap' l cha
      in
      let post = Some post in
      let hc_ident = huffman2 ~post int cha (end_ident()) in
      node_list man bw ~bw_ident:(fun _ -> hc_ident)
        cha nodes

    let edge_list
      (man:manager)
      (bw_node:   'lk bw_node')
      (bw_edge:   'lk bw_edge')
      ?(bw_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) (edges:edge' list) : unit =
      let memo = Hashtbl.create default_size in
      let bw_node' (cha:Channel.t) (node:ident) : unit =
        node_core man
          (Hashtbl.find_opt memo) (Hashtbl.add memo)
           bw_node ~bw_ident cha node
      in
      let bw_edge' (cha:Channel.t) (edge:edge') : unit =
        bw_edge bw_node' cha edge in
      list bw_edge' cha edges

    let huffman_edge_list
      (man:manager)
      (bw_node:   'lk bw_node')
      (bw_edge:   'lk bw_edge')
      ?(bw_ident = int)
      (cha:Channel.t) (nodes:edge' list) : unit =
      let _, clk_ident, end_ident = Tools.cnt default_size in
      let cha_null = Channel.open_null() in
      edge_list man bw_node bw_edge ~bw_ident:(fun _ _ -> clk_ident)
        cha_null nodes;
      Channel.close_null cha_null;
      let post (l:int list) : int t =
        IntHeap.BW.intheap' l cha
      in
      let post = Some post in
      let hc_ident = huffman2 ~post int cha (end_ident()) in
      edge_list man bw_node bw_edge ~bw_ident:(fun _ -> hc_ident)
        cha nodes
  end

  (* [TODO] implement default channel writter using Models' binarisation methods *)
  let bw_node_list (bw_node' : 'lk bw_node') (man:manager) cha nodes =
    BW.huffman_node_list man bw_node' cha nodes
  let bw_edge_list (bw_node' : 'lk bw_node') (bw_edge' : 'lk bw_edge') (man:manager) cha edges =
    BW.huffman_edge_list man bw_node' bw_edge' cha edges

  type 'lk br_next' = 'lk br -> 'lk M.next' br
  type 'lk br_node' = 'lk br -> 'lk M.node' br
  type 'lk br_edge' = 'lk br -> 'lk M.edge' br

  module BR =
  struct
    open OfBStream
    open TreeUtils.OfBStream

    let default_size = 10_000

(* [REUSE]
    let br_gen
      (leaf:M.leaf br)
      (edge:M.edge br)
      (node:M.node br) =
      let next' link = gnext leaf link in
      let edge' link = (edge * (next' link)) in
      let node' link = (trio node (edge' link) (edge' link)) in
      (next', edge', node')
 *)

(* [DEBUG]
    let (br_next'_default, br_edge'_default, br_node'_default) =
      br_gen br_leaf br_edge br_node
 *)

    let node_core
      (man:manager)
      (find : int -> ident option)
      (add : int -> ident -> unit)
      (br : 'ident br_node')
      ?(br_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) : unit -> ident =
      let rec_node cha rec_node nnode : ident * int =
        let nnode = ref nnode in
        let br_lk cha : ident =
          let node, nnode' = rec_node !nnode in
          nnode := nnode';
          node
        in
        let ident = push man (br br_lk cha) in
        (ident, !nnode)
      in
      let nnode = ref 0 in
      (fun () ->
        let node, nnode' = dag_core
          find add rec_node br_ident cha !nnode
        in
        nnode := nnode';
        node
      )

    let node_list
      (man:manager)
      (br : 'ident br_node')
      ?(br_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) : unit -> ident list =
      let memo = Hashtbl.create default_size in
      let br_node = node_core man
        (Hashtbl.find_opt memo) (Hashtbl.add memo)
        br ~br_ident cha
      in
      (fun () ->
        let size = int cha in
        List.init size (fun _ -> br_node())
      )

    let huffman_node_list
      (man:manager)
      (br : 'ident br_node')
      ?(br_ident = int)
      (cha:Channel.t) : ident list =
      let post =
        Some(fun() -> IntHeap.BR.intheap' cha)
      in
      let hc_ident = huffman2 ~post int cha in
      node_list man br ~br_ident:(fun _ -> hc_ident)
        cha ()

    let edge_list
      (man:manager)
      (br_node : 'ident br_node')
      (br_edge : 'ident br_edge')
      ?(br_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) : unit -> edge' list =
      let memo = Hashtbl.create default_size in
      let br_node cha : unit -> ident = node_core man
        (Hashtbl.find_opt memo) (Hashtbl.add memo)
        br_node ~br_ident cha
      in
      let br_edge cha = br_edge (fun cha -> br_node cha ()) cha in
      (fun () -> list br_edge cha)

    let huffman_edge_list
      (man:manager)
      (br_node : 'ident br_node')
      (br_edge : 'ident br_edge')
      ?(br_ident = int)
      (cha:Channel.t) : edge' list =
      let post =
        Some(fun() -> IntHeap.BR.intheap' cha)
      in
      let hc_ident = huffman2 ~post br_ident cha in
      edge_list man br_node br_edge ~br_ident:(fun _ -> hc_ident)
        cha ()

  end

  let br_node_list (br_node': 'lk br_node') (man:manager) cha : ident list =
    BR.huffman_node_list man br_node' cha
  let br_edge_list (br_node': 'lk br_node') (br_edge': 'lk br_edge') (man:manager) cha : edge' list =
    BR.huffman_edge_list man br_node' br_edge' cha

  type 'lk bre_node' =  'lk M.edge' br -> ('lk M.node' -> 'lk) -> 'lk M.edge' br
  type 'lk bre_edge' =  'lk M.edge' br ->                         'lk M.edge' br

  module BRE =
  struct
    open OfBStream

    let default_size = 10_000

    let node_core
      (man:manager)
      (find : int -> 'lk M.edge' option)
      (add :  int -> 'lk M.edge' -> unit)
      (bre : 'ident bre_node')
      ?(nocheck_br_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) : unit -> edge' =
      let rec_node cha rec_node nnode : edge' * int =
        let nnode = ref nnode in
        let br_lk cha : edge' =
          let edge, nnode' = rec_node !nnode in
          nnode := nnode';
          edge
        in
        let edge = bre br_lk (push man) cha in
        (edge, !nnode)
      in
      let nnode = ref 0 in
      (fun () ->
        let edge, nnode' = dag_core
          find add rec_node nocheck_br_ident cha !nnode
        in
        nnode := nnode';
        edge
      )

    let node_list
      (man:manager)
      (bre : 'ident bre_node')
      ?(nocheck_br_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) : unit -> edge' list =
      let memo = Hashtbl.create default_size in
      let br_node = node_core man
        (Hashtbl.find_opt memo) (Hashtbl.add memo)
        bre ~nocheck_br_ident cha
      in
      (fun () ->
        let size = int cha in
        List.init size (fun _ -> br_node())
      )

    let huffman_node_list
      (man:manager)
      (bre : 'ident bre_node')
      ?(nocheck_br_ident = int)
      (cha:Channel.t) : edge' list =
      let post =
        Some(fun() -> IntHeap.BR.intheap' cha)
      in
      let hc_ident = huffman2 ~post nocheck_br_ident cha in
      node_list man bre ~nocheck_br_ident:(fun _ -> hc_ident)
        cha ()

    let edge_list
      (man:manager)
      (bre_node : 'ident bre_node')
      (bre_edge : 'ident bre_edge')
      ?(nocheck_br_ident : int -> int t = fun _ -> int)
      (cha:Channel.t) : unit -> edge' list =
      let memo = Hashtbl.create default_size in
      let br_node cha : unit -> edge' = node_core man
        (Hashtbl.find_opt memo) (Hashtbl.add memo)
        bre_node ~nocheck_br_ident cha
      in
      let br_edge cha = bre_edge (fun cha -> br_node cha ()) cha in
      (fun () -> list br_edge cha)

    let huffman_edge_list
      (man:manager)
      (bre_node : 'ident bre_node')
      (bre_edge : 'ident bre_edge')
      ?(nocheck_br_ident = int)
      (cha:Channel.t) : edge' list =
      let post =
        Some(fun() -> IntHeap.BR.intheap' cha)
      in
      let hc_ident = huffman2 ~post nocheck_br_ident cha in
      edge_list man bre_node bre_edge ~nocheck_br_ident:(fun _ -> hc_ident)
        cha ()
  end

  let bre_node_list (bre_node : 'lk bre_node') (man:manager) cha =
    BRE.huffman_node_list man bre_node cha
  let bre_edge_list (bre_node : 'lk bre_node') (bre_edge : 'lk bre_edge') (man:manager) cha =
    BRE.huffman_edge_list man bre_node bre_edge cha

  let to_stree man : Tree.stree =
    ToSTree.array H2Array.to_stree man.man

  module ToF =
  struct
    let edge' cha e =
      BArray.ToF.barray cha (fst o3b_edge' e)

    (* bottom-up renaming of the nodes
       ?(normalize=false)
       if normalize = true  : preserves relative order of the nodes
       if normalize = false : canonical if the model is order-invariant
        (e.g. ordered and uniform ones but not linears)
     *)
    let export man ?(compactify=true) ?(normalize=false) (cha:out_channel) (fl:edge' list) : unit =
      reachable man fl;
      Io.ToF.int cha (Array.length man.man);
      let rename = Array.make (Array.length man.man) None in
      let map_link hmax lk =
        assert(lk.height < hmax);
        let index = match rename.(lk.height) with
          | Some hr -> (
            let index = hr.(lk.index) in
            assert(index >= 0);
            index
          )
          | None    ->      lk.index
        in {lk with index}
      in
      let map_next hmax = function
        | Tree.GLeaf lf -> Tree.GLeaf lf
        | Tree.GLink lk -> Tree.GLink (map_link hmax lk)
      in
      let map_edge hmax (ee, nx) = (ee, map_next hmax nx) in
      let map_node hmax (tag, e0, e1) =
        (tag, map_edge hmax e0, map_edge hmax e1) in
      let remap_node height (ba:barray) : barray =
        ba |> snd o3b_node'
           |> map_node height
           |> fst o3b_node'
      in
      for height = 0 to (Array.length man.man) -1
      do
        let h2a = H2Array.copy man.man.(height) in
        let r =
          H2Array.compact_fmap h2a
            ~hr:false
            ~copy_fst:false
            ~copy_snd:false
            (fun i s ba ->
              if s >= 0
              then Some(remap_node height ba)
              else None)
        in
        if normalize
        then (
          MyArray.rename_compose_inplace r
            (H2Array.sort h2a ~hr:false ())
        );
        rename.(height) <- Some r;
        H2Array.ToF.h2array cha h2a
      done;
      Io.ToF.list edge' cha
        (fl ||> map_edge (Array.length man.man))

    (* similar to [export] but does not normalize *)
    let export_smart man (cha:out_channel) (fl:edge' list) : unit =
      reachable man fl;
      Io.ToF.int cha (Array.length man.man);
      let map_link hmax lk =
        assert(lk.height < hmax);
        {lk with index =
          Tools.unop
            (H2Array.get_state man.man.(lk.height) lk.index)}
      in
      let map_next hmax = function
        | Tree.GLeaf lf -> Tree.GLeaf lf
        | Tree.GLink lk -> Tree.GLink (map_link hmax lk)
      in
      let map_edge hmax (ee, nx) = (ee, map_next hmax nx) in
      let map_node hmax (tag, e0, e1) =
        (tag, map_edge hmax e0, map_edge hmax e1) in
      let remap_node height (ba:barray) : barray =
        ba |> snd o3b_node'
           |> map_node height
           |> fst o3b_node'
      in
      for height = 0 to Array.length man.man -1
      do
        let h = H2Array.compact_smart
          ~hr:false man.man.(height)
          ~copy_fst:false
          ~copy_snd:false
          (fun _ _ ba -> remap_node height ba)
        in
        H2Array.ToF.h2array cha h
      done;
      Io.ToF.list edge' cha
        (fl ||> map_edge (Array.length man.man))

    let man_edges ?(nocopy=false) ?(normalize=true) ?(destruct=false) man cha (edges:edge' list) : unit =
      if nocopy && normalize && (not destruct)
      then (print_endline "[AriUbdag.man_edges] warning:\"inconsitent options\":{nocopy:true normalize:true destruct:false} fixed:{nocopy:=false}");
      if destruct || (nocopy && (not normalize))
      then (
        let edges = if normalize
          then keep_clean
            ~hr:false
            ~compactify:true
            ~normalize:true
              man edges
          else edges
        in
        Io.ToF.int cha (Array.length man.man);
        Array.iter (H2Array.ToF.h2array cha) man.man;
        Io.ToF.list edge' cha edges;
      )
      else if normalize
        then (export man ~normalize cha edges)
        else (export_smart man cha edges)
  end

  module OfF =
  struct
    let edge' cha =
      snd o3b_edge' (BArray.OfF.barray cha)

    let man_edges cha : _ * edge' list =
      let height = Io.OfF.int cha in
      let man = Array.init height (fun _ -> H2Array.OfF.h2array cha) in
      let edges = Io.OfF.list edge' cha in
      let t = {man} in
      (* assert(List.for_all (traverse t) edges); (* [DEBUG] *) *)
      (t, edges)
  end
end

let default_iob_node iob_core iob_leaf iob_link =
  let iob_next = TreeUtils.IoB.gnext iob_link iob_leaf in
  let iob_meta = O3.trio (iob_core, iob_next, iob_next) in
  let o3_front = O3Utils.from_a_bc_de_to_abd_c_e in
  o3_front +>> iob_meta

module type OOO_MAP_MSig =
sig
  module SRC : Sig
  module DST : Sig

  val map_leaf : SRC.M.leaf -> DST.M.leaf
  val map_edge : SRC.M.edge -> DST.M.edge
  val map_node : SRC.M.node -> DST.M.node
end

module OOO_MAP(Model:OOO_MAP_MSig) =
(* One-On-One MAPping *)
struct
  type manager = {
    src :  Model.SRC.manager;
    dst :  Model.DST.manager;
    man : (Model.SRC.ident, Model.DST.ident) MemoTable.t;
    map_next : Model.SRC.next' -> Model.DST.next';
    map_edge : Model.SRC.edge' -> Model.DST.edge';
    map_node : Model.SRC.ident -> Model.DST.ident;
  }

  let makeman src dst hsize =
    let man, apply = MemoTable.make hsize in
    let rec map_next = function
      | Tree.GLeaf leaf -> Tree.GLeaf (Model.map_leaf leaf)
      | Tree.GLink ident -> Tree.GLink (map_node ident)
    and     map_edge (edge, next) = (Model.map_edge edge, map_next next)
    and     map_node ident = apply (fun ident ->
      let (node, edge'0, edge'1) = Model.SRC.pull src ident in
      Model.DST.push dst (Model.map_node node, map_edge edge'0, map_edge edge'1)
    ) ident
    in {src; dst; man; map_next; map_edge; map_node}

  let newman src dst = makeman src dst MemoTable.default_size

  let man_next man = man.map_next
  let man_edge man = man.map_edge
  let man_node man = man.map_node
end

module REMAN(M:Sig) =
struct

  type manager = {
    src : M.manager;
    dst : M.manager;
    mem : (M.ident, M.ident) MemoTable.t;
    map_node : M.ident -> M.ident;
    map_edge : M.edge' -> M.edge';
  }

  let makeman src dst hsize =
    let mem, apply = MemoTable.make hsize in
    let rec map_node ident = apply (fun ident ->
      M.push dst (rec_node (M.pull src ident))
    ) ident
    and     rec_node (node, edge0, edge1) =
      (node, map_edge edge0, map_edge edge1)
    and     map_edge (edge, next) = (edge, map_next next)
    and     map_next = function
      | Tree.GLeaf leaf -> Tree.GLeaf leaf
      | Tree.GLink ident -> Tree.GLink (map_node ident)
    in {src; dst; mem; map_node; map_edge}

  let newman src dst = makeman src dst MemoTable.default_size

  let map_node man = man.map_node
  let map_edge man = man.map_edge

end

module type SEM_MAP_MSig =
sig
  module SRC : Sig
  module DST : Sig

  val map_leaf : SRC.M.leaf -> DST.edge'
  val map_edge : SRC.M.edge -> DST.edge' -> DST.edge'
  val map_node : SRC.M.node -> DST.edge' -> DST.edge' ->
    DST.M.edge * (DST.next', DST.node') Utils.merge
end

module SEM_MAP(Model:SEM_MAP_MSig) =
struct
  type manager = {
    src :  Model.SRC.manager;
    dst :  Model.DST.manager;
    man : (Model.SRC.ident, Model.SRC.ident, Model.DST.edge', BArray.t) MemoBTable.t;
    map_next : Model.SRC.next' -> Model.DST.edge';
    map_edge : Model.SRC.edge' -> Model.DST.edge';
    map_node : Model.SRC.ident -> Model.DST.edge';
  }

  let makeman src dst hsize =
    let man, apply = MemoBTable.make O3.id Model.DST.o3b_edge' hsize in
    let rec map_next = function
      | Tree.GLeaf leaf -> Model.map_leaf leaf
      | Tree.GLink ident -> map_node ident
    and     map_edge (edge, next) = Model.map_edge edge (map_next next)
    and     map_node ident = apply (fun ident ->
      let (node, edge'0, edge'1) = Model.SRC.pull src ident in
      let edge', merge = Model.map_node node (map_edge edge'0) (map_edge edge'1) in
      (edge', match merge with
        | Utils.MEdge next' -> next'
        | Utils.MNode node' -> Tree.GLink(Model.DST.push dst node'))
    ) ident
    in {src; dst; man; map_next; map_edge; map_node}

  let newman src dst = makeman src dst MemoBTable.default_size

  let man_next man = man.map_next
  let man_edge man = man.map_edge
  let man_node man = man.map_node

end

module SEM_MAP_CACHED(Model:SEM_MAP_MSig) =
struct
  type manager = {
    src :  Model.SRC.manager;
    dst :  Model.DST.manager;
    man : (Model.SRC.ident, Model.SRC.ident, Model.DST.edge', BArray.t) Hashcache.Conv.t;
    map_next : Model.SRC.next' -> Model.DST.edge';
    map_edge : Model.SRC.edge' -> Model.DST.edge';
    map_node : Model.SRC.ident -> Model.DST.edge';
  }

  let makeman src dst hsize =
    let man, apply = Hashcache.Conv.make O3.id Model.DST.o3b_edge' hsize in
    let rec map_next = function
      | Tree.GLeaf leaf -> Model.map_leaf leaf
      | Tree.GLink ident -> map_node ident
    and     map_edge (edge, next) = Model.map_edge edge (map_next next)
    and     map_node ident = apply (fun ident ->
      let (node, edge'0, edge'1) = Model.SRC.pull src ident in
      let edge', merge = Model.map_node node (map_edge edge'0) (map_edge edge'1) in
      (edge', match merge with
        | Utils.MEdge next' -> next'
        | Utils.MNode node' -> Tree.GLink(Model.DST.push dst node'))
    ) ident
    in {src; dst; man; map_next; map_edge; map_node}

  let newman src dst = makeman src dst Hashcache.default_size

  let man_next man = man.map_next
  let man_edge man = man.map_edge
  let man_node man = man.map_node

end

module type EXPORT_MSig =
sig
  module M : Sig

  type extra
  type xnode
  type xnode'
  type xedge

  val o3_xnode : (xnode, xnode') O3.o3

  val map_node : extra -> (unit -> xnode) M.M.node' -> xnode
  val map_edge : extra -> (unit -> xnode) M.M.edge' -> xedge

end

module EXPORT(M:EXPORT_MSig) =
struct

  type next'' = (unit -> M.xnode) M.M.M.next'
  type edge'' = (unit -> M.xnode) M.M.M.edge'
  type node'' = (unit -> M.xnode) M.M.M.node'

  type manager = {
    man : M.M.manager;
    extra : M.extra;
    mem : (M.M.ident, M.M.ident, M.xnode, M.xnode') MemoBTable.t;
    rec_edge : M.M.edge' -> M.xedge;
    rec_node : M.M.ident -> M.xnode;
  }

  let dump_stats man = MemoBTable.dump_stats man.mem

  let makeman man extra hsize =
    let mem, apply = MemoBTable.make O3.id M.o3_xnode hsize in
    let rec map_next : M.M.next' -> next'' = function
      | Tree.GLeaf leaf -> Tree.GLeaf leaf
      | Tree.GLink link -> Tree.GLink (fun () -> rec_node link)
    and     map_edge ((edge, next) : M.M.edge') : edge'' =
      (edge, map_next next)
    and     map_node ((node, edge0, edge1) : M.M.node') : node'' =
      (node, map_edge edge0, map_edge edge1)
    and     rec_node (ident : M.M.ident) : M.xnode = apply (fun ident ->
      M.map_node extra (map_node (M.M.pull man ident))
    ) ident
    in
    let     rec_edge edge = M.map_edge extra (map_edge edge) in
    {man; extra; mem; rec_node; rec_edge}

  let newman man extra = makeman man extra MemoBTable.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
end

module EXPORT_CACHED(M:EXPORT_MSig) =
struct

  type next'' = (unit -> M.xnode) M.M.M.next'
  type edge'' = (unit -> M.xnode) M.M.M.edge'
  type node'' = (unit -> M.xnode) M.M.M.node'

  type manager = {
    man : M.M.manager;
    extra : M.extra;
    mem : (M.M.ident, M.M.ident, M.xnode, M.xnode') Hashcache.Conv.t;
    rec_edge : M.M.edge' -> M.xedge;
    rec_node : M.M.ident -> M.xnode;
  }

  let dump_stats man = Hashcache.Conv.dump_stats man.mem

  let makeman man extra hsize =
    let mem, apply = Hashcache.Conv.make O3.id M.o3_xnode hsize in
    let rec map_next : M.M.next' -> next'' = function
      | Tree.GLeaf leaf -> Tree.GLeaf leaf
      | Tree.GLink link -> Tree.GLink (fun () -> rec_node link)
    and     map_edge ((edge, next) : M.M.edge') : edge'' =
      (edge, map_next next)
    and     map_node ((node, edge0, edge1) : M.M.node') : node'' =
      (node, map_edge edge0, map_edge edge1)
    and     rec_node (ident : M.M.ident) : M.xnode = apply (fun ident ->
      M.map_node extra (map_node (M.M.pull man ident))
    ) ident
    in
    let     rec_edge edge = M.map_edge extra (map_edge edge) in
    {man; extra; mem; rec_node; rec_edge}

  let newman man extra = makeman man extra Hashcache.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
end

module type EXPORT_NOC_MSig =
sig
  module M : Sig

  type extra
  type xnode
  type xedge

  val map_node : extra -> (unit -> xnode) M.M.node' -> xnode
  val map_edge : extra -> (unit -> xnode) M.M.edge' -> xedge

end

module EXPORT_NOC(M:EXPORT_NOC_MSig) =
struct

  type next'' = (unit -> M.xnode) M.M.M.next'
  type edge'' = (unit -> M.xnode) M.M.M.edge'
  type node'' = (unit -> M.xnode) M.M.M.node'

  type manager = {
    man : M.M.manager;
    extra : M.extra;
    mem : (M.M.ident, M.xnode) MemoTable.t;
    rec_edge : M.M.edge' -> M.xedge;
    rec_node : M.M.ident -> M.xnode;
  }

  let get_extra man = man.extra

  let dump_stats man = MemoTable.dump_stats man.mem

  let makeman man extra hsize =
    let mem, apply = MemoTable.make hsize in
    let rec map_next : M.M.next' -> next'' = function
      | Tree.GLeaf leaf -> Tree.GLeaf leaf
      | Tree.GLink link -> Tree.GLink (fun () -> rec_node link)
    and     map_edge ((edge, next) : M.M.edge') : edge'' =
      (edge, map_next next)
    and     map_node ((node, edge0, edge1) : M.M.node') : node'' =
      (node, map_edge edge0, map_edge edge1)
    and     rec_node (ident : M.M.ident) : M.xnode = apply (fun ident ->
      M.map_node extra (map_node (M.M.pull man ident))
    ) ident
    in
    let     rec_edge edge = M.map_edge extra (map_edge edge) in
    {man; extra; mem; rec_node; rec_edge}

  let newman man extra = makeman man extra MemoTable.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
end

(* [BottomUp] functor implements both bottom-up computation, and top-down parameter propagation *)
module type BottomUp_NOC_MSig =
sig
  module M : Sig

  type extra
  type xnode
  type xedge
  type param

  val map_node : extra -> param -> (param -> xnode) M.M.node' -> xnode
  val map_edge : extra -> param -> (param -> xnode) M.M.edge' -> xedge

end

module BottomUp_NOC(M:BottomUp_NOC_MSig) =
struct

  type next'' = (M.param -> M.xnode) M.M.M.next'
  type edge'' = (M.param -> M.xnode) M.M.M.edge'
  type node'' = (M.param -> M.xnode) M.M.M.node'

  type manager = {
    man : M.M.manager;
    extra : M.extra;
    mem : (M.param * M.M.ident, M.xnode) MemoTable.t;
    rec_edge : M.param -> M.M.edge' -> M.xedge;
    rec_node : M.param -> M.M.ident -> M.xnode;
  }

  let get_extra man = man.extra

  let dump_stats man = MemoTable.dump_stats man.mem

  let makeman man extra hsize =
    let mem, apply = MemoTable.make hsize in
    let rec map_next : M.M.next' -> next'' = function
      | Tree.GLeaf leaf -> Tree.GLeaf leaf
      | Tree.GLink link -> Tree.GLink (fun param -> rec_node param link)
    and     map_edge ((edge, next) : M.M.edge') : edge'' =
      (edge, map_next next)
    and     map_node ((node, edge0, edge1) : M.M.node') : node'' =
      (node, map_edge edge0, map_edge edge1)
    and     rec_node (param:M.param) (ident : M.M.ident) : M.xnode =
      apply
        (fun (param, ident) -> M.map_node extra param (map_node (M.M.pull man ident)))
        (param, ident)
    in
    let     rec_edge param edge = M.map_edge extra param (map_edge edge) in
    {man; extra; mem; rec_node; rec_edge}

  let newman man extra = makeman man extra MemoTable.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
end

module type TO_DOT_MSig =
sig
  module M : Sig

  val string_of_leaf : M.M.leaf -> string
  val string_of_edge : bool option -> M.M.edge -> string
  val string_of_node : M.M.node -> string
end

module TO_DOT(Model:TO_DOT_MSig) =
struct

  module M = Model

  module Model =
  struct
    module M = M.M

    type extra  = Udag.String.manager
    type xnode  = Udag.String.ident
    type xedge  = Udag.String.edge_t

    type next' = (unit -> xnode) M.M.next'
    type edge' = (unit -> xnode) M.M.edge'
    type node' = (unit -> xnode) M.M.node'

    let rec_next = function
      | Tree.GLeaf leaf -> Tree.GLeaf (Model.string_of_leaf leaf)
      | Tree.GLink node -> Tree.GLink (node())

    let rec_edge pos (edge, next) = (
      Model.string_of_edge pos edge,
      rec_next next
    )

    let rec_node (node, edge0, edge1) =
      ((None, Model.string_of_node node), [rec_edge (Some false) edge0; rec_edge (Some true) edge1])

    let map_edge man edge = rec_edge None edge
    let map_node man node = Udag.String.push man (rec_node node)

  end

  module Module = EXPORT_NOC(Model)

  let dotfile ubdag edges target =
    let strman = Udag.String.newman () in
    let man = Module.newman ubdag strman in
    let map = Module.rec_edge man in
    let edges' = Tools.map map edges in
    Udag.String.to_dot_file strman edges' target;
    ()
end

