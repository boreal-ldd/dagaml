(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2016-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : core    : manipulation of abstract DAG
 *)

open GuaCaml
open STools

module type Sig =
sig
    module H:Udag.MSig

    type ident = int
(*    type ident *)

    type tree  = (H.leaf, pnode) Tree.gnext
    and  node  = H.node * tree * tree
    and  pnode = ident
(*    and     pnode *)

    val get_ident : pnode -> ident

    val equal_tree : tree -> tree -> bool
    val equal_node : node -> node -> bool

    type manager

    val makeman : int -> manager

    val newman : unit -> manager
    val push : manager -> node -> pnode
    val pull : manager -> pnode -> node

    val dump_stats : manager -> Tree.stree

end

module Make(H0:Udag.MSig) : Sig with
  module H = H0
=
struct
    type ident = int

    module H = H0
    type tree = (H.leaf, pnode) Tree.gnext
    and     node  = (H.node * tree * tree)
    and  pnode = ident

    let get_ident pnode = pnode

    let equal_tree x y = match x, y with
        | Tree.GLeaf x, Tree.GLeaf y -> x = y
        | Tree.GLink x, Tree.GLink y -> (x = y)
        | _ -> false

    let equal_node (d, t0, t1) (d', t0', t1') =
        (d = d')&&(equal_tree t0 t0')&&(equal_tree t1 t1')

    (* let dump man ed *)

    type manager = {
        unique : node H2Table.t;
    }

    let makeman hsize = {
        unique = H2Table.create hsize 0;
    }

    let default_newman_hsize = 10000

    let newman () = makeman default_newman_hsize
    let push man node = H2Table.push man.unique node

    let pull man pnode = H2Table.pull man.unique pnode

    let dump_stats man = Tree.Node [
        Tree.Leaf "#node: "; Tree.Node [ ToSTree.int (H2Table.length man.unique) ]
    ]

end

