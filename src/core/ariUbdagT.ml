(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : core    : manipulation of abstract DAG
 *
 * module  : AriUbdagT : arity-aware unique binarised DAG + composable edges
 *)

open GuaCaml
open STools
open BTools
open Extra
open O3Extra

module type MSig =
sig
  module M : AriUbdag.MSig

  val push_node : 'i M.node' -> M.edge * ('i M.next', 'i M.node') Utils.merge

  val compose : M.edge -> 'i M.edge' -> 'i M.edge'

end

module type Sig =
sig
  module M : MSig
  module G : AriUbdag.Sig with
        type M.leaf = M.M.leaf
    and type M.edge = M.M.edge
    and type M.node = M.M.node

  type manager

  val makeman : int -> manager
  val newman : unit -> manager

  val dump_stats : manager -> Tree.stree

  val push : manager -> G.node' -> G.edge'
  val pull : manager -> G.ident -> G.node'

  val dump : manager -> G.edge' list -> Tree.stree
  val load : Tree.stree  -> manager * G.edge' list

  val copy_into : manager -> manager -> G.edge' -> G.edge'
  val copy_list_into : manager -> manager -> G.edge' list -> G.edge' list

  val export : manager -> G.manager
  val import : G.manager -> manager
end

module Make(Model:MSig) =
struct
  module M = Model
  module G = AriUbdag.Make(Model.M)

  type manager = G.manager

  let makeman = G.makeman

  let newman  = G.newman

  let dump_stats = G.dump_stats
  let dump_estats = G.dump_estats

  let push man node' =
    let edge, merge = M.push_node node' in
    (edge, match merge with
    | Utils.MEdge next' -> next'
    | Utils.MNode node' -> Tree.GLink (G.push man node'))
  let pull man ident = G.pull man ident

  let dump = G.dump
  let load = G.load

  let export man = man
  let import man = man

  let copy_into t1 t2 : G.edge' -> G.edge' =
    if t1 == t2 then (fun e -> e)
    else (
      let mem, apply = MemoTable.(make default_size) in
      let rec map_next edge = function
        | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
        | Tree.GLink link -> M.compose edge (map_rec link)
      and     map_edge (ee, nx) =
        map_next ee nx
      and     map_link link =
        let tag, e0, e1 = pull t1 link in
        let e0' = map_edge e0
        and e1' = map_edge e1 in
        push t2 (tag, e0', e1')
      and     map_rec  link =
        apply map_link link
      in map_edge
      (* [DEBUG]
      (fun e ->
        print_endline "[AriUbdagT.copy_into] begin";
        assert(G.traverse t1 e);
        let e' = map_edge e in
        assert(G.traverse t2 e');
        print_endline "[AriUbdagT.copy_into] end";
        e'
      ) *)
    )

  let copy_list_into t1 t2 : G.edge' list -> G.edge' list =
    if t1 == t2 then (fun fl -> fl) else (
      let copy_edge = copy_into t1 t2 in
      Tools.map copy_edge
    )

end

module type PEVAL_MSig =
sig
  module M : Sig

  type peval

  val iob_peval : peval IoB.t

  type next = peval option * M.G.ident

  type next' = next M.M.M.next'
  type edge' = next M.M.M.edge'
  type node' = next M.M.M.node'

  val eval_edge : peval -> edge' ->  edge'
  val eval_node : peval -> node' -> (edge', node') Utils.merge

end

module type PEVAL_SIG =
sig
  module M : PEVAL_MSig

  type manager

  val rec_edge : manager -> M.edge' -> M.M.G.edge'
  val rec_node : manager -> M.peval -> M.M.G.ident -> M.M.G.edge'
  val map_edge : manager -> M.peval -> M.M.G.edge' -> M.M.G.edge'
  val map_node : manager -> M.peval -> M.M.G.node' -> M.M.G.edge'

end

module PEVAL(M:PEVAL_MSig) =
struct
  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.ident, BArray.t, M.M.G.edge', BArray.t) MemoBTable.t;
    rec_edge : M.edge' -> M.M.G.edge';
    rec_node : M.peval -> M.M.G.ident -> M.M.G.edge';
    map_edge : M.peval -> M.M.G.edge' -> M.M.G.edge';
    map_node : M.peval -> M.M.G.node' -> M.M.G.edge';
  }

  let makeman man hsize =
    let iobA = IoB.closure M.(iob_peval +* M.G.iob_ident) in
    let mem, apply = MemoBTable.make iobA M.M.G.o3b_edge' hsize in
    let compose = M.M.M.compose in
    let rec rec_edge  (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (edge, Tree.GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (M.M.pull man ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> rec_edge edge
      | Utils.MNode (node, edge0, edge1) ->
        M.M.push man (node, rec_edge edge0, rec_edge edge1)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

    let newman man = makeman man MemoBTable.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

end

module PEVAL_CACHED(M:PEVAL_MSig) =
struct
  type manager = {
    man : M.M.manager;
    mem : (M.peval * M.M.G.ident, BArray.t, M.M.G.edge', BArray.t) Hashcache.Conv.t;
    rec_edge : M.edge' -> M.M.G.edge';
    rec_node : M.peval -> M.M.G.ident -> M.M.G.edge';
    map_edge : M.peval -> M.M.G.edge' -> M.M.G.edge';
    map_node : M.peval -> M.M.G.node' -> M.M.G.edge';
  }

  let makeman man hsize =
    let iobA = IoB.closure M.(iob_peval +* M.G.iob_ident) in
    let mem, apply = Hashcache.Conv.make iobA M.M.G.o3b_edge' hsize in
    let compose = M.M.M.compose in
    let rec rec_edge  (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (edge, Tree.GLink ident)
        | Some peval -> compose edge (rec_node peval ident)
    and     rec_node peval ident = apply (fun (peval, ident) ->
      map_node peval (M.M.pull man ident)
    ) (peval, ident)
    and     map_node peval node =
      match M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> rec_edge edge
      | Utils.MNode (node, edge0, edge1) ->
        M.M.push man (node, rec_edge edge0, rec_edge edge1)
    in
    let map_edge peval edge = rec_edge (M.eval_edge peval (Utils.pedge_of_edge edge)) in
    {man; mem; rec_edge; rec_node; map_edge; map_node}

    let newman man = makeman man Hashcache.default_size

  let rec_edge man = man.rec_edge
  let rec_node man = man.rec_node
  let map_edge man = man.map_edge
  let map_node man = man.map_node

end

module type REWRITE_MSig =
sig
  module M : PEVAL_SIG

  type leaf = M.M.M.M.M.leaf
  type edge = M.M.M.M.M.edge
  type node = M.M.M.M.M.node

  type ident = M.M.M.G.ident
  type next' = M.M.M.G.next'
  type edge' = M.M.M.G.edge'
  type node' = M.M.M.G.node'

  type next'' = M.M.next'
  type edge'' = M.M.edge'
  type node'' = M.M.node'
  type tree'' = (next'', edge, node) GTree.edge

  val rewrite : node'' -> tree''
end

module REWRITE(Model:REWRITE_MSig) =
struct

  type manager = {
    man : Model.M.M.M.manager;
    memR : (Model.ident, Model.ident, Model.edge', BArray.t) MemoBTable.t;
    memE : (Model.M.M.peval * Model.ident, BArray.t, Model.edge', BArray.t) MemoBTable.t;
    map : Model.M.M.peval option -> Model.edge' -> Model.edge';
  }

  let makeman man hsize =
    let iobB = Model.M.M.M.G.o3b_edge' in
    let memR, applyR = MemoBTable.make O3.id iobB hsize in
    let iobA = IoB.closure Model.M.M.(iob_peval +* M.G.iob_ident) in
    let memE, applyE = MemoBTable.make iobA  iobB hsize in
    let compose = Model.M.M.M.M.compose in
    let push : Model.node' -> Model.edge' = Model.M.M.M.push man
    and pull : Model.ident -> Model.node' = Model.M.M.M.pull man in
    (* goup = go up *)
    let rec goup_edge ((edge, next) : Model.edge'') : bool * Model.edge'= match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (false, (edge, Tree.GLink ident))
        | Some peval -> (true, compose edge (eval_ident peval ident))
    and     eval_ident peval ident : Model.edge' = applyE (fun (peval, ident) ->
      let node = pull ident in
      match Model.M.M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> goup_edge edge |> snd
      | Utils.MNode node -> goup_node node
    ) (peval, ident)
    and     goup_node (node, edge0, edge1) : Model.edge' =
      let t0, edge0 = goup_edge edge0
      and t1, edge1 = goup_edge edge1 in
      let node : Model.node' = (node, edge0, edge1) in
      if t0||t1
      then (rewr node)
      else (push node)
    and     rewr (node : Model.node') : Model.edge' = read_edge (Model.rewrite (Utils.pnode_of_node node))
    and     read_edge (edge, node) : Model.edge' = match node with
      | GTree.Leaf (next : Model.next'') -> goup_edge ((edge, next) : Model.edge'') |> snd
      | GTree.Node (node, edge0, edge1) ->
        goup_node (Utils.pnode_of_node (node, read_edge edge0, read_edge edge1))
    in
    let rec down_ident ident : Model.edge' = applyR (fun ident ->
      let (node, edge0, edge1) = pull ident in
      rewr (node, down_edge edge0, down_edge edge1)
    ) ident
    and     down_edge ((edge, next) : Model.edge') : Model.edge' = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink ident -> compose edge (down_ident ident)
    in
    let map opeval edge : Model.edge' =
      let edge = match opeval with
        | None -> edge
        | Some peval -> goup_edge (Model.M.M.eval_edge peval (Utils.pedge_of_edge edge)) |> snd
      in
      down_edge edge
    in
    {man; memR; memE; map}

end

module REWRITE_CACHED(Model:REWRITE_MSig) =
struct

  type manager = {
    man : Model.M.M.M.manager;
    memR : (Model.ident, Model.ident, Model.edge', BArray.t) Hashcache.Conv.t;
    memE : (Model.M.M.peval * Model.ident, BArray.t, Model.edge', BArray.t) Hashcache.Conv.t;
    map : Model.M.M.peval option -> Model.edge' -> Model.edge';
  }

  let makeman man hsize =
    let iobB = Model.M.M.M.G.o3b_edge' in
    let memR, applyR = Hashcache.Conv.make O3.id iobB hsize in
    let iobA = IoB.closure Model.M.M.(iob_peval +* M.G.iob_ident) in
    let memE, applyE = Hashcache.Conv.make iobA  iobB hsize in
    let compose = Model.M.M.M.M.compose in
    let push : Model.node' -> Model.edge' = Model.M.M.M.push man
    and pull : Model.ident -> Model.node' = Model.M.M.M.pull man in
    (* goup = go up *)
    let rec goup_edge ((edge, next) : Model.edge'') : bool * Model.edge'= match next with
      | Tree.GLeaf leaf -> (false, (edge, Tree.GLeaf leaf))
      | Tree.GLink (opeval, ident) -> match opeval with
        | None -> (false, (edge, Tree.GLink ident))
        | Some peval -> (true, compose edge (eval_ident peval ident))
    and     eval_ident peval ident : Model.edge' = applyE (fun (peval, ident) ->
      let node = pull ident in
      match Model.M.M.eval_node peval (Utils.pnode_of_node node) with
      | Utils.MEdge edge -> goup_edge edge |> snd
      | Utils.MNode node -> goup_node node
    ) (peval, ident)
    and     goup_node (node, edge0, edge1) : Model.edge' =
      let t0, edge0 = goup_edge edge0
      and t1, edge1 = goup_edge edge1 in
      let node : Model.node' = (node, edge0, edge1) in
      if t0||t1
      then (rewr node)
      else (push node)
    and     rewr (node : Model.node') : Model.edge' = read_edge (Model.rewrite (Utils.pnode_of_node node))
    and     read_edge (edge, node) : Model.edge' = match node with
      | GTree.Leaf (next : Model.next'') -> goup_edge ((edge, next) : Model.edge'') |> snd
      | GTree.Node (node, edge0, edge1) ->
        goup_node (Utils.pnode_of_node (node, read_edge edge0, read_edge edge1))
    in
    let rec down_ident ident : Model.edge' = applyR (fun ident ->
      let (node, edge0, edge1) = pull ident in
      rewr (node, down_edge edge0, down_edge edge1)
    ) ident
    and     down_edge ((edge, next) : Model.edge') : Model.edge' = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink ident -> compose edge (down_ident ident)
    in
    let map opeval edge : Model.edge' =
      let edge = match opeval with
        | None -> edge
        | Some peval -> goup_edge (Model.M.M.eval_edge peval (Utils.pedge_of_edge edge)) |> snd
      in
      down_edge edge
    in
    {man; memR; memE; map}

end

module IMPORT(Model:Sig) =
struct
  module G = Model

  module MSig =
  struct
    module M = G.G

    type extra  = G.manager
    type xnode  = M.edge'
    type xnode' = BArray.t
    type xedge  = M.edge'

    let o3_xnode = M.o3b_edge'

    let rec_edge (edge, next) = match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink node -> G.M.compose edge (node())

    let map_node man (node, edge0, edge1) =
      G.push man (node, rec_edge edge0, rec_edge edge1)

    let map_edge man edge = rec_edge edge

  end

  include AriUbdag.EXPORT(MSig)

end

module STDIO(M1:MSig) =
struct
  module G1 = Make(M1)
  module G0 = G1.G

  module REMAN = AriUbdag.REMAN(G0)
  module IMPORT = IMPORT(G1)

  let to_stree man edges =
    let ubdag = G1.export man in
    let ubdag' = G0.newman () in
    let man = REMAN.newman ubdag ubdag' in
    let map = REMAN.map_edge man in
    let edges' = Tools.map map edges in
    G0.dump ubdag' edges'

  let of_stree stree =
    let ubdag', edges' = G0.load stree in
    let grobdd = G1.newman () in
    let man = IMPORT.newman ubdag' grobdd in
    let map = IMPORT.rec_edge man in
    let edges = Tools.map map edges' in
    (grobdd, edges)

  let to_file man edges target =
    OfSTree.file [to_stree man edges] target

  let of_file target =
    match ToSTree.file target with
      | [] -> assert false
      | objet::_ -> of_stree objet
end
