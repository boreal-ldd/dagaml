(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : src : final executables
 *
 * module  : Conv_ext : Uses file extension to select conversion mechanism
 *)

open GuaCaml

let _ =
  let n = (Array.length Sys.argv-1)/2-1 in
  if not ((n>=1) && (2*(n+1)+1 = Array.length Sys.argv))
  then
  (
    output_string stderr "[dagaml:conv_ext] error : incorrect number of arguments\n";
    exit 1
  );
  (* start by computing extensions' tag *)
  let funmap i =
    let name = Sys.argv.(2*i+1)
    and ext  = Sys.argv.(2*i+2) in
    let tag  = ConvTypesLoad.file_ext ext in
    (name^ext, tag)
  in
  let path = Array.init (succ n) funmap in
  for idx = 0 to n-1
  do
    let fileA, tagA = path.(idx)
    and fileB, tagB = path.(succ idx) in

    Conv.conv tagA fileA tagB fileB [];(*[ConvArgsTypes.print_stats];*)
    print_newline()
  done;
  let stat = Gc.stat() in
  print_endline ("max heap (bytes) : "^(STools.ToS.pretty_int (8*stat.Gc.top_heap_words)));
  OProfile.(print default);
  exit 0
