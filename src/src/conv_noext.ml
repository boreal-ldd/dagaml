(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : src : final executables
 *
 * module  : Conv_noext : uses explicit extension notation to selection conversion mechanism
 *)

open GuaCaml
let fileA = Sys.argv.(1)
and extA  = Sys.argv.(2) |> ConvTypesLoad.file_ext
and fileB = Sys.argv.(3)
and extB  = Sys.argv.(4) |> ConvTypesLoad.file_ext;;

Conv.conv extA fileA extB fileB [ConvArgsTypes.print_stats];;
print_newline();;
exit 0;;
