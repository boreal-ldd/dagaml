(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : Expr    : basic boolean expression
 *)
open GuaCaml
open Expr

type ('a, 'b) reduce_expr = {
  rcst : bool -> 'b;
  rvar : 'a -> 'b;
  ruop : uop -> 'b -> 'b;
  rbop : bop -> 'b -> 'b -> 'b;
}

let rec reduce_expr (r:('a, 'b) reduce_expr) : 'a expr -> 'b =
  function
  | PCst b -> r.rcst b
  | PVar v -> r.rvar v
  | PUop (uop, e) -> r.ruop uop (reduce_expr r e)
  | PBop (bop, e1, e2) -> r.rbop bop (reduce_expr r e1) (reduce_expr r e2)

let support (expr:'a expr) : 'a list =
  let reduce_support : ('a, 'a list) reduce_expr = {
    rcst = (fun _ -> []);
    rvar = (fun x -> [x]);
    ruop = (fun _ s -> s);
    rbop = (fun _ s1 s2 -> SetList.union s1 s2);
  }
  in
  reduce_expr reduce_support expr

open GGLA_FT
open Type

let primal_graph (nv:int) (expr:int expr) : hg =
  (* print_endline "[DAGaml.ExprUtils.primal_graph] {step:0}"; *)
  let sys_expr = IoUtils.system_from_expr "EXPR" nv [expr] in
  (* print_endline "[DAGaml.ExprUtils.primal_graph] {step:1}"; *)
  let sys_nax  = IoUtils.nax_of_expr sys_expr in
  (* print_endline "[DAGaml.ExprUtils.primal_graph] {step:2}"; *)
  let domain, primals = Nax.RefinedPrimalGraph.compute sys_nax in
  (* print_endline "[DAGaml.ExprUtils.primal_graph] {step:3}"; *)
  primals.(0) |> snd
(*  List.fold_left (fun g0 (_, g1) -> union g0 g1) (empty domain) primals *)
