(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : FlatNAX : n-ary representation of NAX circuits
 *
 * === NOTE ===
 *
 * for normalization purpose, a formula is either a constrant or a
 * tree, a tree does contain constants as it's leaves only variables
 *
 * edges can be complemented
 *
 * the normalization rule for complemented edges and XOR is to move
 * the complementation up in the tree.
 *)

open GuaCaml
open STools
open BTools
open Extra
open O3Extra

open Nax

module Model : BinUdag.MSig
  with type Type.leaf = int option
  and  type Type.edge = bool
  and  type Type.node = Nax.nax
=
struct
  module Type =
  struct
    type leaf = int option
    type edge = bool
    type node = Nax.nax (* type nax = And | Xor *)

    type 'i next'' = ('i, leaf) BinUdag.next'''
    type 'i edge'' = ('i, leaf, edge) BinUdag.edge'''
    type 'i node'' = ('i, leaf, edge, node) BinUdag.node'''
  end
  open Type

  module ToS =
  struct
    open STools.ToS
    open Nax.ToS

    let leaf = option int
    let edge = bool
    let node = nax
  end

  module ToB =
  struct
    open BTools.ToB
    open Nax.ToB

    let leaf = option int
    let edge = bool
    let node = nax
  end

  module OfB =
  struct
    open BTools.OfB
    open Nax.OfB

    let leaf = option int
    let edge = bool
    let node = nax
  end

  (* assert(x |> dump |> load = x) && assert(x |> load |> dump |> stream) *)
  (* [DEBUG] *)
  let __check_reverse__ = true
end
include Model

module ModelT =
struct
  module M = Model
  open M
  open Type

  let medge (b, nx) = (b, Utils.MEdge nx)

  let split_edges (edges:'i edge'' list) : int * int * int list * int list * 'i list  * 'i list =
    let open Tree in
    let rec split_rec nF nT lFVar lTVar lFlink lTlink : 'i edge'' list -> _ =
      function
      | [] -> (nF, nT, List.rev lFVar, List.rev lTVar, List.rev lFlink, List.rev lTlink)
      | head::tail ->
        match head with
        | (false, GLeaf None) -> split_rec (succ nF) nT lFVar lTVar lFlink lTlink tail
        | (true , GLeaf None) -> split_rec nF (succ nT) lFVar lTVar lFlink lTlink tail
        | (false, GLeaf (Some v)) -> split_rec nF nT (v::lFVar) lTVar lFlink lTlink tail
        | (true , GLeaf (Some v)) -> split_rec nF nT lFVar (v::lTVar) lFlink lTlink tail
        | (false, GLink lk) -> split_rec nF nT lFVar lTVar (lk::lFlink) lTlink tail
        | (true , GLink lk) -> split_rec nF nT lFVar lTVar lFlink (lk::lTlink) tail
      in split_rec 0 0 [] [] [] [] edges

  type 'lk return = edge * ('lk next'', 'lk node'') Utils.merge

  let medge_bool (b:bool) : 'i return =
    (b, Utils.MEdge (Tree.GLeaf None))
  let medge_var  (b:bool) (i:int) : 'i return =
    (b, Utils.MEdge (Tree.GLeaf(Some i)))
  let medge_link  (b:bool) (lk:'i) : 'i return =
    (b, Utils.MEdge (Tree.GLink lk))

  let push_and (edges:'i edge'' list) : 'i return =
    let open Tree in
    if edges = [] then medge_bool true else (
      let nF, nT, lFVar, lTVar, lFlink, lTlink = split_edges edges in
      assert(nF >= 0 && nT >= 0);
      (* false /\ ? = false => if the number of [false]s is > 0 then false*)
      (* true /\ ?A = A     => we just ignore the [true]s *)
      if nF > 0 then medge_bool false else (
        (* SetList.sort also remove duplicates *)
        let lFVar  = SetList.sort lFVar
        and lTVar  = SetList.sort lTVar
        and lFlink = SetList.sort lFlink
        and lTlink = SetList.sort lTlink in
        (* ?A /\ not ?A /\ ? = false => if lFVar \inter lTVar \neq \emptyset then false *)
        if not(SetList.nointer lFVar lTVar) then medge_bool false else (
        (* ?A /\ not ?A /\ ? = false => if lFlink \inter lTlink \neq \emptyset then false *)
        if not(SetList.nointer lFlink lTlink) then medge_bool false else (
          (* [] = true => if total length = 0 then return true *)
          (* [x] = x => if total length = 1 then return this element *)
          let ne = List.length lFVar + List.length lTVar + List.length lFlink + List.length lTlink in
          assert(ne >= 0);
          match ne with
          | 0 -> medge_bool true
          | 1 -> (
            if lFVar <> [] then medge_var false (List.hd lFVar) else
            if lTVar <> [] then medge_var true  (List.hd lTVar) else
            if lFlink <> [] then medge_link false (List.hd lFlink) else
                                 medge_link true  (List.hd lTlink)
          )
          | _ -> (
            (false, Utils.MNode (And, MyList.flatten [
              (lFVar ||> (fun i -> (false, GLeaf(Some i))));
              (lTVar ||> (fun i -> (true , GLeaf(Some i))));
              (lFlink ||> (fun lk -> (false, GLink lk)));
              (lTlink ||> (fun lk -> (true , GLink lk)));
            ]))
          )
        ))
      )
    )

  let push_xor (edges:'i edge'' list) : 'i return =
    let open Tree in
    if edges = [] then medge_bool false else (
      let nF, nT, lFVar, lTVar, lFlink, lTlink = split_edges edges in
      assert(nF >= 0 && nT >= 0);
      (* false <> ?A = ?A => we just ignore [false]s *)
      (* true  <> ?A = not ?A => we count the the number of negated edges modulo 2 *)
      let ce : bool = ((nT + (List.length lTVar) + (List.length lTlink)) mod 2) = 1 in
      (* [process l = l'] where
       *   - l' is a strictly increasing list of element
       *   - e \in l iff [count e l mod 2 = 1]
       *)
      let process (l:_ list) : _ list =
        l
        |> List.sort Stdlib.compare
        |> Assoc.count_sorted_keys
        |> MyList.opmap (fun (a, n) -> if n mod 2 = 1 then Some a else None)
      in
      let lVar  = process (lFVar @> lTVar) in
      let llink = process (lFlink @> lTlink) in
      let ne = List.length lVar + List.length llink in
      assert(ne >= 0);
      match ne with
      | 0 -> medge_bool true
      | 1 -> (
        if lVar <> [] then medge_var ce (List.hd lVar)
                      else medge_link ce (List.hd llink)
      )
      | _ -> (
        (ce, Utils.MNode (Xor, MyList.flatten [
          (lVar ||> (fun i -> (false, GLeaf(Some i))));
          (llink ||> (fun lk -> (false, GLink lk)));
        ]))
      )
    )

  let push_node (node:'i node'') : 'i return =
    match fst node with
    | And -> push_and (snd node)
    | Xor -> push_xor (snd node)

(*
  let tos_next'' a'link =
    GnTree.ToS.gnext a'link Model.ToS.leaf

  let tos_node'' a'link =
    GnTree.ToS.gnode a'link Model.ToS.leaf Model.ToS.edge Model.ToS.node

  let tos_merge a'a a'b =
    function
    | Utils.MEdge a -> "MEdge "^(a'a a)
    | Utils.MNode b -> "MNode "^(a'b b)

  let tos_return a'link : 'lk return -> string =
    STools.ToS.pair Model.ToS.edge (tos_merge (tos_next'' a'link) (tos_node'' a'link))

  let push_node (node:'i node'') : 'i return =
    print_endline ("node:"^(tos_node'' STools.ToS.ignore node));
    let return = push_node node in
    print_endline ("return:"^(tos_return STools.ToS.ignore return));
    return
 *)

  let compose negx (negy, nexty) = (negx<>negy, nexty)
end

module Module =
struct
  module Module = BinUdagT.STDIO(ModelT)
  include Module
end
open Module

module OfNAX =
struct
  module Model =
  struct
    module M = Nax.NAX.G0

    type extra = G1.manager
    type xnode = G0.Type.edge'
    type xedge = G0.Type.edge'

    let map_next extra = function
      | Tree.GLeaf leaf -> (false, Tree.GLeaf leaf)
      | Tree.GLink link -> link()

    let map_edge extra (neg, next) =
      ModelT.compose neg (map_next extra next)

    let map_node extra (tag, edge0, edge1) =
      G1.push extra (tag, [map_edge extra edge0; map_edge extra edge1])
  end

  module Module = BinUbdag.EXPORT_NOC(Model)

  let export (sman:Nax.NAX.G0.manager) (dman:G1.manager) (outputs:Nax.NAX.G0.edge' array) : G0.Type.edge' array =
    let eman = Module.newman sman dman in
    let map_edge = Module.rec_edge eman in
    Array.map map_edge outputs
end

module ToNAX =
struct
  module Model =
  struct
    module M = Module.G0
    open Nax

    type extra = NAX.G0.manager
    type xnode = NAX.G0.edge'
    type xedge = NAX.G0.edge'

    let map_next extra = function
      | Tree.GLeaf leaf -> (false, Tree.GLeaf leaf)
      | Tree.GLink link -> link()

    let map_edge extra (neg, next) =
      NAX.M1.compose neg (map_next extra next)

    let map_node extra (tag, edges) =
      match List.rev_map (map_edge extra) edges with
      | [] -> ((match tag with And -> true | Xor -> false), Tree.GLeaf None)
      | tail::head ->
        (List.fold_left (fun r l -> NAX.G1.push extra (tag, l, r)) tail head)
  end

  module Module = BinUdag.EXPORT_NOC(Model)

  let export (sman:G1.manager) (dman:Nax.NAX.G0.manager) (outputs:G0.Type.edge' array) : Nax.NAX.G0.edge' array =
    let eman = Module.newman sman dman in
    let map_edge = Module.map_edge eman in
    Array.map map_edge outputs
end

module Flatten =
struct
  module Model =
  struct
    module M = Module.G0

    type extra = G1.manager
    type xnode = G0.Type.node' G0.Type.edge''
    type xedge = G0.Type.edge'

    let rec_next extra : _ -> xnode = function
      | Tree.GLeaf leaf -> (false, Tree.GLeaf leaf)
      | Tree.GLink link -> link()

    let rec_edge extra (neg, next) : xnode =
      ModelT.compose neg (rec_next extra next)

    let map_node (extra:extra) ((tag, edges):_ G0.Type.node'') : xnode =
      let edge, node' = ModelT.push_node (tag, edges ||> rec_edge extra) in
      match node' with
      | Utils.MEdge (Tree.GLeaf leaf) -> (edge, Tree.GLeaf leaf)
      | Utils.MEdge (Tree.GLink node) -> (edge, Tree.GLink node)
      | Utils.MNode (tag, edges) -> (
        let flatten, edges = MyList.map_partition
          (fun ((edge, next) : xnode) ->
            match next with
            | Tree.GLeaf (leaf : Model.Type.leaf) -> Ok ((edge, Tree.GLeaf leaf) : G0.Type.edge')
            | Tree.GLink (node : G0.Type.node') -> (
              if edge = false && (fst node) = tag
              then Error (snd node)
              else (Ok (ModelT.compose edge (G1.push extra node)))
            )
          )
          edges
        in
        let edges = edges @> (MyList.flatten flatten) in
        let edge', node'' = ModelT.push_node (tag, edges) in
        let edge = edge <> edge' in
        match node'' with
        | Utils.MEdge (Tree.GLeaf leaf) -> (edge, Tree.GLeaf leaf)
        | Utils.MEdge (Tree.GLink link) -> (
          let node = G1.pull extra link in
          (edge, Tree.GLink node)
        )
        | Utils.MNode node -> (edge, Tree.GLink node)
      )

    let map_edge (extra:extra) (edge:_ G0.Type.edge'') : xedge =
      let edge, next = rec_edge extra edge in
      match next with
      | Tree.GLeaf leaf -> (edge, Tree.GLeaf leaf)
      | Tree.GLink node -> ModelT.compose edge (G1.push extra node)
  end

  module Module = BinUdag.EXPORT_NOC(Model)

  (* [sman] is the source manager for given edges
   * [dman] is the destination manager for returned edges
   *
   * === NOTE ===
   *
   * possibly [sman == dman]
   *)
  let export (sman:G1.manager) (dman:G1.manager) (edges:G0.Type.edge' array) : G0.Type.edge' array =
    let eman = Module.newman sman dman in
    let map_edge = Module.map_edge eman in
    Array.map map_edge edges
end

module ToGraphviz =
struct
  module Model =
  struct
    module M = G0

    let string_of_leaf = function
      | Some var -> "X"^(string_of_int var)
      | None     -> "L0"

    let string_of_pos = function
      | None   -> "black"
      | Some _ -> "grey"

    let string_of_edge pos edge =
      "[label = \""^((function true -> "-" | false -> "+") edge)^"\"; color=\""^(string_of_pos pos)^"\"];"

    let string_of_node = function
    | And      -> "A"
    | Xor      -> "X"
  end

  module Module = BinUdag.TO_DOT(Model)

  let to_graphviz_file = Module.dotfile
end

module Utils =
struct
  (* performs a DFS (depth-first-search) and returns the order in which
   * variables have been encountered first
   * - **NOTE** structurally useless variables dot not appear in the result
   *)
  let dfs_variables man hsize edges : int list =
    let stack = ref [] in
    let dfs : (int, unit) Hashtbl.t = Hashtbl.create hsize in
    let go_var var =
      match Hashtbl.find_opt dfs var with
      | None -> (
        stack_push stack var;
        Hashtbl.add dfs var ()
      )
      | Some () -> ()
    in
    let memo, apply = MemoTable.make hsize in
    let rec rec_edge (_, next) =
      match next with
      | Tree.GLeaf leaf ->
        (match leaf with Some var -> go_var var | None -> ())
      | Tree.GLink link -> rec_link link
    and     rec_node (_, edges) =
      List.iter rec_edge edges
    and     rec_link link =
      apply (fun link -> rec_node (G1.pull man link)) link
    in
    List.iter rec_edge edges;
    List.rev !stack

  (* [COPY?] [GENERALIZE?] *)
  (* [reorder_namespace man hsize edges order = edges']
   * takes an ordered list of variables
   * every variable withing that list is re-ordered to match the order given in input
   * variables which do not appear in the list stay in place
   *)
  let reorder_namespace man hsize edges order =
    let sorted = SetList.sort order in
    assert(List.length sorted = List.length order);
    let rebase = Tools.hashtbl_of_assoc (List.map2 (fun k v -> (k, v)) sorted order) in
    let go_var var =
      match Hashtbl.find_opt rebase var with
      | None -> var
      | Some var' -> var'
    in
    let go_leaf = Tools.opmap go_var in
    let memo, apply = MemoTable.make hsize in
    let rec rec_next = function
      | Tree.GLeaf leaf -> (false, Tree.GLeaf (go_leaf leaf))
      | Tree.GLink link -> rec_link link
    and     rec_edge (edge, next) =
      ModelT.compose edge (rec_next next)
    and     rec_node (tag, edges) =
      G1.push man (tag, edges ||> rec_edge)
    and     rec_link link =
      apply (fun link -> rec_node (G1.pull man link)) link
    in
    Tools.map rec_edge edges

  (* [COPY?] [GENERALIZE?] *)
  (* [contract_namespace man hsize edges order = edges']
   * takes an ordered list of variables
   * every variable withing that list is re-ordered to match the order given in input
   * variables which do not appear in the list are removed from the order
   *)
  let contract_namespace man hsize edges order =
    let rebase = Tools.hashtbl_of_assoc (List.mapi (fun i k -> (k, i)) order) in
    let go_var var =
      match Hashtbl.find_opt rebase var with
      | None -> assert false
      | Some var' -> var'
    in
    let go_leaf = Tools.opmap go_var in
    let memo, apply = MemoTable.make hsize in
    let rec rec_next = function
      | Tree.GLeaf leaf -> (false, Tree.GLeaf (go_leaf leaf))
      | Tree.GLink link -> rec_link link
    and     rec_edge (edge, next) =
      ModelT.compose edge (rec_next next)
    and     rec_node (tag, edges) =
      G1.push man (tag, edges ||> rec_edge)
    and     rec_link link =
      apply (fun link -> rec_node (G1.pull man link)) link
    in
    Tools.map rec_edge edges

  (* for every reachable identifier (from an an array of starting points)
   * we compute the number of incoming arcs (i.e. the number of parent nodes)
   *)
  let count_parent man hsize edges : (G0.Type.next', int) Hashtbl.t =
    let hash : (_, int) Hashtbl.t = Hashtbl.create hsize in
    let incr next =
      match Hashtbl.find_opt hash next with
      | None -> (
        Hashtbl.add hash next 1;
        true
      )
      | Some count -> (
        Hashtbl.replace hash next (succ count);
        false
      )
    in
    let rec rec_edge (_, next) =
      if incr next
      then
        match next with
        | Tree.GLeaf _ -> ()
        | Tree.GLink link -> rec_link link
    and     rec_node (_, edges) =
      List.iter rec_edge edges
    and     rec_link link =
      rec_node (G1.pull man link)
    in
    List.iter rec_edge edges;
    hash

  (* [flow_impact man edges]
   * the algorithms preprocesses the graph by counting the number of incident
   * edges, then once all incident edges of a node have been processed we
   * distribute the flow to their children
   *)
  let flow_impact man hsize edges : (G0.Type.next', float) Hashtbl.t =
    let count : (G0.Type.next', int) Hashtbl.t = count_parent man hsize edges in
    (* [LATER] change by an accumulator and smart float addition *)
    let hash : (G0.Type.next', float) Hashtbl.t = Hashtbl.create hsize in
    let decr next : bool =
      match Hashtbl.find_opt count next with
      | None -> assert false
      | Some value -> (
        assert(value >= 1);
        Hashtbl.replace count next (pred value);
        value = 1
      )
    in
    let incr add next : float =
      match Hashtbl.find_opt hash next with
      | None       -> Hashtbl.add hash next add; add
      | Some value -> (
        let value = value +. add in
        Hashtbl.replace hash next value;
        value
      )
    in
    let rec rec_edge flow (_, next) =
      let flow' = incr flow next in
      if decr next
      then
        match next with
        | Tree.GLeaf _ -> ()
        | Tree.GLink link -> rec_link flow' link
    and     rec_node flow (_, edges) =
      let n = List.length edges in
      List.iter (rec_edge (flow /. (float_of_int n))) edges
    and     rec_link flow link =
      rec_node flow (G1.pull man link)
    in
    let n = List.length edges in
    List.iter (rec_edge ( 1. /. (float_of_int n))) edges;
    hash

  (* [subst_ident sman dman edges' ident subst = edges']
   *)
  let subst_ident
      (sman:G1.manager)
      (dman:G1.manager)
      (hsize:int)
      (edges:G0.Type.edge' array)
      (next0:G0.Type.next')
      (subst:G0.Type.edge') :
        G0.Type.edge' array =
    let memo, apply = MemoTable.make hsize in
    let rec rec_next (next : G0.Type.next') : G0.Type.edge' =
      if next = next0
      then subst
      else
        match next with
        | Tree.GLeaf leaf -> (false, Tree.GLeaf leaf)
        | Tree.GLink link -> rec_link link
    and     rec_edge ((edge, next) : G0.Type.edge') : G0.Type.edge' =
      ModelT.compose edge (rec_next next)
    and     rec_node ((node, edges) : G0.Type.node') : G0.Type.edge' =
      G1.push dman (node, edges ||> rec_edge)
    and     rec_link (link:G0.Type.ident) : G0.Type.edge' =
      apply (fun link -> rec_node (G1.pull sman link)) link
    in
    Array.map rec_edge edges
end
