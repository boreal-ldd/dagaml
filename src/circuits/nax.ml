(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : Nax     : Negation And Xor
 *)

open GuaCaml
open STools
open BTools
open Extra
open O3Extra

type nax =
  | And
  | Xor

(* Section 1. IO *)

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let nax = function And -> "And" | Xor -> "Xor"
  let pair = trio int int int

  let leaf = option int
  let edge = bool
  let node = nax

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let nax x s = bool (x = And) s

  let leaf = option int
  let edge = bool
  let node = nax

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let nax s : nax * stream =
    let b, s = bool s in
    ((if b then And else Xor), s)

  let leaf = option int
  let edge = bool
  let node = nax

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

(*
module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToBa =
struct
  open BTools.ToB

  let next' a (ident:'i tob) nx (s:stream) : stream =
    match nx with
    | Tree.GLeaf b  -> (assert(a = 0); bool b s)
    | Tree.GLink lk -> (assert(a > 0); ident lk s)

  let edge' a (ident:'i tob) ((x, y), nx) (s:stream) : stream =
    assert(x = a);
    assert(y <= x);
    int (x-y) (next' y ident nx s)

  let node' (ident:'i tob) (node:_ node') (s:stream) : stream =
    let a = arity_node node in
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) node s
end

module OfBa =
struct
  open BTools.OfB

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    if a = 0
    then (let b, s = bool s in (Tree.GLeaf b, s))
    else (let lk, s = ident a s in (Tree.GLink lk, s))

  let edge' a (ident:int -> 'i ofb) s : _ edge' * stream =
    let d, s = int s in
    assert(d <= a);
    let y = a - d in
    let nx, s = next' y ident s in
    (((a, y), nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

let pretty_of_edge (i, j) = (string_of_int i)^" -> "^(string_of_int j)
 *)

module NAX =
struct

  module Model =
  struct
    type leaf = int option (* [Some var] = [var] | [None] = false *)
    type edge = bool
    type node = nax

    let string_of_leaf = ToS.leaf
    let string_of_edge = ToS.edge
    let string_of_node = ToS.node

    let iob_leaf = IoB.leaf
    let iob_edge = IoB.edge
    let iob_node = IoB.node

    type 'i next' = ('i, leaf) Tree.gnext
    type 'i edge' = edge * 'i next'
    type 'i node' = node * 'i edge' * 'i edge'

    let iob_next' = IoB.next'
    let iob_edge' = IoB.edge'
    let iob_node' = IoB.node'

    let __check_reverse__ = false
  end

  module M1 =
  struct
    module M = Model
    (* WIP *)
    let is_cst (b, nx) =
      match nx with
      | Tree.GLeaf None -> Some b
      | _ -> None

    let cst b = (b, Tree.GLeaf None)

    let cneg b0 (b1, nx1) = (b0<>b1, nx1)

    let medge (b, nx) = (b, Utils.MEdge nx)
    let push_node (node, (neg0, next0), (neg1, next1)) = match node with
    | And -> (match next0 with
      | Tree.GLeaf None -> (if neg0
        then (* edge0 = true *) (neg1, Utils.MEdge next1)
        else (false, Utils.MEdge(Tree.GLeaf None))
      )
      | _ -> match next1 with
        | Tree.GLeaf None -> (if neg1
          then  (* edge1 = true *) (neg0, Utils.MEdge next0)
          else (false, Utils.MEdge(Tree.GLeaf None))
        )
        | _ -> if next0 = next1
          then (if neg0 = neg1
            then (neg0, Utils.MEdge next0)
            else (false, Utils.MEdge (Tree.GLeaf None))
          )
          else (false, Utils.MNode (And, (neg0, next0), (neg1, next1))))
    | Xor -> (match next0 with
      | Tree.GLeaf None -> (neg0<>neg1, Utils.MEdge next1)
      | _ -> match next1 with
        | Tree.GLeaf None -> (neg0<>neg1, Utils.MEdge next0)
        | _ -> if next0 = next1
          then (neg0<>neg1, Utils.MEdge(Tree.GLeaf None))
          else (false, Utils.MNode (Xor, (neg0, next0), (neg1, next1))))
    let compose negx (negy, nexty) = (negx<>negy, nexty)
  end

  include BinUbdagT.STDIO(M1)
end

module ToExpr =
struct
  module MODELE =
  struct
    module M = NAX.G0

    type extra = unit
    type xnode = int Expr.expr
    type xedge = int Expr.expr

    let map_next extra = function
      | Tree.GLink link -> link()
      | Tree.GLeaf leaf ->
        match leaf with
        | None -> Expr.PCst false
        | Some var -> Expr.PVar var

    let map_edge extra (neg, next) =
      Expr.xorb neg (map_next extra next)

    let map_node extra (node, f0, f1) =
      let ex0 = map_edge extra f0
      and ex1 = map_edge extra f1 in
      let bop = Expr.(match node with And -> PAnd | Xor -> PXor) in
      Expr.(PBop(bop, ex0, ex1))
  end

  module MODULE = BinUbdag.EXPORT_NOC(MODELE)

  let export (man:NAX.G0.manager) (outputs : NAX.G0.edge' array) : int Expr.expr array =
    let eman = MODULE.newman man () in
    let map_edge = MODULE.rec_edge eman in
    Array.map map_edge outputs
end

module ToSysExpr =
struct
  type carry = {
    ninputs : int;
    mutable wire_cnt : int;
    mutable wires    : int list;
    mutable assign   : (int * int Expr.expr) list;
  }

  module MODELE =
  struct
    module M = NAX.G0

    type extra = carry
    type xnode = int
    type xedge = int Expr.expr

    let map_edge extra (neg, node) = Expr.(PUop ((if neg then PNot else PNop),
      (match node with
        | Tree.GLink node -> PVar (node())
        | Tree.GLeaf leaf -> match leaf with
            | Some var -> assert(0 <= var && var < extra.ninputs); PVar var
            | None -> PCst false
      )))

    let map_node extra (node, f0, f1) =
      let ex0 = map_edge extra f0
      and ex1 = map_edge extra f1 in
      let wire = extra.wire_cnt in
      extra.wire_cnt <- succ extra.wire_cnt;
      let bop = Expr.(match node with And -> PAnd | Xor -> PXor) in
      let assoc = (wire, Expr.(PBop(bop, ex0, ex1))) in
      extra.assign <- assoc :: extra.assign;
      extra.wires  <- wire  :: extra.wires;
      wire
  end

  module MODULE = BinUbdag.EXPORT_NOC(MODELE)

  let export ?(name="NAX") (man:NAX.G0.manager) (ninputs : int) (outputs : NAX.G0.edge' array) =
    let extra = {
      ninputs;
      assign = [];
      wires  = [];
      wire_cnt = ninputs;
    } in
    let eman = MODULE.newman man extra in
    let map_edge = MODULE.rec_edge eman in
    let apply edge =
      let expr = map_edge edge in
      let wire = extra.wire_cnt in
      extra.wire_cnt <- succ extra.wire_cnt;
      extra.assign <- (wire, expr) :: extra.assign;
      wire
    in
    let outs = Array.map apply outputs in
    IoTypes.{
      name;
      man  = ();
      inps = Array.init ninputs (fun x -> x);
      regs = [||];
      wire = Array.of_list (List.rev extra.wires);
      outs;
      sset = Array.of_list (List.rev extra.assign);
    }
end

type 's system =
  ('s, NAX.G1.manager, NAX.G0.edge') IoTypes.system

module ToCmd =
struct
  type carry = {
    ninputs    : int;
    ff         : string;
    prefix     : string;
    inputs     : string array;
    name_space : NameSpace.var_type NameSpace.t;
    assign     : CmdTypes.prg ref
  }

  module MODELE =
  struct
    module M = NAX.G0

    type extra = carry
    type xnode = CmdTypes.var
    type xedge = CmdTypes.f

    let map_edge extra (neg, next) : CmdTypes.f =
      match next with
      | Tree.GLeaf leaf -> (
        match leaf with
        | Some var -> (
          assert(0 <= var && var < extra.ninputs);
          (neg, extra.inputs.(var))
        )
        | None -> (neg, extra.ff)
      )
      | Tree.GLink link -> (neg, link())

    let map_node extra (node, f0, f1) =
      let ex0 = map_edge extra f0 in
      let ex1 = map_edge extra f1 in
      let w = NameSpace.push extra.name_space extra.prefix in
      let cmd =
        match node with
        | And -> CmdTypes.And (w, [ex0; ex1])
        | Xor -> CmdTypes.Xor (w, [ex0; ex1])
      in
      stack_push extra.assign cmd;
      w
  end

  module EXP = BinUbdag.EXPORT_NOC(MODELE)

  let export (sys:string system) : CmdTypes.prg =
    assert(sys.regs = [||]);
    assert(sys.wire = [||]);
    let h_size = Array.(length sys.IoTypes.sset) in
    let name_space = NameSpace.(newman ~h_size Wire ()) in
    let inputs = Array.map
      (NameSpace.push' name_space ~tag:NameSpace.Input)
      sys.inps
    in
    assert(inputs = sys.inps);
    let outputs = Array.map
      (NameSpace.push' name_space ~tag:NameSpace.Output)
      sys.outs
    in
    assert(outputs = sys.outs);
    (* false constant *)
    let ninputs = Array.length sys.inps in
    let ff = NameSpace.push name_space "ff" in
    let assign = ref [CmdTypes.Or (ff, []); CmdTypes.Var (Array.to_list sys.inps)] in
    let extra = {
      ninputs;
      ff;
      prefix = "w";
      inputs;
      name_space;
      assign
    } in
    let eman = EXP.newman sys.IoTypes.man extra in
    let map_edge = EXP.rec_edge eman in
    let outs = Array.map (fun (v, e) ->
      let cmd = CmdTypes.And (v, [map_edge e]) in
      stack_push extra.assign cmd;
      (false, v)) sys.IoTypes.sset in
    stack_push extra.assign (CmdTypes.Info (Array.to_list outs));
    stack_push extra.assign CmdTypes.Flush;
    stack_push extra.assign CmdTypes.Quit;
    let deleted = Hashtbl.create h_size in
    let get_del (fl:CmdTypes.fl) : CmdTypes.var list =
      List.fold_left (fun c (_, v) ->
        if Hashtbl.mem deleted v
        then c
        else (
          Hashtbl.add deleted v ();
          (v::c)
        )
      ) [] fl
    in
    let prg' = List.map (fun cmd ->
      let del = match cmd with
        | CmdTypes.Info fl -> get_del fl
        | CmdTypes.Count fl -> get_del fl
        | CmdTypes.Satisfy fl -> get_del fl
        | CmdTypes.And (_, fl) -> get_del fl
        | CmdTypes.Or (_, fl) -> get_del fl
        | CmdTypes.Xor (_, fl) -> get_del fl
        | CmdTypes.Ite (_, fc, f1, f0) -> get_del [fc; f1; f0]
        | CmdTypes.Equal (f1, f2) -> get_del [f1; f2]
        | CmdTypes.Not (_, f) -> get_del [f]
        | _ -> []
      in
      (CmdTypes.Delete (List.rev del), cmd)) !assign in
    MyList.rev_flatten_pair prg'
end

(* When considering an NNF, it is possible to simply compute a structural
 * bi-decomposition of it by using distributivity of OR over AND.
 * However, exhaustively distributing ORs over ANDs is sub-optimal and may
 * increase exponentially the size of the formula.
 * We introduce a two steps method:
 *   - First, we estimate the primal graph of the maximaly distributed CNF
 *     using basic graph manipulation.
 *   - Second, we use the estimated primal graph to perform only necessary
 *     distributions
 * Optionnally, we may interleave an extra-step where we the use the estimated
 * primal-graph to compute a tree-decomposition, which may lead to further
 * over-approximate the primal graph, hence further decreasing the burden of
 * syntactic transformations.
 * [LINK](https://gitlab.com/boreal-ldd/snowflake/-/wikis/Refined-Primal-Graph-Estimation)
 *)
module RefinedPrimalGraph =
struct
  module G = GGLA_FT
  type hg = G.Type.hg
  type carry = {
    domain : int;
  }
  module MODELE =
  struct
    module M = NAX.G0

    type extra = carry
    type xnode = hg * hg
    type xedge = hg * hg

    let map_edge extra (neg, node) =
      Tools.cswap neg (match node with
        | Tree.GLink node -> node()
        | Tree.GLeaf leaf -> (
          let g =
            match leaf with
            | Some var -> G.clique extra.domain [var]
            | None     -> G.empty  extra.domain
          in
          (g, g)
        )
      )

    let map_node extra (node, f0, f1) =
      let (g00, g01) = map_edge extra f0
      and (g10, g11) = map_edge extra f1 in
      match node with
      | And -> (G.union g00 g10, G.hyperedge g01 g11)
      | Xor -> (
        let g0 = G.union g00 g01 in
        let g1 = G.union g10 g11 in
        let g = G.hyperedge g0 g1 in
        (g, g)
      )
  end

  module MODULE = BinUbdag.EXPORT_NOC(MODELE)

  (* [FIXME] assumes that inputs, registers, and wire are trivially ordered *)
  (* returns the primal graph for every element in sys.sset *)
  let compute (sys:int system) : int * ((int * hg) array) =
    let ni = Array.length sys.IoTypes.inps in
    Array.iteri (fun i0 i -> assert(i = i0)) sys.IoTypes.inps;
    let nr = Array.length sys.IoTypes.regs in
    Array.iteri (fun j0 j -> assert(j = j0 + ni)) sys.IoTypes.regs;
    let nw = Array.length sys.IoTypes.wire in
    Array.iteri (fun k0 k -> assert(k = k0 + (ni+nr))) sys.IoTypes.regs;
    let extra = {domain = ni + nr + nw} in
    let eman = MODULE.newman sys.IoTypes.man extra in
    let map_edge = MODULE.rec_edge eman in
    (extra.domain, Array.map (fun (i, e) -> (i, map_edge e |> fst)) sys.IoTypes.sset)
end

module ToGraphviz =
struct
  module Model =
  struct
    module M = NAX.G0

    let string_of_leaf = function
      | Some var -> "X"^(string_of_int var)
      | None     -> "L0"

    let string_of_pos = function
      | None       -> "black"
      | Some false -> "red"
      | Some true  -> "green"

    let string_of_edge pos edge =
      "[label = \""^((function true -> "-" | false -> "+") edge)^"\"; color=\""^(string_of_pos pos)^"\"];"

    let string_of_node = function
    | And      -> "A"
    | Xor      -> "X"
  end

  module Module = BinUbdag.TO_DOT(Model)

  let to_graphviz_file = Module.dotfile
end
