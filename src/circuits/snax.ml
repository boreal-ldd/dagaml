(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : Snax    : Shannon Negation And Xor
 *)

open GuaCaml
open STools
open BTools
open Extra
open O3Extra

type snax =
  | Sha of int
  | And
  | Xor

let strdump_snax = function
  | Sha var -> "(Sha "^(string_of_int var)^")"
  | And      -> "And"
  | Xor      -> "Xor"

let bindump_snax snax stream = match snax with
  | Sha var -> false::(ToB.int var stream)
  | And      -> true ::false::stream
  | Xor      -> true ::true ::stream

let binload_snax = function
  | false::stream ->
  (
    let var, stream = OfB.int stream in
    (Sha var, stream)
  )
  | true ::false::stream -> (And, stream)
  | true ::true ::stream -> (Xor, stream)
  | _ -> assert false

module ToBStream =
struct
  open ToBStream

  let snax cha : snax -> unit = function
    | Sha n -> (bool cha false; int cha n)
    | And -> (bool cha true; bool cha false)
    | Xor -> (bool cha true; bool cha true )
end

module OfBStream =
struct
  open OfBStream

  let snax cha = match bool cha with
    | false -> Sha(int cha)
    | true -> match bool cha with
      | false -> And
      | true  -> Xor
end

module Module =
struct
  module Model =
  struct
    type leaf = int option (* [Some var] = [var] | [None] = false *)
    type edge = bool
    type node = snax

    let string_of_leaf = ToS.(option int)
    let string_of_edge = ToS.bool
    let string_of_node = strdump_snax

    let iob_leaf = IoB.(option int)
    let iob_edge = IoB.bool
    let iob_node = (bindump_snax, binload_snax)

    type 'i next' = ('i, leaf) Tree.gnext
    type 'i edge' = edge * 'i next'
    type 'i node' = node * 'i edge' * 'i edge'

    let iob_next' iob_ident = TreeUtils.IoB.gnext iob_ident iob_leaf
    let iob_edge' iob_ident = iob_edge +* (iob_next' iob_ident)
    let iob_node' iob_ident = BinUbdag.default_iob_node
      (IoB.trio iob_node iob_edge iob_edge) iob_leaf iob_ident

    let __check_reverse__ = false
  end

  module M1 =
  struct
    module M = Model
    (* WIP *)
    let is_cst (b, nx) =
      match nx with
      | Tree.GLeaf None -> Some b
      | _ -> None

    let cst b = (b, Tree.GLeaf None)

    let cneg b0 (b1, nx1) = (b0<>b1, nx1)

    let medge (b, nx) = (b, Utils.MEdge nx)
    let push_node ((tag, ((n0, nx0) as f0), ((n1, nx1) as f1)) as node) =
      if f0 = f1
      then (
        match tag with
        | Sha _ -> f0 |> medge
        | And    -> f0 |> medge
        | Xor    -> cst false |> medge
      )
      else (
        match tag, is_cst f0, f0, is_cst f1, f1 with
        | And, Some false, f, _, _
        | And, _, _, Some false, f
        | And, Some true, _, _, f
        | And, _, f, Some true, _ -> f |> medge
        | Xor, Some b, _, _, f
        | Xor, _, f, Some b, _ -> cneg b f |> medge
        | _ -> (false, Utils.MNode node)
      )
    let compose negx (negy, nexty) = (negx<>negy, nexty)
  end

  include BinUbdagT.STDIO(M1)

  module TO_DOT_MODELE =
  struct
    module M = G0

    let string_of_leaf = function
      | Some var -> "X"^(string_of_int var)
      | None     -> "L0"

    let string_of_pos = function
      | None       -> "black"
      | Some false -> "red"
      | Some true  -> "green"

    let string_of_edge pos edge =
      "[label = \""^((function true -> "-" | false -> "+") edge)^"\"; color=\""^(string_of_pos pos)^"\"];"
    let string_of_node = function
    | Sha var -> "(C "^(string_of_int var)^")"
    | And      -> "A"
    | Xor      -> "X"
  end

  module TO_DOT = BinUbdag.TO_DOT(TO_DOT_MODELE)

  let dotfile = TO_DOT.dotfile

  module TO_NAX_MODELE =
  struct
    module M = G0

    type extra  = Nax.NAX.G1.manager
    type xnode  = Nax.NAX.G0.edge'
    type xnode' = BArray.t
    type xedge  = Nax.NAX.G0.edge'

    let o3_xnode = Nax.NAX.G0.o3b_edge'

    let rec_edge (neg, next) = match next with
      | Tree.GLink node -> let (neg', node) = node () in (neg<>neg', node)
      | Tree.GLeaf leaf -> (neg, Tree.GLeaf leaf)
    let map_node extra ((node, edge0, edge1):(unit -> xnode) M.M.node') =
      let edge0 = rec_edge edge0
      and edge1 = rec_edge edge1 in
      let push = Nax.NAX.G1.push extra in
      let negb (neg, node) = (not neg, node) in
      let (&!) edge0 edge1 = push(Nax.And, edge0, edge1) in
      let (|!) edge0 edge1 = negb((negb edge0)&!(negb edge1)) in
      match node with
      | Sha var -> (
        let leaf = Tree.GLeaf(Some var) in
        let x0 = (true, leaf) and x1 = (false, leaf) in
        (x0 &! edge0)|!(x1 &! edge1)
      )
      | And       -> push (Nax.And, edge0, edge1)
      | Xor       -> push (Nax.Xor, edge0, edge1)
    let map_edge extra edge = rec_edge edge

  end

  module TO_NAX = BinUbdag.EXPORT(TO_NAX_MODELE)

  module RENAME_MODELE =
  struct
    module M = G0

    type extra  = G0.manager * (int array)
    type xnode  = G0.edge'
    type xedge  = G0.edge'

    let rec_edge rename (neg, next) = match next with
      | Tree.GLink node -> let (neg', node) = node () in (neg<>neg', node)
      | Tree.GLeaf leaf -> match leaf with
        | None -> (neg, Tree.GLeaf None)
        | Some var -> (neg, Tree.GLeaf(Some rename.(var)))
    let map_node (man, rename) ((node, edge0, edge1):(unit -> xnode) M.M.node') =
      let edge0' = rec_edge rename edge0
      and edge1' = rec_edge rename edge1 in
      let node' = match node with
        | Sha var -> Sha(rename.(var))
        | And      -> And
        | Xor      -> Xor in
      G1.push man (node', edge0', edge1')
    let map_edge (man, rename) edge = rec_edge rename edge

  end

  module RENAME = BinUbdag.EXPORT_NOC(RENAME_MODELE)

  module TO_CNF_MODELE =
  struct
    module M = G0

    type extra = {
      cst0 : int;
      mutable input : int;
      mutable formule : (bool * int) list list;
    }
    type xnode = int
    type xedge = unit

    let rec_edge cst0 (neg, next) = (neg, (match next with
      | Tree.GLink node -> node()
      | Tree.GLeaf leaf -> (match leaf with Some var -> (var+1) | None -> cst0)))
    let map_node extra ((node, edge0, edge1):(unit -> xnode) M.M.node') =
      let edge0 = rec_edge extra.cst0 edge0
      and edge1 = rec_edge extra.cst0 edge1 in
      let root_var = extra.input + 1 in
      extra.input <- root_var;
      let root = (false, root_var) in
      let negb (neg, (node:int)) = (not neg, node) in
      let push clause = extra.formule <- clause::extra.formule in
      (match node with
      | Sha var ->
      (
        let var = (false, var+1) in
        push [var; root; negb edge0];
        push [var; negb root; edge0];
        push [negb var; root; negb edge1];
        push [negb var; negb root; edge1];
      )
      | And       ->
      (
        push [root; negb edge0; negb edge1];
        push [negb root; edge0];
        push [negb root; edge1]
      )
      | Xor       ->
      (
        push [root; edge0; negb edge1];
        push [root; negb edge0; edge1];
        push [negb root; edge0; edge1];
        push [negb root; negb edge0; negb edge1]
      ));
      root_var
    let map_edge extra edge =
      (* do not merge these two lines, there is an implicit dependency *)
      let clause = [rec_edge extra.cst0 edge] in
      extra.formule <- clause :: extra.formule;
  end

  module TO_CNF = BinUbdag.EXPORT_NOC(TO_CNF_MODELE)

  let export_cnf snax inputs edges =
    let extra = TO_CNF_MODELE.{
      cst0    = inputs+1;
      input   = inputs+1;
      formule = [[(true, inputs+1)]];
    } in
    let tocnf = TO_CNF.newman snax extra in
    List.iter (TO_CNF.rec_edge tocnf) edges;
    CnfTypes.CSTypes.{
      name = "cnf";
      input   = extra.TO_CNF_MODELE.input;
      quants  = []; (* no quantifier *)
      man     = ();
      (* formule = extra.TO_CNF_MODELE.formule *)
      (* FIXME *)
      formule = Tools.map (fun c ->
        Tools.map (fun (b, x) -> (x-1, b)) c
      ) extra.TO_CNF_MODELE.formule
    }

  module TEVAL_MODELE =
  struct
    module M = G0
    type extra = bool array
    type xnode = bool
    type xedge = bool

    let map_edge sigma (neg, next) = neg <> (match next with
      | Tree.GLink node -> node()
      | Tree.GLeaf None -> false
      | Tree.GLeaf (Some var) -> sigma.(var) )
    let map_node sigma ((node, edge0, edge1):(unit -> xnode) M.M.node') =
      match node with
      | Sha var -> if sigma.(var)
        then (map_edge sigma edge1)
        else (map_edge sigma edge0)
      | And -> (map_edge sigma edge0)&&(map_edge sigma edge1)
      | Xor -> (map_edge sigma edge0)<>(map_edge sigma edge1)
  end

  module TEVAL = BinUbdag.EXPORT_NOC(TEVAL_MODELE)

  let total_eval snax sigma edges =
    let teval = TEVAL.newman snax sigma in
    Array.map (TEVAL.rec_edge teval) edges

  module PEVAL_MODELE =
  struct
    module M = G0

    type extra  = G0.manager * (bool option array)
    type xnode  = G0.edge'
    type xedge  = G0.edge'

    let rec_edge rename (neg, next) = match next with
      | Tree.GLink node -> let (neg', node) = node () in (neg<>neg', node)
      | Tree.GLeaf leaf -> match leaf with
        | None -> (neg, Tree.GLeaf None)
        | Some var -> match rename.(var) with
          | None -> (neg, Tree.GLeaf(Some var))
          | Some cst -> (neg<>cst, Tree.GLeaf None)

    let ifdo_eval_shannon (man, rename) ((node, edge0, edge1):(unit -> xnode) M.M.node') : _ option =
      match node with
      | Sha var -> (
        match rename.(var) with
        | None -> None
        | Some cst ->
          Some(rec_edge rename (if cst then edge1 else edge0))
      )
      | _ -> None

    let map_node (man, rename) ((node, edge0, edge1):(unit -> xnode) M.M.node') =
      match ifdo_eval_shannon (man, rename) (node, edge0, edge1) with
      | Some e -> e
      | None -> (
        let edge0' = rec_edge rename edge0
        and edge1' = rec_edge rename edge1 in
        G1.push man (node, edge0', edge1')
      )
    let map_edge (man, rename) edge = rec_edge rename edge

  end

  module PEVAL = BinUbdag.EXPORT_NOC(PEVAL_MODELE)

  (* Section. Serialization *)

  module BW :
  sig
    val bw_leaf : Model.leaf bw
    val bw_edge : Model.edge bw
    val bw_node : Model.node bw

    val bw_next' : 'lk G0.bw_next'
    val bw_edge' : 'lk G0.bw_edge'
    val bw_node' : 'lk G0.bw_node'
  end =
  struct
    open BTools.ToBStream
    open TreeUtils.ToBStream
    open ToBStream

    let bw_leaf = option int
    let bw_edge = bool
    let bw_node = snax

    let bw_next' bw_link c x =
      (gnext bw_link bw_leaf c x)
    let bw_edge' bw_link c x =
      (bw_edge * (bw_next' bw_link)) c x
    let bw_node' bw_link c x =
      (trio bw_node (bw_edge' bw_link) (bw_edge' bw_link)) c x
  end

  module BR =
  struct
    open BTools.OfBStream
    open TreeUtils.OfBStream
    open OfBStream

    let br_leaf = option int
    let br_edge = bool
    let br_node = snax

    let br_next' br_link c =
      (gnext br_link br_leaf c)
    let br_edge' br_link c =
      (br_edge * (br_next' br_link)) c
    let br_node' br_link c =
      (trio br_node (br_edge' br_link) (br_edge' br_link)) c
  end

  (* remark : BRE is only required for pseudo-canonical models, e.g. l-nnux*)

  let bw_node_list man fl =
    G0.bw_node_list BW.bw_node'             man fl
  let bw_edge_list man fl =
    G0.bw_edge_list BW.bw_node' BW.bw_edge' man fl

  let br_node_list man =
    G0.br_node_list BR.br_node'             man
  let br_edge_list man =
    G0.br_edge_list BR.br_node' BR.br_edge' man

  let barray_of_edge_list man fl =
    barray_of_bw (bw_edge_list man) fl
  let edge_list_of_barray man ba =
    barray_of_br (br_edge_list man) ba

end

type 's system = ('s, Module.G1.manager, Module.G0.edge') IoTypes.system
type ssystem = string system
