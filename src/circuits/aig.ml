(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : Aig     : And Inverter Graph
 *)

open STools
open BTools
open Extra
open O3Extra

module AIG_MODELE =
struct
  module M =
  struct
    type leaf = int option (* [Some var] = [var] | [None] = false *)
    type edge = bool
    type node = unit

    let string_of_leaf = ToS.(option int)
    let string_of_edge = ToS.bool
    let string_of_node = ToS.unit

    let o3s_leaf = IoB.(option int)
    let o3s_edge = IoB.bool
    let o3s_node = IoB.unit

    type 'i next' = ('i, leaf) Tree.gnext
    type 'i edge' = edge * 'i next'
    type 'i node' = node * 'i edge' * 'i edge'

    let o3s_next' o3s_ident = Utils.o3s_gnode o3s_leaf o3s_ident
    let o3s_edge' o3s_ident = o3s_edge +* (o3s_next' o3s_ident)
    let o3s_node' o3s_ident = BinUbdag.default_o3s_node
      (IoB.trio o3s_node o3s_edge o3s_edge) o3s_leaf o3s_ident

    let __check_reverse__ = false
  end

  (* WIP *)
  let push_node node = (false, Utils.MNode node)
  let compose negx (negy, nexty) = (negx<>negy, nexty)
end

module AIG = BinUbdagT.MODULE(AIG_MODELE)

(*
module MIO =
struct
  type ctx = unit
  type 'link func =
    (
      bool * (int option),
      bool * 'link
    ) Tree.gnext
  type 'link node =
    unit * 'link func * 'link func

  let bw_func bw_link cha (func:'link func) =
  ToBStream.(
    Utils.bw_gnode
      (bool * (option int))
      (bool * (fun _ -> bw_link()))
        cha func
  )

  let br_func br_link cha =
  OfBStream.(
    Utils.br_gnode
      (bool * (option int))
      (bool * (fun _ -> br_link()))
        cha
  )

  let bw bw_link () cha (node:'link node) =
  ToBStream.(
    trio unit
      (bw_func bw_link)
      (bw_func bw_link)
        cha node
  )

  let br br_link () cha =
  OfBStream.(
    trio unit
      (br_func br_link)
      (br_func br_link)
        cha
  )

  let string_of_ctx = ToS.unit

  let string_of_edge string_of_link edge = ToS.(
    Utils.string_of_gnode
      (bool * (option int))
      (bool * string_of_link)
        edge
  )

  let string_of_node string_of_link node =
  ToS.(
    trio unit
      (string_of_edge string_of_link)
      (string_of_edge string_of_link)
        node
  )

  let __check_reverse__ = true
end

module VMOD =
struct
  open BTools
  module MIO = MIO

  let bw_ctx = ToBStream.unit
  let br_ctx =  OfBStream.unit

  type 'link node = 'link MIO.node

  let dump ((), f0, f1) =
    let dump_func = Utils.(function
      | Leaf lf -> Leaf lf
      | Node (ee, ((), lk)) -> Node (ee, lk)
    ) in
    ((), ((), dump_func f0, dump_func f1))

  let load ((), ((), f0, f1)) =
    let load_func = Utils.(function
      | Leaf lf -> Leaf lf
      | Node (ee, lk) -> Node (ee, ((), lk))
    ) in
    ((), load_func f0, load_func f1)

  let __check_reverse__ = true
end

module DAG0 = ABHTree.VABHTree.WeakIO(VMOD)

module MODTC =
struct

  (* external model *)
  type leaf = bool * (int option)
  type edge = bool

  (*
  val string_of_leaf : leaf -> string
  val string_of_edge : edge -> string
  *)

  type 'lk func =
    (      edge, leaf, 'lk) WUbdagTC.func'
  type 'lk node =
    (unit, edge, leaf, 'lk) WUbdagTC.node'

  let push_node ((), f0, f1) =
    Utils.MNode (false, ((), f0, f1))

  let pull_node id pull = function
    | Tree.GLeaf _ -> assert false
    | Tree.GLink (b, lk) -> (
      assert(b = false);
      let (), f0, f1 =
    )
    (), f0, f1) -> (
      let map_func
    )
    = Ldd_W_u_nu_gops.uncons

  let compose = Ldd_W_u_nu_gops.compose_func

  (* binary conversion *)
  let b3_leaf =
    Ldd_W_u_nu_io.(BW.leaf_state, BR.leaf_state)
  let b3_edge =
    Ldd_W_u_nu_io.(BW.edge_state, BR.edge_state)

  (* internal model *)
  type iman   = DAG0.Core.t
  type iident = ABHTree.ident
  type ilink  = DAG0.link'

  let ipush : iman -> ilink node -> ilink =
    DAG0.push
  let ipull : iman -> ilink -> ilink node =
    fun _ lk -> DAG0.pull lk
  let ident : ilink -> iident =
    DAG0.get_ident

  (* assert(x |> dump |> load = x) && assert(x |> load |> dump |> stream) *)
  let __check_reverse__ = true
end

module DAG1 = WUbdagTC.Make(MODTC)
*)

