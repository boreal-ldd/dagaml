(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : Expr    : basic boolean expression
 *)
open GuaCaml

(* Definition of type expr' : logical formulae in tree form *)
type uop = PNot | PNop
type bop = PAnd | POr | PIff | PImp | PXor
type 'a expr =
  | PCst of bool
  | PVar of 'a
  | PUop of uop * 'a expr
  | PBop of bop * 'a expr * 'a expr

module ToS =
struct
  open STools.ToS
  let uop = function
    | PNot -> "PNot"
    | PNop -> "PNop"

  let bop = function
    | PAnd -> "PAnd"
    | POr  -> "POr"
    | PIff -> "PIff"
    | PImp -> "PImp"
    | PXor -> "PXor"

  let rec expr var = function
    | PCst b -> "PCst "^(bool b)
    | PVar v -> "PVar "^(var v)
    | PUop (u, e) -> "PUop"^((uop * (expr var))(u, e))
    | PBop (u, e1, e2) -> "PBop"^(trio bop (expr var) (expr var) (u, e1, e2))
end

let no   x   = PUop (PNot, x)
let xorb b x = if b then no x else x
let (&!) x y = PBop (PAnd, x, y)
let (|!) x y = PBop (POr , x, y)
let (^!) x y = PBop (PXor, x, y)
let (=!) x y = PBop (PIff, x, y)
let cons c x0 x1 = ((no c)&!x0)|!(c&!x1)
let ite fc f1 f0 = (fc &! f1) |! ((no fc) &! f0)

let default_fold_left f d l =
  match l with
  | [] -> d
  | h::t -> List.fold_left f h t

let andl l = default_fold_left (&!) (PCst true ) l
let orl  l = default_fold_left (|!) (PCst false) l
let xorl l = default_fold_left (^!) (PCst false) l
let iffl l = default_fold_left (=!) (PCst true ) l

let string_of_uop = function
  | PNop -> "+"
  | PNot -> "-"

let string_of_bop = function
  | PAnd -> "&"
  | POr  -> "|"
  | PIff -> "<->"
  | PImp -> "->"
  | PXor -> "^"

open STools

let to_stree string_of_var =
  let dump_uop = ToSTree.leaf string_of_uop
  and dump_bop = ToSTree.leaf string_of_bop in
  let rec eval = function
    | PCst cst -> Tree.Leaf (if cst then "true" else "false")
    | PVar var -> Tree.Leaf (string_of_var var)
    | PUop (uop, expr) -> Tree.Node [dump_uop uop; eval expr]
    | PBop (bop, expr0, expr1) -> Tree.Node [dump_bop bop; eval expr0; eval expr1]
  in eval

let to_string string_of_var =
  let rec eval = function
    | PCst cst -> [if cst then "true" else "false"]
    | PVar var -> [string_of_var var]
    | PUop (uop, expr) -> ["("]@[string_of_uop uop]@(eval expr)@[")"]
    | PBop (bop, expr0, expr1) -> ["("]@(eval expr0)@[string_of_bop bop]@(eval expr1)@[")"]
  in
  fun expr -> String.concat " " (eval expr)

let rec map_var (f:'a -> 'b) : 'a expr -> 'b expr = function
  | PCst cst ->
      PCst cst
  | PVar var ->
      PVar(f var)
  | PUop (uop, expr) ->
      PUop(uop, map_var f expr)
  | PBop (bop, expr0, expr1) ->
      PBop(bop, map_var f expr0, map_var f expr1)

let rec count_var : _ expr -> int = function
  | PCst _ -> 0
  | PVar _ -> 1
  | PUop(_, e) -> count_var e
  | PBop(_, e0, e1) ->
    count_var e0 + count_var e1

(* [REUSE]
    Array.iter (fun (v, e) -> print_string v; print_string " := "; print_string(Expr.to_string (fun s -> s) e); print_newline()) sset;
 *)
