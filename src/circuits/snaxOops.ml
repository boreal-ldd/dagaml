(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : SnaxOops: OOPS-interface of Module
 *)
open GuaCaml
open Extra
open STools
open Snax

let print_endline s = ()

let newman () = Module.G0.newman()

module OOPS =
struct
  module Model =
  struct
    type t = Module.G0.manager
    type f = {arity : int; edge : Module.G0.edge'}

    let f_to_string (f:f) = ("{arity="^(ToS.int f.arity)^"; edge="^(Module.G0.string_of_edge' f.edge)^"}")

    let rename_of_blist blist = Array.of_list (MyList.indexes (fun x -> x) blist)
    let ( ->> ) t blist edge =
      let reman = Module.RENAME.newman t (t, (rename_of_blist blist)) in
      {arity = List.length blist; edge = Module.RENAME.rec_edge reman edge.edge}
    let arity _ edge = edge.arity
    (* where true represent significant variables and false non significant ones *)
    let   cneg _ b edge = {arity = edge.arity; edge = (b<>(fst(edge.edge)), (snd edge.edge))}
    let push tag t x y = Module.G1.push t (tag, x, y)
    let ( *! ) t f0 f1 =
      print_endline ("[ *! ] f0:"^(f_to_string f0));
      print_endline ("[ *! ] f1:"^(f_to_string f1));
      assert(f0.arity = f1.arity);
      let rename = Array.init f0.arity succ in
      let reman = Module.RENAME.newman t (t, rename) in
      let f0' = Module.RENAME.rec_edge reman f0.edge
      and f1' = Module.RENAME.rec_edge reman f1.edge in
      let f = {arity = succ f0.arity; edge = push (Sha 0) t f0' f1'} in
      print_endline ("[ *! ] f :"^(f_to_string f));
      f
    let andb t f0 f1 =
      assert(f0.arity = f1.arity);
      {arity = f0.arity; edge = push And t f0.edge f1.edge}
    let ( &! ) t f0 f1 =
      print_endline ("[ &! ] f0:"^(f_to_string f0));
      print_endline ("[ &! ] f1:"^(f_to_string f1));
      let f = andb t f0 f1 in
      print_endline ("[ &! ] f :"^(f_to_string f ));
      f
    let ( ^! ) t f0 f1 =
      assert(f0.arity = f1.arity);
      {arity = f0.arity; edge = push Xor t f0.edge f1.edge}
    let ( |! ) t f0 f1 =
      print_endline ("[ |! ] f0:"^(f_to_string f0));
      print_endline ("[ |! ] f1:"^(f_to_string f1));
      let f = cneg t true (andb t (cneg t true f0) (cneg t true f1)) in
      print_endline ("[ |! ] f :"^(f_to_string f ));
      f
    let ( =! ) t x y = cneg t true ((^!) t x y)

    let cst _ bool arity = {arity; edge = (bool, Tree.GLeaf None)}
    let var _ bool arity var = {arity; edge = (bool, Tree.GLeaf(Some var))}
    let to_bool _ edge = None

    let cofactor (t:t) (b:bool) (f:f) : f =
      let peval = Array.make f.arity None in
      peval.(0) <- Some b;
      let peman = Module.PEVAL.newman t (t, peval) in
      let f' = Module.PEVAL.rec_edge peman f.edge in
      let rename = Array.init f.arity pred in
      let reman = Module.RENAME.newman t (t, rename) in
      let f'' = Module.RENAME.rec_edge reman f' in
      {arity = f.arity-1; edge = f''}

    (* returns an identifier which can be used safely for memoization purpose *)
    let id (t:t) (f:f) : BTools.barray =
      let bw_sem cha f = Snax.Module.bw_edge_list t cha [f] in
      let bw cha f = BTools.ToBStream.(int * bw_sem) cha (f.arity, f.edge) in
      BTools.barray_of_bw bw f

    (* returns [true] iff both inputs are syntactically identical *)
    let eq (t:t) (f0:f) (f1:f) : bool = (f0 = f1)

    (* creates a new manager *)
    let newman () : t = Module.G0.newman()

    (* copies a list of function from a given manager into another one *)
    let copy_into (t0:t) (fl0:f list) (t1:t) : f list =
      let map_edge = Module.G1.copy_into t0 t1 in
      Tools.map (fun f -> {f with edge = map_edge f.edge}) fl0

    open BTools

    let bw_fl ?(nocopy=false) ?(destruct=false) (t:t) cha (fl:f list) : unit =
      let t', fl' =
        if nocopy then (t, fl) else (
          let t'  = newman() in
          (t', copy_into t fl t')
        )
      in
      let aries = MyList.map (fun f -> f.arity) fl' in
      ToBStream.(list int cha aries);
      let edges = MyList.map (fun f -> f.edge) fl' in
      Module.bw_edge_list t' cha edges;
      ()

    let br_fl ?(t=(None:(t option))) cha : t * f list =
      let t = match t with Some t -> t | None -> newman() in
      let aries = OfBStream.(list int) cha in
      let edges = Module.br_edge_list t cha in
      (t, MyList.map2 (fun arity edge -> {arity; edge}) aries edges)

    (* serializes a list of function from a given manager *)
    (* [TODO] implement ~destruct:true *)
    let to_barray ?(nocopy=false) ?(destruct=false) (t:t) (fl:f list) : BTools.BArray.t =
      let t', fl' =
        if nocopy then (t, fl) else (
          let t'  = newman() in
          (t', copy_into t fl t')
        )
      in
      let cha = ToBStream.Channel.open_barray () in
      let aries = MyList.map (fun f -> f.arity) fl' in
      ToBStream.(list int cha aries);
      let edges = MyList.map (fun f -> f.edge) fl' in
      Module.G0.bw t' cha edges;
      ToBStream.Channel.close_barray cha

    (* unserializes a list of function into a given manager *)
    let of_barray ?(t=(None:(t option))) (ba:BTools.BArray.t) : t * f list =
      let cha = OfBStream.Channel.open_barray ba in
      let a' = OfBStream.(list int cha) in
      let t', e' = Module.G0.br cha in
      OfBStream.Channel.close_barray cha;
      let fl' = MyList.map2 (fun arity edge -> {arity; edge}) a' e' in
      match t with
      | Some t -> (t , copy_into t' fl' t)
      | None   -> (t', fl')

    let t_stats (t:t) : Tree.stree =
      Module.G0.dump_stats t

    let f_stats (t:t) (fl:f list) : Tree.stree =
      Module.G0.dump_estats t (fl||>(fun e -> e.edge))

    (* no cache available *)
    let clear_caches (t:t) : unit = ()

    let keep_clean ?(normalize=false) (t:t) (fl:f list) : f list =
      clear_caches t;
      let arity = fl ||> (fun f -> f.arity) in
      let edges = fl ||> (fun f -> f.edge) in
      let edges' = Module.G0.keep_clean ~normalize t edges in
      MyList.map2 (fun arity edge -> {arity; edge}) arity edges'

    let check t f = Module.G0.traverse t f.edge

    module F =
    struct
      type f' = {
        mutable alive : bool;
        file_name : string;
        arity : int;
        size : int; (* in bytes *)
      }

      let arity f' = f'.arity
      let size  f' = f'.size

      let prefix = "biggy_"
      let suffix = ".snax.f.pure"

      let of_f ?(nocopy=false) ?(normalize=true) (t:t) (f:f) : f' =
        let file_name = Filename.temp_file prefix suffix in
        let cha = open_out_bin file_name in
        if nocopy
        then (
          Module.G0.ToF.man_edges ~normalize t cha [f.edge];
        )
        else (
          let t' = newman () in
          let f' = Module.G0.copy_into t t' f.edge in
          Module.G0.ToF.man_edges ~normalize ~destruct:true t cha [f'];
        );
        close_out cha;
        let stat = Unix.stat file_name in
        {alive = true; file_name; arity = f.arity ; size = stat.Unix.st_size}

      let to_f (t:t) (f':f') : f =
        assert(f'.alive);
        let cha = open_in_bin f'.file_name in
        let man, fl = Module.G0.OfF.man_edges cha in
        assert(List.length fl = 1);
        let edge = Module.G1.copy_into man t (List.hd fl) in
        {arity = f'.arity; edge}

      let free (f':f') : unit =
        assert(f'.alive);
        f'.alive <- false;
        Sys.remove f'.file_name;
        ()

      let tof ?(normalize=true) (cha:out_channel) (fl:f' list) : unit =
        assert(List.for_all (fun f -> f.alive) fl);
        let fa = fl ||> (fun f -> open_in_bin f.file_name) |> Array.of_list in
        Module.G0.combine_layerized_file ~normalize fa cha;
        Array.iter close_in fa;
        Io.ToF.(sized_array int) cha (fl ||> (fun f -> f.arity) |> Array.of_list);
        ()

      let off (cha:in_channel) : f' list =
        let man, edges = Module.G0.OfF.man_edges cha in
        let aries = Io.OfF.(sized_array int) (List.length edges) cha |> Array.to_list in
        MyList.map2 (fun edge arity -> of_f man {edge; arity}) edges aries
    end

    let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) (t:t) (cha:out_channel) (fl:f list) : unit =
      let t', fl' =
        if nocopy then (t, fl) else (
          let t'  = newman() in
          (t', copy_into t fl t')
        )
      in
      let aries = MyList.map (fun f -> f.arity) fl' in
      let edges = MyList.map (fun f -> f.edge) fl' in
      Module.G0.ToF.man_edges t ~normalize ~destruct cha edges;
      Io.ToF.(sized_array int) cha (Array.of_list aries);
      ()

    let off (cha:in_channel) : (t * (f list)) =
      let man, edges = Module.G0.OfF.man_edges cha in
      let aries = Io.OfF.(sized_array int) (List.length edges) cha |> Array.to_list in
      (man, MyList.map2 (fun edge arity -> {edge; arity}) edges aries)

  end

  include OOPS.Make(Model)

  module SUInt = SUInt.Make(Model) (* fixed Size Unsigned INTeger *)
  module VUInt = VUInt.Make(Model) (* Variable size Unsigned INTeger *)
  module LoadCnf0 = LoadCnf.Make0(Model)
  module LoadCnfA = LoadCnf.MakeA(Model)
end

type 's system =
  ('s, Module.G1.manager, Module.G0.edge') IoTypes.system

let system_of_oops_system (s:'s OOPS.system) : 's system =
  IoTypes.{
    name = s.name;
    man  = s.man;
    inps = s.inps;
    regs = s.regs;
    wire = s.wire;
    outs = s.outs;
    sset = s.sset
      |> Array.map (fun (s, e) -> (s, e.OOPS.Model.edge))
  }
