open GuaCaml
type 'a expr0 =
  | AVar of bool * 'a
  | NAnd of 'a expr0 list
type 'a expr1 = R0 | RE of 'a expr0
type 'a expr = bool * 'a expr1

module ToS =
struct
  open STools.ToS

  let rec expr0 var = function
    | AVar (b, v) -> "AVar"^((bool * var)(b, v))
    | NAnd el -> "NAnd"^(list (expr0 var) el)
  let expr1 var = function
    | R0 -> "R0"
    | RE re -> "RE("^(expr0 var re)^")"
  let expr var = bool * (expr1 var)
end

(* Definition : nand primitive *)

let no ((b, e0):'a expr) : 'a expr = (not b, e0)
let xorb b0 ((b, e0):'a expr) : 'a expr = (b0 <> b, e0)

let rec no0 = function
  | AVar (b, v) -> AVar(not b, v)
  | NAnd el     -> NAnd(Tools.map no0 el)

let xorb0 b e0 =
  if b then no0 e0 else e0

let nand_var (b1, v1) (b2, e2) =
  let ve = AVar(b1, v1) in
  match e2 with
  | AVar(b2', v2) -> NAnd[ve; AVar(b2<>b2', v2)]
  | NAnd el -> (
    assert(el<>[]);
    if b2
    then NAnd(ve::el)
    else NAnd[ve; e2]
  )

let nandb0 (b1, e1) (b2, e2) =
  match b1, e1, b2, e2 with
  | b1, AVar(b1', v1), b2, e2
  | b2, e2, b1, AVar(b1', v1) -> nand_var (b1<>b1', v1) (b2, e2)
  | b1, NAnd el1, b2, NAnd el2 -> (
    assert(el1<>[]); assert(el2<>[]);
    match b1, b2 with
    | true , true  -> NAnd(el1 @    el2)
    | false, true  -> NAnd[e1  ; no0 e2]
    | true , false -> NAnd[e2  ; no0 e1]
    | false, false -> NAnd[e1  ;     e2]
  )

let nandb (e1:'a expr) (e2:'a expr) : 'a expr =
  match e1, e2 with
  | ((b1, R0) as e1), e2
  | e2, ((b1, R0) as e1) -> if b1 then e2 else e1
  | (b1, RE e1), (b2, RE e2) ->
    (false, RE(nandb0 (b1, e1) (b2, e2)))

(* Definition : basic operators *)

let ( &! ) e1 e2 = no(nandb e1 e2)
let ( |! ) e1 e2 = nandb(no e1)(no e2)
let ( ^! ) e1 e2 = ((no e1)&!(no e2))|!(e1&!e2)
let ( =! ) e1 e2 = no(e1 ^! e2)
let ( ->! ) e1 e2 = (no e1)|!e2

let cst b = (b, R0)
let var v = (false, RE(AVar(false, v)))

(* Definition : main conversion function *)

let rexpr_of_expr (e:'a Expr.expr) : 'a expr =
  let rec go = function
    | Expr.PCst b -> cst b
    | Expr.PVar v -> var v
    | Expr.PUop(Expr.PNop, e') -> go e'
    | Expr.PUop(Expr.PNot, e') -> no(go e')
    | Expr.PBop(Expr.PAnd, e1, e2) -> (go e1) &!  (go e2)
    | Expr.PBop(Expr.POr , e1, e2) -> (go e1) |!  (go e2)
    | Expr.PBop(Expr.PIff, e1, e2) -> (go e1) =!  (go e2)
    | Expr.PBop(Expr.PImp, e1, e2) -> (go e1) ->! (go e2)
    | Expr.PBop(Expr.PXor, e1, e2) -> (go e1) ^!  (go e2)
  in go e

let strdump
    (dump_var: 'a  -> string)
    (dump_cst:bool -> string)
    (dump_bop:bool -> string)
    (dump_neg:bool -> string) : 'a expr -> string =
  let rec expr0 b0 = function
    | AVar(b, v) -> (dump_neg(b0<>b))^(dump_var v)
    | NAnd el    -> (
      let bop = dump_bop (not b0) in
      STools.SUtils.catmap bop (expr0 (not b0)) el
    )
  in
  let     expr (b, e1) =
    match e1 with
    | R0    -> dump_cst b
    | RE e0 -> expr0 b e0
  in
  expr
