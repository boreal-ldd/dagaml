(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : cicuits : representation and manipulation of circuits
 *
 * module  : NaxOops : OOPS-interface of NAX
 *)


(* [WIP] *)
(*
module ExportOops(MO:OOPS.MSig) =
struct
  module Model =
  struct
    module M = Nax.NAX.G0

    type extra  = MO.t
    type xnode  = MO.f

    let rec_edge (neg, next) = match next with
      | Tree.GLeaf leaf -> (neg, Tree.GLeaf leaf)
      | Tree.GLink node -> let (neg', node) = node () in (neg<>neg', node)
    let map_node extra ((node, edge0, edge1):(unit -> xnode) M.M.node') =
      let edge0 = rec_edge edge0
      and edge1 = rec_edge edge1 in
      let push = Nax.NAX.push extra in
      let negb (neg, node) = (not neg, node) in
      let (&!) edge0 edge1 = push(Nax.And, edge0, edge1) in
      let (|!) edge0 edge1 = negb((negb edge0)&!(negb edge1)) in
      match node with
      | Cons var ->
      (
        let leaf = Tree.GLeaf(Some var) in
        let x0 = (true, leaf) and x1 = (false, leaf) in
        (x0 &! edge0)|!(x1 &! edge1)
      )
      | And       -> push (Nax.And, edge0, edge1)
      | Xor       -> push (Nax.Xor, edge0, edge1)
    let map_edge extra edge = rec_edge edge

  end

  module Module = BinUbdag.EXPORT_NOC(Model)

  let newman (pure:Module.G1.manager.t) (oman:MO.t) : Module.t =
    Module.newman pure Model.{oman}

  let conv (man:Module.t) (fl:LDD.f list) : MO.f list =
    Tools.map (fun f -> Module.rec_fun man () f) fl
end
*)
