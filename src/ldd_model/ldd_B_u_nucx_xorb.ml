(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_xorb :  Binary Operator Xor
 *)

open GuaCaml
open AB
open Utils

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_pull
open Ldd_B_u_nucx_cons
open Ldd_B_u_nucx_io

let solve_xor_id_id noutput b0 x0 b1 x1 =
  if x0 = x1
  then cst_merge3 (b0<>b1) noutput
  else (
    let rU = MyList.init noutput (fun i -> if i = x0 || i = x1 then A() else B()) in
    let rX = [B(); B()] in
    let decomp = UCX(rU, [A rX]) in
    merge3_of_edge ({neg = b0<>b1; noutput; ninput = None; decomp}, Tree.GLeaf())
  )

let extract_rowUX_block block =
  match block.decomp with
  | UCX(rU, (A rX)::mat) -> (
    let noutput = MyList.count isA rX in
    let decomp = UCX(MyList.make noutput (A()), mat) in
    (compose_A rU rX, {block with noutput; decomp})
  )
  | UCX(rU, mat) -> (
    let noutput = MyList.count isA rU in
    let decomp = UCX(MyList.make noutput (A()), mat) in
    let rUX =
      Tools.map (function A() -> A() | B() -> B(A())) rU
    in
    (rUX, {block with noutput; decomp})
  )
  | _ -> assert false

let compose_rowUX_block rowUX block =
  let rU, rX = extract_A rowUX in
  block
  |> compose_rowCX_block (A rX)
  |> compose_rowU_block rU

let rec solve_xor ((eeX, nxX) as peX) ((eeY, nxY) as peY) =
  assert(eeX.noutput = eeY.noutput);
  match eeX.decomp, peX, eeY.decomp, peY with
  | C0, peC, _, peX
  | _, peX, C0, peC -> (merge3_of_edge (compose_neg_edge (fst peC).neg peX))
  | Id iX, _, Id iY, _ ->
    (solve_xor_id_id eeX.noutput eeX.neg iX eeY.neg iY)
  | Id _, peX, _, peY
  | _, peY, Id _, peX -> (merge3_node peX peY) (* [TODO] improve treatment of (Id/UCX) case *)
  | UCX matX, peX, UCX matY, peY -> (
    if nxX = nxY && matX = matY
    then (cst_merge3 (eeX.neg <> eeY.neg) eeX.noutput)
    else
      let rUXY, (rUX, rUY) = facto_B (fst matX) (fst matY) in
      if listAB_allA rUXY
      then (solve_xor_extract_rowCX peX peY)
      else (
        let peX' = subst_matUCX_in_edge (rUX, snd matX) peX
        and peY' = subst_matUCX_in_edge (rUY, snd matY) peY in
        compose_rowU_merge3 rUXY (solve_xor_extract_rowCX peX' peY')
      )
  )
and      solve_xor_extract_rowCX peX peY =
  let bX, peX' = extract_neg_from_edge peX
  and bY, peY' = extract_neg_from_edge peY in
  compose_neg_merge3 (bX<>bY) (merge3_node peX' peY')

let solve_xor ((eeX, nxX) as peX) ((eeY, nxY) as peY) =
  assert(arity_edge peX = arity_edge peY);
  (* print_endline ("[solve_xor] fst peX: "^(ToS.block eeX)); *)
  if do_check then assert(check_edge peX);
  (* print_endline ("[solve_xor] fst peY: "^(ToS.block eeY)); *)
  if do_check then assert(check_edge peY);
  let peXY = solve_xor peX peY in
  (* print_endline ("[solve_xor] fst peXY: "^(ToS.block (fst peXY))); *)
  assert(arity_merge3 peXY = arity_edge peY);
  if do_check then assert(check_merge3 peXY);
  peXY
