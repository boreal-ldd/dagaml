(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-nucx : The Ordered model of Order 1, includes :
 *   Negation, Useless Variables, Xor variables, Canalizing variables.
 *
 * module  : Ldd_o_nucx_avanced : Generic Functional Interface
 *)

open GuaCaml
open BTools
open Ldd_B_o_nucx

module ComputeSupport =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = unit
    type param = unit

    type xnode = BRLE.t
    type xedge = BRLE.t

    let map_next (():extra) (():param) : _ -> xedge = function
      | Tree.GLeaf k -> BRLE.single (k, false)
      | Tree.GLink lk -> lk ()

    let rec map_tlist ?(carry=BRLE.empty) tl nx =
      match tl with
      | [] -> BRLE.rev_append carry (map_next () () nx)
      | (e, n)::tl' -> (
        map_tlist
          ~carry:(BRLE.cons (n, (e <> ThisT.U)) carry)
           tl' nx
      )

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (():param) (((n, _, tl), nx): _ M.M.edge') : xedge =
      let r = map_tlist tl nx in
      assert(BRLE.length r = n);
      r

    let map_node (():extra) (():param) (((), f0, f1): _ M.M.node') : xnode =
      let s0 = map_edge () () f0
      and s1 = map_edge () () f1 in
      BRLE.push true (BRLE.bw_or s0 s1)
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.manager (* LDD-Support manager, defined for [Support] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : BRLE.t =
    Module.rec_edge tt () f

  let solve_ilist (tt:tt) (f:LDD.f) : int list =
    solve tt f |> BRLE.to_int_list
end

module ToSTree =
struct
  open STools
  open ToSTree
  module Model =
  struct
    module M = LDD.G0

    type extra = unit
    type param = Tree.stree list

    type xnode = Tree.stree
    type xedge = Tree.stree

    let map_next (param:param) : _ -> xedge = function
      | Tree.GLeaf lf -> int lf
      | Tree.GLink lk -> (lk param)

    let rec map_tlist (param:param) tl nx : xedge =
      match tl with
      | []  -> map_next param nx
      | (e, n)::tl' -> (
        assert(n > 0);
        let hd, param' = MyList.hdtl_nth n param in
        Tree.Node [
          Tree.Leaf (ThisG.ToS.tag e);
          Tree.Node hd;
          map_tlist param' tl' nx
        ]
      )

    let map_cneg (neg:bool) (s:xedge) : xedge =
      Tree.Node [Tree.Leaf "CNEG"; bool neg; s]

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (param:param) (((_, b, tl), nx): _ M.M.edge') : xedge =
      map_cneg b (map_tlist param tl nx)

    let map_node (():extra) (param:param) ((_, f0, f1): _ M.M.node') : xnode =
      match param with
      | [] -> assert false
      | hd::tl -> (
        let s0 = map_edge () tl f0
        and s1 = map_edge () tl f1 in
        Tree.Node [Tree.Leaf "ITE"; hd; s0; s1]
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-ToSTree manager, defined for [ToSTree] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve ?(inputs=None) (tt:tt) (f:LDD.f) : Tree.stree =
    let ari = ThisG.arity f in
    let inputs =
      match inputs with
      | Some inputs -> (
        assert(List.length inputs = ari);
        inputs
      )
      | None -> (
        List.init ari (fun x -> Tree.Node [Tree.Leaf "VAR"; Tree.Leaf (string_of_int x)])
      )
    in
    Module.rec_edge tt inputs f

  let solve_to_string ?(inputs=None) (tt:tt) (f:LDD.f) : string =
    let stree = solve ~inputs tt f in
    OfSTree.to_string stree

end

module CntSat =
struct
  open BTools
  module Model =
  struct
    module M = LDD.G0
    open BArray.Nat

    type extra = unit
    type param = unit

    type xnode = int * nat
    type xedge = int * nat

    let map_next : _ -> xedge = function
      | Tree.GLeaf lf -> (lf, zero())
      | Tree.GLink lk -> lk ()

    let push_neg ((a, cnt):xedge) : xedge =
      (a, (pow2 a) -/ cnt)

    let push_cneg b (xe:xedge) : xedge =
      if b then push_neg xe else xe

    let push_U nU ((a, cnt):xedge) : xedge =
      assert(nU >= 0);
      (nU+a, shift_lefti cnt nU)

    let push_C th0 nC ((a, cnt):xedge) : xedge =
      assert(nC >= 0);
      if th0
      then (nC+a, (pow2 (nC+a) -/ pow2 a) +/ cnt)
      else (nC+a, cnt)

    let rec push_tlist (a:int) tl nx : xedge =
      match tl with
      | [] -> map_next nx
      | (e, n)::tl' -> (
        match e with
        | ThisT.U -> push_U n (push_tlist (a-n) tl' nx)
        | ThisT.C(_, th0) -> push_C th0 n (push_tlist (a-n) tl' nx)
        | ThisT.X -> (a, pow2 (a-1))
      )

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (():param) (((a, b, tl), nx): _ M.M.edge') : xedge =
      push_cneg b (push_tlist a tl nx)

    let ( +// ) (ax, cx) (ay, cy) =
      assert(ax = ay);
      (ax, cx +/ cy)

    let map_node (():extra) (():param) ((_, f0, f1): _ M.M.node') : xnode =
      (map_edge () () f0) +// (map_edge () () f1)
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-CntSat manager, defined for [CntSat] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : BNat.nat =
    Module.rec_edge tt () f |> snd

end

(* Forall Relative Naming : eliminate quantified variables from the ordering *)
module ForallR =
struct
  module Model =
  struct
    module M = LDD.G1

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }
    type param = bool list (* Utils.quant *)

    type xnode = LDD.f
    type xedge = LDD.f

    type solve = param -> xedge -> xedge

    let map_next (extra:extra) (solve:solve) (param:param) a neg nx : xedge =
      match nx with
      | Tree.GLeaf lf -> cst neg (MyList.count_false param)
      | _ -> (
        let p0 = MyList.count_false param in (* number of non quantified variables *)
        let p1 = MyList.count_true  param in (* number of quantified variables *)
             if p1 = 0 (* all remaining variables are not quantified *)
        then ((p0, false, []), nx)
        else if p0 = 0 (* all remaining variables are quantified and [f] is not a constant *)
        then (cst false 0)
        else (solve param ((a, neg, []), nx))
      )

    let rec map_tlist (extra:extra) (solve:solve) (param:param) (a:int) (neg:bool) tl nx : xedge =
      match tl with
      | [] -> map_next extra solve param a neg nx
      | (e, n)::tl' -> (
        assert(n > 0);
        let hd, param' = MyList.hdtl_nth n param in
        let f () = map_tlist extra solve param' (a-n) neg tl' nx in
        let p0 = MyList.count_false hd in (* non-quantified variables *)
        let p1 = MyList.count_true  hd in (* quantified variables *)
        match e with
        | ThisT.U ->
          ThisG.push_U ~n:p0  (f())
        | ThisT.X -> (
          if p1 > 0 (* at least one X variable has been quantified *)
          then cst false (MyList.count_false param)
          else solve param ((a, neg, tl), nx)
        )
        | ThisT.C(if0, th0) when neg <> th0 = false && p1 > 0 ->
          cst false (MyList.count_false param)
        | ThisT.C(if0, th0) ->
          ThisG.push_C ~n:p0 if0 th0 (f())
      )

    (* val map_edge : extra -> solve -> param -> M.G.edge' -> xedge *)
    let map_edge (extra:extra) (solve:solve) (param:param) ((a, neg, tl), nx) : xedge =
      map_tlist extra solve param a neg tl nx

    let map_node (extra:extra) (solve:solve) (param:param) ((), f0, f1) : xedge =
      match param with
      | [] -> assert false
      | b0::param' -> (
        let g0 = map_edge extra solve param' f0
        and g1 = map_edge extra solve param' f1 in
        if b0
        (* b0 = true  : eliminate the variable *)
        then (LDD.And.solve extra.and_man g0 g1)
        (* b0 = false : keep the variable *)
        else (LDD.G1.push extra.ldd_man ((), g0, g1))
      )
  end

  module Module = AriUbdagTC.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.tt (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man ~hsize extra in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* Forall Absolute Naming (V0) : does not change variable names *)
(* remark:
  absolute naming can be emulated using either :
    - a wrapper for support variables, or
    - LDD-U-U based models (with uniform useless variables)
 *)
module ForallA0 =
struct
  module Model =
  struct
    module M = LDD.G1

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }
    type param = bool list

    type xnode = LDD.f
    type xedge = LDD.f

    type solve = param -> xedge -> xedge

    let map_next (extra:extra) (solve:solve) (param:param) a neg nx : xedge =
      match nx with
      | Tree.GLeaf lf -> cst neg a
      | _ -> (
        let p0 = MyList.count_false param in (* number of non quantified variables *)
        let p1 = MyList.count_true  param in (* number of quantified variables *)
             if p1 = 0 (* no remaining variable is quantified *)
        then ((a, neg, []), nx)
        else if p0 = 0 (* all remaining variables are quantified and [f] is not a constant *)
        then (cst false a)
        else (solve param ((a, neg, []), nx))
      )

    let map_C hd if0 th0 (f:xedge) : xedge =
      let rec map_C_rec rhd if0 th0 (f:xedge) : xedge =
        match rhd with
        | [] -> f
        | qv::rhd' -> (
          let f' =
            match qv with
            | false -> ThisG.push_C if0 th0 f (* non-quantified variable *)
            | true  -> ThisG.push_U         f (*     quantified variable *)
          in
          map_C_rec rhd' if0 th0 f'
        )
      in map_C_rec (List.rev hd) if0 th0 f

    let rec map_tlist (extra:extra) (solve:solve) (param:param) (a:int) (neg:bool) tl nx : xedge =
      match tl with
      | [] -> map_next extra solve param a neg nx
      | (e, n)::tl' -> (
        assert(n > 0);
        let hd, param' = MyList.hdtl_nth n param in
        let f () = map_tlist extra solve param' (a-n) neg tl' nx in
     (* let p0 = MyList.count_false hd in (* non-quantified variables *) *)
        let p1 = MyList.count_true  hd in (*     quantified variables *)
        match e with
        | ThisT.U ->
          ThisG.push_U ~n (f())
        | ThisT.X -> (
          if p1 > 0 (* at least one X variable has been quantified *)
          then cst false a
          else solve param ((a, neg, tl), nx)
        )
        | ThisT.C(if0, th0) -> (
          match neg <> th0 with
          | false -> (
            if p1 > 0
            then cst false a
            else ThisG.push_C ~n if0 false (f())
          )
          | true  -> (
            map_C hd if0 true (f())
          )
        )
      )

    (* val map_edge : extra -> solve -> param -> M.G.edge' -> xedge *)
    let map_edge (extra:extra) (solve:solve) (param:param) ((a, neg, tl), nx) : xedge =
      map_tlist extra solve param a neg tl nx

    let map_node (extra:extra) (solve:solve) (param:param) ((), f0, f1) : xedge =
      match param with
      | [] -> assert false
      | b0::param' -> (
        let g0 = map_edge extra solve param' f0
        and g1 = map_edge extra solve param' f1 in
        assert(arity g0 = arity g1);
        if b0
        (* b0 = true  : eliminate the variable *)
        then (ThisG.push_U (LDD.And.solve extra.and_man g0 g1))
        (* b0 = false : keep the variable *)
        else (LDD.G1.push extra.ldd_man ((), g0, g1))
      )
  end

  module Module = AriUbdagTC.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.tt (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man ~hsize extra in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* Forall Absolute Naming (V1) : does not change variable names *)
(* remark:
  absolute naming can be emulated using either :
    - a wrapper for support variables, or
    - LDD-U-U based models (with uniform useless variables)
 *)
module ForallA =
struct
  module Model =
  struct
    module M = LDD.G1

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }
    type param = BRLE.t

    type xnode = LDD.f
    type xedge = LDD.f

    type solve = param -> xedge -> xedge

    let map_next (extra:extra) (solve:solve) (param:param) a neg nx : xedge =
      match nx with
      | Tree.GLeaf lf -> cst neg a
      | _ -> (
        (* no remaining variable is quantified *)
             if BRLE.forall false param
        then ((a, neg, []), nx)
        (* all remaining variables are quantified and [f] is not a constant *)
        else if BRLE.forall true  param
        then (cst false a)
        else (solve param ((a, neg, []), nx))
      )

    let map_C (hd:BRLE.t) if0 th0 (f:xedge) : xedge =
      let rec map_C_rec (rhd:BRLE.t) if0 th0 (f:xedge) : xedge =
        match BRLE.uncons rhd with
        | None -> f
        | Some((n, qv), rhd') -> (
          let f' =
            match qv with
            | false -> ThisG.push_C if0 th0 ~n f (* non-quantified variable *)
            | true  -> ThisG.push_U         ~n f (*     quantified variable *)
          in
          map_C_rec rhd' if0 th0 f'
        )
      in map_C_rec (BRLE.rev hd) if0 th0 f

    let rec map_tlist (extra:extra) (solve:solve) (param:param) (a:int) (neg:bool) tl nx : xedge =
      assert(BRLE.length param = a);
      match tl with
      | [] -> map_next extra solve param a neg nx
      | (e, n)::tl' -> (
(*        print_newline();
        print_endline ("[Ldd_B_o_nucx_advanced.ForallA.map_tlist] param:"^(BRLE.ToS.t param));
        print_endline ("[Ldd_B_o_nucx_advanced.ForallA.map_tlist] n:"^(STools.ToS.int n)); *)
        let hd, param' = BRLE.hdtl_nth n param in
(*        print_endline ("[Ldd_B_o_nucx_advanced.ForallA.map_tlist] hd:"^(BRLE.ToS.t hd));
        print_endline ("[Ldd_B_o_nucx_advanced.ForallA.map_tlist] param':"^(BRLE.ToS.t param'));
        assert(param = BRLE.append hd param');
        assert(BRLE.expand param = List.append (BRLE.expand hd) (BRLE.expand param')); *)
        let f () = map_tlist extra solve param' (a-n) neg tl' nx in
     (* let p0 = MyList.count_false hd in (* non-quantified variables *) *)
        let p1 = BRLE.count_true  hd in (*     quantified variables *)
        match e with
        | ThisT.U ->
          ThisG.push_U ~n (f())
        | ThisT.X -> (
          if p1 > 0 (* at least one X variable has been quantified *)
          then cst false a
          else solve param ((a, neg, tl), nx)
        )
        | ThisT.C(if0, th0) -> (
          match neg <> th0 with
          | false -> (
            if p1 > 0
            then cst false a
            else ThisG.push_C ~n if0 false (f())
          )
          | true  -> (
            map_C hd if0 true (f())
          )
        )
      )

    (* val map_edge : extra -> solve -> param -> M.G.edge' -> xedge *)
    let map_edge (extra:extra) (solve:solve) (param:param) ((a, neg, tl), nx) : xedge =
      map_tlist extra solve param a neg tl nx

    let map_node (extra:extra) (solve:solve) (param:param) ((), f0, f1) : xedge =
      let b0, param' = BRLE.pull param in
      let g0 = map_edge extra solve param' f0
      and g1 = map_edge extra solve param' f1 in
      assert(arity g0 = arity g1);
      if b0
      (* b0 = true  : eliminate the variable *)
      then (ThisG.push_U (LDD.And.solve extra.and_man g0 g1))
      (* b0 = false : keep the variable *)
      else (LDD.G1.push extra.ldd_man ((), g0, g1))
  end

  module Module = AriUbdagTC.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.tt (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man ~hsize extra in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:BRLE.t) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* Partial Evaluation (Relative Naming) *)
module PEvalR =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.t;  (* LDD manager *)
    }
    type param = bool option list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_next (extra:extra) (param:param) (a:int) nx : xedge =
      match nx with
      | Tree.GLeaf _  -> cst false (MyList.count_None param)
      | Tree.GLink lk -> lk param

    let map_U hd (fx:unit -> xedge) : xedge =
      ThisG.push_U ~n:(MyList.count_None hd) (fx())
    let map_X hd (fx:unit -> xedge) : xedge =
      let rec get_cneg carry = function
        | [] -> carry
        | (Some v)::hd' -> get_cneg (v <> carry) hd'
        |  None   ::hd' -> get_cneg       carry  hd'
      in
      ThisG.cneg
        (get_cneg false hd)
        (ThisG.push_X ~n:(MyList.count_None hd) (fx()))
    let map_C hd if0 th0 (a:int) (fx:unit -> xedge) : xedge =
      let rec get_cond = function
        | [] -> false
        | (Some v)::hd' when v = if0 -> true
        |       _ ::hd' -> get_cond hd'
      in
      if get_cond hd
      then cst th0 a
      else (ThisG.push_C ~n:(MyList.count_None hd) if0 th0 (fx()))

    let rec map_tlist (extra:extra) (param:param) (a:int) tl nx : xedge =
      match tl with
      | [] -> map_next extra param a nx
      | (e, n)::tl' -> (
        assert(n > 0);
        let hd, param' = MyList.hdtl_nth n param in
        let fx () = map_tlist extra param' (a-n) tl' nx in
        match e with
        | ThisT.U -> map_U hd fx
        | ThisT.X -> map_X hd fx
        | ThisT.C(if0, th0) -> map_C hd if0 th0 (a-n) fx
      )

    let map_edge (extra:extra) (param:param) (((a, b, tl), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      ThisG.cneg b (map_tlist extra param a tl nx)

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | opb::param' -> (
        match opb with
        | None -> (
          let g0 = map_edge extra param' f0
          and g1 = map_edge extra param' f1 in
          LDD.G1.push extra.ldd_man ((), g0, g1)
        )
        | Some b -> (
          map_edge extra param' (if b then f1 else f0)
        )
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [PEvalR] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) : tt =
    let extra = Model.{ldd_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) : tt = makeman ldd_man

  let solve (tt:tt) (param:bool option list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* Partial Evaluation (Absolute Naming) *)
module PEvalA =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.t;  (* LDD manager *)
    }
    type param = bool option list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_next (extra:extra) (param:param) (a:int) nx : xedge =
      match nx with
      | Tree.GLeaf _  -> cst false (List.length param)
      | Tree.GLink lk -> lk param

    let map_U hd (fx:unit -> xedge) : xedge =
      ThisG.push_U ~n:(List.length hd) (fx())
    let rec map_X ?(neg=false) hd (fx:unit -> xedge) : xedge =
      match hd with
      | [] -> ThisG.cneg neg (fx())
      | (Some v)::hd' ->
        ThisG.push_U (map_X ~neg:(v <> neg) hd' fx)
      |  None   ::hd' ->
        ThisG.push_X (map_X                 hd' fx)
    let rec map_C hd (a:int) if0 th0 (fx:unit -> xedge) : xedge =
      match hd with
      | [] -> fx()
      | (Some v)::hd' -> (
        if v = if0
        then ThisG.cst th0 a
        else ThisG.push_U    (map_C hd' (pred a) if0 th0 fx)
      )
      |  None   ::hd' ->
        ThisG.push_C if0 th0 (map_C hd' (pred a) if0 th0 fx)

    let rec map_tlist (extra:extra) (param:param) (a:int) tl nx : xedge =
      match tl with
      | [] -> map_next extra param a nx
      | (e, n)::tl' -> (
        assert(n > 0);
        let hd, param' = MyList.hdtl_nth n param in
        let fx () = map_tlist extra param' (a-n) tl' nx in
        match e with
        | ThisT.U -> map_U hd fx
        | ThisT.X -> map_X hd fx
        | ThisT.C(if0, th0) -> map_C hd a if0 th0 fx
      )

    let map_edge (extra:extra) (param:param) (((a, b, tl), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      ThisG.cneg b (map_tlist extra param a tl nx)

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | opb::param' -> (
        match opb with
        | None -> (
          let g0 = map_edge extra param' f0
          and g1 = map_edge extra param' f1 in
          LDD.G1.push extra.ldd_man ((), g0, g1)
        )
        | Some b -> (
          ThisG.push_U (map_edge extra param' (if b then f1 else f0))
        )
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) : tt =
    let extra = Model.{ldd_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) : tt = makeman ldd_man

  let solve (tt:tt) (param:bool option list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

module OOPS =
struct
  type tt = {
    man_oops : LDD.OOPS.Model.t;
    man_supp : ComputeSupport.tt;
    man_tostree : ToSTree.tt;
    man_pevala : PEvalA.tt;
    man_pevalr : PEvalR.tt;
    man_foralla : ForallA.tt;
    man_forallr : ForallR.tt;
    profile : OProfile.t;
  }

  let get_man_ldd tt =
    LDD.get_cons tt.man_oops

  type ff = LDD.f

  let makeman ?man_oops () =
    let man_oops = match man_oops with
      | Some man_oops -> man_oops
      | None         -> LDD.newman()
    in
    let man_ldd = LDD.get_cons man_oops in
    let man_and = LDD.get_mand man_oops in
    let man_supp = ComputeSupport.newman man_ldd in
    let man_tostree = ToSTree.newman man_ldd in
    let man_pevala = PEvalA.newman man_ldd in
    let man_pevalr = PEvalR.newman man_ldd in
    let man_foralla = ForallA.newman man_ldd man_and in
    let man_forallr = ForallR.newman man_ldd man_and in
    let profile = OProfile.newman ~prefix:"[OOPS]" () in
    {
      man_oops;
      man_supp; man_tostree; man_pevala; man_pevalr; man_foralla; man_forallr;
      profile
    }

  module Model =
  struct
    open LDD.OOPS.Model
    type t = tt
    type f = ff

    let newman () = makeman ()

    let print_profile (t:t) : unit =
      OProfile.stop_other t.profile;
      (* LDD.And.print_profile t.man_and; *)
      OProfile.print t.profile;
      ()

    (* Section 0. OOPS forward definition *)
      (* sub-section 1. Functional / Propositional Aspects *)
    let ( ->> ) t bl f = ( ->> ) t.man_oops bl f
    let arity  t f = arity t.man_oops f
    let   cneg t b f = cneg t.man_oops b f
    let    neg t f = cneg t true f
    let ( &! ) t f1 f2 = ( &! ) t.man_oops f1 f2
    let ( |! ) t f1 f2 = ( |! ) t.man_oops f1 f2
    let ( ^! ) t f1 f2 = ( ^! ) t.man_oops f1 f2
    let ( =! ) t f1 f2 = ( =! ) t.man_oops f1 f2
    let ( *! ) t f0 f1 = ( *! ) t.man_oops f0 f1

    let cst t b n = cst t.man_oops b n
    let var t b n k = var t.man_oops b n k
    let to_bool t f = to_bool t.man_oops f

    let cofactor t b f = cofactor t.man_oops b f
    let id t f = id t.man_oops f
    let eq t f0 f1 = eq t.man_oops f0 f1

      (* sub-section 2. Manager Management *)
    let copy_into t fl t' = copy_into t.man_oops fl t'.man_oops

    let to_barray ?(nocopy=false) ?(destruct=false) t fl =
      to_barray ~nocopy ~destruct t.man_oops fl

    let of_barray ?(t=None) ba =
      let t, fl = of_barray ~t:(Tools.opmap (fun t -> t.man_oops) t) ba in
      (makeman ~man_oops:t (), fl)

    let bw_fl ?(nocopy=false) ?(destruct=false) t =
      bw_fl ~nocopy ~destruct t.man_oops

    let br_fl ?(t=None) cha =
      let t, fl = br_fl ~t:(Tools.opmap (fun t -> t.man_oops) t) cha in
      (makeman ~man_oops:t (), fl)

    (* [FIXME] *)
    let t_stats t = t_stats t.man_oops

    let f_stats t fl = f_stats t.man_oops fl

    (* [FIXME] *)
    let clear_caches t =
      clear_caches t.man_oops

    let keep_clean ?(normalize=false) t fl =
      clear_caches t;
      keep_clean ~normalize t.man_oops fl

    let check t f = check t.man_oops f

    (* Section 3. File-based Management *)

    module F =
    struct
      type f' = F.f'

      let arity = F.arity
      let size  = F.size

      let of_f ?(nocopy=false) ?(normalize=true) t f =
        F.of_f ~nocopy ~normalize t.man_oops f

      let to_f t f' =
        F.to_f t.man_oops f'

      let free f' = F.free f'

      let tof = F.tof
      let off = F.off
    end

    let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) t =
      tof ~nocopy ~normalize ~destruct t.man_oops
    let off cha =
      let t, fl = off cha in
      (makeman ~man_oops:t (), fl)

    (* Section 2. QEOPS level interface *)

    let to_string (t:t) (f:f) : string =
      ToSTree.solve_to_string t.man_tostree f

    let support (t:t) (f:f) : Support.t =
      let l = ComputeSupport.solve t.man_supp f in
      assert(BRLE.length l = arity t f);
      Support.of_BRLE l (* Support.of_bool_list l *)

    let pevalA (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = PEvalA.solve t.man_pevala param f in
      assert(arity t g = arity t f);
      g

    let pevalR (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = PEvalR.solve t.man_pevalr param f in
      assert(arity t g = MyList.count_None param);
      g

    let forallA (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_BRLE param in
      assert(arity t f = BRLE.length param);
      (* let g0 = ForallA0.solve t.man_foralla0 param f in *)
      let g = ForallA.solve t.man_foralla param f in
      (*
      if not (LDD.G1.(=/) g0 g)
      then (
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : begin";
        print_endline "[Ldd_G_o_u.OOPS.forallA] g0";
        print_string (to_string t g0);
        print_endline "[Ldd_G_o_u.OOPS.forallA] g";
        print_string (to_string t g);
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : end";
        assert false
      );
      *)
      assert(arity t g = arity t f);
      g

    let forallR (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_bool_list param in
      assert(arity t f = List.length param);
      let g = ForallR.solve t.man_forallr param f in
      assert(arity t g = MyList.count_false param);
      g

    let existsA t param f : f =
      neg t (forallA t param (neg t f))

    let existsR t param f : f =
      neg t (forallR t param (neg t f))

    (* Section 3. AQEOPS level interface *)

    let ( &!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((&!) t) f0 fl)

    let ( |!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((|!) t) f0 fl)

    let ( ^!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((^!) t) f0 fl)

    let ( =!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((=!) t) f0 fl)

    (* return [true] iff [prim] is implemented within the computation model *)
    let tag_of_atag = function
      | AQEOPS.U -> ThisT.U
      | AQEOPS.X -> ThisT.X
      | AQEOPS.C(b, t) -> ThisT.C(b, t)

    let builtin_o_prim _ = true
    (* counts the number of ordered [prim] primitives available *)
    let detect_o_prim _ p f : int = ThisG.detect_oP (tag_of_atag p) f
    let intro_o_prim _ p n f = ThisG.push_tag ~n (tag_of_atag p) f
    let elim_o_prim _ p n f = ThisG.elim_oP (tag_of_atag p) n f
  end

  let newman = Model.newman
  let print_profile = Model.print_profile

  include OOPS.Make(Model)

  module SUInt = SUInt.Make(Model)
  module VUInt = VUInt.Make(Model)
  module LoadCnf0 = LoadCnf.Make0(Model)
  module LoadCnfA = LoadCnf.MakeA(Model)
  module LoadCnfQ = LoadCnf.MakeQ(Model)
  module CoSTreD = AQEOPS_CoSTreD.Make(Model)
  module CoSTreD_CNF = AQEOPS_CoSTreD.MakeCNF(Model)
  module CoSTreD_CNF_D4 = AQEOPS_CoSTreD.MakeCNF_D4(Model)
  module CCoSTreD = AQEOPS_CCoSTreD.Make(Model)
  module CCoSTreD_CNF = AQEOPS_CCoSTreD.MakeCNF(Model)
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Model)
end

module CSTypes = Constraint_system_types
module CSUtils = Constraint_system_utils
module CSCoSTreD  = Constraint_system_costred

module SNAX = SnaxOops.OOPS.Model

module CoSTreD_Model : CSCoSTreD.MSig =
struct
  module Q = OOPS.Model

  module BddToSnax = ComposeOops(SNAX)
  let translate_pure_to_snax (pure:Q.t) (snax:SNAX.t) (fl:Q.f list) : SNAX.f list =
      let pure = OOPS.get_man_ldd pure in
      BddToSnax.translate pure snax fl

  let default_module_name = default_module_name
end

module CoSTreD_Module = CSCoSTreD.Make(CoSTreD_Model)
