(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * module  : TacxTypes
 *)

open GuaCaml
open STools
open BTools

type tag =
  | And
  | Cons
  | Xor

let tob_tag tag stream = match tag with
  | And  -> false::stream
  | Cons -> true ::false::stream
  | Xor  -> true ::true ::stream

let ofb_tag = function
  | false::stream         -> And, stream
  | true ::false::stream -> Cons, stream
  | true ::true ::stream -> Xor , stream
  | _ -> assert false

let bw_tag cha tag = match tag with
  | And  -> ToBStream.bool cha false
  | Cons -> (ToBStream.bool cha true; ToBStream.bool cha false)
  | Xor  -> (ToBStream.bool cha true; ToBStream.bool cha true )

let br_tag (cha:OfBStream.Channel.t) : tag =
  match OfBStream.bool cha with
  | false -> And
  | true  ->
    match OfBStream.bool cha with
    | false -> Cons
    | true  -> Xor

let o3s_tag = (tob_tag, ofb_tag)

let string_of_tag = function
  | And  -> "A"
  | Cons -> "C"
  | Xor  -> "X"

let string_to_tag = function
  | "A" -> And
  | "C" -> Cons
  | "X" -> Xor
  | _ -> assert false

type ttag =
  | TAnd  of bool * bool
  | TCons of bool
  | TXor

let string_of_ttag =
  let bool b = ToS.bool' ~t:"0" ~f:"1" b in
  function
  | TAnd(b0, b1) -> ("A"^(bool b0)^(bool b1))
  | TCons b0     -> ("C"^(bool b0)          )
  | TXor         -> ("X"                    )

let bw_ttag cha =
ToBStream.(function
  | TAnd(b0, b1) -> (
    bool cha false;
    bool cha b0;
    bool cha b1;
  )
  | TCons b0     -> (
    bool cha true;
    bool cha false;
    bool cha b0;
  )
  | TXor         -> (
    bool cha true;
    bool cha true
  )
)

let br_ttag cha =
OfBStream.(match bool cha with
  | false -> (
    let b0 = bool cha in
    let b1 = bool cha in
      TAnd(b0, b1)
  )
  | true -> (
    match bool cha with
    | false -> (
      let b0 = bool cha in
      TCons b0
    )
    | true  ->
      TXor
  )
)

let tob_ttag ttag stream = match ttag with
  | TAnd (b0, b1) -> false::b0   ::b1   ::stream
  | TCons b    -> true ::false::b    ::stream
  | TXor      -> true ::true ::stream

let ofb_ttag = function
  | false::b0   ::b1   ::stream -> (TAnd (b0, b1)), stream
  | true ::false::b    ::stream -> (TCons b      ), stream
  | true ::true ::stream        -> (TXor         ), stream
  | _ -> assert false

let stree_of_ttag ttag stream = match ttag with
  | TAnd (b0, b1) -> "A"::(Utils.pm_of_bool b0)::(Utils.pm_of_bool b1)::stream
  | TCons b    -> "C"::(Utils.pm_of_bool b)::stream
  | TXor      -> "X"::stream

let stree_to_ttag = function
  | "A"::b0::b1::stream -> (TAnd (Utils.bool_of_pm b0, Utils.bool_of_pm b1), stream)
  | "C"::b::stream -> (TCons (Utils.bool_of_pm b), stream)
  | "X"::stream -> (TXor, stream)
  | _ -> assert false

let default_eval_node eval_edge peval (tag, edge0, edge1) = match tag with
  | Cons -> (match peval with
    | [] -> assert false
    | head::peval -> match head with
      | None       ->
        Utils.MNode(Cons, eval_edge peval edge0, eval_edge peval edge1)
      | Some false ->
        Utils.MEdge(eval_edge peval edge0)
      | Some true  ->
        Utils.MEdge(eval_edge peval edge1)
  )
(* tag = And | Xor *)
  | tag  -> Utils.MNode(tag, eval_edge peval edge0, eval_edge peval edge1)
