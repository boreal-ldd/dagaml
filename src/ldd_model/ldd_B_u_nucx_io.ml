(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_io : IO
 *)

open GuaCaml
open STools
open Extra
open BTools
open O3Extra

open Ldd_B_u_nucx_types

let arity_edge' (blk, _) : int = blk.noutput
let arity_node' (_, e0, e1) : int =
  let a = arity_edge' e0 in
  assert(a = arity_edge' e1);
  succ a

module ToS =
struct
  open ToS
  open TreeUtils.ToS
  open AB.ToS

  let bool b = STools.ToS.bool b

  let elem = function
    | S -> "S"
    | P b -> "P"^(bool b)
    | C(b, t) -> ("C"^((bool * bool) (b, t)))

  let sigma_su  = ab unit unit
  let sigma_sx  = ab unit unit
  let sigma_sux = ab unit (ab unit unit)
  let sigma_sc  = ab unit bool
  let sigma_suc = ab unit (ab unit bool)

  let rowU  : rowU  t = abl unit unit
  let rowX  : rowX  t = abl unit unit
  let rowUX : rowUX t = abl unit (ab unit unit)
  let rowC  : rowC  t = abl unit bool
  let rowUC : rowUC t = abl unit (ab unit bool)

  let rowCX = ab rowX (bool * rowC)

  let matCX = list rowCX
  let matUCX = rowU * matCX
  let bmatUCX = bool * matUCX

  let sucx_tag tag =
    "{hasS = "^(bool tag.hasS)^"; hasU = "^(bool tag.hasU)^"; hasX = "^(bool tag.hasX)^"; hasC = "^(bool tag.hasC)^"}"

  let decomp = function
    | C0 -> "C0"
    | Id x' -> "Id "^(int x')
    | UCX ucx -> "UCX "^(matUCX ucx)

  let bdecomp = bool * decomp

  let block block' =
    "{neg = "^(bool block'.neg)^"; noutput = "^(int block'.noutput)^"; ninput = "^(option int block'.ninput)^"; decomp = "^(decomp block'.decomp)^"}"

  let leaf = unit
  let edge = block
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)

  let merge ident x' = Utils.merge (next' ident) (node' ident) x'
  let emerge ident x' = Utils.emerge block (merge ident) x'

  let merge3 ident x' = Utils.merge3 (next' ident) (node' ident) x'
  let emerge3 ident x' = Utils.emerge3 block (merge3 ident) x'

  let peval = ToS.(list(option bool))
  let opeval = ToS.(option peval)

  let plink ident plink = pair Utils.stree_of_opeval ident plink
  let pnext ident next' = Utils.gnode unit (plink ident) next'
  let pedge ident edge' = pair block (pnext ident) edge'
  let pnode ident node' = trio unit (pedge ident) (pedge ident) node'

  let pmerge ident x' = Utils.merge (pnext ident) (pnode ident) x'
  let pemerge ident x' = Utils.emerge block (pmerge ident) x'

  let pmerge3 ident x' = Utils.merge3 (pnext ident) (pnode ident) x'
  let pemerge3 ident x' = Utils.emerge3 block (pmerge3 ident) x'
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB
  open AB.ToB

  let wrap text tos aa a s =
    print_endline (text^" [0] a:"^(tos a));
    print_string (text^" [0] s:"); SUtils.print_stream s; print_newline();
    let s = aa a s in
    print_string (text^" [1] s:"); SUtils.print_stream s; print_newline();
    s

  let elem e s = match e with
    | S -> false::false::s
    | P b -> false::true::b::s
    | C(b, t) -> true::b::t::s

  let sigma_su  : (unit, unit) AB.ab t = ab unit unit
  let sigma_sx  = ab unit unit
  let sigma_sux = ab unit (ab unit unit)
  let sigma_sc  = ab unit bool
  let sigma_suc = ab unit (ab unit bool)

  let sized_abl_unit_unit = sized_abl_unitAB
  let sized_abl_unit_bool = sized_abl_unitA sized_bool_list

  let rowU  : rowU  t = abl sized_unit_list sized_unit_list
  let rowX  : rowX  t = abl sized_unit_list sized_unit_list
  let rowUX : rowUX t = abl sized_unit_list sized_abl_unit_unit
  let rowC  : rowC  t = abl sized_unit_list sized_bool_list
  let rowUC : rowUC t = abl sized_unit_list sized_abl_unit_bool

  let sized_rowU  : rowU  t = sized_abl_unitAB
  let sized_rowX  : rowX  t = sized_abl_unitAB
  let sized_rowUX : rowUX t = sized_abl_unitA sized_abl_unit_unit
  let sized_rowC  : rowC  t = sized_abl_unitA sized_bool_list
  let sized_rowUC : rowUC t = sized_abl_unitA sized_abl_unit_bool

(*
  let sized_rowU  = wrap "[ToB.rowU ]" ToS.rowU  sized_rowU
  let sized_rowX  = wrap "[ToB.rowX ]" ToS.rowX  sized_rowX
  let sized_rowUX = wrap "[ToB.rowUX]" ToS.rowUX sized_rowUX
  let sized_rowC  = wrap "[ToB.rowC ]" ToS.rowC  sized_rowC
  let sized_rowUC = wrap "[ToB.rowUC]" ToS.rowUC sized_rowUC
 *)

  let rowCX : rowCX t =
    ab rowX (bool * rowC)

  let sized_rowCX : rowCX t =
    ab sized_rowX (bool * sized_rowC)

  let matCX = list rowCX

  let sized_matCX (mat:matCX) (s:stream) : stream =
    let rec loop (mat:matCX) (s:stream) : stream =
      match mat with
      | [] -> s
      | rowCX::matCX -> sized_rowCX rowCX (loop matCX s)
    in
    int (List.length mat) (loop mat s)

  let matUCX = rowU * matCX

  let sized_matUCX ((rU, mat):matUCX) (s:stream) : stream =
    sized_rowU rU (sized_matCX mat s)

  let bmatUCX = bool * matUCX

  let sized_bmatUCX = bool * sized_matUCX

  let sucx_tag tag stream =
    quad bool bool bool bool (tag.hasS, tag.hasU, tag.hasX, tag.hasC) stream

  let decomp choice stream = match choice with
    | C0 -> false::false::stream
    | Id x' -> false::true::(int x' stream)
    | UCX sucx -> true::(matUCX sucx stream)

  let sized_decomp choice stream = match choice with
    | C0 -> false::false::stream
    | Id x' -> false::true::(int x' stream)
    | UCX sucx -> true::(sized_matUCX sucx stream)

  let block blk stream =
    quad bool int (option int) decomp (blk.neg, blk.noutput, blk.ninput, blk.decomp) stream

  let sized_block blk stream =
    trio bool bool sized_decomp (blk.neg, blk.ninput = None, blk.decomp) stream

(* (* [DEBUG] *)
  let sized_block x s = wrap "[ToB.sized_block]" ToS.block sized_block x s
 *)

  let leaf = unit
  let edge e s : stream =
    int e.noutput (sized_block e s)
  let node = unit

(* [DEBUG]
  let leaf x s = wrap "[ToB.leaf]" ToS.leaf leaf x s
  let edge x s = wrap "[ToB.edge]" ToS.edge edge x s
  let node x s = wrap "[ToB.node]" ToS.node node x s
 *)

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB
  open AB.OfB

  let wrap text tos aa s =
    print_string (text^" [1] s:"); SUtils.print_stream s; print_newline();
    let a, s = aa s in
    print_string (text^" [0] s:"); SUtils.print_stream s; print_newline();
    print_endline (text^" [0] a:"^(tos a));
    (a, s)

  let swrap text tos aa n s =
    print_string (text^" [1] s:"); SUtils.print_stream s; print_newline();
    let (a, n), s = aa n s in
    print_string (text^" [0] s:"); SUtils.print_stream s; print_newline();
    print_endline (text^" [0] a:"^(tos a));
    ((a, n), s)

  let elem = function
    | false::false::s -> (S, s)
    | false::true ::b::s -> (P b, s)
    | true ::b::t::s -> (C(b, t), s)
    | _ -> assert false

  let sigma_su  = ab unit unit
  let sigma_sx  = ab unit unit
  let sigma_sux = ab unit (ab unit unit)
  let sigma_sc  = ab unit bool
  let sigma_suc = ab unit (ab unit bool)

  let sized_abl_unit_unit n s =
    sized_abl_unitAB n s
  let sized_abl_unit_bool n s =
    sized_abl_unitA n sized_bool_list s

  let rowU  : rowU  t = abl sized_unit_list sized_unit_list
  let rowX  : rowX  t = abl sized_unit_list sized_unit_list
  let rowUX : rowUX t = abl sized_unit_list sized_abl_unit_unit
  let rowC  : rowC  t = abl sized_unit_list sized_bool_list
  let rowUC : rowUC t = abl sized_unit_list sized_abl_unit_bool

  let sized_rowU  n s = sized_abl_unitAB_countA n s
  let sized_rowX  n s = sized_abl_unitAB_countA n s
  let sized_rowUX n s = sized_abl_unitA_countA n sized_abl_unit_unit s
  let sized_rowC  n s = sized_abl_unitA_countA n sized_bool_list s
  let sized_rowUC n s = sized_abl_unitA_countA n sized_abl_unit_bool s

(* [DEBUG]
  let sized_rowU  = swrap "[OfB.rowU ]" ToS.rowU  sized_rowU
  let sized_rowX  = swrap "[OfB.rowX ]" ToS.rowX  sized_rowX
  let sized_rowUX = swrap "[OfB.rowUX]" ToS.rowUX sized_rowUX
  let sized_rowC  = swrap "[OfB.rowC ]" ToS.rowC  sized_rowC
  let sized_rowUC = swrap "[OfB.rowUC]" ToS.rowUC sized_rowUC
 *)

  let rowCX = ab rowX (bool * rowC)

  let sized_rowCX n s =
    let rCX_b, s = ab (sized_rowX n) (bool * (sized_rowC n)) s in
    match rCX_b with
    | AB.A (rX, nB) -> ((AB.A rX, nB), s)
    | AB.B (th0, (rC, nB)) -> ((AB.B (th0, rC), nB), s)

  let matCX = list rowCX

  let sized_matCX n s =
    let rec loop carry n k s =
      if k = 0
      then ((List.rev carry, n), s)
      else (
        let (rCX, n'), s' = sized_rowCX n s in
        loop (rCX::carry) n' (pred k) s'
      )
    in
    let k, s = int s in
    loop [] n k s

  let matUCX = rowU * matCX

  let sized_matUCX n s =
    let (rU, n), s = sized_rowU n s in
    let (matCX, n), s = sized_matCX n s in
    (((rU, matCX), n), s)

  let bmatUCX = bool * matUCX

  let sucx_tag stream =
    let (hasS, hasU, hasX, hasC), stream =
      quad bool bool bool bool stream in
    {hasS; hasU; hasX; hasC}, stream

  let decomp = function
    | false::false::stream -> (C0, stream)
    | false::true ::stream ->
      let x, stream = int stream in
      (Id x, stream)
    | true::stream ->
      let sucx, stream = matUCX stream in
      (UCX sucx, stream)
    | _ -> assert false

  let sized_decomp n = function
    | false::false::stream ->
      ((C0, None), stream)
    | false::true ::stream ->
      let x, stream = int stream in
      ((Id x, None), stream)
    | true::stream ->
      let (sucx, n'), stream = sized_matUCX n stream in
      ((UCX sucx, Some n'), stream)
    | _ -> assert false

  let block stream =
    let (neg, noutput, ninput, decomp), stream =
      quad bool int (option int) decomp stream in
    {neg; noutput; ninput; decomp}, stream

  let sized_block noutput s =
    let (neg, is_none, (decomp, ninput)), s =
      trio bool bool (sized_decomp noutput) s in
    let ninput =
      if is_none
      then (assert(ninput = Some 0 || ninput = None); None)
      else ninput
    in
    ({neg; noutput; ninput; decomp}, s)

(* [DEBUG]
  let sized_block n s = wrap "[OfB.sized_block]" ToS.block (sized_block n) s
 *)

  let leaf = unit
  let edge s =
    let n, s = int s in
    sized_block n s
  let node = unit

(* [DEBUG]
  let leaf x = wrap "[OfB.leaf]" ToS.leaf leaf x
  let edge x = wrap "[OfB.edge]" ToS.edge edge x
  let node x = wrap "[OfB.node]" ToS.node node x
 *)

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open BTools.ToBStream
  open TreeUtils.ToBStream
  open AB.ToBStream

  let elem cha e =
    match e with
    | S -> bool cha false; bool cha false
    | P b -> bool cha false; bool cha true; bool cha b
    | C(b, t) -> bool cha true; (bool * bool) cha (b, t)

  let sigma_su  : (unit, unit) AB.ab t = ab unit unit
  let sigma_sx  = ab unit unit
  let sigma_sux = ab unit (ab unit unit)
  let sigma_sc  = ab unit bool
  let sigma_suc = ab unit (ab unit bool)

  let sized_abl_unit_unit = sized_abl sized_unit_list sized_unit_list
  let sized_abl_unit_bool = sized_abl sized_unit_list sized_bool_list

  let rowU  : rowU  t = abl sized_unit_list sized_unit_list
  let rowX  : rowX  t = abl sized_unit_list sized_unit_list
  let rowUX : rowUX t = abl sized_unit_list sized_abl_unit_unit
  let rowC  : rowC  t = abl sized_unit_list sized_bool_list
  let rowUC : rowUC t = abl sized_unit_list sized_abl_unit_bool

  let rowCX : rowCX t =
    ab rowX (bool * rowC)

  let matCX = list rowCX
  let matUCX = rowU * matCX
  let bmatUCX = bool * matUCX

  let sucx_tag cha tag =
    quad bool bool bool bool cha (tag.hasS, tag.hasU, tag.hasX, tag.hasC)

  let decomp cha choice =
    match choice with
    | C0       -> bool cha false; bool cha false
    | Id x'    -> bool cha false; bool cha true; int cha x'
    | UCX sucx -> bool cha true ; matUCX cha sucx

  let block cha blk =
    quad bool int (option int) decomp cha (blk.neg, blk.noutput, blk.ninput, blk.decomp)

  let leaf = unit
  let edge = block
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream
  open AB.OfBStream

  let elem cha =
    match bool cha with
    | false -> (match bool cha with
      | false -> S
      | true  -> P(bool cha))
    | true  -> (C((bool * bool) cha))

  let sigma_su  : (unit, unit) AB.ab t = ab unit unit
  let sigma_sx  = ab unit unit
  let sigma_sux = ab unit (ab unit unit)
  let sigma_sc  = ab unit bool
  let sigma_suc = ab unit (ab unit bool)

  let sized_abl_unit_unit (n:int) cha : (unit, unit) AB.ab list =
    sized_abl n sized_unit_list sized_unit_list cha
  let sized_abl_unit_bool (n:int) cha : (unit, bool) AB.ab list =
    sized_abl n sized_unit_list sized_bool_list cha

  let rowU  : rowU  t = abl sized_unit_list sized_unit_list
  let rowX  : rowX  t = abl sized_unit_list sized_unit_list
  let rowUX : rowUX t = abl sized_unit_list sized_abl_unit_unit
  let rowC  : rowC  t = abl sized_unit_list sized_bool_list
  let rowUC : rowUC t = abl sized_unit_list sized_abl_unit_bool

  let rowCX : rowCX t =
    ab rowX (bool * rowC)

  let matCX = list rowCX
  let matUCX = rowU * matCX
  let bmatUCX = bool * matUCX

  let sucx_tag cha =
    let (hasS, hasU, hasX, hasC) =
      quad bool bool bool bool cha in
    {hasS; hasU; hasX; hasC}

  let decomp cha =
    match bool cha with
    | false -> (
      match bool cha with
      | false -> C0
      | true  -> Id(int cha)
    )
    | true  -> UCX(matUCX cha)

  let block cha =
    let neg, noutput, ninput, decomp =
      quad bool int (option int) decomp cha in
    {neg; noutput; ninput; decomp}

  let leaf = unit
  let edge = block
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

(* [TODO] make edge' polarized *)
module ToBa =
struct
  open BTools.ToB

  let next' opa (ident:'i tob) nx (s:stream) : stream =
    match opa, nx with
    | None  , Tree.GLeaf () -> s
    | Some a, Tree.GLink lk -> (assert(a > 0); ident lk s)
    | _, _ -> assert false

  let edge' a (ident:'i tob) (blk, nx) (s:stream) : stream =
    let s = next' blk.ninput ident nx s in
    let s = ToB.sized_block blk s in
    s

  let node' (ident:'i tob) (node:_ node') (s:stream) : stream =
    let a = arity_node' node in
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) node s
end

(* [TODO] make edge' polarized *)
module OfBa =
struct
  open BTools.OfB

  let next' opa (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    match opa with
    | None -> (Tree.GLeaf(), s)
    | Some a -> (let lk, s = ident a s in (Tree.GLink lk, s))

  let edge' a (ident:int -> 'i ofb) (s:stream) : _ edge' * stream =
    let blk, s = OfB.sized_block a s in
    let nx, s = next' blk.ninput ident s in
    ((blk, nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

module AEval =
struct
  let abl (p:'a list) (abl:('b, 'c) AB.ab list) : ('a * 'b) list * ('a * 'c) list =
    let rec aux cA cB p abl =
      match p, abl with
      | [], [] -> (List.rev cA, List.rev cB)
      | [], _ | _, [] -> assert false
      | p0::p, ab::abl -> (
        match ab with
        | AB.A a -> aux ((p0, a)::cA) cB p abl
        | AB.B b -> aux cA ((p0, b)::cB) p abl
      )
    in aux [] [] p abl

  let rowX (p:'a list) (rX:rowX) : 'a list * 'a list =
    let rA, rB = abl p rX in
    let rA = Tools.map (fun (i, ()) -> i) rA in
    let rB = Tools.map (fun (i, ()) -> i) rB in
    (rB, rA)

  let rowU = rowX

  let rowC (p:'a list) (rC:rowC)  : (bool * 'a) list * 'a list =
    let rA, rB = abl p rC in
    let rA = Tools.map (fun (i, ()) -> i) rA in
    let rB = Tools.map (fun (i, if0) -> (if0, i)) rB in
    (rB, rA)

  let rowCX (p:'a list) (rCX:rowCX) : _ * 'a list =
    match rCX with
    | AB.A rX -> (
      let cX, p = rowX p rX in
      (AB.A cX, p)
    )
    | AB.B(th0, rC) -> (
      let cC, p = rowC p rC in
      (AB.B(th0, cC), p)
    )

  let matCX (p:'a list) (mCX:matCX) : _ * 'a list =
    let rec loop carry p = function
      | [] -> (List.rev carry, p)
      | rCX::mCX -> (
        let cCX, p = rowCX p rCX in
        loop (cCX::carry) p mCX
      )
    in loop [] p mCX

  let matUCX (p:'a list) ((rU, mCX):matUCX) : _ * 'a list =
    let cU, p = rowU p rU in
    let cCX, p = matCX p mCX in
    ((cU, cCX), p)
end

module PrettyToS =
struct
  let elem_C if0 th0 =
    match if0, th0 with
    | false, false -> '1'
    | true , false -> '0'
    | false, true  -> 'I'
    | true , true  -> 'O'

  let elem = function
    | S -> 'S'
    | P b -> (if b then 'X' else 'U')
    | C(if0, th0) -> elem_C if0 th0

  let eval_rowP (a:(elem * int) array) (lvl:int) (b:bool) (rP:int list) : unit =
    List.iter (fun i -> a.(i) <- (P b, lvl)) rP

  let eval_rowX a lvl rX = eval_rowP a lvl true  rX
  let eval_rowU a lvl rU = eval_rowP a lvl false rU

  let eval_rowC (a:(elem * int) array) (lvl:int) (th0:bool) (rC:(bool * int)list) : unit =
    List.iter (fun (if0, i) -> a.(i) <- (C(if0, th0), lvl)) rC

  let eval_rowCX (a:(elem * int) array) (lvl:int) rCX =
    match rCX with
    | AB.A rX -> eval_rowX a lvl rX
    | AB.B(th0, rC) -> eval_rowC a lvl th0 rC

  let rec eval_matCX (a:(elem * int) array) (lvl:int) matCX : unit =
    match matCX with
    | [] -> ()
    | h::t -> eval_rowCX a lvl h; eval_matCX a (succ lvl) t

  let eval_matUCX (a:(elem * int) array) (lvl:int) (rU, matCX) : unit =
    eval_rowU a lvl rU;
    eval_matCX a lvl matCX

  let pretty_matUCX neg arity matUCX =
    let matUCX', _ =
      AEval.matUCX (MyList.init arity (fun i -> i)) matUCX
    in
    let earray = Array.make arity (S, 0) in
    eval_matUCX earray 0 matUCX';
    let map_elem (e, i) = (elem e, i) in
    let sarray = Array.map map_elem earray in
    let len = max 1 (List.length (snd matUCX)) in
    let slist = STools.SUtils.char_height_array ~above:' ' ~under:'.' len sarray in
    let slist = match slist with
      | [] -> assert false
      | h::t -> ((Utils.mp_of_bool neg)^h)::(t ||> (fun s -> " "^s))
    in
    String.concat "\n" slist

  let pretty_decomp neg arity = function
    | C0 -> ((if neg then "T" else "F")^"_"^(string_of_int arity))
    | Id k -> ((Utils.mp_of_bool neg)^" (Id "^(string_of_int k)^")_"^(string_of_int arity))
    | UCX matUCX -> pretty_matUCX neg arity matUCX

  let pretty_block block =
    pretty_decomp block.neg block.noutput block.decomp

  let edge = pretty_block
end
