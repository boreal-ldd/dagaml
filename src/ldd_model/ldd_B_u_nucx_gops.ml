(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *   Negation, Useless variables, Xor variables, Canalizing variables
 *
 * module  : Ldd_u_nucx_gops : General OPertorS
 *)

open GuaCaml
open AB
open Utils

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_peval
open Ldd_B_u_nucx_pull
open Ldd_B_u_nucx_cons
open Ldd_B_u_nucx_andb
open Ldd_B_u_nucx_xorb

let arity = arity_edge
let arity_node = arity_node
let neg = neg_edge
let cneg = cneg_edge
let is_cst = edge_is_cst
let cst = cst_edge
let get_cst = get_cst
let var b n k =
  ({neg = b; noutput = n; ninput = None; decomp = Id k}, Tree.GLeaf())
let compose = compose_block_edge
let compose_rowCX = compose_rowCX_edge
let compose_rowU  = compose_rowU_edge

let support_of_edge = support_of_edge

let solve_cons = solve_cons_reduce
let solve_and ((), fx, fy) =
  solve_and fx fy
let solve_xor ((), fx, fy) =
  solve_xor fx fy
let node_pull = node_pull
let peval_pedge = peval_pedge
let peval_pnodeC = peval_pnodeC

let id_block ?(neg=false) noutput = id_block ~neg noutput
let id_edge ?(neg=false) noutput nx = id_edge ~neg noutput nx

let block_of_matCX = block_of_matCX
let edge_of_matCX = edge_of_matCX

let rowU_of_mask mask =
  Tools.map (function
    | true  -> A ()
    | false -> B ()) mask

let compose_mask mask edge =
  compose_rowU_edge (rowU_of_mask mask) edge

let pnode_of_node = function
  | Tree.GLeaf () -> Tree.GLeaf ()
  | Tree.GLink node -> Tree.GLink (None, node)

let pedge_of_edge (block, node) = (block, pnode_of_node node)

let peval_merge3 (solve : 'i pnode -> 'i plink emerge3) (peval : peval) ((edge, merge) : 'i plink emerge3) : 'i plink emerge3 = match merge with
  | M3Edge pnext -> merge3_of_edge (peval_pedge peval (edge, pnext))
  | M3Cons (((), pedge0, pedge1) as pnode) ->
  (
    assert(arity_edge pedge0 = arity_edge pedge1);
    let (edge', pnext') = peval_pedge peval (edge, Tree.GLink (None, ())) in
    match pnext' with
      | Tree.GLeaf () -> (edge', (M3Edge(Tree.GLeaf())))
      | Tree.GLink (opeval, ()) -> match opeval with
        | None -> (edge', M3Cons pnode)
        | Some peval -> match peval_pnodeC peval pnode with
          | MEdge edge -> merge3_of_edge(compose_block_edge edge' edge)
          | MNode node ->
          (
            let edge', merge = compose_block_merge edge' (solve_cons_reduce node) in
            match merge with
            | MEdge next -> merge3_of_edge(edge', next)
            | MNode node -> (edge', M3Cons node)
          )
  )
  | M3Node (((), pedge0, pedge1) as pnode) ->
  (
    assert(arity_edge pedge0 = arity_edge pedge1);
    let edge', pnext' = peval_pedge peval (edge, Tree.GLink (None, ())) in
    match pnext' with
      | Tree.GLeaf () -> (edge', M3Edge(Tree.GLeaf()))
      | Tree.GLink (opeval, ()) ->
        compose_block_merge3 edge' (solve (opeval_pnode opeval pnode))
  )

let opquant_of_quant quant = if List.for_all (fun x -> x = false) quant
  then None
  else (Some quant)

let compose_quant qC qc = Utils.compose false qc qC

let compose_opquant opqC opqc = match opqC, opqc with
  | None, None       -> None
  | Some q, None
  | None, Some q     -> Some q
  | Some qC, Some qc -> Some(compose_quant qC qc)

let compose_qnext quant = function
  | Tree.GLeaf () -> assert(quant = []); Tree.GLeaf()
  | Tree.GLink (opq, node) -> Tree.GLink(compose_opquant(opquant_of_quant quant)opq, node)

(*
let quant_block_spx (quant : bool list) (neg : bool) (arity : int) ((shift, tag, liste) : block_spx) qnext =
  let opmin = List.fold_left2 (fun opmin -> fun quant elem -> match quant, elem with
    | true, X(_, i) when ((mod2 i)<>shift<>neg) = false -> opmin i opmin
    | _ -> opmin) None quant liste in
  match opmin with
  | None ->
  (
    let liste' = MyList.opmap2 (function
      | true  -> (fun _ -> None  )
      | false -> (fun x -> Some x)) quant liste in
    match qnext with
    | Tree.GLeaf() -> spx_liste_to_edge neg shift (liste', Tree.GLeaf())
    | Tree.GLink (opq, node) ->
    (
      let quant' = MyList.opmap2 (function
        | S -> (fun x -> Some x)
        | _ -> (fun _ -> None  )) liste quant in
      let opq' = compose_opquant(opquant_of_quant quant')opq in
      spx_liste_to_edge neg shift (liste', Tree.GLink(opq', node))
    )
  )
  | Some minX ->
  (
    let liste' = MyList.opmap2 (function
      | true -> (fun _            -> None  )
      | false -> (function
        | S|P -> (Some P)
        | X(_, i) as x -> if i >= minX
          then   (Some P)
          else   (Some x))) quant liste in
    spx_liste_to_edge neg shift (liste', Tree.GLeaf())
  )

let quant_block_choice (quant : bool list) (neg : bool) (arity : int) (qnext : (unit, bool list option * _) Tree.gnext) =
  let coarity = MyList.count(fun x -> x = false) quant in
  function
  | C0 -> make_edge_C0 neg coarity
  | Id x -> (if List.nth quant x
    then (make_edge_C0 neg coarity)
    else (
      let x' = MyList.counti (fun i b -> (b = false)&&(i < x)) quant in
      ({neg; arity = coarity; block = Id x'}, Tree.GLeaf())
    )
  )
  | SPX spx -> quant_block_spx quant neg arity spx qnext

let quant_qedge quant (block, qnext) = quant_block_choice quant block.neg block.arity qnext block.block
let quant_qnode  quant ((), edge0, edge1) =
  ((), quant_qedge quant edge0, quant_qedge quant edge1)
let quant_node   quant node = quant_qnode quant (Utils.pnode_of_node node)

let solve_quant quant ((((), edge0, edge1) as qnode) : 'i qnode)  =
  assert(List.length quant = arity_edge edge0 + 1);
  assert(List.length quant = arity_edge edge1 + 1);
  assert(List.length quant > 0);
  if List.for_all (fun x -> x) quant
  then (Utils.M3Edge({neg = false; arity = 0; block = C0}, Tree.GLeaf()))
  (* true in GroBdd due to canonicity, not necessarly true in TACX *)
  else ( match quant with
    | [] -> assert false
    | head::quant' -> if head
    then (Utils.M3Node(quant_qnode quant' qnode))
    else (Utils.M3Cons(quant_qnode quant' qnode))
  )
  (* =( cannot use solve_and in current version as we cannot propagate evaluation
   * NEXT: introduce an EQUANT modele =)
   * SOLUTION: cheating ... make solve_quant use evaluation (needs to performe a EQ inversion)
   *)

*)

(*
let rewrite_expand_AX_node cond ((tag, edgeX, edgeY) as node) =
  let arity = arity_edge edgeX in
  assert(arity = arity_edge edgeY);
  let arity' = arity + TacxTypes.(match tag with Cons -> 1 | _ -> 0) in
  let blockC = make_block_S false arity' in
  if cond && (arity > 0) && (tag <> TacxTypes.Cons)
  then (
    let tail = MyList.ntimes None (arity-1) in
    let branch bool edgeX edgeY =
      let peval  = (Some bool)::tail in
      let edgeX' = peval_pedge peval edgeX
      and edgeY' = peval_pedge peval edgeY in
      let edge, merge = solve_tacx (tag, edgeX', edgeY') in
      (edge, (match merge with
      | Utils.MEdge next -> GTree.Leaf next
      | Utils.MNode node -> GTree.(Node(get_node_leaf node))))
    in
    let edge0 = branch false edgeX edgeY
    and edge1 = branch true  edgeX edgeY in
    (blockC, GTree.Node(TacxTypes.Cons, edge0, edge1))
  ) else (blockC, GTree.(Node(get_node_leaf node)))
*)
