(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-uc0 : Ordered model with Useless variables and Canalizing variables (C10 and C00 only)
 *
 * module  : Ldd_o_uc0_types : Types
 *)

open GuaCaml
open STools
open BTools
open Extra

type tag =
  | U
  | C0 of bool

type tlist = (tag * int) list

type uniq = int * tlist
type pair = int * tlist * tlist

type edge_state = uniq
type node_state = pair

type leaf = bool * int
type edge = edge_state
type node = unit

type 'i next' = ('i, leaf) Tree.gnext
type 'i edge' = edge * 'i next'
type 'i node' = node * 'i edge' * 'i edge'
