(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_types : Types
 *)

open GuaCaml
open AB

type elem =
  | S                  (* significant variables *)
  | P of  bool         (* polarity-phase invariant *)
  | C of (bool * bool) (* canalizing variables *)

type sigma_su  = (unit, unit) ab
(* A () => Significant
   B () => Useless *)

type sigma_sx  = (unit, unit) ab
(* A () => Significant
   B () => Xor *)

type sigma_sux = (unit, (unit, unit) ab) ab
(* A () => Significant
   B A () => Useless
   B B () => Xor *)

type sigma_sc  = (unit, bool) ab
(* A () => Significant
   B if0 => (if0, _) Canalizing
 *)
type sigma_suc = (unit, (unit, bool) ab) ab
(* A () => Significant
   B A () => Useless
   B B if0 => (if0, _)-Canalizing
 *)

type ('a, 'b) row = ('a, 'b) ab list

type rowU  = sigma_su  list
type rowX  = sigma_sx  list
type rowUX = sigma_sux list
type rowC  = sigma_sc  list
type rowUC = sigma_suc list

type rowCX = (rowX, bool * rowC) ab
(* A rX => row of Xor variables
   B(th0, rC) => row of (_, th0)-Canalizing variables
 *)

type matCX = rowCX list
type matUCX = rowU * matCX
type bmatUCX = bool * matUCX

type sucx_tag = {hasS : bool; hasU : bool; hasX : bool; hasC : bool}

type decomp =
  | C0
  | Id  of int
  | UCX of matUCX
(* b = is_leaf *)

type block = {
  neg     : bool;
  noutput : int;
  ninput  : int option;
  decomp  : decomp;
}

type leaf = unit
type edge = block
type node = unit

type 'i next' = ('i, leaf) Tree.gnext
type 'i edge' = edge * 'i next'
type 'i node' = node * 'i edge' * 'i edge'

type 'a plink  = Utils.opeval * 'a
type 'a pedge  = 'a plink edge'
type 'a pnode  = 'a plink node'

type 'a merge   = ('a next', 'a node') Utils.merge
type 'a emerge  = block * 'a merge
type 'a merge3  = ('a next', 'a node', 'a node') Utils.merge3
type 'a emerge3 = block * 'a merge3

type    quant  = Utils.quant
type  opquant  = quant option
type 'a qlink  = opquant * 'a
type 'a qedge  = 'a qlink edge'
type 'a qnode  = 'a qlink node'
