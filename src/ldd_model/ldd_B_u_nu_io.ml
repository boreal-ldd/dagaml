(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nu : Uniform model with Negation, Useless variables
 *
 * module  : Ldd_u_nu_io : IO
 *)

open GuaCaml
open STools
open BTools

open Ldd_B_u_nu_types

let arity_edge' (((_, l), _) : _ edge') : int = List.length l
let arity_node' (_, e0, e1) =
  let a = arity_edge' e0 in
  assert(a = arity_edge' e1);
  succ a

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let uniq_elem = function
    | U -> "U"
    | S -> "S"

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let uniq_elem e s =
    match e with
    | U -> false::s
    | S -> true ::s

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let uniq_elem : stream -> uniq_elem * stream =
    function
    | [] -> assert false
    | h::t -> (if h then S else U), t

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let uniq_elem cha e = bool cha (e = S)

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let uniq_elem cha = if bool cha then S else U

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

(* [TODO] make edge' polarized *)
module ToBa =
struct
  open BTools.ToB

  let next' a (ident:'i tob) nx (s:stream) : stream =
    match nx with
    | Tree.GLeaf () -> assert(a = 0); s
    | Tree.GLink lk -> assert(a > 0); ident lk s

  let edge' a (ident:'i tob) ((b, l), nx) (s:stream) : stream =
    let s = next' (MyList.count ((=)S) l) ident nx s in
    let s = sized_list ToB.uniq_elem l s in
    let s = bool b s in
    s

  let node' (ident:'i tob) (node:_ node') (s:stream) : stream =
    let a = arity_node' node in
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) node s
end

(* [TODO] make edge' polarized *)
module OfBa =
struct
  open BTools.OfB

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    if a = 0
    then (Tree.GLeaf(), s)
    else (let lk, s = ident a s in (Tree.GLink lk, s))

  let edge' a (ident:int -> 'i ofb) (s:stream) : _ edge' * stream =
    let b, s = bool s in
    let l, s = sized_list OfB.uniq_elem a s in
    let nx, s = next' (MyList.count ((=)S) l) ident s in
    (((b, l), nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

module PrettyToS =
struct
  let edge (neg, sub) =
    String.concat "" ((if neg then "-" else "+")::(Tools.map ToS.uniq_elem sub))
end

(* [OLD]
module BW =
struct
  (* uel : uniq_elem list *)
  open ToBStream

  let rle (uniq:uniq) =
    let rec aux carry c0 n0 = function
      | [] -> List.rev (n0::carry)
      | c1::t1 -> (
        if c1 = c0 then (
          aux carry c0 (succ n0) t1
        ) else (
          aux (n0::carry) c1 0 t1
        )
      )
    in
    match uniq with
    | [] -> None
    | c0::t0 ->
      Some(c0, aux [] c0 0 t0)

  let cha_rle cha (c0, rle) =
    bool cha (c0 = S);
    list int cha rle

  let uniq cha uniq =
    option cha_rle cha (rle uniq)

  let uniq cha u =
    print_string "\n[W]uniq"; print_newline();
    let cha0 = open_flush () in
    let cha1 = open_tee cha cha0 in
    uniq cha1 u;
    ignore(close_tee cha1);
    close_flush cha0;
    print_string "\n[W]uniq! : ";
    print_string (ToS.list Ldd_B_u_nu_dumpload.stree_of_uniq_elem)
    print_newline()

  let edge_state cha (b, u) =
    bool cha b;
    uniq cha u

  let pair cha pair =
    let u0, u1 = Ldd_B_u_nu_dumpload.split_pair pair in
    uniq cha u0;
    uniq cha u1;
end

module BR =
struct
  open ToBStream
  IntHeap

  let rle c0 (rle:int list) =
    let neg = function
      | U -> S
      | S -> U
    in
    let rec aux0 carry c0 = function
      | [] -> List.rev carry
      | n0::t0 -> (
        aux0 ((MyList.ntimes c0 n0)@carry) (neg c0) t0
      )
    in aux0 [] c0 rle

  let cha_rle cha =
    let c0 = if bool cha then S else U in
    let rle_ : int list = list int cha in
    rle c0 rle_

  let uniq cha : uniq =
    match option cha_rle cha with
    | None -> []
    | Some liste -> liste

  let uniq cha : uniq =
    print_string "\n[R]uniq"; print_newline();
    let cha = open_flush cha in
    let u = uniq cha in
    close_flush cha;
    print_string "\n[R]uniq!"; print_newline();
    u

  let edge_state cha : edge_state =
    let b = bool cha in
    let u = uniq cha in
    (b, u)

  let pair cha =
    let u0 = uniq cha in
    let u1 = uniq cha in
    assert(List.length u0 = List.length u1);
    Ldd_B_u_nu_dumpload.merge_pair u0 u1
end
*)
