(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nuc : Uniform model with Negation, Useless variables and Canalizing Variables
 *
 * module  : Ldd_u_nuc_types : Types
 *)

open GuaCaml

type elem =
  | S (* significant variable *)
  | U (* useless variable *)
  | C of (bool * int) (* if x then (i [2] Xor shift Xor neg) else ... *)

type suc_tag = {hasS : bool; hasU : bool; maxC : int option}

type block_suc = bool * suc_tag * (elem list)

type block_choice =
  | C0
  | Id  of int
  | SUC of block_suc

type block = {
  neg    : bool;
  arity  : int;
  block  : block_choice;
}

type leaf = unit
type edge = block
type node = unit

type 'i next' = ('i, leaf) Tree.gnext
type 'i edge' = edge * 'i next'
type 'i node' = node * 'i edge' * 'i edge'

type    peval  = Utils.peval
type   opeval  = peval option
type 'a plink  = opeval * 'a
type 'a pedge  = 'a plink edge'
type 'a pnode  = 'a plink node'

type 'a merge   = ('a next', 'a node') Utils.merge
type 'a emerge  = block * 'a merge
type 'a merge3  = ('a next', 'a node', 'a node') Utils.merge3
type 'a emerge3 = block * 'a merge3

type    quant  = Utils.quant
type  opquant  = quant option
type 'a qlink  = opquant * 'a
type 'a qedge  = 'a qlink edge'
type 'a qnode  = 'a qlink node'

type 'a edge'm = ('a, 'a node') Utils.merge edge'
