(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nuc : Uniform model with Negation, Useless variables and Canalizing Variables
 *
 * module  : Ldd_u_nuc_utils : Toolbox
 *)

open GuaCaml
open Utils
open Ldd_B_u_nuc_types
open Ldd_B_u_nuc_io

let arity_block block = block.arity
let arity_edge (block, _) = arity_block block
let neg_block block = {neg = not block.neg; arity = block.arity; block = block.block}
let cneg_block neg block = {neg = neg <> block.neg; arity = block.arity; block = block.block}
let neg_edge (block, node) = (neg_block block, node)
let cneg_edge neg (block, node) = (cneg_block neg block, node)

let pedge_of_edge ((ee, nx) : _ edge') : _ pedge =
  match nx with
  | Tree.GLeaf() -> (ee, Tree.GLeaf())
  | Tree.GLink lk -> (ee, Tree.GLink(None, lk))

let merge_of_edge (ee, nx) = (ee, MEdge nx)
let merge3_of_edge (ee, nx) = (ee, M3Edge nx)

(* let cst_merge3 b n = merge3_of_edge (cst_edge b n) *)

let (=>) (x:bool) (y:bool) : bool = (x <= y)
(*
assert((false => false) = true )
assert((false => true ) = true )
assert((true  => false) = false)
assert((true  => true ) = true )
*)

let make_nU n = MyList.make n U
let make_nS n = MyList.make n S

let isS   = function S   -> true   | _ -> false
let isntS = function S   -> false  | _ -> true
let isU   = function U   -> true   | _ -> false
let isntU = function U   -> false  | _ -> true
let isC   = function C _ -> true   | _ -> false
let isntC = function C _ -> false  | _ -> true

let shiftC shift = function C(b, i) -> C(b, i+shift) | elem -> elem

let hasS block = match block.block with
  | C0 | Id _ -> false
  | SUC (_, tag, _) -> tag.hasS

(* return true if x is odd, false otherwise *)
let mod2 x = (x mod 2) = 1

let block_arity block = block.arity
let edge_arity (block, _) = block_arity block

(* check that a block is properly formed *)
(* TODO
  - check for trailing 0s
  - check that shift is set to 0 if their is no C elem
  - check for a single trailing 1
  - check for sub-normal case +/- I (if false then true else false)
*)

let get_suc_tag_from_block_suc liste =
  let hasS, hasU, maxC = List.fold_left (fun (hasS, hasU, maxC) -> function
    | S      -> (true, hasU, maxC          )
    | U      -> (hasS, true, maxC          )
    | C(b, i)  -> (hasS, hasU, Tools.opmax i maxC  )
  ) (false, false, None) liste in
  {hasS; hasU; maxC}

let suc_is_const liste = List.for_all isU liste

let block_is_const block = match block.block with
  | C0 -> Some block.neg
  | _   -> None

let edge_is_const (block, next) = match next with
  | Tree.GLeaf() -> block_is_const block
  | _            -> None

let block_c0_to_block_suc arity =
  (false, {hasS = false; hasU = arity > 0; maxC = None}, make_nU arity )

let block_id_to_block_suc arity x =
    assert(arity > x);
    (true,  {hasS = false; hasU = arity > 1; maxC = Some 0}, (make_nU x)@((C(true, 0))::(make_nU (arity-x-1))))

let block_choice_to_block_suc arity = function
  | C0 -> block_c0_to_block_suc arity
  | Id x  -> block_id_to_block_suc arity x
  | SUC (shift, tag, liste) ->
    (shift, tag, liste)

let push_S_block block =
  let (shift, tag, liste) = block_choice_to_block_suc block.arity block.block in
  let tag = {hasS = true; hasU = tag.hasU; maxC = tag.maxC} in
  {neg = block.neg; arity = block.arity+1; block = SUC(shift, tag, S::liste)}

let push_S_edge (block, node) = (push_S_block block, node)

let push_U_block_choice = function
  | C0 -> C0
  | Id x -> Id (x+1)
  | SUC (shift, tag, liste) ->
    let tag = {hasS = tag.hasS; hasU = true; maxC = tag.maxC} in
    SUC (shift, tag, U::liste)

let push_U_block block = {neg = block.neg; arity = block.arity+1; block = push_U_block_choice block.block}

let push_U_edge (block, node) = (push_U_block block, node)

let minicheck_suc = List.for_all (function C(_, i) when i < 0 -> false | _ -> true)

let get_suc_tag_from_suc liste =
  let hasS, hasU, maxC = List.fold_left (fun (hasS, hasU, maxC) -> function
    | S      -> (true, hasU, maxC          )
    | U      -> (hasS, true, maxC          )
    | C(b, i)  -> (hasS, hasU, Tools.opmax i maxC  )
  ) (false, false, None) liste in
  {hasS; hasU; maxC}

let check_suc_tag tag liste : bool =
  (if tag.hasS then (List.exists isS liste) else (List.for_all isntS liste))&&
  (if tag.hasU then (List.exists isU liste) else (List.for_all isntU liste))&&
  (match tag.maxC with
    | None -> List.for_all isntC liste
    | Some 0 ->
    (
      (List.for_all (function C(_, i) -> i = 0 | _ -> true) liste)&&
      (List.exists isC liste)
    )
    | Some maxC ->
    (
      (List.for_all (function C(_, i) -> (0<=i)&&(i<=maxC) | _ -> true ) liste)&&
      (List.exists (function C(_, i) when i = maxC -> true | _ -> false) liste)
    )
  )

let get_suc_tag arity = function
  | C0  -> {hasS = false; hasU = arity > 0; maxC = None}
  | Id _  -> {hasS = false; hasU = arity > 1; maxC = None}
  | SUC (_, tag, liste) ->
    assert(check_suc_tag tag liste);
    tag

let get_maxC_block block = match block.block with
  | C0 | Id _ -> None
  | SUC(_, tag, _) -> tag.maxC

(**  Determine (if possible) the value of the function according to its top descriptor [block] for the valuation [x]
    Returns [None] otherwise
 **)

let in_block_suc (shift, tag, liste) sigma =
  match tag.maxC with
  | None -> (if tag.hasS then None else Some false)
    (* if no S and no C, then its a constant *)
  | Some maxC ->
  (
    match List.fold_left (fun opmin -> function
      | C(b, i), b' when b = b' ->
        Tools.opmin i opmin
      | _ -> opmin ) None (List.combine liste sigma) with
    | None -> if tag.hasS then None else Some false
    | Some i -> Some(shift <> mod2 i)
  )

let in_block_choice block sigma = match block with
  | C0 -> Some false
  | Id x -> Some(List.nth sigma x)
  | SUC block_suc -> in_block_suc block_suc sigma

let in_block block sigma = match in_block_choice block.block sigma with
  | None -> None
  | Some bool -> Some (block.neg <> bool)

let suc_is_contig (_, tag, liste) next_is_leaf : bool = match tag.maxC with
  (* checks that Cs are contiguous and that maxC(if not None) is really maxC *)
  | None -> true
  | Some maxC ->
  (
    let cnt = Array.make (maxC+1) 0 in
    let clk x = cnt.(x) <- cnt.(x) + 1 in
    List.iter (function C (_, x) -> clk x | _ -> ()) liste;
    (Array.for_all (fun x -> x > 0) cnt)&&(not(next_is_leaf && (cnt.(maxC) = 1)))
    (* check that the last line is not reduced to a single element *)
  )

let assert_block_suc arity (shift, tag, liste) next_is_leaf  =
  assert(arity = List.length liste);
  assert(check_suc_tag tag liste);
  assert(next_is_leaf => (not tag.hasS));
  match tag.maxC with
    | None -> assert((not shift)&&(not next_is_leaf))
    | Some 0 ->
      assert(next_is_leaf => (shift && (MyList.count isC liste > 1)))
    | Some maxC ->
    assert(
      (0 < maxC) && (maxC < arity) &&
      (MyList.count isC liste >= maxC+1) &&
      (next_is_leaf => (((mod2 maxC) <> shift)))
    )
  ;
  assert(suc_is_contig (shift, tag, liste) next_is_leaf);
  ()

let check_block_suc arity (shift, tag, liste) next_is_leaf : bool =
  (arity = List.length liste)&&
  (check_suc_tag tag liste)&&
  (next_is_leaf => (not tag.hasS))&&
  (tag.maxC = None => (shift = false))&&
  (match tag.maxC with
    | None -> (not shift)&&(next_is_leaf => tag.hasS)
    | Some 0 ->
      (next_is_leaf => (shift && (MyList.count isC liste > 1)))
    | Some maxC ->
      (0 < maxC) && (maxC < arity) &&
      (MyList.count isC liste >= maxC+1) &&
      (next_is_leaf => (((mod2 maxC) <> shift)))
  )&&
  (suc_is_contig (shift, tag, liste) next_is_leaf)

let assert_block_choice arity next_is_leaf = function
  | C0 -> ()
  | Id x -> assert ((x >= 0) && (x < arity)); ()
  | SUC block_suc -> assert_block_suc arity block_suc next_is_leaf; ()

let check_block_choice arity next_is_leaf = function
  | C0 -> next_is_leaf
  | Id x -> next_is_leaf && (x >= 0) && (x < arity)
  | SUC block_suc -> check_block_suc arity block_suc next_is_leaf

let assert_block block next_is_leaf =
  assert(0 <= block.arity);
  assert_block_choice block.arity next_is_leaf block.block;
  ()

let check_block block next_is_leaf =
  (0 <= block.arity) &&
  (check_block_choice block.arity next_is_leaf block.block)

let check_edge (block, node) = check_block block (gnode_is_leaf node)

let reduce_block_suc neg arity next_is_leaf shift liste =
  (* print_endline ("[reduce_block_suc] arity: "^(STools.ToS.int arity)); *)
  (* print_endline ("[reduce_block_suc] liste: "^(STools.ToS.list ToS.elem liste)); *)
  assert(List.length liste = arity);
  assert(minicheck_suc liste); (* just checks that Cs are positive *)
  let tag = get_suc_tag_from_suc liste in
  assert(next_is_leaf => (not tag.hasS));
  match tag.maxC with
  | None ->
    {neg; arity; block = (if next_is_leaf then C0 else SUC(false, tag, liste))}
  | Some maxC ->
  (
    (* check for contiguity *)
    let cnt = Array.make (maxC+1) 0 in
    let clk x = cnt.(x) <- cnt.(x) + 1 in
    List.iter (function C (_, x) -> clk x | _ -> ()) liste;
    let (shift, tag, liste) = if (Array.for_all (fun x -> x > 0) cnt)
      then (shift, tag, liste)
      else
      (
        (* Cs are not contiguous *)
        let minC = Tools.array_index (fun x -> x > 0) cnt |> Tools.unop in
        let shift = shift <> (mod2 minC) in
        let rec aux carry opt curr =
          assert(curr >= opt);
          if curr <= maxC
          then if cnt.(curr) > 0
            then if mod2 (curr-opt) <> (mod2 minC)
              then (aux ((opt+1)::carry) (opt+1) (curr+1))
              else (aux (opt::carry) opt (curr+1))
            else (aux ((-1)::carry) opt (curr+1))
          else (carry |> List.rev |> Array.of_list)
        in
        let remap = aux (0::(MyList.make minC (-1))) 0 (minC+1) in
        assert(Array.length remap = maxC+1);
        let liste = Tools.map (function S -> S | U -> U | C(b, i) -> C(b, remap.(i))) liste in
        let maxC = Array.fold_left max (-1) remap in
        assert(maxC >= 0);
        assert(List.exists (function C(_, i) when i = maxC -> true | _ -> false) liste);
        (shift, {hasS = tag.hasS; hasU = tag.hasU; maxC = Some maxC}, liste)
      )
    in
    let maxC = Tools.unop tag.maxC in
    if next_is_leaf
    then
    (
      assert(tag.hasS = false);
      (* check for a then 0 else 0 last line *)
      match (if shift <> (mod2 maxC)
        then (Ok (shift, tag, liste))
        else if maxC = 0
          then (Error {neg; arity; block = C0})
          else
          (
            let liste = Tools.map (function C(_, i) when i = maxC -> U | x -> x) liste in
            Ok (shift, {hasS = false; hasU = true; maxC = Some (maxC-1)}, liste)
          )
        ) with
      | Error block -> block
      | Ok (shift, tag, liste) ->
      (
        (* check that the last line is not reduced to a single element *)
        let maxC = Tools.unop tag.maxC in
        if MyList.count (function C(_, i) when i = maxC -> true | _ -> false) liste = 1
        then if maxC = 0
          then
          (
            let x, neg' = MyList.ifind (function C(b, j) -> assert(j=0); Some b | _ -> None) liste |> Tools.unop in
            {neg = (neg = neg'); arity; block = Id x}
          )
          else
          (
            let tag = {hasS = tag.hasS; hasU = tag.hasU; maxC = Some(maxC-1)} in
            let liste = Tools.map (function C(b, i) when i = maxC -> C(not b, i-1) | x -> x) liste in
            {neg = not neg; arity; block = SUC(not shift, tag, liste)}
          )
        else {neg; arity; block = (SUC(shift, tag, liste))}
      )

    )
    else  {neg; arity; block = (SUC(shift, tag, liste))}
  )

let reduce_block_choice neg arity next_is_leaf = function
  | C0 -> {neg; arity; block = C0}
  | Id x -> assert(0 <= x && x < arity); {neg; arity; block = Id x}
  | SUC (shift, _, liste) -> reduce_block_suc neg arity next_is_leaf shift liste

let reduce_block block next_is_leaf =
  (* print_string "@@reduce_block"; print_newline();
  print_string (Ldd_B_u_nuc_dump.block block); print_newline();
  print_string ("next_is_leaf = "^(ToS.bool next_is_leaf)); print_newline(); *)
  assert(0 <= block.arity);
  reduce_block_choice block.neg block.arity next_is_leaf block.block

let reduce_edge (block, node) =
  (reduce_block block (gnode_is_leaf node), node)

let suc_liste_to_block neg shift liste =
  reduce_block_suc neg (List.length liste) false shift liste

let suc_liste_to_edge neg shift (liste, node) =
  let block = reduce_block_suc neg (List.length liste) (gnode_is_leaf node) shift liste in
  (block, node)

let push_C0_block_suc iB tB (* if, rank = 0, then *) neg arity (shift, tag, liste) next_is_leaf =
  match tag.maxC with
  | None ->
  (
    assert(not next_is_leaf); (* next_is_leaf => no S; no S + no C => only U; C0 *)
    assert(not shift);
    let tag = {hasS = tag.hasS; hasU = tag.hasU; maxC = Some 0} in
    {neg; arity = arity+1; block = SUC(tB <> neg, tag, (C(iB, 0))::liste)}
  )
  | Some maxC ->
  (
    if (tB <> neg <> shift) = false
    (* correct alignment *)
    then {neg; arity = arity+1; block = SUC(shift, tag, (C(iB, 0))::liste)}
    (* incorrect alignment *)
    else
    (
      let liste = Tools.map (function C(b, i) -> C(b, i+1) | x -> x) liste in
      let tag = {hasS = tag.hasS; hasU = tag.hasU; maxC = Some(maxC+1)} in
      {neg; arity = arity+1; block = SUC(not shift, tag, (C(iB, 0))::liste)}
    )
  )

let push_C0_block_choice iB tB (* if, rank = 0, then *) neg arity next_is_leaf = function
  | C0 ->
  (
    assert(next_is_leaf);
    if neg = tB
    then {neg; arity = arity+1; block = C0}
    else {neg = iB <> tB; arity = arity+1; block = Id 0}
  )
  | Id x ->
  (
    assert(next_is_leaf);
    let tag = {hasS = false; hasU = arity>1; maxC = Some 0} in
    let tB' = tB <> neg in
    assert(arity >= x+1);
    let liste = (C(iB, 0))::((make_nU x)@((C(tB', 0))::(make_nU (arity-x-1)))) in
    {neg = not tB; arity = arity+1; block = SUC(true, tag, liste)}
  )
  | SUC block_suc -> push_C0_block_suc iB tB neg arity block_suc next_is_leaf

let push_C0_block iB tB (* if, rank = 0, then *) block next_is_leaf =
  push_C0_block_choice iB tB block.neg block.arity next_is_leaf block.block

let push_C0_edge iB tB (* if, rank = 0, then *) (block, node) =
  (push_C0_block iB tB block (gnode_is_leaf node), node)

let count_nS_suc = MyList.count isS

let count_nS_block_suc (_, _, l) =
  count_nS_suc l

let count_nS_block_choice = function
  | C0 | Id _ -> 0
  | SUC(_, _, liste) -> count_nS_suc liste

let count_nS_block block = count_nS_block_choice block.block
let count_nS_edge (block, node) = count_nS_block block

let make_block_S neg arity =
  assert(arity>=0);
  {neg; arity; block = SUC(false, {hasS = arity > 0; hasU = false; maxC = None}, make_nS arity)}

let make_block_C0 neg arity =
  {neg; arity; block = C0}

let make_edge_C0 neg arity = (make_block_C0 neg arity, Tree.GLeaf())

let make_block_U neg arity next_is_leaf =
  assert(arity>=0);
  {neg; arity; block = if next_is_leaf then C0 else SUC(false, {hasS = false; hasU = arity>0; maxC = None}, MyList.ntimes U arity)}

let cmake_nS  neg  block = make_block_S  neg  (count_nS_block block)

let compose_block_suc_suc (shift, tag, liste) (shift', tag', liste') =
  (* print_string "@@compose_block_suc_suc: begin"; print_newline(); *)
  let compose0 = Utils.compose S in
  let compose shift sucC succ =
(*    print_string "--@@compose: begin"; print_newline();
    print_string ("List.length sucC = "^(ToS.int(List.length sucC))); print_newline();
    print_string ("List.length succ = "^(ToS.int(List.length succ))); print_newline(); *)
    let sucCc = compose0 sucC (Tools.map (shiftC shift) succ) in
(*    print_string ("List.length sucCc = "^(ToS.int(List.length sucCc))); print_newline();
    print_string "--@@compose: end"; print_newline(); *)
    sucCc
  in
  let shift, maxC, liste = match tag.maxC with
  | None ->
  (
    assert(shift = false);
    (shift', tag'.maxC, compose0 liste liste')
  )
  | Some maxC ->
  (
    match tag'.maxC with
    | None ->
    (
      assert(shift' = false);
      (shift, tag.maxC, compose0 liste liste')
    )
    | Some maxC' ->
    (
      (* print_string "-- Some maxC , Some maxC'"; print_newline(); *)
      let shiftC = if (shift <> (mod2 maxC)) = shift' then maxC else (maxC+1) in
      (shift, Some(shiftC+maxC'), compose shiftC liste liste')
    )
  )
  in
  let tag = {hasS = tag'.hasS; hasU = tag.hasU || tag'.hasU; maxC} in
  let return = (shift, tag, liste) in
  (*print_string (Ldd_B_u_nuc_dump.block_suc return); print_newline();
  print_string "@@compose_block_suc_suc: end"; print_newline();*)
  return

let compose_block blockC blockc next_is_leaf =
  assert(check_block blockC false);
  assert(check_block blockc next_is_leaf);
  assert(count_nS_block blockC = blockc.arity);
  match blockC.block with
  | C0 | Id _ -> assert false
  | SUC(shift, tag, liste) ->
  let blockCc = match tag.maxC with
  | None ->
  (
    (* block SU *)
    (* no need for reduction *)
    assert(shift = false);
    match blockc.block with
    | C0 ->
    (
      assert(next_is_leaf);
      {neg = blockC.neg <> blockc.neg; arity = blockC.arity; block = C0}
    )
    | Id x ->
    (
      assert(next_is_leaf);
      (* blockC is SUC but only one S and no C *)
      let rec aux posC posc : _ list -> int = assert(posc>=0); function
          | [] -> assert false
          | head::tail -> match head with
            | S -> if posc=0
              then posC
              else (aux (posC+1) (posc-1) tail)
            | _ ->  aux (posC+1)  posc    tail
      in
      {neg = blockC.neg <> blockc.neg; arity = blockC.arity; block = Id (aux 0 x liste)}
    )
    | SUC(shift', tag', liste') ->
    (
      let block_suc = compose_block_suc_suc (false, tag, liste) (shift', tag', liste') in
      {neg = blockC.neg <> blockc.neg; arity = blockC.arity; block = SUC block_suc}
    )
  )
  | Some maxC ->
  (
    (* needs reduction *)
    match blockc.block with
    | C0 ->
    (
      assert(next_is_leaf);
      let tag = {hasS = false; hasU = tag.hasU || tag.hasS; maxC = tag.maxC} in
      let liste = Tools.map (function S -> U |  e -> e) liste in
      {neg = blockC.neg <> blockc.neg; arity = blockC.arity; block = SUC(shift <> blockc.neg, tag, liste)}
    )
    | Id x ->
    (
      assert(next_is_leaf);
      let (shift', tag', liste') = block_id_to_block_suc blockc.arity x in
      let block_suc = compose_block_suc_suc (shift <> blockc.neg, tag, liste) (shift', tag', liste') in
      {neg = blockC.neg <> blockc.neg; arity = blockC.arity; block = SUC block_suc}
    )
    | SUC(shift', tag', liste') ->
    (
      let block_suc = compose_block_suc_suc (shift <> blockc.neg, tag, liste) (shift', tag', liste') in
      {neg = blockC.neg <> blockc.neg; arity = blockC.arity; block = SUC block_suc}
    )
  )
  in
  reduce_block blockCc next_is_leaf
(*
let compose_block blockC blockc next_is_leaf =
  (* pretty wrapper *)
  print_string "@@compose_block: begin"; print_newline();
  print_string ("blockC = "^(Ldd_B_u_nuc_dump.block blockC)); print_newline();
  print_string ("blockC = "^(Ldd_B_u_nuc_dump.block blockc)); print_newline();
  print_string ("next_is_leaf = "^(ToS.bool next_is_leaf)); print_newline();
  let blockCc = compose_block blockC blockc next_is_leaf in
  print_string ("blockCc = "^(Ldd_B_u_nuc_dump.block blockCc)); print_newline();
  print_string "@@compose_block: end"; print_newline();
  blockCc
*)
let compose_edge blockC (blockc, nodec) =
    (*print_string "@@compose_edge: start"; print_newline();*)
    let r = (compose_block blockC blockc (gnode_is_leaf nodec), nodec) in
    (*print_string "@@compose_edge: end"; print_newline();*)
    r

let compose_merge blockC (blockc, merge) = match merge with
  | MEdge next -> merge_of_edge (compose_edge blockC (blockc, next))
  | MNode node -> (compose_block blockC blockc false, MNode node)

let compose_merge_node_edge blockC = function
  | MEdge edge -> compose_edge blockC edge
  | MNode node -> (blockC, Tree.GLink node)

let compose_merge3 blockC (blockc, merge) = match merge with
  | M3Edge next -> merge3_of_edge (compose_edge blockC (blockc, next))
  | _ -> (compose_block blockC blockc false, merge)

let arity_merge (block, _) = block.arity

let arity_merge3 (block, _) = block.arity

let check_mergeC (blockC, merge) = match merge with
  | MEdge  next -> check_edge (blockC, next)
  | MNode ((), edge0, edge1) ->
  (
    (check_block blockC false)&&
    (check_edge edge0)&&
    (check_edge edge1)&&
    (arity_edge edge0 = arity_edge edge1)&&
    (count_nS_block blockC = arity_edge edge0 + 1)
  )

let check_mergeAC (blockC, merge) = match merge with
  | MEdge  next -> check_edge (blockC, next)
  | MNode ((), edge0, edge1) ->
  (
    (check_block blockC false)&&
    (check_edge edge0)&&
    (check_edge edge1)&&
    (arity_edge edge0 = arity_edge edge1)&&
    (count_nS_block blockC = arity_edge edge0)
  )

let check_merge3 (blockC, merge) = match merge with
  | M3Edge  next -> check_edge (blockC, next)
  | M3Cons ((), edge0, edge1) ->
  (
    (check_block blockC false)&&
    (check_edge edge0)&&
    (check_edge edge1)&&
    (arity_edge edge0 = arity_edge edge1)&&
    (count_nS_block blockC = arity_edge edge0 + 1)
  )
  | M3Node ((), edge0, edge1) ->
  (
    (check_block blockC false)&&
    (check_edge edge0)&&
    (check_edge edge1)&&
    (arity_edge edge0 = arity_edge edge1)&&
    (count_nS_block blockC = arity_edge edge0)
  )

let make_cst neg arity =
  ({neg; arity; block = C0}, Tree.GLeaf())
let get_root b f = make_cst b (arity_edge f)
let make_id neg arity i =
  ({neg; arity; block = Id i}, Tree.GLeaf())

let neg (block, node) = ({neg = not block.neg; arity = block.arity; block = block.block}, node)
let cneg b (block, node) = ({neg = b <> block.neg; arity = block.arity; block = block.block}, node)

let peval_edge (peval : peval) : 'i edge' -> 'i edge' * opeval =
  let arity = List.length peval in
  let arity' = MyList.count (function None -> true | _ -> false) peval in
  fun (block, node) ->
    assert(check_edge (block, node));
    assert(arity = block.arity);
    match block.block with
    | C0 -> (({neg = block.neg; arity = arity'; block = C0}, Tree.GLeaf()), None)
    | Id x ->
    (
      match List.nth peval x with
      | Some b -> (({neg = block.neg <> b; arity = arity'; block = C0}, Tree.GLeaf()), None)
      | None ->
      (
        let x' = MyList.counti (fun i -> function None when i < x -> true | _ -> false) peval in
        (({neg = block.neg; arity = arity'; block = Id x'}, Tree.GLeaf()), None)
      )
    )
    | SUC(shift, tag, liste) ->
    (
      let opsuboppeval, opmin = MyList.foldmap (fun (opmin : int option) -> fun ((peval : bool option), elem) -> match peval with
        | None      -> ((Some elem, (match elem with S -> Some None | _ -> None)), opmin)
        | Some peval  -> match elem with
          | U        -> ((None     , None                                        ), opmin)
          | S        -> ((None     , Some (Some peval)                           ), opmin)
          | C(b, i) -> let opmin = if b = peval then Tools.opmin i opmin else opmin in
                       ((None     , None                                        ), opmin)
        ) (fun _ -> true) None (List.combine peval liste) in
      let opliste, oppeval = List.split opsuboppeval in
      let liste = MyList.list_of_oplist opliste
      and peval   = MyList.list_of_oplist oppeval   in
      match opmin with
      | None ->
      (
        let block = reduce_block_suc block.neg arity' (gnode_is_leaf node) shift liste in
        ((block, node), (if List.for_all ((=)None) peval then None else (Some peval)))
      )
      | Some min ->
      (
        let liste = Tools.map (function C(_, i) as e when i < min -> e | _ -> U) liste in
        let neg = block.neg <> shift <> (mod2 min) in
        let block = reduce_block_suc neg arity' true (mod2 min) liste in
        ((block, Tree.GLeaf()), None)
      )
    )

let opeval_edge = function
  | None -> (fun edge -> (edge, None))
  | Some set -> (fun edge -> peval_edge set edge)

let check_peval peval = List.exists Tools.isSome peval

let check_opeval = function
  | None -> true
  | Some peval -> check_peval peval

let compose_peval = Utils.compose_peval
let compose_opeval = Utils.compose_opeval

let peval_pedge peval (block, pnode) : _ pedge =
  let (block', pnode'), peval' = peval_edge peval (block, pnode) in
  let pnode' = match pnode' with
    | Tree.GLeaf () -> assert(peval' = None); Tree.GLeaf()
    | Tree.GLink (peval, node) -> Tree.GLink(Utils.compose_opeval peval' peval, node)
  in
  (block', pnode')

let opeval_pedge = function
  | None -> (fun pedge -> pedge)
  | Some peval -> (fun pedge -> peval_pedge peval pedge)

let peval_pnode peval ((), pedge0, pedge1) =
  ((), peval_pedge peval pedge0, peval_pedge peval pedge1)

let opeval_pnode = function
  | None -> (fun node -> node)
  | Some peval -> (fun node -> peval_pnode peval node)

let peval_pnodeC = function
  | [] -> assert false
  | None::peval -> (fun pnode -> MNode (peval_pnode peval pnode))
  | (Some false)::peval -> (fun ((), pedge0, _) -> MEdge (peval_pedge peval pedge0))
  | (Some true )::peval -> (fun ((), _, pedge1) -> MEdge (peval_pedge peval pedge1))

let select b ((), if0, if1) = if b then if1 else if0

(* Ldd_B_u_nu_types.(function true -> S | false -> U) *)
let compose_mask_edge mask (block, next) =
  assert(MyList.count_true mask = block.arity);
  let rowU = Tools.map (fun b -> if b then S else U) mask in
  let arity = List.length mask in
  let suc_tag = {
    hasS = block.arity > 0;
    hasU = arity > block.arity;
    maxC = None
  } in
  let blockC = {neg = false; arity; block = SUC (false, suc_tag, rowU)} in
  compose_edge blockC (block, next)
