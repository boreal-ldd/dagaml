(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-u : Ordered model with Useless variables
 *
 * module  : Ldd_o_u_avanced : Generic Functional Interface
 *)

open GuaCaml
open BTools
open Ldd_B_o_u

module ComputeSupport =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = unit
    type param = unit

    type xnode = bool list
    type xedge = bool list

    let map_next (():extra) (():param) : _ -> xedge = function
      | Tree.GLeaf _ -> []
      | Tree.GLink lk -> lk ()

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (():param) (((x, y), nx): _ M.M.edge') : xedge =
      MyList.ntimes ~carry:(map_next () () nx) false (x-y)

    let blist_union (bl1:bool list) (bl2:bool list) : bool list =
      List.map2 (||) bl1 bl2

    let map_node (():extra) (():param) (((), f0, f1): _ M.M.node') : xnode =
      let s0 = map_edge () () f0
      and s1 = map_edge () () f1 in
      (true::(blist_union s0 s1))
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.manager (* LDD-Support manager, defined for [Support] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : bool list =
    Module.rec_edge tt () f

  let solve_ilist (tt:tt) (f:LDD.f) : int list =
    solve tt f |> MyList.indexes_true
end

module ToSTree =
struct
  open STools
  open ToSTree
  module Model =
  struct
    module M = LDD.G0

    type extra = unit
    type param = Tree.stree list

    type xnode = Tree.stree
    type xedge = Tree.stree

    let map_next (():extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf lf -> Tree.Node [Tree.Leaf "CST"; bool lf]
      | Tree.GLink lk -> (lk param)

    let map_nU (():extra) (nU:int) (param:param) (next:param -> xedge) : xedge =
      let hd, tl = MyList.hdtl_nth nU param in
      next tl

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (param:param) (((x, y), nx): _ M.M.edge') : xedge =
      map_nU () (x-y) param (fun param -> map_next () param nx)

    let map_node (():extra) (param:param) ((_, f0, f1): _ M.M.node') : xnode =
      match param with
      | [] -> assert false
      | hd::tl -> (
        let s0 = map_edge () tl f0
        and s1 = map_edge () tl f1 in
        Tree.Node [Tree.Leaf "ITE"; hd; s0; s1]
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-ToSTree manager, defined for [ToSTree] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve ?(inputs=None) (tt:tt) (f:LDD.f) : Tree.stree =
    let ari = ThisG.arity f in
    let inputs =
      match inputs with
      | Some inputs -> (
        assert(List.length inputs = ari);
        inputs
      )
      | None -> (
        List.init ari (fun x -> Tree.Node [Tree.Leaf "VAR"; Tree.Leaf (string_of_int x)])
      )
    in
    Module.rec_edge tt inputs f

  let solve_to_string ?(inputs=None) (tt:tt) (f:LDD.f) : string =
    let stree = solve ~inputs tt f in
    OfSTree.to_string stree

end

module CntSat =
struct
  open BTools
  module Model =
  struct
    module M = LDD.G0
    open BArray.Nat

    type extra = unit
    type param = unit

    type xnode = nat
    type xedge = nat

    let map_next (():extra) (():param) : _ -> nat = function
      | Tree.GLeaf cst -> if cst then unit() else zero()
      | Tree.GLink lk -> lk ()

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (():param) (((x, y), nx): _ M.M.edge') : xedge =
      let nU = x - y in
      shift_lefti (map_next () () nx) nU

    let map_node (():extra) (():param) ((_, f0, f1): _ M.M.node') : xnode =
      (map_edge () () f0) +/ (map_edge () () f1)
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-CntSat manager, defined for [CntSat] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : BNat.nat =
    Module.rec_edge tt () f

end

(* Forall Relative Naming : eliminate quantified variables from the ordering *)
module ForallR =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }
    type param = bool list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) (param:param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (MyList.count_false hd, tl)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf lf -> cst lf 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) (((x, y), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      let nU = x - y in
      let nU, param' = map_nU nU param in
      ThisG.pushU ~n:nU (map_next extra param' nx)

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | b0::param' -> (
        let g0 = map_edge extra param' f0
        and g1 = map_edge extra param' f1 in
        if b0
        (* b0 = true  : eliminate the variable *)
        then (LDD.And.solve extra.and_man g0 g1)
        (* b0 = false : keep the variable *)
        else (LDD.G1.push extra.ldd_man ((), g0, g1))
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* Forall Absolute Naming (V0) : does not change variable names *)
(* remark:
  absolute naming can be emulated using either :
    - a wrapper for support variables, or
    - LDD-U-U based models (with uniform useless variables)
 *)
module ForallA =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }
    type param = bool list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) (param:param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (nU, tl)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf lf -> cst lf 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) (((x, y), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      let nU = x - y in
      let nU, param' = map_nU nU param in
      ThisG.pushU ~n: nU (map_next extra param' nx)

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | b0::param' -> (
        let g0 = map_edge extra param' f0
        and g1 = map_edge extra param' f1 in
        if b0
        (* b0 = true  : eliminate the variable *)
        (* [RQ] only difference with relative naming version [push-U] *)
        then (ThisG.pushU ~n: 1 (LDD.And.solve extra.and_man g0 g1))
        (* b0 = false : keep the variable *)
        else (LDD.G1.push extra.ldd_man ((), g0, g1))
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* [LATER]
module ForallA1 =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
              and_man   : LDD.And.tt; (* And manager *)
      mutable profile   : OProfile.t;
    }
    type param = bool list
    type 'ee comb = param * 'ee
    type xedge = LDD.f

    (* (de)serialization *)

    let bw_param = ToBStream.(list bool)
    let br_param = OfBStream.(list bool)

    let bw_comb _ _ bwe = ToBStream.pair bw_param bwe
    let br_comb _ _ bre = OfBStream.pair br_param bre

    let bw_xedge man extra = M.BSafe.bw_edge man
    let br_xedge man extra cha =
      M.BSafe.hold_br_edge man extra.stone_ctx cha

    let map_comb f (bl, e) = (bl, f e)

    (* [solve_comb man extra solver comb = xedge] *)
    let solve_comb man extra (solver : _ comb -> xedge) ((bl, e): _ comb) : xedge =
      (* assert hold on input functions *)
      M.BSafe.add_edge_stone man extra.stone_ctx e;
      let result = Ldd_G_o_u_gops.Compute.compute_forallA bl e in
      match result with
      | Utils.MEdge e -> e
      | Utils.MNode (nU', (bl', e')) -> (
        let e0, e1 = M.BSafe.comb_pull man e' in
        match bl' with
        | [] -> assert false
        | b0::bl'' ->  (
          let f0 = solver (bl'', e0)
          and f1 = solver (bl'', e1) in
          let ff0 = M.of_gc_f man f0
          and ff1 = M.of_gc_f man f1 in
          let gg =
            if b0
            (* b0 = true  : eliminate the variable *)
            (* [RQ] only difference with relative naming version [push-U] *)
            then (ThisG.pushU ~n: (nU' + 1)
                    (LDD.And.solve extra.and_man ff0 ff1))
            (* b0 = false : keep the variable *)
            else (ThisG.pushU ~n: nU'
                    (M.push man (ff0, ff1)))
          in
          let g = M.to_gc_f gg in
          M.BSafe.add_edge_stone man extra.stone_ctx g;
          ignore(gg);
          g
        )
      )

    (* [LATER] [TODO] *)
    let simpl_comb man extra solver comb = solver comb
  end

  module Module = MbdagVisitor.EdgeCombinatorBasedExplorer_WithHashCache.Make(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-ForallA manager, defined for [ForallA] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (t:t) (and_man:LDD.And.tt) : tt =
    let profile   = OProfile.newman ~prefix:"[And]" () in
    OProfile.stop_other profile;
    let extra = Model.{
      finalize = Some (fun _ -> ());
      stone_ctx = M.BSafe.new_stone t;
      and_man;
      profile;
    } in
    let man = Module.makeman t ~hsize extra in
    (* [TODO] make a cleaner interface to add/rem post-gc-process *)
    (*
    let postgc = ref (fun () -> Module.postgc man) in
    LDD.G1.BSafe.add_gc_post t (fun _ -> !postgc ());
    extra.Model.finalize <- Some (fun () -> extra.Model.finalize <- None; postgc := (fun () -> ()));
     *)
    man
  let newman ldd_mand and_man : tt = makeman ldd_mand and_man
  let finalize (t:tt) : unit =
    match (Module.get_extra t).finalize with
    | Some f -> f()
    | None -> assert false

  let solve (tt:tt) (bl:bool list) (f:Model.M.f) : Model.M.f =
    let extra  = Module.get_extra tt in
    assert(not(extra.Model.finalize = None));
    OProfile.start_other extra.profile;
    let eZ = Module.solve_func tt (bl, f) in
    OProfile.stop_other extra.profile;
    let t  = Module.get_master tt in
    let fZ = LDD.G1.of_gc_f t eZ in
    Model.M.BSafe.del_stone t extra.Model.stone_ctx;
    extra.Model.stone_ctx <- Model.M.BSafe.new_stone t;
    fZ

  let print_profile (tt:tt) : unit =
    let extra  = Module.get_extra tt in
    OProfile.print extra.profile
end
 *)

(* Partial Evaluation (Relative Naming) *)
module PEvalR =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.t;  (* LDD manager *)
    }
    type param = bool option list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) (param:param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (MyList.count_None hd, tl)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf lf -> cst lf 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) (((x, y), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      let nU = x - y in
      let nU, param' = map_nU nU param in
      ThisG.pushU ~n: nU (map_next extra param' nx)

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | opb::param' -> (
        match opb with
        | None -> (
          let g0 = map_edge extra param' f0
          and g1 = map_edge extra param' f1 in
          LDD.G1.push extra.ldd_man ((), g0, g1)
        )
        | Some b -> (
          map_edge extra param' (if b then f1 else f0)
        )
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [PEvalR] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) : tt =
    let extra = Model.{ldd_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) : tt = makeman ldd_man

  let solve (tt:tt) (param:bool option list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* Partial Evaluation (Absolute Naming) *)
module PEvalA =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.t;  (* LDD manager *)
    }
    type param = bool option list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) (param:param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (nU, tl)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf lf -> cst lf 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) (((x, y), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      let nU = x - y in
      let nU, param' = map_nU nU param in
      ThisG.pushU ~n: nU (map_next extra param' nx)

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | opb::param' -> (
        match opb with
        | None -> (
          let g0 = map_edge extra param' f0
          and g1 = map_edge extra param' f1 in
          LDD.G1.push extra.ldd_man ((), g0, g1)
        )
        | Some b -> (
          ThisG.pushU ~n:1
            (map_edge extra param' (if b then f1 else f0))
        )
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) : tt =
    let extra = Model.{ldd_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) : tt = makeman ldd_man

  let solve (tt:tt) (param:bool option list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

module OOPS =
struct
  type tt = {
    man_oops : LDD.OOPS.Model.t;
    man_supp : ComputeSupport.tt;
    man_tostree : ToSTree.tt;
    man_pevala : PEvalA.tt;
    man_pevalr : PEvalR.tt;
    man_foralla : ForallA.tt;
    man_forallr : ForallR.tt;
    profile : OProfile.t;
  }

  let get_man_ldd tt =
    LDD.get_cons tt.man_oops

  type ff = LDD.f

  let makeman ?man_oops () =
    let man_oops = match man_oops with
      | Some man_oops -> man_oops
      | None         -> LDD.newman()
    in
    let man_ldd = LDD.get_cons man_oops in
    let man_and = LDD.get_mand man_oops in
    let man_supp = ComputeSupport.newman man_ldd in
    let man_tostree = ToSTree.newman man_ldd in
    let man_pevala = PEvalA.newman man_ldd in
    let man_pevalr = PEvalR.newman man_ldd in
    let man_foralla = ForallA.newman man_ldd man_and in
    let man_forallr = ForallR.newman man_ldd man_and in
    let profile = OProfile.newman ~prefix:"[OOPS]" () in
    {
      man_oops;
      man_supp; man_tostree; man_pevala; man_pevalr; man_foralla; man_forallr;
      profile
    }

  module Model =
  struct
    open LDD.OOPS.Model
    type t = tt
    type f = ff

    let newman () = makeman ()

    let print_profile (t:t) : unit =
      OProfile.stop_other t.profile;
      (* LDD.And.print_profile t.man_and; *)
      OProfile.print t.profile;
      ()

    (* Section 0. OOPS forward definition *)
      (* sub-section 1. Functional / Propositional Aspects *)
    let ( ->> ) t bl f = ( ->> ) t.man_oops bl f
    let arity  t f = arity t.man_oops f
    let   cneg t b f = cneg t.man_oops b f
    let    neg t f = cneg t true f
    let ( &! ) t f1 f2 = ( &! ) t.man_oops f1 f2
    let ( |! ) t f1 f2 = ( |! ) t.man_oops f1 f2
    let ( ^! ) t f1 f2 = ( ^! ) t.man_oops f1 f2
    let ( =! ) t f1 f2 = ( =! ) t.man_oops f1 f2
    let ( *! ) t f0 f1 = ( *! ) t.man_oops f0 f1

    let cst t b n = cst t.man_oops b n
    let var t b n k = var t.man_oops b n k
    let to_bool t f = to_bool t.man_oops f

    let cofactor t b f = cofactor t.man_oops b f
    let id t f = id t.man_oops f
    let eq t f0 f1 = eq t.man_oops f0 f1

      (* sub-section 2. Manager Management *)
    let copy_into t fl t' = copy_into t.man_oops fl t'.man_oops

    let to_barray ?(nocopy=false) ?(destruct=false) t fl =
      to_barray ~nocopy ~destruct t.man_oops fl

    let of_barray ?(t=None) ba =
      let t, fl = of_barray ~t:(Tools.opmap (fun t -> t.man_oops) t) ba in
      (makeman ~man_oops:t (), fl)

    let bw_fl ?(nocopy=false) ?(destruct=false) t =
      bw_fl ~nocopy ~destruct t.man_oops

    let br_fl ?(t=None) cha =
      let t, fl = br_fl ~t:(Tools.opmap (fun t -> t.man_oops) t) cha in
      (makeman ~man_oops:t (), fl)

    (* [FIXME] *)
    let t_stats t = t_stats t.man_oops

    let f_stats t fl = f_stats t.man_oops fl

    (* [FIXME] *)
    let clear_caches t =
      clear_caches t.man_oops

    let keep_clean ?(normalize=false) t fl =
      clear_caches t;
      keep_clean ~normalize t.man_oops fl

    let check t f = check t.man_oops f

    (* Section 3. File-based Management *)

    module F =
    struct
      type f' = F.f'

      let arity = F.arity
      let size  = F.size

      let of_f ?(nocopy=false) ?(normalize=true) t f =
        F.of_f ~nocopy ~normalize t.man_oops f

      let to_f t f' =
        F.to_f t.man_oops f'

      let free f' = F.free f'

      let tof = F.tof
      let off = F.off
    end

    let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) t =
      tof ~nocopy ~normalize ~destruct t.man_oops
    let off cha =
      let t, fl = off cha in
      (makeman ~man_oops:t (), fl)

    (* Section 2. QEOPS level interface *)

    let to_string (t:t) (f:f) : string =
      ToSTree.solve_to_string t.man_tostree f

    let support (t:t) (f:f) : Support.t =
      let l = ComputeSupport.solve t.man_supp f in
      assert(List.length l <= arity t f);
      Support.of_bool_list l

    let pevalA (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = PEvalA.solve t.man_pevala param f in
      assert(arity t g = arity t f);
      g

    let pevalR (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = PEvalR.solve t.man_pevalr param f in
      assert(arity t g = MyList.count_None param);
      g

    let forallA (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_bool_list param in
      assert(arity t f = List.length param);
      (* let g0 = ForallA0.solve t.man_foralla0 param f in *)
      let g = ForallA.solve t.man_foralla param f in
      (*
      if not (LDD.G1.(=/) g0 g)
      then (
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : begin";
        print_endline "[Ldd_G_o_u.OOPS.forallA] g0";
        print_string (to_string t g0);
        print_endline "[Ldd_G_o_u.OOPS.forallA] g";
        print_string (to_string t g);
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : end";
        assert false
      );
      *)
      assert(arity t g = arity t f);
      g

    let forallR (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_bool_list param in
      assert(arity t f = List.length param);
      let g = ForallR.solve t.man_forallr param f in
      assert(arity t g = MyList.count_false param);
      g

    let existsA t param f : f =
      neg t (forallA t param (neg t f))

    let existsR t param f : f =
      neg t (forallR t param (neg t f))

    (* Section 3. AQEOPS level interface *)

    let ( &!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((&!) t) f0 fl)

    let ( |!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((|!) t) f0 fl)

    let ( ^!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((^!) t) f0 fl)

    let ( =!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((=!) t) f0 fl)

    (* return [true] iff [prim] is implemented within the computation model *)
    let builtin_o_prim = function
      | AQEOPS.U -> true
      | _ -> false
    (* counts the number of ordered [prim] primitives available *)
    let detect_o_prim t p ((x, y), _) =
      match p with
      | AQEOPS.U -> x - y
      | _ -> 0
    let intro_o_prim t p n f =
      match p with
      | AQEOPS.U -> ThisG.pushU ~n f
      | _ -> assert false
    let elim_o_prim t p n ((x, y), nx) =
      match p with
      | AQEOPS.U -> assert(x-n >= y); ((x-n, y), nx)
      | _ -> assert false

  end

  let newman = Model.newman
  let print_profile = Model.print_profile

  include OOPS.Make(Model)

  module SUInt = SUInt.Make(Model)
  module VUInt = VUInt.Make(Model)
  module LoadCnf0 = LoadCnf.Make0(Model)
  module LoadCnfA = LoadCnf.MakeA(Model)
  module LoadCnfQ = LoadCnf.MakeQ(Model)
  module CoSTreD = AQEOPS_CoSTreD.Make(Model)
  module CoSTreD_CNF = AQEOPS_CoSTreD.MakeCNF(Model)
  module CoSTreD_CNF_D4 = AQEOPS_CoSTreD.MakeCNF_D4(Model)
  module CCoSTreD = AQEOPS_CCoSTreD.Make(Model)
  module CCoSTreD_CNF = AQEOPS_CCoSTreD.MakeCNF(Model)
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Model)
end

module CSTypes = Constraint_system_types
module CSUtils = Constraint_system_utils
module CSCoSTreD  = Constraint_system_costred

module SNAX = SnaxOops.OOPS.Model

module CoSTreD_Model : CSCoSTreD.MSig =
struct
  module Q = OOPS.Model

  module BddToSnax = ComposeOops(SNAX)
  let translate_pure_to_snax (pure:Q.t) (snax:SNAX.t) (fl:Q.f list) : SNAX.f list =
      let pure = OOPS.get_man_ldd pure in
      BddToSnax.translate pure snax fl

  let default_module_name = default_module_name
end

module CoSTreD_Module = CSCoSTreD.Make(CoSTreD_Model)


module QbfCCoSTreD = (* [FIXME] : CnfUtils.CnfCoSTreD_Sig = *)
struct

  module BddToSnax = ComposeOops(SnaxOops.OOPS.Model)

  type t = LDD.G1.manager
  type f = LDD.G0.edge'

  let load_qbf target = StrLoadCnf.load_qbf_file ~discardQ:false target

  let wap_solver =
    if true
    then Cwap_lightweight.lightspeed_solver ~verbose:true
    else Cwap_lightweight.lightweight_solver ~verbose:true

  let conj_to_snax (pure:t) (ninputs:int) (pure_fa:f array) : Snax.ssystem =
    let pure_fl = Array.to_list pure_fa in
    let snax = Snax.Module.G0.newman() in
    let snax_fl = BddToSnax.translate pure snax pure_fl in
    let snax_conj =
      SnaxOops.OOPS.list_and
         snax
        ~arity:(Some ninputs)
         snax_fl
    in
    IoUtils.make ("CCoSTreD_Bpp_"^default_module_name) snax ninputs
      [| snax_conj.SnaxOops.OOPS.Model.edge |]

  let conj_to_v pure ninputs pure_fa (target:string) : unit =
    let ssnax = conj_to_snax pure ninputs pure_fa in
    let sexpr = IoUtils.expr_of_snax ssnax IoUtils.string_fnn in
    StrDumpVerilog.dump_file target sexpr;
    ()

  let to_pure_file ldd fl file =
    let stop = OProfile.(time_start default)
      "[test_ccostred.to_file] file generation {out:pure}"
    in
    let cha = open_out file in
    let ldd0 = LDD.G1.export ldd in
    LDD.G0.ToF.man_edges ldd0 cha fl;
    close_out cha;
    stop();
    ()

  let dump_costred (ldd:t) (arity:int) (fa:f array) (mode:CnfUtils.CnfCoSTreD.mode_out) (file_out:string) : unit =
    match mode with
    | CnfUtils.CnfCoSTreD.MO_Pure -> to_pure_file ldd (Array.to_list fa) file_out
    | CnfUtils.CnfCoSTreD.MO_Verilog -> (
      let stop = OProfile.time_start
        OProfile.default
        "[test_ccostred.conj_to_v] file generation {out:verilog}"
      in
      conj_to_v ldd arity fa file_out;
      stop();
    )

  let main_bpp_synthesis file_in mode file_out =
    let cnf = load_qbf file_in in
    let oops = OOPS.Model.newman() in
    let bdd_cnf = OOPS.LoadCnfA.export_cnf oops cnf in
    let wap_in = CnfUtils.Wap.input_of_qbf cnf in
    assert(wap_in.Cwap_exchange.parameters = []);
    (* Not Implemented Yet *)
    let wap_out = wap_solver wap_in in
    let seq = wap_out.Cwap_exchange.sequence in
    let bpp = Array.map (fun oc ->
      let supp = Cwap_exchange.(SetList.union_list [oc.internal; oc.cfrp_exists; oc.cfrp_forall; oc.cfrp_params]) in
      let proj = SetList.minus wap_in.Cwap_exchange.variables supp in
      let proj_supp = Support.of_sorted_support cnf.input proj in
      OOPS.Model.existsA oops proj_supp bdd_cnf
    ) seq in
    (* print_endline ("[test] bdd_frp:"^(to_string bdd_frp)); *)
    let ldd = OOPS.get_man_ldd oops in
    dump_costred ldd cnf.input bpp mode file_out

  let main_debug file_in ?(bucketize_mode=Tools.ASAP) ?(file_frp=None) ?(file_bpp=None) file_out =
    let cnf = load_qbf file_in in
    let oops = OOPS.newman() in
    (*
    let to_string (f:f) : string =
      OOPS.Model.to_string oops f
    in
     *)
    let costred =
      OOPS.CCoSTreD.CNF.compute ~bucketize_mode ~treefy_bucket:true wap_solver oops cnf
    in
    let ldd = OOPS.get_man_ldd oops in
(*    Tools.opiter
      (fun file -> to_pure_file ldd (costred.OOPS.CCoSTreD.out_mid |> Array.to_list) file)
       file_frp; [FIXME] *)
    Tools.opiter
      (fun file -> to_pure_file ldd (costred.OOPS.CCoSTreD.out_bpp |> Array.to_list) file)
       file_bpp;
    let bdd_cnf = OOPS.LoadCnfA.export_cnf oops cnf in
    (* print_endline ("[test] bdd_cnf:"^(to_string bdd_cnf)); *)
 (*   let bdd_frp = OOPS.array_and oops
      costred.OOPS.CCoSTreD.out_mid
    in *)
    (* print_endline ("[test] bdd_frp:"^(to_string bdd_frp)); *)
    let bdd_bpp = OOPS.array_and oops
      costred.OOPS.CCoSTreD.out_bpp
    in
    (* print_endline ("[test] bdd_bpp:"^(to_string bdd_bpp)); *)
(*    assert (bdd_cnf = bdd_frp); *)
    assert (bdd_cnf = bdd_bpp);
    to_pure_file ldd [bdd_bpp] file_out;
    ()

  let main_perf_bpp ?(bucketize_mode=Tools.ASAP) file_in mode file_out =
    let cnf = load_qbf file_in in
    let oops = OOPS.newman() in
    let costred =
      OOPS.CCoSTreD.CNF.compute ~bucketize_mode ~treefy_bucket:true wap_solver oops cnf
    in
    (* print_endline ("[test] bdd_frp:"^(to_string bdd_frp)); *)
    let ldd = OOPS.get_man_ldd oops in
    dump_costred ldd cnf.input costred.OOPS.CCoSTreD.out_bpp mode file_out

  let main_perf_bpp_aqeops file_in mode file_out =
    let cnf = load_qbf file_in in
    let oops = OOPS.newman() in
    let costred =
      OOPS.CCoSTreD_CNF.CNF.compute ~treefy_bucket:true wap_solver oops cnf
    in
    (* print_endline ("[test] bdd_frp:"^(to_string bdd_frp)); *)
    let ldd = OOPS.get_man_ldd oops in
    dump_costred ldd cnf.input costred.OOPS.CCoSTreD_CNF.CNF.out_bpp mode file_out

  (* [FIXME] *)
  let main_perf_frp file_in file_out = assert false
(*
    print_endline "[test_costred] --frp";
    let cnf = load_qbf file_in in
    let oops = OOPS.newman() in
    let costred =
      OOPS.CCoSTreD.CNF.compute ~treefy_bucket:true ~frp_only:true wap_solver oops cnf
    in
    (* print_endline ("[test] bdd_frp:"^(to_string bdd_frp)); *)
    let costred_frp = Array.to_list
      costred.OOPS.CCoSTreD.out_mid
    in
    let ldd = OOPS.get_man_ldd oops in
    to_pure_file ldd costred_frp file_out;
    ()
 *)

  let main_perf_dummy file_in file_out =
    let cnf = load_qbf file_in in
    let oops = OOPS.newman() in
    let f = OOPS.CCoSTreD.CNF.compute_dummy ~lvl:1 oops cnf in
    let ldd = OOPS.get_man_ldd oops in
    to_pure_file ldd [f] file_out;
    ()
end
