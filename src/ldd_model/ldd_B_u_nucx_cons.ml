(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx : Normalizing Shannon Operator
 *)

open GuaCaml
open AB
open Utils

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_pull
open Ldd_B_u_nucx_io

let ddl_cons = false
let ddl_and  = false
let ddl_xor  = false

let reduce_edge (block, next) =
  (normalize_block block, next)

let print_node _ _ = ()

let solve_cons_reduce (((), edge0, edge1) as node) =
  print_node "$C-" node;
  if do_check then assert(check_edge edge0);
  if do_check then assert(check_edge edge1);
  (* let edge0 = reduce_edge edge0
  and edge1 = reduce_edge edge1 in *)
  solve_cons edge0 edge1

let merge3_cons fX fY =
  if do_check then assert(check_edge fX);
  if do_check then assert(check_edge fY);
  match solve_cons fX fY with
  | (blk, MEdge link) -> (blk, M3Edge link)
  | (blk, MNode node) -> (blk, M3Cons node)

(* val pull_node : 'i M.edge' -> ('i M.node', 'i * ('i M.node' -> 'i M.node')) Utils.merge *)

let node_pull (((block, node) as edge):'lk edge') : ('lk node', 'lk * ('lk node' -> 'lk node')) Utils.merge =
  if do_check then assert(check_edge edge);
  if block.noutput <= 0
  then failwith ("[DAGaml.Ldd_B_u_nucx_cons.node_pull] block.noutput:"^(STools.ToS.int block.noutput));
  let noutput = pred block.noutput in
  match block.decomp with
  | C0 ->
  (
    assert(node = Tree.GLeaf());
    let edge' = ({block with noutput}, Tree.GLeaf()) in
    if do_check then assert(check_edge edge');
    MEdge ((), edge', edge')
  )
  | Id 0 ->
  (
    assert(node = Tree.GLeaf());
    let edge0 = ({block with noutput; decomp = C0}, Tree.GLeaf())
    and edge1 = ({block with neg = not block.neg; noutput; decomp = C0}, Tree.GLeaf()) in
    if do_check then assert(check_edge edge0);
    if do_check then assert(check_edge edge1);
    MEdge ((), edge0, edge1)
  )
  | Id x ->
  (
    assert(node = Tree.GLeaf());
    assert(x > 0);
    let edge' = ({block with noutput; decomp = Id(pred x)}, Tree.GLeaf()) in
    if do_check then assert(check_edge edge');
    MEdge ((), edge', edge')
  )
  | UCX matUCX -> (
    match pull_bmatUCX block.ninput node (block.neg, matUCX) with
    | A(f0, f1) -> MEdge((), f0, f1)
    | B delta -> (
      MNode (gnode_node node, fun ((), f0, f1) ->
        if do_check then assert(check_edge f0);
        if do_check then assert(check_edge f1);
        let f0' = compose_bmatUCX_edge delta f0
        and f1' = compose_bmatUCX_edge delta f1 in
        if do_check then assert(check_edge f0');
        if do_check then assert(check_edge f1');
        ((), f0', f1'))
    )
  )

(* [WIP]
let extract_single_from_ablist (i:int) (l:('a, 'b)row) : ('b * ('a, 'b)row, 'a *

let extract_single_CX_from_matCX i matCX

let extract_single_UCX_from_edge i edge =
  match (fst edge).decomp with
  | UCX(rowU, mat) -> (
    let rU_1, rU_23 = MyList.hdtl_nth i rowU in
    let rU_2, rU_3 = MyList.hdtl rU_23 in
    match rU_2 with
    | B() -> Some (A(), subst_matUCX_in_edge (rU_1@rU_3, mat) edge)
    | A() -> (
      match mat with
      | [] -> None
      | rowCX::mat' ->
      let i' = MyList.count isA rU_1 in
    )

    match MyList.nth i rowU with
  )
  | _ -> None
 *)
