(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-c10 : Ordered model with canalizing variables (C10 only).
 *
 * module  : Ldd_o_c10_types : Types module
 *)

open GuaCaml
open BTools

type uniq = int * int
type pair = int * int * int

type edge_state = uniq
type node_state = pair

type leaf = bool
type edge = edge_state
type node = unit

type 'i next' = ('i, leaf) Tree.gnext
type 'i edge' = edge * 'i next'
type 'i node' = node * 'i edge' * 'i edge'
