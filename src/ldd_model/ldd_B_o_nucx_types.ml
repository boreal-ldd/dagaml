(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2021 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-nucx : The Ordered model of Order 1, includes :
 *   Negation, Useless Variables, Xor variables, Canalizing variables.
 *
 * module  : Ldd_o_nucx_types : Types
 *)

open GuaCaml

type tag =
  | U
  | X
  | C of bool * bool (* C(if0, th0) *)

type tagi = tag * int
type tlist = tagi list

type uniq = int * bool * tlist
type pair = int * tlist * (bool * tlist)

type edge_state = uniq
type node_state = pair

type leaf = int
type edge = edge_state
type node = unit

type 'i next' = ('i, leaf) Tree.gnext
type 'i edge' = edge * 'i next'
type 'i node' = node * 'i edge' * 'i edge'

type 'a merge = edge_state * ('a next', 'a node') Utils.merge
type 'a edge'm = ('a, 'a node') Utils.merge edge'
