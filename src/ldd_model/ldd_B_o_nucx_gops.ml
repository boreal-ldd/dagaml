(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-nucx : The Ordered model of Order 1, includes :
 *   Negation, Useless Variables, Xor variables, Canalizing variables.
 *
 * module  : Ldd_o_nucx_gops : General OPeratorS
 *)

open GuaCaml
open STools
open BTools
open Extra
open Ldd_B_o_nucx_types

(* Section 0. Preliminaries *)

let arity (((n, _, _), _):'lk edge') : int= n
let arity_node ((), e0, e1) =
  let a0 = arity e0 and a1 = arity e1 in
  assert(a0 = a1);
  (succ a0)

(* Section 1. IO *)

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let tag = function
    | U -> "U"
    | X -> "X"
    | C(x, y) -> "C"^ToS.((bool * bool) (x, y))

  let tlist = list (tag * int)
  let uniq = trio int bool tlist
  let pair = trio int tlist (pair bool tlist)

  let leaf = int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let tag t s =
    match t with
    | U       -> false::false::s
    | X       -> false::true ::s
    | C(x, y) -> true ::x::y ::s

  let tlist = list(tag * int)
  let uniq = trio int bool tlist
  let pair = trio int tlist (pair bool tlist)

  let leaf = int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let tag = function
    | false::false::stream -> (U, stream)
    | false::true ::stream -> (X, stream)
    | true ::x::y ::stream -> (C(x, y), stream)
    | _ -> assert false

  let tlist = list(tag * int)
  let uniq = trio int bool tlist
  let pair = trio int tlist (pair bool tlist)

  let leaf = int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let tag cha = function
    | U       -> (bool cha false; bool cha false)
    | X       -> (bool cha false; bool cha true )
    | C(x, y) -> (bool cha true ; bool cha x    ; bool cha y)

  let tlist = list(tag * int)
  let uniq = trio int bool tlist
  let pair = trio int tlist (pair bool tlist)

  let leaf = int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let tag cha = match bool cha with
    | false -> (
      match bool cha with
      | false -> U
      | true  -> X
    )
    | true -> (
      let x = bool cha in
      let y = bool cha in
      C(x, y)
    )

  let tlist = list(tag * int)
  let uniq = trio int bool tlist
  let pair = trio int tlist (pair bool tlist)

  let leaf = int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToBa =
struct
  open BTools.ToB

  let tag = ToB.tag

  (* implemented by (length, liste) instead of unary encoding *)
  let rec tlist_rec (a:int) (tl:tlist) (s:stream) : stream =
    match tl with
    | [] -> s
    | (hd, k)::tl ->
      (tag hd (int (pred k) (tlist_rec (a-k) tl s)))

  let tlist (a:int) (tl:tlist) (s:stream) : stream =
    let n = List.length tl in
    int n (tlist_rec a tl s)

  let next' a (ident:'i tob) (nx:_ next') (s:stream) : stream =
    ToB.next' ident nx s

  (* side = function false -> 0 | true  -> 1 *)
  let edge' side a (ident:'i tob) (((n, b, tl), nx) : _ edge') (s:stream) : stream =
    assert(n = a);
    let d = MyList.map_sum snd tl in
    let s = tlist a tl (next' (a-d) ident nx s) in
    if side then (bool b s) else (assert(b = false); s)

  let node' (ident:'i tob) (node: _ node') (s:stream) : stream =
    let a = arity_node node in
    trio unit
      (edge' false (pred a) ident)
      (edge' true  (pred a) ident) node s
end

module OfBa =
struct
  open BTools.OfB

  let tag = OfB.tag

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    OfB.next' (ident a) s

  let rec tlist_rec a length (ident:int -> 'i ofb) (s:stream) : (tlist * _ next') * stream =
    if length = 0
    then (
      let nx, s = next' a ident s in
      (([], nx), s)
    )
    else (
      let tag, s = tag s in
      let k', s = int s in
      let k = succ k' in
      let (tl', nx), s = tlist_rec (a-k) (pred length) ident s in
      (((tag, k)::tl', nx), s)
    )

  let tlist a (ident:int -> 'i ofb) (s:stream) : (tlist * _ next') * stream =
    let n, s = int s in
    let (tl, nx), s = tlist_rec a n ident s in
    ((tl, nx), s)

  let edge' side a (ident:int -> 'i ofb) (s:stream) : _ edge' * stream =
    let b, s = if side then bool s else (false, s) in
    let (tl, nx), s = tlist a ident s in
    (((a, b, tl), nx), s)

  let node' a (ident:int -> 'i ofb) (s:stream) : _ node' * stream =
    trio unit
      (edge' false (pred a) ident)
      (edge' true  (pred a) ident) s
end

let char_of_tag : _ -> char = function
  | U -> 'U'
  | X -> 'X'
  | C(false, false) -> '1'
  | C(true , false) -> '0'
  | C(false, true ) -> 'I'
  | C(true , true ) -> 'O'

let pretty_of_tlist tl : string =
  let pretty_of_elem (t, n) =
    String.make n (char_of_tag t)
  in
  "["^(SUtils.catmap "" pretty_of_elem tl)^"]"

let pretty_of_edge e : string = STools.ToS.(trio int bool pretty_of_tlist) e

(* Section 2. Shannon *)

let is_cst ((a, b, tl), nx) =
  match tl, nx with
  | [], Tree.GLeaf _ -> Some b
  | _ -> None

let cst b n : 'lk edge' = ((n, b, []), Tree.GLeaf n)
let get_cst b f : 'lk edge' = cst b (arity f)

let is_id ((a, b, tl), nx) =
  match tl, nx with
  | [(X, 1)], Tree.GLeaf _ -> Some b
  | _ -> None

let make_id b n = ((n, b, [(X, 1)]), Tree.GLeaf(pred n))

let push0_tlist ?(n=1) c tl =
  if n = 0
  then tl
  else (
    match tl with
    | [] -> [(c, n)]
    | (c', n')::tl' ->
      if c = c'
      then ((c', n+n')::tl')
      else ((c , n   )::tl )
  )

let cneg_tag b = function
  | U -> U
  | X -> X
  | C(if0, th0) -> C(if0, b<>th0)

let push0_tag ?(n=1) c ((a, b, tl), nx) =
  let c' = cneg_tag b c in
  let tl' = push0_tlist ~n c' tl in
  ((n+a, b, tl'), nx)

let push_U ?(n=1) (f:'lk edge') : 'lk edge' =
  let a = arity f in
  match is_cst f with
  | Some b -> cst b (n+a)
  | None -> push0_tag ~n U f

let push_X ?(n=1) (f:'lk edge') : 'lk edge' =
  push0_tag ~n X f

let push_C ?(n=1) if0 th0 (f:'lk edge') : 'lk edge' =
  let a = arity f in
  match is_cst f with
  | Some th1 -> (
    if th0 = th1
    then cst th1 (n+a)
    else (
      let f1 = make_id (if0<>th0) (succ a) in
      let f2 = push0_tag ~n:(pred n) (C(if0, th0)) f1 in
      assert(arity f2 = n+a);
      f2
    )
  )
  | None -> (
    push0_tag ~n (C(if0, th0)) f
  )

let push_tag ?(n=1) t f =
  if n = 0
  then f
  else (
    match t with
    | U -> push_U ~n f
    | X -> push_X ~n f
    | C(if0, th0) -> push_C ~n if0 th0 f
  )

let push_ttag (t, n) f = push_tag ~n t f

let detect_oU (f:'lk edge') : int =
  let a = arity f in
  match is_cst f with
  | Some _ -> a
  | None -> (
    let (_, _, tl), _ = f in
    match tl with
    | (U, n)::_ -> n
    | _ -> 0
  )

let detect_oX (f:'lk edge') : int =
  match is_cst f with
  | Some _ -> 0
  | None -> (
    let (_, _, tl), _ = f in
    match tl with
    | (X, n)::_ -> n
    | _ -> 0
  )

let rec detect_oC (if0:bool) (th0:bool) (f:'lk edge') : int =
  let a = arity f in
  match is_cst f with
  | Some b -> if b = th0 then a else 0
  | None -> (
    match is_id f with
    | Some b -> if if0 <> b = th0 then 1 else 0
    | None -> (
      let (a, b, tl), nx = f in
      match tl with
      | (C(if1, th1), n)::tl'
        when if1 = if0 && (th1 <> b) = th0 ->
          (n + detect_oC if0 th0 ((a-n, b, tl'), nx))
      | _ -> 0
    )
  )

let detect_oP t f =
  match t with
  | U -> detect_oU f
  | X -> detect_oX f
  | C(if0, th0) -> detect_oC if0 th0 f

let elim_oU (k:int) ((((a, b, tl), nx) as f):'lk edge') : 'lk edge' =
  assert(0 <= k && k <= a);
  if k = 0 then f else (
    match is_cst f with
    | Some b -> cst b (a-k)
    | None ->
    match tl with
    | (U, n)::tl' -> (
      assert(k <= n);
      ((a-k, b, (if n = k then tl' else ((U, n-k)::tl'))), nx)
    )
    | _ -> assert false
  )

let elim_oX (k:int) ((((a, b, tl), nx) as f):'lk edge') : 'lk edge' =
  assert(0 <= k);
  if k = 0 then f else (
    match tl with
    | (X, n)::tl' -> (
      assert(k <= n);
      ((a-k, b, (if n = k then tl' else ((X, n-k)::tl'))), nx)
    )
    | _ -> assert false
  )

let rec elim_oC (if0:bool) (th0:bool) (k:int) ((((a, b, tl), nx) as f):'lk edge') : 'lk edge' =
  assert(0 <= k);
  if k = 0 then f else (
    match is_cst f with
    | Some b -> cst b (a-k)
    | None -> (
      match is_id f with
      | Some b -> (
        if if0 <> b = th0
        then (assert(k = 1); cst ((not if0) <> b) (a-1))
        else (assert false)
      )
      | None -> (
        let (a, b, tl), nx = f in
        match tl with
        | (C(if1, th1), n)::tl'
          when if1 = if0 && (th1 <> b) = th0 -> (
            if k <= n
            then ((a-k, b, (if n = k then tl' else ((C(if1, th1), n-k)::tl'))), nx)
            else elim_oC if0 th0 (k-n) ((a-n, b, tl'), nx)
        )
        | _ -> assert false
      )
    )
  )

(* [DEBUG]
let elim_oC (if0:bool) (th0:bool) (k:int) (f:'lk edge') : 'lk edge' =
  print_endline ("[DAGaml.Ldd_B_o_nucx_gops.elim_oC] if0:"^(STools.ToS.bool if0));
  print_endline ("[DAGaml.Ldd_B_o_nucx_gops.elim_oC] th0:"^(STools.ToS.bool th0));
  print_endline ("[DAGaml.Ldd_B_o_nucx_gops.elim_oC] k:"^(STools.ToS.int k));
  print_endline ("[DAGaml.Ldd_B_o_nucx_gops.elim_oC] f:"^(ToS.edge' STools.ToS.ignore f));
  let f' = elim_oC if0 th0 k f in
  print_endline ("[DAGaml.Ldd_B_o_nucx_gops.elim_oC] f':"^(ToS.edge' STools.ToS.ignore f'));
  f'
 *)

let elim_oP t k f =
  assert(0 <= k);
  if k = 0
  then f
  else (
    match t with
    | U -> elim_oU k f
    | X -> elim_oX k f
    | C(if0, th0) -> elim_oC if0 th0 k f
  )

let medge_of_edge (ee, nx) = (ee, Utils.MEdge nx)

let neg ((a, b, tl), nx) = ((a, not b, tl), nx)
let cneg (b0:bool) ((a, b, tl), nx) = ((a, b0<>b, tl), nx)
let un_neg ((a, b, tl), nx) = (b, ((a, false, tl), nx))

let solve_node (((), (((a0, b0, tl0), nx0) as f0), (((a1, b1, tl1), nx1) as f1)):'lk node') =
  assert(a0 = a1);
  if tl0 = tl1 && nx0 = nx1
  then if b0 = b1
    then (push_tag U f0 |> medge_of_edge)
    else (push_tag X f0 |> medge_of_edge)
  else
    match is_cst f0 with
    | Some th0 -> (push_tag (C(false, th0)) f1 |> medge_of_edge)
    | None ->
    match is_cst f1 with
    | Some th1 -> (push_tag (C(true , th1)) f0 |> medge_of_edge)
    | None -> (
      let b0, f0' = un_neg f0 in
      let f1' = cneg b0 f1 in
      ((succ a0, b0, []), Utils.MNode((), f0', f1'))
  )

let compose (aC, bC, tlC) (((ac, bc, tlc), i) as fc) =
  assert(MyList.map_sum snd tlc <= ac);
  assert(MyList.map_sum snd tlC + ac = aC);
  let fC = List.fold_right push_ttag tlC fc in
  assert(arity fC = aC);
  cneg bC fC

let normalize ((a, b, tl), nx) =
  let len_tl = MyList.map_sum snd tl in
  assert(len_tl <= a);
  let fnx = ((a - len_tl, false, []), nx) in
  let f' = compose (a, b, tl) fnx in
  assert(arity f' = a);
  f'

let pull_tlist ((a, b, tl), nx) =
  assert(a>0);
  match tl with
  | [] -> (
    match nx with
    | Tree.GLeaf n -> (
      (if a <> n then failwith "[ldd_o_nucx/pull_tlist] consitency error");
      Utils.MEdge (U, ((pred a, b, []), Tree.GLeaf(pred a)))
    )
    | Tree.GLink lk -> (Utils.MNode(b, lk))
  )
  | ((c', n')::tl') -> (
    assert(n' >= 1);
    let c'' = cneg_tag b c' in
    let tl'' =
      if n' = 1
      then tl'
      else (c', pred n')::tl'
    in
    Utils.MEdge(c'', ((pred a, b, tl''), nx))
  )

let node_pull (((a, b, tl), nx) as f) =
  assert(a>=1);
  match pull_tlist f with
  | Utils.MEdge(c, e) -> (
    match c with
    | U -> Utils.MEdge((), e, e)
    | X -> Utils.MEdge((), e, neg e)
    | C(if0, th0) -> (
      let th0' = get_cst th0 e in
      if if0
      then (Utils.MEdge((), e, th0'))
      else (Utils.MEdge((), th0', e))
    )
  )
  | Utils.MNode(b, lk) ->
    Utils.MNode(lk, fun ((), f0, f1) ->
      ((), cneg b f0, cneg b f1))

let sort_node ((), e0, e1) =
  if e0 < e1 then ((), e0, e1) else ((), e1, e0)

let f_hdtl ((a, b, tl), nx) =
  match tl with
  | [] -> None
  | (t, i)::tl' ->
    (Some((cneg_tag b t, i), ((a-i, b, tl'), nx)))

let edge'm_of_edge (e, nx) =
  match nx with
  | Tree.GLeaf lf -> (e, Tree.GLeaf lf)
  | Tree.GLink lk -> (e, Tree.GLink(Utils.MEdge lk))

let edge'm_of_merge (e, mnx) =
  match mnx with
  | Utils.MEdge nx -> (edge'm_of_edge (e, nx))
  | Utils.MNode nd -> (e, Tree.GLink(Utils.MNode nd))

let merge_of_edge'm (e, nx'm) =
  match nx'm with
  | Tree.GLeaf lf -> (e, Utils.MEdge(Tree.GLeaf lf))
  | Tree.GLink lk'm -> (match lk'm with
    | Utils.MEdge lk -> (e, Utils.MEdge(Tree.GLink lk))
    | Utils.MNode nd -> (e, Utils.MNode nd))

let rec solve_and_rec (f0:'lk edge') (f1:'lk edge') : 'lk edge'm =
  let a0 = arity f0 and a1 = arity f1 in
  assert(a0 = a1);
       if f0 = f1
    then (f0 |> edge'm_of_edge)
  else if f0 = neg f1
    then (get_cst false f0 |> edge'm_of_edge)
    else
  match is_cst f0 with
  | Some false -> (f0 |> edge'm_of_edge)
  | Some true  -> (f1 |> edge'm_of_edge)
  | None ->
  match is_cst f1 with
  | Some false -> (f1 |> edge'm_of_edge)
  | Some true  -> (f0 |> edge'm_of_edge)
  | None -> (
    let f01 () = ((a0, false, []), Tree.GLink(Utils.MNode(sort_node ((), f0, f1)))) in
    match f_hdtl f0, f_hdtl f1 with
    | Some((e0, n0), f0'), Some((e1, n1), f1') -> (
      let n01, f0'', f1'' =
             if n0  = n1
        then (n0, f0', f1')
        else if n0 <= n1
        then (n0, f0', push_ttag (e1, n1-n0) f1')
        else (n1, push_ttag (e0, n0-n1) f0', f1')
      in
      let f0''1'' ?(b0=false) ?(b1=false) () = solve_and_rec (cneg b0 f0'') (cneg b1 f1'') in
      match e0, e1 with
      | U, U -> compose (a0, false, [(U, n01)]) (f0''1''())
      | C(if0, th0), C(if1, th1) ->
        if if0 = if1
        then compose (a0, false, [(C(if0, th0 && th1), n01)]) (f0''1''())
        else ( (* if0 <> if1 *)
          match th0, th1 with
          | false, false -> (cst false a0)
          | false, true  -> compose (a0, false, [(C(if0, false), n01)]) (f0'' |> edge'm_of_edge)
          | true , false -> compose (a0, false, [(C(if1, false), n01)]) (f1'' |> edge'm_of_edge)
          | true , true  -> f01() (* [IMPROVE] return M3Node *)
        )
      | C(if0, false), _ -> (
        compose (a0, false, [(C(if0, false), n01)]) (match e1 with
          | C _ -> assert false (* already dealt with *)
          | U -> f0''1''()
          | X -> (
            let b1 = (not if0) && (Tools.mod2 n01 = 1) in
            f0''1'' ~b1 ()
          ))
        )
      | _, C(if1, false) -> (
        compose (a0, false, [(C(if1, false), n01)]) (match e0 with
          | C _ -> assert false (* already dealt with *)
          | U -> f0''1''()
          | X -> (
            let b0 = (not if1) && (Tools.mod2 n01 = 1) in
            f0''1'' ~b0 ()
          ))
        )
      | _, _ -> f01() (* [IMPROVE] *)
    )
    | _, _ -> f01()
  )

let solve_and ((), f0, f1) : 'lk merge = merge_of_edge'm (solve_and_rec f0 f1)

let rec solve_xor_rec f0 f1 =
  let a0 = arity f0 and a1 = arity f1 in
  assert(a0 = a1);
       if f0 = f1
    then (get_cst false f0 |> edge'm_of_edge)
  else if f0 = neg f1
    then (get_cst true  f0 |> edge'm_of_edge)
    else
  match is_cst f0 with
  | Some b0 -> (cneg b0 f1 |> edge'm_of_edge)
  | None ->
  match is_cst f1 with
  | Some b1 -> (cneg b1 f0 |> edge'm_of_edge)
  | None -> (
    let f01 () =
      let b0, f0' = un_neg f0 in
      let b1, f1' = un_neg f1 in
      ((a0, b0<>b1, []), Tree.GLink(Utils.MNode(sort_node ((), f0', f1'))))
    in
    match f_hdtl f0, f_hdtl f1 with
    | Some((e0, n0), f0'), Some((e1, n1), f1') -> (
      let n01, f0'', f1'' =
             if n0  = n1
        then (n0, f0', f1')
        else if n0 <= n1
        then (n0, f0', push_ttag (e1, n1-n0) f1')
        else (n1, push_ttag (e0, n0-n1) f0', f1')
      in
      let f0''1'' () = solve_xor_rec f0'' f1'' in
      match e0, e1 with
      | (U|X), (U|X) -> (
        let e01 = if e0 = e1 then U else X in
        compose (a0, false, [(e01, n01)]) (f0''1''())
      )
      | C(if0, th0), C(if1, th1) when if0 = if1 -> (
        let e01 = C(if0, th0 <> th1) in
        compose (a0, false, [(e01, n01)]) (f0''1''())
      )
      | _, _ -> f01() (* [IMPROVE] *)
    )
    | _, _ -> f01()
  )

let solve_xor ((), f0, f1) : 'lk merge = merge_of_edge'm (solve_xor_rec f0 f1)

(* [vanilla]
let solve_and (((), f0, f1):'lk node') =
  let a0 = arity f0 and a1 = arity f1 in
  assert(a0 = a1);
       if f0 = f1
    then (f0 |> medge_of_edge)
  else if f0 = neg f1
    then (get_cst false f0 |> medge_of_edge)
    else
  match is_cst f0 with
  | Some false -> (f0 |> medge_of_edge)
  | Some true  -> (f1 |> medge_of_edge)
  | None ->
  match is_cst f1 with
  | Some false -> (f1 |> medge_of_edge)
  | Some true  -> (f0 |> medge_of_edge)
  | None ->
    ((a0, false, []), Utils.MNode(sort_node ((), f0, f1)))

let solve_xor (((), f0, f1):'lk node') =
  let a0 = arity f0 and a1 = arity f1 in
  assert(a0 = a1);
       if f0 = f1
    then (get_cst false f0 |> medge_of_edge)
  else if f0 = neg f1
    then (get_cst true  f0 |> medge_of_edge)
    else
  match is_cst f0 with
  | Some b0 -> (cneg b0 f1 |> medge_of_edge)
  | None ->
  match is_cst f1 with
  | Some b1 -> (cneg b1 f0 |> medge_of_edge)
  | None -> (
    let b0, f0' = un_neg f0 in
    let b1, f1' = un_neg f1 in
    ((a0, b0<>b1, []), Utils.MNode(sort_node ((), f0', f1')))
  )
*)

let cons_and  node = match solve_and node with
  | (edge, Utils.MEdge next) -> edge, Utils.M3Edge next
  | (edge, Utils.MNode node) -> edge, Utils.M3Node node

let cons_xor  node = match solve_xor node with
  | (edge, Utils.MEdge next) -> edge, Utils.M3Edge next
  | (edge, Utils.MNode node) -> edge, Utils.M3Node node
