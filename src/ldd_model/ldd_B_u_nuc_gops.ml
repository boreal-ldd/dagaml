(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nuc : Uniform model with Negation, Useless variables and Canalizing Variables
 *
 * module  : Ldd_u_nuc_gops : General OPeratorS
 *)

open GuaCaml
open Utils
open Ldd_B_u_nuc_types
open Ldd_B_u_nuc_utils

let arity (blk, _) = blk.arity
let neg (blk, nx) = ({blk with neg = not blk.neg}, nx)
let cneg b (blk, nx) = ({blk with neg = b <> blk.neg}, nx)
let un_neg (blk, nx) =
  (blk.neg, ({blk with neg = false}, nx))
let is_cst (blk, nx) =
  match blk.block with
  | C0 -> Some blk.neg
  | _ -> None
let cst neg arity = ({neg; arity; block = C0}, Tree.GLeaf())
let var neg arity pos = ({neg; arity; block = Id pos}, Tree.GLeaf())
let get_cst b e = cst b (arity e)

let edge_nop neg arity =
  let tag = {hasS = arity > 0; hasU = false; maxC = None} in
  let block = (false, tag, MyList.make arity S) in
  {neg; arity; block = SUC block}

let compose = compose_edge
let compose_mask = compose_mask_edge
let peval_pedge = peval_pedge
let peval_pnodeC = peval_pnodeC

let sort_node = Utils.sort_node

let edge'm_of_edge : 'lk edge' -> 'lk edge'm =
  Utils.edge'm_of_edge
let edge'm_of_merge : 'lk emerge -> 'lk edge'm =
  Utils.edge'm_of_merge
let merge_of_edge'm : 'lk edge'm -> 'lk emerge =
  Utils.merge_of_edge'm

let ddl_cons = false
let ddl_and  = false
let ddl_xor  = false

let block_is_singleton block = match block.block with Id _ -> true | _ -> false

let solve_cons_id_suc_no_merge x shift liste =
  let rec aux carryC carryc i = function
    | [] -> (List.rev carryC, List.rev carryc)
    | head::tail -> match head with
      | U when i<>x  -> (aux (U::carryC)        carryc  (i+1) tail)
      | _            -> (aux (S::carryC) (head::carryc) (i+1) tail)
  in
  let listeC, listec = aux [] [] 0 liste in
  let listeC = S::listeC in
  let tagC = get_suc_tag_from_suc listeC
  and tagc = get_suc_tag_from_suc listec in
  let rec aux n i = function
    | [] -> assert false
    | head::tail -> if i = x then n else match head with
      | U -> aux  n    (i+1) tail
      | _ -> aux (n+1) (i+1) tail
  in
  (false, tagC, listeC), (List.length listec), (aux 0 0 liste), (shift, tagc, listec)

let meta_solve_cons' ((), ((block0, node0) as edge0), ((block1, node1) as edge1)) ifsuc2 =
  if edge0 = edge1
  then (merge_of_edge(push_U_edge edge0))
  else match block0.block, block1.block with
  | C0      , _        -> merge_of_edge(push_C0_edge false block0.neg edge1)
  | _       , C0       -> merge_of_edge(push_C0_edge true  block1.neg edge0)
  | Id x0   , Id x1    ->
  (
    if x0 = x1
    then
    (
      assert(block0.neg <> block1.neg);
      let liste = S::(MyList.init block0.arity (fun i -> if i = x0 then S else U)) in
      let tag = {hasS = true; hasU = block0.arity > 1; maxC = None} in
      let block_suc = (false, tag, liste) in
      (
        {neg = block0.neg; arity = block0.arity+1; block = SUC block_suc},
        MNode ((),
          ({neg = false; arity = 1; block = Id 0}, node0),
          ({neg = true ; arity = 1; block = Id 0}, node1)
        )
      )
    )
    else
    (
      let liste = S::(MyList.init block0.arity (fun i -> if i = x0 || i = x1 then S else U)) in
      let id0, id1 = if x0 < x1 then (0, 1) else (1, 0) in
      let tag = {hasS = true; hasU = block0.arity > 2; maxC = None} in
      let block_suc = (false, tag, liste) in
      (
        {neg = block0.neg; arity = block0.arity+1; block = SUC block_suc},
        MNode ((),
          ({neg = false; arity = 2; block = Id id0}, node0),
          ({neg = block0.neg <> block1.neg; arity = 2; block = Id id1}, node1)
        )
      )
    )
  )
  | Id x   , SUC (shift, _, liste) ->
  (
    match List.nth liste x with
    | C(b, 0) when (b <> block0.neg) = (shift <> block1.neg) ->
    (
      let liste' = (C(false, 1))::(List.mapi (fun y -> function C(b, i) when x<>y -> C(b, i+2) | e -> e) liste) in
      let block = reduce_block_suc block1.neg (block1.arity+1) (gnode_is_leaf node1) shift liste' in
      (block, MEdge node1)
    )
    | _ ->
    (
      let sucC, (arityc:int), (xc:int), succ = solve_cons_id_suc_no_merge x shift liste in
      (
        {neg = block0.neg; arity = block0.arity+1; block = SUC sucC},
        MNode ((),
          ({neg = false; arity = arityc; block = Id xc}, node0),
          ({neg = block0.neg <> block1.neg; arity = arityc; block = SUC succ}, node1)
        )
      )
    )
  )
  | SUC (shift, _, liste), Id x    ->
  (
    match List.nth liste x with
    | C(b, 0) when (shift <> block0.neg) = (b <> block1.neg) ->
    (
      let liste' = (C(true, 1))::(List.mapi (fun y -> function C(b, i) when x<>y -> C(b, i+2) | e -> e) liste) in
      let block = reduce_block_suc block0.neg (block0.arity+1) (gnode_is_leaf node0) shift liste' in
      (block, MEdge node0)
    )
    | _ ->
    (
      let sucC, arityc, xc, succ = solve_cons_id_suc_no_merge x shift liste in
      (
        {neg = block0.neg; arity = block0.arity+1; block = SUC sucC},
        MNode ((),
          ({neg = false; arity = arityc; block = SUC succ}, node0),
          ({neg = block0.neg <> block1.neg; arity = arityc; block = Id xc}, node1)
        )
      )
    )
  )
  | SUC suc0, SUC suc1 -> ifsuc2 block0.arity block0.neg block1.neg (suc0, node0) (suc1, node1)

let meta_solve_cons (((), edge0, edge1) : 'i node') ifsuc2 : 'i emerge =
  assert(check_edge edge0);
  assert(check_edge edge1);
  assert(arity_edge edge0 = arity_edge edge1);
  let blockC, merge = meta_solve_cons' ((), edge0, edge1) ifsuc2 in
  assert(arity_edge edge0 + 1 = arity_block blockC);
  (match merge with
  | MEdge next ->
  (
    (match next with
    | Tree.GLeaf () -> assert(snd edge0 = Tree.GLeaf () && snd edge1 = Tree.GLeaf ())
    | node -> match (snd edge0), (snd edge1) with
      | node'  , Tree.GLeaf ()
      | Tree.GLeaf (), node'   -> assert(node = node')
      | node0  , node1   -> assert(node = node0 && node = node1));
    assert(check_edge (blockC, next));
    ()
  )
  | MNode ((), edge0', edge1') ->
  (
    assert(snd edge0 = snd edge0');
    assert(snd edge1 = snd edge1');
    assert(arity_edge edge0' = arity_edge edge1');
    assert(((fst edge0).block <> C0) && ((fst edge1).block <> C0));
    assert(check_block blockC false);
    assert(check_edge edge0');
    assert(check_edge edge1');
    ()
  ));
  (blockC, merge)

let final_cons_ifsuc2 arity neg0 neg1 (suc0, i0) (suc1, i1) =
  (
    make_block_S neg0 (arity+1),
    MNode ((),
      ({neg = false     ; arity; block = SUC suc0}, i0),
      ({neg = neg0<>neg1; arity; block = SUC suc1}, i1)
    )
  )

let find_consensus_block_suc sucC sucY =
  List.fold_left2 (fun opmin x y -> match x, y with
      | C bi, C bi' -> if bi = bi'
        then opmin
        else (Tools.opmin (min (snd bi) (snd bi')) opmin)
      | C(_, i), _
      | _      , C(_, i) -> Tools.opmin i opmin
      | _                -> opmin
  ) None sucC sucY

let facto_cons_ifsuc2 arity neg0 neg1 ((s0, t0, l0), i0) ((s1, t1, l1), i1) =
  match t0.maxC, t1.maxC with
  | Some max0, Some max1 when (neg0 <> s0) = (neg1 <> s1) ->
  (
    let opmin = find_consensus_block_suc l0 l1 in
    let shift, cfun = match opmin with
      | None     -> false     , (function
        | U    , U     ->                   (U   , None      )
        | C bi , C bi' -> assert(bi = bi'); (C bi, None      )
        | x    , y     ->                   (S   , Some(x, y)))
      | Some min -> let remapC = shiftC (-min) in
                    (mod2 min), (function
        | U    , U     ->                   (U   , None      )
        | C bi , C bi' when bi = bi' && snd bi <= min ->
                                            (C bi, None      )
        | x    , y     ->                    (S   , Some (remapC x, remapC y)))
    in
    let listeC, (listec0, listec1) = Utils.consensus cfun l0 l1 in
    let blockC = reduce_block_suc neg0 (arity+1) false s0 (S::listeC)
    and edge0 = suc_liste_to_edge false        (s0<>shift) (listec0, i0)
    and edge1 = suc_liste_to_edge (neg0<>neg1) (s1<>shift) (listec1, i1) in
    let merge = meta_solve_cons ((), edge0, edge1) final_cons_ifsuc2 in
    compose_merge blockC merge
  )
  | _ ->
  (
    let cfun = function (U, U) -> U, None | (x, y) -> (S, Some(x, y)) in
    let listeC, (listec0, listec1) = Utils.consensus cfun l0 l1 in
    let blockC = reduce_block_suc neg0 (arity+1) false false (S::listeC)
    and edge0 = suc_liste_to_edge false        s0 (listec0, i0)
    and edge1 = suc_liste_to_edge (neg1<>neg0) s1 (listec1, i1) in
    (blockC, MNode((), edge0, edge1))
  )

let print_node head node = ()
(*  print_string head; print_string Ldd_B_u_nuc_o3.(fst o3str_node' (conv_node node)); print_newline() *)

let solve_cons (((), edge0, edge1) as node) =
  print_node "$C-" node;
  let edge0 = reduce_edge edge0
  and edge1 = reduce_edge edge1 in
  let node = ((), edge0, edge1) in
  assert(check_edge edge0);
  assert(check_edge edge1);
  assert(arity_edge edge0 = arity_edge edge1);
  meta_solve_cons node facto_cons_ifsuc2

let pull_C i neg arity shift liste next_is_leaf =
  let mod2i = mod2 i in
  let liste_then = Tools.map (function (C(_, j)) as e when j < i -> e | _ -> U) liste in
  let block_then = reduce_block_suc (neg <> shift <> mod2i) (arity-1) true mod2i liste_then in
  let block_else = if List.for_all (function C(_, j) -> i<>j | _ -> true) liste
  then if i = 0
    then
    (
      let liste = Tools.map (function C(b, j) -> C(b, j-1) | e -> e) liste in
      reduce_block_suc neg (arity-1) next_is_leaf (not shift) liste
    )
    else
    (
      let liste = Tools.map (function C(b, j) when j > i -> C(b, j-2) | e -> e) liste in
      reduce_block_suc neg (arity-1) next_is_leaf shift liste
    )
  else (reduce_block_suc neg (arity-1) next_is_leaf shift liste) in
  (block_then, block_else)

let node_pull ((block, node) as edge) =
  assert(block.arity > 0);
  assert(check_edge edge);
  match block.block with
  | C0 ->
  (
    assert(node = Tree.GLeaf());
    let edge' = ({neg = block.neg; arity = block.arity-1; block = C0}, Tree.GLeaf()) in
    MEdge ((), edge', edge')
  )
  | Id 0 ->
  (
    assert(node = Tree.GLeaf());
    let edge0 = ({neg = block.neg; arity = block.arity-1; block = C0}, Tree.GLeaf())
    and edge1 = ({neg = not block.neg; arity = block.arity-1; block = C0}, Tree.GLeaf()) in
    MEdge ((), edge0, edge1)
  )
  | Id x ->
  (
    assert(node = Tree.GLeaf());
    assert(x > 0);
    let edge' = ({neg = block.neg; arity = block.arity-1; block = Id(x-1)}, Tree.GLeaf()) in
    MEdge ((), edge', edge')
  )
  | SUC(shift, tag, liste) -> match liste with
    | [] -> assert false
    | elem::liste' ->
    (
      let block' = reduce_block_suc block.neg (block.arity-1) (gnode_is_leaf node) shift liste' in
      match elem with
      | U -> MEdge ((), (block', node), (block', node))
      | S -> MNode (gnode_node node, fun ((), edge0, edge1) ->
        ((), compose_edge block' edge0, compose_edge block' edge1)
      )
      | C(b, i) ->
        let block_then, block_else = pull_C i block.neg block.arity shift liste' (gnode_is_leaf node) in
        let edge_then = (block_then, Tree.GLeaf())
        and edge_else = (block_else, node) in
        let edge0, edge1 = Tools.cswap b (edge_then, edge_else) in
        MEdge ((), edge0, edge1)
    )

let solve_and_id_pedge arity neg0 x0 pedge1 =
  let peval = MyList.init arity (fun i -> if i = x0 then (Some(not neg0)) else None)
  and liste = MyList.init arity (fun i -> if i = x0 then (C(neg0, 0))     else S   ) in
  let tag = {hasU = false; hasS = arity>1; maxC = Some 0} in
  let blockC = {neg = false; arity; block = SUC (false, tag, liste)} in
  assert(check_block blockC false);
  assert(arity_edge pedge1 = arity);
  let pedge' = peval_pedge peval pedge1 in
  assert(arity_edge pedge' + 1 = arity);
  merge3_of_edge(compose_edge blockC pedge')

let meta_solve_and (((), ((block0, pnode0) as edge0), ((block1, pnode1) as edge1))) ifsuc2 =
(*  print_string "--@@meta_solve_and: begin"; print_newline();
  Ldd_B_u_nuc_dump.pnode pnode |> print_string; print_newline(); *)
  assert(check_edge edge0);
  assert(check_edge edge1);
  assert(block0.arity = block1.arity);
  match block0.block, block1.block with
  | C0      , _        ->
  (merge3_of_edge(if block0.neg
    then edge1
    else (make_edge_C0 false block1.arity)
  ))
  | _       , C0       ->
  (merge3_of_edge(if block1.neg
    then edge0
    else (make_edge_C0 false block0.arity)
  ))
  | Id x0   , Id x1    ->
  (merge3_of_edge(if x0 = x1
    then if block0.neg = block1.neg
      then edge0
      else (make_edge_C0 false block0.arity)
    else
    (
      let liste = MyList.init block0.arity (fun i ->
        if i = x0
          then C(block0.neg, 0)
        else if i = x1
          then C(block1.neg, 0)
          else U
      ) in
      let tag = {hasS = false; hasU = block0.arity>2; maxC = Some 0} in
      let suc = (true, tag, liste) in
      ({neg = true; arity = block0.arity; block = SUC suc}, Tree.GLeaf())
    )
  ))
  | Id x   , _         -> solve_and_id_pedge block0.arity block0.neg x edge1
  | _      , Id x      -> solve_and_id_pedge block0.arity block1.neg x edge0
  | SUC suc0, SUC suc1 -> if suc0 = suc1 && pnode0 = pnode1
    then (merge3_of_edge(if block0.neg = block1.neg
      then edge0
      else (make_edge_C0 false block0.arity)))
    else (ifsuc2 block0.arity block0.neg block1.neg (suc0, pnode0) (suc1, pnode1))

exception Return_False

let factoUU arity neg0 neg1 ((shift0, tag0, liste0), pnode0) ((shift1, tag1, liste1), pnode1) =
  let cfun = function (U, U) -> U, None | (x, y) -> (S, Some(x, y)) in
  let listeC, (liste0, liste1) = Utils.consensus cfun liste0 liste1 in
  let arity' = List.length liste0 in
  assert(arity' = List.length liste1);
  let blockC = reduce_block_suc false arity  false                    false  listeC
  and edge0 = suc_liste_to_edge neg0 shift0 (liste0, pnode0)
  and edge1 = suc_liste_to_edge neg1 shift1 (liste1, pnode1) in
  (blockC, ((), edge0, edge1))

let final_and_ifsuc2 arity neg0 neg1 suc_pnode0 suc_pnode1 =
  let edge, node = factoUU arity neg0 neg1 suc_pnode0 suc_pnode1 in
  (edge, M3Node node)

let rec facto_and_ifsuc2 arity neg0 neg1 ((((shift0, tag0, liste0) as suc0), pnode0) as pedge0) ((((shift1, tag1, liste1) as suc1), pnode1) as pedge1) =
  let factoUU () =
    let edge, node = factoUU arity neg0 neg1 pedge0 pedge1 in
    (edge, M3Node node)
  in
  let extractCO liste =
    let listeC_peval, oplistec = Tools.map (function
      | C(b, 0) -> (C(b, 0), (Some(not b))), None
      | C(b, i) -> (S      , None         ), Some(C(b, i-1))
      | e       -> (S      , None         ), Some e
    ) liste |> List.split in
    let listeC, peval = List.split listeC_peval in
    let listec = MyList.unop oplistec in
    (listeC, peval, listec)
  in
  let extractC0C0 listeX listeY =
    let cfun = function
      | U       , U        -> U, (None, None)
      | C(bX, 0), C(bY, 0) ->
      (if bX = bY
        then (C(bX, 0), (None, None))
        else (raise Return_False)
      )
      | C(bX, 0), e        -> C(bX, 0), (None, Some(Some(not bX), e))
      | e       , C(bY, 0) -> C(bY, 0), (Some(Some(not bY), e), None)
      | eX      , eY       -> S       , (Some(None, eX), Some(None, eY))
    in
    let listeC, pevalX_listeX, pevalY_listeY = Utils.consensus2 cfun listeX listeY in
    let pevalX, listeX = List.split pevalX_listeX
    and pevalY, listeY = List.split pevalY_listeY in
    (listeC, (pevalX, listeX), (pevalY, listeY))
  in
  let extractC1C1 listeX listeY =
    let cfun = function
      | U, U -> U, None
      | C(b, 0), C(b', 0) when b = b' -> C(b, 0), None
      | ex, ey -> S, (Some (ex, ey))
    in
    Utils.consensus cfun listeX listeY
  in
  let factoCU () =
    let listeC, peval, liste0 = extractCO liste0 in
    let blockC = reduce_block_suc false arity false false listeC in
    let pedge0 = suc_liste_to_edge neg0 (not shift0) (liste0, pnode0) in
    let pedge1 = peval_pedge peval ({neg = neg1; arity; block = SUC suc1}, pnode1) in
    let pnode = ((), pedge0, pedge1) in
    assert(arity_edge pedge0 = arity_edge pedge1);
    assert(count_nS_block blockC = arity_edge pedge0);
    if List.for_all isS listeC
    then (blockC, M3Node pnode)
    else (compose_merge3 blockC (meta_solve_and pnode facto_and_ifsuc2))
  in
  let factoUC () =
    let listeC, peval, liste1 = extractCO liste1 in
    let blockC = reduce_block_suc false arity false false listeC in
    let pedge1 = suc_liste_to_edge neg1 (not shift1) (liste1, pnode1) in
    let pedge0 = peval_pedge peval ({neg = neg0; arity; block = SUC suc0}, pnode0) in
    let pnode = ((), pedge0, pedge1) in
    assert(arity_edge pedge1 = arity_edge pedge0);
    assert(count_nS_block blockC = arity_edge pedge1);
    if List.for_all isS listeC
    then (blockC, M3Node pnode)
    else (compose_merge3 blockC (meta_solve_and pnode facto_and_ifsuc2))
  in
  match tag0.maxC, tag1.maxC with
  | None      , None       -> (factoUU())
  | Some maxC0, None       -> (if neg0 <> shift0 then (factoUU()) else (factoCU ()))
  | None      , Some maxC1 -> (if neg1 <> shift1 then (factoUU()) else (factoUC ()))
  | Some maxC0, Some maxC1 ->
  (match neg0<>shift0, neg1<>shift1 with
    | false, false ->
    (
      try
      (
        (* assert(List.length liste0 = arity); *)
        (* assert(List.length liste1 = arity); *)
        let listeC, (peval0, liste0), (peval1, liste1) = extractC0C0 liste0 liste1 in
        (* assert(List.length listeC = arity); *)
        let blockC = reduce_block_suc false arity false false listeC in
        let pedge0 = suc_liste_to_edge neg0 shift0 (liste0, pnode0) |> peval_pedge peval0
        and pedge1 = suc_liste_to_edge neg1 shift1 (liste1, pnode1) |> peval_pedge peval1 in
        assert(List.exists isntS listeC);
        compose_merge3 blockC (meta_solve_and ((), pedge0, pedge1) facto_and_ifsuc2)
      )
      with Return_False -> merge3_of_edge(make_edge_C0 false arity)
    )
    | true , false -> (factoUC ())
    | false, true  -> (factoCU ())
    | true , true  ->
    (
      let listeC, (liste0, liste1) = extractC1C1 liste0 liste1 in
      let blockC = reduce_block_suc false arity false true listeC in
      match liste0, liste1 with
      | (C(b, 0))::liste0, (C(b', 0))::liste1 ->
      (
        assert(b<>b');
        let pedge0 = suc_liste_to_edge neg0 shift0 (liste0, pnode0)
        and pedge1 = suc_liste_to_edge neg1 shift1 (liste1, pnode1) in
        let pedge0, pedge1 = Tools.cswap (not b) (pedge0, pedge1) in
        (blockC, M3Cons ((), pedge0, pedge1))
      )
      | _ ->
      (
        let pedge0 = suc_liste_to_edge neg0 shift0 (liste0, pnode0)
        and pedge1 = suc_liste_to_edge neg1 shift1 (liste1, pnode1) in
        if get_maxC_block blockC = None
        then (blockC, (M3Node ((), pedge0, pedge1)))
        else (compose_merge3 blockC (meta_solve_and ((), pedge0, pedge1) facto_and_ifsuc2))
      )
    )
  )

let solve_xor_id_suc_no_merge x shift liste =
  let rec aux carryC carryc i = function
    | [] -> (List.rev carryC, List.rev carryc)
    | head::tail -> match head with
      | U when i<>x  -> (aux (U::carryC)        carryc  (i+1) tail)
      | _            -> (aux (S::carryC) (head::carryc) (i+1) tail)
  in
  let listeC, listec = aux [] [] 0 liste in
  let tagC = get_suc_tag_from_suc listeC
  and tagc = get_suc_tag_from_suc listec in
  let rec aux n i = function
    | [] -> assert false
    | head::tail -> if i = x then n else match head with
      | U -> aux  n    (i+1) tail
      | _ -> aux (n+1) (i+1) tail
  in
  (false, tagC, listeC), (List.length listec), (aux 0 0 liste), (shift, tagc, listec)

let solve_xor_id_suc arity neg0 x0 neg1 (shift1, liste1, pnode1) =
  match List.nth liste1 x0 with
  | C(b1, 0) ->
  (
    let neg = neg0 <> neg1 <> b1 <> true in
    let func i = function
      | C(b, j) -> C(b, if i = x0 then (assert(j=0); 0) else j+1)
      | e -> e
    in
    let liste = List.mapi func liste1 in
    merge3_of_edge(suc_liste_to_edge neg (not shift1) (liste, pnode1))
  )
  | _ ->
  (
    let sucC, (arityc:int), (xc:int), succ = solve_xor_id_suc_no_merge x0 shift1 liste1 in
    if xc = 0
    then
    (
      let pedgec = ({neg = false; arity = arityc; block = SUC succ}, pnode1) in
      assert(arity>=1);
      let tail = MyList.ntimes None (arityc-1) in
      let pedge0 =           peval_pedge ((Some false)::tail) pedgec
      and pedge1 = neg_edge (peval_pedge ((Some true )::tail) pedgec) in
      let blockC = {neg = neg0<>neg1; arity; block = SUC sucC} in
      (blockC, M3Cons((), pedge0, pedge1))
    )
    else
    (
      {neg = neg0<>neg1; arity = arity; block = SUC sucC},
      M3Node ((),
        ({neg = false; arity = arityc; block = Id xc}, Tree.GLeaf()),
        ({neg = false; arity = arityc; block = SUC succ}, pnode1)
      )
    )
  )

let meta_solve_xor ((), ((block0, pnode0) as edge0), ((block1, pnode1) as edge1)) ifsuc2 =
  assert(check_edge edge0);
  assert(check_edge edge1);
  assert(block0.arity = block1.arity);
  match block0.block, block1.block with
  | C0      , _        -> (merge3_of_edge(cneg_edge block0.neg edge1))
  | _       , C0       -> (merge3_of_edge(cneg_edge block1.neg edge0))
  | Id x    , Id y     ->
  (
    if x = y
    then (merge3_of_edge(make_edge_C0 (block0.neg<>block1.neg) block0.arity))
    else
    (
      assert(pnode0 = Tree.GLeaf());
      assert(pnode1 = Tree.GLeaf());
      assert(block0.arity >= 2);
      let liste = MyList.init block0.arity (fun i -> if i = x || i = y then S else U) in
      let tag = {hasS = true; hasU = block0.arity>2; maxC = None} in
      let blockC = {neg = block0.neg<>block1.neg; arity = block0.arity; block = SUC(false, tag, liste)} in
      let edge0 = ({neg = false; arity = 1; block = Id 0}, Tree.GLeaf())
      and edge1 = ({neg = true ; arity = 1; block = Id 0}, Tree.GLeaf()) in
      (blockC, M3Cons((), edge0, edge1))
    )
  )
  | Id x   , SUC(shift, _, liste) -> solve_xor_id_suc block0.arity block0.neg x block1.neg (shift, liste, pnode1)
  | SUC(shift, _, liste), Id x    -> solve_xor_id_suc block0.arity block1.neg x block0.neg (shift, liste, pnode0)
  | SUC suc0, SUC suc1 -> if suc0 = suc1 && pnode0 = pnode1
    then (merge3_of_edge(make_edge_C0 (block0.neg<>block1.neg) block0.arity))
    else (ifsuc2 block0.arity block0.neg block1.neg (suc0, pnode0) (suc1, pnode1))

let final_xor_ifsuc2 arity neg0 neg1 suc_pnode0 suc_pnode1 =
  let blockC, ((), edge0, edge1) = factoUU arity false false suc_pnode0 suc_pnode1 in
  assert((fst edge0).neg = false);
  assert((fst edge1).neg = false);
  let blockC = cneg_block (neg0<>neg1) blockC in
  (blockC, M3Node((), edge0, edge1))

let rec facto_xor_ifsuc2 arity neg0 neg1 ((((shift0, tag0, liste0)), pnode0) as pedge0) ((((shift1, tag1, liste1)), pnode1) as pedge1) : _ plink emerge3 =
  match tag0.maxC, tag1.maxC with
  | Some maxC0, Some maxC1 ->
  (
    let opmin = List.fold_left (fun opmin -> function
      | C(b, i), C(b', i') -> if (b=b')&&(i=i')
        then opmin
        else (Tools.opmin (min i i') opmin)
      | C(_, i), _
      | _      , C(_, i) -> Tools.opmin i opmin
      | _                -> opmin) None (List.combine liste0 liste1)
    in
    let shift, cfun = match opmin with
      | None -> false, (function
        | (U, U) -> U, None
        | C((b, i) as bi), C((b', _) as bi')->
          (
            assert(bi = bi');
            C(b, 0), None;
          )
        | (x, y) -> S, Some(x, y)
      )
      | Some min ->
      (
        let remapC = function C(b, i) -> assert(i>=min); C(b, i-min) | e -> e in
        (mod2 min), (function
          | U, U -> U, None
          | C(b, i), C(b', i') when (b=b')&&(i=i')&&(i<=min) -> C(b, 0), None
          | (x, y) -> S, Some (remapC x, remapC y)
        )
      )
    in
    let listeC, (liste0, liste1) = Utils.consensus cfun liste0 liste1 in
    let blockC = reduce_block_suc (neg0<>neg1) arity false (shift0<>shift1) listeC in
    match liste0, liste1 with
    | (C(b, 0))::liste0, (C(b', 0))::liste1 ->
    (
      assert(b<>b');
      let pedge0 = suc_liste_to_edge (shift1<>shift) (shift0<>shift) (liste0, pnode0)
      and pedge1 = suc_liste_to_edge (shift0<>shift) (shift1<>shift) (liste1, pnode1) in
      let pedge0, pedge1 = Tools.cswap b (pedge1, pedge0) in
      (blockC, M3Cons ((), pedge0, pedge1))
    )
    | _ ->
    (
      let pedge0 = suc_liste_to_edge false (shift0<>shift) (liste0, pnode0)
      and pedge1 = suc_liste_to_edge false (shift1<>shift) (liste1, pnode1) in
      if get_maxC_block blockC = None
      then (blockC, M3Node ((), pedge0, pedge1))
      else (compose_merge3 blockC (meta_solve_xor ((), pedge0, pedge1) facto_xor_ifsuc2))
    )
  )
  | _ -> (final_xor_ifsuc2 arity neg0 neg1 pedge0 pedge1)

let meta_solve_binop (solver : 'i pnode -> 'i plink emerge3) ((((), pedge0, pedge1) as pnode) : 'i pnode) : 'i plink emerge3 =
  let reduce_pedge = Utils.preduce_pedge in
  assert(check_edge pedge0);
  assert(check_edge pedge1);
  assert(arity_edge pedge0 = arity_edge pedge1);
  let blockC, merge = solver pnode in match merge with
  | M3Edge next -> merge3_of_edge (blockC, next)
  | M3Cons (((), pedge0', pedge1') as pnode') ->
  (
(*    print_string "--@@meta_solve_binop:M3Cons:begin"; print_newline();
    Ldd_B_u_nuc_dump.block blockC |> print_string; print_newline();
    Ldd_B_u_nuc_dump.pnode pnode' |> print_string; print_newline(); *)
    assert(check_block blockC false);
    assert(arity_edge pedge0 = arity_block blockC);
    assert(check_edge pedge0');
    assert(check_edge pedge1');
    assert(arity_edge pedge0' = arity_edge pedge1');
    assert(arity_edge pedge0' + 1 = count_nS_block blockC);
    let edge, merge as emerge = compose_merge blockC (solve_cons pnode') in
(*    Ldd_B_u_nuc_dump.pemerge emerge |> print_string; print_newline();
    print_string "--@@meta_solve_binop:M3Cons:end"; print_newline(); *)
    (edge, match merge with MEdge next -> M3Edge next | MNode node -> M3Cons node)
  )
  | M3Node ((), pedge0', pedge1') ->
  (
    let pedge0' : 'i pedge = reduce_pedge pedge0'
    and pedge1' : 'i pedge = reduce_pedge pedge1' in
    assert(check_block blockC false);
    assert(arity_edge pedge0 = arity_block blockC);
    assert(check_edge pedge0');
    assert(check_edge pedge1');
    assert(arity_edge pedge0' = arity_edge pedge1');
    assert(arity_edge pedge0' = count_nS_block blockC);
    let pedge0', pedge1' = Tools.cswap (not(pedge0'<=pedge1')) (pedge0', pedge1') in
    (blockC, M3Node ((), pedge0', pedge1'))
  )

let solve_and (pnode : 'a pnode) =
  print_node "$A-" pnode;
  if ddl_and then (print_string "@@solve_and:"; print_newline());
  meta_solve_binop (fun pnode -> meta_solve_and pnode facto_and_ifsuc2) pnode

let solve_xor pnode =
  print_node "$C-" pnode;
  if ddl_xor then (print_string "@@solve_xor:"; print_newline());
  meta_solve_binop (fun pnode -> meta_solve_xor pnode facto_xor_ifsuc2) pnode

(* [DEBUG]

let rec solve_and_rec (f0:'lk edge') (f1:'lk edge') : 'lk edge'm =
  let a0 = arity f0 and a1 = arity f1 in
  assert(a0 = a1);
       if f0 = f1
    then (f0 |> edge'm_of_edge)
  else if f0 = neg f1
    then (get_cst false f0 |> edge'm_of_edge)
    else
  match is_cst f0 with
  | Some false -> (f0 |> edge'm_of_edge)
  | Some true  -> (f1 |> edge'm_of_edge)
  | None ->
  match is_cst f1 with
  | Some false -> (f1 |> edge'm_of_edge)
  | Some true  -> (f0 |> edge'm_of_edge)
  | None -> (
    (edge_nop false a0, Tree.GLink(Utils.MNode(sort_node ((), f0, f1))))
  )

let solve_and ((), f0, f1) : 'lk emerge =
  merge_of_edge'm (solve_and_rec f0 f1)

let rec solve_xor_rec f0 f1 : _ edge'm =
  let a0 = arity f0 and a1 = arity f1 in
  assert(a0 = a1);
       if f0 = f1
    then (get_cst false f0 |> edge'm_of_edge)
  else if f0 = neg f1
    then (get_cst true  f0 |> edge'm_of_edge)
    else
  match is_cst f0 with
  | Some b0 -> (cneg b0 f1 |> edge'm_of_edge)
  | None ->
  match is_cst f1 with
  | Some b1 -> (cneg b1 f0 |> edge'm_of_edge)
  | None -> (
    let b0, f0' = un_neg f0 in
    let b1, f1' = un_neg f1 in
    (edge_nop (b0<>b1) a0, Tree.GLink(Utils.MNode(sort_node ((), f0', f1'))))
  )

let solve_xor ((), f0, f1) : 'lk emerge =
  merge_of_edge'm (solve_xor_rec f0 f1)

let cons_and  node = match solve_and node with
  | (edge, Utils.MEdge next) -> edge, Utils.M3Edge next
  | (edge, Utils.MNode node) -> edge, Utils.M3Node node

let cons_xor  node = match solve_xor node with
  | (edge, Utils.MEdge next) -> edge, Utils.M3Edge next
  | (edge, Utils.MNode node) -> edge, Utils.M3Node node
 *)

let pnode_of_node = function
  | Tree.GLeaf () -> Tree.GLeaf ()
  | Tree.GLink node -> Tree.GLink (None, node)

let pedge_of_edge (block, node) = (block, pnode_of_node node)

let peval_merge3 (solve : 'i pnode -> 'i plink emerge3) (peval : peval) ((edge, merge) : 'i plink emerge3) : 'i plink emerge3 = match merge with
  | M3Edge pnext -> merge3_of_edge (peval_pedge peval (edge, pnext))
  | M3Cons (((), pedge0, pedge1) as pnode) ->
  (
    assert(arity_edge pedge0 = arity_edge pedge1);
    let (edge', pnext') = peval_pedge peval (edge, Tree.GLink (None, ())) in
    match pnext' with
      | Tree.GLeaf () -> (edge', (M3Edge(Tree.GLeaf())))
      | Tree.GLink (opeval, ()) -> match opeval with
        | None -> (edge', M3Cons pnode)
        | Some peval -> match peval_pnodeC peval pnode with
          | MEdge edge -> merge3_of_edge(compose_edge edge' edge)
          | MNode node ->
          (
            let edge', merge = compose_merge edge' (solve_cons node) in
            match merge with
            | MEdge next -> merge3_of_edge(edge', next)
            | MNode node -> (edge', M3Cons node)
          )
  )
  | M3Node (((), pedge0, pedge1) as pnode) ->
  (
    assert(arity_edge pedge0 = arity_edge pedge1);
    let edge', pnext' = peval_pedge peval (edge, Tree.GLink (None, ())) in
    match pnext' with
      | Tree.GLeaf () -> (edge', M3Edge(Tree.GLeaf()))
      | Tree.GLink (opeval, ()) ->
        compose_merge3 edge' (solve (opeval_pnode opeval pnode))
  )

let opquant_of_quant quant = if List.for_all (fun x -> x = false) quant
  then None
  else (Some quant)

let compose_quant qC qc = Utils.compose false qc qC

let compose_opquant opqC opqc = match opqC, opqc with
  | None, None       -> None
  | Some q, None
  | None, Some q     -> Some q
  | Some qC, Some qc -> Some(compose_quant qC qc)

let compose_qnext quant = function
  | Tree.GLeaf () -> assert(quant = []); Tree.GLeaf()
  | Tree.GLink (opq, node) -> Tree.GLink(compose_opquant(opquant_of_quant quant)opq, node)

let quant_block_suc (quant : bool list) (neg : bool) (arity : int) ((shift, tag, liste) : block_suc) qnext =
  let opmin = List.fold_left2 (fun opmin -> fun quant elem -> match quant, elem with
    | true, C(_, i) when ((mod2 i)<>shift<>neg) = false -> Tools.opmin i opmin
    | _ -> opmin) None quant liste in
  match opmin with
  | None ->
  (
    let liste' = MyList.opmap2 (function
      | true  -> (fun _ -> None  )
      | false -> (fun x -> Some x)) quant liste in
    match qnext with
    | Tree.GLeaf() -> suc_liste_to_edge neg shift (liste', Tree.GLeaf())
    | Tree.GLink (opq, node) ->
    (
      let quant' = MyList.opmap2 (function
        | S -> (fun x -> Some x)
        | _ -> (fun _ -> None  )) liste quant in
      let opq' = compose_opquant(opquant_of_quant quant')opq in
      suc_liste_to_edge neg shift (liste', Tree.GLink(opq', node))
    )
  )
  | Some minC ->
  (
    let liste' = MyList.opmap2 (function
      | true -> (fun _            -> None  )
      | false -> (function
        | S|U -> (Some U)
        | C(_, i) as x -> if i >= minC
          then   (Some U)
          else   (Some x))) quant liste in
    suc_liste_to_edge neg shift (liste', Tree.GLeaf())
  )

let quant_block_choice (quant : bool list) (neg : bool) (arity : int) (qnext : (bool list option * _, unit) Tree.gnext) =
  let coarity = MyList.count(fun x -> x = false) quant in
  function
  | C0 -> make_edge_C0 neg coarity
  | Id x -> (if List.nth quant x
    then (make_edge_C0 neg coarity)
    else (
      let x' = MyList.counti (fun i b -> (b = false)&&(i < x)) quant in
      ({neg; arity = coarity; block = Id x'}, Tree.GLeaf())
    )
  )
  | SUC suc -> quant_block_suc quant neg arity suc qnext

let quant_qedge quant (block, qnext) = quant_block_choice quant block.neg block.arity qnext block.block
let quant_qnode  quant ((), edge0, edge1) =
  ((), quant_qedge quant edge0, quant_qedge quant edge1)
let quant_node   quant node = quant_qnode quant (Utils.pnode_of_node node)

let solve_quant quant ((((), edge0, edge1) as qnode) : 'i qnode)  =
  assert(List.length quant = arity_edge edge0 + 1);
  assert(List.length quant = arity_edge edge1 + 1);
  assert(List.length quant > 0);
  if List.for_all (fun x -> x) quant
  then (Utils.M3Edge({neg = false; arity = 0; block = C0}, Tree.GLeaf()))
  (* true in GroBdd due to canonicity, not necessarly true in TACC *)
  else ( match quant with
    | [] -> assert false
    | head::quant' -> if head
    then (Utils.M3Node(quant_qnode quant' qnode))
    else (Utils.M3Cons(quant_qnode quant' qnode))
  )
  (* =( cannot use solve_and in current version as we cannot propagate evaluation
   * NECT: introduce an EQUANT modele =)
   * SOLUTION: cheating ... make solve_quant use evaluation (needs to performe a EQ inversion)
   *)

(*
let rewrite_expand_AC_node cond ((tag, edgeC, edgeY) as node) =
  let arity = arity_edge edgeC in
  assert(arity = arity_edge edgeY);
  let arity' = arity + TacxTypes.(match tag with Cons -> 1 | _ -> 0) in
  let blockC = make_block_S false arity' in
  if cond && (arity > 0) && (tag <> TacxTypes.Cons)
  then (
    let tail = MyList.ntimes None (arity-1) in
    let branch bool edgeC edgeY =
      let peval  = (Some bool)::tail in
      let edgeC' = peval_pedge peval edgeC
      and edgeY' = peval_pedge peval edgeY in
      let edge, merge = solve_tacx (tag, edgeC', edgeY') in
      (edge, (match merge with
      | Utils.MEdge next -> GTree.Leaf next
      | Utils.MNode node -> GTree.(Node(get_node_leaf node))))
    in
    let edge0 = branch false edgeC edgeY
    and edge1 = branch true  edgeC edgeY in
    (blockC, GTree.Node(TacxTypes.Cons, edge0, edge1))
  ) else (blockC, GTree.(Node(get_node_leaf node)))
 *)
