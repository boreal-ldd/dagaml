(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2015-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml    : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nu  : Uniform model with Negation, Useless variables
 *
 * module  : Ldd_u_nu_types : Types
 *)

open GuaCaml

(* S => Significant
   U => Useless
 *)
type uniq_elem = U | S

type uniq = uniq_elem list

type leaf = unit
type edge = bool * uniq
type node = unit

type 'i next' = ('i, leaf) Tree.gnext
type 'i edge' = edge * 'i next'
type 'i node' = node * 'i edge' * 'i edge'
