(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_forall : Quantifier Elimination
 *)

open GuaCaml
open AB
open Utils
open STools

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_io

(* type quant = bool list *)

let forall_rowA (red_pa:'c -> 'a -> 'c) (init_pa:'c) (p:quant) (r:(unit, 'a) row) : 'c * (unit, 'a) row * quant =
  let rec loop red_pa cC cr' cp' p r =
    match p, r with
    | [], [] -> (cC, List.rev cr', List.rev cp')
    | [], _ | _, [] -> assert false
    | p0::p, r0::r -> (
      match p0, r0 with
      | false , A() -> loop red_pa cC              ((A())::cr') (false::cp') p r
      | false , B b -> loop red_pa cC              ((B b)::cr')         cp'  p r
      | true  , A() -> loop red_pa cC                      cr'  (true ::cp') p r
      | true  , B b -> loop red_pa (red_pa cC b)         cr'          cp'  p r
    )
  in loop red_pa init_pa [] [] p r

let forall_rowU (p:quant) (r:rowU) : rowU * quant =
  let (), r, p = forall_rowA (fun () () -> ()) () p r in
  (r, p)

(* [forall_rowX p r = b] where :
 *  - [b : bool], [b = true] iff [p] quantifies a Xor-variable in which case,
 *      one can determine the value of [forall p. (r o \phi)]
 *)
let forall_rowX (p:quant) (r:rowU) : bool =
  let b, _, _ = forall_rowA (fun _ () -> true) false p r in
  b

(* [forall_rowC p r = (b, r, p)] where :
  - [b : bool], [b = true] iff [p] quantifies a canalizing variable
 *)
let forall_rowC (p:quant) (r:rowC) : bool * rowC * quant =
  forall_rowA (fun _ _ -> true) false p r

let forall_C0 (p:quant) (neg:bool) (a:int) : _ edge' =
  cst_edge neg (MyList.count_false p)

let forall_Id (p:quant) (neg:bool) (a:int) (k:int) : _ edge' =
  let noutput = MyList.count_false p in
  if List.nth p k
  then cst_edge false noutput
  else (
    let k' = MyList.count_false (fst(MyList.hdtl_nth k p)) in
    ({neg; noutput; ninput = None; decomp = Id k'}, Tree.GLeaf())
  )
