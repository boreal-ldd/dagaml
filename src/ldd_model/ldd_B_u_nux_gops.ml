(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2015-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nux : Uniform model with Negation, Useless variables
 *   and Xor variables
 *
 * module  : Ldd_u_nux_gops : General OPeratorS
 *)

open GuaCaml
open Ldd_B_u_nux_types
open Extra

let arity ((_, l), _) = List.length l

let edge_of_mask (mask:bool list) =
  (false, Tools.map (function true -> S | false -> P false) mask)

let cst b n =
  ((b, MyList.ntimes (P false) n), Tree.GLeaf ())

let is_cst ((b, l), nx) =
  match nx with
  | Tree.GLeaf () ->
    if List.for_all ((=)(P false)) l then Some b else None
  | Tree.GLink _  ->
    None

let get_cst b ((_, l), _) =
  cst b (List.length l)

let merge_uniq_cons (uniqX, uniqY) = Utils.consensus (function
  | P b0, P b1 when b0 = b1 -> (P b0, None)
  | x, y -> S, Some(x, y)) uniqX uniqY

let solve_cons ((), ((bX, lX), iX), ((bY, lY), iY)) =
  (* print_string "$C"; print_newline(); *)
  assert(List.length lX = List.length lY);
  if lX = lY && iX = iY
  then ((bX, (P (bX<>bY)::lX)), Utils.MEdge iX)
  else
  (
    let lXY, (lX', lY') = merge_uniq_cons (lX, lY) in
    ((bX, S::lXY), Utils.MNode ((), ((false, lX'), iX), ((bX<>bY, lY'), iY)))
  )

let merge_uniq_and (uniqX, uniqY) = Utils.consensus (function
  | P false, P false -> (P false, None)
  | x, y -> S, Some(x, y)) uniqX uniqY

let is_const ((b, l), i) =
  match i with
  | Tree.GLeaf () -> if List.for_all ((=)(P false)) l
    then Some b
    else None
  | Tree.GLink _ -> None

let solve_and ((), (((bx, lx), ix) as x), (((by, ly), iy) as y)) =
  (* print_string "$A"; print_newline(); *)
  assert(List.length lx = List.length ly);
  match is_const x with
  | Some bx ->
  (
    let e, i = if bx then (* x ~ 1 *) y else (* x ~ 0 *) x in
    (e, Utils.M3Edge i)
  )
  | None ->
  match is_const y with
  | Some by ->
  (
    let e, i = if by then (* y ~ 1 *) x else (* y ~ 0 *) y in
    (e, Utils.M3Edge i)
  )
  | None ->
  if (ix = iy) && (lx = ly)
  then
  (
    let e, i = if bx = by then (* x =  y *) x else (* x = ~y *) (get_cst false x) in
    (e, Utils.M3Edge i)
  )
  else
  (
    let lxy, (lx', ly') = merge_uniq_and (lx, ly) in
    let bx, ix, lx', by, iy, ly' = if ((ix, bx, lx') <= (iy, by, ly'))
      then (bx, ix, lx', by, iy, ly')
      else (by, iy, ly', bx, ix, lx')
    in
    ((false, lxy), Utils.M3Node ((), ((bx, lx'), ix), ((by, ly'), iy)))
  )

let neg ((b, l), i) = ((not b, l), i)
let cneg x ((b, l), i) = (( x <> b, l), i)

let merge_uniq_xor (uniqX, uniqY) = Utils.consensus (function
  | P b0, P b1 -> (P(b0<>b1), None)
  | x, y -> S, Some(x, y)) uniqX uniqY

let compose_uniq lx ly =
  let rec aux c = function
    | ([], []) -> List.rev c
    | ([], _ ) -> assert false
    | ((P b)::x, y) -> aux ((P b)::c) (x, y)
    | (S::x, []) -> assert false
    | (S::x, h::y) -> aux (h::c) (x, y)
  in aux [] (lx, ly)

let compose_edge (bx, lx) (by, ly) =
  (bx<>by, compose_uniq lx ly)

let compose ex (ey, i) =
  (compose_edge ex ey, i)

let solve_xor_trivial (((bx, lx), ix) as x) (((by, ly), iy)as y) =
  match is_const x with
  | Some bx -> Some (cneg bx y)
  | None ->
  match is_const y with
  | Some by -> Some (cneg by x)
  | None ->
  if (ix == iy) && (lx = ly)
  then (Some(get_cst (bx<>by) x))
  else None

let m3edge_of_edge (ee, nx) = (ee, Utils.M3Edge nx)

let solve_xor ((), (((bx, lx), ix)as x), (((by, ly), iy)as y)) =
  (* print_string "$X"; print_newline(); *)
  assert(List.length lx = List.length ly);
  match solve_xor_trivial x y with
  | Some result -> m3edge_of_edge result
  | None ->
  (
    let lxy, (lx', ly') = merge_uniq_xor (lx, ly) in
    let ix, lx', iy, ly' = if (( ix, lx') <= ( iy, ly'))
      then (ix, lx', iy, ly')
      else (iy, ly', ix, lx')
    in
    let delta = (bx<>by, lxy)
    and f1' = ((false, lx'), ix)
    and f2' = ((false, ly'), iy) in
    match solve_xor_trivial f1' f2' with
    | Some result -> m3edge_of_edge (compose delta result)
    | None        -> (delta, Utils.M3Node ((), f1', f2'))
  )

let node_pull (((bx, lx), i) : 'i edge') : ('i node', 'i * ('i node' -> 'i node')) Utils.merge = match lx with
  | [] -> assert false
  | h::lx' -> let e' = (bx, lx') in match h with
    | P b -> Utils.MEdge ((), (e', i), ((bx<>b, lx'), i))
    | S -> Utils.MNode (Utils.gnode_node i, fun ((), edge0, edge1) ->
      ((), (compose e' edge0), (compose e' edge1))
    )

(*
let assign peval ((neg, sub), next) =
  let sub, peval = Utils.consensus3 (function
    | None, x -> Some x, Some None
    | Some b, x -> (None, match x with P -> None | S -> Some (Some b))
  ) peval sub in
  let edge = (neg, sub) in
  let opevalC = if List.for_all ((=)None) peval
    then None
    else (Some peval)
  in
  match next with
  | Tree.GLeaf ()   -> assert(opevalC = None); (edge, Tree.GLeaf ())
  | Tree.GLink (opevalc, node) -> (edge, Tree.GLink (Utils.compose_opeval opevalC opevalc, node))
*)
