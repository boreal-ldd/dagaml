(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nuc : Uniform model with Negation, Useless variables and Canalizing Variables
 *
 * module  : Ldd_u_nuc_io : IO
 *)

open GuaCaml
open Extra
open STools
open BTools

open Ldd_B_u_nuc_types

let arity_edge' (blk, _) = blk.arity
let arity_node' (_, e0, e1) =
  let a = arity_edge' e0 in
  assert(a = arity_edge' e1);
  succ a

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let elem = function
    | S -> "S"
    | U -> "U"
    | C(b, i) -> ("C"^((bool * int)(b, i)))

  let suc_tag tag =
    "{hasS = "^(bool tag.hasS)^"; hasU = "^(bool tag.hasU)^"; maxC = "^(option int tag.maxC)^"}"

  let block_suc = trio bool suc_tag (list elem)

  let block_choice = function
    | C0 -> "C0"
    | Id n -> "Id "^(int n)
    | SUC blk -> "SUC"^(block_suc blk)

  let block block =
    "{neg = "^(bool block.neg)^"; arity = "^(int block.arity)^"; block = "^(block_choice block.block)^"}"

  let leaf = unit
  let edge = block
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)

  let merge ident x' = Utils.merge (next' ident) (node' ident) x'
  let emerge ident x' = Utils.emerge block (merge ident) x'

  let merge3 ident x' = Utils.merge3 (next' ident) (node' ident) x'
  let emerge3 ident x' = Utils.emerge3 block (merge3 ident) x'

  let peval = ToS.(list(option bool))
  let opeval = ToS.(option peval)

  let plink ident plink = pair Utils.stree_of_opeval ident plink
  let pnext ident next' = Utils.gnode unit (plink ident) next'
  let pedge ident edge' = pair block (pnext ident) edge'
  let pnode ident node' = trio unit (pedge ident) (pedge ident) node'

  let pmerge ident x' = Utils.merge (pnext ident) (pnode ident) x'
  let pemerge ident x' = Utils.emerge block (pmerge ident) x'

  let pmerge3 ident x' = Utils.merge3 (pnext ident) (pnode ident) x'
  let pemerge3 ident x' = Utils.emerge3 block (pmerge3 ident) x'
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let wrap text tos aa a s =
    print_endline (text^" [0] a:"^(tos a));
    print_string (text^" [0] s:"); SUtils.print_stream s; print_newline();
    let s = aa a s in
    print_string (text^" [1] s:"); SUtils.print_stream s; print_newline();
    s

  let elem e stream =
    match e with
    | S -> true::stream
    | U -> false::true::stream
    | C(b, i) -> false::false::((bool * int) (b, i) stream)

  let suc_tag tag stream =
    trio bool bool (option int) (tag.hasS, tag.hasU, tag.maxC) stream

  let block_suc blk stream = trio bool suc_tag (list elem) blk stream

  let block_choice blk stream =
    match blk with
    | C0 -> false::false::stream
    | Id n -> false::true ::(int n stream)
    | SUC blk -> true::(block_suc blk stream)

  let block block =
    trio bool int block_choice (block.neg, block.arity, block.block)

  let leaf = unit
  let edge = block
  let node = unit

(* [DEBUG]
  let leaf x = wrap "[ToB.leaf]" ToS.leaf leaf x
  let edge x = wrap "[ToB.edge]" ToS.edge edge x
  let node x = wrap "[ToB.node]" ToS.node node x
 *)

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let wrap text tos aa s =
    print_string (text^" [1] s:"); SUtils.print_stream s; print_newline();
    let a, s = aa s in
    print_string (text^" [0] s:"); SUtils.print_stream s; print_newline();
    print_endline (text^" [0] a:"^(tos a));
    (a, s)

  let elem = function
    | true :: stream -> (S, stream)
    | false::true ::stream -> (U, stream)
    | false::false::stream -> (
      let (b, i), stream = (bool * int) stream in
      (C(b, i), stream)
    )
    | _ -> assert false

  let suc_tag stream =
    let (hasS, hasU, maxC), stream = trio bool bool (option int) stream in
    ({hasS; hasU; maxC}, stream)

  let block_suc stream =
    trio bool suc_tag (list elem) stream

  let block_choice = function
    | false::false::stream -> (C0, stream)
    | false::true ::stream -> (
      let n, stream = int stream in
      (Id n, stream)
    )
    | true ::stream -> (
      let blk, stream = block_suc stream in
      (SUC blk, stream)
    )
    | _ -> assert false

  let block stream =
    let (neg, arity, block), stream = trio bool int block_choice stream in
    ({neg; arity; block}, stream)

  let leaf = unit
  let edge = block
  let node = unit

(* [DEBUG]
  let leaf x = wrap "[OfB.leaf]" ToS.leaf leaf x
  let edge x = wrap "[OfB.edge]" ToS.edge edge x
  let node x = wrap "[OfB.node]" ToS.node node x
 *)

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let elem cha e =
    match e with
    | S -> bool cha true
    | U -> bool cha false; bool cha true
    | C(b, i) -> bool cha false; bool cha false; (bool * int) cha (b, i)

  let suc_tag cha tag =
    trio bool bool (option int) cha (tag.hasS, tag.hasU, tag.maxC)

  let block_suc cha blk = trio bool suc_tag (list elem) cha blk

  let block_choice cha blk =
    match blk with
    | C0 -> bool cha false; bool cha false
    | Id n -> bool cha false; bool cha true; int cha n
    | SUC blk -> bool cha true; block_suc cha blk

  let block cha block =
    trio bool int block_choice cha (block.neg, block.arity, block.block)

  let leaf = unit
  let edge = block
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let elem cha =
    match bool cha with
    | true -> S
    | false -> match bool cha with
      | true  -> U
      | false -> C((bool * int) cha)

  let suc_tag cha =
    let hasS, hasU, maxC = trio bool bool (option int) cha in
    {hasS; hasU; maxC}

  let block_suc cha = trio bool suc_tag (list elem) cha

  let block_choice cha =
    match bool cha with
    | false -> (
      match bool cha with
      | false -> C0
      | true  -> Id(int cha)
    )
    | true  -> SUC(block_suc cha)

  let block cha =
    let neg, arity, block = trio bool int block_choice cha in
    {neg; arity; block}

  let leaf = unit
  let edge = block
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

(* [TODO] make edge' polarized *)
module ToBa =
struct
  open BTools.ToB

  let next' a (ident:'i tob) nx (s:stream) : stream =
    match nx with
    | Tree.GLeaf () -> assert(a = 0); s
    | Tree.GLink lk -> assert(a > 0); ident lk s

  let block_suc a (ident:'i tob) ((_, _, l) as blk) nx (s:stream) : stream =
    let a' = MyList.count ((=)S) l in
    let s = next' a' ident nx s in
    let s = trio bool ToB.suc_tag (sized_list ToB.elem) blk s in
    s

  let edge' a (ident:'i tob) (blk, nx) (s:stream) : stream =
    let s = match blk.block with
      | C0 -> false::false::s
      | Id n -> false::true ::(int n s)
      | SUC blk -> true ::(block_suc a ident blk nx s)
    in
    let s = bool blk.neg s in
    s

  let node' (ident:'i tob) (node:_ node') (s:stream) : stream =
    let a = arity_node' node in
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) node s
end

(* [TODO] make edge' polarized *)
module OfBa =
struct
  open BTools.OfB

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    if a = 0
    then (Tree.GLeaf(), s)
    else (let lk, s = ident a s in (Tree.GLink lk, s))

  let block_suc a (ident:int -> 'i ofb) (s:stream) : (block_suc * _ next') * stream =
    let ((_, _, l) as blk), s = trio bool OfB.suc_tag (sized_list OfB.elem a) s in
    let a' = MyList.count((=)S) l in
    let nx, s = next' a' ident s in
    ((blk, nx), s)

  let edge' arity (ident:int -> 'i ofb) (s:stream) : _ edge' * stream =
    let neg, s = bool s in
    let (block, nx), s =
      match s with
      | false::false::s -> ((C0, Tree.GLeaf()), s)
      | false::true ::s -> (
        let n, s = int s in
        ((Id n, Tree.GLeaf()), s)
      )
      | true ::s -> (
        let (blk, nx), s = block_suc arity ident s in
        ((SUC blk, nx), s)
      )
      | _ -> assert false
    in
    (({neg; arity; block}, nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

module PrettyToS =
struct
  let elem_C_char if0 th0 =
    match if0, th0 with
    | false, false -> '1'
    | true , false -> '0'
    | false, true  -> 'I'
    | true , true  -> 'O'

  let block_suc_neo neg arity (shift, tag, liste) =
    if tag.maxC = None && (not(tag.hasS && tag.hasU))
    then ("("^(Utils.mp_of_bool neg)^" "^(string_of_int arity)^" "^(if tag.hasS then "S" else "U")^")")
    else (
      assert(List.length liste > 0);
      let map_elem = function
        | S -> ('S', 0)
        | U -> ('U', 0)
        | C(if0, i) -> (
          let th0 = shift <> (Tools.mod2 i = 1) in
          (elem_C_char if0 th0, i)
        )
      in
      let sarray = MyArray.map_of_list map_elem liste in
      let len = match tag.maxC with None -> 1 | Some n -> n+1 in
      let slist = STools.SUtils.char_height_array ~above:' ' ~under:'.' len sarray in
      let slist = match slist with
        | [] -> assert false
        | h::t -> ((Utils.mp_of_bool neg)^h)::(t ||> (fun s -> " "^s))
      in
      String.concat "\n" slist
    )

  let block block = match block.block with
    | C0 -> ("("^(Utils.mp_of_bool block.neg)^" "^(string_of_int block.arity)^" C0 )")
    | Id x -> ("("^(Utils.mp_of_bool block.neg)^" "^(string_of_int block.arity)^" Id "^(string_of_int x)^")")
    | SUC blk -> block_suc_neo block.neg block.arity blk

  let edge = block
end

(* [OLD]
let log2 = Tools.math_log_up 2

let is_nil (*next_is_leaf*) block = match block.block with
  | C0 | Id _ -> true
  | SPX _ -> false

let is_nil2 (blockX, blockY) = (is_nil blockX, is_nil blockY)

let bindump_elem_spx size elem stream = match elem with
  | S -> true::stream
  | P -> false::true::stream
  | X(b, i) -> false::false::b::(ToB.sized_int size i stream)

let binload_elem_spx size = function
  | true ::stream -> S, stream
  | false::true::stream -> P, stream
  | false::false::b::stream ->
    let i, stream = OfB.sized_int size stream in
    X(b, i), stream
  | _ -> assert false

let bindump_elem_sx size elem stream = match elem with
  | S -> true::stream
  | P -> assert false
  | X(b, i) -> false::b::(ToB.sized_int size i stream)

let binload_elem_sx size = function
  | true::stream -> S, stream
  | false::b::stream ->
    let i, stream = OfB.sized_int size stream in
    X(b, i), stream
  | _ -> assert false

let bindump_elem_sp elem stream = match elem with
  | S -> true ::stream
  | P -> false::stream
  | X _ -> assert false

let binload_elem_sp = function
  | true ::stream -> S, stream
  | false::stream -> P, stream
  | _ -> assert false

let bindump_elem_px size elem stream = match elem with
  | S -> assert false
  | P -> true ::stream
  | X(b, i) -> false::b::(ToB.sized_int size i stream)

let binload_elem_px size = function
  | true ::stream -> P, stream
  | false::b::stream ->
    let i, stream = OfB.sized_int size stream in
    X(b, i), stream
  | _ -> assert false

let bindump_elem_x size elem stream = match elem with
  | S | P -> assert false
  | X(b, i) -> b::(ToB.sized_int size i stream)

let binload_elem_x size = function
  | b::stream ->
    let i, stream = OfB.sized_int size stream in
    X(b, i), stream
  | _ -> assert false

let bindump_block block next_is_leaf stream =
  assert(check_block block next_is_leaf);
  block.neg::(ToB.int block.arity (match block.arity with
    | 0 ->
    (
      (if next_is_leaf
      then (assert(block.block = C0))
      else (assert(block.block = SPX(false, {hasS = false; hasP = false; maxX = None}, []))));
      stream
    )
    | n ->
    (
      let logn = log2 (n+1) in
      match block.block with
      | C0 -> false::stream
      | Id x -> true::false::(ToB.sized_int logn x stream)
      | SPX(shift, tag, liste) ->
      let store_hasS stream = if next_is_leaf
        then (assert(tag.hasS = false); stream)
        else (tag.hasS::stream)
      in
      let store_maxX stream =
        let logm = log2 (n+2) in
        match tag.maxX with
        | None   -> ToB.sized_int logm    0  stream
        | Some x -> ToB.sized_int logm (x+1) stream
      in
      let dumpliste dump : bool list =
        ToB.sized_list dump liste stream
      in
      true::true::(store_hasS(tag.hasP::(store_maxX(match tag.maxX with
        | None ->
        (
          assert(shift = false);
          if tag.hasS && tag.hasP
            then (dumpliste bindump_elem_sp )
            else stream
        )
        | Some maxX ->
        let dumpliste' dump = dumpliste (dump (log2 (maxX+1))) in
        shift::(if tag.hasS
          then if tag.hasP
            then (dumpliste' bindump_elem_spx)
            else (dumpliste' bindump_elem_sx )
          else if tag.hasP
            then (dumpliste' bindump_elem_px )
            else (dumpliste' bindump_elem_x  )
        )
    ))))
  )))

let bindump_edge block stream =
  let nil = is_nil block in
  nil::(bindump_block block nil stream)

let binload_block next_is_leaf stream : block * (bool list)=
  let neg, stream = OfB.bool stream in
  let arity, stream = OfB.int stream in
  let block, stream = match arity with
  | 0 ->
  ((if next_is_leaf
    then C0
    else SPX(false, {hasS = false; hasP = false; maxX = None}, [])
  ), stream)
  | n ->
  (
    let logn = log2 (n+1) in
    match stream with
    | false::stream -> (C0, stream)
    | true ::false::stream ->
    (
      let x, stream = OfB.sized_int logn stream in
      (Id x, stream)
    )
    | true ::true ::stream ->
    (
      let load_hasS stream = if next_is_leaf
        then (false, stream)
        else (OfB.bool stream)
      in
      let load_maxX stream =
        let logm = log2 (n+2) in
        let maxx, stream = OfB.sized_int logm stream in
        ((if maxx = 0 then None else (Some (maxx-1))), stream)
      in
      let hasS, stream = load_hasS stream in
      let hasP, stream = OfB.bool stream in
      let maxX, stream = load_maxX stream in
      let tag = {hasS; hasP; maxX} in
      let loadliste load stream =
        OfB.sized_list load arity stream
      in
      let shift, (liste, stream) = match maxX with
        | None ->
        (false, (if hasS
          then if hasP
            then (loadliste binload_elem_sp stream)
            else (MyList.ntimes S arity, stream)
          else if hasP
            then (MyList.ntimes P arity, stream)
            else (assert(arity = 0); ([], stream))
        ))
        | Some maxX ->
        (
          let shift, stream = OfB.bool stream in
          let loadliste' (load : int -> bool list -> _ * bool list) : (_ list) * (bool list) =
            loadliste (load (log2 (maxX+1))) stream
          in
          (shift, loadliste' (if hasS
          then if hasP
            then binload_elem_spx
            else binload_elem_sx
          else if hasP
            then binload_elem_px
            else binload_elem_x
          ))
        )
      in
      (SPX(shift, tag, liste), stream)
    )
    | _ -> assert false
  ) in
  {neg; arity; block}, stream

let binload_edge       stream =
  let nil, stream = OfB.bool stream in
  binload_block nil stream

let o3s_edge = (bindump_edge, binload_edge)

let o3s_node = O3.trio (IoB.unit, o3s_edge, o3s_edge)

let conv_next = Utils.(function
  | Tree.GLeaf leaf -> Tree.GLeaf leaf
  | Tree.GLink node -> Tree.GLink 0
)

let conv_next2 = Utils.(function
  | Tree.GLink node0, Tree.GLink node1 ->
  (
    if node0 = node1
      then (Tree.GLink 0, Tree.GLink 0)
    else if node0 < node1
      then (Tree.GLink 0, Tree.GLink 1)
      else (Tree.GLink 1, Tree.GLink 0)
  )
  | next0, next1 -> (conv_next next0, conv_next next1)
)

let conv_node (node, (edge0, next0), (edge1, next1)) =
  let next0, next1 = conv_next2 (next0, next1) in
  (node, (edge0, next0), (edge1, next1))

let o3s_next' = Utils.o3s_gnode IoB.unit IoB.int
let o3s_node' = O3Utils.from_a_bc_de_to_abd_c_e +>> O3.(trio (o3s_node, o3s_next', o3s_next'))
let o3b_node' = IoB.closure o3s_node'
let o3str_node' = o3b_node' %>> BArray.o3_stree

let o3s_tacx  = O3.trio (TacxTypes.o3s_tag, o3s_edge, o3s_edge)
let o3b_tacx  = IoB.closure o3s_tacx
let o3str_tacx  = o3b_tacx  %>> BArray.o3_stree

let o3s_tacx' = O3Utils.from_a_bc_de_to_abd_c_e +>> O3.(trio (o3s_tacx, o3s_next', o3s_next'))

let bindump_block2 (blockX, blockY) (next_is_leafX, next_is_leafY) stream =
  bindump_block blockX next_is_leafX (bindump_block blockY next_is_leafY stream)

let binload_block2 (next_is_leafX, next_is_leafY) stream =
  let blockX, stream = binload_block next_is_leafX stream in
  let blockY, stream = binload_block next_is_leafY stream in
  ((blockX, blockY), stream)
 *)
