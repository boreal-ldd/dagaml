(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2015-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nux : Uniform model with Negation, Useless variables
 *   and Xor variables
 *
 * module  : Ldd_u_nux_io : IO
 *)

open GuaCaml
open STools
open BTools

open Ldd_B_u_nux_types

let arity_edge' ((_, l), _) = List.length l
let arity_node' (_, e0, e1) =
  let a = arity_edge' e0 in
  assert(a = arity_edge' e1);
  succ a

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let uniq_elem = function
    | P false -> "U"
    | P true  -> "X"
    | S       -> "S"

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let uniq_elem e s =
    match e with
    | P b -> false::b::s
    | S -> true ::s

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let uniq_elem : stream -> uniq_elem * stream =
    function
    | false::b::t -> (P b, t)
    | true::t -> (S, t)
    | _ -> assert false

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let uniq_elem cha e =
    match e with
    | P b -> bool cha false; bool cha b
    | S   -> bool cha true

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let uniq_elem cha =
    match bool cha with
    | false -> P(bool cha)
    | true  -> S

  let uniq = list uniq_elem

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

(* [TODO] make edge' polarized *)
module ToBa =
struct
  open BTools.ToB

  let next' a (ident:'i tob) nx (s:stream) : stream =
    match nx with
    | Tree.GLeaf () -> assert(a = 0); s
    | Tree.GLink lk -> assert(a > 0); ident lk s

  let edge' a (ident:'i tob) ((b, l), nx) (s:stream) : stream =
    let s = next' (MyList.count ((=)S) l) ident nx s in
    let s = sized_list ToB.uniq_elem l s in
    let s = bool b s in
    s

  let node' (ident:'i tob) (node:_ node') (s:stream) : stream =
    let a = arity_node' node in
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) node s
end

(* [TODO] make edge' polarized *)
module OfBa =
struct
  open BTools.OfB

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    if a = 0
    then (Tree.GLeaf(), s)
    else (let lk, s = ident a s in (Tree.GLink lk, s))

  let edge' a (ident:int -> 'i ofb) (s:stream) : _ edge' * stream =
    let b, s = bool s in
    let l, s = sized_list OfB.uniq_elem a s in
    let nx, s = next' (MyList.count ((=)S) l) ident s in
    (((b, l), nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

module PrettyToS =
struct
  let edge (neg, sub) =
    String.concat "" ((if neg then "-" else "+")::(Tools.map ToS.uniq_elem sub))
end
