(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_gops : Descriptor Normalization
 *)

open GuaCaml
open AB
open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_io

let compose_rowU_matUCX rowU_C (rowU_c, rowCX_l) =
  (compose_same rowU_C rowU_c, rowCX_l)

let swap_rowCX_rowU (rCX:rowCX) (rU:rowU) : rowU * rowCX =
  (* assert(rowCX_countS rCX = List.length rU); *)
  match rCX with
  | A rX -> (
    let rU, rX = swap_ablA_ablB rX rU in
    (rU, A rX)
  )
  | B (th0, rC) -> (
    let rU, rC = swap_ablA_ablB rC rU in
    (rU, B(th0, rC))
  )

let swap_rowU_rowCX (rU:rowU) (rCX:rowCX) : rowCX * rowU =
  (* assert(listAB_countA rU = rowCX_length rCX); *)
  match rCX with
  | A rX -> (
    let rX, rU = swap_ablA_ablB rU rX in
    (A rX, rU)
  )
  | B (th0, rC) -> (
    let rC, rU = swap_ablA_ablB rU rC in
    (B(th0, rC), rU)
  )

let compose_rowCX_matCX rowCX matCX =
  if rowCX_allS rowCX
  then matCX
  else match matCX with
  | [] -> [rowCX]
  | rowCX'::matCX' -> match rowCX, rowCX' with
    | A rX        , A rX'         -> A(       compose_same rX rX') :: matCX'
    | B(false, rC), B(false, rC') -> B(false, compose_same rC rC') :: matCX'
    | B(true , rC), B(true , rC') -> B(true , compose_same rC rC') :: matCX'
    | _, _ -> rowCX :: matCX

let compose_rowCX_matUCX (rowCX:rowCX) ((rowU, matCX):matUCX) : matUCX =
  (* print_endline ("[compose_rowCX_matUCX] rowCX: "^(ToS.rowCX rowCX)); *)
  (* print_endline ("[compose_rowCX_matUCX] matUCX: "^(ToS.matUCX matUCX)); *)
  if rowCX_allS rowCX
  then (rowU, matCX)
  else
    let rowU, rowCX = swap_rowCX_rowU rowCX rowU in
    (rowU, compose_rowCX_matCX rowCX matCX)

let compose_matUCX_matUCX (rowU_C, matCX_C) mat_c : matUCX =
  let (rowU_c, mat) = List.fold_right compose_rowCX_matUCX matCX_C mat_c in
  (compose_same rowU_C rowU_c, mat)

let compose_rowU_bmatUCX rU_C (b, (rU_c, mUCX)) =
  (b, (compose_same rU_C rU_c, mUCX))

let compose_rowCX_bmatUCX rCX (b, mUCX) : bmatUCX =
  (b, compose_rowCX_matUCX (xor_rowCX b rCX) mUCX)

let compose_matUCX_bmatUCX mUCX_C (b_c, mUCX_c) : bmatUCX =
  (b_c, (compose_matUCX_matUCX (xor_matUCX b_c mUCX_C) mUCX_c))

let compose_bmatUCX_bmatUCX bmC (bc, mc) : bmatUCX =
  let bC, mC = xor_bmatUCX bc bmC in
  (bC, compose_matUCX_matUCX mC mc)

let compose_rowCX_with_C0 rCX bD : bool * decomp =
  if rowCX_allS rCX then (bD, C0) else
  (* non-trivial hypothesis *)
  match rCX with
  | A rX -> (
    if MyList.count isB rX = 1
    then match MyList.index isB rX with
      | Some i -> (bD, Id i)
      | None -> assert false (* not consistent with non-trivial hypothesis *)
    else (
      (* assert(MyList.count isB rX >= 2); *)
      let rU, rX = extract_A (mapS_to_rowU rX) in
      (bD, UCX (rU, [A rX]))
    )
  )
  | B(th0, rC) -> (
        if th0 = bD
      then (bD, C0)
    else if MyList.count isB rC = 1
      then
        match MyList.ifind (function A _ -> None | B b -> Some b) rC with
      | Some (i, if0) -> (th0 <> if0, Id i)
      | None -> assert false (* not consistent with non-trivial hypothesis *)
    else (
      (* assert(MyList.count isB rC >= 2); *)
      let rU, rC = extract_A (mapS_to_rowU rC) in
      (bD, UCX (rU, [B(true, rC)]))
    )
  )

let compose_rowCX_with_Id rCX bD i : bool * decomp =
  if rowCX_allS rCX then (bD, Id i) else
  (* non-trivial hypothesis *)
  match rCX with
  | A rX -> (
    let i', () = where_upward_A rX i in
    let rU, rX = extract_A (mapS_to_rowU (MyList.setnth rX i' (B()))) in
    (bD, UCX (rU, [A rX]))
  )
  | B(th0, rC) -> (
    let i', () = where_upward_A rC i in
    let rU, rC = extract_A (mapS_to_rowU (MyList.setnth rC i' (B(th0<>bD)))) in
    (not th0, UCX (rU, [B(true, rC)]))
  )

(*
let compose_rowCX_with_Id rCX bD i : bool * decomp =
  print_endline ("[compose_rowCX_with_Id] rCX: "^(ToS.rowCX rCX));
  print_endline ("[compose_rowCX_with_Id] bD: "^(STools.ToS.bool bD));
  print_endline ("[compose_rowCX_with_Id] i: "^(STools.ToS.int i));
  compose_rowCX_with_Id rCX bD i
  |> Tools.tee (fun bdecomp ->
    print_endline ("[compose_rowCX_with_Id] bdecomp: "^(ToS.bdecomp bdecomp)))
 *)

let compose_rowCX_bdecomp rCX (bD, decomp) : bool * decomp =
  if rowCX_allS rCX then (bD, decomp) else
  (* non-trivial hypothesis *)
  match decomp with
  | C0         -> compose_rowCX_with_C0 rCX bD
  | Id i       -> compose_rowCX_with_Id rCX bD i
  | UCX matUCX -> (bD, UCX(compose_rowCX_matUCX (xor_rowCX bD rCX) matUCX))

let compose_matCX_bdecomp matCX (bdecomp:bool * decomp) : bool * decomp =
  List.fold_right compose_rowCX_bdecomp matCX bdecomp

let rec findnth i p l =
  let rec aux i pos = function
  | [] -> None
  | h::t -> (
    if p h then
      if i = 0 then Some pos
      else aux (pred i) (succ pos) t
    else aux i (succ pos) t
  )
  in
  aux i 0 l

let compose_rowU_bdecomp rU (bD, decomp) : bool * decomp =
  match decomp with
  | C0 -> (bD, C0)
  | Id i -> (
    let i', _ = where_upward_A rU i in
    (bD, Id i')
  )
  | UCX (rU', matCX') -> (
    (* assert(listAB_countA rU = List.length rU'); *)
    (bD, UCX(compose_same rU rU', matCX'))
  )

let compose_matUCX_bdecomp (rU, matCX) bdecomp : bool * decomp =
  compose_rowU_bdecomp rU (compose_matCX_bdecomp matCX bdecomp)

let compose_bmatUCX_bdecomp (b, matUCX) (bdecomp:bool * decomp) : bool * decomp =
  let b', decomp = compose_matUCX_bdecomp matUCX bdecomp in
  (b <> b', decomp)

let compose_bdecomp_bdecomp ((bC, dC):bool*decomp) (bdc:bool*decomp) : bool * decomp =
  match dC with
  | C0 | Id _ -> assert false
  | UCX mC -> compose_bmatUCX_bdecomp (bC, mC) bdc

let compose_block_block blkC blkc =
  if do_check then assert(check_block blkC);
  if do_check then assert(check_block blkc);
  match blkC.ninput with
  | Some ninput ->
    assert(blkc.noutput = ninput);
    let neg, decomp = compose_bdecomp_bdecomp (blkC.neg, blkC.decomp) (blkc.neg, blkc.decomp) in
    {neg; noutput = blkC.noutput; ninput = blkc.ninput; decomp}
    |> Tools.check_print check_block (fun blkCc ->
      print_endline ("[compose_block_block] blkC: "^(ToS.block blkC));
      print_endline ("[compose_block_block] blkc: "^(ToS.block blkc));
      print_endline ("[compose_block_block] blkCc: "^(ToS.block blkCc)))
  | None -> assert false

let compose_block_edge blkC (blkc, next) =
  ((compose_block_block blkC blkc), next)

let compose_block_merge blkC (blkc, next) =
  ((compose_block_block blkC blkc), next)

let compose_block_merge3 blkC (blkc, next) =
  ((compose_block_block blkC blkc), next)

let normalize_block blk =
  match blk.decomp with
  | C0   ->
    assert(blk.ninput = None);
    blk
  | Id i ->
  (
    assert(blk.ninput = None);
    assert(i < blk.noutput);
    blk
  )
  | UCX matUCX ->
    let root = match blk.ninput with
    | Some nS -> (false, UCX (MyList.make nS (A()), []))
    | None    -> (false, C0)
    in
    let neg, decomp = compose_bmatUCX_bdecomp (blk.neg, matUCX) root in
    {blk with neg; decomp}

let normalize_edge (ee, nx) = (normalize_block ee, nx)

let compose_matCX_block matCX block : block =
  if matCX = [] then block else (
    let noutput = rowCX_length(List.hd matCX) in
    let neg, decomp = compose_matCX_bdecomp matCX (block.neg, block.decomp) in
    {neg; decomp; noutput; ninput = block.ninput}
  )
  |> Tools.check_print check_block (fun blk ->
    print_endline "\n[compose_matCX_block] failed: check_block";
    print_endline ("matCX: "^(ToS.matCX matCX));
    print_endline ("block: "^(ToS.block block));
    print_endline ("return: "^(ToS.block blk)))

let compose_matUCX_block (rowU, matCX) block : block =
  let noutput = List.length rowU in
  let neg, decomp =
    (block.neg, block.decomp)
    |> compose_matCX_bdecomp matCX
    |> compose_rowU_bdecomp rowU
  in
  {neg; decomp; noutput; ninput = block.ninput}

let normalize_ninput ?(neg=false) ninput : block =
  match ninput with
  | Some nS -> {neg; noutput = nS; ninput; decomp = UCX (MyList.make nS (A()), [])}
  | None    -> {neg; noutput = 0 ; ninput; decomp = C0}

let normalize_matCX matCX ninput : block =
  let root = normalize_ninput ninput in
  compose_matCX_block matCX root

let normalize_matUCX matUCX ninput : block =
  let root = normalize_ninput ninput in
  compose_matUCX_block matUCX root

let normalize_rowU rowU ninput : block = normalize_matUCX (rowU, []) ninput

let compose_bmatUCX_block (b, matUCX) block =
  cneg_block b (compose_matUCX_block matUCX block)

let compose_matCX_edge matCX (block, next) = (compose_matCX_block matCX block, next)
let compose_matUCX_edge matUCX (block, next) = (compose_matUCX_block matUCX block, next)
let compose_bmatUCX_edge bmatUCX (block, next) =
  (* print_endline ("[compose_bmatUCX_edge] bmatUCX: "^(ToS.bmatUCX bmatUCX)); *)
  (* print_endline ("[compose_bmatUCX_edge] edge: "^(ToS.edge' STools.ToS.ignore (block, next))); *)
  let result = (compose_bmatUCX_block bmatUCX block, next) in
  (* print_endline ("[compose_bmatUCX_edge] result: "^(ToS.edge' STools.ToS.ignore result)); *)
  result

let compose_rowCX_block rowCX block =
  if do_check then assert(check_block block);
  assert(rowCX_countS rowCX = block.noutput);
  let neg, decomp = compose_rowCX_bdecomp rowCX (block.neg, block.decomp) in
  {neg; noutput = rowCX_length rowCX; ninput = block.ninput; decomp}
  |> Tools.check_print check_block (fun blk ->
    print_endline  "\n[compose_rowCX_block] failed: check_block";
    print_endline ("rowCX: "^(ToS.rowCX rowCX));
    print_endline ("block: "^(ToS.block block));
    print_endline ("return: "^(ToS.block blk)))

let compose_rowU_block rowU block =
  if do_check then assert(check_block block);
  assert(MyList.count isA rowU = block.noutput);
  let neg, decomp = compose_rowU_bdecomp rowU (block.neg, block.decomp) in
  {neg; noutput = List.length rowU; ninput = block.ninput; decomp}
  |> Tools.check check_block

let compose_rowCX_edge rowCX (blk, nx) = (compose_rowCX_block rowCX blk, nx)
let compose_rowU_edge rowU (blk, nx) = (compose_rowU_block rowU blk, nx)

let compose_rowU_merge3 rowU (blk, m3) =
  (compose_rowU_block rowU blk, m3)
let compose_rowU_merge rowU (blk, m3) =
  (compose_rowU_block rowU blk, m3)

let compose_rowCX_merge3 rowCX (blk, m3) =
  (* print_endline ("[compose_rowCX_merge3] rowCX: "^(ToS.rowCX rowCX)); *)
  (* print_endline ("[compose_rowCX_merge3] block: "^(ToS.block blk)); *)
  (compose_rowCX_block rowCX blk, m3)
  (* |> Tools.tee (fun m3 -> print_endline ("[compose_rowCX_merge3] merge3: "^(ToS.emerge3 STools.ToS.ignore m3))) *)

let compose_rowCX_merge rowCX (blk, m3) =
  (* print_endline ("[compose_rowCX_merge3] rowCX: "^(ToS.rowCX rowCX)); *)
  (* print_endline ("[compose_rowCX_merge3] block: "^(ToS.block blk)); *)
  (compose_rowCX_block rowCX blk, m3)
  (* |> Tools.tee (fun m3 -> print_endline ("[compose_rowCX_merge3] merge3: "^(ToS.emerge STools.ToS.ignore m3))) *)

let type_rowCX_of_matCX matCX =
  match matCX with
  | [] -> None
  | hd::tl -> Some(match hd with A _ -> A() | B(th0, _) -> B())

let subst_matUCX_in_edge matUCX (ee, nx) =
  if ee.ninput = None && snd matUCX = []
  then (
    assert(nx = Tree.GLeaf());
    cst_edge ee.neg (List.length(fst matUCX))
  )
  else ({ee with noutput = List.length (fst matUCX); decomp = UCX matUCX}, nx)

let extract_rowCX_from_edge edge =
  match (fst edge).decomp with
  | UCX (rowU, mat) -> (
    match mat with
    | [] -> None
    | rowCX::mat' -> (
      let rowCX', rowU' = swap_rowU_rowCX rowU rowCX in
      Some (xor_rowCX (fst edge).neg rowCX', subst_matUCX_in_edge (rowU', mat') edge)
    )
  )
  | _ -> None

let extract_rowCX_from_edge edge =
  match (fst edge).decomp with
  | UCX (rowU, mat) -> (
    match mat with
    | [] -> None
    | rowCX::mat' -> (
      let rowCX', rowU' = swap_rowU_rowCX rowU rowCX in
      Some (xor_rowCX (fst edge).neg rowCX', subst_matUCX_in_edge (rowU', mat') edge)
    )
  )
  | _ -> None
