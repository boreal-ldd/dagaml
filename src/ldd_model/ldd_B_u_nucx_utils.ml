(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *   Negation, Useless variables, Xor variables, Canalizing variables
 *
 * module  : Ldd_u_nucx_utils : Toolbox
 *)

open GuaCaml
open Extra
open AB
open Utils
open Ldd_B_u_nucx_types

let arity_edge (block, next) = block.noutput
let arity_node ((), e0, e1) =
  let a0 = arity_edge e0
  and a1 = arity_edge e1 in
  assert(a0 = a1);
  (succ a0)
let arity_merge (block, next) = block.noutput
let arity_merge3 (block, next) = block.noutput
let arity_block block = block.noutput

let neg_block blk = {blk with neg = not blk.neg}
let cneg_block b blk = {blk with neg = b <> blk.neg}

let cst_block neg noutput : block =
  {neg; noutput; ninput = None; decomp = C0}

let neg_edge (blk, nx) : _ edge' = (neg_block blk, nx)
let cneg_edge b (blk, nx) : _ edge' = (cneg_block b blk, nx)
let cst_edge b n : _ edge' = (cst_block b n, Tree.GLeaf())

let get_cst b e = cst_edge b (arity_edge e)

let support_of_edge ((blk, nx) : _ edge') : bool list =
  match blk.decomp with
  | C0 -> MyList.make blk.noutput false
  | Id k -> (
    MyList.ntimes
      ~carry:(true::(MyList.make (blk.noutput-k-1) false))
       false k
  )
  | UCX (rU, _) -> rU ||> (function A() -> true | B() -> false)

let rowCX_allS : rowCX -> bool = function
  | A rX        -> listAB_allA rX
  | B ( _ , rC) -> listAB_allA rC

let rowCX_length : rowCX -> int = function
  | A rX -> List.length rX
  | B (_, rC) -> List.length rC

let rowCX_type : rowCX -> (unit, bool) ab = function
  | A _ -> A()
  | B (th0, _) -> B th0

let rowCX_countS : rowCX -> int = function
  | A     rX  -> MyList.count isA rX
  | B (_, rC) -> MyList.count isA rC

let rowCX_countB : rowCX -> int = function
  | A     rX  -> MyList.count isB rX
  | B (_, rC) -> MyList.count isB rC

let id_block ?(neg=false) noutput =
  {
    neg;
    noutput;
    ninput = Some noutput;
    decomp = UCX(MyList.make noutput (A()), [])
  }

let id_edge ?(neg=false) noutput nx = (id_block ~neg noutput, nx)

let block_of_matCX ?(neg=false) noutput matCX ninput =
  {neg; noutput; ninput; decomp = UCX(MyList.make noutput (A()), matCX)}

let edge_of_matCX ?(neg=false) noutput matCX ninput nx =
  (block_of_matCX ~neg noutput matCX ninput, nx)

let merge_of_edge (ee, nx) = (ee, MEdge nx)
let merge3_of_edge (ee, nx) = (ee, M3Edge nx)

let cst_merge3 b n = merge3_of_edge (cst_edge b n)

let block_is_cst blk =
  match blk.decomp with
  | C0 -> Some blk.neg
  | _ -> None

let edge_is_cst (ee, _) = block_is_cst ee

let mapS_to_rowU row =
  Tools.map (function
    | A () -> B(A())
    | B b  -> B(B b)) row

let make_id_block neg noutput : block =
  {neg; noutput; ninput = Some noutput; decomp = UCX (MyList.make noutput (A()), [])}

let merge3_node fX fY =
  assert((fst fX).noutput = (fst fY).noutput);
  let fX, fY = Poly.v2_sort (fX, fY) in
  (make_id_block false (fst fX).noutput, M3Node((), fX, fY))

let peval_of_rowC0 rC0 =
  Tools.map (function B if0 -> Some(not if0) | _ -> None) rC0

let extract_neg_from_edge (ee, nx) = (ee.neg, ({ee with neg = false}, nx))
let set_neg_edge neg (ee, nx) = ({ee with neg}, nx)

let compose_neg_block b block = {block with neg = block.neg <> b}
let compose_neg_edge b (ee, nx) = (compose_neg_block b ee, nx)
let compose_neg_merge3 b (ee, nx) = (compose_neg_block b ee, nx)

let dual_rowCX = function
  | A rX        -> A rX
  | B (th0, rC) -> B(not th0, rC)

let xor_rowCX b r = if b then dual_rowCX r else r

let dual_matCX mat = Tools.map dual_rowCX mat
let xor_matCX b m = if b then dual_matCX m else m

let dual_matUCX (rU, matCX) = (rU, dual_matCX matCX)
let xor_matUCX b m = if b then dual_matUCX m else m

let dual_bmatUCX (b, matUCX) = (not b, dual_matUCX matUCX)
let xor_bmatUCX bx (b, matUCX) = (bx <> b, xor_matUCX bx matUCX)

let pedge_of_edge ((ee, nx) : _ edge') : _ pedge =
  match nx with
  | Tree.GLeaf() -> (ee, Tree.GLeaf())
  | Tree.GLink lk -> (ee, Tree.GLink(None, lk))

let get_type_matCX neg (m:matCX) : (unit, bool) ab option =
  match m with
  | [] -> None
  | h::_ -> match h with
    | A _ -> Some(A())
    | B(th0, _) -> Some(B(neg<>th0))

let get_type_matUCX neg ((_, matCX):matUCX) : (unit, bool) ab option =
  get_type_matCX neg matCX
