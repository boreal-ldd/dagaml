(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2015-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nux : Uniform model with Negation, Useless variables
 *   and Xor variables
 *
 * module  : Ldd_u_nux_types : Types
 *)

open GuaCaml

(* S => Significant
   P => Polarity-Phase invariant
     P false => Useless
     P true  => Xor-variable
  *)
type uniq_elem = P of bool | S

type uniq = uniq_elem list

type leaf = unit
type edge = bool * uniq
type node = unit

type 'i next' = ('i, leaf) Tree.gnext
type 'i edge' = edge * 'i next'
type 'i node' = node * 'i edge' * 'i edge'
