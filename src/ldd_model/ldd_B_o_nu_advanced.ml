(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-nu : Ordered model with Useless variables
 *
 * module  : Ldd_o_nu_avanced : Generic Functional Interface
 *)

open GuaCaml
open Extra
open BTools
open Ldd_B_o_nu

module ComputeSupport =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = unit
    type param = unit

    type xnode = bool list
    type xedge = bool list

    let map_next (():extra) (():param) : _ -> xedge = function
      | Tree.GLeaf _ -> []
      | Tree.GLink lk -> lk ()

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (():param) (((b, (x, y)), nx): _ M.M.edge') : xedge =
      MyList.ntimes ~carry:(map_next () () nx) false (x-y)

    let blist_union (bl1:bool list) (bl2:bool list) : bool list =
      List.map2 (||) bl1 bl2

    let map_node (():extra) (():param) (((), f0, f1): _ M.M.node') : xnode =
      let s0 = map_edge () () f0
      and s1 = map_edge () () f1 in
      (true::(blist_union s0 s1))
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.manager (* LDD-Support manager, defined for [Support] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : bool list =
    Module.rec_edge tt () f

  let solve_ilist (tt:tt) (f:LDD.f) : int list =
    solve tt f |> MyList.indexes_true
end

module ToSTree =
struct
  open STools
  open ToSTree
  module Model =
  struct
    module M = LDD.G0

    type extra = unit
    type param = Tree.stree list

    type xnode = Tree.stree
    type xedge = Tree.stree

    let map_next (():extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf () -> Tree.Node [Tree.Leaf "False"]
      | Tree.GLink lk -> (lk param)

    let map_nU (():extra) (nU:int) (param:param) (next:param -> xedge) : xedge =
      let hd, tl = MyList.hdtl_nth nU param in
      next tl

    let map_cneg (():extra) (neg:bool) (s:xedge) : xedge =
      Tree.Node [Tree.Leaf "CNEG"; bool neg; s]

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (param:param) (((b, (x, y)), nx): _ M.M.edge') : xedge =
      map_cneg () b (map_nU () (x-y) param (fun param -> map_next () param nx))

    let map_node (():extra) (param:param) ((_, f0, f1): _ M.M.node') : xnode =
      match param with
      | [] -> assert false
      | hd::tl -> (
        let s0 = map_edge () tl f0
        and s1 = map_edge () tl f1 in
        Tree.Node [Tree.Leaf "ITE"; hd; s0; s1]
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-ToSTree manager, defined for [ToSTree] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve ?(inputs=None) (tt:tt) (f:LDD.f) : Tree.stree =
    let ari = ThisG.arity f in
    let inputs =
      match inputs with
      | Some inputs -> (
        assert(List.length inputs = ari);
        inputs
      )
      | None -> (
        List.init ari (fun x -> Tree.Node [Tree.Leaf "VAR"; Tree.Leaf (string_of_int x)])
      )
    in
    Module.rec_edge tt inputs f

  let solve_to_string ?(inputs=None) (tt:tt) (f:LDD.f) : string =
    let stree = solve ~inputs tt f in
    OfSTree.to_string stree

end

module CntSat =
struct
  open BTools
  module Model =
  struct
    module M = LDD.G0
    open BArray.Nat

    type extra = unit
    type param = unit

    type xnode = nat * nat
    type xedge = nat * nat

    let map_next (():extra) (():param) : _ -> xedge = function
      | Tree.GLeaf () -> (zero(), unit())
      | Tree.GLink lk -> lk ()

    let shift_lefti2 (x0, x1) nU =
      (shift_lefti x0 nU, shift_lefti x1 nU)

    (* map_edge extra src f = (label, edge) *)
    let map_edge (():extra) (():param) (((b, (x, y)), nx): _ M.M.edge') : xedge =
      let nU = x - y in
      Tools.cswap b (shift_lefti2 (map_next () () nx) nU)

    let ( +// ) (x0, x1) (y0, y1) = (x0 +/ y0, x1 +/ y1)

    let map_node (():extra) (():param) ((_, f0, f1): _ M.M.node') : xnode =
      (map_edge () () f0) +// (map_edge () () f1)
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-CntSat manager, defined for [CntSat] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : BNat.nat =
    Module.rec_edge tt () f |> fst
end

let sys_to_cntsat fileA =
  let sys = OfExpr.GO.system_off fileA in
  let pure = sys.IoTypes.man.LDD.cons in
  let edges = Array.to_list sys.IoTypes.sset ||> snd in
  let cntsat = CntSat.newman pure in
  edges
  ||> CntSat.solve cntsat
  ||> STools.ToSTree.leaf BTools.BArray.Nat.to_string

(* Forall Relative Naming : eliminate quantified variables from the ordering *)
module ForallR =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }
    type param = bool * (bool list)

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) ((neg, param):param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (MyList.count_false hd, (neg, tl))

    let map_cneg (b:bool) ((neg, param):param) : param =
      (b <> neg, param)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf () -> cst (fst param) 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) ((((b, (x, y)), nx) as ee):(param -> xedge) M.M.edge') : xedge =
      assert(List.length (snd param) = arity ee);
      let nU = x - y in
      let nU, param' = param |> map_cneg b |> map_nU nU in
      ThisG.pushU ~n:nU (map_next extra param' nx)

    let map_node (extra:extra) ((neg, param):param) (((_, f0, f1) as nd):(param -> xnode) M.M.node') : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | b0::param' -> (
        let param' = (neg, param') in
        let g0 = map_edge extra param' f0
        and g1 = map_edge extra param' f1 in
        if b0
        (* b0 = true  : eliminate the variable *)
        then (LDD.And.solve extra.and_man g0 g1)
        (* b0 = false : keep the variable *)
        else (LDD.G1.push extra.ldd_man ((), g0, g1))
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt (false, param) f
end

(* Forall Absolute Naming (V0) : does not change variable names *)
(* remark:
  absolute naming can be emulated using either :
    - a wrapper for support variables, or
    - LDD-U-U based models (with uniform useless variables)
 *)
module ForallA =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }
    type param = bool * (bool list)

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) ((neg, param):param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (nU, (neg, tl))

    let map_cneg (b:bool) ((neg, param):param) : param =
      (b <> neg, param)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf () -> cst (fst param) 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) ((((b, (x, y)), nx) as ee):(param -> xedge) M.M.edge') : xedge =
      assert(List.length (snd param) = arity ee);
      let nU = x - y in
      let nU, param' = param |> map_cneg b |> map_nU nU in
      ThisG.pushU ~n:nU (map_next extra param' nx)

    let map_node (extra:extra) ((neg, param):param) (((_, f0, f1) as nd):(param -> xnode) M.M.node') : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | b0::param' -> (
        let param' = (neg, param') in
        let g0 = map_edge extra param' f0
        and g1 = map_edge extra param' f1 in
        if b0
        (* b0 = true  : eliminate the variable *)
        (* [RQ] only difference with relative naming version [push-U] *)
        then (ThisG.pushU ~n:1 (LDD.And.solve extra.and_man g0 g1))
        (* b0 = false : keep the variable *)
        else (LDD.G1.push extra.ldd_man ((), g0, g1))
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt (false, param) f
end

(* Partial Evaluation (Relative Naming) *)
module PEvalR =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.t;  (* LDD manager *)
    }
    type param = bool option list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) (param:param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (MyList.count_None hd, tl)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf () -> cst false 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) (((b, (x, y)), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      let nU = x - y in
      let nU, param' = map_nU nU param in
      ThisG.cneg b
        (ThisG.pushU ~n:nU (map_next extra param' nx))

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | opb::param' -> (
        match opb with
        | None -> (
          let g0 = map_edge extra param' f0
          and g1 = map_edge extra param' f1 in
          LDD.G1.push extra.ldd_man ((), g0, g1)
        )
        | Some b -> (
          map_edge extra param' (if b then f1 else f0)
        )
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [PEvalR] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) : tt =
    let extra = Model.{ldd_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) : tt = makeman ldd_man

  let solve (tt:tt) (param:bool option list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

(* Partial Evaluation (Absolute Naming) *)
module PEvalA =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = {
      ldd_man   : LDD.t;  (* LDD manager *)
    }
    type param = bool option list

    type xnode = LDD.f
    type xedge = LDD.f

    let map_nU (nU:int) (param:param) : int * param =
      let hd, tl = MyList.hdtl_nth nU param in
      (nU, tl)

    let map_next (extra:extra) (param:param) : _ -> xedge = function
      | Tree.GLeaf () -> cst false 0
      | Tree.GLink lk -> lk param

    let map_edge (extra:extra) (param:param) (((b, (x, y)), nx) as ee) : xedge =
      assert(List.length param = arity ee);
      let nU = x - y in
      let nU, param' = map_nU nU param in
      ThisG.cneg b
        (ThisG.pushU ~n:nU (map_next extra param' nx))

    let map_node (extra:extra) (param:param) ((_, f0, f1) as nd) : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | opb::param' -> (
        match opb with
        | None -> (
          let g0 = map_edge extra param' f0
          and g1 = map_edge extra param' f1 in
          LDD.G1.push extra.ldd_man ((), g0, g1)
        )
        | Some b -> (
          ThisG.pushU ~n:1
            (map_edge extra param' (if b then f1 else f0))
        )
      )
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) : tt =
    let extra = Model.{ldd_man} in
    let man = Module.makeman ldd_man extra hsize in
    man
  let newman (ldd_man:LDD.And.t) : tt = makeman ldd_man

  let solve (tt:tt) (param:bool option list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f
end

module OOPS =
struct
  type tt = {
    man_oops : LDD.OOPS.Model.t;
    man_supp : ComputeSupport.tt;
    man_tostree : ToSTree.tt;
    man_pevala : PEvalA.tt;
    man_pevalr : PEvalR.tt;
    man_foralla : ForallA.tt;
    man_forallr : ForallR.tt;
    profile : OProfile.t;
  }

  let get_man_ldd tt =
    LDD.get_cons tt.man_oops

  type ff = LDD.f

  let makeman ?man_oops () =
    let man_oops = match man_oops with
      | Some man_oops -> man_oops
      | None         -> LDD.newman()
    in
    let man_ldd = LDD.get_cons man_oops in
    let man_and = LDD.get_mand man_oops in
    let man_supp = ComputeSupport.newman man_ldd in
    let man_tostree = ToSTree.newman man_ldd in
    let man_pevala = PEvalA.newman man_ldd in
    let man_pevalr = PEvalR.newman man_ldd in
    let man_foralla = ForallA.newman man_ldd man_and in
    let man_forallr = ForallR.newman man_ldd man_and in
    let profile = OProfile.newman ~prefix:"[OOPS]" () in
    {
      man_oops;
      man_supp; man_tostree; man_pevala; man_pevalr; man_foralla; man_forallr;
      profile
    }

  module Model =
  struct
    open LDD.OOPS.Model
    type t = tt
    type f = ff

    let newman () = makeman ()

    let print_profile (t:t) : unit =
      OProfile.stop_other t.profile;
      (* LDD.And.print_profile t.man_and; *)
      OProfile.print t.profile;
      ()

    (* Section 0. OOPS forward definition *)
      (* sub-section 1. Functional / Propositional Aspects *)
    let ( ->> ) t bl f = ( ->> ) t.man_oops bl f
    let arity  t f = arity t.man_oops f
    let   cneg t b f = cneg t.man_oops b f
    let    neg t f = cneg t true f
    let ( &! ) t f1 f2 = ( &! ) t.man_oops f1 f2
    let ( |! ) t f1 f2 = ( |! ) t.man_oops f1 f2
    let ( ^! ) t f1 f2 = ( ^! ) t.man_oops f1 f2
    let ( =! ) t f1 f2 = ( =! ) t.man_oops f1 f2
    let ( *! ) t f0 f1 = ( *! ) t.man_oops f0 f1

    let cst t b n = cst t.man_oops b n
    let var t b n k = var t.man_oops b n k
    let to_bool t f = to_bool t.man_oops f

    let cofactor t b f = cofactor t.man_oops b f
    let id t f = id t.man_oops f
    let eq t f0 f1 = eq t.man_oops f0 f1

      (* sub-section 2. Manager Management *)
    let copy_into t fl t' = copy_into t.man_oops fl t'.man_oops

    let to_barray ?(nocopy=false) ?(destruct=false) t fl =
      to_barray ~nocopy ~destruct t.man_oops fl

    let of_barray ?(t=None) ba =
      let t, fl = of_barray ~t:(Tools.opmap (fun t -> t.man_oops) t) ba in
      (makeman ~man_oops:t (), fl)

    let bw_fl ?(nocopy=false) ?(destruct=false) t =
      bw_fl ~nocopy ~destruct t.man_oops

    let br_fl ?(t=None) cha =
      let t, fl = br_fl ~t:(Tools.opmap (fun t -> t.man_oops) t) cha in
      (makeman ~man_oops:t (), fl)

    (* [FIXME] *)
    let t_stats t = t_stats t.man_oops

    let f_stats t fl = f_stats t.man_oops fl

    (* [FIXME] *)
    let clear_caches t =
      clear_caches t.man_oops

    let keep_clean ?(normalize=false) t fl =
      clear_caches t;
      keep_clean ~normalize t.man_oops fl

    let check t f = check t.man_oops f

    (* Section 3. File-based Management *)

    module F =
    struct
      type f' = F.f'

      let arity = F.arity
      let size  = F.size

      let of_f ?(nocopy=false) ?(normalize=true) t f =
        F.of_f ~nocopy ~normalize t.man_oops f

      let to_f t f' =
        F.to_f t.man_oops f'

      let free f' = F.free f'

      let tof = F.tof
      let off = F.off
    end

    let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) t =
      tof ~nocopy ~normalize ~destruct t.man_oops
    let off cha =
      let t, fl = off cha in
      (makeman ~man_oops:t (), fl)

    (* Section 2. QEOPS level interface *)

    let to_string (t:t) (f:f) : string =
      ToSTree.solve_to_string t.man_tostree f

    let support (t:t) (f:f) : Support.t =
      let l = ComputeSupport.solve t.man_supp f in
      assert(List.length l <= arity t f);
      Support.of_bool_list l

    let pevalA (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = PEvalA.solve t.man_pevala param f in
      assert(arity t g = arity t f);
      g

    let pevalR (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = PEvalR.solve t.man_pevalr param f in
      assert(arity t g = MyList.count_None param);
      g

    let forallA (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_bool_list param in
      assert(arity t f = List.length param);
      (* let g0 = ForallA0.solve t.man_foralla0 param f in *)
      let g = ForallA.solve t.man_foralla param f in
      (*
      if not (LDD.G1.(=/) g0 g)
      then (
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : begin";
        print_endline "[Ldd_G_o_u.OOPS.forallA] g0";
        print_string (to_string t g0);
        print_endline "[Ldd_G_o_u.OOPS.forallA] g";
        print_string (to_string t g);
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : end";
        assert false
      );
      *)
      assert(arity t g = arity t f);
      g

    let forallR (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_bool_list param in
      assert(arity t f = List.length param);
      let g = ForallR.solve t.man_forallr param f in
      assert(arity t g = MyList.count_false param);
      g

    let existsA t param f : f =
      neg t (forallA t param (neg t f))

    let existsR t param f : f =
      neg t (forallR t param (neg t f))

    (* Section 3. AQEOPS level interface *)

    let ( &!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((&!) t) f0 fl)

    let ( |!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((|!) t) f0 fl)

    let ( ^!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((^!) t) f0 fl)

    let ( =!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((=!) t) f0 fl)

    (* return [true] iff [prim] is implemented within the computation model *)
    let builtin_o_prim = function
      | AQEOPS.U -> true
      | _ -> false
    (* counts the number of ordered [prim] primitives available *)
    let detect_o_prim t p ((_, (x, y)), _) =
      match p with
      | AQEOPS.U -> x - y
      | _ -> 0
    let intro_o_prim t p n f =
      match p with
      | AQEOPS.U -> ThisG.pushU ~n f
      | _ -> assert false
    let elim_o_prim t p n ((b, (x, y)), nx) =
      match p with
      | AQEOPS.U -> assert(x-n >= y); ((b, (x-n, y)), nx)
      | _ -> assert false
  end

  let newman = Model.newman
  let print_profile = Model.print_profile

  include OOPS.Make(Model)

  module SUInt = SUInt.Make(Model)
  module VUInt = VUInt.Make(Model)
  module LoadCnf0 = LoadCnf.Make0(Model)
  module LoadCnfA = LoadCnf.MakeA(Model)
  module LoadCnfQ = LoadCnf.MakeQ(Model)
  module CoSTreD = AQEOPS_CoSTreD.Make(Model)
  module CoSTreD_CNF = AQEOPS_CoSTreD.MakeCNF(Model)
  module CCoSTreD = AQEOPS_CCoSTreD.Make(Model)
  module CCoSTreD_CNF = AQEOPS_CCoSTreD.MakeCNF(Model)
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Model)
end

module CSTypes = Constraint_system_types
module CSUtils = Constraint_system_utils
module CSCoSTreD  = Constraint_system_costred

module SNAX = SnaxOops.OOPS.Model

module CoSTreD_Model : CSCoSTreD.MSig =
struct
  module Q = OOPS.Model

  module BddToSnax = ComposeOops(SNAX)
  let translate_pure_to_snax (pure:Q.t) (snax:SNAX.t) (fl:Q.f list) : SNAX.f list =
      let pure = OOPS.get_man_ldd pure in
      BddToSnax.translate pure snax fl

  let default_module_name = default_module_name
end

module CoSTreD_Module = CSCoSTreD.Make(CoSTreD_Model)
