(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_pull : Reversing Shannon Factorization
 *)


open GuaCaml
open AB
open Utils
open STools

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_io

let opminus opx y =
  match opx with
  | Some x -> Some(x-y)
  | None   -> None

let rec pull_matCX ninput nx carry = function
  | [] -> B (List.rev carry)
  | (A rX)::tl -> (
    let hdX, tlX = MyList.hdtl rX in
    match hdX with
    | A() -> (
      pull_matCX ninput nx ((A tlX)::carry) tl
    )
    | B() -> (
      (* print_endline ("tl: "^(ToS.matCX tl)); *)
      let tail_0 = (normalize_matCX tl ninput, nx) in
      (* print_endline ("tail_0: "^(ToS.edge' STools.ToS.ignore tail_0)); *)
      let tail_1 = neg_edge tail_0 in
      (* print_endline ("tail_0: "^(ToS.edge' STools.ToS.ignore tail_0)); *)
      (* let matCX = List.rev((A tlX)::carry) in *)
      (* print_endline ("matCX: "^(ToS.matCX matCX)); *)
      let matCX = List.rev((A tlX)::carry) in
      let edge_0 = compose_matCX_edge matCX tail_0 in
      let edge_1 = compose_matCX_edge matCX tail_1 in
      A(edge_0, edge_1)
    )
  )
  | (B(th0, rC))::tl -> (
    let hdC, tlC = MyList.hdtl rC in
    match hdC with
    | A() -> (
      pull_matCX ninput nx ((B(th0, tlC))::carry) tl
    )
    | B if0 -> (
      let tail_else = (normalize_matCX tl ninput, nx) in
      let tail_then = cst_edge th0 (arity_edge tail_else) in
      let matCX = List.rev((B(th0, tlC))::carry) in
      let edge_else = compose_matCX_edge matCX tail_else in
      let edge_then = compose_matCX_edge matCX tail_then in
      if if0
      then A(edge_else, edge_then)
      else A(edge_then, edge_else)
    )
  )

let pull_matUCX ninput nx (rowU, matCX) =
  let hdU, tlU = MyList.hdtl rowU in
  match hdU with
  | A() -> (
    (* print_endline ("[pull_matUCX] ninput: "^(STools.ToS.(option int) ninput)); *)
    (* print_endline ("[pull_matUCX] matCX: "^(ToS.matCX matCX)); *)
    match pull_matCX ninput nx [] matCX with
    | A(f0, f1) ->
      let f0' = compose_rowU_edge tlU f0
      and f1' = compose_rowU_edge tlU f1 in
      A(f0', f1')
    | B(matCX') -> (
      B(tlU, matCX')
    )
  )
  | B() -> (
    let f0 : _ edge' = (normalize_matUCX (tlU, matCX) ninput, nx) in
    A(f0, f0)
  )

let pull_bmatUCX ninput nx (b, matUCX) =
  match pull_matUCX ninput nx matUCX with
  | A(f0, f1) -> A(cneg_edge b f0, cneg_edge b f1)
  | B(matUCX') -> B(b, matUCX')
