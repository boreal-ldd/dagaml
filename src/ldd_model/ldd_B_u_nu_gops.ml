(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2015-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml    : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nu  : Uniform model with Negation and Useless variables
 *
 * module  : Ldd_u_nu_gops : General OPeratorS
 *)

open GuaCaml
open Ldd_B_u_nu_types
open Ldd_B_u_nu_io
open Extra

let arity = arity_edge'

let arity_node ((), e0, e1) =
  let a0 = arity e0 and a1 = arity e1 in
  assert(a0 = a1);
  (succ a0)

let push_U ?(n=1) (((b, su), nx) : _ edge') : _ edge' =
  assert(n>=0);
  ((b, MyList.ntimes ~carry:su U n), nx)

let split_by_uniq (l:'a list) (su:uniq) : 'a list * 'a list =
  List.combine l su
  ||> (fun (p, su) ->
    match su with
    | U -> (Some p, None  )
    | S -> (None  , Some p))
  |> List.split
  |> (fun (opu, opp) ->
      (MyList.unop opu, MyList.unop opp))

let edge_of_mask (mask:bool list) =
  (false, Tools.map (function true -> S | false -> U) mask)

let support_of_edge ((b, bl), nx) : bool list =
  bl ||> (function U -> false | S -> true)

let merge_uniq (uniqX, uniqY) = Utils.consensus (function
  | U, U -> U, None
  | x, y -> S, Some(x, y)) uniqX uniqY

let solve_cons ((), (((bX, lX), iX) as eX), (((bY, lY), iY) as eY)) =
  (* print_string "$C"; print_newline(); *)
  assert(List.length lX = List.length lY);
  if eX = eY
  then ((bX, U::lX), Utils.MEdge iX)
  else
  (
    let lXY, (lX', lY') = merge_uniq (lX, lY) in
    ((bX, S::lXY), Utils.MNode ((), ((false, lX'), iX), ((bX<>bY, lY'), iY)))
  )

let make_cst b n = ((b, MyList.ntimes U n), Tree.GLeaf())
let cst = make_cst
let get_root b f = make_cst b (arity f)
let get_cst = get_root

let is_cst ((b, l), nx) =
  match nx with
  | Tree.GLeaf() -> Some b
  | Tree.GLink _ -> None

let solve_and (((), (((bx, lx), ix) as x), (((by, ly), iy) as y)) : _ node') =
  (* print_string "$A"; print_newline(); *)
  assert(List.length lx = List.length ly);
  match ix with
  | Tree.GLeaf () ->
  (
    let e, i = if bx then (* x ~ 1 *) y else (* x ~ 0 *) x in
    (e, Utils.M3Edge i)
  )
  | Tree.GLink nx ->
  match iy with
  | Tree.GLeaf () ->
  (
    let e, i = if by then (* y ~ 1 *) x else (* y ~ 0 *) y in
    (e, Utils.M3Edge i)
  )
  | Tree.GLink ny ->
  if (nx = ny) && (lx = ly)
  then
  (
    let e, i = if bx = by then (* x =  y *) x else (* x = ~y *) (get_root false x) in
    (e, Utils.M3Edge i)
  )
  else
  (
    let lxy, (lx', ly') = merge_uniq (lx, ly) in
    let bx, ix, lx', by, iy, ly' = if ((nx, bx, lx') <= (ny, by, ly'))
      then (bx, ix, lx', by, iy, ly')
      else (by, iy, ly', bx, ix, lx')
    in
    ((false, lxy), Utils.M3Node ((), ((bx, lx'), ix), ((by, ly'), iy)))
  )

let neg ((b, l), i) = ((not b, l), i)
let cneg x ((b, l), i) = (( x <> b, l), i)

let solve_xor ((), (((bx, lx), ix)as x), (((by, ly), iy)as y)) =
  (* print_string "$X"; print_newline(); *)
  assert(List.length lx = List.length ly);
  match ix with
  | Tree.GLeaf () ->
  (
    let e, i = cneg bx y in
    (e, Utils.M3Edge i)
  )
  | Tree.GLink nx ->
  match iy with
  | Tree.GLeaf () ->
  (
    let e, i = cneg by x in
    (e, Utils.M3Edge i)
  )
  | Tree.GLink ny ->
  if (nx == ny) && (lx = ly)
  then
  (
    let e, i = get_root (bx<>by) x in
    (e, Utils.M3Edge i)
  )
  else
  (
    let lxy, (lx', ly') = merge_uniq (lx, ly) in
    let ix, lx', iy, ly' = if (( nx, lx') <= ( ny, ly'))
      then (ix, lx', iy, ly')
      else (iy, ly', ix, lx')
    in
    ((bx<>by, lxy), Utils.M3Node ((), ((false, lx'), ix), ((false, ly'), iy)))
  )

let compose_uniq lx ly =
  let rec aux c = function
    | ([], []) -> List.rev c
    | ([], _ ) -> assert false
    | (U::x, y) -> aux (U::c) (x, y)
    | (S::x, []) -> assert false
    | (S::x, h::y) -> aux (h::c) (x, y)
  in aux [] (lx, ly)

let compose_edge (bx, lx) (by, ly) =
  (bx<>by, compose_uniq lx ly)

let compose ex (ey, i) =
  (compose_edge ex ey, i)

let node_pull (((bx, lx), i) :  'lk Ldd_B_u_nu_types.edge') : ('lk Ldd_B_u_nu_types.node', 'lk * ('lk Ldd_B_u_nu_types.node' -> 'lk Ldd_B_u_nu_types.node')) Utils.merge = match lx with
  | [] -> assert false
  | h::lx' -> let e' = (bx, lx') in match h with
    | U -> Utils.MEdge ((), (e', i), (e', i))
    | S -> Utils.MNode (Utils.gnode_node i, fun ((), edge0, edge1) ->
      ((), (compose e' edge0), (compose e' edge1))
    )

let assign peval ((neg, sub), next) =
  let sub, peval = Utils.consensus3 (function
    | None, x -> Some x, Some None
    | Some b, x -> (None, match x with U -> None | S -> Some (Some b))
  ) peval sub in
  let edge = (neg, sub) in
  let opevalC = if List.for_all ((=)None) peval
    then None
    else (Some peval)
  in
  match next with
  | Tree.GLeaf ()   -> assert(opevalC = None); (edge, Tree.GLeaf ())
  | Tree.GLink (opevalc, node) -> (edge, Tree.GLink (Utils.compose_opeval opevalC opevalc, node))

