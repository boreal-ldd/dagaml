(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-nu  : Interface with the MLBDD package (Arlen Cox) implementing the ldd-o-nu model
 *
 * module  : Ldd_mlbdd : Model Root
 *)

open GuaCaml
open Extra
open BTools

module LDD =
struct
  module OOPS =
  struct
    let apply_mask (man:MLBDD.man) (mask:bool list) (arity:int) (f:MLBDD.t) : int * MLBDD.t =
      let amask = mask |> MyList.indexes_true |> Array.of_list in
      assert(Array.length amask = arity);
      (List.length mask, MLBDD.permute amask f)

    module Model =
    struct
      type t = MLBDD.man
      type f = {
        ari : int;
        sem : MLBDD.t;
      }

      let arity _ f = f.ari
      let ( ->> ) t mask f =
        assert(f.ari = MyList.count_true mask);
        let ari, sem = apply_mask t mask f.ari f.sem in
        let f' = {ari; sem} in
        assert(f'.ari = List.length mask);
        f'

      let neg (t:t) (f:f) : f = {f with sem = MLBDD.dnot f.sem}

      let cneg t b f =
        if b then neg t f else f

      let ( *! ) t f0 f1 =
        assert(f0.ari = f1.ari);
        let f0' = MLBDD.permutef (fun i -> i+1) f0.sem in
        let f1' = MLBDD.permutef (fun i -> i+1) f1.sem in
        let f01 = {ari = f0.ari+1; sem = MLBDD.ite f0' 0 f1'} in
        assert(f01.ari = f0.ari+1);
        f01

      let ( &! ) t f0 f1 =
        assert(f0.ari = f1.ari);
        let f' = {ari = f0.ari; sem = MLBDD.dand f0.sem f1.sem} in
        assert(f'.ari = f0.ari);
        f'
      let ( ^! ) t f0 f1 =
        assert(f0.ari = f1.ari);
        let f' = {ari = f0.ari; sem = MLBDD.xor f0.sem f1.sem} in
        assert(f'.ari = f0.ari);
        f'
      let ( |! ) t f0 f1 =
        assert(f0.ari = f1.ari);
        let f' = {ari = f0.ari; sem = MLBDD.dor f0.sem f1.sem} in
        assert(f'.ari = f0.ari);
        f'
      let ( =! ) t f0 f1 =
        assert(f0.ari = f1.ari);
        let f' = {ari = f0.ari; sem = MLBDD.eq f0.sem f1.sem} in
        assert(f'.ari = f0.ari);
        f'

      let cst t b n =
        {ari = n; sem = (if b then MLBDD.dtrue t else MLBDD.dfalse t)}

      let var t b n k =
        assert(0 <= k && k < n);
        let f = {ari = n; sem = MLBDD.ithvar t k} in
        cneg t b f

      let to_bool t f =
        match MLBDD.inspectb f.sem with
        | MLBDD.BFalse -> Some false
        | MLBDD.BTrue  -> Some true
        | MLBDD.BIf _  -> None

      let cofactor (t:t) (b:bool) (f:f) : f =
        assert(f.ari > 0);
        let f0, f1 = MLBDD.cofactor 0 f.sem in
        let f' = if b then f1 else f0 in
        let sem = MLBDD.permutef (fun n -> assert(n>0); n-1) f' in
        {ari = f.ari -1; sem}

      let id (t:t) (f:f) : BTools.barray =
        ToB.closure ToB.(int * signed_int) (f.ari, MLBDD.id f.sem)

      let eq (t:t) (f0:f) (f1:f) : bool =
        f0.ari = f1.ari && MLBDD.equal f0.sem f1.sem

      let newman _ = MLBDD.init  ()

      (* copies a list of function from a given manager into another one *)
      let copy_into (t0:t) (fl0:f list) (t1:t) : f list =
        assert false

      (* unserializes a list of function into a given manager *)
      let of_barray ?(t=(None:(t option))) (ba:BTools.BArray.t) : t * f list =
        assert false

      let do_nocopy nocopy t fl =
        assert false

      (* serializes a list of function from a given manager *)
      let to_barray ?(nocopy=false) ?(destruct=false) (t:t) (fl:f list) : BTools.BArray.t =
        assert false

      (* unserializes a list of function into a given manager *)
      let br_fl ?(t=(None:(t option))) cha : t * f list =
        assert false

      (* serializes a list of function from a given manager *)
      let bw_fl ?(nocopy=false) ?(destruct=false) (t:t) cha (fl:f list) : unit =
        assert false

      let t_stats (t:t) : Tree.stree =
        assert false

      let f_stats (t:t) (fl:f list) : Tree.stree =
        assert false

      let clear_caches (t:t) : unit =
        assert false

      let keep_clean ?(normalize=false) (t:t) (fl: f list) : f list =
        assert false

      let check t f = true

      module F =
      struct
        type f' = {
          mutable alive : bool;
          file_name : string;
          arity : int;
          size : int; (* in bytes *)
          normalized : bool;
        }

        let arity_f = arity
        let arity f' = f'.arity
        let size  f' = f'.size

        let prefix = "biggy_"
        let suffix = ".mlbdd.F.pure"

        let of_f ?(nocopy=false) ?(normalize=true) (t:t) (f:f) : f' =
          assert false

        let to_f (t:t) (f':f') : f =
          assert false

        let free (f':f') : unit =
          assert false

        let tof ?(normalize=true) (cha:out_channel) (fl:f' list) : unit =
          assert false

        let off (cha:in_channel) : f' list =
          assert false
      end

      let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) (t:t) (cha:out_channel) (fl:f list) : unit =
        assert false

      let off (cha:in_channel) : t * (f list) =
        assert false

      (* Section 2. QEOPS level interface *)

      let to_string (t:t) (f:f) : string =
        "{ari = "^(string_of_int f.ari)^"; sem = ("^MLBDD.to_string f.sem^") }"

      let support (t:t) (f:f) : Support.t =
        f.sem
        |> MLBDD.support
        |> MLBDD.list_of_support
        |> Support.of_sorted_support f.ari

      (* [MOVEME:GuaCaml:MyList] *)
      let rec assoc_of_option_list ?(carry=[]) ?(i=0) (l:'a option list) : (int * 'a) list =
        match l with
        | [] -> List.rev carry
        | None::t -> assoc_of_option_list ~carry ~i:(succ i) t
        | (Some a)::t -> assoc_of_option_list ~carry:((i, a)::carry) ~i:(succ i) t

      let pevalA (t:t) (param:bool option list) (f:f) : f =
        assert(arity t f = List.length param);
        let subst = assoc_of_option_list param in
        let g = {ari = f.ari; sem = Snowflake.MlbddUtils.peval f.sem subst} in
        assert(arity t g = arity t f);
        g

      (* [MOVEME:GuaCaml:MyArray] *)
      let contract_bool_array (a:bool array) : int array =
        let n = Array.length a in
        let ai = Array.make n (-1) in
        let rec loop i j =
          if i < n then (
            if a.(i) then (
              ai.(i) <- j;
              loop (succ i) (succ j)
            )
            else
              loop (succ i)       j
          )
        in
        loop 0 0;
        ai

      let contract (t:t) (param:Support.t) (f:f) : f =
        let param = Support.to_bool_list param in
        assert(arity t f = List.length param);
        let p = contract_bool_array (Array.of_list param) in
        {ari = MyList.count_true param; sem = MLBDD.permute p f.sem}

      let pevalR (t:t) (param:bool option list) (f:f) : f =
        pevalA t param f
        |> contract t (param ||> Tools.isSome |> Support.of_bool_list)

      let forallA (t:t) (param:Support.t) (f:f) : f =
        let param = Support.to_bool_list param in
        assert(arity t f = List.length param);
        let supp = MyList.indexes_true param |> MLBDD.support_of_list in
        let g = {ari = f.ari; sem = MLBDD.forall supp f.sem} in
        assert(arity t g = arity t g);
        g

      (* [MOVEME:GuaCaml:Tools] *)
      let negb (b:bool) : bool = not b

      let forallR (t:t) (param:Support.t) (f:f) : f =
        forallA t param f
        |> contract t param

      let existsA t param f : f =
        neg t (forallA t param (neg t f))

      let existsR t param f : f =
        neg t (forallR t param (neg t f))

      (* Section 3. AQEOPS level interface *)

      let ( &!! ) (t:t) (ga:int) (fl:f list) : f =
        match fl with
        | [] -> (cst t true ga)
        | f0::fl -> (List.fold_left ((&!) t) f0 fl)

      let ( |!! ) (t:t) (ga:int) (fl:f list) : f =
        match fl with
        | [] -> (cst t false ga)
        | f0::fl -> (List.fold_left ((|!) t) f0 fl)

      let ( ^!! ) (t:t) (ga:int) (fl:f list) : f =
        match fl with
        | [] -> (cst t false ga)
        | f0::fl -> (List.fold_left ((^!) t) f0 fl)

      let ( =!! ) (t:t) (ga:int) (fl:f list) : f =
        match fl with
        | [] -> (cst t true ga)
        | f0::fl -> (List.fold_left ((=!) t) f0 fl)

      let builtin_o_prim _ = false (* return [true] iff [prim] is implemented within the computation model *)
      let detect_o_prim _ _ _ = assert false  (* counts the number of ordered [prim] primitives available *)
      let intro_o_prim _ _ _ _ = assert false (* push [n:int] ordered [prim] primitives *)
      let elim_o_prim _ _ _ _ = assert false  (* removes [n:int] ordered [prim] primitives *)
    end
  end
end

module ComposeOops(MO:OOPS.MSig) =
struct
  module GO = OOPS.Make(MO)

  type f0  = LDD.OOPS.Model.f
  type tt0 = LDD.OOPS.Model.t
  type t   = {
    pure : tt0;
    oman : MO.t;
    fa   : MO.f array;
    arity: int;
    hist : MO.f MLBDD.hist
  }

  let newman (pure:tt0) (oman:MO.t) (arity:int) (fa:MO.f array) : t =
    { pure; oman; fa; arity; hist = MLBDD.fold_init pure}

  let compute_doneg (t:t) (f:f0) : MO.f =
    MLBDD.fold_cont t.hist (function
      | MLBDD.False -> MO.cst t.oman false t.arity
      | MLBDD.True  -> MO.cst t.oman true  t.arity
      | MLBDD.Not f -> MO.cneg t.oman true f
      | MLBDD.If (f0, n, f1) -> GO.ite t.oman t.fa.(n) f0 f1) f.sem

  let compute (t:t) (fl:f0 list) : MO.f list =
    Tools.map (compute_doneg t) fl

  let translate (pure:tt0) (oman:MO.t) (fl:f0 list) : MO.f list =
    match MyList.get_onehash (LDD.OOPS.Model.arity pure) fl with
    | None -> []
    | Some ari -> (
      let fa = GO.array_make_n_var oman ari in
      let man = newman pure oman ari fa in
      compute man fl
    )
end

module ToSnax = ComposeOops(SnaxOops.OOPS.Model)

(* [NOTE] assumes that 'oU' is built in the computation model *)
module TranslateAqeops(Q:AQEOPS.MSig) =
struct
  let _ =
    if not(Q.builtin_o_prim AQEOPS.U)
    then failwith "[DAGaml.Ldd_mlbdd.TranslateAqeops] AQEOPS.U must be supported"

  module M = LDD.OOPS.Model

  type f0  = M.f
  type tt0 = M.t
  type t = {
    pure : tt0;
    oman : Q.t;
        (* (MLBDD.id, 'current index', 'global arity') -> Q.f *)
    memo : (int * int * int, Q.f) MemoTable.t;
  }

  let newman (pure:tt0) (oman:Q.t) : t = {pure; oman; memo = MemoTable.(create default_size)}

  let compute (t:t) (f:f0) : Q.f =
    let rec tra_rec (f:MLBDD.t) (now:int) (ari:int) : Q.f =
      assert(0 <= now && now <= ari);
      match MLBDD.inspect f with
      | MLBDD.False -> Q.cst t.oman false (ari-now)
      | MLBDD.True  -> Q.cst t.oman true  (ari-now)
      | MLBDD.Not f' -> Q.cneg t.oman true (tra_rec f' now ari)
      | MLBDD.If(f0, v, f1) -> (
        assert(now <= v && v < ari);
        let f'0 = tra_mem f0 (v+1) ari in
        let f'1 = tra_mem f1 (v+1) ari in
        Q.intro_o_prim t.oman AQEOPS.U (v-now) ( Q.( *! ) t.oman f'0 f'1 )
      )
    and     tra_mem (f:MLBDD.t) (now:int) (ari:int) : Q.f =
      MemoTable.apply t.memo (fun _ -> tra_rec f now ari) (MLBDD.id f, now, ari)
    in
    let f' = tra_rec f.M.sem 0 f.M.ari in
    assert(Q.arity t.oman f' = f.M.ari);
    f'

  let translate (pure:tt0) (oman:Q.t) (fl:f0 list) : Q.f list =
    let t = newman pure oman in
    fl ||> (compute t)

(*
  (* [DEBUG] *)
  module C = ComposeOops(Q)

  let translate (pure:tt0) (oman:Q.t) (fl:f0 list) : Q.f list =
    let t1 = translate pure oman fl in
    let t2 = C.translate pure oman fl in
    assert(List.for_all (fun (f1, f2) -> Q.eq oman f1 f2) (List.combine t1 t2));
    t1
 *)

end

module ImportAqeops(Q:AQEOPS.MSig) =
struct
  let _ =
    if not(Q.builtin_o_prim AQEOPS.U)
    then failwith "[DAGaml.Ldd_mlbdd.ImportAqeops] AQEOPS.U must be supported"

  module M = LDD.OOPS.Model

  type f0  = M.f
  type tt0 = M.t
  type t = {
    pure : tt0;
    oman : Q.t;
        (* (Q.f, 'current index') -> MLBDD.t *)
    memo : (barray * int, MLBDD.t) MemoTable.t;
  }

  let newman (pure:tt0) (oman:Q.t) : t = {pure; oman; memo = MemoTable.(create default_size)}

  let compute (t:t) (f:Q.f) : f0 =
    let rec tra_rec (f:Q.f) (now:int) : MLBDD.t =
      match Q.to_bool t.oman f with
      | Some cst -> if cst then MLBDD.dtrue t.pure else MLBDD.dfalse t.pure
      | None -> (
        let nU = Q.detect_o_prim t.oman AQEOPS.U f in
        let now = now + nU in
        let fU = Q.elim_o_prim t.oman AQEOPS.U nU f in
        let f0 = Q.cofactor t.oman false fU in
        let f1 = Q.cofactor t.oman true  fU in
        let f'0 = tra_mem f0 (now+1)
        and f'1 = tra_mem f1 (now+1) in
        MLBDD.ite f'0 now f'1
      )
    and     tra_mem (f:Q.f) (now:int) : MLBDD.t =
      MemoTable.apply t.memo (fun _ -> tra_rec f now) (Q.id t.oman f, now)
    in
    let sem = tra_rec f 0 in
    M.{sem; ari = Q.arity t.oman f}

  let translate (pure:tt0) (oman:Q.t) (fl:Q.f list) : f0 list =
    let t = newman pure oman in
    fl ||> (compute t)

(*
  (* [DEBUG] *)
  module C = ComposeOops(Q)

  let translate (pure:tt0) (oman:Q.t) (fl:f0 list) : Q.f list =
    let t1 = translate pure oman fl in
    let t2 = C.translate pure oman fl in
    assert(List.for_all (fun (f1, f2) -> Q.eq oman f1 f2) (List.combine t1 t2));
    t1
 *)
end

