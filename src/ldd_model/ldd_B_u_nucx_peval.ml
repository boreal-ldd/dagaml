(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_peval : Partial Evaluation
 *)

open GuaCaml
open AB
open Utils
open STools

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_io

let peval_rowA (red_pa:'c -> 'p -> 'a -> 'c) (init_pa:'c) (p:peval) (r:(unit, 'a) row) : 'c * (unit, 'a) row * peval =
  let rec loop red_pa cC cr' cp' p r =
    match p, r with
    | [], [] -> (cC, List.rev cr', List.rev cp')
    | [], _ | _, [] -> assert false
    | p0::p, r0::r -> (
      match p0, r0 with
      | None  , A() -> loop red_pa cC              ((A())::cr') ( None   ::cp') p r
      | None  , B b -> loop red_pa cC              ((B b)::cr')            cp'  p r
      | Some v, A() -> loop red_pa cC                      cr'  ((Some v)::cp') p r
      | Some v, B b -> loop red_pa (red_pa cC v b)         cr'             cp'  p r
    )
  in loop red_pa init_pa [] [] p r

let peval_rowU (p:peval) (r:rowU) : rowU * peval =
  let (), r, p = peval_rowA (fun () _ _ -> ()) () p r in
  (r, p)

(* (b, r, p) with [b] complementation state *)
let peval_rowX (p:peval) (r:rowX) : bool * rowX * peval =
  peval_rowA (fun a b () -> a <> b) false p r

(* (b, r, p) with [b] is true if condition is reached *)
let peval_rowC (p:peval) (r:rowC) : bool * rowC * peval =
  peval_rowA (fun a b if0 -> a || (b = if0)) false p r

let peval_matCX (p:peval) ?(neg=false) (mat:matCX) :
    (bool * matCX * peval, bool * matCX * int) ab =
  let rec aux carry (p:peval) neg mat =
    match mat with
    | [] -> (A (neg, xor_matCX neg (List.rev carry), p))
    | rowCX::matCX -> (
      match rowCX with
      | A rX -> (
        let x, rX', p' = peval_rowX p rX in
        aux ((A rX')::carry) p' (neg <> x) matCX
      )
      | B(th0, rC) -> (
        let c, rC', p' = peval_rowC p rC in
        if c
        then (B(neg <> th0, (xor_matCX (neg <> th0) (List.rev carry)), List.length rC'))
        else
          aux ((B(neg <> th0, rC'))::carry) p' neg matCX
      )
    )
  in aux [] p neg mat

let peval_edge (p:peval) (ee, nx) =
  if do_check then assert(check_block ee);
  let noutput = MyList.count Tools.isNone p in
  (* print_endline ("[peval_pedge] none:"^(ToS.bool(ee.noutput = noutput))); *)
  match ee.decomp with
  | C0 -> (
    assert(nx = Tree.GLeaf());
    ({ee with noutput}, Tree.GLeaf())
  )
  | Id i -> (
    assert(nx = Tree.GLeaf());
    match List.nth p i with
    | None -> (
      let i' = MyList.count Tools.isNone (fst(MyList.hdtl_nth i p)) in
      ({ee with noutput; decomp = Id i'}, Tree.GLeaf())
    )
    | Some p -> (
      ({ee with neg = p <> ee.neg; noutput; decomp = C0}, Tree.GLeaf())
    )
  )
  | UCX (rU, matCX) ->
  (
    (* print_endline ("[peval_edge] rU: "^(ToS.rowU rU)); *)
    (* print_endline ("[peval_edge] p: "^(STools.ToS.(list(option bool)) p)); *)
    let rU, p = peval_rowU p rU in
    (* print_endline ("[peval_edge] rU: "^(ToS.rowU rU)); *)
    (* print_endline ("[peval_edge] matCX: "^(ToS.matCX matCX)); *)
    match peval_matCX p ~neg:ee.neg matCX with
    | A(neg, mat, p) ->
    (
      (* print_endline ("[peval_edge] mat: "^(ToS.matCX mat)); *)
      (* print_endline ("[peval_edge] p: "^(ToS.peval p)); *)
      let ninput, pnx = match nx with
        | Tree.GLeaf () -> (None, Tree.GLeaf())
        | Tree.GLink lk -> (Some(MyList.count Tools.isNone p), Tree.GLink (p, lk))
      in
      let blk = {neg; noutput; ninput; decomp = UCX(rU, mat)} in
      (normalize_block blk, pnx)
    )
    | B(neg, mat, n') -> (
      (* print_endline ("[peval_edge] bmatUCX: "^(ToS.bmatUCX (neg, (rU, mat)))); *)
      (*print_endline ("[peval_edge] n': "^(STools.ToS.int n')); *)
      compose_bmatUCX_edge (neg, (rU, mat))
        (cst_edge false n')
    )
  )

let normalize_ppnext = function
  | Tree.GLeaf () -> Tree.GLeaf()
  | Tree.GLink (pC, (pc, lk)) -> Tree.GLink(Utils.compose_peval_opeval pC pc, lk)

let peval_pedge (p:peval) (pe:'lk pedge) : 'lk pedge =
  (* print_endline ("[peval_pedge] p: "^(STools.ToS.(list(option bool)) p)); *)
  (* print_endline ("[peval_pedge] pe: "^(ToS.pedge STools.ToS.ignore pe)); *)
  (* assert(List.length p = arity_edge pe); *)
  let ee, ppnx = peval_edge p pe in
  let ppe = (ee, normalize_ppnext ppnx) in
  (* print_endline ("[peval_pedge] ppe: "^(ToS.pedge STools.ToS.ignore ppe)); *)
  (* assert(MyList.count Tools.isNone p = arity_block ee); *)
  ppe

let peval_pnode peval ((), pedge0, pedge1) =
  ((), peval_pedge peval pedge0, peval_pedge peval pedge1)

let opeval_pnode = function
  | None -> (fun node -> node)
  | Some peval -> (fun node -> peval_pnode peval node)

let peval_pnodeC = function
  | [] -> assert false
  | None::peval -> (fun pnode -> MNode (peval_pnode peval pnode))
  | (Some false)::peval -> (fun ((), pedge0, _) -> MEdge (peval_pedge peval pedge0))
  | (Some true )::peval -> (fun ((), _, pedge1) -> MEdge (peval_pedge peval pedge1))

let select b ((), if0, if1) = if b then if1 else if0
