(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_facto : Shannon Factorization
 *)

open GuaCaml
open AB
open Utils

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_io

let facto_B (r1:(unit, 'a) row) (r2:(unit, 'a)row) : (unit, 'a) row * ((unit, 'a) row * (unit, 'a) row) =
  let rec aux carry carry1 carry2 = function
    | ([], []) ->
      (List.rev carry, (List.rev carry1, List.rev carry2))
    | ([], _) | (_, []) -> assert false
    | (h1::t1, h2::t2) ->
    ( match h1, h2 with
      | B b1, B b2 when b1 = b2 ->
        aux ((B b1)::carry) carry1 carry2 (t1, t2)
      | _, _ ->
        aux ((A ())::carry) (h1::carry1) (h2::carry2) (t1, t2)
    )
  in aux [] [] [] (r1, r2)

let facto_U : rowU -> rowU -> rowU * (rowU * rowU) = facto_B

(* assumes that rU1 inter rU2 is empty *)
let facto_alpha
  (rU1:rowU) (rA1:(unit, 'a)row)
  (rU2:rowU) (rA2:(unit, 'a)row) :
    (unit, 'a) row *
      ((rowU * (unit, 'a)row) * (rowU * (unit, 'a)row)) =
  let r1 = compose_A rU1 rA1 in
  let r2 = compose_A rU2 rA2 in
  let r, (r1', r2') = facto_B r1 r2 in
  let rU, rA = extract_A r in
  (* assert(listAB_allA rU); *)
  rA, (extract_A r1', extract_A r2')

let genU i n : rowU =
  MyList.ntimes ~carry:((B () )::MyList.make (n-i-1) (A())) (A()) i

let genX i n : rowX =
  MyList.ntimes ~carry:((B () )::MyList.make (n-i-1) (A())) (A()) i

let genC (if0:bool) i n : rowC =
  MyList.ntimes ~carry:((B if0)::MyList.make (n-i-1) (A())) (A()) i

let rowCX_genX i n : rowCX =
  A (genX i n)
let rowCX_genC (if0:bool) (th0:bool) i n : rowCX =
  B (th0, genC if0 i n)

let bmatUCX_genX i n bmatUCX =
  compose_rowCX_bmatUCX (rowCX_genX i n) bmatUCX

let bmatUCX_genC if0 th0 i n bmatUCX =
  compose_rowCX_bmatUCX (rowCX_genC if0 th0 i n) bmatUCX

let picknth l i =
  let rec aux carry i = function
    | [] -> None
    | h::t -> if i = 0
      then Some(h, (List.rev carry) @ t)
      else aux (h::carry) (pred i) t
  in
  assert(i >= 0);
  aux [] i l

let unify_Id_rowCX side b1 i1 n b2 (rowCX:rowCX) : bmatUCX option =
  match rowCX with
  | A rX ->
  (
    match picknth rX i1 with
    | Some(B(), rX') ->
      let rU = MyList.make (List.length rX') (A()) in
      Some(
        bmatUCX_genX (i1+1) (n+1)
          (bmatUCX_genC side b1 0 n
            (b2, (rU, [A rX'])))
      )
    | _ -> None
  )
  | B (th0, rC) ->
  (
    match picknth rC i1 with
    | Some(B if0, rC')
        when b1 <> b2 <> if0 <> th0 = false -> (
      let rU = MyList.make (List.length rC') (A()) in
      let th1 = b1 <> if0 in
      Some(
        bmatUCX_genC if0 th1 (i1+1) (n+1)
          (bmatUCX_genC side (not th1) 0 n
            (b2, (rU, [B(th0, rC')])))
      )
    )
    | _ -> None
  )

let facto_Id_matUCX_no_unify side b1 i1 n b2 (rU2, matCX2) noutput2 ninput2 nx2 =
  (* assert(List.length rU2 = noutput2); *)
  let rU1 = (MyList.make i1 (B())) @ [A()] @ (MyList.make (n-i1-1) (B())) in
  let rU, (rU1', rU2') = facto_U rU1 rU2 in
  let i' = Tools.unop (MyList.index isA rU1') in
  let nS = MyList.count isA rU in
  let blk = {
    neg = false;
    decomp = UCX((A())::rU, []);
    noutput = succ noutput2;
    ninput = Some(succ nS)
  }
  in
  let pi1 = {
    neg = false;
    decomp = Id i';
    noutput = nS;
    ninput = None;
  } in
  let blk2 = {
    neg = false;
    decomp = UCX(rU2', matCX2);
    noutput = nS;
    ninput = ninput2
  } in
  if side
  then ({blk with neg = b2}, MNode((), ({blk2 with neg = false}, nx2), ({pi1 with neg = b2 <> b1}, Tree.GLeaf())))
  else ({blk with neg = b1}, MNode((), ({pi1 with neg = false}, Tree.GLeaf()), ({blk2 with neg = b2 <> b1}, nx2)))

let facto_Id_matUCX side b1 i1 n b2 (rU2, matCX2) noutput2 ninput2 nx2 =
  match matCX2 with
  | [] ->
    facto_Id_matUCX_no_unify side b1 i1 n b2 (rU2, matCX2) noutput2 ninput2 nx2
  | rCX2::matCX2' ->
    let rCX2', rU2' = swap_rowU_rowCX rU2 rCX2 in
    match unify_Id_rowCX side b1 i1 n b2 rCX2' with
    | Some bmat -> (
      (normalize_matUCX (rU2', matCX2') ninput2, nx2)
      |> compose_bmatUCX_edge bmat
      |> merge_of_edge
    )
    | None ->
      facto_Id_matUCX_no_unify side b1 i1 n b2 (rU2, matCX2) noutput2 ninput2 nx2

let block_genX i n block =
  compose_rowCX_block (rowCX_genX i n) block

let block_genC if0 th0 i n block =
  compose_rowCX_block (rowCX_genC if0 th0 i n) block

let block_genU i n block =
  compose_rowU_block (genU i n) block

let solve_cons_id_id noutput b1 i1 b2 i2 =
  assert(i1 <> i2);
  let i, j = min i1 i2, max i1 i2 in
  let rU = [A()] @ (MyList.make i (B())) @
           [A()] @ (MyList.make (j-i-1) (B())) @
           [A()] @ (MyList.make (noutput-j-1) (B())) in
  (* assert(List.length rU = succ noutput); *)
  let i1', i2' = if i1 < i2 then (0, 1) else (1, 0) in
  let block = {neg = b1; noutput = succ noutput; ninput = Some 3; decomp = UCX(rU, [])} in
  let f0 = ({neg = false; noutput = 2; ninput = None; decomp = Id i1'}, Tree.GLeaf()) in
  let f1 = ({neg = b1 <> b2; noutput = 2; ninput = None; decomp = Id i2'}, Tree.GLeaf()) in
  (block, MNode ((), f0, f1))

let solve_cons_no_unify (f0: _ edge') (f1: _ edge') : _ emerge =
  let neg, f0' = extract_neg_from_edge f0 in
  let f1' = compose_neg_edge neg f1 in
  (id_block ~neg (succ (fst f0).noutput), MNode((), f0', f1'))

let rec solve_cons ((ee0, nx0) as f0) ((ee1, nx1) as f1) : _ emerge =
  assert(ee0.noutput = ee1.noutput);
  let noutput = ee0.noutput in
  if nx0 = nx1 && ee0.decomp = ee1.decomp
  then (merge_of_edge (if ee0.neg = ee1.neg
    then (block_genU 0 (succ noutput) ee0, nx0)
    else (block_genX 0 (succ noutput) ee0, nx0)))
  else match ee0.decomp, ee1.decomp with
  | C0, _ ->
    merge_of_edge (block_genC false ee0.neg 0 (succ noutput) ee1, nx1)
  | _, C0 ->
    merge_of_edge (block_genC true  ee1.neg 0 (succ noutput) ee0, nx0)
  | Id i0, Id i1 ->
    (solve_cons_id_id ee0.noutput ee0.neg i0 ee1.neg i1)
  | Id i0, UCX matUCX1 ->
    facto_Id_matUCX false ee0.neg i0 ee0.noutput ee1.neg matUCX1 ee1.noutput ee1.ninput nx1
  | UCX matUCX0, Id i1 ->
    facto_Id_matUCX true  ee1.neg i1 ee1.noutput ee0.neg matUCX0 ee0.noutput ee0.ninput nx0
  | UCX mat0, UCX mat1 -> (
    let rU01, (rU0, rU1) = facto_B (fst mat0) (fst mat1) in
    if listAB_allA rU01
    then (solve_cons_extract_rowCX f0 f1)
    else (
      let f0' = subst_matUCX_in_edge (rU0, snd mat0) f0
      and f1' = subst_matUCX_in_edge (rU1, snd mat1) f1 in
      compose_rowU_merge ((A())::rU01)
        (solve_cons_extract_rowCX f0' f1')
    )
  )
and     solve_cons_extract_rowCX (f0: _ edge') (f1 : _ edge') : _ emerge =
  match extract_rowCX_from_edge f0, extract_rowCX_from_edge f1 with
  | Some(A r0, f0'), Some(A r1, f1') -> (
    let r01, (r0, r1) = facto_B r0 r1 in
    if listAB_allA r01
    then (solve_cons_no_unify f0 f1)
    else (
      let f0'' : _ edge' = compose_rowCX_edge (A r0) f0'
      and f1'' : _ edge' = compose_rowCX_edge (A r1) f1' in
      compose_rowCX_merge (A((A())::r01)) (solve_cons f0'' f1'')
    )
  )
  | Some(B(th0, r0), f0'), Some(B(th1, r1), f1') when th0 = th1 -> (
    let r01, (r0, r1) = facto_B r0 r1 in
    if listAB_allA r01
    then (solve_cons_no_unify f0 f1)
    else (
      let f0'' = compose_rowCX_edge (B(th0, r0)) f0'
      and f1'' = compose_rowCX_edge (B(th0, r1)) f1' in
      compose_rowCX_merge (B(th0, ((A())::r01))) (solve_cons f0'' f1'')
    )
  )
  | _, _ -> (solve_cons_no_unify f0 f1)

let solve_cons f0 f1 =
  (* print_endline ("[solve_cons] f0: "^(ToS.edge' STools.ToS.ignore f0)); *)
  (* print_endline ("[solve_cons] f1: "^(ToS.edge' STools.ToS.ignore f1)); *)
  if do_check then assert(check_edge f0);
  if do_check then assert(check_edge f1);
  assert(arity_edge f0 = arity_edge f1);
  let f01 = solve_cons f0 f1 in
  (* print_endline ("[solve_cons] f01: "^(ToS.emerge STools.ToS.ignore f01)); *)
  if do_check then assert(check_merge f01);
  assert(arity_merge f01 = succ(arity_edge f0));
  f01
