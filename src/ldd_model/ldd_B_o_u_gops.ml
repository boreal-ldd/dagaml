(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-u : Uniform model with Useless variables
 *
 * module  : Ldd_o_u_gops : General OPeratorS
 *)

open GuaCaml

open Extra
open STools
open BTools
open O3Extra
open Ldd_B_o_u_types

(* Section 0. Preliminaries *)

let arity ((n, _), _) = n
let arity_node ((), e0, e1) =
  let a0 = arity e0 and a1 = arity e1 in
  assert(a0 = a1);
  (succ a0)
let pushU ?(n=1) ((x, y), nx) =
  assert(n>=0);
  ((n+x, y), nx)

(* Section 1. IO *)

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let uniq = int * int
  let pair = trio int int int

  let leaf b = bool b
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToBa =
struct
  open BTools.ToB

  let next' a (ident:'i tob) nx (s:stream) : stream =
    match nx with
    | Tree.GLeaf b  -> (assert(a = 0); bool b s)
    | Tree.GLink lk -> (assert(a > 0); ident lk s)

  let edge' a (ident:'i tob) ((x, y), nx) (s:stream) : stream =
    assert(x = a);
    assert(y <= x);
    int (x-y) (next' y ident nx s)

  let node' (ident:'i tob) (node:_ node') (s:stream) : stream =
    let a = arity_node node in
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) node s
end

module OfBa =
struct
  open BTools.OfB

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    if a = 0
    then (let b, s = bool s in (Tree.GLeaf b, s))
    else (let lk, s = ident a s in (Tree.GLink lk, s))

  let edge' a (ident:int -> 'i ofb) s : _ edge' * stream =
    let d, s = int s in
    assert(d <= a);
    let y = a - d in
    let nx, s = next' y ident s in
    (((a, y), nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

let pretty_edge (i, j) = (string_of_int i)^" -> "^(string_of_int j)

(* Section 2. Shannon *)

let cst b n =
  assert(n>=0);
  ((n, 0), Tree.GLeaf b)

let get_cst b f = cst b (arity f)
let is_cst ((x, y), nx) =
  match nx with
  | Tree.GLeaf b -> Some b
  | Tree.GLink _ -> None

let compose (xC, yC) ((xc, yc), i) =
  assert(yC = xc);
  ((xC, yc), i)

let node_pull ((x, y), i) =
  assert(x>=y);
  if x = y
  then
  (
    Utils.MNode (Utils.gnode_node i,
      fun ((), x', y') -> ((), x', y')
    )
  )
  else
  (
    let e = ((x-1, y), i) in
    Utils.MEdge ((), e, e)
  )

let solve_cons ((), (((xX, yX), iX) as edgeX), (((xY, yY), iY) as edgeY)) =
  assert(xX = xY);
  if edgeX = edgeY
  then (Utils.MEdge ((xX+1, yX), iX))
  else
  (Utils.MNode (
    (succ xX, succ xX),
    ((), ((xX, yX), iX), ((xY, yY), iY))
  ))

(* Section 3. Operators *)

let sort_node ((), e0, e1) =
  if e0 < e1 then ((), e0, e1) else ((), e1, e0)

let solve_and ((), (((xX, yX), iX) as edgeX), (((xY, yY), iY) as edgeY)) =
  assert(xX = xY);
  match iX with
  | Tree.GLeaf b -> Utils.MEdge(if b then (* x = 1 *) edgeY else (* x = 0 *) edgeX)
  | Tree.GLink nx ->
  match iY with
  | Tree.GLeaf b -> Utils.MEdge(if b then (* y = 1 *) edgeX else (* y = 0 *) edgeY)
  | Tree.GLink ny ->
  if (nx = ny) && (yX = yY)
  then Utils.MEdge edgeX
  else (
    let yXY = max yX yY in
    Utils.MNode ((xX, yXY), (sort_node ((), ((yXY, yX), iX), ((yXY, yY), iY))))
  )

let solve_xor ((), (((xX, yX), iX) as edgeX), (((xY, yY), iY) as edgeY)) =
  assert(xX = xY);
  match iX, iY with
  | Tree.(GLeaf bX   , GLeaf bY) -> Utils.MEdge ((xX, 0), Tree.GLeaf (bX<>bY))
  | Tree.(GLeaf false, GLink nY) -> Utils.MEdge edgeY
  | Tree.(GLink nX   , GLeaf false) -> Utils.MEdge edgeX
  | _, _ -> (
    if iX = iY then (Utils.MEdge (get_cst false edgeX))
    else (
      let yXY = max yX yY in
      Utils.MNode ((xX, yXY), (sort_node ((), ((yXY, yX), iX), ((yXY, yY), iY))))
    )
  )

let solve_node node = match solve_cons node with
  | Utils.MEdge (edge, next) -> edge, Utils.MEdge next
  | Utils.MNode (edge, node) -> edge, Utils.MNode node

let cons_and  node = match solve_and node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let cons_xor  node = match solve_xor node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let solve_of_tag = TacxTypes.(function
  | And  -> solve_and
  | Cons -> solve_cons
  | Xor  -> solve_xor)

let solve_tacx (tag, edge0, edge1) = match (solve_of_tag tag) ((), edge0, edge1) with
  | Utils.MEdge (edge, next) -> edge, Utils.MEdge next
  | Utils.MNode (edge, ((), edge0, edge1)) -> edge, Utils.MNode (tag, edge0, edge1)
