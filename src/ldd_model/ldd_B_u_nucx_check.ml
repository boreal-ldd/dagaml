(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx : Normalization Check
 *)

open GuaCaml
open AB
open Utils

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils

let do_check = false

(* only checks that the number of S matches from one layer to the next *)

let checkS_row nS row : int option =
  if MyList.count isA row = nS
  then Some(List.length row)
  else None

let checkS_rowCX nS rowCX : int option = match rowCX with
  | A     rX  -> checkS_row nS rX
  | B (_, rC) -> checkS_row nS rC

let checkS_matCX nS matCX : int option =
  List.fold_right (fun rCX op -> match op with
    | Some nS -> checkS_rowCX nS rCX
    | None    -> None) matCX (Some nS)

let checkS_matUCX nS (rU, mCX) : int option =
  match checkS_matCX nS mCX with
  | Some nS -> checkS_row nS rU
  | None -> None

let checkS_bmatUCX nS (_, d) : int option =
  checkS_matUCX nS d

let check_alternation_matCX matCX =
  List.fold_left (fun oplast rowCX ->
    match oplast, rowCX_type rowCX with
    | None, _ -> None
    | Some None, cx -> Some(Some cx)
    | Some(Some cx0), cx1 ->
      if cx0 = cx1 then None else Some(Some cx1)
    ) (Some None) matCX
  |> Tools.isSome

let check_block_next ee nx =
  match ee.ninput, nx with
  | None, Tree.GLeaf()
  | Some _, Tree.GLink _ -> true
  | _ -> false

let check_noutput_ninput ee =
  (Tools.unop_default 0 ee.ninput) <= ee.noutput

(* only apply when block.ninput = None *)
let check_last_matCX ninput matCX =
  match ninput with
  | Some _ -> true (* non-terminal edge *)
  | None -> (      (* terminal edge *)
    match MyList.opget_last matCX with
    | None -> false (* terminal edge with no(non-U) primitive => False *)
    | Some rowCX -> (
      (rowCX_countB rowCX >= 2) &&
      (rowCX_type rowCX <> B false)
    )
  )

let check_block (ee:block) : bool =
  let ninput = Tools.unop_default 0 ee.ninput in
  ninput <= ee.noutput &&
  match ee.decomp with
  | C0 -> true
  | Id x -> x < ee.noutput
  | UCX((rowU, matCX) as matUCX) -> (
    checkS_matUCX ninput matUCX = Some ee.noutput &&
    check_alternation_matCX matCX &&
    check_last_matCX ee.ninput matCX
  )

let check_edge ((ee, nx) : _ edge') : bool =
  check_block_next ee nx && check_block ee

(* add consistency with (and on) m3 segment *)
let check_merge (ee, m3) : bool =
  check_block ee &&
  match m3 with
  | MEdge _ -> true
  | MNode ((), f0, f1) -> check_edge f0 && check_edge f1

(* add consistency with (and on) m3 segment *)
let check_merge3 (ee, m3) : bool =
  check_block ee &&
  match m3 with
  | M3Edge _ -> true
  | M3Node ((), f0, f1) -> check_edge f0 && check_edge f1
  | M3Cons ((), f0, f1) -> check_edge f0 && check_edge f1
