(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-c10 : Ordered model with canalizing variables (C10 only).
 *
 * module  : Ldd_o_c10_gops : General OPeratorS
 *)

open GuaCaml
open Extra
open STools
open BTools
open Ldd_B_o_c10_types

(* Section 0. Preliminaries *)

let arity ((n, _), _) = n
let arity_node ((), e0, e1) =
  let a0 = arity e0 and a1 = arity e1 in
  assert(a0 = a1);
  (succ a0)

(* Section 1. IO *)

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let uniq = int * int
  let pair = trio int int int

  let leaf b = bool b
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = bool
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToBa =
struct
  open BTools.ToB

  let next' a (ident:'i tob) nx (s:stream) : stream =
    match nx with
    | Tree.GLeaf b  -> (assert(a = 0); bool b s)
    | Tree.GLink lk -> (assert(a > 0); ident lk s)

  let edge' a (ident:'i tob) ((x, y), nx) (s:stream) : stream =
    assert(x = a);
    assert(y <= x);
    int (x-y) (next' y ident nx s)

  let node' (ident:'i tob) (node:_ node') (s:stream) : stream =
    let a = arity_node node in
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) node s
end

module OfBa =
struct
  open BTools.OfB

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    if a = 0
    then (let b, s = bool s in (Tree.GLeaf b, s))
    else (let lk, s = ident a s in (Tree.GLink lk, s))

  let edge' a (ident:int -> 'i ofb) s : _ edge' * stream =
    let d, s = int s in
    assert(d <= a);
    let y = a - d in
    let nx, s = next' y ident s in
    (((a, y), nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

let pretty_of_edge (i, j) = (string_of_int i)^" -> "^(string_of_int j)

(* Section 2. Shannon *)

let solve_node ((), (((xX, yX), iX) as edgeX), (((xY, yY), iY) as edgeY)) =
  assert(xX = xY);
  match iY with
  | Tree.GLeaf false ->
  (
    assert(yY = 0);
    ((xX+1, yX), Utils.MEdge iX)
  )
  | _ ->
    ((xX+1, xX+1), Utils.MNode ((), edgeX, edgeY))

let compose (xC, yC) ((xc, yc), i) =
  assert(yC = xc);
  ((xC, yc), i)

let node_pull ((x, y), i) =
  assert(x>=y);
  if x = y
  then
  (
    match i with
    | Tree.GLeaf _ -> assert false
    | Tree.GLink nd -> (
      Utils.MNode (nd, fun node -> node)
    )
  )
  else
  (
    let zero = ((x-1, 0), Tree.GLeaf false)
    and path = ((x-1, y), i) in
    Utils.MEdge ((), path, zero)
  )

let solve_and ((), (((xX, yX), iX) as fX), (((xY, yY), iY) as fY)) =
  assert(xX = xY);
  match fX, fY with
  | ((_, Tree.GLeaf false) as f), _
  | _, ((_, Tree.GLeaf false) as f)
  | ((0, _), Tree.GLeaf true), f
  | f, ((0, _), Tree.GLeaf true) ->
    (Utils.MEdge f)
  | _ -> if fX = fY then (Utils.MEdge fX) else
  (
    let xx = max yX yY in
    assert(xx > 0);
    Utils.MNode ((xX, xx), ((), ((xx, yX), iX), ((xx, yY), iY)))
  )

let solve_or ((), (((xX, yX), iX) as fX), (((xY, yY), iY) as fY)) =
  assert(xX = xY);
  match fX, fY with
  | (_, Tree.GLeaf b0), (_, Tree.GLeaf b1) ->
    (Utils.MEdge ((xX, 0), Tree.GLeaf(b0||b1)))
  | (_, Tree.GLeaf false), f
  | f, (_, Tree.GLeaf false)
  | (((0, _), Tree.GLeaf true) as f), _
  | _, (((0, _), Tree.GLeaf true) as f) ->
    (Utils.MEdge f)
  | _ ->
  (
    let xx = max yX yY in
    assert(xx > 0);
    Utils.MNode ((xX, xx), ((), ((xx, yX), iX), ((xx, yY), iY)))
  )

let solve_xor ((), (((xX, yX), iX) as fX), (((xY, yY), iY) as fY)) =
  assert(xX = xY);
  match fX, fY with
  | (_, Tree.GLeaf false), f
  | f, (_, Tree.GLeaf false) -> (Utils.MEdge f)
  | ((0, _), Tree.GLeaf bX), ((0, _), Tree.GLeaf bY) ->
    Utils.MEdge((0, 0), Tree.GLeaf (bX<>bY))
  | _ -> if fX = fY
  then (Utils.MEdge((xX, 0), Tree.GLeaf false))
  else (Utils.MNode((xX, xX), ((), fX, fY)))

let solve_iff ((), (((xX, yX), iX) as fX), (((xY, yY), iY) as fY)) =
  assert(xX = xY);
  match fX, fY with
  | ((0, _), Tree.GLeaf bX), ((0, _), Tree.GLeaf bY) ->
    Utils.MEdge((0, 0), Tree.GLeaf (bX=bY))
  | _ -> (
    Utils.MNode((xX, xX), ((), fX, fY))
  )

let cons_and  node = match solve_and node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let cons_or  node = match solve_or node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let cons_xor node = match solve_xor node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let cons_iff node = match solve_iff node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node
