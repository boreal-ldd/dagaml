(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2020-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : Uniform model with Negation and Useless variables
 *   Negation, Useless variables, Xor variables, Canalizing variables
 *
 * module  : Ldd_u_nucx_avanced : Generic Functional Interface
 *)

open GuaCaml
open BTools
open Ldd_B_u_nucx
open Extra

module ToS =
struct
  module Model =
  struct
    module M = LDD.G0

    type extra = unit
    type param = unit

    type xnode = string
    type xedge = string

    let map_edge (():extra) (():param) (ee : _ M.M.edge') : xedge =
      ThisI.ToS.edge' (fun lk -> lk ()) ee

    let map_node (():extra) (():param) (nd : _ M.M.node') : xnode =
      ThisI.ToS.node' (fun lk -> lk ()) nd
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-CntSat manager, defined for [CntSat] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : string =
    Module.rec_edge tt () f
end

module CntSat =
struct
  open BTools
  module Model =
  struct
    module M = LDD.G0
    open BArray.Nat

    type extra = unit
    type param = unit

    type xnode = int * nat
    type xedge = int * nat

    let map_next : _ -> xedge = function
      | Tree.GLeaf () -> (0, zero())
      | Tree.GLink lk -> lk ()

    let push_neg ((a, cnt):xedge) : xedge =
      (a, (pow2 a) -/ cnt)

    let push_cneg b (xe:xedge) : xedge =
      if b then push_neg xe else xe

    let push_U nU ((a, cnt):xedge) : xedge =
      assert(nU >= 0);
      (nU+a, shift_lefti cnt nU)

    let push_C th0 nC ((a, cnt):xedge) : xedge =
      assert(nC >= 0);
      if th0
      then (nC+a, (pow2 (nC+a) -/ pow2 a) +/ cnt)
      else (nC+a, cnt)

    let rec push_matCX (a:int) (matCX:ThisT.matCX) nx : xedge =
      match matCX with
      | [] -> map_next nx
      | rCX::matCX' -> (
        match rCX with
        | A rX -> (
          assert(not(AB.listAB_allA rX));
          (a, pow2 (a-1))
        )
        | B(th0, rC) -> (
          let a' = AB.listAB_countA rC in
          let nC = AB.listAB_countB rC in
          push_C th0 nC (push_matCX a' matCX' nx)
        )
      )

    let push_matUCX (a:int) ((rU, matCX):ThisT.matUCX) nx : xedge =
      let a' = AB.listAB_countA rU in
      let nU = AB.listAB_countB rU in
      push_U nU (push_matCX a' matCX nx)

    let push_decomp (a:int) (decomp:ThisT.decomp) nx : xedge =
      match decomp with
      | ThisT.C0 -> (a, zero())
      | ThisT.Id _ -> (a, pow2 (a-1))
      | ThisT.UCX matUCX -> push_matUCX a matUCX nx

    let map_edge (():extra) (():param) ((blk, nx): _ M.M.edge') : xedge =
      push_cneg blk.neg (push_decomp blk.noutput blk.decomp nx)

    let ( +// ) (ax, cx) (ay, cy) =
      assert(ax = ay);
      (ax, cx +/ cy)

    let map_node (():extra) (():param) ((_, f0, f1): _ M.M.node') : xnode =
      (map_edge () () f0) +// (map_edge () () f1)
  end

  module Module = AriUbdag.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.t (* LDD-CntSat manager, defined for [CntSat] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.t) : tt =
    Module.makeman ldd_man () hsize
  let newman (ldd_man:LDD.t) : tt = makeman ldd_man

  let solve (tt:tt) (f:LDD.f) : BNat.nat =
    Module.rec_edge tt () f |> snd
end

(* Forall Relative Naming : eliminate quantified variables from the ordering *)
module ForallR =
struct
  module Model =
  struct
    module M = LDD.G1

    type extra = {
      ldd_man   : LDD.And.t;  (* LDD manager *)
      and_man   : LDD.And.tt; (* And manager *)
    }

    type param = bool list (* Utils.quant *)
    (* false => non-quantified variable
       true  =>     quantified variable *)

    type xedge = LDD.f
    type xnode = xedge

    type solve = param -> xedge -> xedge

    let map_next (extra:extra) (solve:solve) (param:param) a neg nx : xedge =
      assert(List.length param = a);
      match nx with
      | Tree.GLeaf () -> cst neg 0
      | _ -> (
        let p0 = MyList.count_false param in (* number of non quantified variables *)
        let p1 = MyList.count_true  param in (* number of quantified variables *)
        if p0 = 0
        then cst false 0
        else (
          let ee = ThisG.id_edge ~neg a nx in
          if p1 = 0 (* all remaining variables are not quantified *)
          then ee
          else (solve param ee)
        )
      )

    let rec map_matCX (extra:extra) (solve:solve) (param:param) a neg (matCX:ThisT.matCX) ni nx : xedge =
      match matCX with
      | [] -> map_next extra solve param a neg nx
      | rCX :: matCX' -> (
        match rCX with
        | A rX -> (
          if ThisQ.forall_rowX param rX
          then cst false (MyList.count_false param)
          else solve param (ThisG.edge_of_matCX ~neg a matCX ni nx)
        )
        | B(th0, rC) -> (
          let cond, rC', param' = ThisQ.forall_rowC param rC in
          if cond && (neg <> th0 = false)
          then (
            cst false (MyList.count_false param)
          )
          else (
            let a' = AB.listAB_countA rC in
            ThisG.compose_rowCX (B(neg <> th0, rC'))
              (map_matCX extra solve param' a' neg matCX' ni nx)
          )
        )
      )

    let map_matUCX (extra:extra) (solve:solve) (param:param) a neg ((rU, matCX):ThisT.matUCX) (ni:int option) nx : xedge =
      let rU', param' = ThisQ.forall_rowU param rU in
      let a' = AB.listAB_countA rU in
      ThisG.compose_rowU rU'
        (map_matCX extra solve param' a' neg matCX ni nx)

    let map_edge (extra:extra) (solve:solve) (param:param) (((blk, nx) as ee): _ ThisT.edge') : xedge =
      if List.for_all (fun b -> b = false) param
      then ee
      else (
        assert(ThisG.arity ee = List.length param);
        match blk.decomp with
        | ThisT.C0 -> ThisQ.forall_C0 param blk.neg blk.noutput
        | ThisT.Id k -> ThisQ.forall_Id param blk.neg blk.noutput k
        | ThisT.UCX matUCX -> (
          map_matUCX extra solve param blk.noutput blk.neg matUCX blk.ninput nx
        )
    )

    let map_node (extra:extra) (solve:solve) (param:param) (((_, f0, f1) as nd): _ ThisT.node') : xnode =
      assert(List.length param = ThisG.arity_node nd);
      match param with
      | [] -> assert false
      | b0::param' -> (
        let g0 = map_edge extra solve param' f0
        and g1 = map_edge extra solve param' f1 in
        assert(ThisG.arity g0 = ThisG.arity g1);
        if b0
        (* b0 = true  : eliminate the variable *)
        then (LDD.And.solve extra.and_man g0 g1)
        (* b0 = false : keep the variable *)
        else (LDD.G1.push extra.ldd_man ((), g0, g1))
      )
  end

  module Module = AriUbdagTC.BottomUp_NOC(Model)

  type t = LDD.t (* LDD manager, defined by this file *)
  type tt = Module.tt (* LDD-Forall manager, defined for [Forall] *)

  let dump_stats (t:tt) = Module.dump_stats t
  let makeman ?(hsize=MemoBTable.default_size) (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt =
    let extra = Model.{ldd_man; and_man} in
    let man = Module.makeman ldd_man ~hsize extra in
    man
  let newman (ldd_man:LDD.And.t) (and_man:LDD.And.tt) : tt = makeman ldd_man and_man

  let solve (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    Module.rec_edge tt param f

  let solveA (tt:tt) (param:bool list) (f:LDD.f) : LDD.f =
    let su = param ||> (function false -> AB.A() | true -> AB.B()) in
    ThisG.compose_rowU su (solve tt param f)
end

module OOPS =
struct
  type tt = {
    man_oops : LDD.OOPS.Model.t;
    man_tos : ToS.tt;
    man_forallr : ForallR.tt;
    profile : OProfile.t;
  }

  let get_man_ldd tt =
    LDD.get_cons tt.man_oops

  type ff = LDD.f

  let makeman ?man_oops () =
    let man_oops = match man_oops with
      | Some man_oops -> man_oops
      | None         -> LDD.newman()
    in
    let man_ldd = LDD.get_cons man_oops in
    let man_and = LDD.get_mand man_oops in
    let man_tos = ToS.newman man_ldd in
    let man_forallr = ForallR.newman man_ldd man_and in
    let profile = OProfile.newman ~prefix:"[OOPS]" () in
    {
      man_oops;
      man_tos; man_forallr;
      profile
    }

  module Model =
  struct
    open LDD.OOPS.Model
    type t = tt
    type f = ff

    let newman () = makeman ()

    let print_profile (t:t) : unit =
      OProfile.stop_other t.profile;
      (* LDD.And.print_profile t.man_and; *)
      OProfile.print t.profile;
      ()

    (* Section 0. OOPS forward definition *)
      (* sub-section 1. Functional / Propositional Aspects *)
    let ( ->> ) t bl f = ( ->> ) t.man_oops bl f
    let arity  t f = arity t.man_oops f
    let   cneg t b f = cneg t.man_oops b f
    let    neg t f = cneg t true f
    let ( &! ) t f1 f2 = ( &! ) t.man_oops f1 f2
    let ( |! ) t f1 f2 = ( |! ) t.man_oops f1 f2
    let ( ^! ) t f1 f2 = ( ^! ) t.man_oops f1 f2
    let ( =! ) t f1 f2 = ( =! ) t.man_oops f1 f2
    let ( *! ) t f0 f1 = ( *! ) t.man_oops f0 f1

    let cst t b n = cst t.man_oops b n
    let var t b n k = var t.man_oops b n k
    let to_bool t f = to_bool t.man_oops f

    let cofactor t b f = cofactor t.man_oops b f
    let id t f = id t.man_oops f
    let eq t f0 f1 = eq t.man_oops f0 f1

      (* sub-section 2. Manager Management *)
    let copy_into t fl t' = copy_into t.man_oops fl t'.man_oops

    let to_barray ?(nocopy=false) ?(destruct=false) t fl =
      to_barray ~nocopy ~destruct t.man_oops fl

    let of_barray ?(t=None) ba =
      let t, fl = of_barray ~t:(Tools.opmap (fun t -> t.man_oops) t) ba in
      (makeman ~man_oops:t (), fl)

    let bw_fl ?(nocopy=false) ?(destruct=false) t =
      bw_fl ~nocopy ~destruct t.man_oops

    let br_fl ?(t=None) cha =
      let t, fl = br_fl ~t:(Tools.opmap (fun t -> t.man_oops) t) cha in
      (makeman ~man_oops:t (), fl)

    (* [FIXME] *)
    let t_stats t = t_stats t.man_oops

    let f_stats t fl = f_stats t.man_oops fl

    (* [FIXME] *)
    let clear_caches t =
      clear_caches t.man_oops

    let keep_clean ?(normalize=false) t fl =
      clear_caches t;
      keep_clean ~normalize t.man_oops fl

    let check t f = check t.man_oops f

    (* Section 3. File-based Management *)

    module F =
    struct
      type f' = F.f'

      let arity = F.arity
      let size  = F.size

      let of_f ?(nocopy=false) ?(normalize=true) t f =
        F.of_f ~nocopy ~normalize t.man_oops f

      let to_f t f' =
        F.to_f t.man_oops f'

      let free f' = F.free f'

      let tof = F.tof
      let off = F.off
    end

    let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) t =
      tof ~nocopy ~normalize ~destruct t.man_oops
    let off cha =
      let t, fl = off cha in
      (makeman ~man_oops:t (), fl)

    (* Section 2. QEOPS level interface *)

    let to_string (t:t) (f:f) : string =
      ToS.solve t.man_tos f

    let support (t:t) (f:f) : Support.t =
      let l = ThisG.support_of_edge f in
      assert(List.length l <= arity t f);
      Support.of_bool_list l

    let pevalA (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = LDD.PEvalR.solveA (LDD.get_man_peval t.man_oops) param f in
      assert(arity t g = arity t f);
      g

    let pevalR (t:t) (param:bool option list) (f:f) : f =
      assert(arity t f = List.length param);
      let g = LDD.PEvalR.solve (LDD.get_man_peval t.man_oops) param f in
      assert(arity t g = MyList.count_None param);
      g

    let forallA (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_bool_list param in
      assert(arity t f = List.length param);
      (* let g0 = ForallA0.solve t.man_foralla0 param f in *)
      let g = ForallR.solveA t.man_forallr param f in
      (*
      if not (LDD.G1.(=/) g0 g)
      then (
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : begin";
        print_endline "[Ldd_G_o_u.OOPS.forallA] g0";
        print_string (to_string t g0);
        print_endline "[Ldd_G_o_u.OOPS.forallA] g";
        print_string (to_string t g);
        print_endline "[Ldd_G_o_u.OOPS.forallA] [error] generational inconsistency : end";
        assert false
      );
      *)
      assert(arity t g = arity t f);
      g

    let forallR (t:t) (param:Support.t) (f:f) : f =
      let param = Support.to_bool_list param in
      assert(arity t f = List.length param);
      let g = ForallR.solve t.man_forallr param f in
      assert(arity t g = MyList.count_false param);
      g

    let existsA t param f : f =
      neg t (forallA t param (neg t f))

    let existsR t param f : f =
      neg t (forallR t param (neg t f))

    (* Section 3. AQEOPS level interface *)

    let ( &!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((&!) t) f0 fl)

    let ( |!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((|!) t) f0 fl)

    let ( ^!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t false ga)
      | f0::fl -> (List.fold_left ((^!) t) f0 fl)

    let ( =!! ) (t:t) (ga:int) (fl:f list) : f =
      match fl with
      | [] -> (cst t true ga)
      | f0::fl -> (List.fold_left ((=!) t) f0 fl)

    let builtin_o_prim _ = false (* return [true] iff [prim] is implemented within the computation model *)
    let detect_o_prim _ _ _ = assert false  (* counts the number of ordered [prim] primitives available *)
    let intro_o_prim _ _ _ _ = assert false (* push [n:int] ordered [prim] primitives *)
    let elim_o_prim _ _ _ _ = assert false  (* removes [n:int] ordered [prim] primitives *)
  end

  let newman = Model.newman
  let print_profile = Model.print_profile

  include OOPS.Make(Model)

  module SUInt = SUInt.Make(Model)
  module VUInt = VUInt.Make(Model)
  module LoadCnf0 = LoadCnf.Make0(Model)
  module LoadCnfA = LoadCnf.MakeA(Model)
  module LoadCnfQ = LoadCnf.MakeQ(Model)
  module CoSTreD = AQEOPS_CoSTreD.Make(Model)
  module CoSTreD_CNF = AQEOPS_CoSTreD.MakeCNF(Model)
  module CoSTreD_CNF_D4 = AQEOPS_CoSTreD.MakeCNF_D4(Model)
  module CCoSTreD = AQEOPS_CCoSTreD.Make(Model)
  module CCoSTreD_CNF = AQEOPS_CCoSTreD.MakeCNF(Model)
  module WCoSTreD = AQEOPS_CoSTreD_ArgMax_vUInt.Make(Model)
end

module CSTypes = Constraint_system_types
module CSUtils = Constraint_system_utils
module CSCoSTreD  = Constraint_system_costred

module SNAX = SnaxOops.OOPS.Model

module CoSTreD_Model : CSCoSTreD.MSig =
struct
  module Q = OOPS.Model

  module BddToSnax = ComposeOops(SNAX)
  let translate_pure_to_snax (pure:Q.t) (snax:SNAX.t) (fl:Q.f list) : SNAX.f list =
      let pure = OOPS.get_man_ldd pure in
      BddToSnax.translate pure snax fl

  let default_module_name = default_module_name
end

module CoSTreD_Module = CSCoSTreD.Make(CoSTreD_Model)
