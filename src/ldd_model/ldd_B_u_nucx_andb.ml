(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-u-nucx : The Uniform model of Order 1, includes :
 *
 * module  : Ldd_u_nucx_and : Binary Operator, And
 *
 * === NOTES ===
 *
 * LDD-U-NUCX solver/factorizer for And (Boolean Conjunction)
 *   => uses propagation of partial evaluation
 *)

open GuaCaml
open AB
open Utils
open STools

open Ldd_B_u_nucx_types
open Ldd_B_u_nucx_utils
open Ldd_B_u_nucx_check
open Ldd_B_u_nucx_norm
open Ldd_B_u_nucx_facto
open Ldd_B_u_nucx_peval
open Ldd_B_u_nucx_pull
open Ldd_B_u_nucx_cons
open Ldd_B_u_nucx_io

let solve_and_id_id noutput b0 x0 b1 x1 =
  if x0 = x1
  then if b0 = b1
    then merge3_of_edge ({neg = b0; noutput; ninput = None; decomp = Id x0}, Tree.GLeaf())
    else cst_merge3 false noutput
  else
  (
    let rU = MyList.init noutput (fun i -> if i = x0 || i = x1 then A() else B()) in
    let rC = if x0 < x1 then [B b0; B b1] else [B b1; B b0] in
    let decomp = UCX (rU, [B(true, rC)]) in
    merge3_of_edge ({neg = true; noutput; ninput = None; decomp}, Tree.GLeaf())
  )

(* pe0 = (neg0, Id x0) /\ pe1 *)
let solve_and_id_pedge arity neg0 x0 pedge1 =
  (* print_endline ("[solve_and_id_pedge] arity: "^(STools.ToS.int arity)); *)
  (* print_endline ("[solve_and_id_pedge] neg0: "^(STools.ToS.bool neg0)); *)
  (* print_endline ("[solve_and_id_pedge] x0: "^(STools.ToS.int x0)); *)
  (* print_endline ("[solve_and_id_pedge] pedge1: "^(ToS.edge' STools.ToS.ignore pedge1)); *)
  let peval = MyList.init arity (fun i -> if i = x0 then (Some(not neg0)) else None)
  and rowC0 = MyList.init arity (fun i -> if i = x0 then (B        neg0 ) else A() ) in

  (* print_endline ("[solve_and_id_pedge] peval: "^(STools.ToS.(list(option bool)) peval)); *)
  (* print_endline ("[solve_and_id_pedge] rowC0: "^(ToS.rowC rowC0)); *)
  pedge1
  |> peval_pedge peval
  (* |> Tools.tee (fun f -> print_endline ("[solve_and_id_pedge] f0: "^(ToS.pedge STools.ToS.ignore f))) *)
  |> compose_rowCX_edge (B(false, rowC0))
  (* |> Tools.tee (fun f -> print_endline ("[solve_and_id_pedge] f1: "^(ToS.pedge STools.ToS.ignore f))) *)
  |> merge3_of_edge

let solve_and_rowC0 rU rC0 =
  let rC0, rU = swap_ablA_ablB rU rC0 in
  let peval = peval_of_rowC0 rC0 in
  (peval, rU)

let solve_andb_rowUC0_rowUC0  (r1:rowUC) (r2:rowUC) : (rowUC * (rowU * peval) * (rowU * peval)) option =
  let rec loop (c12:rowUC) (c1:rowU) (p1:peval) (c2:rowU) (p2:peval) (r1:rowUC) (r2:rowUC) =
    match r1, r2 with
    | [], [] -> Some List.(rev c12, (rev c1, rev p1), (rev c2, rev p2))
    | [], _ | _, [] -> assert false
    | x1::r1, x2::r2 -> (
      match x1, x2 with
      (* U, U *)
      | B(A()), B(A()) -> (
        loop ((B(A()))::c12) c1 p1 c2 p2 r1 r2
      )
      (* C, C *)
      | B(B if1), B(B if2) -> (
        if if1 = if2
        then loop ((B(B if1))::c12) c1 p1 c2 p2 r1 r2
        else None
      )
      (* C, U | U, C *)
      | B(B if0), B(A())
      | B(A())  , B(B if0) -> (
        loop ((B(B if0))::c12) c1 p1 c2 p2 r1 r2
      )
      (* C, S *)
      | B(B if1), A ()  -> (
        loop ((B(B if1))::c12) c1 p1 c2 ((Some(not if1))::p2) r1 r2
      )
      (* S, C *)
      | A()  , B(B if2) -> (
        loop ((B(B if2))::c12) c1 ((Some(not if2))::p1) c2 p2 r1 r2
      )
      | _, _ -> (
        let x1' = match x1 with A() -> A() | B(A()) -> B() | B(B _) -> assert false in
        let x2' = match x2 with A() -> A() | B(A()) -> B() | B(B _) -> assert false in
        loop ((A())::c12) (x1'::c1) (None::p1) (x2'::c2) (None::p2) r1 r2
      )
    )
  in loop [] [] [] [] [] r1 r2

let solve_andb_rowC0_rowC0  (r1:rowC) (r2:rowC) : (rowC * peval * peval) option =
  let rec loop (c12:rowC) (p1:peval) (p2:peval) (r1:rowC) (r2:rowC) =
    match r1, r2 with
    | [], [] -> Some List.(rev c12, rev p1, rev p2)
    | [], _ | _, [] -> assert false
    | x1::r1, x2::r2 -> (
      match x1, x2 with
      (* C, C *)
      | B if1, B if2 -> (
        if if1 = if2
        then loop ((B if1)::c12) p1 p2 r1 r2
        else None
      )
      (* C, S *)
      | B if1, A ()  -> (
        loop ((B if1)::c12) p1 ((Some(not if1))::p2) r1 r2
      )
      (* S, C *)
      | A()  , B if2 -> (
        loop ((B if2)::c12) ((Some(not if2))::p1) p2 r1 r2
      )
      | A(), A() -> (
        loop ((A())::c12) (None::p1) (None::p2) r1 r2
      )
    )
  in loop [] [] [] r1 r2

let rec solve_and ((eeX, nxX) as peX) ((eeY, nxY) as peY) =
  (* print_endline ("[solve_and] peX: "^(ToS.edge' STools.ToS.ignore peX)); *)
  (* print_endline ("[solve_and] peY: "^(ToS.edge' STools.ToS.ignore peY)); *)
  assert(eeX.noutput = eeY.noutput);
  match peX, eeX.decomp, peY, eeY.decomp with
  | peX, C0, peY, _
  | peY, _ , peX, C0 ->
    (merge3_of_edge (if (fst peX).neg then peY else peX))
  | _, Id iX, _, Id iY -> (solve_and_id_id eeX.noutput eeX.neg iX eeY.neg iY)
  | peX, Id iX, peY, _
  | peY, _    , peX, Id iX -> solve_and_id_pedge eeX.noutput (fst peX).neg iX peY
  | _, UCX matX, _, UCX matY -> (
    let tX = get_type_matUCX eeX.neg matX in
    let tY = get_type_matUCX eeY.neg matY in
    if nxX = nxY && matX = matY
    then (if eeX.neg = eeY.neg then (merge3_of_edge peX) else (cst_merge3 false eeX.noutput))
    else
      let rUXY, (rUX, rUY) = facto_B (fst matX) (fst matY) in
      if listAB_allA rUXY
      then (solve_and_extract_rowCX tX peX tY peY)
      else (
        let peX' = subst_matUCX_in_edge (rUX, snd matX) peX
        and peY' = subst_matUCX_in_edge (rUY, snd matY) peY in
        compose_rowU_merge3 rUXY (solve_and_extract_rowCX tX peX' tY peY')
      )
  )
and      solve_and_extract_rowCX tX peX tY peY =
  match tX, tY with
  | Some(B false), Some(B false) -> (
    match extract_rowCX_from_edge peX with
    | Some(B(false, rX), peX') -> (
      match extract_rowCX_from_edge peY with
      | Some(B(false, rY), peY') -> (
        match solve_andb_rowC0_rowC0 rX rY with
        | Some(rXY, pX, pY) -> (
          let fX = peval_pedge pX peX' in
          let fY = peval_pedge pY peY' in
          compose_rowCX_merge3 (B(false, rXY)) (solve_and fX fY)
        )
        | None -> (cst_merge3 false (fst peX).noutput)
      )
      | _ -> assert false
    )
    | _ -> assert false
  )
  | Some(B false), _ -> (
    match extract_rowCX_from_edge peX with
    | Some(B(false, rX), peX') -> (
      let fY = peval_pedge (peval_of_rowC0 rX) peY in
      compose_rowCX_merge3 (B(false, rX)) (solve_and peX' fY)
    )
    | _ -> assert false
  )
  | _, Some(B false) -> (
    match extract_rowCX_from_edge peY with
    | Some(B(false, rY), peY') -> (
      let fX = peval_pedge (peval_of_rowC0 rY) peX in
      compose_rowCX_merge3 (B(false, rY)) (solve_and peY' fX)
    )
    | _ -> assert false
  )
  | Some(B true), Some(B true) -> (
    match extract_rowCX_from_edge peX, extract_rowCX_from_edge peY with
    | Some(B(true, rX), peX'), Some(B(true, rY), peY') -> (
      let rXY, (rX', rY') = facto_B rX rY in
      compose_rowCX_merge3 (B(true, rXY))
        (match rX', rY' with
          | (B false)::rX'', (B true )::rY'' -> (
            let fX = compose_rowCX_edge (B(true, rX'')) peX'
            and fY = compose_rowCX_edge (B(true, rY'')) peY' in
            merge3_cons fY fX
          )
          | (B true )::rX'', (B false)::rY'' -> (
            let fX = compose_rowCX_edge (B(true, rX'')) peX'
            and fY = compose_rowCX_edge (B(true, rY'')) peY' in
            merge3_cons fX fY
          )
          | _, _ -> (
            let fX = compose_rowCX_edge (B(true, rX')) peX'
            and fY = compose_rowCX_edge (B(true, rY')) peY' in
            if listAB_allA rXY then merge3_node fX fY else solve_and fX fY
          )
        )
    )
    | _ -> assert false
  )
  | _, _ -> merge3_node peX peY

let solve_and ((eeX, nxX) as peX) ((eeY, nxY) as peY) =
  assert(arity_edge peX = arity_edge peY);
  (* print_endline ("[solve_and] peX: "^(ToS.pedge STools.ToS.ignore peX)); *)
  if do_check then assert(check_edge peX);
  (* print_endline ("[solve_and] peY: "^(ToS.pedge STools.ToS.ignore peY)); *)
  if do_check then assert(check_edge peY);
  let peXY = solve_and peX peY in
  (* print_endline ("[solve_and] peXY: "^(ToS.pemerge3 STools.ToS.ignore peXY)); *)
  assert(arity_merge3 peXY = arity_edge peY);
  if do_check then assert(check_merge3 peXY);
  peXY
