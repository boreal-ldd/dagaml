(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml     : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model  : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-nucx : The Ordered model of Order 1, includes :
 *   Negation, Useless Variables, Xor variables, Canalizing variables.
 *
 * module  : Ldd_o_nucx : Model Root
 *
 * === NOTES ===
 *
 * Implementation of LDD-O-NUCX
 * Overall time  : 2h44min
 * Specific time : 1h52min
 *   [+2h40 rewritting normalization]
 *   [+1h30 rewritting using absorbing leaves]
 *   [+0h30 unification of IO Section, added ToBa/OfBa]
 *   [+2h00 writting advanced (Support, CntSat, Forall{R,A}, PEvall{R,A})]
 *)

open GuaCaml
open Extra
open STools
open BTools
open O3Extra
module ThisT = Ldd_B_o_nucx_types
module ThisG = Ldd_B_o_nucx_gops

let arity    = ThisG.arity
let neg      = ThisG.neg
let cneg     = ThisG.cneg
let is_cst   = ThisG.is_cst
let cst      = ThisG.cst
let get_cst  = ThisG.get_cst

module LDD =
struct
  module Model =
  struct
    type leaf = ThisT.leaf
    type edge = ThisT.edge
    type node = ThisT.node

    let string_of_leaf = ThisG.ToS.leaf
    let string_of_edge = ThisG.ToS.edge
    let string_of_node = ThisG.ToS.node

    type 'i next' = ('i, leaf) Tree.gnext
    type 'i edge' = edge * 'i next'
    type 'i node' = node * 'i edge' * 'i edge'

    let iob_leaf = ThisG.IoB.leaf
    let iob_edge = ThisG.IoB.edge
    let iob_node = ThisG.IoB.node

    let iob_next' = ThisG.IoB.next'
    let iob_edge' = ThisG.IoB.edge'
    let iob_node' = ThisG.IoB.node'

    let arity_node = ThisG.arity_node
    let arity_edge = ThisG.arity

    let toba_node' = ThisG.ToBa.node'
    let ofba_node' = ThisG.OfBa.node'

    let __check_reverse__ = false
  end

  module M1 =
  struct
    module M = Model

    let arity     = ThisG.arity
    let compose   = ThisG.compose
    let push_node = ThisG.solve_node
    let pull_node = ThisG.node_pull
  end

  include AriUbdagTC.STDIO(M1)

  module TO_DOT =
  struct
    module Model =
    struct
      module M = G0

      let string_of_leaf lf = ("L"^(ToS.int lf))

      let string_of_pos = function
        | None       -> "black"
        | Some false -> "red"
        | Some true  -> "green"

      let string_of_edge pos edge =
        "[label = \""^(ThisG.pretty_of_edge edge)^"\"; color=\""^(string_of_pos pos)^"\"];"
      let string_of_node () = ""
    end

    include AriUbdag.TO_DOT(Model)

    let stree_to_file stree file =
      let pure, edges = of_stree stree in
      dotfile pure edges file
  end

  let stree_to_dot = TO_DOT.stree_to_file
  let dotfile = TO_DOT.dotfile

  let stree_to_stats stree =
    let man, edges = of_stree stree in
    [G1.dump_estats man edges]

(* [TODO]
  module PEvalR =
  struct
    module MOD =
    struct
      module M = G1

      type peval = Utils.peval

      let iob_peval = IoB.bool_option_list

      type next = (peval option * M.G.ident

      type next' = next M.M.M.next'
      type edge' = next M.M.M.edge'
      type node' = next M.M.M.node'

      let eval_edge : peval -> edge' -> edge' = ThisG.peval_pedge
      let eval_node : peval -> node' -> (edge', node') Utils.merge = ThisG.peval_pnodeC
    end
    include AriUbdagTC.PEvalR_CACHED(MOD)

    let solve (tt:tt) (param:bool option list) (f:f) : f =
      map_edge tt param f

    let solveA (tt:tt) (param:bool option list) (f:f) : f =
      let su = param ||> (function None -> AB.A() | Some _ -> AB.B()) in
      ThisG.compose_rowU su (solve tt param f)
  end
 *)

  module And =
  struct
    module MOD =
    struct
      module M = G1
      let solver = ThisG.cons_and
    end
    include AriUbdagTC.BINOP_CACHED(MOD)
  end

  module Xor =
  struct
    module MOD =
    struct
      module M = G1
      let solver = ThisG.cons_xor
    end
    include AriUbdagTC.BINOP_CACHED(MOD)
  end

  let mask (man:G1.manager) : _ * (bool list -> G0.edge' -> G0.edge') =
    let mem, apply = MemoTable.(make default_size) in
    let rec map_rec (mask, f) =
      assert(MyList.count_true mask = M1.arity f);
      match is_cst f with
      | Some b -> cst b (List.length mask)
      | _ -> if List.for_all (fun x -> x) mask
        then f
        else (apply map_fun (mask, f))
    and     map_fun (mask, f) =
      match mask with
      | [] -> assert false
      | false::mask' -> (
        let f' = map_rec (mask', f) in
        let f'' = G1.push man ((), f', f') in
        assert(M1.arity f'' = List.length mask);
        f''
      )
      | true::mask' -> (
        let (), f0, f1 = G1.pull man f in
        let f0' = map_rec (mask', f0)
        and f1' = map_rec (mask', f1) in
        let f'' = G1.push man ((), f0', f1') in
        assert(M1.arity f'' = List.length mask);
        f''
      )
    in
    (mem, (fun mask f -> map_rec (mask, f)))

  type t = G1.manager
  type f = G0.edge'

  type manager = {
    cons : G1.manager;
    and_ : And.manager;
    xor_ : Xor.manager;
    mask_ : (bool list * G0.edge', G0.edge') MemoTable.t;
    solve_cons : G0.node' -> G0.edge';
    solve_and  : G0.node' -> G0.edge';
    solve_xor  : G0.node' -> G0.edge';
    apply_mask : bool list -> G0.edge' -> G0.edge';
  }

  let get_cons man = man.cons
  let get_mand man = man.and_
  let push man = man.solve_cons

  let default_makeman_hsize = G0.default_hsize

  let makeman ?(cons=None) ?(hsize=default_makeman_hsize) () : manager =
    let cons = match cons with None -> G1.makeman hsize | Some cons -> cons in
    let and_ = And.makeman cons hsize
    and xor_ = Xor.makeman cons hsize in
    let solve_cons = G1.push cons
    and solve_and  = And.map and_
    and solve_xor  = Xor.map xor_ in
    let mask_, apply_mask = mask cons in
    {
      cons; and_; xor_; mask_;
      solve_cons; solve_and; solve_xor;
      apply_mask
    }

  let newman () = makeman ()

  let clear_caches (man:manager) : unit =
    And.clear man.and_;
    Xor.clear man.xor_;
    MemoTable.clear man.mask_;
    ()

  (* Section. Serialization *)

  module BW :
  sig
    val bw_leaf : Model.leaf bw
    val bw_edge : Model.edge bw
    val bw_node : Model.node bw

    val bw_next' : 'lk G0.bw_next'
    val bw_edge' : 'lk G0.bw_edge'
    val bw_node' : 'lk G0.bw_node'
  end =
  struct
    let bw_leaf = ThisG.ToBStream.leaf
    let bw_edge = ThisG.ToBStream.edge
    let bw_node = ThisG.ToBStream.node

    let bw_next' = ThisG.ToBStream.next'
    let bw_edge' = ThisG.ToBStream.edge'
    let bw_node' = ThisG.ToBStream.node'
  end

  module BR :
  sig
    val br_leaf : Model.leaf br
    val br_edge : Model.edge br
    val br_node : Model.node br

    val br_next' : 'lk G0.br_next'
    val br_edge' : 'lk G0.br_edge'
    val br_node' : 'lk G0.br_node'
  end =
  struct
    let br_leaf = ThisG.OfBStream.leaf
    let br_edge = ThisG.OfBStream.edge
    let br_node = ThisG.OfBStream.node

    let br_next' = ThisG.OfBStream.next'
    let br_edge' = ThisG.OfBStream.edge'
    let br_node' = ThisG.OfBStream.node'
  end

  (* remark : BRE is only required for pseudo-canonical models, e.g. l-nnux*)

  let bw_node_list man fl =
    G0.bw_node_list BW.bw_node'             man fl
  let bw_edge_list man fl =
    G0.bw_edge_list BW.bw_node' BW.bw_edge' man fl

  let br_node_list man =
    G0.br_node_list BR.br_node'             man
  let br_edge_list man =
    G0.br_edge_list BR.br_node' BR.br_edge' man

  let barray_of_edge_list man fl =
    barray_of_bw (bw_edge_list man) fl
  let edge_list_of_barray man ba =
    barray_of_br (br_edge_list man) ba

  module OOPS =
  struct
    module Model =
    struct
      type t = manager
      type f = G0.edge'

      let arity _ f = M1.arity f
      let ( ->> ) t mask f =
        assert(M1.arity f = MyList.count_true mask);
        let f' = t.apply_mask mask f in
        assert(M1.arity f' = List.length mask);
        f'

      let cneg _ = ThisG.cneg
      let neg _ = ThisG.neg

      let ( *! ) t f0 f1 =
        assert(M1.arity f0 = M1.arity f1);
        let f' = t.solve_cons ((), f0, f1) in
        assert(M1.arity f' = succ(M1.arity f0));
        f'

      let ( &! ) t f0 f1 =
        assert(M1.arity f0 = M1.arity f1);
        let f' = t.solve_and ((), f0, f1) in
        assert(M1.arity f' = M1.arity f0);
        f'
      let ( ^! ) t f0 f1 =
        assert(M1.arity f0 = M1.arity f1);
        let f' = t.solve_xor ((), f0, f1) in
        assert(M1.arity f' = M1.arity f0);
        f'
      let ( |! ) t f0 f1 =
        neg t ((&!) t (neg t f0) (neg t f1))
      let ( =! ) t f0 f1 =
        neg t ((^!) t f0 f1)

      let cst _ b n = cst b n
      let var t b n k =
        let k' = n-k-1 in
        assert(k'>=0);
        let idk' = ( *! ) t (cst t b k') (cst t (not b) k') in
        let var = ThisG.push_U ~n:k idk' in
        assert(M1.arity var = n);
        var

      let to_bool _ f = ThisG.is_cst f

      let cofactor (t:t) (b:bool) (f:f) : f =
        let ((), f0, f1) = G1.pull t.cons f in
        if b then f1 else f0

      (* returns an identifier which can be used safely for memoization purpose *)
      let id (t:t) (f:f) : BTools.barray =
        fst G0.o3b_edge' f

      let eq (t:t) (f0:f) (f1:f) : bool = f0 = f1

      let newman = newman

      (* copies a list of function from a given manager into another one *)
      let copy_into (t0:t) (fl0:f list) (t1:t) : f list =
        G1.copy_list_into t0.cons t1.cons fl0

      (* unserializes a list of function into a given manager *)
      let of_barray ?(t=(None:(t option))) (ba:BTools.BArray.t) : t * f list =
        let t', fl' = G0.of_barray ba in
        match t with
        | Some t -> (t, G1.copy_list_into t' t.cons fl')
        | None   -> (makeman ~cons:(Some t') (), fl')

      let do_nocopy nocopy t fl =
        if nocopy
        then (t, fl)
        else (let t' = G1.newman() in (t', G1.copy_list_into t t' fl))

      (* serializes a list of function from a given manager *)
      let to_barray ?(nocopy=false) ?(destruct=false) (t:t) (fl:f list) : BTools.BArray.t =
        let t', fl' = do_nocopy nocopy t.cons fl in
        G0.to_barray ~destruct:(destruct||(not nocopy)) t' fl'

      (* unserializes a list of function into a given manager *)
      let br_fl ?(t=(None:(t option))) cha : t * f list =
        let t', fl' = G0.br cha in
        match t with
        | Some t -> (t, G1.copy_list_into t' t.cons fl')
        | None   -> (makeman ~cons:(Some t') (), fl')

      (* serializes a list of function from a given manager *)
      let bw_fl ?(nocopy=false) ?(destruct=false) (t:t) cha (fl:f list) : unit =
        let t', fl' = do_nocopy nocopy t.cons fl in
        G0.bw ~destruct:(destruct||(not nocopy)) t' cha fl'

      let t_stats (t:t) : Tree.stree =
        G1.dump_stats t.cons

      let f_stats (t:t) (fl:f list) : Tree.stree =
        G1.dump_estats t.cons fl

      let clear_caches : t -> unit = clear_caches

      let keep_clean ?(normalize=false) (t:t) (fl: f list) : f list =
        clear_caches t;
        G0.keep_clean ~normalize t.cons fl

      let check t f = G0.traverse t.cons f

      module F =
      struct
        type f' = {
          mutable alive : bool;
          file_name : string;
          arity : int;
          size : int; (* in bytes *)
          normalized : bool;
        }

        let arity_f = arity
        let arity f' = f'.arity
        let size  f' = f'.size

        let prefix = "biggy_"
        let suffix = ".o-nucx.F.pure"

        let of_f ?(nocopy=false) ?(normalize=true) (t:t) (f:f) : f' =
          (* assert(G0.traverse t.cons f); (* [DEBUG] *) *)
          let file_name = Filename.temp_file prefix suffix in
          let cha = open_out_bin file_name in
          G0.ToF.man_edges ~nocopy ~normalize t.cons cha [f];
          close_out cha;
          Gc.compact();
          let size = Unix.((stat file_name).st_size) in
          {alive = true; file_name; arity = arity_f t f; size; normalized = normalize}

        let to_f (t:t) (f':f') : f =
          assert(f'.alive);
          let cha = open_in_bin f'.file_name in
          let man, fl = G0.OfF.man_edges cha in
          assert(List.length fl = 1);
          let f = G1.copy_into man t.cons (List.hd fl) in
          (* assert(G0.traverse t.cons f); *)
          f

        let free (f':f') : unit =
          assert(f'.alive);
          f'.alive <- false;
          Sys.remove f'.file_name;
          ()

        let tof ?(normalize=true) (cha:out_channel) (fl:f' list) : unit =
          assert(List.for_all (fun f -> f.alive) fl);
          let fa = fl ||> (fun f -> open_in_bin f.file_name) |> Array.of_list in
          G0.combine_layerized_file ~normalize fa cha;
          Array.iter close_in fa;
          ()

        let off (cha:in_channel) : f' list =
          let man, edges = G0.OfF.man_edges cha in
          let t = makeman ~cons:(Some man) () in
          MyList.map (fun f -> of_f t f) edges
      end

      let tof ?(nocopy=false) ?(normalize=true) ?(destruct=false) (t:t) (cha:out_channel) (fl:f list) : unit =
        let copy = (not nocopy) || (normalize && (not destruct)) in
        let t', fl' = do_nocopy (not copy) t.cons fl in
        let destruct = destruct || copy in
        G0.ToF.man_edges ~normalize ~destruct t' cha fl'

      let off (cha:in_channel) : t * (f list) =
        let t, fl = G0.OfF.man_edges cha in
        (makeman ~cons:(Some t) (), fl)
    end
    module LoadCnfA = LoadCnf.MakeA(Model)
  end

  module OOPS_GC =
  struct
    module Model = OopsGcOfOops.Make(OOPS.Model)
    module LoadCnfA = LoadCnf.MakeA(Model)
  end
end

module ComposeOops(MO:OOPS.MSig) =
struct
  module GO = OOPS.Make(MO)

  module Model =
  struct
    module M = LDD.G0

    type extra = {
      oman : MO.t;
      fa   : MO.f array;
      arity: int;
    }
    type xnode = int * MO.f
    type xedge = int * MO.f

    let get_var t b i =
      MO.cneg t.oman b t.fa.(i)

    let map_next (t:extra) = function
      | Tree.GLeaf n -> (n, MO.cst t.oman false t.arity)
      | Tree.GLink lk -> lk()

    let intro_Xi (t:extra) f i =
      MO.(^!) t.oman (get_var t false i) f

    (* [TODO] check propositional transformation of C_{x, y, i} *)
    (* [Coq:BasicBool.b]
    Lemma Cxy_to_circuit2 (x0 if0 th0 fx:bool) :
      (if (eqb x0 if0) then th0 else fx) =
          (if th0 then orb  (xorb (negb if0) x0) fx
                  else andb (xorb (     if0) x0) fx
          ).
    Proof.
    dest_bool.
    Qed.
     *)
    let intro_CXYi (t:extra) f if0 th0 i =
      if th0
      then (MO.(|!) t.oman (get_var t (not if0) i) f)
      else (MO.(&!) t.oman (get_var t      if0  i) f)

    let rec intro_X (t:extra) ((n0, f0):xedge) nX =
      assert(nX >= 0);
      if nX = 0 then (n0, f0)
      else (
        let f1 = intro_Xi t f0 n0 in
        intro_X t (succ n0, f1) (pred nX)
      )

    let rec intro_C (t:extra) ((n0, f0):xedge) nC if0 th0 =
      assert(nC >= 0);
      if nC = 0 then (n0, f0)
      else (
        let f1 = intro_CXYi t f0 if0 th0 n0 in
        intro_C t (succ n0, f1) (pred nC) if0 th0
      )

    let intro_U (t:extra) ((n0, f0):xedge) nU : xedge =
      assert(nU >= 0);
      (nU+n0, f0)

    let rec map_tlist (t:extra) ((n0, f0):xedge) : ThisT.tlist -> xedge =
      ThisG.(
        function
        | [] -> (n0, f0)
        | (t1, n1)::tl -> (
          let n0, f0 = map_tlist t (n0, f0) tl in
          match t1 with
          | U ->
            (n1+n0, f0)
          | X ->
            intro_X t (n0, f0) n1
          | C(if0, th0) ->
            intro_C t (n0, f0) n1 if0 th0
        )
      )

    let cneg t b ((n, f):xedge) : xedge = (n, MO.cneg t.oman b f)

    let map_edge (t:extra) ((n, b, tl), nx) : xedge =
      let fnx = map_next t nx in
      let fee = map_tlist t fnx tl in
      assert(fst fee = n);
      cneg t b fee

    let map_node (t:extra) (((), ee0, ee1):(unit -> xnode) M.M.node') : xnode =
      assert(arity ee0 = arity ee1);
      let n0, f0 = map_edge t ee0 in
      let n1, f1 = map_edge t ee1 in
      assert(n0 = n1);
      (succ n0, GO.ite t.oman (get_var t false n0) f0 f1)

    (* [TODO] define a new map_edge to perform variable order reversion *)
  end

  module Module = AriUbdag.EXPORT_NOC(Model)

  type t   = Module.manager
  type f0  = LDD.G0.edge'
  type tt0 = LDD.G1.manager

  let newman (pure:tt0) (oman:MO.t) (fa:MO.f array) (arity:int) : t =
    Module.newman pure Model.{oman; fa = MyArray.rev fa; arity}

  let compute (man:t) (fl:f0 list) : MO.f list =
    Tools.map (fun f -> Module.rec_edge man f |> snd) fl

  let translate (pure:tt0) (oman:MO.t) ?man (fl:f0 list) : MO.f list =
    match MyList.get_onehash arity fl with
    | None -> []
    | Some len -> (
      let man =
        match man with
        | Some man -> (
          let extra = Module.get_extra man in
          assert(Array.length extra.Model.fa = len);
          man
        )
        | None -> (
          let vars = GO.array_make_n_var oman len in
          newman pure oman vars len
        )
      in
      compute man fl
    )
end

module ToSnax = ComposeOops(SnaxOops.OOPS.Model)

module OfExpr = OfExprOfOops.Make(LDD.OOPS.Model)

let default_module_name = "ONucxBPure"

let to_verilog fileA fileB : unit =
  let sys = OfExpr.GO.system_off fileA in
  let pure = sys.IoTypes.man.LDD.cons in
  let edges = Array.to_list sys.IoTypes.sset ||> snd in
  let snax = Snax.Module.G0.newman() in
  let snax_edges =
    ToSnax.translate (LDD.G1.export pure) snax edges
  in
  ConvUtils.snaxoops_to_verilog snax snax_edges default_module_name fileB

let ldd_of_expr ?(out_list=false) ?(smart=true) expr : string OfExpr.GO.system =
  OfExpr.ldd_of_expr ~out_list ~smart expr

let of_verilog ?(out_list=false) ?(smart=true) fileA fileB : unit =
  OfExpr.pure_of_verilog ~out_list ~smart fileA fileB

let of_pla ?(out_list=false) ?(smart=true) fileA fileB : unit =
  OfExpr.pure_of_pla ~out_list ~smart fileA fileB

let from_cnf ?(out_list=false) ?(smart=true) fileA fileB =
  OfExpr.pure_of_cnf ~out_list ~smart fileA fileB

let sys_to_stats fileA =
  let sys = OfExpr.GO.system_off fileA in
  let pure = sys.IoTypes.man.LDD.cons in
  let edges = Array.to_list sys.IoTypes.sset ||> snd in
  [LDD.G1.dump_estats pure edges]

let sys_to_dot fileA fileB =
  let sys = OfExpr.GO.system_off fileA in
  let pure = sys.IoTypes.man.LDD.cons in
  let edges = Array.to_list sys.IoTypes.sset ||> snd in
  LDD.TO_DOT.dotfile pure edges fileB
