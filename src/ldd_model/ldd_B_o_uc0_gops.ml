(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2019-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-uc0 : Ordered model with Useless variables and Canalizing variables (C10 and C00 only)
 *
 * module  : Ldd_o_uc0_gops : General OPeratorS
 *)

open GuaCaml
open Extra
open STools
open BTools
open Ldd_B_o_uc0_types

(* Section 0. Preliminaries *)

let arity ((n, _), _) = n
let arity_node ((), e0, e1) =
  let a0 = arity e0 and a1 = arity e1 in
  assert(a0 = a1);
  (succ a0)

(* Section 1. IO *)

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let tag = function
    | U -> "U"
    | C0 b -> "C0("^(ToS.bool b)^")"

  let tlist = list (tag * int)
  let uniq = int * tlist
  let pair = trio int tlist tlist

  let leaf = bool * int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let tag t s =
    match t with
    | U -> false::s
    | C0 b -> true::b::s

  let tlist = list(tag * int)
  let uniq = int * tlist
  let pair = trio int tlist tlist

  let leaf = bool * int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let tag = function
    | false::s -> (U, s)
    | true::b::s -> (C0 b, s)
    | _ -> assert false

  let tlist = list(tag * int)
  let uniq = int * tlist
  let pair = trio int tlist tlist

  let leaf = bool * int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let tag cha = function
    | U -> (bool cha false)
    | C0 if0 -> (bool cha true; bool cha if0)

  let tlist = list(tag * int)
  let uniq = int * tlist
  let pair = trio int tlist tlist

  let leaf = bool * int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let tag cha = match bool cha with
    | false -> U
    | true -> C0(bool cha)

  let tlist = list(tag * int)
  let uniq = int * tlist
  let pair = trio int tlist tlist

  let leaf = bool * int
  let edge = uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToBa =
struct
  open BTools.ToB

  let tag (p:tag) (tag:tag) (s:stream) : stream =
    assert(p <> tag);
    match p, tag with
    | U, U -> assert false
    | U, C0 b -> b::s
    | C0 _, U -> false::s
    | C0 _, C0 _ -> true::s

  (* implemented by (length, liste) instead of unary encoding *)
  let rec tlist_rec (a:int) (p:tag) (tl:tlist) (s:stream) : stream =
    match tl with
    | [] -> s
    | (hd, k)::tl ->
      (tag p hd (int (pred k) (tlist_rec (a-k) hd tl s)))

  let tlist (a:int) (tl:tlist) (s:stream) : stream =
    let n = List.length tl in
    let s =
      match tl with
      | [] ->
        (assert(n = 0); s)
      | (hd, k)::tl -> (
        assert(n > 0);
        assert(k > 0);
        (ToB.tag hd (int (pred k) (tlist_rec (a-k) hd tl s)))
      )
    in
    int n s

  let next' a (otag:tag option) (ident:'i tob) (nx:_ next') (s:stream) : stream =
    ToB.next' ident nx s

  (* side = function false -> 0 | true  -> 1 *)
  let edge' a (ident:'i tob) (((n, tl), nx) : _ edge') (s:stream) : stream =
    assert(n = a);
    let d = MyList.map_sum snd tl in
    let otag = match MyList.opget_last tl with
      | Some (t, _) -> Some t
      | None -> None
    in
    tlist a tl (next' (a-d) otag ident nx s)

  let node' (ident:'i tob) (node: _ node') (s:stream) : stream =
    let a = arity_node node in
    trio unit
      (edge' (pred a) ident)
      (edge' (pred a) ident) node s
end

module OfBa =
struct
  open BTools.OfB

  let tag (p:tag) (s:stream) : tag * stream =
    let b, s = bool s in
    match p with
    | U -> (C0 b, s)
    | C0 c0 -> if b then (C0(not c0), s) else (U, s)

  let next' a (otag:tag option) (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    OfB.next' (ident a) s

  let rec tlist_rec a length (p:tag) (ident:int -> 'i ofb) (s:stream) : (tlist * _ next') * stream =
    if length = 0
    then (
      let nx, s = next' a (Some p) ident s in
      (([], nx), s)
    )
    else (
      let tag, s = tag p s in
      let k', s = int s in
      let k = succ k' in
      let (tl', nx), s = tlist_rec (a-k) (pred length) tag ident s in
      (((tag, k)::tl', nx), s)
    )

  let tlist a (ident:int -> 'i ofb) (s:stream) : (tlist * _ next') * stream =
    let n, s = int s in
    if n = 0
    then (
      let nx, s = next' a None ident s in
      (([], nx), s)
    )
    else (
      let hd, s = OfB.tag s in
      let k', s = int s in
      let k = succ k' in
      let (tl', nx), s = tlist_rec (a-k) (pred n) hd ident s in
      (((hd, k)::tl', nx), s)
    )

  let edge' a (ident:int -> 'i ofb) (s:stream) : _ edge' * stream =
    let (tl, nx), s = tlist a ident s in
    (((a, tl), nx), s)

  let node' a (ident:int -> 'i ofb) (s:stream) : _ node' * stream =
    trio unit (edge' (pred a) ident) (edge' (pred a) ident) s
end

let char_of_tag : _ -> char = function
  | U -> 'U'
  | C0 false -> '1'
  | C0 true  -> '0'

let pretty_of_tlist tl : string =
  let pretty_of_elem (t, n) =
    String.make n (char_of_tag t)
  in
  "["^(SUtils.catmap "" pretty_of_elem tl)^"]"

let pretty_of_edge e : string = STools.ToS.(pair int pretty_of_tlist) e

(* Section 2. Shannon *)

let is_cst ((a, tl), nx) =
  match tl, nx with
  | [], Tree.GLeaf (b, n) -> Some b
  | _ -> None

let cst b n = ((n, []), Tree.GLeaf(b, n))
let arity ((a, tl), nx) = a
let get_cst b f = cst b (arity f)

let push0_tlist ?(n=1) c tl =
  assert(n >= 1);
  match tl with
  | [] -> [(c, n)]
  | (c', n')::tl' ->
    if c = c'
    then ((c', n+n')::tl')
    else ((c , n   )::tl )

let push_U ?(n=1) (((a, tl), nx) as f) =
  assert(n >= 1);
  match is_cst f with
  | Some b -> cst b (n+a)
  | None -> ((n+a, push0_tlist ~n U tl), nx)

let push_C0 ?(n=1) if0 (((a, tl), nx) as f) =
  assert(n >= 1);
  match is_cst f with
  | Some false -> cst false (n+a)
  | _ -> ((n+a, push0_tlist ~n (C0 if0) tl), nx)

let push_ttag (t, n) f =
  if n = 0
  then f
  else
  match t with
  | U -> push_U ~n f
  | C0 if0 -> push_C0 ~n if0 f

let push_tag ?(n=1) t f = push_ttag (t, n) f

let medge_of_edge (ee, nx) = (ee, Utils.MEdge nx)

let solve_node (((), f0, f1):'lk node') =
  let a0 = arity f0 and a1 = arity f1 in
  assert(a0 = a1);
  if f0 = f1
  then push_tag U f0 |> medge_of_edge
  else
  match is_cst f0 with
  | Some false -> push_tag (C0 false) f1 |> medge_of_edge
  | _ ->
  match is_cst f1 with
  | Some false -> push_tag (C0 true ) f0 |> medge_of_edge
  | _ ->
    ((succ a0, []), Utils.MNode ((), f0, f1))

let compose (aC, tlC) (((ac, tlc), i) as fc) =
  assert(MyList.map_sum snd tlc <= ac);
  assert(MyList.map_sum snd tlC + ac = aC);
  let fC = List.fold_right push_ttag tlC fc in
  assert(arity fC = aC);
  fC

let normalize ((a, tl), nx) =
  let len_tl = MyList.map_sum snd tl in
  assert(len_tl <= a);
  let fnx = ((a - len_tl, []), nx) in
  let f' = compose (a, tl) fnx in
  assert(arity f' = a);
  f'

let pull_tlist ((a, tl), nx) =
  assert(a>0);
  match tl with
  | [] -> (
    match nx with
    | Tree.GLeaf(b, n) -> (
      (if n <= 0 then failwith "[ldd_o_uc0_gops/pull_tlist] consistency error");
      Utils.MEdge (U, ((pred a, []), Tree.GLeaf(b, pred n)))
    )
    | Tree.GLink lk -> (Utils.MNode lk)
  )
  | ((c', n')::tl') -> (
    assert(n' >= 1);
    let tl'' =
      if n' = 1
      then tl'
      else (c', pred n')::tl'
    in
    Utils.MEdge(c', ((pred a, tl''), nx))
  )

let node_pull (((a, tl), nx) as f) =
  assert(a>=1);
  match pull_tlist f with
  | Utils.MEdge(c, e) -> (
    match c with
    | U -> Utils.MEdge((), e, e)
    | C0 b ->
      let cst0 = get_cst false e in
      if b
      then (Utils.MEdge((), e, cst0))
      else (Utils.MEdge((), cst0, e))
  )
  | Utils.MNode lk -> Utils.MNode(lk, fun node -> node)

(* Section 3. Operators *)

let solve_and ((), (((xX, tlX), iX) as fX), (((xY, tlY), iY) as fY)) =
  assert(xX = xY);
  match is_cst fX with
  | Some b  ->
    (Utils.MEdge(if b then fY else fX))
  | None -> match is_cst fY with
    | Some b ->
      (Utils.MEdge(if b then fX else fY))
    | None -> if fX = fY
    then (Utils.MEdge fX)
    else
  (
    Utils.MNode ((xX, []), ((), fX, fY))
  )

let solve_or ((), (((xX, tlX), iX) as fX), (((xY, tlY), iY) as fY)) =
  assert(xX = xY);
  match is_cst fX with
  | Some b ->
    (Utils.MEdge(if b then fX else fY))
  | None -> match is_cst fY with
    | Some b ->
      (Utils.MEdge(if b then fY else fX))
    | None -> if fX = fY
    then (Utils.MEdge fX)
    else
  (
    Utils.MNode ((xX, []), ((), fX, fY))
  )

let solve_xor ((), fX, fY) =
  let aX = arity fX and aY = arity fY in
  assert(aX = aY);
  match is_cst fX, is_cst fY with
  | Some b0, Some b1 -> Utils.MEdge(cst (b0<>b1) aX)
  | Some false, _ -> Utils.MEdge fY
  | _, Some false -> Utils.MEdge fX
  | _, _ -> if fX = fY
  then (Utils.MEdge (cst false aX))
  else (
    Utils.MNode ((aX, []), ((), fX, fY))
  )

let cons_and  node = match solve_and node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let cons_or  node = match solve_or node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let cons_xor node = match solve_xor node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node
