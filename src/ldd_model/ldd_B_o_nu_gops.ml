(*
 * LGPL-3.0 Linking Exception
 *
 * Copyright (c) 2017-2020 Joan Thibault (joan.thibault@irisa.fr)
 *
 * package : DAGaml  : Abstract DAG manipulation in OCaml
 *
 * folder  : ldd_model      : implementation of some models Lambda Decision Diagram
 *
 * model   : ldd-o-nu : Ordered model with Negation and Useless variables
 *
 * module  : Ldd_o_nu_gops : General OPeratorS
 *)

open GuaCaml
open Extra
open STools
open BTools
open Ldd_B_o_nu_types

(* Section 0. Preliminaries *)

let arity ((_, (n, _)), _) = n

let arity_node ((), e0, e1) =
  let a0 = arity e0 and a1 = arity e1 in
  assert(a0 = a1);
  (succ a0)

let pushU ?(n=1) ((b, (x, y)), nx) =
  assert(n>=0);
  ((b, (n+x, y)), nx)

(* Section 1. IO *)

module ToS =
struct
  open ToS
  open TreeUtils.ToS

  let uniq = int * int
  let pair = trio int int int

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToB =
struct
  open ToB
  open TreeUtils.ToB

  let uniq = int * int
  let pair = trio int int int

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfB =
struct
  open OfB
  open TreeUtils.OfB

  let uniq = int * int
  let pair = trio int int int

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module IoB =
struct
  let leaf = (ToB.leaf, OfB.leaf)
  let edge = (ToB.edge, OfB.edge)
  let node = (ToB.node, OfB.node)

  let next' ident = (ToB.next' (fst ident), OfB.next' (snd ident))
  let edge' ident = (ToB.edge' (fst ident), OfB.edge' (snd ident))
  let node' ident = (ToB.node' (fst ident), OfB.node' (snd ident))
end

module ToBStream =
struct
  open ToBStream
  open TreeUtils.ToBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module OfBStream =
struct
  open OfBStream
  open TreeUtils.OfBStream

  let uniq = int * int
  let pair = trio int int int

  let leaf = unit
  let edge = bool * uniq
  let node = unit

  let next' ident = gnext ident leaf
  let edge' ident = edge * (next' ident)
  let node' ident = trio node (edge' ident) (edge' ident)
end

module ToBa =
struct
  open BTools.ToB

  let next' a (ident:'i tob) (nx:_ next') (s:stream) : stream =
    match nx with
    | Tree.GLeaf () -> (assert(a = 0); s)
    | Tree.GLink lk -> (assert(a > 0); ident lk s)

  (* side = function false -> 0 | true  -> 1 *)
  let edge' side a (ident:'i tob) (((b, (x, y)), nx): _ edge') (s:stream) : stream =
    assert(x = a);
    assert(y <= x);
    let s = int (x-y) (next' y ident nx s) in
    if side then (bool b s) else (assert(b = false); s)

  let node' (ident:'i tob) (node: _ node') (s:stream) : stream =
    let a = arity_node node in
    trio unit
      (edge' false (pred a) ident)
      (edge' true  (pred a) ident) node s
end

module OfBa =
struct
  open BTools.OfB

  let next' a (ident:int -> 'i ofb) (s:stream) : _ next' * stream =
    if a = 0
    then (Tree.GLeaf(), s)
    else (let lk, s = ident a s in (Tree.GLink lk, s))

  let edge' side a (ident:int -> 'i ofb) s : _ edge' * stream =
    let b, s = if side then bool s else (false, s) in
    let d, s = int s in
    assert(d <= a);
    let y = a - d in
    let nx, s = next' y ident s in
    (((b, (a, y)), nx), s)

  let node' a (ident:int -> 'i ofb) s : _ node' * stream =
    trio unit
      (edge' false (pred a) ident)
      (edge' true  (pred a) ident) s
end

let pretty_edge (b, (i, j)) =
  (Utils.mp_of_bool b)^" "^(string_of_int i)^" -> "^(string_of_int j)

(* Section 2. Shannon *)

let neg ((b, l), i) = ((not b, l), i)
let cneg x ((b, l), i) = ((x <> b, l), i)

let cst b n = ((b, (n, 0)), Tree.GLeaf())
let get_cst b f = cst b (arity f)

let is_cst = function
  | ((b, _), Tree.GLeaf ()) -> Some b
  | _ -> None

let solve_cons ((), (((bX, (xX, yX)), iX) as edgeX), (((bY, (xY, yY)), iY) as edgeY)) =
  assert(xX = xY);
  if edgeX = edgeY
  then (Utils.MEdge ((bX, (xX+1, yX)), iX))
  else
  (Utils.MNode (
    (bX, (xX+1, xX+1)),
    ((), ((false, (xX, yX)), iX), ((bX<>bY, (xY, yY)), iY))
  ))

let compose (bC, (xC, yC)) ((bc, (xc, yc)), i) =
  assert(yC = xc);
  ((bC <> bc, (xC, yc)), i)

let node_pull ((b, (x, y)), i) =
  assert(x>=y);
  if x = y
  then
  (
    Utils.MNode (Utils.gnode_node i, fun ((), x', y') ->
      ((), cneg b x', cneg b y')
    )
  )
  else
  (
    let e = ((b, (x-1, y)), i) in
    Utils.MEdge ((), e, e)
  )

(* Section 3. Operators *)

let solve_and ((), (((bX, (xX, yX)), iX) as edgeX), (((bY, (xY, yY)), iY) as edgeY)) =
  assert(xX = xY);
  match iX with
  | Tree.GLeaf () -> Utils.MEdge (if bX then (* x = 1 *) edgeY else (* x = 0 *) edgeX)
  | Tree.GLink nx ->
  match iY with
  | Tree.GLeaf () -> Utils.MEdge (if bY then (* y = 1 *) edgeX else (* y = 0 *) edgeY)
  | Tree.GLink ny ->
  if (nx = ny) && (yX = yY)
  then Utils.MEdge (if bX = bY
    then (* x =  y *) edgeX
    else (* x = ~y *) (get_cst false edgeX)
                 )
  else
  (
    let yXY = max yX yY in Utils.MNode (
      (false, (xX, yXY)),
      ((), ((bX, (yXY, yX)), iX), ((bY, (yXY, yY)), iY))
    )
  )

let solve_xor ((), (((bX, (xX, yX)), iX) as edgeX), (((bY, (xY, yY)), iY) as edgeY)) =
  assert(xX = xY);
  match iX with
  | Tree.GLeaf () -> Utils.MEdge (cneg bX edgeY)
  | Tree.GLink nx ->
  match iY with
  | Tree.GLeaf () -> Utils.MEdge (cneg bY edgeX)
  | Tree.GLink ny ->
  if (nx = ny) && (yX = yY)
  then Utils.MEdge (get_cst (bX<>bY) edgeX)
  else
  (
    let yXY = max yX yY in Utils.MNode (
      (bX<>bY, (xX, yXY)),
      ((), ((false, (yXY, yX)), iX), ((false, (yXY, yY)), iY))
    )
  )

let solve_node node = match solve_cons node with
  | Utils.MEdge (edge, next) -> edge, Utils.MEdge next
  | Utils.MNode (edge, node) -> edge, Utils.MNode node

let cons_and  node = match solve_and node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let cons_xor  node = match solve_xor node with
  | Utils.MEdge (edge, next) -> edge, Utils.M3Edge next
  | Utils.MNode (edge, node) -> edge, Utils.M3Node node

let solve_of_tag = TacxTypes.(function
  | And  -> solve_and
  | Cons -> solve_cons
  | Xor  -> solve_xor)

let solve_tacx (tag, edge0, edge1) = match (solve_of_tag tag) ((), edge0, edge1) with
  | Utils.MEdge (edge, next) -> edge, Utils.MEdge next
  | Utils.MNode (edge, ((), edge0, edge1)) -> edge, Utils.MNode (tag, edge0, edge1)
