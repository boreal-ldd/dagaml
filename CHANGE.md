# Preliminary

## Acronyms

# LDD : Lambda Decision Diagram

# DP : Day   x Person
# WP : Weak  x Person
# MP : Month x Person
# YP : Year  x Person

# Change

## release-0.03 [LATER]

### 2022 / 07 / 04 (10HP) : Refined Primal Graph Estimation [MR !13]
- added `RefinedPrimalGraph` module to `NAX`
- added `RefinedPrimalGraph` module to `Constraint_system_rbtf`
- added `refined_primal_graph_of_system` function to `Constraint_system_utils`
- added `REFINE` option to `test_rbtf`


### 2022 / 07 / 04 (13HP) : introducing `FlatNAX`, an n-ary NAX representation
- introduced FlatNax (and n-ary NAX representation)
- added conversion from NAX to Expr and Graphviz
- upgraded RExpr and extended export facilities
  + XNNF text display
  + FOAX text display
- added test facilities for **FlatNAX**
  - basic test facilities : evaluation of flow impact metric
  - basic test for determined variables
  - basic test for namespace reordering/contraction
- extended expression and circuit syntax with `true` and `false`
- fixed `unB` into `mergeB` in `AQEOPS_RBTF`

### 2022 / 06 / 19 (12HP) : adding BinUdag
- added serialized (binarized) N-ary Unified DAG
- swaped parameters of gnext from ('leaf, 'link') to ('link, 'leaf)
  - **TODO** propagate this change to unify BinUbdag's interface with BinUdag's interface
- started functorizing out the memoization/caching mechanism of the operators in BinUdagT
  - **TODO** propagate this change to every core modules

### 2022 / 04 / 19 (1 WP) : functorizing / unifying RBTF test modules
- removed `test_rbtf_*` files and added `test_rbtf` file
- added CIR input representation (LATER : make it output representation as well)
  + added parser based upon ParseUtils.SSBL
  + based internal representation on `Expr` module
    * added a specific parser for `Expr` : `StrLoadExpr`
  + introduced new abstract representation `Constraint_sytem_types` (factorizing out `CnfTypes`)
- interface with GuaCaml's Streem sub-module to safeguard from OCaml's Stream module deprecation
- factorized code in `AQEOPS_RBTF` and `AQEOPS_CRBTF` by introducing `compute ~frp_only`

## release-0.02

### 2021 / 01 / 20 (1 WP) : added ad-hoc `ComposeOops`-level variable ordering to `test_dpll_trace`
- TODO transpose the variable ordering part to other modules

### 2021 / 01 / 15 (1 WP) : added waterfall modules (naive internal sat-solver)
- NOTE encouraging results on the satlib benchmarks

### 2020 / 11 / 21 : extended preliminary work integrating RBTF into mainstream CNF solving
  - extended preliminary tests on RBTF integration with `lightspeed` heuristic from `Snowflake`
	- fixed the preprocessing of CNF to deal with redundant clauses
	  + i.e. clauses where the same variables appear several times
		  * possibly with a different polarity, leading to the clauses being always satisfied [FIXED]
	- designed an interface with D4 to allows external solving of local formulas
	  + [TESTME] added a consistent renaming algorithm upon CNF dumping in order remove unused variables

## release-0.01

### 2020 / 10 / 27 : added lambda-loop logo

### 2020 / 10 : (2 MP) RBTF/WAP with LDD-O-NU, LDD-O-NUCX, LDD-U-NU, LDD-U-NUCX

### 2020 / 03 : (2 MP) RBTF/WAP with LDD-O-U

### 2018 / 12 : (4 MP) LDD-U-NUCX

### 2017 / 09 : (3 MP) core/binUbdag

### 2017 / 06 : (6 MP) LDD-U-NUC

### 2016 / 06 : (3 MP) LDD-L-NNUX

### 2015 / 12 : (3 MP) LDD-U-NNUCX [FAILED]
	- EDIT : 2017 / 06 : we proved that this model does not exists.

### 2015 / 06 : (1 MP) LDD-U-NUX

### 2015 / 03 : (1 MP) LDD-U-NU

## empty
