import sys

file_in = sys.argv[1]
file_out = sys.argv[2]

FILE = open(file_in)
TEXT = FILE.read().split('\n')
FILE.close()

def parse_time(time) :
	assert(time[-1] == 's')
	time = time[:-1]
	min_, sec_ = tuple(time.split('m'))
	msec_ = '0'
	if '.' in sec_ :
		sec_, msec_ = tuple(sec_.split('.'))
	elif '-' in sec_ :
		sec_, msec_ = tuple(sec_.split('-'))
	elif sec_ == '' :
		sec_ = 0
	else :
		pass # TODO ?
	return str(60 * int(min_) + int(sec_) + int(msec_) / 1000.)

def parse_time_tag(time_tag) :
	if "-2-" not in time_tag :
		print("[parse_time_tag] time_tag:"+time_tag)
	model = time_tag.split('-2-')[1]
	assert(model in ['o-u', 'o-nu', 'o-c10', 'o-uc0', 'o-nucx', 'u-nu', 'u-nux', 'u-nuc', 'u-nucx', 'dd'])
	return model

FILE = open(file_out, 'w')

for line in TEXT :
	if "-2-" not in line : continue
	try:
		l = tuple(line.split(' '))
		if len(l) != 2 : print("error[0] incorrect number of ' ' :"+str(l))
		name, line = l
		l = tuple(line.split(':'))
		if len(l) != 2 : print("error[1] incorrect number of ':' :"+str(l))
		time_tag, time = l
		try:
			model = parse_time_tag(time_tag)
		except Exception as err :
			print("error[2] parse_time_tag :"+str(l))
			raise err
		try:
			time = parse_time(time)
		except Exception as err :
			print("error[3] parse_time :"+str(l))
			raise err
		FILE.write('file_name:%s model:%s time=%s\n'%(name, model, time))
	except Exception as err :
		print(err)

FILE.close()
