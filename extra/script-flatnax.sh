# time ./bin/test_rbtf.native REFINED 2013-hard/f-sub4p.cir 2013-hard/f-sub4p.cir.refined.cft.dot --output-graphviz --output-CFT --set-vertices-of-interest 158 807 808 4167 4168 4823 6146 6817 6819 7101 9559 9734 10085 10086 12066 12067 12072 12073 12084 12085 12088 12089 12090 12091 12092 12095 12098 12101 12106 12111 12112 12159 12160 12202 12203 12211 12212 12226 12227 12228 12229 12259 12260 12261 12271 12272 12287 12288 12289 12290 12297 12300 12301 12309 12310 12311 12312 12313 12316 12317 12352 12353 12384 12385 12402 12403 12408 12409 12410 12411 12426 12427 12511 12514 12516 12517 12518 12519 12520 12523 12524 12527 12528 12548 12549 12593 12594 12595 12596 12597 12598 12623 12624 12627 12628 12638 12639 13103 13110 13115 13116 13117 13118 13119 13120 13130 13131 13132 13134 13135 13136 13137 13138 13141 13142 13151 13152 13162 13163 13206 13207 14132 14133 15066 15067 15068 15069 15070 15071 15072 15075 15079 15080 15081 15082 15083 15084 15087 15088 15089 15095 15118 15119 15120 15123 15135 15138 15151 15152 15153 15154 15252 15253 15329 15330 15333 15334 15335 15338

# for root in "2013-hard/f-soft.cir" "2013-hard/f-hard.cir" # "2013-hard/f-sub4p-sub13n14n15n16n17n18n19n.cir" "2013-hard/f-sub4p-sub13p14p15p16p17p18p19p.cir" # "2013-hard/f-sub4p-sub13p14p16p18p.cir" "2013-hard/f-sub4p-sub13n14n16n18n.cir" # "2013-hard/f.cir" "2013-hard/f-sub4p.cir" "2013-hard/f-sub4n.cir"
# do
# 	echo -e "\nroot: $root"
# 	echo -e "\tlog file : $root.refined.cft.dot.log"
# 	(time ./bin/test_rbtf.native REFINED $root $root.refined.cft.dot --output-graphviz --output-CFT) &> $root.refined.cft.dot.log
# 	echo -e "\tlog file : $root.refined.cft.ocaml.log"
# 	(time ./bin/test_rbtf.native REFINED $root $root.refined.cft.ocaml --output-ocaml --output-CFT) &> $root.refined.cft.ocaml.log
# 	echo -e "\tlog file : $root.refined.wap.ocaml.log"
# 	(time ./bin/test_rbtf.native REFINED $root $root.refined.wap.ocaml --output-ocaml --wap-true) &> $root.refined.wap.ocaml.log
# done

./bin/test_flatnax.native 2013-hard/f-hard.cir false 2013-hard/f-hard-0.cir
./bin/test_flatnax.native 2013-hard/f-hard.cir true  2013-hard/f-hard-1.cir
./bin/test_flatnax.native 2013-hard/f-hard-1.cir false 2013-hard/f-hard-1-0.cir
./bin/test_flatnax.native 2013-hard/f-hard-1.cir true  2013-hard/f-hard-1-1.cir
./bin/test_flatnax.native 2013-hard/f-hard-1-1.cir false 2013-hard/f-hard-1-1-0.cir
./bin/test_flatnax.native 2013-hard/f-hard-1-1.cir true  2013-hard/f-hard-1-1-1.cir
./bin/test_flatnax.native 2013-hard/f-hard-1-1-1.cir false 2013-hard/f-hard-1-1-1-0.cir
./bin/test_flatnax.native 2013-hard/f-hard-1-1-1.cir true  2013-hard/f-hard-1-1-1-1.cir
./bin/test_flatnax.native 2013-hard/f-hard-1-1-1-1.cir false 2013-hard/f-hard-1-1-1-1-0.cir
./bin/test_flatnax.native 2013-hard/f-hard-1-1-1-1.cir true  2013-hard/f-hard-1-1-1-1-1.cir
