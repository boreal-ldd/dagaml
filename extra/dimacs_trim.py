import sys

FILE_IN = sys.argv[1]
FILE_OUT = sys.argv[2]

FILE = open(FILE_IN)
TXT = FILE.read()
FILE.close()
L0 = TXT.split('\n')
L0i = [i for i, l in enumerate(L0) if len(l)>=1 and l[0] == '%']
if len(L0i) >= 1:
    print('trimed')
    i = L0i[0]
    L1 = L0[:i]
    FILE = open(FILE_OUT, 'w')
    FILE.write('\n'.join(L1))
    FILE.close()
else:
    print('no trim')
    FILE = open(FILE_OUT, 'w')
    FILE.write('\n'.join(L0))
    FILE.close()

