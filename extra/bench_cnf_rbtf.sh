#!/bin/bash
dagaml="$(readlink -f "$(dirname $(readlink -f $0))/..")"
echo "dagaml: " $dagaml
cd $dagaml
#rm -rf workdir; mkdir workdir
extra=$dagaml/extra
bin=$dagaml/bin
conv="$bin/conv_ext.native"
abc="${BOREAL_ABC_CMD}"
#export abc
#echo "abc: " $abc
#if [ -z $abc ]
#then
#	echo "abc check : nope"
#	exit 1
#else
#	if [ -e $abc ]
#	then
#		echo "abc check : pass"
#	else
#		echo "abc check : fail"
#		exit 1
#	fi
#fi
#cloud_dd="${BOREAL_CLOUD_DD_CMD}"
#echo "cloud_dd: " $cloud_dd
#if [ -z $cloud_dd ]
#then
#	echo "Cloud-DD check : nope"
#else
#	if [ -e $cloud_dd ]
#	then
#		echo "Cloud-DD check : pass"
#	else
#		echo "Cloud-DD check : fail"
#		exit 1
#	fi
#fi
echo "" > logs.fof
echo "" > diff.log
for src in $@
do
	dst=$(python3 $extra/extract_name.py $src)
	echo "$dst"
	install -D /dev/null $dst.null; rm $dst.null
	echo "INIT" > $dst.done
	cp $src $dst.cnf
	echo "CP" > $dst.done
	echo "primal graph"
	(time $bin/test_rbtf_o_u.native --primgraph $dst.cnf $dst.graph) &> $dst.graph.log
	echo "wap solving"
	(time timeout 10m $bin/test_rbtf_o_u.native --wap-solver $dst.cnf $dst.wap) &> $dst.wap.log
	value_time=$(grep real $dst.wap.log | tr '\t' '\n' | sed "1~2d")
	value_cost=$(grep "^cost:" $dst.wap.log | sed "s/ //g" | tr ':' '\n' | sed "1~2d")
	if [ -e $dst.wap ]; then value_wap="true"; else value_wap="false"; fi
	echo "file:$dst time:$value_time cost:$value_cost wap:$value_wap"
	echo "file:$dst time:$value_time cost:$value_cost wap:$value_wap" >> workdir/global.data
	# echo "compute : rbtf --bpp --aqeops"
	# (time $bin/test_rbtf_o_u.native --bpp-a-verilog $dst.cnf $dst-rbtf.v) &> $dst.rbtf.log
	# echo "compute : rbtf --bpp --aqeops --d4"
	# (time $bin/test_rbtf_o_u.native --bpp-a-d4-verilog $dst.cnf $dst-rbtf-d4.v) &> $dst.rbtf-d4.log
	echo "DONE" > $dst.done
done
