import time
t = time.gmtime()
def norm2(x):
	s = str(x)
	return '0'*(2-len(s))+s
print("%s-%s-%s.%sh%sm%ss"%tuple(map(norm2, (t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec))))

