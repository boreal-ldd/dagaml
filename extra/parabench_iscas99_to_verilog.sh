#!/bin/bash
abc="${BOREAL_ABC_CMD}"
echo "abc: " $abc
if [ -e $abc ]
then
	echo "abc check : pass"
else
	echo "abc check : fail"
	exit 1
fi

echo "" > nohup.out
echo "" > global.data
rm -rf workdir


bench="DAGaml-benchmarks/iscas99/*.pla"
output="$(python3 extra/gmtime.py).iscas99-to-verilog.xz"
echo "param : '$1', bench : $bench, output : $output"
startdate="$(date)"
echo startdate > log.log
echo "" > global.data
echo "start date: $startdate"

timeout="10800"
echo "timeout: $timeout"

if [ -z ${BOREAL_BENCH_NPROC} ]
then
	nproc=1
else
	nproc=${BOREAL_BENCH_NPROC}
fi
echo "nproc: $nproc"

echo "main loop : start"
if true
# true  : use parallel command (from package parallel)
# false : do not use parallel command
then
	time ls $bench | parallel --jobs $nproc --timeout $timeout '( ./extra/bench_pla_to_verilog.sh {} >> log.log 2>&1 )' &> log.log
else
	function loop () {
		for target in $(ls $bench)
		do
			timeout $timeout ./extra/bench_pla_to_verilog.sh $target >> log.log 2>&1
		done
	}
	time loop
fi
echo "main loop : done"

echo "finalization : start"
extra/parse_time.sh > parse_time.log
cat grep_time.data >> global.data
sort global.data > workdir/global.data
cp workdir/global.data global.data
cp log.log workdir/
cp nohup.out workdir/
echo "finalization : done"

echo "compression : start"
tar -cJf $output workdir/
echo "compression : done"

if true
then
	echo "computing stats : start"
	cp workdir/global.data logs/data/iscas85.data
	echo "computing stats : done"
else
	echo "computing stats : skip"
fi

