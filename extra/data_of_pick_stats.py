import sys

with open(sys.argv[1]) as ifile :
	TXT = ifile.read().split('\n')

name = sys.argv[1].split('/')[-1].split('.')[0]

if not (len(TXT)>=2) :
	print('[error] file_name:'+sys.argv[1]+'\n')
	print('[error] len(TXT)='+str(len(TXT))+'\n')
	assert(False)
model = TXT[0]

L1 = TXT[1].split(' ')
if not (len(L1)==4) :
	print('[error] file_name:'+sys.argv[1])
	print('[error] len(L1)='+str(len(L1)))
	assert(False)
num_node, size_node, num_edge, size_edge = tuple(L1)

size_total_bytes = str(22*int(num_node)+10*int(num_edge)+int(size_node)+int(size_edge))

print('file_name:%s model:%s card_nodes=%s size_nodes_bytes=%s card_edges=%s size_edges_bytes=%s size_total_bytes=%s'%(name, model, num_node, size_node, num_edge, size_edge, size_total_bytes))
