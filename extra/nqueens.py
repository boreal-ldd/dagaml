class VAR :
    def __init__(self, v, s = False):
        self.var = v
        self.sign = s
    def negb(self):
        return VAR(self.var, not self.sign)
    def __str__(self):
        if self.sign:
            return '-'+(str(self.var+1))
        else:
            return str(self.var+1)

class CNF : 
    def __init__(self, n):
        self.input = n
        self.formule = []
    def get_vars(self):
        return [VAR(i) for i in range(self.input)]
    def clause(self, c):
        self.formule.append(c)
    def atLeastOne(self, c):
        self.clause(c)
    def atMostOne(self, c):
        nc = [ci.negb() for ci in c]
        for i, nci in enumerate(nc):
            for j, ncj in enumerate(nc[(i+1):]):
                self.clause([nci, ncj])
    def exactlyOne(self, c):
        self.atLeastOne(c)
        self.atMostOne(c)
    def __str__(self):
        stack = []
        stack.append("p cnf %d %d\n"%(self.input, len(self.formule)))
        for c in self.formule :
            stack.append(' '.join(map(str, c))+' 0\n')
        return ''.join(stack)
            

def nqueens(n):
    cnf = CNF(n**2)
    var = cnf.get_vars()
    board = [[var[n*i+j] for j in range(n)] for i in range(n)]
    # 1] we encode rows and colums
    for i in range(n):
        cnf.exactlyOne([board[i][j] for j in range(n)])
        cnf.exactlyOne([board[j][i] for j in range(n)])
    # 2] we encode diagonals
    #   a) we encode diagonals which rise from left to right
    for i in range(-n, n):
        cnf.atMostOne([board[i+j][j] for j in range(n) if i+j>=0 and i+j<n])
    #   b) we encode diagonals which rise from right to left
    for i in range(0, 2*n):    
        cnf.atMostOne([board[i-j][j] for j in range(n) if i-j>=0 and i-j<n])
    return cnf

if __name__ == '__main__' :
    import sys
    n = int(sys.argv[1])
    out_name = sys.argv[2]
    out_string = str(nqueens(n))
    out_file = open(out_name, 'w')
    out_file.write(out_string)
    out_file.close()
    
