cd src/
echo "" > temp.txt
for file in *
do
	if [ -d $file ]
	then (cd $file
		echo $(wc -l $(find -name "*.ml") | grep total | sed "s/total//") ".ml" $file >> ../temp.txt
		#echo $(wc -l $(find -name "*.mli") | grep total | sed "s/total//") ".mli" $file >> ../temp.txt
	)
	fi
done
cat temp.txt | sort -n
rm temp.txt

