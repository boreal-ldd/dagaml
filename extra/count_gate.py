import sys

file_in = sys.argv[1]

FILE = open(file_in)
TEXT = FILE.read()
FILE.close()

print('number of "~": %d'%TEXT.count('~'))
print('number of "&": %d'%TEXT.count('&'))
print('number of "|": %d'%TEXT.count('|'))
print('number of "^": %d'%TEXT.count('^'))
