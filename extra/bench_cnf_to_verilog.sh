#!/bin/bash
dagaml="$(readlink -f "$(dirname $(readlink -f $0))/..")"
echo "dagaml: " $dagaml
cd $dagaml
#rm -rf workdir; mkdir workdir
extra=$dagaml/extra
bin=$dagaml/bin
conv="$bin/conv_ext.native"
abc="${BOREAL_ABC_CMD}"
export abc
echo "abc: " $abc
if [ -z $abc ]
then
	echo "abc check : nope"
	exit 1
else
	if [ -e $abc ]
	then
		echo "abc check : pass"
	else
		echo "abc check : fail"
		exit 1
	fi
fi
cloud_dd="${BOREAL_CLOUD_DD_CMD}"
echo "cloud_dd: " $cloud_dd
if [ -z $cloud_dd ]
then
	echo "Cloud-DD check : nope"
else
	if [ -e $cloud_dd ]
	then
		echo "Cloud-DD check : pass"
	else
		echo "Cloud-DD check : fail"
		exit 1
	fi
fi
echo "" > logs.fof
echo "" > diff.log
for src in $@
do
	dst=$(python3 $extra/extract_name.py $src)
	echo "$dst"
	install -D /dev/null $dst.null; rm $dst.null
	echo "INIT" > $dst.done
	cp $src $dst.cnf
	if false # select preprocessing
	# true  = use external preprocessor (usually faster)
	# false = DAGaml's default preprocessor
	then
		echo "pre-processing:enabled"
		sed -i "s/%//;s/^0//" $dst.cnf
		if false # select preprocessing
		# true  = use d4
		# false = use DPLL-based experimental preprocessor
		then
			echo "pre-processing:d4"
			echo "d4 : .cnf -> .nnf"
			(time $extra/d4 $dst.cnf -out="$dst_v") &> $dst.pp-d4.log
			echo ".nnf -> .v"
			dst_v="$dst.nnf"
			(time $conv $dst .nnf $dst_v .v) &> $dst.preproc.log
		else
			echo "pre-processing:waterfall"
			echo "dagaml : .cnf -> .v"
			dst_v="$dst.dpll"
			(time $bin/test_dpll_trace.native $dst.cnf v $dst_v.v) &> $dst.cnf-2-v.log
		fi
		if [ ! -z $cloud_dd ]
		then
			echo ".v -> .cmd"
			(time $conv $dst_v .v $dst_v .cmd) &> $dst_v-2-cmd.log
			echo "set output file in .cmd"
			sede="s/info y0/store y0 $(echo "$dst" | sed "s/\//\\\\\//g").dd/"
			echo $sede
			sed -i "$sede" $dst_v.cmd
			echo "compute .cmd"
			(time $cloud_dd -f $dst_v.cmd) &> $dst.cmd-2-dd.log
			grep -R real $dst.cmd-2-dd.log > $dst.dd-time.log
			python $extra/parse_time.py $dst.dd-time.log $dst.dd-time.data
			cat $dst.dd-time.data >> global.data
		fi
		for tag in "o-uc0" "o-nucx" "u-nucx" # "o-u" "o-nu" "o-c10" "o-uc0" "o-nucx" "u-nu" "u-nux" "u-nuc" "u-nucx"
		do
			echo ".v -> .$tag.B.pure"
			(time $conv $dst_v .v $dst .$tag.B.pure) &> $dst.v-2-$tag.log
			echo ".$tag.B.pure -> .stats"
			(time $conv $dst .$tag.B.pure $dst.$tag.B.pure .stats) &> $dst.$tag-2-stats.log
			echo "cec: .$tag.B.pure"
			(time $abc -c "read $dst_v.v; cec $dst.$tag.v") &> $dst.$tag.cec
			echo "$tag" > $dst.$tag.csv
			python3 $extra/pick.py --strip " :()[]\"" --sep " " -7 -5 -3 -1 "$(cat $dst.$tag.B.pure.stats)" >> $dst.$tag.csv
			python3 $extra/data_of_pick_stats.py $dst.$tag.csv >> global.data
		done
	else
		echo "pre-processing:disabled"
		(time $conv $dst .cnf $dst.cnf .v) &> $dst.cnf-2-v.log
		if [ ! -z $cloud_dd ]
		then
			echo ".v -> .cmd"
			(time $conv $dst.cnf .v $dst.cnf .cmd) &> $dst.v-2-cmd.log
			echo "set output file in .cmd"
			sede="s/info y0/store y0 $(echo "$dst" | sed "s/\//\\\\\//g").dd/"
			echo $sede
			sed -i "$sede" $dst.cnf.cmd
			echo "compute .cmd"
			(time $cloud_dd -f $dst.cnf.cmd) &> $dst.cmd-2-dd.log
			grep -R real $dst.cmd-2-dd.log > $dst.dd-time.log
			python $extra/parse_time.py $dst.dd-time.log $dst.dd-time.data
			cat $dst.dd-time.data >> global.data
		fi
		for tag in "o-nucx" "u-nucx" # "o-u" "o-nu" "o-c10" "o-uc0" "o-nucx" "u-nu" "u-nux" "u-nuc" "u-nucx"
		do
			echo ".v -> .$tag.B.pure"
			(time $conv $dst.cnf .v $dst .$tag.B.pure) &> $dst.cnf-2-$tag.log
			echo ".$tag.B.pure -> .stats"
			(time $conv $dst .$tag.B.pure $dst.$tag.B.pure .stats) &> $dst.$tag-2-stats.log
			echo ".$tag.B.pure -> .v"
			(time $conv $dst .$tag.B.pure $dst.$tag .v) &> $dst.$tag-2-v.log
			echo "cec: .$tag.B.pure"
			(time $abc -c "read $dst.cnf.v; cec $dst.$tag.v") &> $dst.$tag.cec
			echo "datagen: .$tag.B.pure"
			echo "$tag" > $dst.$tag.csv
			python3 $extra/pick.py --strip " :()[]\"" --sep " " -7 -5 -3 -1 "$(cat $dst.$tag.B.pure.stats)" >> $dst.$tag.csv
			python3 $extra/data_of_pick_stats.py $dst.$tag.csv >> global.data
		done
	fi
	echo "DONE" > $dst.done
done
