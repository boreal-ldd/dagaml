#!/bin/bash
dagaml="$(readlink -f "$(dirname $(readlink -f $0))/..")"
echo "dagaml: " $dagaml
cd $dagaml
#rm -rf workdir; mkdir workdir
extra=$dagaml/extra
bin=$dagaml/bin
conv="$bin/conv_ext.native"
abc="${BOREAL_ABC_CMD}"
export abc
echo "abc: " $abc
if [ -e $abc ]
then
	echo "abc check : pass"
else
	echo "abc check : fail"
	exit 1
fi
echo "" > logs.fof
echo "" > diff.log
for src in $@
do
	dst=$(python3 $extra/extract_name.py $src)
	echo "$dst"
	install -D /dev/null $dst.null; rm $dst.null
	echo "INIT" > $dst.done
	cp $src $dst.pla
	for tag in "o-u" "o-nu" "o-c10" "o-uc0" "o-nucx" "u-nu" "u-nux" "u-nuc" "u-nucx"
	do
		echo ".cnf -> .$tag.B.pure -> .stats"
		(time $conv $dst .pla $dst .$tag.B.pure $dst.$tag.B.pure .stats) &> $dst.cnf-2-$tag-2-stats.log
		echo ".$tag.B.pure -> .v"
		(time $conv $dst .$tag.B.pure $dst.$tag .v) &> $dst.$tag-2-v.log
		##abc's cec with input is not available as transformation throught o-* does not preserve input naming [FIXME]
		#echo "cec: .$tag.B.pure"
		#(time $abc -c "read $dst.cnf.v; cec $dst.$tag.v") &> $dst.$tag.cec
		echo "datagen: .$tag.B.pure"
		echo "$tag" > $dst.$tag.csv
		python3 $extra/pick.py --strip " :()[]\"" --sep " " -7 -5 -3 -1 "$(cat $dst.$tag.B.pure.stats)" >> $dst.$tag.csv
		python3 $extra/data_of_pick_stats.py $dst.$tag.csv >> global.data
	done
	echo "LOOP" > $dst.done
	for tag in "o-nu" "o-c10" "o-uc0" "o-nucx" "u-nu" "u-nux" "u-nuc" "u-nucx"
	do
		echo "cec: .$tag.B.pure"
		(time $abc -c "read $dst.o-u.v; cec $dst.$tag.v") &> $dst.$tag.cec
	done
	echo "DONE" > $dst.done
done
