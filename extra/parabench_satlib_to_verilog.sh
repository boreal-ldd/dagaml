#!/bin/bash
echo "BEGIN" > nohup.out
#echo $0
#echo "PID: " $(pgrep $0)
#pgrep $0 >> /sys/fs/cgroup/memory/fullram-cgroup/tasks

abc="${BOREAL_ABC_CMD}"
echo "abc: " $abc
if [ -e $abc ]
then
	echo "abc check : pass"
else
	echo "abc check : fail"
	exit 1
fi


echo "rm/mkdir workdir/"
rm -rf workdir
mkdir workdir
echo "main loop : start"
case $1 in
	20)  tag1="uf20-91"   ; tag2="uf20"  ;;  
	50)  tag1="uf50-218"  ; tag2="uf50"  ;; 
	75)  tag1="uf75-325"  ; tag2="uf75"  ;; 
	100) tag1="uf100-430" ; tag2="uf100" ;; 
	125) tag1="uf125-538" ; tag2="uf125" ;; 
	150) tag1="uf150-645" ; tag2="uf150" ;; 
	175) tag1="uf175-753" ; tag2="uf175" ;; 
	200) tag1="uf200-860" ; tag2="uf200" ;; 
	225) tag1="uf225-960" ; tag2="uf225" ;; 
	1065) tag1="uf250-1065"; tag2="uf1065" ;;  
	*) echo "'$1' is not a valid parameter"; exit -1 ;;
esac
bench="DAGaml-benchmarks/satlib/$tag1/*.cnf"
output="$(python3 extra/gmtime.py).cnf-to-verilog-$tag2.xz"
echo "param : '$1', bench : $bench, output : $output"
startdate="$(date)"
echo startdate > log.log
echo "" > global.data
echo "start date: $startdate"

timeout="27h"
echo "timeout: $timeout"

if [ -z ${BOREAL_BENCH_NPROC} ]
then
	nproc=1
else
	nproc=${BOREAL_BENCH_NPROC}
fi
echo "nproc: $nproc"

if true
# true  : use parallel command (from package parallel)
# false : do not use parallel command
then
	time ls $bench | parallel --jobs $nproc --timeout $timeout '( ./extra/bench_cnf_to_verilog.sh {} >> log.log 2>&1 )' &> log.log
else
	function loop () {
		for target in $(ls $bench)
		do
			timeout $timeout ./extra/bench_cnf_to_verilog.sh $target >> log.log 2>&1
		done
	}
	time loop
fi

# DAGaml-benchmarks/satlib/uf20-91:
# DAGaml-benchmarks/satlib/uf50-218:
# DAGaml-benchmarks/satlib/uf75-325:
# DAGaml-benchmarks/satlib/uf100-430:
# DAGaml-benchmarks/satlib/uf125-538:
# DAGaml-benchmarks/satlib/uf150-645:
# DAGaml-benchmarks/satlib/uf175-753:
# DAGaml-benchmarks/satlib/uf200-860:
# DAGaml-benchmarks/satlib/uf225-960:
# DAGaml-benchmarks/satlib/uf250-1065:

echo "main loop : done"

echo "finalization : start"
extra/parse_time.sh > parse_time.log
cat grep_time.data >> global.data
sort global.data > workdir/global.data
cp workdir/global.data global.data
cp log.log workdir/
cp nohup.out workdir/
echo "finalization : done"

echo "compression : start"
tar -cJf $output workdir/
echo "compression : done"

if true
then
	echo "computing stats : start"
	cp workdir/global.data logs/data/$tag1.data
	echo "computing stats : done"
else
	echo "computing stats : skip"
fi

