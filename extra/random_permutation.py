import numpy
import numpy.random
import sys

if __name__ == "__main__" :
    n = int(sys.argv[1])
    print(' '.join(map(str, numpy.random.permutation(n))))
