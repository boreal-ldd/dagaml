#!/bin/bash

n=$1
echo "n:$n"
nn=$(($n * $n))
echo "n^2:$nn"
N=100
if false
then
	rm permut-$n.txt > /dev/null || true
	for i in $(seq $N)
	do
		echo -ne "$i\r"
		echo $(python extra/random_permutation.py $nn) >> permut-$n.txt
	done
fi
i=0
python extra/nqueens.py $n workdir/q-$n.cnf
rm global.data
cat permut-$n.txt | while read permut
do
	echo "i:$i"
	i=$(($i+1))
	echo "permut:$permut"
	field_permut="[$(echo $permut | tr " " ";")]"
	for tag in "o-nu" "o-uc0" "o-nucx" "u-nu" "u-nux" "u-nuc" "u-nucx"
	do
		./bin/test_dpll_trace.native workdir/q-$n.cnf $tag workdir/q-$n.dpll.$tag.B.pure "$permut" > workdir/q-$n-dpll.cnf-2-$tag.log
		./bin/conv_ext.native workdir/q-$n.dpll .$tag.B.pure workdir/q-$n.dpll .stats > workdir/q-$n-dpll.$tag-2-stats.log
		echo "$tag" > workdir/q-$n-dpll.$tag.csv
		python3 ./extra/pick.py --strip " :()[]\"" --sep " " -7 -5 -3 -1 "$(cat workdir/q-$n.dpll.stats)" >> workdir/q-$n-dpll.$tag.csv
		field_tail=$(python3 ./extra/data_of_pick_stats.py workdir/q-$n-dpll.$tag.csv)
		echo "permut:$field_permut $field_tail" >> global.data
	done
done
