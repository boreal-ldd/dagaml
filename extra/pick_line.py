import sys

LINE = int(sys.argv[1])
FileOut = sys.argv[2]
FilesIn = sys.argv[3:]

FileOUT = open(FileOut, 'w')

for FileIn in FilesIn :
    FileIN = open(FileIn)
    L = FileIN.read().split('\n')[LINE]
    FileIN.close()
    FileOUT.write(FileIn+' '+L+'\n')

FileOUT.close()

