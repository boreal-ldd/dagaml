#!/bin/bash
echo "BEGIN" > nohup.out
#echo $0
#echo "PID: " $(pgrep $0)
#pgrep $0 >> /sys/fs/cgroup/memory/fullram-cgroup/tasks

abc="${BOREAL_ABC_CMD}"
echo "abc: " $abc
if [ -e $abc ]
then
	echo "abc check : pass"
else
	echo "abc check : fail"
	exit 1
fi

echo "rm/mkdir workdir/"
rm -rf workdir
mkdir workdir
echo "main loop : start"
bench="DAGaml-benchmarks/SatComp2019-ZIP/$1/*.cnf"
output="$(python3 extra/gmtime.py).satcomp2019-rbtf-$1.xz"
echo "param : '$1', bench : $bench, output : $output"
startdate="$(date)"
echo startdate > log.log
echo "" > global.data
echo "start date: $startdate"

timeout="5m"
echo "timeout: $timeout"

if [ -z ${BOREAL_BENCH_NPROC} ]
then
	nproc=1
else
	nproc=${BOREAL_BENCH_NPROC}
fi
echo "nproc: $nproc"

if true
# true  : use parallel command (from package parallel)
# false : do not use parallel command
then
	time ls $bench | parallel --jobs $nproc --timeout $timeout '( ./extra/bench_cnf_rbtf.sh {} >> log.log 2>&1 )' &> log.log
else
	function loop () {
		for target in $(ls $bench)
		do
			timeout $timeout ./extra/bench_cnf_rbtf.sh $target >> log.log 2>&1
		done
	}
	time loop
fi

echo "main loop : done"

echo "finalization : start"
extra/parse_time.sh > parse_time.log
cat grep_time.data >> global.data
sort global.data > workdir/global.data
cp workdir/global.data global.data
cp log.log workdir/
cp nohup.out workdir/
echo "finalization : done"

echo "compression : start"
tar -cJf $output workdir/
echo "compression : done"

if true
then
	echo "computing stats : start"
	cp workdir/global.data logs/data/$tag1.data
	echo "computing stats : done"
else
	echo "computing stats : skip"
fi

