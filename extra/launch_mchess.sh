for n in $(seq 3 31)
do
	echo "n: " $n
	# time cadical DAGaml-benchmarks/SatComp2019/mchess_$n.cnf &> __/cadical_chess_$n.txt 
	(time ./bin/test_rbtf_o_u.native --bpp-a-verilog DAGaml-benchmarks/MChess/mchess-$n.cnf __/rbtf_chess_$n.v) &> __/rbtf_chess_$n.log
	get_time=$(grep real __/rbtf_chess_$n.log | tr '\t' '\n' | grep 'm')
	echo "bench:mchess-$n time:$get_time"
done
